<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\bootstrap\Modal;

Modal::begin([
    "id"=>"errorModal",
    "header"=>"<h4 class='modal-title' style='color:red'></h4>",
    "footer"=>"<button class='btn btn-default' onclick='history.go(-1)'>Cerrar</button>",
    "closeButton"=>false,
]);
Modal::end();

$mensaje='<div style="font-size: 18px">Error en la acción realizada</div><div style="font-size: 16px;color:red">'.$exception->getMessage().'</div>';

?>

<link rel="stylesheet" href="css/bootstrap.min.css">

<script>
    function loadJS(url, success){
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0];
        done = false;
        head.appendChild(script);
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
    }

    if (typeof jQuery == 'undefined'){
            loadJS("js/jquery.min.js", function() {
            loadJS("js/bootstrap.min.js", function() {
                $(document).ready(function() {
                    $('#errorModal .modal-dialog').css({'width': '600px'});
                    $('#errorModal .modal-title').html('<p style="color:red">ERROR</p>');
                    $('#errorModal .modal-body').html('<?php echo $mensaje; ?>');
                    $('#errorModal').modal('show');
                });
            });
        });
    } else {
        // jQuery was already loaded
        if ($('#ajaxCrudModal')) {
            $('#ajaxCrudModal .modal-dialog').css({'width': '600px'});
            $('#ajaxCrudModal .modal-title').html('<p style="color:red">ERROR</p>');
            $('#ajaxCrudModal .modal-body').html('<?php echo $mensaje; ?>');
        }else{
            // no tengo modal, muestro errorModal
            $(document).ready(function() {
                $('#errorModal .modal-dialog').css({'width': '600px'});
                $('#errorModal .modal-title').html('<p style="color:red">ERROR</p>');
                $('#errorModal .modal-body').html('<?php echo $mensaje; ?>');
                $('#errorModal').modal('show');
            });
        }
    }

</script>


<!-- Este es el error.php original 
<?php
//print_r($name);
//print_r($message);
//print_r($exception->getCode());
//print_r($exception->getMessage());

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

//$name: the error name
//$message: the error message
//$exception: the exception being handled

//use yii\helpers\Html;
//$this->title = $name;
?>

<div class="site-error">
    <h1><?php //echo Html::encode($this->title) ?></h1>
    <div class="alert alert-danger">
        <?php //echo nl2br(Html::encode($message)) ?>
    </div>
    <div class="alert alert-danger">
        <?php //echo nl2br(Html::encode($exception)) ?>
    </div>
    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>
</div>

-->