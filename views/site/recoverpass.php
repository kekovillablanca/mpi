
 <?php

/* @var $this \yii\web\View */
/* @var $content string */


use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;
$this->title = 'Recuperar password';
$this->params['breadcrumbs'][] = $this->title;
AppAsset::register($this);



?>

        <style> .breadcrumb { background-color: #e1e1e1; 
            width: auto;        } </style>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <nav class="navbar-inverse navbar-fixed-top navbar" style='min-height:51px;'></nav>



</div>




<h3><?= $msg ?></h3>
 
<h1>Recover Password</h1>
<?php 
$form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>
 
<div class="form-group">
 <?= $form->field($model, "email")->textInput(['style'=>'width:350px']) ?>  
</div>
 
<?= Html::submitButton("Recuperar contraseña", ["class" => "btn btn-primary"]) ?>
 
<?php $form->end() ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>