<?php

use kartik\alert\Alert;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>
<script>

function guardarPassword(){
    var $form = $('#change');
    var data = $form.serialize();

    $.ajax({
    url: '<?php echo Url::to(['changepassword']) ?>',
    dataType: 'json',
    type:"POST",
    data: data,
    success: function(data) {
       
        if(data.status=='error' && data.new_check=='check' && data.new=='new'){
            document.getElementById('alertErrorNew').style.display="block"
            
            document.getElementById('alertErrorNew').innerText="La contrasenia nueva no coincide o es la misma que la anterior";
            document.getElementById('alertErrorCheck').style.display="block"
            
            document.getElementById('alertErrorCheck').innerText="La contrasenia nueva no coincide o es la misma que la anterior";
            setTimeout(function() {
                       document.getElementById("alertErrorCheck").style.display="none"
                       document.getElementById("alertErrorNew").style.display="none"
           }, 2000);
        }
        if(data.status=='error' && data.pass_ctrl=='ctrl'){
            document.getElementById('alertErrorCtrl').style.display="block"
            
            document.getElementById('alertErrorCtrl').innerText="La contrasenia esta incorrecta";
            setTimeout(function() {
                document.getElementById("alertErrorCtrl").style.display="none"
           }, 2000);

        }
    
    }
})
}




</script>

<div class="password-form">
<div class="alert alert-warning" >
<?php if(Yii::$app->session->hasFlash('success')) {
    echo Alert::widget([
        'options' => [
            'class' => 'alert-success',
            'id' => 'success-alert', // Agrega un ID para identificar la alerta en JavaScript
        ],
        'body' => Yii::$app->session->getFlash('success'),
    ]);
} ?>


    <h1 class="modal-title">Para generar la nueva contraseña la misma debe contener al menos</h1>
    <ul>
        <li>
            Una letra minuscula
        </li>
        <li>
            Una letra mayuscula
            </li>
            <li>
            Un numero
            </li>
            <li>
            Un caracter no alfanumerico (Ej: @,!,?,%,&,etc).
            </li>
    </ul>
    
</div>
    <?php $form = ActiveForm::begin(['id' => 'change']); ?>
 
    <?php echo $form->field($model, 'password')->passwordInput(['maxlength' => true,'onkeydown' => 'if(event.key === \'Enter\') { event.preventDefault(); guardarPassword(); }']);
    /*Html::passwordInput('password', null, [
    'class' => 'form-control',
    'maxlength' => true,
    'onkeydown' => 'if(event.key === \'Enter\') { event.preventDefault(); guardarPassword(); }'
]) */ ?>
  
    <div class="alert alert-danger" id="alertErrorCtrl" role="alert" style="display: none;">
    </div>

    <?php echo $form->field($change, 'pass_new')->passwordInput(['maxlength' => true,'onkeydown' => 'if(event.key === \'Enter\') { event.preventDefault(); guardarPassword(); }']);?>
    <div class="alert alert-danger" id="alertErrorNew" role="alert" style="display: none;">
    </div>

    <?php echo $form->field($change, 'pass_new_check')->passwordInput(['maxlength' => true,'onkeydown' => 'if(event.key === \'Enter\') { event.preventDefault(); guardarPassword(); }']);?>
    <div class="alert alert-danger" id="alertErrorCheck" role="alert" style="display: none;">
    </div>

    <?php echo Html::a('Cancelar', ['site/login'], ['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).Html::button('Cambiar', ['class' => 'btn btn-primary pull-right' ,'name' => 'Cambiar',
                         'onclick' => 'guardarPassword()','id' => 'cambiar']);?>
    <?php ActiveForm::end(); ?>
    
</div>
