
<?php

use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

//$name: the error name
//$message: the error message
//$exception: the exception being handled

//use yii\helpers\Html;
//$this->title = $name;
?>

<div class="site-error">

    <div class="alert alert-danger">
        <?php echo nl2br(Html::encode($exception->getMessage())) ?>
    </div>


</div>
