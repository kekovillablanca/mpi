<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

?>
<div class="host-index">
    <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'rowOptions'=>function($model){
                        $color=$model->color;
                        return ['style'=>'font-size:14px;font-weight:bold;color:black;background-color:'.$color.';'];
                        },
                'columns' => $columns,
                'toolbar'=> [ ['content'=>''] ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'showHeader' => false,
                'showFooter' => false,
                'showPageSummary' => false,
                'summary' => false,
                'panel' => [
                    'type'=>'primary',
                    'heading' => 'Categoria | Nombre | Servicio | IP | Puerto | Tipo | Tiempo',
                    'footer'=>false,
                    'before'=>false,
                    'after'=>false,
                    'headingOptions'=>['class'=>'panel-heading','style'=>'color:#f0f8f8;background-color:#222222'],
                ]
            ])?>
        
    </div>
</div>

