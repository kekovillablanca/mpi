<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use app\widgets\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Ingreso al Sistema';
$this->params['breadcrumbs'][] = $this->title;
?>
<script>

setTimeout(function() {
    $('[id^=w0-success]').fadeOut('slow');
}, 3000); 
</script>

<div class="site-login">

        <div style='width:30%;float:left'>
        <br>
        <h3><b><?= Yii::$app->name ?></b></h3>
        <h4>Ministerio de Salud . Río Negro</h4>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <br>
        
        <?= $form->field($model, 'username')->textInput(['style'=>'width:350px']) ?>
        <?= $form->field($model, 'password')->passwordInput(['style'=>'width:350px']) ?>
        <div class="form-check">
        <input type="checkbox" class="form-check-input" id="show-password">
        <label class="form-check-label" for="show-password">Mostrar contraseña</label>
    </div>
        <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        <?= Html::a('<p >recuperar contraseña</p>', ['recoverpass'],
            ['role'=>'modal-remote','title'=> 'recover']); ?>

        <?php ActiveForm::end(); ?>
        </div>

        <!-- div para logos, mensajes, novedades, etc.. -->
        <div style='width:70%;float:left'>
        <br><br>
        </div>

</div>

<script>
document.getElementById('show-password').addEventListener('change', function() {
    var passwordField = document.getElementById('loginform-password');
    if (this.checked) {
        passwordField.type = 'text';
    } else {
        passwordField.type = 'password';
    }
});
</script>