<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\Seguridad\Seguridad;

AppAsset::register($this);
?>
<script>
//localStorage.clear();

</script>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body>
<style>
    .panel > .table-responsive > .table-bordered > thead > tr:first-child > th{
    border-bottom: 0;
    line-break: initial;
    text-align: center;
    line-height: initial;
    text-wrap: nowrap;
    vertical-align: top;

}
    .navbar> .container {
        width: 97%;
    }
    .navbar-brand {
        padding-top: 13px;
        color: white!important;
    }
    .dropdown-submenu {
        position: relative;
    }
    .dropdown-submenu>.dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -6px;
        margin-left: -1px;
        -webkit-border-radius: 0 6px 6px 6px;
        -moz-border-radius: 0 6px 6px;
        border-radius: 0 6px 6px 6px;
        min-width: 200px;
    }
    .dropdown-submenu:hover>.dropdown-menu {
        display: block;
    }
    .dropdown-submenu>a:after {
        display: block;
        content: " ";
        float: right;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px 0 5px 5px;
        border-left-color: #ccc;
        margin-top: 5px;
        margin-right: -10px;
    }
    .dropdown-submenu:hover>a:after {
        border-left-color: #fff;
    }
    .dropdown-submenu.pull-left {
        float: none;
    }
    .dropdown-submenu.pull-left>.dropdown-menu {
        left: -100%;
        margin-left: 10px;
        -webkit-border-radius: 6px 0 6px 6px;
        -moz-border-radius: 6px 0 6px 6px;
        border-radius: 6px 0 6px 6px;
    }
    .breadcrumb {
        background-color: #e1e1e1;
    }    

</style>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
if(!Yii::$app->user->getIsGuest()){
        NavBar::begin([
            'brandLabel' => 'RN.Salud',
//            'brandImage' => 'images/logo_menu.png',
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);


    echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items' => [
        ['label' => 'Padrón Pacientes',
            'items' => [
                ['label' => 'Todos', 'url' => ['/mpi/maestro/index']],
                ['label' => 'Federados', 'url' => ['/mpi/maestro/federados']],
                ['label' => 'No Federados', 'url' => ['/mpi/maestro/nofederados']],
                ['label' => 'Autenticados', 'url' => ['/mpi/maestro/autenticados']],
                ['label' => 'No Autenticados', 'url' => ['/mpi/maestro/noautenticados']],
                ['label' => 'Inconsistencias', 'url' => ['/mpi/maestro/inconsistencias']],
                ['label' => 'Duplicados x Federador', 'url' => ['/mpi/maestro/duplicadosfederador']],
                ['label' => 'Duplicados x Renaper', 'url' => ['/mpi/maestro/duplicadosrenaper']],
            ]
        ],
        ['label' => 'Consulta Ambulatoria',
            'items' => [
                ['label' => 'Resumen Paciente', 'url' => ['/mpi/consultaambulatoriamaster/buscarpaciente']],
                ['label' => 'Consultas x Area', 'url' => ['/mpi/consultaambulatoriamaster/buscararea']],
                ['label' => 'reportes',
                    'itemsOptions'=>['class'=>'dropdown-submenu'],
                    'submenuOptions'=>['class'=>'dropdown-menu'],
                    'items' => [
                         ['label' => 'reporte roca', 'url' => ['/mpi/consultaambulatoriamaster/reporte','localidad'=>'hospitalroca']],
                         ['label' => 'reporte bariloche', 'url' => ['/mpi/consultaambulatoriamaster/reporte','localidad'=>'hospitalbariloche']],
                         ['label' => 'reporte cipolleti', 'url' => ['/mpi/consultaambulatoriamaster/reporte','localidad'=>'hospitalcipolletti']],
                         ['label' => 'reporte viedma', 'url' => ['/mpi/consultaambulatoriamaster/reporte','localidad'=>'hospitalviedma']],
                         ['label' => 'reporte consulta odontologia roca', 'url' => ['/mpi/consultaambulatoriamaster/odontologia']],
                         ['label' => 'reportes de consulta ambulatoria migracion al hsi',
                         'itemsOptions'=>['class'=>'dropdown-submenu'],
                         'submenuOptions'=>['class'=>'dropdown-menu'],
                         'items' => [
                            ['label' => 'reporte de roca', 'url' => ['/mpi/consultaambulatoriamaster/migration','centro'=>"hospitalroca"]],
                            ['label' => 'reporte de bariloche', 'url' => ['/mpi/consultaambulatoriamaster/migration','centro'=>"hospitalbariloche"]],
                            ['label' => 'reporte de cipolleti', 'url' => ['/mpi/consultaambulatoriamaster/migration','centro'=>"hospitalcipolleti"]],
                            ['label' => 'reporte de viedma', 'url' => ['/mpi/consultaambulatoriamaster/migration','centro'=>"hospitalviedma"]],
                         ],
                        ],
                    ],
                 ],
                ['label' => 'Consultas x Paciente', 'url' => ['/mpi/consultaambulatoriamaster/totalpaciente']],
                ['label' => 'Listados de Consultas', 'url' => ['/mpi/consultaambulatoriamaster/buscarlistado']],
                ]
        ],

        ['label' => 'Estadística',
            'items' => [
                ['label' => 'Recepcion CMP', 'url' => ['/sies/consulta/recepcioncmp']],
                ['label' => 'Servicios Tecnicos',
                   'itemsOptions'=>['class'=>'dropdown-submenu'],
                   'submenuOptions'=>['class'=>'dropdown-menu'],
                   'items' => [
                        ['label' => 'Laboratorio', 'url' => ['/sies/laboratorio/encabezado']],
                        ['label' => 'Imágenes', 'url' => ['/sies/imagenes/encabezado']],
                        ['label' => 'Cuidados Paliativos', 'url' => ['/sies/cuidados/encabezado']],
                        ['label' => 'Odontologia', 'url' => ['/sies/odontologia/encabezado']],
                        ['label' => 'Cirugia', 'url' => ['/sies/cirugia/encabezado']],
                        ['label' => 'Varios', 'url' => ['/sies/varios/encabezado']],
                        ['label' => 'Salud Mental', 'url' => ['/sies/saludmental/encabezado']],
                    ],
                ],
                ['label' => 'Resumen Consultas',
                    'itemsOptions'=>['class'=>'dropdown-submenu'],
                    'submenuOptions'=>['class'=>'dropdown-menu'],
                    'items' => [
                        ['label' => 'Ingreso de Datos', 'url' => ['/sies/resumenconsulta/resumen']],
                        ['label' => '2021', 'url' => ['/sies/resumenconsulta/index','anio'=>2021]],
                        ['label' => '2022', 'url' => ['/sies/resumenconsulta/index','anio'=>2022]],
                        ['label' => '2023', 'url' => ['/sies/resumenconsulta/index','anio'=>2023]],
                        ['label' => '2024', 'url' => ['/sies/resumenconsulta/index','anio'=>2024]],
                    ],
                ],
                ['label' => 'Consultas Ambulatorias',
                   'itemsOptions'=>['class'=>'dropdown-submenu'],
                   'submenuOptions'=>['class'=>'dropdown-menu'],
                   'items' => [
                        ['label' => '2021', 'url' => ['/sies/consulta/index','anio'=>2021]],
                        ['label' => '2022', 'url' => ['/sies/consulta/index','anio'=>2022]],
                        ['label' => '2023', 'url' => ['/sies/consulta/index','anio'=>2023]],
                        ['label' => '2024', 'url' => ['/sies/consulta/index','anio'=>2023]],
                   ],
                ],
                ['label' => 'Morbilidad Mortalidad',
                   'itemsOptions'=>['class'=>'dropdown-submenu'],
                   'submenuOptions'=>['class'=>'dropdown-menu'],
                   'items' => [
                        ['label' => '2021', 'url' => ['/sies/morbimor/index','anio'=>2021]],
                        ['label' => '2022', 'url' => ['/sies/morbimor/index','anio'=>2022]],
                        ['label' => '2023', 'url' => ['/sies/morbimor/index','anio'=>2023]],
                        ['label' => '2024', 'url' => ['/sies/morbimor/index','anio'=>2024]],
                        ['label' => 'Internacion Viedma', 'url' => ['/sies/consulta/internacionviedma']]
                   ],
                ],
                ['label' => 'Resumen de Servicios Técnicos', /*asaldivar 2022*/
                   'itemsOptions'=>['class'=>'dropdown-submenu'],
                   'submenuOptions'=>['class'=>'dropdown-menu'],
                   'items' => [
                        ['label' => 'Prestaciones CAPs', 'url' => ['/sies/prestacionescaps/index']],
                   ],
                ],
                ['label' => 'Carga Consultas',
                'itemsOptions'=>['class'=>'dropdown-submenu'],
                'submenuOptions'=>['class'=>'dropdown-menu'],
                'items' => [
                    ['label' => 'Pacientes', 'url' => ['/mpi/paciente/index']],
                    ['label' => 'IECMA','url'=> ['/mpi/turno/index']],
                    ['label' => 'Servicios', 'url' => ['/mpi/pservicio/index']],
                    ['label' => 'Diagnosticos', 'url' => ['/mpi/diagnostico/index']],
                    ['label' => 'Profesionales', 'url' => ['/mpi/profesional/index']],
                    ['label' => 'Establecimientos','url'=> ['/mpi/establecimiento/index']],
                    ['label' => 'Profesional Establecimiento','url'=> ['/mpi/profesionalestablecimiento/index']],
                    ['label' => 'Obra social','url'=> ['/mpi/obrasocial/index']],
                    ['label' => 'Especialidad','url'=> ['/mpi/especialidad/index']],
                    ['label' => 'servicio y especialidad','url'=> ['mpi/thesep/index']],

                    ['label' => 'reportes',
                    'itemsOptions'=>['class'=>'dropdown-submenu'],
                    'submenuOptions'=>['class'=>'dropdown-menu'],
                    'items' => [
                        ['label' => 'situacion laboral','url'=> ['/internacion/situacionlaboral/index']],
                        ['label' => 'apiv1','url'=> ['/apiv1/paciente/index']],
                         ['label' => 'reporte 2021', 'url' => ['/sies/consulta/reporte','anio'=>2021]],
                         
                         ['label' => 'reporte 2023', 'url' => ['/sies/consulta/reporte','anio'=>2023]],
                         
                         ['label' => 'reporte turno 2023', 'url' => ['/sies/consulta/reportturno']],
                         //['label' => 'reporte 2021 de sigesh + sies 2021', 'url' => ['/sies/consulta/reportesiessigesh','anio'=>2021]],
                         ['label' => 'reporte 2022 de sigesh + sies 2022', 'url' => ['/sies/consulta/reportesiessigesh','anio'=>2022]],
                         ['label' => 'reporte 2023 de sigesh + sies 2023', 'url' => ['/sies/consulta/reportesiessigesh','anio'=>2023]],

                         ['label' => 'reporte 2023 de sigesh + sies + turno 2023', 'url' => ['/sies/consulta/reportesiessigeshturno']],
                        // ['label' => 'insertar en reportes sies + sigesh', 'url' => ['/sies/consulta/insertarsiessigesh']],
                        ['label' => 'reporte 2022', 'url' => ['/mpi/consultaambulatoriamaster/reporte','anio'=>2022]],

                    ],
                 ],

                 
               
             
                    
                ],
                    
                ],
            ]
            
        ],
        ['label' => 'Tablas',
            'items' => [
                ['label' => 'Dominios', 'url' => ['/mpi/dominio/index']],
                ['label' => 'Servicio', 'url' => ['/sies/mservicio/index']],
                ['label' => 'Area', 'url' => ['/sies/marea/index']],
                ['label' => 'Caps', 'url' => ['/sies/mpuesto/index']],
                ['label' => 'Localidad', 'url' => ['/sies/mlocal/index']],
                /*['label' => 'Prestaciones CAPs', 'url' => ['/sies/prestacionescaps/index']],*/ /*asaldivar 2022*/
            ]
        ],
        ['label' => 'Red Salud',
            'items' => [
                ['label' => 'Monitor',
                'itemsOptions'=>['class'=>'dropdown-submenu'],
                'submenuOptions'=>['class'=>'dropdown-menu'],
               'items' => [
                        ['label' => 'Configuracion', 'url' => ['/monitor/monitor/index']],
                        ['label' => 'Monitor Red', 'url' => ['/site/monitor']],
                        ['label' => 'MPI Log', 'url' => ['/mpi/logscript/index']],
                    ]
                ],
            ],
        ],
        ['label' => 'internacion',
        
        'items' => [['label' => 'Crear internacion', 'url'=> ['/internacion/internacion/index']],
        ['label' => 'Ver internaciones', 'url' => ['/internacion/internacion/indexinternacion']],
        ['label' => 'Movimientos pacientes', 'url' => ['/internacion/movimientopaciente/index']]
        ]

        ,],
        ['label' => 'Sistema',
            'items' => [
                ['label' => 'Mis datos', 'url' => ['/admin/usuario/misdatos']],
                ['label' => 'Seguridad',
                   'itemsOptions'=>['class'=>'dropdown-submenu'],
                   'submenuOptions'=>['class'=>'dropdown-menu'],
                   'items' => [
                        ['label' => 'Usuarios', 'url' => ['/admin/usuario/index']],
                        ['label' => 'Grupos', 'url' => ['/admin/grupo/index']],
                        ['label' => 'Permisos', 'url' => ['/admin/permiso/index']],
                        ['label' => 'Usuarios Estadistica', 'url' => ['/admin/usuarioestadistica/index']],
                    ],
                ],
            ],'visible'=>Seguridad::tienePermisoAdmin()
        ],
       
     
        Yii::$app->user->isGuest ? (
        ['label' => 'Ingresar', 'url' => ['/site/login']]
        ) : (
            '<li>'
            .Html::beginForm(['/site/logout'], 'post')
            .Html::submitButton('Salir (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout'])
            .Html::endForm()
            .'</li>'
        ),

    ],
    ]);


        NavBar::end();}
    ?>

    <div class="container">
        <?php
        $nombre_usuario=Yii::$app->session->get('nombreusuario');
        $datos= isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [];
        $datos[]=[
            'label' => '',
            'template' => "<p style='float: right;'>".$nombre_usuario."</p>", // template for this link only
        ];
//        echo Breadcrumbs::widget(['links' => $datos ])
        ?>
        <?= $content ?>
    </div>
</div>

<?php ?>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Ministerio de Salud. Río Negro. <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::$app->name ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
