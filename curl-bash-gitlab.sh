#!/bin/bash

# Definimos la URL de la tarea de GitLab a ejecutar para que se actualize la imagen de Docker
GITLAB_TASK_URL=$GITLAB_TASK_URL
echo $GITLAB_TASK_URL
# Ejecutamos la tarea usando cURL y guardamos la respuesta en una variable
RESPONSE=$(curl -s -o /dev/null -w "%{http_code}" -X POST $GITLAB_TASK_URL)

# Verificamos si la respuesta es 2xx (es decir, exitosa)
if [[ $RESPONSE =~ ^2[0-9]{2}$ ]]; then
  echo "La tarea se ha ejecutado exitosamente. Se generara otro contenedor para sale-system-backend(Evan)"
  exit 0
else
  echo "Error: la tarea ha fallado con código de respuesta $RESPONSE."
  exit 1
fi
