<?php

namespace app\modules\apiv1\controllers;

use app\components\Seguridad\Seguridad;
use app\modules\mpi\models\Maestro;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;

use Exception;
use Yii;


use yii\web\Controller;

use yii\web\Response;

use yii\filters\AccessControl;
use yii\filters\Cors;

/**
 * Default controller for the `apiv1` module
 */
class PacienteController extends Controller
{

    public static function allowedDomains()
    {
        return [$_SERVER["REMOTE_ADDR"], 'http://localhost:8080'];
    }

    public function behaviors()
{
    $behaviors = parent::behaviors();

    $behaviors['authenticator'] = [
        'class' => \yii\filters\AccessControl::class,
        'only' => ['index', 'view', 'create', 'update', 'delete'], // Define aquí las acciones a las que se aplicará la autenticación
        'rules' => [
            [
                'actions' => ['index', 'view', 'create', 'update', 'delete'],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    return $this->actionAuthenticateApiKey();
                },
            ],
        ],
    ];

    // Otros comportamientos...
    
    return $behaviors;
}



public function actionAuthenticateApiKey()
{

    $apiKey = Yii::$app->request->getHeaders()['api-key'];
    if ($this->isValidApiKey($apiKey)) {
        return true; // API Key válida
    } else {
        throw new \yii\web\ForbiddenHttpException('API Key inválida.');
    }
}

protected function isValidApiKey($apiKey)
{
    // Lógica para verificar si la clave API es válida
    // Puedes buscar la clave API en la base de datos o en alguna otra fuente
    return $apiKey === Yii::$app->params['API-KEY']; // Aquí debes implementar tu lógica de verificación
}

    public function actionIndex($dni=null,$sexo=null,$fechaNacimiento=null,$idPaciente=null)
    {    
        
        
        $apiKey = Yii::$app->request->getHeaders()['api-key'];
        if ($this->isValidApiKey($apiKey)) {

       
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        //esta es la forma de hacer que un paciente tenga muchos turnos (one to many o uno a muchos 'un paciente tiene muchos turnos')
  
       /* ->andFilterWhere(['like', 'to_char(turno.fecha_turno,\'DD/MM/YYYY\')',$this->fecha_turno])
        ->andFilterWhere(['like', 'lower(unidad_edad.descripcion)',strtolower($this->id_unidad_edad)])
        ->andFilterWhere(['like', 'lower(turno.ubicacion_hc)',strtolower($this->ubicacion_hc)])
        
        /*if($dni!=null && $fechaNacimiento==null && $sexo==null){
            $jsonPaciente=Paciente::find()->asArray()->where(['dni' => $dni])->one();
        }*/
        /*if($dni!=null || $fechaNacimiento!=null || $sexo!=null){
            $jsonPaciente=ConsultaAmbulatoriaMaster::find()->where(['paciente_numerodoc' => $dni])->orWhere(['paciente_sexo' => $sexo])->orWhere(['paciente_nac' => $fechaNacimiento])->one();
        }
        //$jsonPaciente=ConsultaAmbulatoriaMaster::find()->asArray()->where(['paciente_numerodoc' => $dni])->orWhere(['paciente_sexo' => $sexo])->orWhere(['paciente_nac' => $fechaNacimiento])->one();
       */ 
   try{

       if($dni==null && $fechaNacimiento==null && $sexo==null && $idPaciente==null){
            throw new Exception("<h1>No se pasaron los parametros de entrada, dni&fechaNacimiento&sexo<h1/><h2>ejemplo:
            Findex&idPaciente=482803&dni=52322706&sexo=male&fechaNacimiento=09/04/2012<h2/>");
         
        }
    
   //SELECT * FROM dominio inner join maestro ON dominio.id = maestro.id_area where maestro.id_maestro=482803;
   $paciente = Maestro::find()->join('inner join','dominio d','maestro.id_area = d.id' )
   ->andWhere(['maestro.id_maestro'=>$idPaciente])
   ->andWhere(['maestro.sexo'=>$sexo])
   ->andWhere(['like', 'to_char(maestro.fecha_nacimiento,\'DD/MM/YYYY\')',$fechaNacimiento])->all();

  /* $connection = Yii::$app->db; 
   $paciente=$connection->createCommand("
    SELECT  m.*,d.id,d.area,d.base,d.dominio
    FROM dominio d
     right JOIN maestro m on (d.id=m.id_area)
     where m.id_maestro= ".$idPaciente."")->queryAll();*/
   /*$query = new Query;
$query  ->select(['maestro.*', 'dominio.id','dominio.area','dominio.base','dominio.dominio'])  
        ->from('maestro' )
        ->leftJoin('dominio', 'dominio.id = maestro.id_area')
        ->andWhere(['maestro.id_maestro'=>$idPaciente]);

$command = $query->createCommand();
$paciente = $command->queryAll();*/

    /*->andWhere(['like', 'lower(m.sexo)',strtolower($sexo)])
    ->andWhere(['like', 'to_char(m.fecha_nacimiento,\'DD/MM/YYYY\')',$fechaNacimiento])
    ->andWhere(['like', 'lower(m.documento_numero)',strtolower($dni)])->one();*/
    //var_dump($paciente->dominio);
    foreach($paciente as $paciente){
    unset($paciente['dominio']["nombre_renaper"]);
    unset($paciente['dominio']["clave_renaper"]);
    unset($paciente['dominio']["coddominio_renaper"]);
    unset($paciente['dominio']["ident_federador"]);
    unset($paciente['dominio']["key_federador"]);
    }
    //var_dump($paciente["dominio"][]);
    //exit;
   // array_diff_key()
    if($paciente==null){
        throw new Exception("<h1>No se pasaron correctamente los parametros de entrada<h2/>");
    }
    }
    
    catch(Exception $e){
        Yii::$app->response->format = Response::FORMAT_HTML;
        return $e->getMessage();

    }
    return ['dominio'=>$paciente->dominio,'paciente'=>$paciente];
    
    } else {
    throw new \yii\web\ForbiddenHttpException('API Key inválida.');
    }
    }
}