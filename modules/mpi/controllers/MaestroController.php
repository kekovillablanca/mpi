<?php

namespace app\modules\mpi\controllers;

use app\modules\mpi\models\Dominio;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
//
use app\modules\mpi\models\Maestro;
use app\modules\mpi\models\MaestroSearch;
use app\modules\mpi\models\Duplicado;
use app\modules\mpi\models\DuplicadoSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\components\JWT\JWT;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;

/**
 * MaestroController implements the CRUD actions for Maestro model.
 */
class MaestroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','federados','nofederados','autenticados','noautenticados',
                           'inconsistencias','duplicadosfederador','duplicadosrenaper',
                           'view','export','select','list','renaper','federador','diferencia'],
                'rules' => [
                    [
                        'actions' => ['index','federados','nofederados','autenticados','noautenticados',
                                      'inconsistencias','duplicadosfederador','duplicadosrenaper',
                                      'view','export','select','list','renaper','federador','diferencia'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Maestro models.
     * @return mixed
     */
    public function actionIndex()
    {

        $model= new Maestro();
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/index');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    }

    public function actionFederados()
    {
    
        $model= new Maestro();
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['flag_federado'=>true]);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/federados');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    
    }

    public function actionNofederados()
    {
    
        $model= new Maestro();
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['flag_federado'=>false]);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/nofederados');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    
    }

    public function actionAutenticados()
    {
    
        $model= new Maestro();
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['flag_autenticado'=>true]);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/autenticados');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    
    }

    public function actionNoautenticados()
    {
    
        $model= new Maestro();
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['flag_autenticado'=>false]);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/noautenticados');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    
    }

    public function actionInconsistencias()
    {

        $model= new Maestro();
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['flag_federado'=>true]);
        $dataProvider->query->andWhere(['flag_modificado'=>true]);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/inconsistencias');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
            'inconsistencia'=>true,
        ]);

    }

    public function actionDuplicadosfederador()
    {

        $model= new Maestro();
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere(['>','id_duplicado',0]);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/duplicadosfederador');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);

    }
    public function actionDuplicadosrenaper()
    {

        $model= new Duplicado();
        $searchModel = new DuplicadoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $columnas=Metodos::obtenerColumnas($model,'mpi/maestro/duplicadosrenaper');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'renaper'=>Seguridad::tienePermiso('renaper'),
                    'federador'=>Seguridad::tienePermiso('federador'),
                ];

        return $this->render('indexduplicado', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);

    }

    /**
     * Displays a single Maestro model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView($model);

        if(isset($_GET['pdf'])){

            $titulo='Datos del Paciente';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $request = Yii::$app->request;
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Paciente #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $model,
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**df kartik
     * Finds the Maestro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Maestro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Maestro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null,$modelo=null)
    {

        $request = Yii::$app->request;
        $reporte= new Reporte();

        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();

        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($modelo){
                $dataProvider=Yii::$app->session->get($modelo.'-dataprovider');

            }else{
                Yii::$app->die();
            }
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null,$modelo=null)
    {
        $request = Yii::$app->request;
    
        if ($modelo){
            $model = new $modelo;
        }else{
            Yii::$app->die();
        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Accion List, utilizada cuando se hace una busqueda autocomplete de este modelo
     * en query->select , verificar el campo de  busqueda
     * @param string de busqueda $q
     * @return arrary de resultados $results
     */
    public function actionList($q = null,$id = null)
    {

        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            $results = ['results' => ['id' => '', 'text' => '']];
            if (!is_null($q)) {

                $q="'%".strtolower(addslashes($q))."%'";

                $sql="SELECT id_maestro as id, CONCAT(documento_numero,' | ',trim(primer_apellido),' ',trim(segundo_apellido),' ',trim(primer_nombre),' ',trim(segundo_nombre),' | ',to_char(fecha_nacimiento, 'DD/MM/YYYY'),' | ', upper(dominio.area) ) as text 
                        FROM consultasestadisticas.maestro LEFT JOIN dominio ON maestro.id_area=dominio.id
                        WHERE (documento_numero LIKE $q)
                        OR (lower(CONCAT(trim(primer_apellido),' ',trim(segundo_apellido),' ',trim(primer_nombre),' ',trim(segundo_nombre))) LIKE $q)
                        OR (to_char(fecha_nacimiento,'DD/MM/YYYY') LIKE $q)
                        ORDER BY CONCAT(trim(primer_apellido),' ',trim(segundo_apellido),' ',trim(primer_nombre),' ',trim(segundo_nombre))
                        LIMIT 50";


                $query= Yii::$app->db->createCommand($sql);
                $data = $query->queryAll();
      
                $results['results'] = array_values($data);
            }
            elseif ($id > 0) {
                $results['results'] = ['id' => $id, 'text' => Maestro::findOne($id)->nombre_completo];
            }
            return $results;


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionFederador($id)
    {

        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeViewInfo($model);

        $return = $this->consulta_federador($id);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Paciente #".$id,
                'content'=>$this->renderAjax('_federador', [
                    'model' => $model,
                    'attributes' => $attributes,
                    'return' => $return,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
            ];
    
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionIps($id)
    {

        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeViewInfo($model);

        $return = $this->consulta_ips($id);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Paciente #".$id,
                'content'=>$this->renderAjax('_ips', [
                    'model' => $model,
                    'attributes' => $attributes,
                    'return' => $return,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
            ];
    
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionRenaper($id)
    {

        $request = Yii::$app->request;

        $model=$this->findModel($id);
        $return = $this->consulta_renaper($id);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Paciente #".$id,
                'content'=>$this->renderAjax('_renaper', [
                    'return' => $return,
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
            ];
    
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionDiferencia($id)
    {

        $request = Yii::$app->request;
        $model=$this->findModel($id);

        if(!$request->isAjax){
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
        // busco en la base del hospital los datos y luego los comparo
        // no tengo el modelo pero tengo la conexion, hago un query directo

        switch($model->id_area)
        {
            case 100:
                $dbhospital=\Yii::$app->get('db_hospital_viedma');
                break;
			case 200:
                $dbhospital=\Yii::$app->get('db_hospital_bariloche');
                break;
			case 300:
                $dbhospital=\Yii::$app->get('db_hospital_roca');
                break;
            case 400:
                $dbhospital=\Yii::$app->get('db_hospital_cipolletti');
                break;
        }
        
        $modificado = $dbhospital->createCommand(
            'SELECT empenumero,empeapelli,empenombre,empefecnac,empesexo,embajcodig,empefecbaj
             FROM emperson
             WHERE empenumero='.$model->id_federado_provincial)
            ->queryOne();
        
        $datosmodificados=array();

        if ($modificado) {
            // datos nuevos: empenumero,empeapelli,empenombre,empefecnac,empesexo,empebajcodig,empefecbaj
            if (strpos(trim($modificado["empeapelli"]), ' ')){
                $datosmodificados['primer_apellido'] = trim(substr(trim($modificado["empeapelli"]), 0, strpos(trim($modificado["empeapelli"]), ' ')));
                $datosmodificados['segundo_apellido'] = trim(substr(trim($modificado["empeapelli"]), strpos(trim($modificado["empeapelli"]), ' ')));
            }else{
                $datosmodificados['primer_apellido'] = trim($modificado["empeapelli"]);
                $datosmodificados['segundo_apellido'] = "";
            }
            if (strpos(trim($modificado["empenombre"]), ' ')){
                $datosmodificados['primer_nombre'] = trim(substr(trim($modificado["empenombre"]), 0, strpos(trim($modificado["empenombre"]), ' ')));
                $datosmodificados['segundo_nombre'] = trim(substr(trim($modificado["empenombre"]), strpos(trim($modificado["empenombre"]), ' ')));
            }else{
                $datosmodificados['primer_nombre'] = trim($modificado["empenombre"]);
                $datosmodificados['segundo_nombre'] = "";
            }
            $datosmodificados['fecha_nacimiento'] = (!($modificado["empefecnac"]) ? "null" : 
                Metodos::dateConvert($modificado["empefecnac"],'View')
             );
            $datosmodificados['sexo'] = trim($this->mapeo_sexo($modificado["empesexo"]));
            
            // documento
            $documento = $dbhospital->createCommand(
                'SELECT emdctdcodi,emdcnumero
                FROM emdocume
                WHERE empenumero='.$model->id_federado_provincial.'
                LIMIT 1')
                ->queryOne();
            
            $datosmodificados['documento_tipo'] = ($documento) ? trim($this->mapeo_documento($documento["emdctdcodi"])) : "";
            $datosmodificados['documento_numero'] = ($documento) ? trim($documento["emdcnumero"]) : "";

            $estado='Activo';
            if ($modificado["embajcodig"]==1)
                $estado='Unificado';
            if ($modificado["embajcodig"]==2)
                $estado='Fallecido';
    
            $datosmodificados['estado']=$estado;

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Diferencias Paciente #".$id,
                'content'=>$this->renderAjax('_diferencia', [
                    'datosmodificados' => $datosmodificados,
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
                
                ];
    
        }else{

            return [
                'title'=> "Diferencias Paciente #".$id,
                'content'=>'DATOS NO ENCONTRADOS EN BASE HOSPITAL',
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
                
                ];
    
        }

    }

    public static function mapeo_documento($tipo_documento)
    {
        $ret = "dni"; // default
        switch ($tipo_documento) {
            case "DNI":
                $ret = "dni";
                break;
            case "CI":
                $ret = "ci";
                break;
            case "LE":
                $ret = "le";
                break;
            case "OTR":
                $ret = "otr";
                break;
            case "PAS":
                $ret = "pas";
                break;
            case "LC":
                $ret = "lc";
                break;
            case "IN":
                $ret = "in";
                break;
        }
        return $ret;
    }

    public static function mapeo_sexo($sexo_hospital)
    {

        $ret = "";    // default
        if ($sexo_hospital == "F") {
            $ret = "female";
        }
        if ($sexo_hospital == "M") {
            $ret = "male";
        }
        return $ret;
    }

    public static function consulta_renaper($id)
    {

        $model=Maestro::findOne($id);
        $dominioModel = Dominio::findOne($model->id_area);

        // URLs
        $urllogin = 'https://federador.msal.gob.ar/masterfile-federacion-service/api/usuarios/aplicacion/login';
        $urlrenaper = 'https://federador.msal.gob.ar/masterfile-federacion-service/api/personas/renaper'; //?nroDocumento=28037737&idSexo=2

        // datos de la tabla maestro y dominio
        $sexo = (trim($model->sexo)=='female')?'1':'2';
        $documento = trim($model->documento_numero);
        $nombre=$dominioModel->nombre_renaper;
        $clave=$dominioModel->clave_renaper;
        $codDominio=$dominioModel->coddominio_renaper;

        $urlrenaper = $urlrenaper.'?nroDocumento='.$documento.'&idSexo='.$sexo;

        // armo post en forma de array para login
        $post_array = array(
            "nombre" => $nombre,
            "clave" => $clave,
            "codDominio"=>$codDominio
        );

        // ejecuto la consulta al login para obtener token, con JSON POST.  Respuesta: Response y status
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urllogin);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_array, JSON_UNESCAPED_SLASHES));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($status==200){

            $response_array = json_decode($response, true);
            $token=$response_array['token'];

            // ejecuto la consulta al GET a renaper
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlrenaper);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('token: '.$token,'codDominio: '.$codDominio));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            $response = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if (!( $response===false and $status===false )) {
                if ($status == 200) {      // tengo respuesta
                    $return = json_decode($response, true);
                } else {
                    $return = "Datos no encontrados";
                }
            }else{
                $return="Error de Comunicacion";
            }

        }else{
            $return="Error de Comunicacion";
        }

        return $return;

    }

    public static function consulta_federador($id)
    {

        $model=Maestro::findOne($id);
        $modelDominio=Dominio::findOne($model->id_area);

        // URLs
        $urlbase = 'https://federador.msal.gob.ar/masterfile-federacion-service/fhir/Patient/$match';
        $urlrenaper = 'http://www.renaper.gob.ar';
        $urllogin= 'https://federador.msal.gob.ar/bus-auth/auth';

        // datos de la tabla maestro
        $primer_apellido = trim($model->primer_apellido);
        $segundo_apellido = trim($model->segundo_apellido);
        $primer_nombre = trim($model->primer_nombre);
        $segundo_nombre = trim($model->segundo_nombre);
        $fecha_nacimiento = Metodos::dateConvert($model->fecha_nacimiento,'toSql');
        $sexo = trim($model->sexo);
        $documento_tipo = trim($model->documento_tipo);
        $documento_numero = trim($model->documento_numero);

        // Acomodo nombres y apellidos
        $textNombre=$primer_nombre;
        $arrayNombre=array($primer_nombre);
        $textApellido=$primer_apellido;
        $arrayApellido=array(array( "url" => "http://hl7.org/fhir/StructureDefinition/humanname-fathers-family",
                                    "valueString" => $primer_apellido ));
        if ($segundo_nombre){
            $textNombre=$primer_nombre." ".$segundo_nombre;
            $arrayNombre=array($primer_nombre, $segundo_nombre);
        }
        if ($segundo_apellido){
            $textApellido=$primer_apellido." ".$segundo_apellido;
            $arrayApellido=array(array( "url" => "http://hl7.org/fhir/StructureDefinition/humanname-fathers-family",
                                        "valueString" => $primer_apellido ),
                                  array( "url" => "http://hl7.org/fhir/StructureDefinition/humanname-mothers-family",
                                        "valueString" => $segundo_apellido ));
        }

        // armo post en forma de array
        $post_array = array(
            "resourceType" => "Parameters",
            "id" => "0",
            "parameter" => array(
                array(
                    "name" => "resource",
                    "resource" =>
                        array(
                            "resourceType" => "Patient",
                            "identifier" => array(
                                array(
                                    "use"=>"usual",
                                    "system" => $urlrenaper . "/" . $documento_tipo,
                                    "value" => $documento_numero
                                )
                            ),
                            "name" => array(
                                array(
                                    "use" => "official",
                                    "text" => $textNombre . " " . $textApellido,
                                    "family" => $textApellido,
                                    "_family" => array(
                                        "extension" => $arrayApellido,
                                    ),
                                    "given" => $arrayNombre,
                                )
                            ),
                            "birthDate" => $fecha_nacimiento,
                            "gender" => $sexo
                        )
                ),
                array(
                    "name" => "count",
                    "valueInteger" => "5"
                )
            )
        );

        // obtengo autorizacion TOKEN, con JSON POST y jwt.  Respuesta: Response y status
        $token = array(
            "iss" => trim($modelDominio->dominio),
            "sub" => trim($modelDominio->sub_federador),
            "aud" => $urllogin,
            "iat" => time(),
            "exp" => time()+300,
            "name" => trim($modelDominio->name_federador),
            "role" => trim($modelDominio->role_federador),
            "ident" => trim($modelDominio->ident_federador)
        );

        $jwt = JWT::encode($token, trim($modelDominio->key_federador));

        $post_jwt = array(
            "grantType"=>"client_credentials",
            "scope"=>"Patient/*.read,Patient/*.write",
            "clientAssertionType"=>"urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
            "clientAssertion"=>$jwt
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urllogin);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_jwt, JSON_UNESCAPED_SLASHES));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($status==200){

            $response_array = json_decode($response, true);
            $token = $response_array['accessToken'];


            // ejecuto la consulta al federador, con JSON POST.  Respuesta: Response y status
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlbase);

            curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json','Authorization: Bearer '.$token));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_array, JSON_UNESCAPED_SLASHES));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            $response = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // analizo response y status y preparo $return (respuesta de esta funcion) . Ver al final la info
            if (!($status==0 or $status===false)) {

                if (!($response===false)) {
                    if ($status == 200) {      // tengo respuesta ok, pero puede ser respuesta cero
                        $return = json_decode($response, true);
                        if (intval($return["total"])==0){
                            $return =  "No existen registros federados con esos datos";
                        }
                    } else {        // No hay respuesta.
                        // Error de datos,
                        $return =  "Error de datos del federador";
                    }
                }

            }else{
                $return="Error de Comunicacion";
            }

        }else{
            $return="Error de Comunicacion";
        }

        return $return;
    }

    public static function consulta_ips($id)
    {

        $model=Maestro::findOne($id);
        $modelDominio=Dominio::findOne($model->id_area);

        // URLs
        $urlbase = 'https://federador.msal.gob.ar/masterfile-federacion-service/fhir/Patient/$match';
        $urlrenaper = 'http://www.renaper.gob.ar';
        $urllogin= 'https://federador.msal.gob.ar/bus-auth/auth';
        
        $urlfhir='http://mhd.sisa.msal.gov.ar/fhir/';


//        $urlbase='http://mhd.sisa.msal.gov.ar/fhir/DocumentReference?subject:identifier=http://salud.mendoza.gov.ar|753079&custodian=http://salud.mendoza.gov.ar&type=http://loinc.org|60591-5';

    $urlbase='http://mhd.sisa.msal.gov.ar/fhir/DocumentReference?subject=https://www.hospitalitaliano.org.ar|2489290&custodian=http://dummy.com.ar&type=http://loinc.org|60591-5';

        

        // obtengo autorizacion TOKEN, con JSON POST y jwt.  Respuesta: Response y status
        $token = array(
            "iss" => trim($modelDominio->dominio),
            "sub" => trim($modelDominio->sub_federador),
            "aud" => $urllogin,
            "iat" => time(),
            "exp" => time()+300,
            "name" => trim($modelDominio->name_federador),
            "role" => trim($modelDominio->role_federador),
            "ident" => trim($modelDominio->ident_federador)
        );

        $jwt = JWT::encode($token, trim($modelDominio->key_federador));

        $post_jwt = array(
            "grantType"=>"client_credentials",
            "scope"=>"Patient/*.read,Patient/*.write",
            "clientAssertionType"=>"urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
            "clientAssertion"=>$jwt
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urllogin);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_jwt, JSON_UNESCAPED_SLASHES));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($status==200){

            $response_array = json_decode($response, true);
            $token = $response_array['accessToken'];

            // ejecuto la consulta al federador, con JSON POST.  Respuesta: Response y status
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlbase);
            curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json','Authorization: Bearer '.$token));

            curl_setopt($ch, CURLOPT_POST, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

        
            // analizo response y status y preparo $return (respuesta de esta funcion) . Ver al final la info
            if (!($status==0 or $status===false)) {

                if (!($response===false)) {
                    if ($status == 200) {      // tengo respuesta ok, pero puede ser respuesta cero

                        $datos_return = json_decode($response, true);

                        if ($datos_return['total']<=0){
                            $return =  "No existen datos en ese dominio";
                        }

                        $return = [];
                        foreach ($datos_return['entry'] as $value1){
                            foreach ($value1['resource']['content'] as $value2){
                                $return[]=$value2['attachment'];
                            }
                        }


                        $bundle=$datos_return['entry'][0]['resource']['content'][0]['attachment']['url'];

                        $urlfhir.=$bundle;
                        // ejecuto la consulta al federador, con JSON POST.  Respuesta: Response y status
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $urlfhir);
                        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json','Authorization: Bearer '.$token));

                        curl_setopt($ch, CURLOPT_POST, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($ch);
                        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        if (!($status==0 or $status===false)) {

                            if (!($response===false)) {
                                if ($status == 200) {      // tengo respuesta ok, pero puede ser respuesta cero

                                    print_r($response);
                                    die();
                                    
                                    $return = json_decode($response, true);
                                    $return = $response;

                                } else {        // No hay respuesta.
                                    // Error de datos,
                                    $return =  "Error de datos del federador";
                                }
                            }

                        }else{
                            $return="Error de Comunicacion";
                        }


                    } else {        // No hay respuesta.
                        // Error de datos,
                        $return =  "Error de datos del federador";
                    }
                }

            }else{
                $return="Error de Comunicacion";
            }

        }else{
            $return="Error de Comunicacion";
        }

        return $return;
    }

}
