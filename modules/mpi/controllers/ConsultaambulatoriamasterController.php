<?php

namespace app\modules\mpi\controllers;

use CsvDataProvider;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\data\SqlDataProvider;

use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use Mpdf\Mpdf;

//
use app\modules\mpi\models\ConsultaAmbulatoriaMaster;
use app\modules\mpi\models\ConsultaAmbulatoriaMasterSearch;
use app\modules\mpi\models\ConsultaAmbulatoriaSubjetivo;
use app\modules\mpi\models\ConsultaAmbulatoriaObjetivo;
use app\modules\mpi\models\ConsultaAmbulatoriaAnalisis;
use app\modules\mpi\models\ConsultaAmbulatoriaPlan;
use app\modules\mpi\models\Dominio;
use app\modules\mpi\models\Maestro;
use app\modules\mpi\models\MaestroSearch;
use app\modules\mpi\models\ListadoForm;
use app\modules\mpi\models\ServicioEspecialidad;
use app\modules\mpi\models\ICPC2;
use app\modules\mpi\models\SISTEMA;
use app\modules\mpi\models\CIE10;
use app\modules\mpi\models\NOMENCLADOR;
use app\modules\mpi\models\Domicilio;
use app\modules\mpi\models\Telefono;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;

use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;
use DateTime;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\FileHelper;

/**
 * ConsultaambulatoriamasterController implements the CRUD actions for ConsultaAmbulatoriaMaster model.
 */
class ConsultaambulatoriamasterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // index se renombro como area
                'only' => ['pdfplan','view','export','select','area','buscararea','buscarpaciente',
                    'paciente','totalpaciente','listdetalle','viewdetalle','exportconsulta','exportdatos',
                    'exportlistado','buscarlistado','listado','servicioespecialidad','icpc2','sistema',
                    'cie10','nomenclador','informe','exportconsultaseleccion','centro'],
                
                'rules' => [
                    [
                    'actions' => ['pdfplan','view','export','select','area','buscararea','buscarpaciente',
                        'paciente','totalpaciente','listdetalle','viewdetalle','exportconsulta','exportdatos',
                        'exportlistado','buscarlistado','listado','servicioespecialidad','icpc2','sistema',
                        'cie10','nomenclador','informe','exportconsultaseleccion','centro'],
                    'allow' => Seguridad::tienePermiso(),
                    'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function actionBuscararea()
    {    
        $model= new ConsultaAmbulatoriaMaster();
        $model->setDbId(100);  // dominio inicial para la busqueda
        if ($model->load(Yii::$app->request->post())) {

            return $this->redirect(['area', 'dominio' => $model->dominio]);
        }else{
            return $this->render('buscararea', [
                'model' => $model,
            ]);
        }
    }
    public function actionMigration($hospital){
        $query=(new \yii\db\Query())->select(["*"])->from($hospital.'.consulta_migration');
        $csv = 'nombre_completo_profesional,nombre_completo_paciente,evolucion,sexo,fecha_consulta,paciente_documento_numero,paciente_tipodocumento,especialidad';
   foreach( $query->createCommand()->queryAll() as $columna)
       $csv .= "\n".implode(',', $columna);
  /* header("Content-type: text/csv");
   header("Content-Disposition: attachment; filename=file.csv");*/
   return \Yii::$app->response->sendContentAsFile($csv, 'migration_hsi'.$hospital.'.csv', [
    'mimeType' => 'text/csv', 
    'inline'   => false
]);


    }
    public function actionReporte2($localidad){
        //hacerlo a mi forma
        $query=(new \yii\db\Query())->select(["c.codigo_sisa,extract(YEAR FROM ca.fecha) as anioconsul,extract(month FROM ca.fecha) as mesconsul,s.codigo_nacion as coduoperat,
         
        CASE
   WHEN  ca.paciente_sexo='M' and count(ca.paciente_sexo)>0 THEN count(ca.paciente_sexo)
   else
       0
 END AS masculino,
 CASE
   WHEN  ca.paciente_sexo='F' and count(ca.paciente_sexo)>0 THEN count(ca.paciente_sexo)
   else
     0
 END AS femenino,
  CASE
  WHEN EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  >= 1 and EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <= 4  THEN '1-4'
  WHEN  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  >= 5 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <= 9 THEN '5-9'
  WHEN   EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  >= 10 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <= 14  THEN '10-14'
  WHEN   EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 15 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <= 19  THEN '15-19'
  WHEN EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 20 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <=24  THEN '20-24'
  WHEN  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 25 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <=34  THEN '25-34'
  WHEN  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 35 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <=44  THEN '35-44'
  WHEN  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 45 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <=49  THEN '45-49'
  WHEN  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 50 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <=64  then '50-64'
  WHEN  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 65 and  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)  <=74  then '65-74'
  WHEN  EXTRACT('Year' FROM ca.fecha)-extract('Year' from ca.paciente_nac)   >= 75  then '>75'
  else
      'S/E'
 END AS rangoedad
   "])->from($localidad.'.consulta_ambulatoria_master ca')->join('INNER JOIN',$localidad.'.servicio_especialidad s','s.nombre_servicio=ca.servicio')
   ->join('INNER JOIN',$localidad.'.centro c','c.nombre=ca.centro')->groupBy('c.codigo_sisa,ca.fecha,ca.paciente_nac,anioconsul,mesconsul,coduoperat,ca.paciente_sexo');
   $query2=$query->createCommand()->queryAll();
 $columnas=[];
foreach($query2 as $q){  
$columna=[
    $q['codigo_sisa'], 
    $q['anioconsul'],$q['mesconsul'],$q['coduoperat'],
    $q['masculino'],$q['femenino'],$q['rangoedad'],
       ];
       array_push($columnas,$columna);
   }
   $csv = 'CODPROV,CODSISA,ANIOCONSUL,MESCONSUL,CODUOPERAT,VARONES,MUJERES,RANGOEDAD';
   foreach($columnas as $columna)
       $csv .= "\n".implode(',', $columna);
  /* header("Content-type: text/csv");
   header("Content-Disposition: attachment; filename=file.csv");*/
   return \Yii::$app->response->sendContentAsFile($csv, 'reporte.csv', [
    'mimeType' => 'text/csv', 
    'inline'   => false
]);
    }
    public function actionReporte($localidad){
        $connection = Yii::$app->db; 
        $query2=$connection->createCommand("
        WITH TEMP AS (
            SELECT  62 AS CODPROV,
                    c2.codigo_sisa  as PUESTO,
                     EXTRACT('Year' FROM c.fecha) AS ANIOCONSUL,
                    EXTRACT('Month' FROM c.fecha) AS MESCONSUL,
                    FLOOR(EXTRACT('Year' FROM c.fecha)-extract('Year' from c.paciente_nac))  AS EDAD,
                    s.codigo  AS CODUOPERAT,
                    /*COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                    COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES,*/
                    c.paciente_sexo  as CODSEXO
            FROM ".$localidad.".consulta_ambulatoria_master c inner 
            join (select distinct nombre_servicio
            from ".$localidad.".servicio_especialidad) se on (c.servicio  = se.nombre_servicio)
            join ".$localidad.".servicio_auxiliar sa on (sa.nombre=nombre_servicio)
            join consultasestadisticas.p_servicio s on (s.id=sa.id_servicio)

             JOIN ".$localidad.".centro c2 on (UPPER(c.centro) = UPPER(c2.nombre))
            )
        SELECT  CODPROV,  PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                'menor a 1' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD < 1.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '1 a 4' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 1.0 AND EDAD < 5.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '5 a 9' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 5.0 AND EDAD < 10.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '10 a 14' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 10.0 AND EDAD < 15.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '15 a 19' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 15.0 AND EDAD < 20.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '20 a 24' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 20.0 AND EDAD < 25.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '25 a 34' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 25.0 AND EDAD < 35.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '35 a 44' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 35.0 AND EDAD < 45.0
        GROUP BY CODPROV, PUESTO,ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '45 a 49' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 45.0 AND EDAD < 50.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '50 a 64' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 50.0 AND EDAD < 65.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                '65 a 74' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 65.0 AND EDAD < 75.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                'mayor a 75' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD >= 75.0
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
        UNION
        SELECT  CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT,
                'sin especificar' AS RANGOEDAD,
                COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
                COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES
        FROM TEMP
        WHERE EDAD IS NULL
        GROUP BY CODPROV, PUESTO, ANIOCONSUL, MESCONSUL, CODUOPERAT
 ")->queryAll();
 $columnas=[];
foreach($query2 as $q){
$columna=[
    $q['codprov'],
    $q['puesto'],$q['anioconsul'],
    $q['mesconsul'],$q['coduoperat'],$q['varones'],$q['mujeres'],
    $q['rangoedad']
       ];
       array_push($columnas,$columna);
   }
   $csv = 'CODPROV,CODIGO SISA,ANIOCONSUL,MESCONSUL,CODIGO NACION,VARONES,MUJERES,RANGOEDAD';
 
   foreach($columnas as $columna)
       $csv .= "\n".implode(',', $columna);
  /* header("Content-type: text/csv");
   header("Content-Disposition: attachment; filename=file.csv");*/
   return \Yii::$app->response->sendContentAsFile($csv, 'reporte'.$localidad.'.csv', [
    'mimeType' => 'text/csv', 
    'inline'   => false
]);
    }


    public function actionExportodonto($accion)
    {
        $request = Yii::$app->request;
        $reporte = new Reporte();
        
        // Procesar la exportación cuando el formulario ha sido enviado
        if (isset($_POST['Reporte']) && isset($_POST['tipo_archivo'])) {
            $reporte->attributes = $_POST['Reporte'];
            $tipo_archivo = $_POST['tipo_archivo'];
            
            \app\components\Listado\Listado::widget([
                'reporte' => $reporte,
                'tipo' => $tipo_archivo,
            ]);
    
            $fileName = preg_replace("/ /", "_", $reporte->titulo) . "." . $tipo_archivo;
            Yii::$app->response->sendFile('../runtime/' . $fileName)->send();
            Yii::$app->end();
        }
    
        // Manejar las solicitudes AJAX
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
    
            // Recuperar el DataProvider de la sesión
            $dataProvider = Yii::$app->session->get('odontologiaDataProvider');
            
            // Verificar si el DataProvider es válido
            if (!$dataProvider instanceof \yii\data\ActiveDataProvider) {
                throw new \yii\web\ServerErrorHttpException('No se pudo recuperar el DataProvider desde la sesión.');
            }
    
            // Configurar el reporte usando el DataProvider
            $reporte->setFromDataProviderNotClass($dataProvider, $accion);
    
            return [
                'title' => "Exportar Datos",
                'content' => $this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,
                    'tipo_archivo' => 'pdf',
                    'totalRegistros' => $dataProvider->totalCount,
                ]),
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Aceptar', ['export', 'accion' => $accion], [
                        'class' => 'btn btn-primary',
                        'target' => 'exportFrame',
                        'onclick' => 'document.getElementById("export-form").submit();',
                    ]) .
                    '<iframe src="" style="display:none" name="exportFrame"/>',
            ];
        } else {
            // Manejar solicitudes normales (no AJAX)
            return $this->redirect(['index']);
        }
    }
    
    private function fechaInicioFechaFin($indice = null, $tipo = null, $anio = null) {
        // Si no hay índice definido, usar el mes actual
        if ($indice === null) {
            $indice = date('n') - 1; // Convertir de 1-12 a 0-11
        }
    
        // Si no se define el tipo, se considera el año
        if ($tipo === null) {
            $tipo = "anio";
        }
    
        // Si no se define el año, usar el año actual
        if ($anio === null) {
            $anio = date("Y");
        }
    
        // Ajustar el índice si se necesita sumar o restar meses
             // Ajustar el índice y el año de forma inmediata según el tipo
             if ($tipo == "restar") {
                $indice--;
                if ($indice < 0) {
                    $indice = 11; // Cambia a diciembre
                      // Retrocede un año
                }
            } elseif ($tipo == "sumar") {
                $indice++;
                if ($indice > 11) {
                    $indice = 0;  // Cambia a enero
                    // Avanza un año
                }
            }
    
        $indiceMes = $this->convertFecha($indice);
    
        // Crear las fechas de inicio y fin del mes usando DateTime
        $fechaInicio = (new DateTime("$anio-$indiceMes-01"))->format('Y-m-d');
        $fechaFin = (new DateTime("$anio-$indiceMes-01"))->modify('last day of this month')->format('Y-m-d');
    
        return [
            'indice' => $indice,
            'tipo' => $tipo,
            'anio' => $anio,
            'fechaInicio' => $fechaInicio,
            'fechaFin' => $fechaFin
        ];
    }
    
    public function actionOdontologia($indice = null, $tipo = null, $anio = null) {
        // Definir los meses con índices correctos (de 1 a 12)
        $mesesTrimestre = array_map(function ($mes, $nombre) {
            return [$mes, $nombre];
        }, range(0, 11), [
            "enero", "febrero", "marzo", "abril", "mayo", "junio",
            "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
        ]);
        

        
    
        $fechas = $this->fechaInicioFechaFin($indice, $tipo, $anio, $mesesTrimestre);
        
        // Definir las columnas a seleccionar
        $selectColumns = [
            'profesional_nombre',
            'profesional_apellido',
            'paciente_nombre',
            'paciente_apellido',
            'descripcion_diagnostico',
            'fecha_turno',
            'diagnostico_codigo',
            'descripcion_especialidad',
            'centro_nombre',
            'codigo',
        ];
        
        // Crear la consulta
        $query = (new Query())
            ->select($selectColumns)
            ->from('hospitalroca.odontologia')
            ->andFilterWhere(['between', 'fecha_turno',$fechas["fechaInicio"], $fechas["fechaFin"]]);
        
        // Crear el DataProvider
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20, // Ajusta el tamaño de página aquí según sea necesario
            ],
        ]);
        
        Yii::$app->session->set('odontologiaDataProvider', $dataProvider);
        
        // Definir las columnas para la vista de forma más compacta
        $columnas = array_map(function ($attr, $label) {
            return [
                'attribute' => $attr,
                'label' => $label,
            ];
        }, $selectColumns, [
            'Fecha Turno',
            'Nombre Profesional',
            'Apellido Profesional',
            'Nombre Paciente',
            'Apellido Paciente',
            'Descripción Diagnóstico',
            'Código Diagnóstico',
            'Descripción Especialidad',
            'Nombre Centro',
            'Código',
        ]);
        
        // Obtener permisos
        $permisos = ['export' => Seguridad::tienePermiso('export')];
        
        // Renderizar la vista
        return $this->render('odontologia', [
            'dataProvider' => $dataProvider,
            'columns' => $columnas,
            'permisos' => $permisos,
            'indiceMatriz' => 1,
            'trimestres' => $mesesTrimestre,
            'indice' => $fechas['indice'],
            'anio' => $fechas['anio'],
        ]);
    }


    public function convertFecha($indice){
        $fechaArrays = $this->obtenerFechas();
        return $fechaArrays[$indice][0] ?? null; // Devuelve el mes o null si no existe
    }
    public function actionConvertfechanombre($indice){

        $fechaArrays = $this->obtenerFechas();
        return $fechaArrays[$indice][1] ?? null; // Devuelve el mes o null si no existe
    }

    public function obtenerFechas(){
       return [
            0 => [1,"enero"],
            1 => [2,"febrero"],
            2 => [3,"marzo"],
            3 => [4,"abril"],
            4 => [5,"mayo"],
            5 => [6,"junio"],
            6 => [7,"julio"],
            7 => [8, "agosto"],
            8 => [9,"septiembre"],
            9 => [10,"octubre"],
            10 => [11,"noviembre"],
            11 => [12,"diciembre"]
        ];
    }

public function actionDownloadodontologia($mesNombre){
    $fechas=$this->obtenerFechas();
    foreach ($fechas as $indice => $mes) {
        if (strtolower($mes[1]) === strtolower($mesNombre)) {
            $fechas=$this->fechaInicioFechaFin($indice,"anio",2024);

        }
    }


    $fechaInicio = date('Y-m-d', strtotime($fechas["fechaInicio"]));
    $fechaFin = date('Y-m-d', strtotime($fechas["fechaFin"]));

    // Obtiene la conexión a la base de datos
    $connection = Yii::$app->db;

    // Prepara la consulta SQL
    $sql = "
        SELECT 
            profesional_nombre,
            profesional_apellido,
            paciente_nombre,
            paciente_apellido,
            descripcion_diagnostico,
            fecha_turno,
            diagnostico_codigo,
            descripcion_especialidad,
            centro_nombre,
            codigo
        FROM hospitalroca.odontologia
           WHERE fecha_turno BETWEEN :fechaInicio AND :fechaFin";

    $command = $connection->createCommand($sql, [
        ':fechaInicio' => $fechaInicio,
        ':fechaFin' => $fechaFin,
    ]);

    // Nombre del archivo CSV
    $filename = "odontologia_{$fechaInicio}_{$fechaFin}.csv";

    // Configuración de cabeceras para la descarga
    header('Content-Type: text/csv; charset=UTF-8');
    header('Content-Disposition: attachment; filename="' . $filename . '"');

    // Crear un archivo CSV en memoria
    $output = fopen('php://output', 'w');

    // Escribir las cabeceras del CSV
    $fields = [
        'profesional_nombre', 'profesional_apellido', 'paciente_nombre', 'paciente_apellido',
        'descripcion_diagnostico', 'fecha_turno', 'diagnostico_codigo', 'descripcion_especialidad',
        'centro_nombre', 'codigo'
    ];
    fputcsv($output, $fields);

    // Escribir los datos en el CSV
    $reader = $command->query();
    while (($row = $reader->read()) !== false) {
        fputcsv($output, [
            $row['profesional_nombre'], $row['profesional_apellido'], $row['paciente_nombre'],
            $row['paciente_apellido'], $row['descripcion_diagnostico'], $row['fecha_turno'],
            $row['diagnostico_codigo'], $row['descripcion_especialidad'], $row['centro_nombre'],
            $row['codigo']
        ]);
    }

    fclose($output);

    // Finaliza el script para evitar salida extra
    Yii::$app->end();

    }

    
    public function actionArea($dominio=null)
    {    
        if ($dominio){
            $model= new ConsultaAmbulatoriaMaster();
            $model->setDbId($dominio);
            $searchModel = new ConsultaAmbulatoriaMasterSearch();
            $searchModel->setDbId($dominio);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);            
            $columnas=Metodos::obtenerColumnas($model,'mpi/consultaambulatoriamaster/area');
                $permisos=[ 'area'=>Seguridad::tienePermiso('area'),
                'viewdetalle'=>Seguridad::tienePermiso('viewdetalle'),
                'pdfplan'=>Seguridad::tienePermiso('pdfplan'),
                'exportconsulta'=>Seguridad::tienePermiso('exportconsulta'),
                'export'=>Seguridad::tienePermiso('export'),
                'select'=>Seguridad::tienePermiso('select'),
            ];
            return $this->render('area', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'columns'=>$columnas,
                'permisos'=>$permisos,
                'dominio'=>$dominio,
            ]);
        }
    }

    public function actionBuscarpaciente()
    {
        $modelBuscar = new Maestro();
        $respuesta=Yii::$app->request->post();
        if ($respuesta) {
            if ($respuesta['Maestro']['documento_tipo']>=0 and $respuesta['Maestro']['documento_numero'] and $respuesta['Maestro']['id_area']>=0 ){
                $data=['dni','lc','le','pas','ci','in','otr']; // igual al del form
                $documento_tipo=$data[$respuesta['Maestro']['documento_tipo']];                
                $model= Maestro::findOne([
                    'id_area'=>$respuesta['Maestro']['id_area'],
                    'documento_tipo'=>$documento_tipo,
                    'documento_numero'=>trim($respuesta['Maestro']['documento_numero'])
                    ]);

            }else if ($respuesta['Maestro']['id_maestro']){
                $model= Maestro::findOne($respuesta['Maestro']['id_maestro']);
            }else{
                $model=null;
            }
            if ($model){
                return $this->redirect(['paciente', 
                    'id_maestro'=>$model->id_maestro                    
                ]);
            }
        }
        return $this->render('buscarpaciente', [
            'model' => $modelBuscar,
        ]);
    }

 
    public function actionPaciente($id_maestro = null)
{
    $model = Maestro::findOne($id_maestro);

    if ($model) {
        // Get all models depending on the federado flag
        $modelos = $model->flag_federado ? Maestro::findAll([
            'documento_tipo' => $model->documento_tipo,
            'documento_numero' => trim($model->documento_numero),
            'flag_federado' => $model->flag_federado
        ]) : [$model];

        // Initialize arrays for domicilios and telefonos
        $ids_federados = ArrayHelper::getColumn($modelos, 'id_federado_provincial');

        // Prepare to collect domicilios and telefonos
        $domicilios = [];
        $telefonos = [];

        foreach ($modelos as $model_aux) {
            $dominio = $model_aux->id_area;

            // Set the database context for Domicilio
            Domicilio::setDbId($dominio);
            $domicilio = Domicilio::find()
                ->where(['paciente_id' => $model_aux->id_federado_provincial])
                ->one();
            $domicilios[$model_aux->id_federado_provincial] = $domicilio ? $domicilio->domicilio_completo : '';

            // Set the database context for Telefono
            Telefono::setDbId($dominio);
            $telefono = Telefono::find()
                ->where(['paciente_id' => $model_aux->id_federado_provincial])
                ->one();
            $telefonos[$model_aux->id_federado_provincial] = $telefono ? $telefono->telefono_completo : '';
        }

        // Prepare datos
        $datos = [];
        foreach ($modelos as $model_aux) {
            $id_federado = $model_aux->id_federado_provincial;
            $datos[] = [
                'nombre' => $model_aux->dominio->area,
                'hc' => $model_aux->numero_hc,
                'domicilio' => $domicilios[$id_federado],
                'telefono' => $telefonos[$id_federado]
            ];
        }

        // Consultas RENAPER y federador
        $json_renaper = MaestroController::consulta_renaper($model->id_maestro);
        $json_federador = MaestroController::consulta_federador($model->id_maestro);
        //$json_renaper = null;
        //$json_federador=null;
        // Retrieve all consultas in a single query
        $data = [];
        foreach ($modelos as $model_aux) {
            $dominio = $model_aux->id_area;
            ConsultaAmbulatoriaMaster::setDbId($dominio);
            $consultas_all = ConsultaAmbulatoriaMaster::find()
                ->where(['paciente_id' => $model_aux->id_federado_provincial])
                ->limit(100)
                ->all();

            foreach ($consultas_all as $consulta_aux) {
                $dato_aux = ArrayHelper::toArray($consulta_aux);
                $dato_aux['fecha_hora'] = $consulta_aux->fecha . ' (' . substr(trim($consulta_aux->id2), -8) . ')';
                $dato_aux['profesional'] = trim($consulta_aux->medico_ape) . ', ' . $consulta_aux->medico_nom;
                $data[] = $dato_aux;
            }
        }

        // Prepare data provider for the grid
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => 'id2',
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'attributes' => ['id2', 'area', 'centro', 'servicio', 'especialidad', 'profesional', 'cie10_codigo'],
                'defaultOrder' => ['id2' => SORT_ASC],
            ],
        ]);

        $columns = [
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'id2', 'value' => 'fecha_hora'],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'area'],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'centro'],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'servicio'],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'especialidad'],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'profesional'],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'cie10_codigo'],
        ];

        return $this->render('paciente', [
            'model' => $model,
            'datos' => $datos,
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'json_federador' => $json_federador,
            'json_renaper' => $json_renaper,
        ]);
    } else {
        return $this->redirect(['buscarpaciente']);
    }
}

    

    /**
     * Displays a single ConsultaAmbulatoriaMaster model.
     * @param integer $id1
     * @param string $id2
     * @return mixed
     */
    public function actionView(array $id,$dominio)
    {   
        $id1=$id['id1'];
        $id2=$id['id2'];
        $request = Yii::$app->request;
        $model= new ConsultaAmbulatoriaMaster();
        $model->setDbId($dominio);
        $model = ConsultaAmbulatoriaMaster::findOne(['id1' => $id1, 'id2' => $id2]);
        $attributes = $model->attributeView($model);
        if(isset($_GET['pdf'])){
            $titulo='Datos de Consulta Ambulatoria Master';
            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id1.'|'.$id2,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));
            $request = Yii::$app->request;
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Consulta Ambulatoria Master #".$id1.'|'.$id2,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $this->findModel($id1,$id2),
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the ConsultaAmbulatoriaMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id1
     * @param string $id2
     * @return ConsultaAmbulatoriaMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id1, $id2)
    {
        if (($model = ConsultaAmbulatoriaMaster::findOne(['id1' => $id1, 'id2' => $id2])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */

    public function actionExport($accion=null,$sqlBase=null,$param=null,$dominio=null)
    {
        $request = Yii::$app->request;
        $reporte= new Reporte();
        // para recuperar el dominio y seterlo en el modelo
        $model= new ConsultaAmbulatoriaMaster();
        $model->setDbId($dominio);
        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $dataProvider=Yii::$app->session->get('consultaambulatoriamaster-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);
            $reporte->titulo='Listado de Consultas Ambulatorias';
            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
                ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */

    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new ConsultaAmbulatoriaMaster();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];
                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }
                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }
                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);
                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }
            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);
            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionTotalpaciente()
    {
        $searchModel = new MaestroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columnas=[
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'id_area',
            'value'=>'dominio.area',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'numero_hc',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'documento_tipo',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'documento_numero',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'primer_apellido',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'segundo_apellido',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'primer_nombre',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'segundo_nombre',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'fecha_nacimiento',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'sexo',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'estado',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'flag_federado',
            'format'=>'boolean',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'id_federado_provincial',
        ],
        ];
        $permisos=[ 'view'=>Seguridad::tienePermiso('mpi/maestro/view'),
                    'info'=>Seguridad::tienePermiso('mpi/maestro/info'),
                    'listdetalle' => Seguridad::tienePermiso('listdetalle'),
                    'viewdetalle' => Seguridad::tienePermiso('viewdetalle'),
                    'exportconsulta' => Seguridad::tienePermiso('exportconsulta'),
        ];
        return $this->render('totalpaciente', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);

    }

    public function actionListdetalle($id_maestro_maestro)
    {

        if (isset($id_maestro_maestro)) {
            $modelPaciente = Maestro::findOne($id_maestro_maestro);
            $dominio = $modelPaciente->id_area;
            $paciente_id=$modelPaciente->id_federado_provincial;
            $model= new ConsultaAmbulatoriaMaster();
            $model->setDbId($dominio);
            $searchModel = new ConsultaAmbulatoriaMasterSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where(['paciente_id' => $paciente_id]);
            $dataProvider->setPagination(false);
            $dataProvider->setSort(false);
            return $this->renderPartial('_listDetalle', [
                'id_maestro' => $id_maestro_maestro,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return '<div>No se encontraron resultados</div>';
        }
    }

    public function actionViewdetalle(array $id,$dominio)
    {
        // este es el detalle de la consulta con todas las vistas
        $id1=$id['id1'];
        $id2=$id['id2'];
        $request = Yii::$app->request;
        $model= new ConsultaAmbulatoriaMaster();
        $model->setDbId($dominio);
        $model = $model->findOne(['id1' => $id1, 'id2' => $id2]);
        $model_subjetivo= new ConsultaAmbulatoriaSubjetivo();
        $model_subjetivo->setDbId($dominio);
        $model_subjetivo=ConsultaAmbulatoriaSubjetivo::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        $model_objetivo= new ConsultaAmbulatoriaObjetivo();
        $model_objetivo->setDbId($dominio);
        $model_objetivo=ConsultaAmbulatoriaObjetivo::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        $model_analisis= new ConsultaAmbulatoriaAnalisis();
        $model_analisis->setDbId($dominio);
        $model_analisis=ConsultaAmbulatoriaAnalisis::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        $model_plan= new ConsultaAmbulatoriaPlan();
        $model_plan->setDbId($dominio);
        $model_plan=ConsultaAmbulatoriaPlan::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Consulta Ambulatoria SOAP",
                'content'=>$this->renderAjax('_viewDetalle', [
                    'model_subjetivo' => $model_subjetivo,
                    'model_objetivo' => $model_objetivo,
                    'model_analisis' => $model_analisis,
                    'model_plan' => $model_plan,
                    'model'=>$model,
                    'dominio'=>$dominio,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionExportconsulta(array $id,$dominio)
    {
        // este es el detalle de la consulta con todas las vistas
        $id1=$id['id1'];
        $id2=$id['id2'];
        $model= new ConsultaAmbulatoriaMaster();
        $model->setDbId($dominio);
        $model = ConsultaAmbulatoriaMaster::findOne(['id1' => $id1, 'id2' => $id2]);
        $model_subjetivo= new ConsultaAmbulatoriaSubjetivo();
        $model_subjetivo->setDbId($dominio);
        $model_subjetivo=ConsultaAmbulatoriaSubjetivo::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        $model_objetivo= new ConsultaAmbulatoriaObjetivo();
        $model_objetivo->setDbId($dominio);
        $model_objetivo=ConsultaAmbulatoriaObjetivo::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        $model_analisis= new ConsultaAmbulatoriaAnalisis();
        $model_analisis->setDbId($dominio);
        $model_analisis=ConsultaAmbulatoriaAnalisis::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        $model_plan= new ConsultaAmbulatoriaPlan();
        $model_plan->setDbId($dominio);
        $model_plan=ConsultaAmbulatoriaPlan::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
        $html=$this->viewConsultaHtml($model,$model_subjetivo,$model_objetivo,$model_analisis,$model_plan);
        $titulo='Consulta Ambulatoria';
        \app\components\Htmlpdf\Htmlpdf::widget(array(
            'titulo'=>$titulo,
            'subtitulo'=>$model->fecha.' ('.substr(trim($model->id2),-8).')',
            'data'=>$html,
        ));
        $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
        Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
        Yii::$app->end();
    }
    public function actionExportconsultaseleccion()
    {
        if (isset($_POST['keylist'])) {
            // objeto PDF
            // mPDF($mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P')
            $parametrosPDF=['mode'=>'utf-8','format'=>'A4','margin_right'=>15,
            'margin_left'=>15,'margin_top'=>30,'margin_bottom'=>15,'margin_header'=>9,'margin_footer'=>9,'orientation'=>'P'];
            $objPDF = new mPDF($parametrosPDF);
            $cantidad = count($_POST['keylist']);
            foreach ($_POST['keylist'] as $key=>$value) {
                $datos=json_decode($value);
                $id1=$datos->id1;
                $id2=$datos->id2;
                $dominio=$datos->dominio;
                // consutruccion del HTML de cada hoja 
                $model= new ConsultaAmbulatoriaMaster();
                $model->setDbId($dominio);
                $model = ConsultaAmbulatoriaMaster::findOne(['id1' => $id1, 'id2' => $id2]);
                $model_subjetivo= new ConsultaAmbulatoriaSubjetivo();
                $model_subjetivo->setDbId($dominio);
                $model_subjetivo=ConsultaAmbulatoriaSubjetivo::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
                $model_objetivo= new ConsultaAmbulatoriaObjetivo();
                $model_objetivo->setDbId($dominio);
                $model_objetivo=ConsultaAmbulatoriaObjetivo::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
                $model_analisis= new ConsultaAmbulatoriaAnalisis();
                $model_analisis->setDbId($dominio);
                $model_analisis=ConsultaAmbulatoriaAnalisis::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
                $model_plan= new ConsultaAmbulatoriaPlan();
                $model_plan->setDbId($dominio);
                $model_plan=ConsultaAmbulatoriaPlan::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
                // titulo y subtitulo
                $html=Metodos::CabeceraHtmlPdf('Consulta Ambulatoria',$model->fecha.' ('.substr(trim($model->id2),-8).')');
                $html.=$this->viewConsultaHtml($model,$model_subjetivo,$model_objetivo,$model_analisis,$model_plan);
                $objPDF->WriteHTML($html);
                // Si es el ultimo registro , no agrego pagina
                if ($key < $cantidad-1 ){
                    $objPDF->AddPage();
                }
                $html="";                
            }
            // el objeto pdf se guarda en un archivo en la carpeta assets
            $titulo='Consultas Ambulatorias '.trim($model->paciente_numerodoc);
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            $objPDF->Output('assets/'.$fileName,"F");
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['status' => 'success',
                'file' => 'assets/'.$fileName,
            ];
            Yii::$app->end();
        }
    }

    public function actionExportdatos($id)
    {
        $model = Maestro::findOne($id);
        if ($model) {
            // Verifico si existe el paciente dentro del mpi en otra área
            // La búsqueda de pacientes en dos áreas se hace a través del federador
            // Si no está federado, el array modelos tiene un solo elemento.
            $areas = [];
            if ($model->flag_federado) {
                $modelos = Maestro::findAll([
                    'documento_tipo' => $model->documento_tipo,
                    'documento_numero' => trim($model->documento_numero),
                    'flag_federado' => $model->flag_federado
                ]);
            } else {
                $modelos[0] = $model;
            }
    
            foreach ($modelos as $dato) {
                $dominio = $dato->id_area;
    
                // Domicilio
                $modelDomicilio = new Domicilio();
                $modelDomicilio->setDbId($dominio);
                $modelDomicilio = $modelDomicilio->findOne($dato->id_federado_provincial);
                $domicilio = $modelDomicilio ? $modelDomicilio->domicilio_completo : '';
    
                // Teléfono
                $modelTelefono = new Telefono();
                $modelTelefono->setDbId($dominio);
                $modelTelefono = $modelTelefono->findOne($dato->id_federado_provincial);
                $telefono = $modelTelefono ? $modelTelefono->telefono_completo : '';
    
                // Agregar los datos al array de áreas, incluyendo el dominio
                $areas[] = [
                    'nombre' => $dato->dominio->area,
                    'hc' => $dato->numero_hc,
                    'domicilio' => $domicilio,
                    'telefono' => $telefono,
                    'dominio' => $dato->dominio->dominio  // Agregar el dominio aquí
                ];
            }
    
            // Recolectar consultas
            $consultas = [];
            foreach ($modelos as $model) {
                $dominio = $model->id_area;
                $auxiliar = new ConsultaAmbulatoriaMaster();
                $auxiliar->setDbId($dominio);
                $consultas_all = ConsultaAmbulatoriaMaster::findAll([
                    'paciente_id' => $model->id_federado_provincial
                ]);
                foreach ($consultas_all as $consulta_aux) {
                    $consultas[] = $consulta_aux;
                }
            }
    
            // Ordenar consultas
            usort($consultas, function($a1, $a2) {
                return $a1['id2'] <=> $a2['id2']; // Usar el operador de comparación de PHP 7+
            });
    
            // Generar PDF
            $html = $this->viewDatosHtml($model, $areas, $consultas);
            $titulo = 'Datos Paciente';
            $pdfContent = \app\components\Htmlpdf\Htmlpdf::widget([
                'titulo' => $titulo,
                'subtitulo' => 'Registro #' . $id,
                'data' => $html,
                'output' => true,
            ]);
            
            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');
            Yii::$app->response->data = $pdfContent;
        }
        Yii::$app->end();
    }
    
    
    protected function viewConsultaHtml($model,$subjetivo,$objetivo,$analisis,$plan)
    {
        $html=' <div style="font-family: Helvetica, sans-serif;">
                <div style="font-size:14px"><br>
                <b>'.$model->paciente_ape.", ".$model->paciente_nom.'</b><br>
                <b>'.strtoupper($model->paciente_tipodoc)." ".$model->paciente_numerodoc.'</b><br>
                </div>
                <div style="font-size:12px">
                <b>Fecha Nacimiento: </b>'.$model->paciente_nac.'<br>
                <b>Sexo: </b>'.(($model->paciente_sexo=='M')?" Masculino":(($model->paciente_sexo=='F')?" Femenino":" Indeterminado")).'<br><br>
                <b>Datos de la consulta</b><br>
                <b>Area: '.$model->area.'</b><br>
                <b>Centro: '.$model->centro." </b>(hc ".$model->paciente_numero_hc.')<br>
                <b>Fecha: </b>'.$model->fecha.' ('.substr(trim($model->id2),-8).')<br>
                <b>Servicio: </b>'.$model->servicio.'<br>
                <b>Especialidad: </b>'.$model->especialidad.'<br>
                <b>Profesional: </b>'.trim($model->medico_ape).', '.$model->medico_nom.'<br>    
                </div>
                <style>
                    table {border-collapse: collapse;font-family: Helvetica, sans-serif;font-size:12px;}
                    tr {border: 1px solid #fff}
                    th, td {border: 1px solid #fff;padding-left:5px;padding-right:5px;}
                </style>';
        // inicio datos subjetivos   
                    $html.='<br>
                    <b>Datos Subjetivos</b>
                    <div><table><tbody>';  
                foreach($subjetivo as $modelo){ 
                        $html.='<tr>
                        <th>nomenclador</th>
                        <th>codigo</th>
                        <th>codigo capitulo</th>
                        <th>codigo componente</th>
                        <th>codigo descripccion</th>
                        <th>Observaciones</th>
                        </tr>';
                        $html.='<tr>';
                        if(!isset($modelo->nomenclador))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->nomenclador.'</td>';
                        if(!isset($modelo->detalle))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->detalle.'</td>'; 
                        if(!isset($modelo->codigo_capitulo))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->codigo_capitulo.'</td>';
                        if(!isset($modelo->codigo_componente))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->codigo_componente.'</td>';
                        if(!isset($modelo->codigo_descripcion))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->codigo_descripcion.'</td>';
                        if(!isset($modelo->detalle))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->detalle .'</td>';
                        $html.='</tr>';
                }
        $html.='</tbody></table></div>';         // fin datos subjetivos
        // inicio datos objetivos
        $html.='<br>       
                <b>Datos Objetivos</b>
                <div> 
                <table>
                <tbody>';
                foreach($objetivo as $modelo){
                        $html.='<tr>
                        <th>nomenclador</th>
                        <th>codigo</th>
                        <th>examen especialidad capitulo</th>
                        <th>examen area</th>
                        <th>examen tipo</th>
                        <th>examen resultado</th>
                        <th>valor</th>
                        <th>Observaciones</th>
                        </tr>';
                        $html.="<tr>";
                        if(!isset($modelo->nomenclador))
                            $html.="<td> no definido </td>";
                        else
                            $html.="<td>".$modelo->nomenclador."</td>";
                        if(!isset($modelo->codigo_nomenclador))
                            $html.="<td> no definido </td>";
                        else
                            $html.="<td>".$modelo->codigo_nomenclador."</td>"; 
                        if(!isset($modelo->examen_especialidad))
                            $html.="<td> no definido </td>";
                        else
                            $html.="<td>".$modelo->examen_especialidad."</td>";
                        if(!isset($modelo->examen_area))
                            $html.="<td> no definido </td>";
                        else
                            $html.="<td>".$modelo->examen_area."</td>";
                        if(!isset($modelo->examen_tipo))
                            $html.="<td> no definido </td>";
                        else
                            $html.="<td>".$modelo->examen_tipo."</td>";
                        if(!isset($modelo->examen_resultado))
                            $html.="<td> no definido </td>";
                        else
                            $html.="<td>".$modelo->examen_resultado."</td>";
                        $html.="<td>";
                            if (trim($modelo->valor)=='true')
                                $html.='SI';
                            else if (trim($modelo->valor)=='false')    
                                $html.='NO';
                            else
                                $html.=$modelo->valor;
                        $html.="</td>";
                        if(!isset($modelo->detalle))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->detalle .'</td>';
                        $html.="</tr>";
                }
        $html.='</tbody></table></div>';       // fin datos objetivos
        // inicio datos analisis
        $html.='<br>
                <b>Analisis</b>
                <div> 
                <table>
                <tbody>';
                foreach($analisis as $modelo){
                        $html.='<tr>
                        <th>episodio</th>
                        <th>nomenclador</th>
                        <th>codigo nomenclador</th>
                        <th>codigo descripcion</th>
                        <th>tipo diagnostico</th>
                        <th>episodio</th>
                        <th>Observaciones</th>
                        </tr>';
                        $html.="<tr>";
                        if(!isset($modelo->episodio))
                            $html.="<td>Episodio: no definido </td>";
                        else
                            $html.="<td> Episodio: ".$modelo->episodio."</td>";
                        if(!isset($modelo->nomenclador))
                            $html.="<td>no definido </td>";
                        else
                            $html.="<td>".$modelo->nomenclador."</td>";
                        if(!isset($modelo->codigo_nomenclador))
                            $html.="<td>no definido </td>";
                        else
                            $html.="<td>".$modelo->codigo_nomenclador."</td>"; 
                        if(!isset($modelo->codigo_descripcion))
                            $html.="<td>no definido </td>";
                        else
                            $html.="<td>".$modelo->codigo_descripcion."</td>";
                        if (!isset($modelo->tipo_diagnostico))
                            $html.="<td>".$modelo->tipo_diagnostico."</td>";
                        else
                            $html.="<td>".$modelo->tipo_diagnostico ."</td>";
                        if(!isset($modelo->detalle))
                            $html.='<td> (no definido) </td>';
                        else
                            $html.='<td>'.$modelo->detalle .'</td>';
                        if(!isset($modelo->episodio))
                            $html.="<td>no definido </td>";
                        else
                            $html.="<td> Episodio: ".$modelo->episodio."</td>";
                        $html.="</tr>";
                }
                $html.='</tbody></table></div>';   
          // fin datos analisi
        // inicio datos plan
        $html.='<br>
                <b>Plan</b>
                <div> 
                <table>
                <tbody>';
                $html.='<tr>
                <th>nomenclador</th>
                <th>codigo nomenclador</th>
                <th>seccion nomenclador</th>
                <th>grupo nomenclador</th>
                <th>descripcion_completa</th>
                <th>preinforme</th>
                <th>Observaciones</th>
                <th>link informe</th>
                </tr>';
                foreach($plan as $modelo){
                        $html.="<tr>";
                        if(!isset($modelo->nomenclador))
                            $html.="<td> no definido</td>";
                        else
                            $html.="<td>".$modelo->nomenclador."</td>";
                        if(!isset($modelo->codigo_nomenclador))
                            $html.="<td> no definido</td>";     
                        else
                            $html.="<td>".$modelo->codigo_nomenclador."</td>";
                        if(!isset($modelo->seccion_nomenclador))
                            $html.="<td> no definido</td>";
                        else
                            $html.="<td>".$modelo->seccion_nomenclador."</td>";
                        if(!isset($modelo->grupo_nomenclador))
                            $html.="<td> no definido</td>";
                        else
                            $html.="<td>".$modelo->grupo_nomenclador."</td>";
                        if(!isset($modelo->descripcion_completa))
                            $html.="<td> no definido</td>";
                        else
                            $html.="<td>".$modelo->descripcion_completa."</td>";
                        if(!isset($modelo->preinforme))
                            $html.="<td> no definido</td>";
                        else
                            $html.="<td>".$modelo->preinforme."</td>";
                        $informe_adjunto=false;
                        $link_informe = "";
                        if(!isset($modelo->detalle))
                            $html.="<td>no definido. </td>";
                        else
                            $html.="<td>".$modelo->detalle."</td>";
                        if ($modelo->informe_adjunto!=null){
                            $informe_adjunto=true;
                            $link_informe .='<td>Informe ('.$modelo->informe_adjunto.') </td>'; 
                        }
                        if (isset($informe_adjunto)){
                            $html.= "<td><br><b>".$link_informe."</b></td>";
                        }
                        $html.="</tr>";
                }
                $html.="</tbody></table>";
        $html.='</div>';        // fin datos plan*/
        $html.='</div>';        // div inicial*/
        return $html;
    }

    protected function viewDatosHtml($model,$areas,$consultas)
    {
    $html=' <div>
            <div style="font-family: Helvetica, sans-serif;height:auto;float:left;width:75%;margin-top: 150px;"> 
                <div style="font-size:14px"><br>
                    <b>'.$model->primer_apellido." ".$model->segundo_apellido.", ".$model->primer_nombre." ".$model->segundo_nombre.'</b><br>
                    <b>'.strtoupper($model->documento_tipo)." ".$model->documento_numero;
                    if ($model->estado==2){
                       $html.=' (Paciente Fallecido)';
                    }   
    $html.=        '</b><br><br>                    
                    <b>'.(($model->flag_federado)?"Paciente Federado (id ".$model->id_federado_nacional.")":"Paciente NO Federado").'</b><br>
                </div>                    
                <div style="font-size:12px">
                        <b>Fecha Nacimiento: </b>'.$model->fecha_nacimiento.'<br>
                        <b>Sexo: </b>'.(($model->sexo=='male')?" Masculino":(($model->sexo=='female')?" Femenino":" Indeterminado")).'<br>
                </div>';
                    foreach($areas as $area){
                        $html.='<div style="font-size:14px">' ;
                        $html.="<b>Area: ".$area['nombre']." </b>(hc ".$area['hc'].")<br>" ;
                        $html.='</div>
                        <div style="font-size:12px">
                        <b>Domicilio: </b>'.$area['domicilio'].'<br>
                        <b>Telefono: </b>'.$area['telefono'].'<br>
                        </div>';
                    }
    $html.='    
                <br>

                <style>
                    table, tbody, tr, th { 
                        padding-left: 10px; 
                        text-align:left;
                        font-family: Helvetica, sans-serif;
                        font-size:12px;
                    }
                </style>

                <div><b>DATOS RENAPER</b></div>
                <table>
                <tbody>';
                    $json_renaper=MaestroController::consulta_renaper($model->id_maestro);
                    if (is_array($json_renaper)){ 
                        $apellido_comparacion=strtoupper(trim(trim($model->primer_apellido)." ".trim($model->segundo_apellido)));
                        $nombre_comparacion=strtoupper(trim(trim($model->primer_nombre)." ".trim($model->segundo_nombre)));   // minusculas
                        $fecha_nacimiento_comparacion = Metodos::dateConvert($model->fecha_nacimiento,'toSql');
                        $documento_tipo_comparacion=trim($model->documento_tipo);
                        $value=$json_renaper['apellido'];
                        // como el renaper es una garca y puedo tener 2 espacios cuando hay dos nombres o mas
                        // tengo que hacer esta chotada.
                        $auxiliar=explode(" ",$value);
                        $value="";
                        foreach($auxiliar as $apellido){
                            if ($apellido) {
                                $value = $value . " " . $apellido;
                            }
                        }
                        if (strtoupper(trim($value))==$apellido_comparacion){
                            $html.="<tr><th>Apellido</th><td>".$value."</td></tr>";
                        } else {
                            $html.="<tr><th style='font-weight: bold;'>Apellido</th><td style='font-weight: bold;'>".$value."</td></tr>";
                        }
                        $value=$json_renaper['nombres'];
                        // como el renaper es una garca y puedo tener 2 espacios cuando hay dos nombres o mas
                        // tengo que hacer esta chotada.
                        $auxiliar=explode(" ",$value);
                        $value="";
                        foreach($auxiliar as $nombre){
                            if ($nombre) {
                                $value = $value . " " . $nombre;
                            }
                        }
                        if (strtoupper(trim($value))==$nombre_comparacion){
                            $html.="<tr><th>Nombre</th><td>".$value."</td></tr>";
                        } else {
                            $html.="<tr><th style='font-weight: bold;'>Nombre</th><td style='font-weight: bold;'>".$value."</td></tr>";
                        }

                        $value=$json_renaper['fechaNacimiento'];
                        if (trim($value)==$fecha_nacimiento_comparacion){
                            $html.="<tr><th>Fecha Nacimiento</th><td>".$value."</td></tr>";
                        } else {
                            $html.="<tr><th style='font-weight: bold;'>Fecha Nacimiento</th><td style='font-weight: bold;'>".$value."</td></tr>";
                        }
                        $value=$json_renaper['sexo'];
                        $html.="<tr><th>Sexo</th><td>".$value."</td></tr>";
                        $value=$json_renaper['cuil'];
                        $html.="<tr><th>CUIL</th><td>".$value."</td></tr>";
                        $value=$json_renaper['calle']." ".$json_renaper['numero']." ".$json_renaper['piso']." ".$json_renaper['departamento'];
                        $html.="<tr><th>Dirección</th><td>".$value."</td></tr>";
                        $value=$json_renaper['cpostal']." ".$json_renaper['ciudad']." ".$json_renaper['provincia']." ".$json_renaper['pais'];
                        $html.="<tr><th>Ciudad</th><td>".$value."</td></tr>";                        
                    }else{
                        echo "<tr><td>&nbsp;&nbsp;".$json_renaper."</td></tr>";
                    }
    $html.='    </tbody>
                </table>
                <br>
                <div><b>RED DIGITAL DE SALUD</b></div>
                <table>
                <tbody>';
                    if ($model->flag_federado){
                        $json_federador=MaestroController::consulta_federador($model->id_maestro);
                        if (is_array($json_federador)){
                            foreach ($json_federador["entry"] as $entry) {                            
                                $identifier = $entry["resource"]["identifier"];                
                                foreach ($identifier as $key1 => $value_aux) {
                                    $system = $value_aux['system'];
                                    $value = $value_aux['value'];
                                    $html.="<tr><th>".$system."</th><td>".$value."</td></tr>";
                                }
                            }
                        }else{
                            echo "<tr><td>&nbsp;&nbsp;".$json_federador."</td></tr>";
                        }
                    }else{
                        echo "<tr><td>&nbsp;&nbsp;"."Paciente no federado"."</td></tr>";
                    }
    $html.='    </tbody>
                </table>
            </div>

            <div style="float:left; width:25%"><br>
                <div style="border:1px solid grey; height:auto; padding:15px">';
                    if (is_array($json_renaper) and $json_renaper['foto'])
                        $html.="<img style='max-width:250px; height: auto;' src='".$json_renaper['foto']."' />";
                    else
                        $html.="<img style='max-width:250px; height: auto;' src='images/imagen_anonimo.png' />";                 
    $html.='    </div>            
            </div>
            </div>
            
            <br>            
            <div id="tabla_consulta" style="clear:both;font-family: Helvetica, sans-serif;">
                <b>CONSULTAS AMBULATORIAS</b>
                <style>
                    #tabla_consulta table, #tabla_consulta thead, #tabla_consulta tbody, #tabla_consulta tr, #tabla_consulta th, #tabla_consulta td {
                        border:1px solid #ccc;
                        text-align:left;
                        font-size:10px; 
                        padding-left:10px;
                        padding-right:10px;
                    }
                </style>
                <table style="border-collapse:collapse;">
                <thead>
                <tr><th>Fecha</th><th>Area</th><th>Servicio</th><th>Especialidad</th><th>Medico</th></tr>
                </thead>
                <tbody>';
                foreach($consultas as $consulta){
                    $html.="<tr>";
                    $html.="<td>".$consulta->fecha." (".substr(trim($consulta->id2),-8).")</td>";
                    $html.="<td>".Dominio::findOne(['id'=>$consulta->dominio])->area."</td>";
                    $html.="<td>".$consulta->servicio."</td>"; 
                    $html.="<td>".$consulta->especialidad."</td>"; 
                    $html.="<td>".trim($consulta->medico_ape).", ".$consulta->medico_nom."</td>"; 
                    $html.="</tr>";
                }
                $html.="
                </tbody>
                </table>
            </div>";             
            return $html;
    }

    public function actionBuscarlistado()
    {    
        $model= new ListadoForm();
        if ($model->load(Yii::$app->request->post())) {
            if(isset($_POST['filtros']))
            {
                return $this->render('buscarfiltros', [
                    'model' => $model,
                ]);
            }else{         
                // guardo en sesion el modelo y despues lo recupero
                Yii::$app->session->set('listado-consultas-ambulatorias',serialize($model));
                return $this->redirect(['listado']);
            }
        }else{
            return $this->render('buscarlistado', [
                'model' => $model,
            ]);
        }

    }

    public function actionListado()
    {    
        $model=Yii::$app->session->get('listado-consultas-ambulatorias');
        if ($model)
            $listado=unserialize($model);
        if ($listado){
            $datos=unserialize($this->sqlFromListado($listado));
            $sql=$datos['sql'];
            $sqlCount=$datos['sqlCount'];
            $columnas=$datos['columnas'];
            $count = Yii::$app->db->createCommand($sqlCount)->queryAll();
            $totalCount=$count[0]['count'];
            $dataProvider = new SqlDataProvider([
                'sql' => $sql,
                'totalCount' => $totalCount,
                'sort' => false,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
            return $this->render('listado', [
                'dataProvider'=>$dataProvider,
                'columns'=>$columnas,
                'listado'=>$listado,
            ]);
        }
    }

    public function actionExportlistado()
    {
        $model=Yii::$app->session->get('listado-consultas-ambulatorias');
        $listado=unserialize($model);
        if ($model)
            $listado=unserialize($model);
        if ($listado){
            $datos=unserialize($this->sqlFromListado($listado));
            $nombre_area=Dominio::findOne($listado->dominio)->area;
            $sql=$datos['sql'];
            $columnas=$datos['columnas'];
            $datos=array();
            foreach($columnas as $datosAux) {
                $nombre='';
                $nombreExp=explode('_',$datosAux);
                foreach($nombreExp as $nombreAux){
                    $nombre.=ucfirst($nombreAux)." ";
                }
                $datos[$datosAux]=array(trim($nombre),0);
            }
            $reporte= new Reporte();
            $reporte->id_usuario=Yii::$app->user->id ;
            $reporte->modelo='consulta_ambulatoria_master';
            $reporte->titulo="Listado de Consultas Ambulatorias";
            $reporte->subtitulo=$nombre_area;
            $reporte->tamano_letra=0;
            $reporte->tipo_hoja=0;
            $reporte->ancho_reporte=0;
            $reporte->resumen='';
            $reporte->datos=serialize($datos);
            $reporte->seleccion=$sql;
            $tipo_archivo='csv';
            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo,
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();

        }
    }

    public static function sqlFromListado($listado)
    {
        $fecha_desde=Metodos::dateConvert($listado->fecha_desde,'toSql');
        $fecha_hasta=Metodos::dateConvert($listado->fecha_hasta,'toSql');
        switch($listado->dominio)
        {
            case 100:
                $schema='hospitalviedma';
                break;
            case 200:
                $schema='hospitalbariloche';
                break;
            case 300:
                $schema='hospitalroca';
                break;
            case 400:
                $schema='hospitalcipolletti';
                break;
        }
        // SELECT INICIAL del dataprovider
        $select="select 
                 consulta_ambulatoria_master.centro as centro,
                 consulta_ambulatoria_master.paciente_numero_hc as hc,
                 consulta_ambulatoria_master.paciente_ape as apellido,
                 consulta_ambulatoria_master.paciente_nom as nombre,
                 consulta_ambulatoria_master.paciente_nac as fecha_nac,
                 consulta_ambulatoria_master.paciente_sexo as sexo,
                 consulta_ambulatoria_master.paciente_tipodoc as tipo_doc,
                 consulta_ambulatoria_master.paciente_numerodoc as numero_doc,
                 consulta_ambulatoria_master.fecha as fecha,
                 consulta_ambulatoria_master.servicio as servicio,
                 consulta_ambulatoria_master.especialidad as especialidad,
                 consulta_ambulatoria_master.cie10_codigo as diagnostico,
                 concat(trim(consulta_ambulatoria_master.medico_ape),' ',trim(consulta_ambulatoria_master.medico_nom)) as profesional";
        $where=" where consulta_ambulatoria_master.fecha>='".$fecha_desde."' and consulta_ambulatoria_master.fecha<='".$fecha_hasta."'";
        $join='';
        if (!empty($listado->centro)){
            $where.=" and consulta_ambulatoria_master.centro='".$listado->centro."'";
        }
        if (!empty($listado->servicio)){
            $where.=" and consulta_ambulatoria_master.servicio='".$listado->servicio."'";
        }
        if (!empty($listado->especialidad)){
            $where.=" and consulta_ambulatoria_master.especialidad='".$listado->especialidad."'";
        }
        if (!empty($listado->diagnostico)){
            $where.=" and consulta_ambulatoria_master.cie10_codigo='".$listado->diagnostico."'";
        }
        // construyo columnas
        $columnas=[
                'centro',
                'hc',
                'apellido',
                'nombre',
                'fecha_nac',
                'sexo',
                'tipo_doc',
                'numero_doc',
                'fecha',
                'servicio',
                'especialidad',
                'diagnostico',
                'profesional',
        ];
        // datos subjetivos
        if (!(empty($listado->subjetivoicpc2_1) and
              empty($listado->subjetivoicpc2_2) and
              empty($listado->subjetivoicpc2_3) and
              empty($listado->subjetivoicpc2_4) and
              empty($listado->subjetivoicpc2_5) and
              empty($listado->subjetivoobs)) ){
              $join.=" inner join ".$schema.".consulta_ambulatoria_subjetivo on consulta_ambulatoria_master.id1=consulta_ambulatoria_subjetivo.paciente_id and consulta_ambulatoria_master.id2=consulta_ambulatoria_subjetivo.fecha_consulta";
              $select.=',consulta_ambulatoria_subjetivo.codigo_nomenclador as subjetivo_icpc2
                        ,consulta_ambulatoria_subjetivo.detalle as subjetivo_obs';
              $columnas[]='subjetivo_icpc2';
              $columnas[]='subjetivo_obs';          
              if (!empty($listado->subjetivoicpc2_1)){
                $where.=" and consulta_ambulatoria_subjetivo.codigo_nomenclador='".$listado->subjetivoicpc2_1."'";
              }
              if (!empty($listado->subjetivoicpc2_2)){
                $where.=" and consulta_ambulatoria_subjetivo.codigo_nomenclador='".$listado->subjetivoicpc2_2."'";
              }
              if (!empty($listado->subjetivoicpc2_3)){
                $where.=" and consulta_ambulatoria_subjetivo.codigo_nomenclador='".$listado->subjetivoicpc2_3."'";
              }
              if (!empty($listado->subjetivoicpc2_4)){
                $where.=" and consulta_ambulatoria_subjetivo.codigo_nomenclador='".$listado->subjetivoicpc2_4."'";
              }
              if (!empty($listado->subjetivoicpc2_5)){
                $where.=" and consulta_ambulatoria_subjetivo.codigo_nomenclador='".$listado->subjetivoicpc2_5."'";
              }
              if (!(empty($listado->subjetivoobs)) ){
                  $where.=" and upper(consulta_ambulatoria_subjetivo.detalle) like '%".strtoupper($listado->subjetivoobs)."%'";
              }
        }
        // datos objetivos
        if (!(empty($listado->objetivosistema_1) and
              empty($listado->objetivosistema_2) and
              empty($listado->objetivosistema_3) and
              empty($listado->objetivosistema_4) and
              empty($listado->objetivosistema_5) and
              empty($listado->objetivoobs)) ){
              $join.=" inner join ".$schema.".consulta_ambulatoria_objetivo on consulta_ambulatoria_master.id1=consulta_ambulatoria_objetivo.paciente_id and consulta_ambulatoria_master.id2=consulta_ambulatoria_objetivo.fecha_consulta";
              $select.=',consulta_ambulatoria_objetivo.codigo_nomenclador as objetivo_codigo
                        ,consulta_ambulatoria_objetivo.valor as objetivo_valor
                        ,consulta_ambulatoria_objetivo.detalle as objetivo_obs';
              $columnas[]='objetivo_codigo';
              $columnas[]='objetivo_valor';          
              $columnas[]='objetivo_obs';          
              if (!empty($listado->objetivosistema_1)){
                $where.=" and consulta_ambulatoria_objetivo.codigo_nomenclador='".$listado->objetivosistema_1."'";
              }
              if (!empty($listado->objetivosistema_2)){
                $where.=" and consulta_ambulatoria_objetivo.codigo_nomenclador='".$listado->objetivosistema_2."'";
              }
              if (!empty($listado->objetivosistema_3)){
                $where.=" and consulta_ambulatoria_objetivo.codigo_nomenclador='".$listado->objetivosistema_3."'";
              }
              if (!empty($listado->objetivosistema_4)){
                $where.=" and consulta_ambulatoria_objetivo.codigo_nomenclador='".$listado->objetivosistema_4."'";
              }
              if (!empty($listado->objetivosistema_5)){
                $where.=" and consulta_ambulatoria_objetivo.codigo_nomenclador='".$listado->objetivosistema_5."'";
              }
              if (!(empty($listado->objetivoobs)) ){
                  $where.=" and upper(consulta_ambulatoria_objetivo.detalle) like '%".strtoupper($listado->objetivoobs)."%'";
              }
        }
        // analisis
        if (!(empty($listado->analisiscie10_1) and
              empty($listado->analisiscie10_2) and
              empty($listado->analisiscie10_3) and
              empty($listado->analisiscie10_4) and
              empty($listado->analisiscie10_5) and
              empty($listado->analisisobs)) ){
              $join.=" inner join ".$schema.".consulta_ambulatoria_analisis on consulta_ambulatoria_master.id1=consulta_ambulatoria_analisis.paciente_id and consulta_ambulatoria_master.id2=consulta_ambulatoria_analisis.fecha_consulta";
              $select.=',consulta_ambulatoria_analisis.episodio as analisis_episodio
                        ,consulta_ambulatoria_analisis.codigo_nomenclador as analisis_cie10
                        ,consulta_ambulatoria_analisis.tipo_diagnostico as analisis_tipo_diag
                        ,consulta_ambulatoria_analisis.detalle as analisis_obs';
              $columnas[]='analisis_episodio';
              $columnas[]='analisis_cie10';
              $columnas[]='analisis_tipo_diag';          
              $columnas[]='analisis_obs';          
              if (!empty($listado->analisiscie10_1)){
                $where.=" and consulta_ambulatoria_analisis.codigo_nomenclador='".$listado->analisiscie10_1."'";
              }
              if (!empty($listado->analisiscie10_2)){
                $where.=" and consulta_ambulatoria_analisis.codigo_nomenclador='".$listado->analisiscie10_2."'";
              }
              if (!empty($listado->analisiscie10_3)){
                $where.=" and consulta_ambulatoria_analisis.codigo_nomenclador='".$listado->analisiscie10_3."'";
              }
              if (!empty($listado->analisiscie10_4)){
                $where.=" and consulta_ambulatoria_analisis.codigo_nomenclador='".$listado->analisiscie10_4."'";
              }
              if (!empty($listado->analisiscie10_5)){
                $where.=" and consulta_ambulatoria_analisis.codigo_nomenclador='".$listado->analisiscie10_5."'";
              }

              if (!(empty($listado->analisisobs)) ){
                  $where.=" and upper(consulta_ambulatoria_analisis.detalle) like '%".strtoupper($listado->analisisobs)."%'";
              }
        }

        // plan
        if (!(empty($listado->plannome_1) and
              empty($listado->plannome_2) and
              empty($listado->plannome_3) and
              empty($listado->plannome_4) and
              empty($listado->plannome_5) and
              empty($listado->planobs)) ){
                
              $join.=" inner join ".$schema.".consulta_ambulatoria_plan on consulta_ambulatoria_master.id1=consulta_ambulatoria_plan.paciente_id and consulta_ambulatoria_master.id2=consulta_ambulatoria_plan.fecha_consulta";
              $select.=',consulta_ambulatoria_plan.seccion_nomenclador as plan_seccion
                        ,consulta_ambulatoria_plan.codigo_nomenclador as plan_nomenclador
                        ,consulta_ambulatoria_plan.preinforme as plan_preinf
                        ,consulta_ambulatoria_plan.detalle as plan_obs';

              $columnas[]='plan_seccion';
              $columnas[]='plan_nomenclador';
              $columnas[]='plan_preinf';          
              $columnas[]='plan_obs';          


              if (!empty($listado->plannome_1)){
                $plannome1=explode('.',$listado->plannome_1);
                $where.=" and consulta_ambulatoria_plan.seccion_nomenclador='".$plannome1[0]."' "."
                          and consulta_ambulatoria_plan.codigo_nomenclador='".$plannome1[1]."'";
              }
              if (!empty($listado->plannome_2)){
                $plannome2=explode('.',$listado->plannome_2);
                $where.=" and consulta_ambulatoria_plan.seccion_nomenclador='".$plannome2[0]."' "."
                          and consulta_ambulatoria_plan.codigo_nomenclador='".$plannome2[1]."'";
              }
              if (!empty($listado->plannome_3)){
                $plannome3=explode('.',$listado->plannome_3);
                $where.=" and consulta_ambulatoria_plan.seccion_nomenclador='".$plannome3[0]."' "."
                          and consulta_ambulatoria_plan.codigo_nomenclador='".$plannome3[1]."'";
              }
              if (!empty($listado->plannome_4)){
                $plannome4=explode('.',$listado->plannome_4);
                $where.=" and consulta_ambulatoria_plan.seccion_nomenclador='".$plannome4[0]."' "."
                          and consulta_ambulatoria_plan.codigo_nomenclador='".$plannome4[1]."'";
              }
              if (!empty($listado->plannome_5)){
                $plannome5=explode('.',$listado->plannome_5);
                $where.=" and consulta_ambulatoria_plan.seccion_nomenclador='".$plannome5[0]."' "."
                          and consulta_ambulatoria_plan.codigo_nomenclador='".$plannome5[1]."'";
              }

              if (!(empty($listado->planobs)) ){
                  $where.=" and upper(consulta_ambulatoria_plan.detalle) like '%".strtoupper($listado->planobs)."%'";
              }
        }

        $sql=$select." from ".$schema.".consulta_ambulatoria_master".$join.$where;
        $sqlCount="select count(*) from ".$schema.".consulta_ambulatoria_master".$join.$where;

        return serialize(['sql'=>$sql,'sqlCount'=>$sqlCount,'columnas'=>$columnas]);
        
    }


    public function actionServicioespecialidad()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if (isset($_POST['dominio']))
            $dominio=$_POST['dominio'];
        else
            $dominio=null;

        if (isset($_POST['servicio']))
            $servicio=$_POST['servicio'];
        else
            $servicio=false;

        $return="<option value=''>Seleccione una opción</option>";
        if ($dominio){
         
            if($servicio===false){

                // devolucion de servicio (servicioespecialidad group by)

                $model= new ServicioEspecialidad();
                $model->setDbId($dominio);
    
                $servicios = $model->find()
                ->select(['nombre_servicio'])
                ->groupBy(['nombre_servicio'])
                ->orderBy('nombre_servicio')
                ->all();

                if ($servicios) {
                    foreach ($servicios as $servicio){
                        $return.="<option value='" . $servicio->nombre_servicio . "'>" . $servicio->nombre_servicio . "</option>";
                    }
                }

            }else{
                // devolucion de especialidad (servicioespecialidad where)

                $model= new ServicioEspecialidad();
                $model->setDbId($dominio);

                if (empty($servicio)){
                    $servicios = $model->find()
                    ->select(['nombre_especialidad'])
                    ->groupBy(['nombre_especialidad'])
                    ->orderBy('nombre_especialidad')
                    ->all();
                }else{
                    $servicios = $model->find()
                    ->select(['nombre_especialidad'])
                    ->where(['nombre_servicio'=>$servicio])
                    ->orderBy('nombre_especialidad')
                    ->all();
                }

                if ($servicios) {
                    foreach ($servicios as $servicio){
                        $return.="<option value='" . $servicio->nombre_especialidad . "'>" . $servicio->nombre_especialidad . "</option>";
                    }
                }
            }
        }
        return $return;   
    }

    public function actionCentro()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if (isset($_POST['dominio']))
            $dominio=$_POST['dominio'];
        else
            $dominio=null;

        $return="<option value=''>Seleccione una opción</option>";
        if ($dominio){
         
                // devolucion de centros)

                $model= new ConsultaAmbulatoriaMaster();
                $model->setDbId($dominio);
    
                $centros = $model->find()
                ->select(['centro'])
                ->groupBy(['centro'])
                ->all();

                if ($centros) {
                    foreach ($centros as $centro){
                        $return.="<option value='" . $centro->centro . "'>" . $centro->centro . "</option>";
                    }
                }

        }
        return $return;   
    }

    public function actionIcpc2($q = null,$id = null, $dominio=null)
    {

        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            $results = ['results' => ['id' => '', 'text' => '']];

            if (!$dominio){
                $dominio=300;   // roca
            }

            if (!is_null($q) and !is_null($dominio) ) {

                $q="'%".strtoupper(addslashes($q))."%'";

                $modelICPC2= new ICPC2();
                $modelICPC2->setDbId($dominio);

                $data = $modelICPC2->find()
                ->select(['id as id, trim(nombre) as text'])
                ->where("upper(concat('(',id,')',' ',nombre)) like ".$q)
                ->asArray()->limit(50)->all();

                $results['results'] = array_values($data);
            
            }elseif ($id > 0){
            
//                $results['results'] = ['id' => $id, 'text' => ICPC2::findOne($id)->nombre];
            
            }
            return $results;


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }


    public function actionSistema($q = null,$id = null, $dominio=null)
    {

        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            $results = ['results' => ['id' => '', 'text' => '']];

            if (!$dominio){
                $dominio=300;   // roca
            }

            if (!is_null($q) and !is_null($dominio) ) {

                $q="'%".strtoupper(addslashes($q))."%'";

                $modelSISTEMA= new SISTEMA();
                $modelSISTEMA->setDbId($dominio);

                $data = $modelSISTEMA->find()
                    ->select(['id as id, trim(nombre) as text'])
                    ->where("upper(concat('(',id,')',' ',nombre)) like ".$q)
                    ->asArray()->limit(50)->all();

                $results['results'] = array_values($data);
            
            }elseif ($id > 0){
            
//                $results['results'] = ['id' => $id, 'text' => ICPC2::findOne($id)->nombre];
            
            }
            return $results;


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionCie10($q = null,$id = null, $dominio=null)
    {

        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            $results = ['results' => ['id' => '', 'text' => '']];

            if (!$dominio){
                $dominio=300;   // roca
            }

            if (!is_null($q) and !is_null($dominio) ) {

                $q="'%".strtoupper(addslashes($q))."%'";

                $modelCIE10= new CIE10();
                $modelCIE10->setDbId($dominio);

                $data=$modelCIE10::find()->select(['id as id, trim(nombre) as text'])
                      ->where("upper(concat('(',id,')',' ',nombre)) like ".$q)
                      ->asArray()->limit(50)->all();
    
                $results['results'] = array_values($data);
            
            }elseif ($id > 0){
            
//                $results['results'] = ['id' => $id, 'text' => ICPC2::findOne($id)->nombre];
            
            }
            return $results;


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionNomenclador($q = null,$id = null, $dominio=null)
    {

        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            $results = ['results' => ['id' => '', 'text' => '']];

            if (!$dominio){
                $dominio=300;   // roca
            }

            if (!is_null($q) and !is_null($dominio) ) {

                $q="'%".strtoupper(addslashes($q))."%'";

                $modelNOME= new NOMENCLADOR();
                $modelNOME->setDbId($dominio);

                $data = $modelNOME->find()
                    ->select(['id as id, trim(nombre) as text'])
                    ->where("upper(concat('(',id,')',' ',nombre)) like ".$q)
                    ->asArray()->limit(50)->all();

                $results['results'] = array_values($data);
            
            }elseif ($id > 0){
            
//                $results['results'] = ['id' => $id, 'text' => ICPC2::findOne($id)->nombre];
            
            }
            return $results;


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }
    public function actionHtmlplan($modelPlan){

        $html=' <div style="font-family: Helvetica, sans-serif"> 

        <style>
table {  border:1px solid  #337ab7;    border-collapse: separate; border-radius:5px; border-spacing:0;
        }
td, th  { border: none; }
td + td, th + th {border-left:1px solid #ddd}
th, tr td  {border-bottom:1px solid #ddd}
th:first-child { border-radius:5px 0 0 0; border-left:solid}
th:last-child { border-radius:0 5px 0 0;border-right:solid }
tfoot td:first-child {  border-radius:0 0 0 5px ;border-left:solid}
tfoot td:last-child {  border-radius:0 0 5px 0;border-right:solid}
</style>';
$informe_adjunto=false;
$link_informe = "";
$html.='<br>
<b>Plan</b>
<div> 
<table>
<tbody>';
$html.='<tr>
<th>nomenclador</th>
<th>codigo nomenclador</th>
<th>seccion nomenclador</th>
<th>grupo nomenclador</th>
<th>descripcion_completa</th>
<th>preinforme</th>
<th>Observaciones</th>
<th>link informe</th>
</tr>';

foreach($modelPlan as $modelo){
    $html.="<tr>";
    if(!isset($modelo->nomenclador))
        $html.="<td> no definido</td>";
    else
        $html.="<td>".$modelo->nomenclador."</td>";
    if(!isset($modelo->codigo_nomenclador))
        $html.="<td> no definido</td>";
        
    else
        $html.="<td>".$modelo->codigo_nomenclador."</td>";
    if(!isset($modelo->seccion_nomenclador)){
        $html.="<td> no definido</td>";
    }
    else
    {
        $html.="<td>".$modelo->seccion_nomenclador."</td>";
    }
    if(!isset($modelo->grupo_nomenclador)){
        $html.="<td> no definido</td>";
    }
    else{
        $html.="<td>".$modelo->grupo_nomenclador."</td>";
    }
    if(!isset($modelo->descripccion_completa)){
        $html.="<td> no definido</td>";
    }
    else{
        $html.="<td>".$modelo->descripccion_completa."</td>";
    }
    if(!isset($modelo->preinforme)){
        $html.="<td> no definido</td>";
    }
    else{
        $html.="<td>".$modelo->preinforme."</td>";
    }
    if(!isset($modelo->detalle)){
            $html.="<td> no definido</td>";
        }
    else
        {
        $html.="<td>".$modelo->detalle."</td>";
        }
    if (isset($modelo->informe_adjunto)){
        $informe_adjunto=true;
        $link_informe .='<td>Informe ('.$modelo->informe_adjunto.') </td>'; 
    }
    else{
        $html.="<td> no definido</td>";
    }
    if (isset($informe_adjunto)){
        $html.= "<td><br><b>".$link_informe."</b></td>";
    }
        $html.="</tr>";
}
    $html.="</tbody></table>";
    $html.='</div>';        // fin datos plan*/
    $html.='</div>';        // div inicial*/
return $html;     
    }

    public function actionPdfplan(array $id,$dominio){
        try {
        $titulo='Datos plan';
        $id1=$id['id1'];
        $id2=$id['id2'];
        $model_plan= new ConsultaAmbulatoriaPlan();
        $model_plan->setDbId($dominio);
        
        $model_plan=ConsultaAmbulatoriaPlan::findAll(['paciente_id'=>$id1,'fecha_consulta'=>$id2]);
       
        if($model_plan!=null){
         
            $html=$this->actionHtmlplan($model_plan);
            \app\components\Htmlpdf\Htmlpdf::widget(array(
                'titulo'=>$titulo,
                //'subtitulo'=>'Registro #'.$id,
                'data'=>$html,
            ));
    
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
      

        Yii::$app->end();
        
        }
        else{
            Yii::$app->response->format = Response::FORMAT_HTML;
            throw new NotFoundHttpException('Se encuentra vacio el PLAN ',500);
        }
       
    } catch (yii\db\Exception $e ) {
        Yii::$app->response->format = Response::FORMAT_HTML;
        throw new NotFoundHttpException('Error de base de datos.',500);
    }
      
    }
    public function actionInforme($dominio=null,$informe=null)
    {
    
            Yii::$app->response->format = Response::FORMAT_JSON;
    
            if ($dominio and $informe){

                switch($dominio)
                {
                    case 100: // Viedma
                        $db_informe = pg_connect("host=172.16.0.5 port=5432 dbname=ALTEA_SP_RN_PROD user=postgres password=omega");
                    break;
                    case 200: // Bariloche
                        $db_informe = pg_connect("host=172.16.0.2 port=5432 dbname=ALTEA_RN_PROD user=postgres password=omega");
                    break;
                    case 300:  // Roca
                        $db_informe = pg_connect("host=172.16.0.4 port=5432 dbname=ALTEA_RN_PROD user=postgres password=omega");
                    break;
                    case 400:  // Cipo
                        $db_informe = pg_connect("host=172.16.0.3 port=5432 dbname=ALTEA_RN_PROD user=postgres password=omega");
                    break;
                }

                if ($db_informe){

                    $result = pg_query($db_informe, "SELECT mdmedarchi as archivo, mdmedfilet as tipo FROM mdmedia WHERE mdmedcodig=".$informe.";");

                    if (pg_last_error()){
                        // error de conexion
                        return ['status' => 'error'];
                        Yii::$app->end();
                    }else{
                        $r = pg_fetch_assoc($result);
                        $archivo=$r['archivo'];
                        $tipo=ltrim($r['tipo']);
                        pg_close($db_informe);
                    }

                    if (empty($archivo)){
                        // error de archivo
                        return ['status' => 'error'];
                        Yii::$app->end();
                    }
                    
                    $file = fopen("assets/informe_".$informe.".".$tipo,"w");
                    fwrite($file,pg_unescape_bytea($archivo));
                    fclose($file);

                    return ['status' => 'success',
                    'file' => "assets/informe_".$informe.".".$tipo,
                    ];

                    Yii::$app->end();

                }
            }
    }
}
