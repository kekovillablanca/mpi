<?php

namespace app\modules\mpi\controllers;

use app\modules\admin\controllers\UsuarioestadisticaController;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
//
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;
use app\modules\mpi\models\Turno;
use app\modules\mpi\models\TurnoSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\modules\internacion\models\SistemaEducativo;
use app\modules\mpi\models\AnioFecha;
use app\modules\mpi\models\Contacto;
use app\modules\mpi\models\Paciente;
use app\modules\mpi\models\PacienteSearch;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use app\modules\mpi\models\Profesional;
use app\modules\mpi\models\PServicio;

use app\modules\mpi\models\Establecimiento;
use app\modules\mpi\models\Diagnostico;
use app\modules\mpi\models\Edad;
use app\modules\mpi\models\Especialidad;
use app\modules\mpi\models\HistoriaClinica;
use app\modules\mpi\models\Localidad;
use app\modules\mpi\models\ObraSocial;
use DateTime;
use Exception;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\db\BaseActiveRecord;
use yii\helpers\Url;

/**
 * TurnoController implements the CRUD actions for Turno model.
 */
class TurnoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','exportall','select','list'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','exportall','select','list'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }
    /*private function obtenerUsuarioPorAreaReal(){
        $sql = "SELECT l.nombre FROM consultasestadisticas.localidad l  JOIN sies.usuario_estadistica ue  ON(l.id=ue.id_localidad) JOIN public.usuario u  on (u.id=ue.id_usuario)
        where u.id=".Yii::$app->user->identity->id;
        $query= Yii::$app->db->createCommand($sql);
        $data = $query->queryAll();
        $q=null;
        $results = array_values($data); 
        if (isset($results[0])){
            $localidad=$results[0]["nombre"];
        $q=strtoupper(addslashes(trim($localidad)));
        }
        return $q;
    }*/

    /*private function obtenerUsuarioPorArea(){
        $sql = "SELECT ma.descrip FROM sies.m_area ma  JOIN sies.usuario_estadistica ue  ON(ma.id=ue.id_m_area) JOIN public.usuario u  on (u.id=ue.id_usuario)
        where u.id=".Yii::$app->user->identity->id;
        $query= Yii::$app->db->createCommand($sql);
        $data = $query->queryAll();
        $results = array_values($data); 

        $descrip=$results[0]["descrip"];

        $descripcciones=explode(" ",$descrip);
       
        $verificarString="";
        
        for ($i=0;$i<count($descripcciones);$i++){
            if($i>0){
                $verificarString=$verificarString." ".$descripcciones[$i];
            
            }

        }
        
       
        
       
        $q=strtoupper(addslashes(trim($verificarString)));
       
        return $q;
    }*/
    //mostrar por fechas los turnos

    /**
     * Lists all Turno models.
     * @return mixed
     */
    public function actionIndex($indice=null,$tipo=null,$anio=null)
    {    
        if($indice==null){
            $mes=date('n');
            if($mes>=1 && $mes<=3 )
                $indice=1;
            if($mes>=4 && $mes<=6 )
                $indice=2;
            if($mes>=7 && $mes<=9 )
                $indice=3;
            if($mes>=10 && $mes<=12 )
                $indice=4;
            }
        if($tipo==null){
            $tipo="anio";
        }
        if($anio==null){
            $anio=date("Y");
        }
        if($tipo=="restar"){
            if($indice==1){
                $indice=4;
                    
                }
                else{
                    $indice--;
                }
            }
            else if($tipo=="sumar"){
                if($indice==4){
                    $indice=1;}
                    else{
                        $indice++;
                    }
            }
        $model= new Turno();
        $searchModel = new TurnoSearch();
         $q=UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
         $mesesTrimestre=[1=>[1,3,"enero-marzo"],2=>[4,6,"abril-junio"],3=>[7,9,"julio-septiembre"],4=>[10,12,"octubre-diciembre"]];
         $tiempoAhora=date("j-n-Y");
         $tiempoList=explode("-",$tiempoAhora);
         $fechaInicio="";
         $fechaFin="";
         $hastaQueDiaTiene=0;
        for ($i=count($tiempoList)-1;$i>=0;$i--){
            if($i==2) {
                 $fechaInicio=$fechaInicio."".$anio;
                 $fechaFin=$fechaFin."".$anio;
            }
            if($i==1){
                 $fechaInicio=$fechaInicio."-".$mesesTrimestre[$indice][0];
                 $fechaFin=$fechaFin."-".$mesesTrimestre[$indice][1];
                 if($mesesTrimestre[$indice][1]==3){
                    $hastaQueDiaTiene=31;
                 }
                 if($mesesTrimestre[$indice][1]==6){
                    $hastaQueDiaTiene=30;
                 }
                 if($mesesTrimestre[$indice][1]==9){
                    $hastaQueDiaTiene=30;
                 }
                 if($mesesTrimestre[$indice][1]==12){
                    $hastaQueDiaTiene=31;
                 }
            }
            if($i==0){
                $fechaInicio=$fechaInicio."-"."1";
                $fechaFin=$fechaFin."-".strval($hastaQueDiaTiene);
            }
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$q,$fechaInicio,$fechaFin);
        // $dataProvider->query->where(['between', 'turno.fecha_turno',]);
         $columnas=Metodos::obtenerColumnas($model,'mpi/turno/index');
         $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                     'view'=>Seguridad::tienePermiso('view'),
                     'create'=>Seguridad::tienePermiso('create'),
                     'update'=>Seguridad::tienePermiso('update'),
                     'delete'=>Seguridad::tienePermiso('delete'),
                     'export'=>Seguridad::tienePermiso('export'),
                     'exportall'=>Seguridad::tienePermiso('exportall'),
                     'select'=>Seguridad::tienePermiso('select'),
                     'list'=>Seguridad::tienePermiso('list'),
                     'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
                     
         ];
         return $this->render('index', [
             "trimestres"=>$mesesTrimestre,
             "indice"=>$indice,
             "indiceMatriz"=>2,
             "anio"=>$anio,
             'searchModel' => $searchModel,
             'dataProvider' => $dataProvider,
             'permisos'=>$permisos,
             'columns'=>$columnas,
         ]);
    }

public function actionReporte(){
    $searchModel=new TurnoSearch();
    $provider=$searchModel->reporteRangoEdad(Yii::$app->request->queryParams);
    $columna=[
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'provincia',
            'value'=>'codprov',
        ],
        [
             'class'=>'\kartik\grid\DataColumn',
             'attribute'=>'codigo sisa',
             'value'=>'codigo_sisa',
         ],
         [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'anio consulta',
            'value'=>'anioconsul',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
           'attribute'=>'mes consulta',
            'value'=>'mesconsul',
       ],
         [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'codigo operat',
            'value'=>'coduoperat',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'masculino',
            'value'=>'masculino',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'femenino',
            'value'=>'femenino',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'rango edad',
            'value'=>'rango_edad',
        ],
        ];
        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
        'view'=>Seguridad::tienePermiso('view'),
        'create'=>Seguridad::tienePermiso('create'),
        'update'=>Seguridad::tienePermiso('update'),
        'delete'=>Seguridad::tienePermiso('delete'),
        'export'=>Seguridad::tienePermiso('export'),
        'select'=>Seguridad::tienePermiso('select'),
        'list'=>Seguridad::tienePermiso('list'),
        'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
];
        return $this->render('indexreporte', [ 
            'dataProvider'=>$provider,
            'permisos' => $permisos,
            'columns' => $columna,
        ]); 
}
public function actionExportreporte()
{
   $query=(new \yii\db\Query())->select(["p2.codigo as codprov,sexo.nombre,e.codigo_sisa,extract(YEAR FROM t.fecha_turno) as anioconsul,extract(month FROM t.fecha_turno) as mesconsul,ps.codigo as coduoperat,
  CASE
  WHEN u.tipo_unidad = 2 or u.tipo_unidad= 3 or u.tipo_unidad=4 or u.tipo_unidad=5  THEN '<1'
  WHEN t.edad >= 1 and t.edad <= 4 and u.tipo_unidad= 1 THEN '1-4'
  WHEN  t.edad >= 5 and  t.edad <= 9 and u.tipo_unidad= 1 THEN '5-9'
  WHEN   t.edad >= 10 and  t.edad <= 14 and u.tipo_unidad=1 THEN '10-14'
  WHEN   t.edad  >= 15 and  t.edad <= 19 and u.tipo_unidad=1  THEN '15-19'
  WHEN t.edad  >= 20 and  t.edad <=24 and u.tipo_unidad=1   THEN '20-24'
  WHEN  t.edad  >= 25 and  t.edad <=34 and u.tipo_unidad=1  THEN '25-34'
  WHEN  t.edad  >= 35 and  t.edad <=44 and u.tipo_unidad=1  THEN '35-44'
  WHEN  t.edad  >= 45 and  t.edad <=49 and u.tipo_unidad=1  THEN '45-49'
  WHEN  t.edad  >= 50 and  t.edad <=64 and u.tipo_unidad=1  then '50-64'
  WHEN  t.edad  >= 65 and  t.edad <=74 and u.tipo_unidad=1  then '65-74'
  WHEN  t.edad  >= 75 and u.tipo_unidad=1  then '>75'
  else
      'S/E'
 
 END AS rango_edad
   "])->from('consultasestadisticas.turno t')->join('INNER JOIN','consultasestadisticas.paciente p','p.id=t.id_paciente')->join('INNER JOIN','consultasestadisticas.establecimiento e','e.id=t.id_area')
   ->join('INNER JOIN','consultasestadisticas.p_servicio ps','ps.id=t.id_servicio')->join('INNER JOIN','consultasestadisticas.sexo sexo','sexo.id=p.id_sexo')->join('INNER JOIN','consultasestadisticas.localidad l','l.id=e.id_localidad')
   ->join('INNER JOIN','consultasestadisticas.provincia p2','p2.id=l.id_provincia')->join('INNER JOIN','consultasestadisticas.unidad_edad u','u.id=t.id_unidad_edad')->groupBy('p2.codigo,e.codigo_sisa,anioconsul,mesconsul,coduoperat,t.edad,u.tipo_unidad,sexo.nombre');
   $query2=$query->createCommand()->queryAll();
 $columnas=[];
   foreach($query2 as $q){
$columna=[
    $q['codprov'],$q['codigo_sisa'], 
    $q['anioconsul'],$q['mesconsul'],$q['coduoperat'],
    $q['masculino'],$q['femenino'],$q['rango_edad'],
    ];
    array_push($columnas,$columna);
   }
   $csv = 'CODPROV,CODEST,ANIOCONSUL,MESCONSUL,CODUOPERAT,VARONES,MUJERES,RANGOEDAD';
   foreach($columnas as $columna)
       $csv .= "\n".implode(',', $columna);
  /* header("Content-type: text/csv");
   header("Content-Disposition: attachment; filename=file.csv");*/
   return \Yii::$app->response->sendContentAsFile($csv, 'reporte.csv', [
    'mimeType' => 'text/csv', 
    'inline'   => false
]);
 }

 /**
     * Creates a new Paciente model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionHistoria(){
        Yii::$app->response->format = Response::FORMAT_JSON;
         $id = key($_POST);
        $historia  = HistoriaClinica::find()
        ->joinWith('paciente',true,'LEFT JOIN')
        ->where(['historia_clinica.id_paciente'=> $id])
        ->one();
        if($historia==null || empty($historia->historia_clinica)){
            return ['status'=>'error'];
        }
        return ['status'=>'ok',
        'historia'=>$historia];
     }
     /*public function actionAllexport($indice,$anio){
    
        $fechaInicio=null;
        $fechaFin=null;
        if($indice==1){
        $fechaInicio=1;
        $fechaFin=3;
    }
        if($indice==2 ){
        $fechaInicio=4;
        $fechaFin=6;
        }
        if($indice==3 ){
        $fechaInicio=7;
        $fechaFin=9;
    }
        if($indice==4){
        $fechaInicio=10;
        $fechaFin=12;
    }
   

        $query=(new \yii\db\Query())->select(["t.numero as numero","t.control_embarazo as control_embarazo","t.nino_sano as ninio_sano" ,"t.primera_vez as primera_vez","t.presente as presente","t.fecha_turno as fecha_turno","t.observacion as observacion","t.edad as edad","t.ubicacion_hc as ubicacion_hc","p.nombre as paciente_nombre","e.nombre as nombre_establecimiento"
        ,"e.codigo_sisa as codigo_sisa","p.nombre as paciente_nombre","ps.nombre servicio_nombre","ps.codigo as codigo_servicio","u.descripcion descripccion_unidad","d.nombre as diagnostico","pr.nombre as profesional_nombre","pr.apellido as profesional_apellido","es.nombre especialidad_nombre"])->from('consultasestadisticas.turno t')
        ->join('LEFT JOIN','consultasestadisticas.paciente p','p.id=t.id_paciente')->join('LEFT JOIN','consultasestadisticas.establecimiento e','e.id=t.id_area')
        ->join('LEFT JOIN','consultasestadisticas.p_servicio ps','ps.id=t.id_servicio')->join('LEFT JOIN','consultasestadisticas.localidad l','l.id=e.id_localidad')
        ->join('LEFT JOIN','consultasestadisticas.provincia p2','p2.id=l.id_provincia')->join('LEFT JOIN','consultasestadisticas.unidad_edad u','u.id=t.id_unidad_edad')
        ->join('LEFT JOIN','consultasestadisticas.diagnostico d','d.id=t.id_diagnostico')->join('LEFT JOIN','consultasestadisticas.profesional pr','p.id=t.id_profesional')
        ->join('LEFT JOIN','consultasestadisticas.especialidad es','es.id=t.id_especialidad')->where(['between', 'extract(month FROM t.fecha_turno)', $fechaInicio, $fechaFin])->andWhere(['extract(YEAR FROM t.fecha_turno)'=>$anio]);
        //->groupBy('p2.codigo,e.codigo_sisa,anioconsul,mesconsul,coduoperat,t.edad,u.tipo_unidad,p.sexo');
   $query2=$query->createCommand()->queryAll();
  
   $csv = "NUMERO_TURNO,CONTROL_EMBARAZO,NINIO_SANO,PRIMERA_VEZ,PRESENTE,FECHA_TURNO,OBSERVACION,EDAD,UBICACION_HC,PACIENTE_NOMBRE,NOMBRE_ESTABLECIMIENTO,CODIGO_SISA,PACIENTE_APELLIDO,SERVICIO_NOMBRE,CODIGO_SERVICIO,DESCRIPCCION_UNIDAD,DIAGNOSTICO,PROFESIONAL_NOMBRE,PROFESIONAL_NOMBRE,PROFESIONAL_APELLIDO,ESPECIALIDAD_NOMBRE";
   foreach($query2 as $columna)
   $csv .= "\n".implode(',', $columna);



 Yii::$app->response->sendContentAsFile($csv, 'Export_turno.csv', [
    'mimeType' => 'application/csv', 
    'inline'   => true
]);
Yii::$app->end();

}*/
public function actionCreatepaciente()
{
    $request = Yii::$app->request;
    $model = new Paciente();
    $model->id_obra_social = 1;

    $modelHistoriaClinica = new HistoriaClinica();
    $sistemaEducativo = new SistemaEducativo();
    $contacto = new Contacto();
    $localidad = new Localidad();

    $parametroOS = ObraSocial::find()->all();
    $listaOS = ArrayHelper::map($parametroOS, 'id', 'nombre');

    $parametroEstablecimiento = Establecimiento::find()->joinWith(['localidad'])->all();
    $parametroLocalidad = Localidad::find()->all();

    $q = UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
    if ((Yii::$app->user->identity->username != 'administrador' && $q != 'CENTRAL') && ($q != '' || $q != null)) {
        $parametroEstablecimiento = Establecimiento::find()
            ->joinWith(['localidad'])
            ->joinWith(['localidad.usuarioEstadistica'])
            ->where(['usuario_estadistica.id_usuario' => Yii::$app->user->identity->id])
            ->all();
    }

    $listaEstablecimiento = ArrayHelper::map($parametroEstablecimiento, 'id', 'nombre');

    if ($request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load($request->post())) {
            $sistemaEducativo->load($request->post());
            $model->id_sistema_educativo = $sistemaEducativo->elegirId();

            try {
                $contacto->numero_telefono = trim($request->getBodyParam('Contacto')['numero_telefono']);
                $contacto->save();
                $model->id_contacto = $contacto->id;

                if ($model->save()) {
                    $modelHistoriaClinica->id_paciente = $model->id;
                    $modelHistoriaClinica->historia_clinica = $request->getBodyParam('HistoriaClinica')['historia_clinica'];
                    $modelHistoriaClinica->id_establecimiento = $model->id_establecimiento;

                    if ($modelHistoriaClinica->save()) {
                        $content = '<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Paciente #" . $model->id,
                            'content' => $content,
                        ];
                    }
                }
            } catch (yii\db\Exception $e) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de base de datos.', 500);
            }
        }

        return [
            'title' => "Crear Paciente",
            'content' => $this->renderAjax('@app/modules/mpi/views/paciente/_form', [
                'model' => $model,
                'documento_ajeno' => $model->documento_ajeno,
                'listaOS' => $listaOS,
                'establecimiento' => $listaEstablecimiento,
                'modelHistoria' => $modelHistoriaClinica,
                'listaLocalidad' => [],
                'contacto' => $contacto,
                'localidad' => $localidad,
                'sistemaEducativo' => $sistemaEducativo,
            ]),
            'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"]),
        ];
    } else {
        // Process for non-ajax request
        return $this->redirect(['index']);
    }
}

   
    /**
     * Displays a single Turno model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView();
        // soporte de enter para el caso de un campo textarea para view y pdf. por ejemplo campo observaciones.
        // en el $model->attributeView, usamos formato html (campo_textarea:html) y reemplazo enter por <br> en el modelo
        // $model->(campo_textarea)=str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"),"<br>",$model->(campo_texarea);
        if(isset($_GET['pdf'])){
            $titulo='Datos de Turno';
            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=> "Turno #".$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "IECMA #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $model,
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
   
    /**
     * Creates a new Turno model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $listaServicios=null;
        $listaPaciente=null;
        $listaProfesional=null;
        $listaDiagnostico=null;
        $request = Yii::$app->request;
        $model = new Turno();  
        $historia = new HistoriaClinica();
        $esUpdate=false;
        $q=UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
        $parametroArea = Establecimiento::find()->joinWith(['localidad'])->all();
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null))
            $parametroArea=Establecimiento::find()->joinWith(['localidad'])->joinWith(['localidad.usuarioEstadistica'])->where(['usuario_estadistica.id_usuario' =>Yii::$app->user->identity->id])->all();  
        $listaAreas= ArrayHelper::map( $parametroArea , 'id', 'nombre');
        $parametroEdad = Edad::find()->all();                            
        $listaEdad= ArrayHelper::map( $parametroEdad , 'id', 'descripcion');
        $parametroEspecialidad = Especialidad::find()->all();
        $listaEspecialidad= ArrayHelper::map($parametroEspecialidad,'id','nombre');
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
            if($model->load($request->post())){
                try {
                    $result=null;
                    $historia->historia_clinica=$_POST["HistoriaClinica"]['historia_clinica'];
                    $historia->id_paciente=$model->getAttributes()['id_paciente'];
                    $paciente=Paciente::findOne($model->getAttributes()['id_paciente']);
                    $historia->id_establecimiento=$paciente->id_establecimiento;
                     if($model->getAttributes()['fecha_turno']!=null){
                     $result=$model->verificarFechaSeaCorrecta($model->getAttributes()['fecha_turno']);
                    if($result=="errorPasado"){
                        Yii::$app->response->format = Response::FORMAT_HTML;
                        throw new NotFoundHttpException('Error de: '.$result,500);
                    }
                    if($result=="errorFuturo"){
                        Yii::$app->response->format = Response::FORMAT_HTML;
                        throw new NotFoundHttpException('Error de: '.$result,500);
                    }
                }   
                if( $model->save()){
                    $historia->save();
                    $content='<span class="text-success">Registro guardado correctamente</span>';
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "IECMA #".$model->id,
                        'content'=>$content,
                    ];
                }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
            return [
                'title'=> "Crear IECMA",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                    'historia'=> $historia,
                    'listaAreas' =>$listaAreas,
                    'listaServicios' =>$listaServicios,
                    'listaProfesional' =>$listaProfesional,
                    'listaEspecialidad'=>$listaEspecialidad,
                    'listaPaciente' =>$listaPaciente,
                    'listaEdad' =>$listaEdad,
                    'listaDiagnostico' =>$listaDiagnostico,
                    'esUpdate'=>$esUpdate
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit",'id' => 'button-guardar'])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    public function actionCreate2(){
    try {
        $historia = new HistoriaClinica();
        $modelTurno = new Turno();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelTurno->load(Yii::$app->request->post());
            $result=null;
            $historia->historia_clinica=$_POST["HistoriaClinica"]['historia_clinica'];
            $historia->id_paciente=$modelTurno->getAttributes()['id_paciente'];
            $paciente=Paciente::findOne($modelTurno->getAttributes()['id_paciente']);
            $historia->id_establecimiento=$paciente->id_establecimiento;
            $result=$modelTurno->verificarFechaSeaCorrecta($modelTurno->getAttributes()['fecha_turno']);
            if($result!=null){
                if($result=="errorPasado"){
                    return ['status' => 'errorAnio'];
                }
                if($result=="errorFuturo"){
                    return ['status' => 'errorFuturo'];
                }
                if($result=='ok'){
                    if($modelTurno->save()){
                        $historia->save();
                        return ['status' => 'ok'];
                    }
                }
            }
                if($modelTurno->save()){
                    $historia->save();
                    return ['status' => 'ok'];
                }
                else{
                    return ['status' => 'error'];
                }
    }catch (Exception $e) {
        Yii::$app->response->format = Response::FORMAT_HTML;
        throw new Exception($e."sadas", 500);
    }
       
    }
    /**
     * Updates an existing Turno model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);   
        $esUpdate=true;
        $q=UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
        $historia= HistoriaClinica::find()
        ->where(['historia_clinica.id_paciente'=> $model->id_paciente])
        ->andWhere(['historia_clinica.id_establecimiento'=> $model->paciente->id_establecimiento])
        ->andWhere(['not', ['historia_clinica.historia_clinica' => null]])
        ->one();
        if($historia==null){
            $historia=new HistoriaClinica();
        }
        
        $parametroArea = Establecimiento::find()->joinWith(['localidad'])->all();
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null))
            $parametroArea=Establecimiento::find()->joinWith(['localidad'])->joinWith(['localidad.usuarioEstadistica'])->where(['usuario_estadistica.id_usuario' =>Yii::$app->user->identity->id])->all();
        $listaAreas= ArrayHelper::map( $parametroArea , 'id', 'nombre');
        $parametroServicio = PServicio::find()->where(['id'=>$model->id_servicio])->one();                          
        $listaServicios=array();
        if($parametroServicio!=null)
            $listaServicios[$parametroServicio->id]=$parametroServicio->nombre;
        $parametroEspecialidad = Especialidad::find()->where(['id'=>$model->id_especialidad])->one();                          
        $listaEspecialidad=array();
        if($parametroEspecialidad!=null)
            $listaEspecialidad[$parametroEspecialidad->id]=$parametroEspecialidad->nombre; 
        $parametroProfesional = Profesional::find()->where(['id'=>$model->id_profesional])->one();   
        $listaProfesional=array();
        if($parametroProfesional!=null)
            $listaProfesional[$parametroProfesional->id]=$parametroProfesional->nombre.','.$parametroProfesional->apellido;
        $parametroPaciente = Paciente::find()->where(['id'=>$model->id_paciente])->one();
        $listaPaciente=array();
        if($parametroPaciente!=null)
            $listaPaciente[$parametroPaciente->id]=$parametroPaciente->nombre.','.$parametroPaciente->apellido;
        $parametroDiagnostico = Diagnostico::find()->where(['id'=>$model->id_diagnostico])->one();      
        $listaDiagnostico=array();
        if($parametroDiagnostico!=null)
            $listaDiagnostico[$parametroDiagnostico->id]=$parametroDiagnostico->nombre.','.$parametroDiagnostico->codigo;
        $parametroEdad = Edad::find()->all();                            
        $listaEdad= ArrayHelper::map( $parametroEdad , 'id', 'descripcion');
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post())){
                try {
                    $result=null;
                    $historia->historia_clinica=Yii::$app->request->getBodyParam('HistoriaClinica')['historia_clinica'];
                    $historia->id_paciente=$model->getAttributes()['id_paciente'];
                    $paciente=Paciente::findOne($model->getAttributes()['id_paciente']);
                    $historia->id_establecimiento=$paciente->id_establecimiento;
                     $result=$model->verificarFechaSeaCorrecta($_POST['Turno']['fecha_turno']);
                     if($result!=null){
                    if($result=="errorPasado"){
                        Yii::$app->response->format = Response::FORMAT_HTML;
                        throw new NotFoundHttpException('Error de: '.$result,500);
                    }
                    if($result=="errorFuturo"){
                        Yii::$app->response->format = Response::FORMAT_HTML;
                        throw new NotFoundHttpException('Error de: '.$result,500);
                    }
                }
                    if( $model->save() ){
                        $historia->save();
                        
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "IECMA #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
            return [
                'title'=> "Editar IECMA #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                    'historia'=> $historia,
                    'listaAreas' =>$listaAreas,
                    'listaServicios' =>$listaServicios,
                    'listaProfesional' =>$listaProfesional,
                    'listaPaciente' =>$listaPaciente,
                    'listaEdad' =>$listaEdad,
                    'listaDiagnostico' =>$listaDiagnostico,
                    'listaEspecialidad' =>$listaEspecialidad,
                    'esUpdate'=>$esUpdate
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit",'id' => 'button-guardar'])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    /**
     * Delete an existing Turno model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($model->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de base de datos.',500);
            }
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    /**
     * Finds the Turno model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Turno the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Turno::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }
     /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null)
    {
        $request = Yii::$app->request;
        $reporte= new Reporte();
  
        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
           
                $reporte->attributes=$_POST['Reporte'];
                $tipo_archivo=$_POST['tipo_archivo'];
                \app\components\Listado\Listado::widget(array(
                    'reporte'=>$reporte,
                    'tipo'=>$tipo_archivo
                ));
                $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;
                Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
                Yii::$app->end();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $dataProvider=Yii::$app->session->get('turno-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);
            //$dataProvider->array_push
            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new Turno();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];
                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }
                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }
                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);
                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);
            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    /**
     * Accion List, utilizada cuando se hace una busqueda autocomplete de este modelo
     * en query->select , verificar el campo de  busqueda
     * @param string de busqueda $q
     * @return arrary de resultados $results
     */
    public function actionList($q = null)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $q="%".strtolower(addslashes($q))."%";
            $sql = "SELECT id as id, nombre as value
                    FROM consultasestadisticas.turno                    
                    WHERE lower(turno.nombre) LIKE '$q'
                    LIMIT 50";

            $query= Yii::$app->db->createCommand($sql);
            $data = $query->queryAll();
            $results = array_values($data); 
            return $results;
            
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    public function actionListedad($q=null){
        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            $q="%".strtolower(addslashes($q))."%";

            $sql = "SELECT id as id, descripcion  as value
                    FROM consultasestadisticas.unidad_edad                    
                    WHERE lower(unidad_edad.descripcion) LIKE '$q'
                    LIMIT 50";

            $query= Yii::$app->db->createCommand($sql);
            $data = $query->queryAll();
            $results = array_values($data); 
            return $results;
            
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    public function actionIndexdetalle() 
    {   
        $request = Yii::$app->request; 
  
        if($request->isAjax){ 
            $permisos=[ 'indexdetalle' => Seguridad::tienePermiso('indexdetalle'), 
//                        'exportdetalle' => Seguridad::tienePermiso('exportdetalle'), 
            ];
            if ( isset($id_turno) || isset($_POST['expandRowKey']) ) {   
                    
                if (!isset($id_turno))
                    $id_turno=$_POST['expandRowKey']; 
                $verificarTurno=Turno::findOne($id_turno); 
                $paciente= Paciente::findOne($verificarTurno->id_paciente);
                $searchModel = new PacienteSearch(); 
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->where(['paciente.id' => $paciente->id]); 
                $dataProvider->setPagination(false); 
                $columnas= [
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'Nombre',
                        'value'=>'nombre',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'Apellido',
                        'value'=>'apellido',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'Numero de documento',
                        'value'=>'numero_documento',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'Sexo',
                        'value'=>'idsexo.nombre',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'Obra social',
                        'value'=>'obrasocial.nombre',
                        // soporte sort, debe estar el atributo en el Search
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'label'=>'localidad',
                        'value'=>'localidad.nombre',
                        // soporte sort, debe estar el atributo en el Search
                    ],    
                 ];
                if ($dataProvider) {
                        return $this->renderPartial('indexdetalle', [ 
                        'id_paciente' => $verificarTurno->id_paciente, 
                        'searchModel' => $searchModel, 
                        'dataProvider' => $dataProvider,
                        'permisos' => $permisos,
                        'columns' => $columnas,
                    ]); 
                }else{
                    return '<div>No se encontraron resultados</div>'; 
                }
            }
        }else{ 
            // Process for non-ajax request 
            return $this->redirect(['index']); 
        } 
    } 
    public function actionViewdetalle($id_paciente) 
    { 
        $request = Yii::$app->request;
        $modelPaciente = Paciente::findOne($id_paciente); 
        $attributes = $modelPaciente->attributeView(); 
        if (isset($_GET['pdf'])) { 
            $titulo = 'Datos de paciente'; 
            \app\components\Viewpdf\Viewpdf::widget(array( 
                'titulo' => $titulo, 
                'subtitulo' => 'Paciente #' . $id_paciente, 
                'data' => $modelPaciente, 
                'attributes' => $attributes, 
                'resumen' => '', 
            )); 
            $fileName = preg_replace("/ /", "_", $titulo) . ".pdf"; 
            Yii::$app->response->sendFile('../runtime/' . $fileName)->send(); 
            Yii::$app->end();
        } 
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
            return [ 
                'title' => "paciente #" . $id_paciente, 
                'content' => $this->renderAjax('@app/components/Vistas/_view', [ 
                    'model' => $modelPaciente, 
                    'attributes' => $attributes, 
                ]), 
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                (Seguridad::tienePermiso('viewdetalle')?Html::a('
                <i class="glyphicon glyphicon-pencil"></i>', ['paciente/update', 'id' => $id_paciente],
                ['role'=>'modal-remote','title'=> 'Editar registro','class'=>'btn btn-default']):"").
                (Seguridad::tienePermiso('viewdetalle')?Html::a('Imprimir',['viewdetalle','id_paciente'=>$id_paciente,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                '<iframe src="" style="display:none" name="exportFrame"/>' 
            ]; 
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    } 
    public function actionDeletedetalle($id_paciente) 
    { 
        $request = Yii::$app->request;
        $modelUsuarioGrupo = Paciente::findOne(['id_grupo'=>$id_paciente]);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            try { 
                if ($modelUsuarioGrupo->delete()){ 
                    // borro registro en este caso por que es una relacion NN 
                    return ['forceClose' => true, 'success' => 'reloadDetalle('.$id_paciente.')'];   
                } 
            } catch (yii\db\Exception $e) { 
                return ['forceClose' => false, 
                    'title' => '<p style="color:red">ERROR</p>', 
                    'content' => '<div style="font-size: 18px">Error en la acción realizada</div><div style="font-size: 14px">Error de base de datos.</div>',
                    'success' => 'reloadDetalle('.$id_paciente.')']; 
            } 
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }    
    } 
}
