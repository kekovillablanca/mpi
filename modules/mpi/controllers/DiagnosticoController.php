<?php

namespace app\modules\mpi\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
//
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;
use app\modules\mpi\models\Diagnostico;
use app\modules\mpi\models\DiagnosticoSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use DateTime;
use yii\helpers\Json;

/**
 * DiagnosticoController implements the CRUD actions for Diagnostico model.
 */
class DiagnosticoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select','list'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select','list'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Diagnostico models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $model= new Diagnostico();
        $searchModel = new DiagnosticoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columnas=Metodos::obtenerColumnas($model,'mpi/diagnostico/index');
        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'list'=>Seguridad::tienePermiso('list'),
        ];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    }


    /**
     * Displays a single Diagnostico model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView();
        // soporte de enter para el caso de un campo textarea para view y pdf. por ejemplo campo observaciones.
        // en el $model->attributeView, usamos formato html (campo_textarea:html) y reemplazo enter por <br> en el modelo
        // $model->(campo_textarea)=str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"),"<br>",$model->(campo_texarea);
        if(isset($_GET['pdf'])){
            $titulo='Datos de Diagnostico';
            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=> "Diagnostico #".$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Diagnostico #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $model,
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    //(Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Diagnostico model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Diagnostico();  
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Diagnostico #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
            return [
                'title'=> "Crear Diagnostico",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing Diagnostico model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Diagnostico #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
            return [
                'title'=> "Editar Diagnostico #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing Diagnostico model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($model->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('El registro se esta utilizando.',500);
            }
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Diagnostico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Diagnostico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Diagnostico::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }
    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null)
    {
        $request = Yii::$app->request;
        $reporte= new Reporte();
        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];
            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();

        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $dataProvider=Yii::$app->session->get('diagnostico-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);
            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new Diagnostico();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];
                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }
                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }
                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);
                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }
            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);
            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    /**
     * Accion List, utilizada cuando se hace una busqueda autocomplete de este modelo
     * en query->select , verificar el campo de  busqueda
     * @param string de busqueda $q
     * @return arrary de resultados $results
     */
    public function actionList($q = null)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!is_null($q)) {
                $q="'%".strtolower(addslashes($q))."%'";
                $sql="SELECT d.id, CONCAT(trim(d.codigo),' | ',trim(d.nombre)) as text 
                        FROM consultasestadisticas.diagnostico as d
                        WHERE (lower(d.nombre) LIKE $q)
                        OR (lower( CONCAT(trim(d.nombre),' | ',trim(d.codigo))) LIKE $q)
                        ORDER BY  CONCAT(trim(d.nombre),' | ',trim(d.codigo))
                        LIMIT 50";
                $query= Yii::$app->db->createCommand($sql);
                $data = $query->queryAll();
                $results['results'] = array_values($data);
            }
            return $results;
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionListfilter($q=null,$sexo = null,$fecha_nacimiento=null)
    {
        $request = Yii::$app->request;

        if($request->isAjax){
            /*$nacimiento = DateTime::createFromFormat('d/m/Y', $fecha_nacimiento);
            $hoy = new DateTime();
            $edad = $hoy->diff($nacimiento);
            $anios = $edad->y;
            $meses = $edad->m;
            $allResults = [];*/
            Yii::$app->response->format = Response::FORMAT_JSON;
        
            if (!is_null($q)) {
                $q = "'%" . strtolower(addslashes($q)) . "%'";
                $sql = "SELECT d.id, CONCAT(TRIM(d.codigo), ' | ', TRIM(d.nombre)) AS text 
                        FROM consultasestadisticas.diagnostico AS d 
                        WHERE (LOWER(d.nombre) LIKE $q 
                        OR LOWER(CONCAT(TRIM(d.nombre), ' | ', TRIM(d.codigo))) LIKE $q)"; // Base de la consulta
                // Condiciones basadas en los años y meses
                /*if ($anios == 0 && $meses > 0 && $meses <= 12) {
                    $sql .= " AND d.codigo LIKE '%P00%' ";
                } elseif ($anios < 15) {
                    $sql .= " AND d.codigo NOT LIKE '%J40X%' ";
                } elseif ($anios >= 15) {
                    $sql .= " AND d.codigo NOT BETWEEN 'J200' AND 'J219' ";
                }*/
        
                // Condición de sexo
                if ($sexo == 1) {
                    $sql .=" AND (d.codigo NOT LIKE 'O%' 
                        AND LENGTH(d.codigo) = 4
                        AND LOWER(d.nombre) NOT LIKE '%embarazo%') "; // Cerrar el paréntesis aquí

                }

                // Orden y límite
                $sql .= " ORDER BY CONCAT(TRIM(d.nombre), ' | ', TRIM(d.codigo)) 
                        LIMIT 50";
        
                // Ejecutar la consulta
                $query = Yii::$app->db->createCommand($sql);
                $data = $query->queryAll();
                $results['results'] = array_values($data);
         
            }
 
            return $results;
        
        } else {
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
        
    }
    public function actionIsembarazo($id_diagnostico){
        $isEmbarazo=false;
        if($id_diagnostico!=null){        
            $model=$this->findModel($id_diagnostico);
            if(strpos($model->codigo, 'O') == true)
                $isEmbarazo=true;

        }

       return  Json::encode(['result' => $isEmbarazo]);
    }
}
