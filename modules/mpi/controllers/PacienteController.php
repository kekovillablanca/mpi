<?php

namespace app\modules\mpi\controllers;

use app\modules\admin\controllers\UsuarioestadisticaController;
use app\modules\mpi\models\HistoriaClinica;
use app\modules\mpi\models\HistoriaClinicaSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
//
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;
use app\modules\mpi\models\Paciente;
use app\modules\mpi\models\PacienteSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\modules\internacion\models\SistemaEducativo;
use app\modules\mpi\models\Contacto;
use app\modules\mpi\models\Establecimiento;
use app\modules\mpi\models\Genero;
use app\modules\mpi\models\Localidad;
use app\modules\mpi\models\Maestro;
use app\modules\mpi\models\ObraSocial;
use Exception;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\NotAcceptableHttpException;

/**
 * PacienteController implements the CRUD actions for Paciente model.
 */
class PacienteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select','list'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select','list'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }
    /*private function obtenerUsuarioPorAreaReal(){
        $sql = "SELECT l.nombre FROM consultasestadisticas.localidad l  JOIN sies.usuario_estadistica ue  ON(l.id=ue.id_localidad) JOIN public.usuario u  on (u.id=ue.id_usuario)
        where u.id=".Yii::$app->user->identity->id;
        $query= Yii::$app->db->createCommand($sql);
        $data = $query->queryAll();
        $q=null;
        $results = array_values($data); 
        if (isset($results[0])){
            $localidad=$results[0]["nombre"];
        $q=strtoupper(addslashes(trim($localidad)));
        }
        return $q;
    }*/
    /**
     * Lists all Paciente models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $model= new Paciente();
        $searchModel = new PacienteSearch();
  
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $columnas=Metodos::obtenerColumnas($model,'mpi/paciente/index');
        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'list'=>Seguridad::tienePermiso('list'),
                    'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
                    'viewdetalle' => Seguridad::tienePermiso('viewdetalle'),
        ];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    }
    /**
     * Displays a single Paciente model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView();
        if(isset($_GET['pdf'])){
            $titulo='Datos de Paciente';
            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=> "Paciente #".$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Paciente #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $model,
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    public function actionDeletedetalle($id_historia) 
    { 
        $request = Yii::$app->request;
        $model = HistoriaClinica::findOne(['id'=>$id_historia]);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            try { 
                if ($model->delete()){ 
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                } 
            } catch (yii\db\Exception $e) { 
                return ['forceClose' => false, 
                    'title' => '<p style="color:red">ERROR</p>', 
                    'content' => '<div style="font-size: 18px">Error en la acción realizada</div><div style="font-size: 14px">Error de base de datos.</div>',
                    'success' => 'reloadDetalle('.$id_historia.')']; 
            } 
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }    
    } 
    /**
     * Creates a new Paciente model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Paciente();  
        $model->id_obra_social = 1;
        $modelHistoriaClinica=new HistoriaClinica();
        $contacto=new Contacto();
        $localidad=new Localidad();
        $sistemaEducativo=new SistemaEducativo();
        $parametroOS = ObraSocial::find()->all();                            
        $listaOS= ArrayHelper::map( $parametroOS , 'id', 'nombre');
        $parametroEstablecimiento =  Establecimiento::find()->joinWith(['localidad'])->all();
        $parametroLocalidad=  Localidad::find()->all();

        $q=UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null)){
           $parametroEstablecimiento=Establecimiento::find()->joinWith(['localidad'])->joinWith(['localidad.usuarioEstadistica'])->where(['usuario_estadistica.id_usuario' =>Yii::$app->user->identity->id])->all();

        }
        $listaEstablecimiento= ArrayHelper::map( $parametroEstablecimiento , 'id', 'nombre');
   
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if( $model->load($request->post())){
                $model->validate();
                $sistemaEducativo->load($request->post());
                if($sistemaEducativo->validate())
                $model->id_sistema_educativo=$sistemaEducativo->elegirId();
                try {
                if($model->numero_documento==9999999 || $model->numero_documento==99999999){
                    $contacto->numero_telefono=trim(Yii::$app->request->getBodyParam('Contacto')['numero_telefono']);
                    $contacto->save();
                    $model->id_contacto=$contacto->id;
                    if($model->save(false)){
                        $modelHistoriaClinica->id_establecimiento=$model->id_establecimiento;
                        $modelHistoriaClinica->historia_clinica=trim(Yii::$app->request->getBodyParam('HistoriaClinica')['historia_clinica']);
                        $modelHistoriaClinica->id_paciente=$model->id;
                        if($modelHistoriaClinica->save()){
                            $content='<span class="text-success">Registro guardado correctamente con historia clinica</span>';
                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "Paciente #".$model->id,
                                'content'=>$content,
                            ];
                        }
                    }
                }
                else{
                  
                    $contacto->numero_telefono=trim(Yii::$app->request->getBodyParam('Contacto')['numero_telefono']);
                    $contacto->save();
                    $model->id_contacto=$contacto->id;
                    if($model->save()){
                        $modelHistoriaClinica->historia_clinica=trim(Yii::$app->request->getBodyParam('HistoriaClinica')['historia_clinica']);
                        $modelHistoriaClinica->id_establecimiento=$model->id_establecimiento;
                        $modelHistoriaClinica->id_paciente=$model->id;
                    if($modelHistoriaClinica->save()){
                    $content='<span class="text-success">Registro guardado correctamente</span>';
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Paciente #".$model->id,
                        'content'=>$content,
                    ];
                    }
            }
            }
                }catch (yii\db\Exception $e ) {
 
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
                catch (NotAcceptableHttpException $e) {
 
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotAcceptableHttpException($e->getMessage(),500);
                }
            }
            return [
                'title'=> "Crear Paciente",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                    'documento_ajeno' => $model->documento_ajeno,
                    'modelHistoria' =>$modelHistoriaClinica,
                    'listaOS' =>$listaOS,
                    'establecimiento'=>$listaEstablecimiento,
                    'listaLocalidad'=>[],
                    'contacto'=>$contacto,
                    'localidad'=>$localidad,
                    'sistemaEducativo'=>$sistemaEducativo
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    //meterle mano a esta parte jee yo keko
    public function actionViewdetalle($id_historia) 
    { 
        $request = Yii::$app->request;
     
        $model = HistoriaClinica::findOne($id_historia); 
        $attributes = $model->attributeView(); 
        if (isset($_GET['pdf'])) { 
            $titulo = 'Datos de historia clinica'; 
            \app\components\Viewpdf\Viewpdf::widget(array( 
                'titulo' => $titulo, 
                'subtitulo' => 'historia #' . $model->historia_clinica, 
                'data' => $model, 
                'attributes' => $attributes, 
                'resumen' => '', 
            )); 
            $fileName = preg_replace("/ /", "_", $titulo) . ".pdf"; 
            Yii::$app->response->sendFile('../runtime/' . $fileName)->send(); 
            Yii::$app->end();
        } 
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [ 
                'title' => "historia #" . $id_historia, 
                'content' => $this->renderAjax('@app/components/Vistas/_view', [ 
                    'model' => $model, 
                    'attributes' => $attributes, 
                ]), 
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]).
                (Seguridad::tienePermiso('view')?Html::a('Imprimir',['viewdetalle','id_historia'=>$id_historia,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"")
            ]; 
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    } 
    public function actionIndexdetalle($id_paciente=null) 
    { 
      
        $request = Yii::$app->request; 
        if($request->isAjax){ 
            $permisos=[ 'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),     
            ];
            if ( isset($id_paciente) || isset($_POST['expandRowKey']) ) { 
                if (!isset($id_paciente))
                $id_paciente=$_POST['expandRowKey'];
                $verificarPaciente=Paciente::findOne($id_paciente);
                $searchModel = new HistoriaClinicaSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->where(['id_paciente' =>$verificarPaciente->id]); 
                $dataProvider->setPagination(false); 
                $columnas= [
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'historia_clinica',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'establecimiento.nombre',
                        'label'=>'nombre establecimiento'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'paciente.nombre',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'paciente.numero_documento',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'establecimiento.codigo_sisa',
                    ],
                 ];
                if ($dataProvider) {
                        return $this->renderPartial('indexdetalle', [ 
                        //'id_paciente' => $verificarHistoria->id_paciente, 
                        'searchModel' => $searchModel, 
                        'dataProvider' => $dataProvider,
                        'permisos' => $permisos,
                        'columns' => $columnas,
                        'id_paciente'=>$id_paciente
                    ]); 
                }else{
                    return '<div>No se encontraron resultados</div>'; 
                }
            }
        }else{ 
            // Process for non-ajax request 
            return $this->redirect(['index']); 
        } 
    } 
    public function actionCreatedetalle($id_paciente){
        $request = Yii::$app->request;
        $model = new HistoriaClinica();  
        $parametroEstablecimiento =  Establecimiento::find()->joinWith(['localidad'])->all();
        $q=UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null))
           $parametroEstablecimiento=Establecimiento::find()->joinWith(['localidad'])->joinWith(['localidad.usuarioEstadistica'])->where(['usuario_estadistica.id_usuario' =>Yii::$app->user->identity->id])->all();
           $listaEstablecimiento= ArrayHelper::map( $parametroEstablecimiento , 'id', 'nombre');
        if($request->isAjax){   
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post())){
                $model->id_paciente=$id_paciente;
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "HistoriaClinica #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
            return [
                'title'=> "Crear HistoriaClinica",
                'content'=>$this->renderAjax('_formdetalle', [
                    'model' => $model,
                    'establecimiento'=>$listaEstablecimiento,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    public function actionEditdetalle($id_historia){
        $request = Yii::$app->request;
        $model = HistoriaClinica::findOne($id_historia);
        $parametroEstablecimiento =  Establecimiento::find()->joinWith(['localidad'])->all();
        $q=UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null))
           $parametroEstablecimiento=Establecimiento::find()->joinWith(['localidad'])->joinWith(['localidad.usuarioEstadistica'])->where(['usuario_estadistica.id_usuario' =>Yii::$app->user->identity->id])->all();
           $listaEstablecimiento= ArrayHelper::map( $parametroEstablecimiento , 'id', 'nombre');
        if($request->isAjax){   
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "HistoriaClinica #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
            return [
                'title'=> "Crear HistoriaClinica",
                'content'=>$this->renderAjax('_formdetalle', [
                    'model' => $model,
                    'establecimiento'=>$listaEstablecimiento,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
    public function actionExistepaciente($param){
        Yii::$app->response->format = Response::FORMAT_JSON;

        $paciente=Paciente::find()->where(['numero_documento'=>$param])->one();

        if(isset($paciente))
        return ['status'=>'ok',
        'paciente'=>$paciente];

        return ['status'=>'error',
        'paciente'=>$paciente];

    }

    public function actionBuscarnumerodocumento()
    {
    try{
        Yii::$app->response->format = Response::FORMAT_JSON;
        $dni=0;
        $dni = key($_POST);
        $paciente=new Paciente();
        $paciente->nombre = null;
        $paciente->apellido = null;
        $paciente->fecha_nacimiento = null;
        $paciente->id_sexo = null;
        $maestro= Maestro::find()->where(["documento_numero"=> $dni])->one();
        
        
        if($maestro==null || $maestro==""){
            
            return ['status'=>'error','paciente'=>$paciente];
        }
      
       $paciente->nombre=$maestro->primer_nombre;
       $paciente->apellido=$maestro->primer_apellido;
       $paciente->fecha_nacimiento=$maestro->fecha_nacimiento;
       if($maestro->sexo=="female"){
        $paciente->id_sexo=2;
       }
       else{
        $paciente->id_sexo=1;
       }
      // var_dump($paciente);
       
        
        //$paciente->load(Yii::$app->request->post());
        //var_dump($paciente);

        return ['status'=>'ok',
                'paciente'=>$paciente];
    
    } catch (yii\db\Exception $e) {
        Yii::$app->response->format = Response::FORMAT_HTML;
        throw new NotFoundHttpException('Error de base de datos.', 500);
    }
    }

    /**
     * Updates an existing Paciente model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

     
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);    
        $contacto = $model->id_contacto 
        ? Contacto::findOne(['id' => $model->id_contacto]) 
        : new Contacto();
    $localidad = $model->id_localidad 
        ? Localidad::findOne(['id' => $model->id_localidad]) 
        : new Localidad();
    $sistemaEducativo = $model->id_sistema_educativo
        ? SistemaEducativo::findOne($model->id_sistema_educativo) 
        : new SistemaEducativo();
     
        $modelHistoriaClinica=HistoriaClinica::find()
        ->where(['historia_clinica.id_paciente'=> $id])
        ->andWhere(['historia_clinica.id_establecimiento'=> $model->id_establecimiento])
        ->andWhere(['not', ['historia_clinica.historia_clinica' => null]])
        ->one();
        if($modelHistoriaClinica==null){
            $modelHistoriaClinica=new HistoriaClinica();
        }
        $parametroOS = ObraSocial::find()->all();                            
        $listaOS= ArrayHelper::map( $parametroOS , 'id', 'nombre');
        $parametroEstablecimiento =  Establecimiento::find()->joinWith(['localidad'])->all();
        $q=UsuarioestadisticaController::obtenerUsuarioPorAreaReal();
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null))
           $parametroEstablecimiento=Establecimiento::find()->joinWith(['localidad'])->joinWith(['localidad.usuarioEstadistica'])->where(['usuario_estadistica.id_usuario' =>Yii::$app->user->identity->id])->all();
        $listaEstablecimiento= ArrayHelper::map( $parametroEstablecimiento , 'id', 'nombre');
        $listaLocalidad= ArrayHelper::map( Localidad::find()->all() , 'id', 'nombre');
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load(Yii::$app->request->post())){
                $sistemaEducativo->load($request->post());
                $model->id_sistema_educativo = $sistemaEducativo->elegirId();
                try {
                        if($model->numero_documento==9999999 || $model->numero_documento==99999999){
                        $modelHistoriaClinica->id_establecimiento=$model->id_establecimiento;
                        $modelHistoriaClinica->historia_clinica=trim(Yii::$app->request->getBodyParam('HistoriaClinica')['historia_clinica']);
                        $modelHistoriaClinica->id_paciente=$model->id;
                        $contacto->numero_telefono=trim(Yii::$app->request->getBodyParam('Contacto')['numero_telefono']);
                        $contacto->save();
                        $model->id_contacto=$contacto->id;
                        if($modelHistoriaClinica->save() && $model->save(false)){
                            $content='<span class="text-success">Registro guardado correctamente</span>';
                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "Paciente #".$model->id,
                                'content'=>$content,
                            ];
                        }
                        }else{
                            $modelHistoriaClinica->id_establecimiento=$model->id_establecimiento;
                            $modelHistoriaClinica->historia_clinica=trim(Yii::$app->request->getBodyParam('HistoriaClinica')['historia_clinica']);
                            $modelHistoriaClinica->id_paciente=$model->id;
                            $contacto->numero_telefono=trim(Yii::$app->request->getBodyParam('Contacto')['numero_telefono']);
                   
                            $contacto->save();
                            $model->id_contacto=$contacto->id;
                          
                            if( $model->save() && $modelHistoriaClinica->save()){

                            $content='<span class="text-success">Registro guardado correctamente</span>';
                            return [
                                'forceReload'=>'#crud-datatable-pjax',
                                'title'=> "Paciente #".$model->id,
                                'content'=>$content,
                            ];
                            }
                        } 
              }catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
            return [
                'title'=> "Editar Paciente #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                    'documento_ajeno' => $model->documento_ajeno,
                    'modelHistoria' => $modelHistoriaClinica,
                    'listaOS' =>$listaOS,
                    'establecimiento'=>$listaEstablecimiento,
                    'listaLocalidad'=>$listaLocalidad,
                    'contacto'=>$contacto,
                    'localidad'=>$localidad,
                    'sistemaEducativo'=>$sistemaEducativo
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }



    public function actionProvinciadelocalidad($provinciaId){

        Yii::$app->response->format = Response::FORMAT_JSON;
 
       $localidad  = Localidad::find()->innerJoinWith('provincia', true)->where(['localidad.id_provincia'=>$provinciaId])->all();
       if($localidad==null || empty($localidad)){
           return ['status'=>'error'];
       }
    return $localidad;
    }

    /**
     * Delete an existing Paciente model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            try {
                if ($model->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('No se puede borrar ya que esta asociado a un turno o tiene historia clinica.',500);
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Paciente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paciente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
                if (($model = Paciente::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null)
    {

        $request = Yii::$app->request;
        $reporte= new Reporte();

        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {

                $reporte->attributes=$_POST['Reporte'];
                $tipo_archivo=$_POST['tipo_archivo'];

                \app\components\Listado\Listado::widget(array(
                    'reporte'=>$reporte,
                    'tipo'=>$tipo_archivo
                ));

                $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;
                Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
                Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $dataProvider=Yii::$app->session->get('paciente-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'csv','totalRegistros'=>$dataProvider->totalCount,]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new Paciente();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Accion List, utilizada cuando se hace una busqueda autocomplete de este modelo
     * en query->select , verificar el campo de  busqueda
     * @param string de busqueda $q
     * @return arrary de resultados $results
     */
    public function actionList($q = null,$id=null)
    {

        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            
            $results = ['results' => ['id' => '', 'text' => '']];
            if (!is_null($q)) {

                $q="'%".strtolower(addslashes($q))."%'";

                $sql="SELECT p.id, CONCAT(p.nombre,' | ',trim(p.apellido),' | ',p.numero_documento) as text 
                        FROM consultasestadisticas.paciente as p
                        WHERE (lower(p.nombre) LIKE $q)
                        OR (lower( CONCAT(trim(p.nombre),' ',trim(p.apellido),' ',p.numero_documento)) LIKE $q)
                        ORDER BY  CONCAT(trim(p.nombre),' | ',trim(p.apellido),' | ',p.numero_documento)
                        LIMIT 50";


                
                $query= Yii::$app->db->createCommand($sql);
                $data = $query->queryAll();
      
                $results['results'] = array_values($data);
            }
            elseif ($id > 0) {
                $results['results'] = ['id' => $id, 'text' => Paciente::findOne($id)->nombre];
            }
            return $results;
            
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

}
