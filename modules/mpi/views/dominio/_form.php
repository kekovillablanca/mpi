<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Dominio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dominio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'id')->textInput();?>

    <?php echo $form->field($model, 'area')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'base')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'dominio')->textInput(['maxlength' => true]);?>

    <?php ActiveForm::end(); ?>
    
</div>
