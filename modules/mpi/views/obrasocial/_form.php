<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\ObraSocial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obra-social-form">

    <?php $form = ActiveForm::begin(['id' => 'form-obra-social']); ?>


    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]);       // ->textarea()?>
    <?php echo $form->field($model, 'cod_os')->textInput();?>

    <?php ActiveForm::end(); ?>
    
</div>
