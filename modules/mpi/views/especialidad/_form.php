<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\modules\mpi\models\PServicio;

/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\Especialidad */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="especialidad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]);       // ->textarea()?>
    <?php echo $form->field($model, 'codigo')->textInput(['maxlength' => true]);       // ->textarea()?>
    <?php 
		/*$data=ArrayHelper::map(PServicio::find()->asArray()->all(), 'id', 'nombre');
    	echo $form->field($model, 'id_servicio')->dropDownList($data,['prompt'=>'Seleccione una opción']);*/

        
        echo $form->field($model, 'id_servicio')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            //'initValueText' => ($model->id_servicio)?$model->consultasestadisticas.p_servicio->nombre:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['pservicio/list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
                'templateSelection' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
            ],
        ]);
        
	?>

    <?php ActiveForm::end(); ?>
    
</div>
