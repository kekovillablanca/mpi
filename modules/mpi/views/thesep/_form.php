<?php

use app\modules\mpi\models\Especialidad;
use app\modules\mpi\models\PServicio;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\Thesep */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="thesep-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'id_especialidad')->widget(Select2::classname(), [
     'options' => ['placeholder' => 'Select especialidad ...'],
     'data' => $listaEspecialidad,
     'pluginOptions' => [
     'minimumInputLength' => 3,
     'allowClear' => true,
     'language' => [
         'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
     ],
     'ajax' => [
         'url' => \yii\helpers\Url::to(['/mpi/especialidad/list']),
         'dataType' => 'json',
         'data' => new JsExpression('function(params) { return {q:params.term}; }')
     ],
     'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                 'templateResult' => new JsExpression('function(id_especialidad) { return id_especialidad.text; }'),
                 'templateSelection' => new JsExpression('function(id_especialidad) { return id_especialidad.text; }'),
    ],

   
   
]);  
?>

<?php echo $form->field($model, 'id_servicio')->widget(Select2::classname(), [
     'options' => ['placeholder' => 'Select servicio ...'],
     'data' => $listaServicios,
     'pluginOptions' => [
     'minimumInputLength' => 3,
     'allowClear' => true,
     'language' => [
         'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
     ],
     'ajax' => [
         'url' => \yii\helpers\Url::to(['/mpi/pservicio/list']),
         'dataType' => 'json',
         'data' => new JsExpression('function(params) { return {q:params.term}; }')
     ],
     'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                 'templateResult' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
                 'templateSelection' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
    ],

   
   
]);  
?>

    <?php echo $form->field($model, 'fecha')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php ActiveForm::end(); ?>
    
</div>
