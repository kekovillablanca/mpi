<?php

use app\modules\mpi\models\Profesional;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\ProfesionalEstablecimiento */
/* @var $form yii\widgets\ActiveForm */
//var_dump($model);
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="profesional-establecimiento-form">

    <?php $form = ActiveForm::begin(); ?>
    
           <?php 
    
    $parametroProfesional = Profesional::find()->where(['id'=>$model->id_profesional])->one();   
        $listaProfesional=array();
        if($parametroProfesional!=null)
            $listaProfesional[$parametroProfesional->id]=$parametroProfesional->nombre.','.$parametroProfesional->apellido;

    
    echo $form->field($model, 'id_profesional')->widget(Select2::classname(), [
                        'options' => ['placeholder' => 'Select profesional ...'],
                        'data' => $listaProfesional,
                        'pluginOptions' => [
                        'minimumInputLength' => 3,
                        'allowClear' => true,
                        'language' => [
                            'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['/mpi/profesional/list2']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(id_profesional) { return id_profesional.text; }'),
                                    'templateSelection' => new JsExpression('function(id_profesional) { return id_profesional.text; }'),
                                ],
                   
                      
                      
                   ])->error();  
   ?> 
  <?php echo $form->field($model, 'id_establecimiento')->widget(Select2::classname(), [
    'options' => ['placeholder' => 'Select a establecimiento ...'],
    'data' => $listaAreas,
    /*'pluginOptions' => [
    'minimumInputLength' => 3,
    'allowClear' => true,
    'language' => [
        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
    ],
    'ajax' => [
        'url' => \yii\helpers\Url::to(['/mpi/establecimiento/list']),
        'dataType' => 'json',
        'data' => new JsExpression('function(params) { return {q:params.term}; }')
    ],
    'escapemarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_establecimiento) { return id_establecimiento.text; }'),
                'templateSelection' => new JsExpression('function(id_establecimiento) { return id_establecimiento.text; }'),
            ],*/

   
   
]);  
?>

    <?php ActiveForm::end(); ?>
    
</div>
