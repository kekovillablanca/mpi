<div class="consulta-ambulatoria-master-objetivos">

    <br>
    <?php

    echo "<table class='kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap'>
        <thead>
        <tr><th>Nomenclador</th><th>Codigo</th><th>Especialidad</th><th>Area</th><th>Tipo</th><th>Resultado</th><th>Valor</th></tr>
        </thead>";

    echo "<tbody>";
  
    foreach($model as $modelo){

        echo "<tr>";
        echo "<td>".$modelo->nomenclador."</td>";
        echo "<td>".$modelo->codigo_nomenclador."</td>"; 
        echo "<td>".$modelo->examen_especialidad."</td>";
        echo "<td>".$modelo->examen_area."</td>";
        echo "<td>".$modelo->examen_tipo."</td>";
        echo "<td>".$modelo->examen_resultado."</td>";
        echo "<td>";
        if (trim($modelo->valor)=='true')
            echo 'SI';
        else if (trim($modelo->valor)=='false')    
            echo 'NO';
        else
            echo $modelo->valor;
        echo "</td>";
        echo "</tr>";
    }

    echo "</tbody></table>";

    echo "Observaciones: ";
    foreach($model as $modelo){
        echo $modelo->detalle;
    }
  
?>

</div>
