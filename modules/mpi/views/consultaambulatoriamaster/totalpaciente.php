<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaestroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Indice Pacientes';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

CrudAsset::register($this);

// Cuando tengo detalle agrego expandrowcolumn como  primer columna
if ($permisos['listdetalle']) {
    $columns = array_merge(array(array(
        'class' => '\kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function($model, $key, $index, $column) {
            return Yii::$app->runAction('mpi/consultaambulatoriamaster/listdetalle', ['id_maestro_maestro'=>$key]);
        },
        'detailRowCssClass' => 'expanded-row',
        'expandIcon' => '<i class="glyphicon glyphicon-expand" style="color:#337ab7"></i>',
        'collapseIcon' => '<i class="glyphicon glyphicon-collapse-down" style="color:#337ab7"></i>',
        'detailAnimationDuration' => 100,
        'allowBatchToggle' => false,
//        'expandOneOnly' => true,
    )), $columns);
}

// agrego acciones a $columns
$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['maestro/'.$action,'id'=>$key]);
        },
        'template' => '{view}',
        'visibleButtons'=>[
            'view'=> $permisos['view'],
        ],
        'buttons' => [
            'view' => function ($url) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver',]);
                },

        ],

    ];

$stringToolbar="";
?>
<div class="dominio-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => 'Indice Pacientes',
            ]
        ])?>
    </div>
</div>
<?php 
Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size"=>"modal-lg"
])?>
<?php Modal::end(); ?>
