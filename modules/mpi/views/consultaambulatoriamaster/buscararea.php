<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultaAmbulatoriaMaster */

$this->title = 'Seleccionar Area Programa';
$this->params['breadcrumbs'][] = $this->title;
?><script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="consulta-ambulatoria-master-area">

    <h2><?= Html::encode($this->title) ?></h2>

    <div class="consulta-ambulatoria-master-area">

        <?php $form = ActiveForm::begin(); ?>
        <br>
        <?php
        $data=ArrayHelper::map(app\modules\mpi\models\Dominio::find()->asArray()->all(), 'id', 'area');
        echo $form->field($model, 'dominio')->dropDownList($data,['prompt'=>'Seleccionar...','style'=>'width:360px'])->label(false);
        ?>
        <br>
        <div class="form-group">
            <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
