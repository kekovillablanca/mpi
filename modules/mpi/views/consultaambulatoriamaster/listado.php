<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConsultaAmbulatoriaMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$nombre_area=app\modules\mpi\models\Dominio::findOne($listado->dominio)->area;

$this->title = 'Listado Consulta Ambulatoria Area '.$nombre_area;
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$stringToolbar=Html::a('<i class="glyphicon glyphicon-download-alt"></i>',['exportlistado'],
                    ['data-pjax'=>'0','title'=> 'Exportar CSV','class'=>'btn btn-default']);

?>
<div class="consulta-ambulatoria-master-index">
    <div id="ajaxCrudDatatable">

        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => 'Listado Consulta Ambulatoria Area '.$nombre_area,
            ]
        ])?>
    </div>
</div>

