<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConsultaAmbulatoriaMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<script>

function downloadCsvViaAjax() {
    // Obtener el parámetro "indice" de la URL
    var btnMes = document.getElementById('btn-mes');
var spanContent = btnMes.firstElementChild.textContent;
console.log(spanContent); // Esto mostrará "febrero"


    // Solicitud para la descarga del archivo
    $.ajax({
        url: '<?= Url::to(['/mpi/consultaambulatoriamaster/downloadodontologia']) ?>',
        type: 'GET',
        data: {
            mesNombre: spanContent,
        },
        success: function(response) {
            console.log('Descarga exitosa.');

            // Crear el blob y la URL para el archivo
            var blob = new Blob([response], { type: 'text/csv;charset=utf-8;' });
            var url = window.URL.createObjectURL(blob);

            // Crear un enlace para descargar el archivo
            var a = document.createElement('a');
            a.href = url;
            a.target = '_blank';
            a.download = 'odontologia_'+spanContent;
            console.log('Nombre del archivo:', a.download);
            a.click();

            // Liberar memoria después de la descarga
            window.URL.revokeObjectURL(url);
        },
        error: function(xhr, status, error) {
            console.error('Error al descargar el archivo:', error);
        }
    });
}


</script>
<?php






$this->title = 'Listado Consulta Odontologia roca';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$stringToolbar="";
/*if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['downloadodontologia','indice'=> $indice,"tipo"=>"restar","anio"=>$anio],
            ['target'=>'_blank','title'=> 'Exportar','class'=>'btn btn-default']);
}*/
if ($permisos['export']){
    $stringToolbar .= Html::button('Descargar CSV', [
        'class' => 'btn btn-primary',
        'onclick' => 'console.log("Descargar CSV clicked"); downloadCsvViaAjax()'
    ]);
 

    
  

    
}

    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-chevron-left"></i>', ['odontologia','indice'=> $indice,"tipo"=>"restar","anio"=>$anio],
            ['class'=>'btn btn-default']);

    $stringToolbar=$stringToolbar.Html::a('<span>'. $trimestres[$indice][$indiceMatriz].'</span>', ['odontologia','indice'=> $indice,"tipo"=>"anio","anio"=>$anio],
           ['class'=>'btn btn-default' ,'id' => 'btn-mes']);
//$data= ['label'=>'2023', 'url' => \yii\helpers\Url::to(['/mpi/turno/restar','anio'=>'2023','indice'=>$indice]), 'label'=>'2022','url' => \yii\helpers\Url::to(['/mpi/turno/restar','anio'=>'2022','indice'=>$indice]),'label'=>'2021','url' => \yii\helpers\Url::to(['/mpi/turno/restar','anio'=>'2021','indice'=>$indice])];

//$stringToolbar=$stringToolbar.Html::dropDownList($anio,  null, ["2023"=>2023,"2022"=>2022,"2021"=>2021], $options = ["class"=>'btn btn-default','value' => 'none','onchange'=>'seleccionAnio(this)']);
   
    $stringToolbar=$stringToolbar.ButtonDropdown::widget([
        'label' => $anio,
        'options'=>[
            'class'=>'btn btn-default'
        ],
        
        'dropdown' => [
            'items' => [
            ['label'=>2024, 'url' => \yii\helpers\Url::to(['odontologia','indice'=>$indice,"tipo"=>"anio",'anio'=>2024])], 
            ['label'=>2023, 'url' => \yii\helpers\Url::to(['odontologia','indice'=>$indice,"tipo"=>"anio",'anio'=>2023])], 
            ['label'=>2022,'url' => \yii\helpers\Url::to(['odontologia','indice'=>$indice,"tipo"=>"anio",'anio'=>2022])],
            ['label'=>2021,'url' => \yii\helpers\Url::to(['odontologia','indice'=>$indice,"tipo"=>"anio",'anio'=>2021])]],
               
            
        
    ]]);




    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-chevron-right"></i>', ['odontologia','indice'=> $indice,"tipo"=>"sumar","anio"=>$anio],
            ['class'=>'btn btn-default']);

?>
<div class="consulta-ambulatoria-master-index">
    <div id="ajaxCrudDatatable">

        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'columns' => $columns,
        'toolbar'=> [  ['content' => $stringToolbar],],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
                
            ],     
            'options' => ['style' => 'width: fit-content;'], 
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
]);


Modal::end(); ?>