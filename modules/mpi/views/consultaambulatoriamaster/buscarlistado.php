<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultaAmbulatoriaMaster */

$this->title = 'Datos Listado de Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="consulta-ambulatoria-master">

    <h2><?= Html::encode($this->title) ?></h2>

    <div class="consulta-ambulatoria-master">

        <?php $form = ActiveForm::begin(); ?>
        <br>

        <div style="float:left;">
        <?php
        echo $form->field($model, 'fecha_desde')->widget(DatePicker::className(),[
        'options' => ['placeholder' => 'Ingrese una fecha'],
        'pluginOptions' => ['autoclose'=>true],
        'options' => ['style'=>'width:280px'],
        ]);
        ?>
        </div>

        <div style="float:left;margin-left:6px;">
        <?php
        echo $form->field($model, 'fecha_hasta')->widget(DatePicker::className(),[
        'options' => ['placeholder' => 'Ingrese una fecha'],
        'pluginOptions' => ['autoclose'=>true],
        'options' => ['style'=>'width:280px'],
        ]);
        ?>
        </div>
        <div style="clear:both;"></div>

        <div style="float:left;">
        <?php
        $data=ArrayHelper::map(app\modules\mpi\models\Dominio::find()->asArray()->all(), 'id', 'area');
        echo $form->field($model, 'dominio')->dropDownList($data,[
            'prompt'=>'Seleccionar...',
            'style'=>'width:360px',            
            'onchange' => '
                $.post("' . Url::to(["servicioespecialidad"]) . '", 
                    { dominio: $(this).val() }, 
                    function(data){
                        $("#listadoform-servicio").html(data);
                    }
                    );
                $.post("' . Url::to(["servicioespecialidad"]) . '", 
                    { dominio: $(this).val(), servicio: ""}, 
                    function(data){
                        $("#listadoform-especialidad").html(data);
                    }
                );                
                $.post("' . Url::to(["centro"]) . '", 
                    { dominio: $(this).val(), centro: ""}, 
                    function(data){
                        $("#listadoform-centro").html(data);
                    }
                );                
                $("#listadoform-centro").val("");
                $("#listadoform-servicio").val("");
                $("#listadoform-especialidad").val("");',
        ]);
        ?>
        </div>

        <div style="float:left;margin-left:6px;">
        <?php
        echo $form->field($model, 'centro')->dropDownList(['' => 'Seleccionar...' ],[
            'style'=>'width:360px',            
        ]);
        ?>
        </div>
        <div style="clear:both;"></div>

        <div style="float:left;">
        <?php
        echo $form->field($model, 'servicio')->dropDownList(['' => 'Seleccionar...' ],[
            'style'=>'width:360px',
            'onchange' => '
                $.post("' . Url::to(["servicioespecialidad"]) . '", 
                        { dominio: $("#listadoform-dominio").val(), servicio: $(this).val() }, 
                        function(data){
                            $("#listadoform-especialidad").html(data);
                        }
                    );
                    $("#listadoform-especialidad").val("");',            
        ]);
        ?>
        </div>

        <div style="float:left;margin-left:6px;">
        <?php
        echo $form->field($model, 'especialidad')->dropDownList(['' => 'Seleccionar...' ],[
            'style'=>'width:360px',            
        ]);
        ?>
        </div>
        <div style="clear:both;"></div>

        <div style='width:726px;'>
        <?php
        echo $form->field($model, 'diagnostico')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => "",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['cie10']).'&dominio='.$model->dominio,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(diagnostico) { return diagnostico.text; }'),
                'templateSelection' => new JsExpression('function(diagnostico) { return diagnostico.text; }'),
            ],
        ]);
        ?>
        </div>

        <br>
        <div class="form-group" style='width:726px;'>
        <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('Filtros Avanzados', ['class' => 'btn btn-primary','style'=>'float:right;','name' => 'filtros']) ?>
        </div>
        
        <?php ActiveForm::end(); ?>

    </div>

</div>
