<?php

use app\modules\mpi\models\ConsultaAmbulatoriaPlan;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConsultaAmbulatoriaMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?><script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<style>
 .panel > .table-responsive > .table-bordered > thead > tr:first-child > th{
    border-bottom: 0;
    line-break: initial;
    text-align: center;
    line-height: initial;

    vertical-align: top;

}
</style>
<script>
function descargar_informe(dominio,informe){
    // muestro mensaje
    $("#mensaje_informe").css({"display":"block"});
    $.get({
        url: '<?php echo Url::to(['informe']) ?>',
        data:{
            dominio: dominio,
            informe: informe,
        },
        success: function(data) {                
            if( data.status == 'success' ){
                console.log(data.file);
                $('#'+"informe_"+informe).attr("href", data.file);
                $('#'+"informe_"+informe)[0].click();
                $("#mensaje_informe").css({"display":"none"});
            }else{
                $("#mensaje_informe").html('<center><br><b><p style="color:red">Error en la operacion indicada</p></b><br><button onclick="$(\'#mensaje_informe\').css({\'display\':\'none\'})">Aceptar</button></center>');
            }
        }
    });
}

</script>
<?php
$nombre_area=app\modules\mpi\models\Dominio::findOne($dominio)->area;

$this->title = 'Consulta Ambulatoria Area '.$nombre_area;
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

// agrego acciones a $columns
$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key,'dominio'=>$model->dominio]);
        },
        'template'=>'{viewdetalle} {informe}',
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'visibleButtons'=>[
            'viewdetalle'=> $permisos['viewdetalle'],
            'informe'=> $permisos['pdfplan'],
            
        ],
        
        'buttons' => [
                'viewdetalle' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                        ['role'=>'modal-remote','title'=> 'Ver',]);
                    },    

                    'informe' => function ($url,$model) {
                        $id1=$model->getAttributes()['id1'];
                        $id2=$model->getAttributes()['id2'];
                        $dominio=$model->getAttributes()['dominio'];
                        $model_plan= new ConsultaAmbulatoriaPlan();
                        $model_plan->setDbId($dominio);
                        $model_plan=ConsultaAmbulatoriaPlan::find()->where(['paciente_id'=>$id1,'fecha_consulta'=>$id2,])->andWhere(['IS NOT', 'informe_adjunto', null])
                        ->all();   
                        if(!empty($model_plan)){
                            return Html::tag('span', 'Tiene informe', ['class' => "text-uppercase, text-success"]);
                    }
                    else{
                       return Html::tag('p', 'No tiene informe', ['class' => "text-uppercase, text-danger"]);
                    }
                    },
               
                    
            ],
    
    ];

$stringToolbar="";

if ($permisos['area']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['','dominio'=>$dominio],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r'],'dominio'=>$dominio],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

?>
<div class="consulta-ambulatoria-master-index">
    <div id="ajaxCrudDatatable">

        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ],
            'options' => ['style' => 'width: fit-content;'], 
        ])?>
    </div>
</div>

<style> .modal-dialog { width: 900px; } </style>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>