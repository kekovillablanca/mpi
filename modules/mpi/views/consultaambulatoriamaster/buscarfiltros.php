<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\mpi\models\Dominio;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\ConsultaAmbulatoriaMaster */

$this->title = 'Filtros avanzados - Listado de Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="consulta-ambulatoria-master">

    <h2><?= Html::encode($this->title) ?></h2>

    <div class="consulta-ambulatoria-master">

        <?php $form = ActiveForm::begin(); ?>
        <br>

        <label class="control-label" for="listadoform-filtros">Fecha desde:&nbsp;
        <?=$model->fecha_desde ?>
        </label><br>
        <label class="control-label" for="listadoform-filtros">Fecha hasta:&nbsp;
        <?=$model->fecha_hasta ?>
        </label><br>
        <label class="control-label" for="listadoform-filtros">Area:&nbsp;
        <?=Dominio::findOne($model->dominio)->area;?>
        </label><br>
        <label class="control-label" for="listadoform-filtros">Servicio:&nbsp;
        <?=$model->servicio;?>
        </label><br>
        <label class="control-label" for="listadoform-filtros">Especialidad:&nbsp;
        <?=$model->especialidad;?>
        </label>
        <br>
        <?php
        echo $form->field($model, 'fecha_desde')->hiddenInput()->label(false)->error(false);
        echo $form->field($model, 'fecha_hasta')->hiddenInput()->label(false)->error(false);
        echo $form->field($model, 'dominio')->hiddenInput()->label(false)->error(false);
        echo $form->field($model, 'servicio')->hiddenInput()->label(false)->error(false);
        echo $form->field($model, 'especialidad')->hiddenInput()->label(false)->error(false);
        ?>

<br><label class="control-label" for="listadoform-filtros">DATOS SUBJETIVOS</label><br><br>
<div style="border-left :1px solid #d3d3d3;margin-left: 30px;padding-left:15px;">

        <?php    

            echo $form->field($model, 'subjetivoicpc2_1')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['icpc2']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(subjetivoicpc2_1) { return subjetivoicpc2_1.text; }'),
                    'templateSelection' => new JsExpression('function(subjetivoicpc2_1) { return subjetivoicpc2_1.text; }'),
                ],
            ]);

            echo $form->field($model, 'subjetivoicpc2_2')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['icpc2']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(subjetivoicpc2_2) { return subjetivoicpc2_2.text; }'),
                    'templateSelection' => new JsExpression('function(subjetivoicpc2_2) { return subjetivoicpc2_2.text; }'),
                ],
            ]);

            echo $form->field($model, 'subjetivoicpc2_3')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['icpc2']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(subjetivoicpc2_3) { return subjetivoicpc2_3.text; }'),
                    'templateSelection' => new JsExpression('function(subjetivoicpc2_3) { return subjetivoicpc2_3.text; }'),
                ],
            ]);

            echo $form->field($model, 'subjetivoicpc2_4')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['icpc2']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(subjetivoicpc2_4) { return subjetivoicpc2_4.text; }'),
                    'templateSelection' => new JsExpression('function(subjetivoicpc2_4) { return subjetivoicpc2_4.text; }'),
                ],
            ]);

            echo $form->field($model, 'subjetivoicpc2_5')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['icpc2']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(subjetivoicpc2_5) { return subjetivoicpc2_5.text; }'),
                    'templateSelection' => new JsExpression('function(subjetivoicpc2_5) { return subjetivoicpc2_5.text; }'),
                ],
            ]);

        ?>

        <?php echo $form->field($model, 'subjetivoobs')->textInput(['maxlength' => true]);?>

</div>
        

<br><label class="control-label" for="listadoform-filtros">DATOS OBJETIVOS</label><br><br>
<div style="border-left :1px solid #d3d3d3;margin-left: 30px;padding-left:15px;">

        <?php    

            echo $form->field($model, 'objetivosistema_1')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['sistema']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(objetivosistema_1) { return objetivosistema_1.text; }'),
                    'templateSelection' => new JsExpression('function(objetivosistema_1) { return objetivosistema_1.text; }'),
                ],
            ]);

            echo $form->field($model, 'objetivosistema_2')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['sistema']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(objetivosistema_2) { return objetivosistema_2.text; }'),
                    'templateSelection' => new JsExpression('function(objetivosistema_2) { return objetivosistema_2.text; }'),
                ],
            ]);

            echo $form->field($model, 'objetivosistema_3')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['sistema']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(objetivosistema_3) { return objetivosistema_3.text; }'),
                    'templateSelection' => new JsExpression('function(objetivosistema_3) { return objetivosistema_3.text; }'),
                ],
            ]);

            echo $form->field($model, 'objetivosistema_4')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['sistema']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(objetivosistema_4) { return objetivosistema_4.text; }'),
                    'templateSelection' => new JsExpression('function(objetivosistema_4) { return objetivosistema_4.text; }'),
                ],
            ]);

            echo $form->field($model, 'objetivosistema_5')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['sistema']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(objetivosistema_5) { return objetivosistema_5.text; }'),
                    'templateSelection' => new JsExpression('function(objetivosistema_5) { return objetivosistema_5.text; }'),
                ],
            ]);

        ?>

        <?php echo $form->field($model, 'objetivoobs')->textInput(['maxlength' => true]);?>

</div>

<br><label class="control-label" for="listadoform-filtros">ANALISIS</label><br><br>
<div style="border-left :1px solid #d3d3d3;margin-left: 30px;padding-left:15px;">

        <?php    

            echo $form->field($model, 'analisiscie10_1')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['cie10']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(analisiscie10_1) { return analisiscie10_1.text; }'),
                    'templateSelection' => new JsExpression('function(analisiscie10_1) { return analisiscie10_1.text; }'),
                ],
            ]);

            echo $form->field($model, 'analisiscie10_2')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['cie10']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(analisiscie10_2) { return analisiscie10_2.text; }'),
                    'templateSelection' => new JsExpression('function(analisiscie10_2) { return analisiscie10_2.text; }'),
                ],
            ]);

            echo $form->field($model, 'analisiscie10_3')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['cie10']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(analisiscie10_3) { return analisiscie10_3.text; }'),
                    'templateSelection' => new JsExpression('function(analisiscie10_3) { return analisiscie10_3.text; }'),
                ],
            ]);

            echo $form->field($model, 'analisiscie10_4')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['cie10']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(analisiscie10_4) { return analisiscie10_4.text; }'),
                    'templateSelection' => new JsExpression('function(analisiscie10_4) { return analisiscie10_4.text; }'),
                ],
            ]);

            echo $form->field($model, 'analisiscie10_5')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['cie10']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(analisiscie10_5) { return analisiscie10_5.text; }'),
                    'templateSelection' => new JsExpression('function(analisiscie10_5) { return analisiscie10_5.text; }'),
                ],
            ]);

        ?>

        <?php echo $form->field($model, 'analisisobs')->textInput(['maxlength' => true]);?>

</div>

<br><label class="control-label" for="listadoform-filtros">PLAN</label><br><br>
<div style="border-left :1px solid #d3d3d3;margin-left: 30px;padding-left:15px;">

        <?php    

            echo $form->field($model, 'plannome_1')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['nomenclador']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(plannome_1) { return plannome_1.text; }'),
                    'templateSelection' => new JsExpression('function(plannome_1) { return plannome_1.text; }'),
                ],
            ]);

            echo $form->field($model, 'plannome_2')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['nomenclador']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(plannome_2) { return plannome_2.text; }'),
                    'templateSelection' => new JsExpression('function(plannome_2) { return plannome_2.text; }'),
                ],
            ]);

            echo $form->field($model, 'plannome_3')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['nomenclador']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(plannome_3) { return plannome_3.text; }'),
                    'templateSelection' => new JsExpression('function(plannome_3) { return plannome_3.text; }'),
                ],
            ]);

            echo $form->field($model, 'plannome_4')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['nomenclador']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(plannome_4) { return plannome_4.text; }'),
                    'templateSelection' => new JsExpression('function(plannome_4) { return plannome_4.text; }'),
                ],
            ]);

            echo $form->field($model, 'plannome_5')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
                'initValueText' => "",
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                    ],
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['nomenclador']).'&dominio='.$model->dominio,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(plannome_5) { return plannome_5.text; }'),
                    'templateSelection' => new JsExpression('function(plannome_5) { return plannome_5.text; }'),
                ],
            ]);

        ?>

        <?php echo $form->field($model, 'planobs')->textInput(['maxlength' => true]);?>

</div>

        <div class="form-group">
            <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
