<?php

use app\components\Metodos\Metodos;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
CrudAsset::register($this);

?>

<script>
    function exportDetalle() {
        let keys = $('.detalle-seleccionado input[type="checkbox"]:checked').map(function() {
            return $(this).val();
        }).get();

        if (keys.length) {
            $.ajax({
                url: '<?= Url::to(['exportconsultaseleccion']) ?>',
                dataType: 'json',
                type: "POST",
                data: { keylist: keys },
                success: function(data) {
                    if (data.status === 'success') {
                        $('#exportar_consultas').attr("href", data.file).get(0).click();
                    }
                }
            });
        }
    }
</script>

<div style="height:auto;"> 

    <div style="float:left;width:75%;padding-right:20px;"> 

        <div>
            <h3><?= Html::encode($model->primer_apellido . " " . $model->segundo_apellido . ", " . $model->primer_nombre . " " . $model->segundo_nombre) ?></h3>
            <?= Html::a('<span class="glyphicon glyphicon-download-alt" style="font-size: 21px; padding-left:30px"></span>',
                        Url::to(['exportdatos', 'id' => $model->id_maestro]),
                        ['title' => 'Exportar el informe a formato PDF']); ?>
        </div>

        <h4 style='padding-bottom:10px;'><b><?= strtoupper($model->documento_tipo) . " " . $model->documento_numero . (($model->estado == 2) ? " (Paciente Fallecido)" : "") ?></b></h4>
        <h4><b><?= $model->flag_federado ? "Paciente Federado (id " . $model->id_federado_nacional . ")" : "Paciente NO Federado" ?></b></h4>
        <b>Fecha Nacimiento:</b> <?= $model->fecha_nacimiento ?><br>
        <b>Sexo:</b> <?= $model->sexo === 'male' ? "Masculino" : ($model->sexo === 'female' ? "Femenino" : "Indeterminado") ?><br>

        <?php foreach ($datos as $dato): ?>
            <h4><b>Área <?= Html::encode($dato['nombre']) ?> (hc <?= Html::encode($dato['hc']) ?>)</b></h4>
            <b>Domicilio:</b> <?= Html::encode($dato['domicilio']) ?><br>
            <b>Teléfono:</b> <?= Html::encode($dato['telefono']) ?><br><br>
        <?php endforeach; ?>

        <div><b>DATOS RENAPER</b></div>
        <table id="datos" class="table table-striped table-bordered detail-view">
            <tbody>
                <?php
                if (is_array($json_renaper)) {
                    $apellido_comparacion = strtoupper(trim(trim($model->primer_apellido) . " " . trim($model->segundo_apellido)));
                    $nombre_comparacion = strtoupper(trim(trim($model->primer_nombre) . " " . trim($model->segundo_nombre)));
                    $fecha_nacimiento_comparacion = Metodos::dateConvert($model->fecha_nacimiento, 'toSql');

                    foreach (['apellido' => 'Apellido', 'nombres' => 'Nombre', 'fechaNacimiento' => 'Fecha Nacimiento', 'sexo' => 'Sexo', 'cuil' => 'CUIL', 'calle' => 'Dirección', 'cpostal' => 'Ciudad'] as $key => $label) {
                        $value = ($key === 'calle') ? $json_renaper['calle'] . " " . $json_renaper['numero'] . " " . $json_renaper['piso'] . " " . $json_renaper['departamento'] : $json_renaper[$key];
                        $isMatch = ($key === 'apellido') ? strtoupper(trim($value)) === $apellido_comparacion : (($key === 'nombres') ? strtoupper(trim($value)) === $nombre_comparacion : ($key === 'fechaNacimiento' ? trim($value) === $fecha_nacimiento_comparacion : true));

                        if ($isMatch) {
                            echo "<tr><th>$label</th><td>$value</td></tr>";
                        } else {
                            echo "<tr><th style='color:red; font-weight: bold;'>$label</th><td style='color:red; font-weight: bold;'>$value</td></tr>";
                        }
                    }
                } else {
                    echo "<tr><td>&nbsp;&nbsp;".$json_renaper."</td></tr>";
                }
                ?>
            </tbody>
        </table>

        <div><b>RED DIGITAL DE SALUD</b></div>
        <table id='datos' class="table table-striped table-bordered detail-view" style='padding:0px;'>
            <tbody>
                <?php
                if ($model->flag_federado) {
                    if (is_array($json_federador)) {
                        foreach ($json_federador["entry"] as $entry) {
                            foreach ($entry["resource"]["identifier"] as $value_aux) {
                                echo "<tr><th>{$value_aux['system']}</th><td>{$value_aux['value']}</td></tr>";
                            }
                        }
                    } else {
                        echo "<tr><td>&nbsp;&nbsp;" . $json_federador . "</td></tr>";
                    }
                } else {
                    echo "<tr><td>&nbsp;&nbsp;Paciente no federado</td></tr>";
                }
                ?>
            </tbody>
        </table>

    </div>

    <div style="float:left; width:25%">
        <div style="border:1px solid grey; height:auto; padding:15px">
            <?= Html::img($json_renaper['foto'] ?? 'images/imagen_anonimo.png', ['style' => 'max-width:250px; height: auto;']) ?>
        </div>
    </div>

</div>

<br><br>
<div style="clear:both;"></div>

<style>
    .detalle-seleccionado > td {
        background-color: #bee0bf !important;
    }
    .crudconsulta-datatable > table > tbody > tr > th,
    .crudconsulta-datatable > table > tbody > tr > td {
        padding-left: 2px;
        padding-top: 1px;
        padding-bottom: 1px;
    }
    .modal-dialog { width: 900px; }
</style>

<?= Html::a('', null, ['id' => 'exportar_consultas', 'style' => 'display:none', 'download' => '']) ?>

<?php

$columns = array_merge([
    [
        'class' => '\kartik\grid\CheckboxColumn',
        'header' => Html::a('<span class="glyphicon glyphicon-download-alt"></span>', '', ['title' => 'Descargar registros seleccionados', 'onclick' => 'exportDetalle()']),
        'checkboxOptions' => function($model) {
            $id = ['id1' => $model['id1'], 'id2' => $model['id2'], 'dominio' => $model['dominio']];
            return ['value' => json_encode($id)];
        },
        'rowSelectedClass' => 'detalle-seleccionado',
    ]
], $columns);

$columns[] = [
    'class' => 'kartik\grid\ActionColumn',
    'contentOptions' => ['style' => ['white-space' => 'nowrap']],
    'dropdown' => false,
    'vAlign' => 'middle',
    'urlCreator' => function($action, $model, $key, $index) {
        $id = ['id1' => $model['id1'], 'id2' => $model['id2']];
        return Url::to([$action, 'id' => $id, 'dominio' => $model['dominio']]);
    },
    'template' => '{viewdetalle} {exportconsulta}',
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>&nbsp;', $url, ['role' => 'modal-remote', 'title' => 'Ver datos del registro']);
        },
        'exportconsulta' => function ($url) {
            return Html::a('<span class="glyphicon glyphicon-download-alt"></span>&nbsp;', $url, ['data-pjax' => '0', 'title' => 'Exportar PDF']);
        },
    ],
];

?>
<div class="consulta-index">
        <div id="ajaxCrudDatatable">
        <?= GridView::widget([
    'id' => 'crudconsulta-datatable',
    'dataProvider' => $dataProvider,
    'filterModel' => null,
    'pjax' => true,
    'columns' => $columns,
    'toolbar' => '',
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'panel' => [
        'type' => 'primary',
        'heading' => 'Consultas Ambulatorias',
    ],
    'pager' => [
        'firstPageLabel' => 'Primera',
        'lastPageLabel' => 'Última',
        'nextPageLabel' => 'Siguiente',
        'prevPageLabel' => 'Anterior',
    ],
]) ?>
    </div>
</div>

<style> .modal-dialog { width: 900px; } </style>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
