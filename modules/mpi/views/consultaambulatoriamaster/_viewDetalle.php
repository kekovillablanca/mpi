<?php
use yii\bootstrap\Tabs;
?>

<div class="consulta-ambulatoria-master-detalle">

    <?php
    echo '<b>';
    echo $model->paciente_ape.', '.$model->paciente_nom.' ('.trim($model->paciente_tipodoc).' '.trim($model->paciente_numerodoc).')';
    echo "<br>";
    echo strtoupper($model->area)." (".$model->centro.")";
    echo "<br>";
    echo $model->fecha." (".substr(trim($model->id2),-8).")";
    echo "<br><br>";
    echo '</b>';

    echo Tabs::widget([

    'items' => [
        [
        'label' => 'Datos Subjetivos',
        'content' => $this->render('_subjetivos', ['model'=>$model_subjetivo,]),
        'active' => true,
        'headerOptions'=>['style'=>'font-weight:bold;'],
        ],
        [
        'label' => 'Datos Objetivos',
        'content' => $this->render('_objetivos', ['model'=>$model_objetivo]),
        'headerOptions'=>['style'=>'font-weight:bold;'],
        ],

        [
        'label' => 'Analisis',
        'content' => $this->render('_analisis', ['model'=>$model_analisis]),
        'headerOptions'=>['style'=>'font-weight:bold;'],
        ],    
        [
        'label' => 'Plan',
        'content' => $this->render('_plan', ['model'=>$model_plan,'dominio'=>$dominio,'area'=>$model->area]),
        'headerOptions'=>['style'=>'font-weight:bold;'],
        ],
    ],
    ]);
    ?>

</div>
