<div class="consulta-ambulatoria-master-subjetivos">
    <br>
    <?php

    echo "<table class='kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap'>
        <thead>
        <tr><th>Nomenclador</th><th>Codigo</th><th>Capitulo</th><th>Componente</th><th>Descripcion</th></tr>
        </thead>";

    echo "<tbody>";
  
    foreach($model as $modelo){

        echo "<tr>";
        echo "<td>".$modelo->nomenclador."</td>";
        echo "<td>".$modelo->codigo_nomenclador."</td>"; 
        echo "<td>".$modelo->codigo_capitulo."</td>";
        echo "<td>".$modelo->codigo_componente."</td>";
        echo "<td>".$modelo->codigo_descripcion."</td>";
        echo "</tr>";
    }

    echo "</tbody></table>";

    echo "Observaciones: ";
    foreach($model as $modelo){
        echo $modelo->detalle;
    }
  
?>

</div>

