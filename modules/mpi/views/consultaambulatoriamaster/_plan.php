<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<script>

function descargar_informe(dominio,informe){
        // muestro mensaje
        $("#mensaje_informe").css({"display":"block"});
        $.get({
            url: '<?php echo Url::to(['informe']) ?>',
            data:{
                dominio: dominio,
                informe: informe,
            },
            success: function(data) {                
                if( data.status == 'success' ){
                    $('#'+"informe_"+informe).attr("href", data.file);
                    $('#'+"informe_"+informe)[0].click();
                    $("#mensaje_informe").css({"display":"none"});
                }else{
                    $("#mensaje_informe").html('<center><br><b><p style="color:red">Error en la operacion indicada</p></b><br><button onclick="$(\'#mensaje_informe\').css({\'display\':\'none\'})">Aceptar</button></center>');
                }
            }
        });
}
</script>

<div class="consulta-ambulatoria-master-plan">

<br>
    <?php

    echo "<table class='kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap'>
        <thead>
        <tr><th>Nomenclador</th><th>Codigo</th><th>Seccion</th><th>Grupo</th><th>Descripcion</th><th>Pre informe</th></tr>
        </thead>";

    echo "<tbody>";
  
    foreach($model as $modelo){

        echo "<tr>";
        echo "<td>".$modelo->nomenclador."</td>";
        echo "<td>".$modelo->codigo_nomenclador."</td>"; 
        echo "<td>".$modelo->seccion_nomenclador."</td>"; 
        echo "<td>".$modelo->grupo_nomenclador."</td>"; 
        echo "<td>".$modelo->descripcion_completa."</td>";
        echo "<td>".$modelo->preinforme."</td>";
        echo "</tr>";
    }
    echo "</tbody></table>";
    echo "Observaciones: ";
    $informe_adjunto=false;
    $link_informe = "";
    $link_download = "";

    foreach($model as $modelo){
        echo $modelo->detalle;
        if ($modelo->informe_adjunto>0){
            $informe_adjunto=true;
            $link_informe.=
                    Html::a('Informe ('.$modelo->informe_adjunto.')','#',      
                    [ 'onclick'=>'descargar_informe('.$dominio.','.$modelo->informe_adjunto.')',
                      'title'=> 'Descargar Informe']).'&nbsp;&nbsp;';
            }
            $link_download.='<a id="informe_'.$modelo->informe_adjunto.'" style="display:none" download=""></a>';
    }
    if ($informe_adjunto){
        echo "<br><b>".$link_informe."</b>";
        echo $link_download;
        echo "<div id='mensaje_informe' class='modal-content' 
        style='display:none;width: 400px;height: 135px;position: absolute;top:50%;left: 50%;margin-top: -100px;margin-left: -200px;'>
        <center><br><b>Solicitando Informe al Sistema Informatico</b><br>
        <b>Area ".$area."</b><br><br> 
        <img src='images/loading.gif'>
        </center></div>";
    }
    ?>

</div>
