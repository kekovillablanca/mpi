<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
?><script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<link rel="stylesheet" type="text/css" href="<?= Yii::$app->request->baseUrl ?>/css/detail.css" />

<div class="detalle-expand" style="padding-left: 5px;padding-right: 5px;">
    <div style="font-size: 16px;padding-top: 0px;padding-bottom: 0px;"><b>Consultas del paciente</b></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id'=>'crud-detail',
        'layout' => '{items}',
        'pjax'=>true,
        'columns' => [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'area',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'centro',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'especialidad',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'medico_completo',
			],
            [
                'class' => 'kartik\grid\ActionColumn',
                'vAlign'=>'middle',
                'contentOptions' =>['width'=>'75px', 'style'=>'text-align:center;'],
                'template' => '{viewdetalle}',
                'buttons' => [
                    'viewdetalle' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>&nbsp;', $url,
                                ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
                    },
                ],
                'urlCreator' => function ($action, $searchModel,$key) {
                        if ($action === 'viewdetalle') {
                            return Url::to(['viewdetalle','id'=>$key,'dominio'=>$searchModel->dominio]);
                        }

                }
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
    ]);

    ?>

</div>
