<div class="consulta-ambulatoria-master-analisis">

    <br>
    <?php

    echo "<table class='kv-grid-table table table-bordered table-striped table-condensed kv-table-wrap'>
        <thead>
        <tr><th>Episodio</th><th>Nomenclador</th><th>Codigo</th><th>Descripcion</th><th>Tipo Diag</th></tr>
        </thead>";

    echo "<tbody>";
  
    foreach($model as $modelo){

        echo "<tr>";
        echo "<td>".$modelo->episodio."</td>";
        echo "<td>".$modelo->nomenclador."</td>";
        echo "<td>".$modelo->codigo_nomenclador."</td>"; 
        echo "<td>".$modelo->codigo_descripcion."</td>";
        echo "<td>".$modelo->tipo_diagnostico."</td>";
        echo "</tr>";

    }

    echo "</tbody></table>";

    echo "Observaciones: ";
    foreach($model as $modelo){
            
        echo "(Ep: ".$modelo->episodio.") ";
        echo $modelo->detalle;
    }
?>

</div>

