<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */

$this->title = 'Buscar Paciente';
$this->params['breadcrumbs'][] = $this->title;
?><script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="maestro-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <div class="maestro-form">

        <?php $form = ActiveForm::begin(); ?>
        <?php

        echo "<div style='float:left;width:200px;'>";
        $data=ArrayHelper::map(app\modules\mpi\models\Dominio::find()->asArray()->all(), 'id', 'area');
        echo $form->field($model, 'id_area')->dropDownList($data,['prompt'=>'Seleccionar...','style'=>'width:200px']);
        echo "</div>";

        $data=['dni','lc','le','pas','ci','in','otr'];
        echo "<div style='float:left;width:180px;;margin-left:10px;'>";
        echo $form->field($model, 'documento_tipo')->dropDownList($data,['prompt'=>'Seleccionar...']);
        echo "</div>";
        echo "<div style='float:left;width:280px;margin-left:10px;'>";
        echo $form->field($model, 'documento_numero')->textInput();
        echo "</div>";
        
        ?>
        <div style='clear:both'></div>
        <div class="form-group">
            <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
        </div>
        <br><br>

        <?php    

        echo $form->field($model, 'id_maestro')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => ($model->id_maestro)?$model->localidad->descripcion_completa:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => \yii\helpers\Url::to(['/mpi/maestro/list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_maestro) { return id_maestro.text; }'),
                'templateSelection' => new JsExpression('function(id_maestro) { return id_maestro.text; }'),
            ],
        ])->label('Paciente');

        ?>

        <div class="form-group">
            <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
