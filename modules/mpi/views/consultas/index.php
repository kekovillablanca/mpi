<?php

use kartik\grid\GridView;
use yii\bootstrap\Html;

// Create a panel layout for your GridView widget
echo GridView::widget([
    'dataProvider'=> $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="fas fa-globe"></i> Countries</h3>',
        'type'=>'success',
        'before'=>Html::a('<i class="fas fa-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
        'after'=>Html::a('<i class="fas fa-redo"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        'footer'=>false
    ],
]);