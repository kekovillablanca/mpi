<?php

use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

$this->title = 'IECMA';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
if ($permisos['indexdetalle']) {
    $columns = array_merge(array(array(
        'class' => '\kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => Url::to(['indexdetalle']),
        'detailRowCssClass' => 'expanded-row',
        'expandIcon' => '<i class="glyphicon glyphicon-expand" style="color:#337ab7"></i>',
        'collapseIcon' => '<i class="glyphicon glyphicon-collapse-down" style="color:#337ab7"></i>',
        'detailAnimationDuration' => 50,
        'allowBatchToggle' => false,
    )), $columns);
}
// agrego acciones a $columns
$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'IECMA',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]
    ];

$stringToolbar="";


if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-chevron-left"></i>', ['index','indice'=> $indice,"tipo"=>"restar","anio"=>$anio],
            ['class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<span>'. $trimestres[$indice][$indiceMatriz].'</span>', ['index','indice'=> $indice,"tipo"=>"anio","anio"=>$anio],
           ['class'=>'btn btn-default']);
}
//$data= ['label'=>'2023', 'url' => \yii\helpers\Url::to(['/mpi/turno/restar','anio'=>'2023','indice'=>$indice]), 'label'=>'2022','url' => \yii\helpers\Url::to(['/mpi/turno/restar','anio'=>'2022','indice'=>$indice]),'label'=>'2021','url' => \yii\helpers\Url::to(['/mpi/turno/restar','anio'=>'2021','indice'=>$indice])];

//$stringToolbar=$stringToolbar.Html::dropDownList($anio,  null, ["2023"=>2023,"2022"=>2022,"2021"=>2021], $options = ["class"=>'btn btn-default','value' => 'none','onchange'=>'seleccionAnio(this)']);
   
    $stringToolbar=$stringToolbar.ButtonDropdown::widget([
        'label' => $anio,
        'options'=>[
            'class'=>'btn btn-default'
        ],
        
        'dropdown' => [
            'items' => [
            ['label'=>2024, 'url' => \yii\helpers\Url::to(['index','indice'=>$indice,"tipo"=>"anio",'anio'=>2024])], 
            ['label'=>2023, 'url' => \yii\helpers\Url::to(['index','indice'=>$indice,"tipo"=>"anio",'anio'=>2023])], 
            ['label'=>2022,'url' => \yii\helpers\Url::to(['index','indice'=>$indice,"tipo"=>"anio",'anio'=>2022])],
            ['label'=>2021,'url' => \yii\helpers\Url::to(['index','indice'=>$indice,"tipo"=>"anio",'anio'=>2021])]],
               
            
        
    ]]);



if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-chevron-right"></i>', ['index','indice'=> $indice,"tipo"=>"sumar","anio"=>$anio],
            ['class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-list-alt"></i>', ['reporte'],
            ['class'=>'btn btn-default','title'=> 'reporte']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);

}
/*if ($permisos['exportall']){
    $stringToolbar=$stringToolbar.Html::a('exportar todo',['allexport','indice'=>$indice,'anio'=>$anio],['class'=>'btn btn-primary','onclick' => new JsExpression("window.open(this.href,'_self'); return false;")]);
   

}*/
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}
?>
<div class="turno-index"> <!--  style='width:100%;margin-right: auto;margin-left: auto;' -->
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [  ['content' => $stringToolbar],],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
                
            ],
           
            'options' => ['style' => 'width: fit-content;'], 
   
        ])?>
    </div>
</div>

<script>
    function activarExport(){
        document.getElementById('form1').addEventListener('submit', function(evt){
    evt.preventDefault();
 
})

    }
    </script>
<!-- <style> .modal-dialog { width: 900px !important; } </style> -->
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>

<?php Modal::end(); ?>