<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
?>
<link rel="stylesheet" type="text/css" href="<?= Yii::$app->request->baseUrl ?>/css/detail.css" />
<script>
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
</script>
<?php
$stringToolbar="";

/*if ($permisos['exportdetalle']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['exportdetalle','accion'=>$_GET['r'],'id_maestro'=>$id_maestro],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}*/
// agrego acciones a $columns


$columns[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
    'template' => '{viewdetalle}',
    
    'buttons' => [
        'viewdetalle' => function ($url) {

            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
     
        
    ],
        'urlCreator' => function ($action, $searchModel) {
 
            $url=Url::to([$action,'id_paciente'=>$searchModel->id]);
            return $url;
    }
    /*'visibleButtons'=>[
        'viewdetalle'=> $permisos['viewdetalle'],*/
//        'updatedetalle'=> $permisos['updatedetalle'],
        //'deletedetalle'=> $permisos['deletedetalle']
    ]

?>

<div class="detalle-expand">

    <div style="font-size: 15px;"><b>Pacientes</b></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id'=>'crud-detail',
        'layout' => '{items}',
        'pjax'=>true,
        'columns' => $columns,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
    ]);
        
    ?>
</div>
