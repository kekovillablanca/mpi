<?php

use app\modules\mpi\models\Edad;
use app\modules\mpi\models\Establecimiento;

use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\select2\Select2Asset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\helpers\Url;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\Alert;

use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\Turno */
/* @var $form yii\widgets\ActiveForm */
?>
<script >
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
  
$("#turno-fecha_turno").on('change', function() {

    var fechas=this.value.split('/');
    var fechaDiaTurno=fechas[0];
    var fechaMesTurno=fechas[1];
    var fechaAnioTurno=fechas[2];
    fechaMesTurno=parseInt(fechaMesTurno)-1;
    fechaAnioTurno=parseInt(fechaAnioTurno);
    fechaDiaTurno=parseInt(fechaDiaTurno);
    var fecha=new Date();
    var fechaMes=fecha.getMonth();
    var fechaDia=fecha.getUTCDate();
    var fechaAnio=fecha.getUTCFullYear();

    //verifica que la fecha del turno asignada sea menor a el año actual
    //if(fechaMesTurno==11 && fechaDia>0 && fechaDia<7 && fechaDiaTurno>24){
   
    
    if(fechaAnioTurno<fechaAnio ){
        
        document.getElementById('button-guardar').disabled=true;
        activarAlerta('Fecha pasada','El año debe ser actual',"#f8d7da","#721c24");
        if(document.getElementById('button-guardar-siguiente')!=null)
        document.getElementById('button-guardar-siguiente').disabled=true;
        
    }
    else{
        document.getElementById('button-guardar').disabled=false;
        document.getElementById('button-guardar-siguiente').disabled=false;
        document.getElementById('turno-id_diagnostico').disabled=false;
    }
    //verifica si una fecha de turno es pasada la actual no se podra asignar un diagnostico
    if((fechaAnio<fechaAnioTurno || fechaAnio==fechaAnioTurno && fechaMesTurno>fechaMes)
){      
        activarAlerta('Fecha futura','Debe ser una fecha actual',"#f8d7da","#721c24");
        document.getElementById('button-guardar').disabled=true;
        document.getElementById('button-guardar-siguiente').disabled=true;
    }
});

function verIdArea(){
    let idArea=document.getElementById('turno-id_area');
    return idArea.value;
}

function verificarProfesionales(){
    let valorCheckBox=document.getElementById('checkbox');
    if(valorCheckBox.checked){
        return false;
    }
    else{
        return true;
    }

}
function activarAlerta(titulo,descripcionTexto,color,colorTitulo){
    document.getElementById("alerta").style.display="block"
    document.getElementById("alerta").style.backgroundColor=color
    document.getElementById("descripccionAlerta").innerText=descripcionTexto
    document.getElementById("tituloAlerta").innerText = titulo
    document.getElementById("tituloAlerta").style.color=colorTitulo;

    setTimeout(function() {
                       document.getElementById("alerta").style.display="none"
           }, 5000);
} 
function verFecha(event){
   /* var fecha=document.getElementById("turno-fecha_turno").value.split('/');
    */
   
    var fechaMesTurno=event.date.getMonth();
    var fechaDiaTurno=event.date.getDay();
    var fechaAnioTurno=event.date.getUTCFullYear();
    var fecha=new Date();
    var fechaMes=fecha.getMonth();
    var fechaDia=fecha.getDay();
    //var fechaDia=fecha.getUTCDate();
    var fechaAnio=fecha.getUTCFullYear();
    //verifica que la fecha del turno asignada sea menor a el año actual
 

    if(fechaAnioTurno<fechaAnio-1){
        document.getElementById('button-guardar').disabled=true;
        activarAlerta('Fecha pasada','El año debe ser actual',"#f8d7da","#721c24");
        if(document.getElementById('button-guardar-siguiente')!=null)
        document.getElementById('button-guardar-siguiente').disabled=true;
    }
    else{
      
        document.getElementById('button-guardar').disabled=false;
        document.getElementById('button-guardar-siguiente').disabled=false;
        document.getElementById('turno-id_diagnostico').disabled=false;
        
    }
    //verifica si una fecha de turno es pasada la actual no se podra asignar un diagnostico
    if((fechaAnio<fechaAnioTurno || fechaAnio==fechaAnioTurno && fechaMesTurno>fechaMes)
){
        activarAlerta('Fecha futura','Debe ser una fecha actual',"#f8d7da","#721c24");
        document.getElementById('button-guardar').disabled=true;
        document.getElementById('button-guardar-siguiente').disabled=true;
    }
}

function habilitarEspecialidad(){
    let idEspecialidad=document.getElementById('turno-id_especialidad');

    idEspecialidad.value=-1;
    idEspecialidad.value="";
    idEspecialidad.textContent="";
  
    
    idEspecialidad.disabled=false;
}
function getIdServicio(){
    let idServicio=document.getElementById('turno-id_servicio');
    idServicio=idServicio.value;
    
    return idServicio
}


    
function submitEncabezado(){
    var $form = $('#form-encabezado');
    var data = $form.serialize();

    $.ajax({
    url: '<?php echo Url::to(['create2']) ?>',
    dataType: 'json',
    type:"POST",
    data: data,
    success: function(data) {
        if(data.status == 'ok'){
            document.getElementById("turno-edad").value = '';
            document.getElementById("turno-id_paciente").value=-1;
            document.getElementById("turno-id_unidad_edad").value=-1;
            document.getElementById("turno-id_diagnostico").value='';
            $("#turno-ubicacion_hc").attr("readonly", false);
            document.getElementById("select2-turno-id_paciente-container").innerText ="seleccionar paciente...";
            document.getElementById("select2-turno-id_unidad_edad-container").innerText = 'seleccionar unidad edad...';
            document.getElementById("turno-ubicacion_hc").value = '';
            document.getElementById("select2-turno-id_diagnostico-container").innerText = 'Seleccionar diagnostico...';
            document.getElementById("historiaclinica-historia_clinica").value = '';
            document.querySelectorAll('#form-encabezado input[type=checkbox]').forEach(function(checkElement) {
            checkElement.checked = false;
            });
            document.querySelectorAll('#form-encabezado input[type=radio]').forEach(function(radioElement) {
            radioElement.checked = false;
            });
            
            document.getElementById("turno-observacion").value = '';
            document.getElementById("turno-id_paciente").value=-1;
            document.getElementById("turno-id_paciente").focus();

            //dejo todo igual y muestro cartel rojo
          
            activarAlerta('Se creo el turno','Se cargo correctamente',"#d4edda","#155724");
           
            $.pjax.reload('#' + $.trim('crud-datatable-pjax'), {timeout: 3000});
        }else if(data.status === 'errorFuturo'){
            //dejo todo igual y muestro cartel rojo
            activarAlerta('Fecha futura','Debe ser una fecha actual',"#f8d7da","#721c24");
            
         
        }
        else if(data.status === 'errorPasado'){
            //dejo todo igual y muestro cartel rojo
            activarAlerta('Fecha pasada','El año debe ser actual',"#f8d7da","#721c24");
            
       
        }
        else if(data.status === "error"){

            activarAlerta('Hubo un error','Quedaron campos vacios',"#f8d7da","#721c24");
            
       
        }
    }
    });
    
  


}
$('#turno-id_paciente').on("change", function(e) { 
        verHistoriaClinica(e.target.value);
    });
var historiaActiva=false;
function verHistoriaClinica(event){


    
    $.ajax({
            url: '<?php echo Url::to(['historia']) ?>',
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            type:"POST",
            data: event,
            success: function(data) {
                
                if (data.status==="error"){
                    $("#historiaclinica-historia_clinica").val("");
                    $("#historiaclinica-historia_clinica").attr("readonly", false);
        
            }
                //const datos=JSON.stringify(data);
            else if(data.status==="ok"){
                $("#historiaclinica-historia_clinica").val(data.historia.historia_clinica);
                $("#historiaclinica-historia_clinica").attr("readonly", true);
                historiaActiva=true;
            }
            }
            
        });

}
function agregarHistoriaClinica(event){
    var input = document.getElementById(event.id);
    const boton = document.getElementById('accept');

            if(historiaActiva==true){
                boton.style.display='block';
                document.getElementById('changeHistory').style.display='block';
                boton.addEventListener('click', function() {
                $("#historiaclinica-historia_clinica").attr("readonly", false);
                //document.getElementById('changeHistory').style.display='none';
               // historiaActiva=false;
               boton.style.display='none';
            })
            } 
    }
    




</script>

<div class="turno-form">

<?php
 $form = ActiveForm::begin(['id' => 'form-encabezado']);

 echo(Html::label('Crear paciente','paciente',['style' => ['width' => '100px', 'height' => '20px']])); 
 echo("<br/>");
 echo(Html::a('
 <i class="glyphicon glyphicon-plus"></i>', ['createpaciente'],
 ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']));

 ?>
<?= $form->field($model, 'id_paciente')->widget(Select2::class, [
    'options' => ['placeholder' => 'Select paciente ...'],
    'data' => $listaPaciente,
    'pluginOptions' => [
    'minimumInputLength' => 3,
    'allowClear' => true,
    'language' => [
    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
    ],
    'ajax' => [
    'url' => \yii\helpers\Url::to(['/mpi/paciente/list']),
    'dataType' => 'json',
    'data' => new JsExpression('function(params) { 

        return {q:params.term}; }'),
        ],
    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
    'templateResult' => new JsExpression('function(id_paciente) { return id_paciente.text; }'),
    'templateSelection' => new JsExpression('function(id_paciente) {  
        return id_paciente.text; }'),
        ]
 ]);?>
 



<?php echo $form->field($model, 'id_servicio')->widget(Select2::classname(), [
     'options' => ['placeholder' => 'Select servicio ...'],
     'data' => $listaServicios,
     'pluginOptions' => [
     'minimumInputLength' => 3,
     'allowClear' => true,
     'language' => [
         'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
     ],
     'ajax' => [
         'url' => \yii\helpers\Url::to(['/mpi/pservicio/list']),
         'dataType' => 'json',
         'data' => new JsExpression('function(params) { 
           
            return {q:params.term}; }'),
     ],
     'escapeMarkup' => new JsExpression('function(markup) {return markup; }'),
        'templateResult' => new JsExpression('function(id_servicio) {  return id_servicio.text; }'),
        'templateSelection' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
    ],

    'pluginEvents' => [
        "change" => new JsExpression("function(e) {
            habilitarEspecialidad();
        }"
            
     )],
]);  
?>
<?php     

echo $form->field($model, 'id_especialidad')->widget(Select2::classname(), [
    'options' => ['placeholder' => 'Select especialidad ...'],
    'data' => $listaEspecialidad,
    'pluginOptions' => [
    'allowClear' => true,
    'language' => [
        'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
    ],
    'ajax' => [
        'url' => \yii\helpers\Url::to(['/mpi/especialidad/data']),
        'dataType' => 'json',
        'data' => new JsExpression('function(params) { 
            return {id:getIdServicio()}; }'),
    ],
    'escapeMarkup' => new JsExpression('function(markup) {return markup; }'),
       'templateResult' => new JsExpression('function(id_servicio) {  return id_servicio.text; }'),
       'templateSelection' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
   ],
]);
?>
<?php 
     echo $form->field($model, 'id_area')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Select a establecimiento ...'],
        'data' => $listaAreas,
        'pluginOptions' => [
        'allowClear' => true,
        'language' => [
            'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
        ],
        /*'ajax' => [
            'url' => \yii\helpers\Url::to(['/mpi/establecimiento/list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],*/
        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(id_area) { 
                        
                      
                        return id_area.text; }'),
                    'templateSelection' => new JsExpression('function(id_area) { 
                        
                        return id_area.text; }'),
                   
                ],
    ]); ?>
    <div>
      <input type="checkbox" id="checkbox" name="profesionales" value="generales"
             >
      <label for="generales">profesional generales</label>
    </div>
  
        
<p id="error">

</p>
<?php 

    echo $form->field($model, 'id_profesional')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Select profesional ...'],
        'data' => $listaProfesional,
        'pluginOptions' => [
        'minimumInputLength' => 3,
        'allowClear' => true,
        'language' => [
            'errorLoading' => new JsExpression("function() { return 'no se encontro nada, seleccione profesional generales o un establecimiento'; }"),
        ],
        'ajax' => [
            'url' => \yii\helpers\Url::to(['/mpi/profesional/list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { 
                if(verificarProfesionales() && verIdArea()!="" ){
               
                return {q:params.term,
                        id:verIdArea()}; 
                    }   
            else if(verificarProfesionales()==false){
                return {q:params.term,id:null};
            }         
        }')
        ],
        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(id_profesional) { 
                        return id_profesional.text; }'),
                    'templateSelection' => new JsExpression('function(id_profesional) { 
                        return id_profesional.text; }'),
                ],   
    ]);  
    echo $form->field($model, 'fecha_turno')->widget(DatePicker::className(),[
    'options' => ['placeholder' => 'Ingrese una fecha' ],
    'pluginOptions' => ['autoclose'=>true],
    'language' => 'es',
    'pluginEvents' =>[
        "changeDate" => "function(e) {  verFecha(e)}",
    ] ,
   
        

   
    ])->label('Fecha turno');
             function datosRepetir($form,$model,$listaEdad,$listaDiagnostico,$historia){
    echo $form->field($model, 'edad')->textInput();
    echo $form->field($model, 'id_unidad_edad')->widget(Select2::classname(), [
        'data' => $listaEdad,
        'options' => ['placeholder' => Yii::t('app', 'Seleccione unidad edad') . '...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    echo $form->field($model, 'ubicacion_hc')->textInput(['maxlength' => true]);
    echo $form->field($historia, 'historia_clinica')->textInput(['maxlength' => true,'onclick'=>"agregarHistoriaClinica(this)"]);

//display: flow-root;
    ?>
  <div id="changeHistory" style=" z-index: 2;
    display: none;
    bottom:15px;
    position: relative;" class="alert-info" role="alert">
   <button id="accept"  class="btn btn-info" style=" float:right;">Modificar</button>
</div>
<?php
    echo $form->field($model, 'id_diagnostico')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Select diagnostico ...'],
        'data' => $listaDiagnostico,
        'pluginOptions' => [
            'minimumInputLength' => 3,
            'allowClear' => true,
            'language' => [
                'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/mpi/diagnostico/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
                        'templateSelection' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
                    ],
        
          
          
                ]);

    echo $form->field($model, 'control_embarazo')->checkbox();
    echo $form->field($model, 'nino_sano')->checkbox();
    echo $form->field($model, 'primera_vez')->checkbox();
    echo $form->field($model, 'presente')->radioList([true=>"presente",
   false=>"ausente"
    ]);
    echo $form->field($model, 'observacion')->textarea(['maxlength' => true]);
                
}
datosRepetir($form,$model,$listaEdad,$listaDiagnostico,$historia);
?>
<?php if($esUpdate==false){ ?>
    <script >
$( document ).ready(function(){
    document.getElementById("turno-id_especialidad").disabled=true;
    
})
</script>
<div style="margin-bottom:5% ;">
<?php echo Html::button('Guardar y agregar siguiente', ['class' => 'btn btn-primary pull-right', 'name' => 'Guardar y agregar siguiente',
                         'onclick' => 'submitEncabezado()','id' => 'button-guardar-siguiente']);?>
</div>




</div>
<div id="alerta" style=" z-index: 2;
    margin-top: 5.8%;
    position: fixed;
    top: 70%;
    right: 5px;
    width: 71%;
    height: 12%;
    display: none;" class="alert alert-danger" role="alert">
    <h1 id="tituloAlerta"style="text-decoration: underline; text-align: center;"></h1>
    <h4 id="descripccionAlerta" style=" margin-right: 5%;  font-family: sans-serif;  text-align: center; text-indent:2%; color: black;"> </h4>
 
</div>
<?php }?>
<?php ActiveForm::end(); ?>
