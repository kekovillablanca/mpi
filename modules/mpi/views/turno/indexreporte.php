<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Widget;
use yii\web\JsExpression;
use \yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\mpi\models\TurnoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'IECMA';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);


// agrego acciones a $columns
$subtitle="Reporte consultas año 2023";

$stringToolbar="";


if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-list-alt"></i>', ['index'],
            ['class'=>'btn btn-default']);
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['exportreporte'],
            ['target'=>'_blank','title'=> 'Exportar','class'=>'btn btn-default']);
}


?>
<div class="turno-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
                'before'=>"<h3>".$subtitle."</h3>"
            ]
        ])?>
    </div>
</div>


<?php
Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
]);
Modal::end();
