<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
?>
<link rel="stylesheet" type="text/css" href="<?= Yii::$app->request->baseUrl ?>/css/detail.css" />

<?php
$stringToolbar="";

if ($permisos['indexdetalle']){
$stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['createdetalle','id_paciente'=> $id_paciente],
['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}


// agrego acciones a $columns


$columns[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
    'template' => '{viewdetalle} {editdetalle} {deletedetalle} ',
    
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
            'editdetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'editar registro',]);
                },
            'deletedetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'eliminar registro',]);
                },
       
     
        
    ],
        'urlCreator' => function ($action, $searchModel) {
           
            $url=Url::to([$action,'id_historia'=>$searchModel->id]);
            return $url;
    },
]

?>

<div class="detalle-expand">

    <div style="font-size: 15px;"><b>historia clinicas</b></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id'=>'crud-detail',
        'layout' => '{items}',
        'pjax'=>true,
        'columns' => $columns,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
    ]);
        
    ?>
</div>
