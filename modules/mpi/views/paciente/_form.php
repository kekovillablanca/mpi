<?php

use app\modules\internacion\models\Asistio;
use app\modules\internacion\models\Educacion;
use app\modules\mpi\models\Provincia;
use app\modules\mpi\models\Sexo;
use app\modules\mpi\models\TipoDocumento;
use app\modules\mpi\models\TipoPlanSocial;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\Paciente */
/* @var $form yii\widgets\ActiveForm */

?>

<script>

$(document).ready(function() {
    //obtneer el id por php
    var internacionId = <?= json_encode($model->id) ?>;
    var sistemaEducativo=document.getElementById("sistemaeducativo-id_asistio").value;

    hablitarSistemaEducativo(sistemaEducativo);


});
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
$(document).ready(function() {
    $('#btn-guardar').on('click', function() {
        $('#form-dni').submit(); // Disparar el evento de submit manualmente
    });

    $('#form-dni').on('submit', function(e) {
        e.preventDefault(); // Evita el envío normal del formulario
        console.log("entro a submit");
        $.ajax({
            type: 'POST',
            url: $(this).attr('create'),
            data: $(this).serialize(),
            success: function(response) {
                if (response.success === false) {
                    showErrorModal(response.message); // Mostrar el modal de error
                } else {
                    // Manejar la respuesta en caso de éxito
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Error: ", textStatus, errorThrown);
                showErrorModal('Error inesperado: ' + errorThrown); // Mostrar error inesperado
            }
        });
    });
});
$(document).ready(function() {
    function actualizarCampoDocumento() {
        var tipoDocumento = $('#paciente-id_tipo_documento').val(); // Obtiene el valor del tipo de documento
        // Limpia el campo numero_documento y remueve el patrón anterior
        $('#paciente-numero_documento').val(''); // Limpia el campo
        $('#paciente-numero_documento').removeAttr('pattern'); // Elimina el patrón anterior
        $('#paciente-numero_documento').prop('required', false); // Lo hace opcional al inicio

        // Verifica el tipo de documento y el estado del checkbox
        if (tipoDocumento == 1) {
            // Si el tipo de documento es DNI
          
                // Si no es documento ajeno, usa el patrón estándar para DNI
                $('#paciente-numero_documento').attr('pattern', '^[0-9]{1,10}$');
                $('#paciente-numero_documento').prop('required', true); // Lo hace requerido

                $('#paciente-numero_documento').off('input').on('input', function() {
                    var value = $(this).val();
                    // Reemplaza cualquier carácter no numérico
                    value = value.replace(/[^\d]/g, '');
                    // Limita la longitud a 8 dígitos
                    if (value.length > 10) {
                        value = value.slice(0, 10);
                    }
                    $(this).val(value); // Actualiza el valor del campo
                });
            
        } else if (tipoDocumento == 2 || tipoDocumento == 3) {
            // Si es tipo 2 o 3 (otros documentos), permite letras y números sin restricciones
            $('#paciente-numero_documento').off('input').removeAttr('pattern').val('').prop('required', false);
        } else if (tipoDocumento == 4) {
            // Si es tipo 4 (Pasaporte), permite exactamente 9 caracteres alfanuméricos
            console.log("Tipo de documento 4 seleccionado");

            $('#paciente-numero_documento').off('input').val('').prop('required', true).attr('pattern', '^[a-zA-Z0-9]{9}$');

            $('#paciente-numero_documento').on('input', function() {
                var value = $(this).val();
                // Reemplaza cualquier carácter que no sea alfanumérico
                value = value.replace(/[^a-zA-Z0-9]/g, '');
                // Limita la longitud a 9 caracteres
                if (value.length > 9) {
                    value = value.slice(0, 9);
                }
                $(this).val(value); // Actualiza el valor del campo
            });
        } else {
            // Si el tipo de documento es null o vacío, deshabilita el campo
            $('#paciente-numero_documento').off('input').val('').prop('required', false);
        }
    }
    // Manejar el cambio en el tipo de documento
    $('#paciente-id_tipo_documento').change(function() {
        console.log('Cambio en el tipo de documento');
        actualizarCampoDocumento(); // Llama a la función para actualizar el campo
    });
})

function actualizarLocalidades(provinciaId) {
    if (!provinciaId) return;

    $.ajax({
        url: '<?php echo Url::to(['/mpi/paciente/provinciadelocalidad', 'provinciaId' => '']); ?>' + provinciaId,
    type: 'GET',
    dataType: 'json',
        success: function(data) {
            let select = $('#paciente-id_localidad');
            select.empty().trigger('change'); // Limpia el select
 
            select.append(new Option('Seleccione localidad', '')); // Placeholder
            data.forEach(item => {
                select.append(new Option(item.nombre, item.id));
            });
            select.trigger({
                type: 'select2:selecting',
                params: { data: data } // Evento opcional para manejar los nuevos datos
            });
        }
    });
}

$('#provincia-id').on('change', function() {
    let provinciaId = $(this).val();

    actualizarLocalidades(provinciaId);
});
function habilitarObraSocial(event){

var obraSocial=document.getElementById("paciente-id_obra_social").parentElement;
if(event.target.value==5){
    obraSocial.style.display='none';
}
else{
    obraSocial.style.display='block';
}
}

function hablitarSistemaEducativo(value) {
    const sistemaEducativoElement = document.getElementById("sistemaEducativo");
    
    // Verificar si el sistema educativo debe ser ocultado
    sistemaEducativoElement.style.display = 'none';
    if (value == 2) {
        sistemaEducativoElement.style.display = 'block';
        document.getElementById('sistemaeducativo-primario_completo').required = true;
        document.getElementById('sistemaeducativo-secundario_completo').required = true;
        document.getElementById('sistemaeducativo-superior_completo').required = true;
        document.getElementById('sistemaeducativo-universitario_completo').required = true;
        document.getElementById(
        sistemaEducativo.primario_completo === 'PRIMARIO COMPLETO' 
        ? "sistemaeducativo-primario-completo--0" 
        : "sistemaeducativo-primario-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'SECUNDARIO COMPLETO' 
        ? "sistemaeducativo-secundario-completo--0" 
        : "sistemaeducativo-secundario-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'SUPERIOR COMPLETO' 
        ? "sistemaeducativo-superior-completo--0" 
        : "sistemaeducativo-superior-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'UNIVERSITARIO COMPLETO' 
        ? "sistemaeducativo-universitario-completo--0" 
        : "sistemaeducativo-universitario-completo--1"
        ).checked = true;
    }


}
function buscarPersona(event){
    var tipoDocumento=document.getElementById("paciente-id_tipo_documento").value;

    if(tipoDocumento==1){
        $.ajax({
            url: '<?php echo Url::to(['buscarnumerodocumento']) ?>',
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            type:"POST",
            data: event,
            success: function(data) {
                //const datos=JSON.stringify(data);
                if(data.status==="ok"){
               
        
               
                document.getElementById("paciente-nombre").value=data["paciente"].nombre;
                document.getElementById("paciente-apellido").value=data["paciente"].apellido
                document.getElementById("paciente-id_sexo").value=data["paciente"].id_sexo;
                document.getElementById("paciente-fecha_nacimiento").value=data["paciente"].fecha_nacimiento;
                document.getElementById("alerta").style.display="block"
                document.getElementById("alerta").style.backgroundColor="#a5ffa5";
                document.getElementById("tituloAlerta").innerText = 'Se encontro el paciente en renaper';
                document.getElementById("tituloAlerta").style.color="#3a75ff";
        setTimeout(function() {
                        document.getElementById("alerta").style.display="none"
            }, 5000);
                }
                else if(data.status==="error"){
                    document.getElementById("paciente-nombre").value=data["paciente"].nombre;
                document.getElementById("paciente-apellido").value=data["paciente"].apellido
                document.getElementById("paciente-id_sexo").value=data["paciente"].id_sexo;
                document.getElementById("paciente-fecha_nacimiento").value=data["paciente"].fecha_nacimiento;
                document.getElementById("alerta").style.display="block"
                document.getElementById("alerta").style.backgroundColor="#FFF3CD";
                document.getElementById("tituloAlerta").innerText = 'No se encontro un paciente';
                document.getElementById("tituloAlerta").style.color="#721c24";
        setTimeout(function() {
                        document.getElementById("alerta").style.display="none"
            }, 5000);
                }
                
            }
            
        });
    }
    
}

</script>
<div class="paciente-form">


    <?php

    $form = ActiveForm::begin(['id' => 'form-dni'// shows tooltip styled validation error feedback
    ,'fieldConfig' => ['options' => ['class' => 'form-group mb-3 mr-2 me-2']], // spacing field groups
    'options' => ['style' => 'align-items: flex-start']]);
  if($documento_ajeno==true){
echo $form->field($model, 'documento_ajeno')->checkbox([
    'label' => Yii::t('app', 'Documento ajeno'),
    'uncheck' => '0',  // Enviar '0' si no está marcado
    'value' => '1',    // Enviar '1' si está marcado
    'labelOptions' => [
        'style' => 'color: #333; font-size: 18px; font-weight: bolder;',  // Color oscuro y tamaño de fuente más grande
    ]

]); 
}
   $tipoDocumento=ArrayHelper::map(TipoDocumento::find()->asArray()->all(), 'id', 'nombre');
   echo $form->field($model, 'id_tipo_documento')->widget(Select2::classname(), [
    'data' => $tipoDocumento,
    'options' => ['placeholder' => Yii::t('app', 'Seleccione tipo de documento') . '...'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
])->label('Tipo de Documento');
echo $form->field($model, 'numero_documento')->textInput([
    'onkeyup' => "buscarPersona(this.value)", // Llama a la función buscarPersona
]);?>
    <div id="alerta" style="    z-index: 2;
    margin-top: 8.8%;
    position: fixed;
    top: 11%;
    right: 18%;
    width: 55%;
    height: 3.2%;
    display: none;
    background-color: rgb(248, 215, 218);" class="alert alert-danger" role="alert">
    <p id="tituloAlerta"style="font-size:large; text-decoration: underline; text-align: center; margin: -11px 0 10px;"></p>
  
    </div>
    <?php
    
	echo $form->field($modelHistoria, 'historia_clinica')->textInput(['maxlength' => true]);
    
	echo $form->field($model, 'nombre')->textInput(['maxlength' => true,'style' => 'text-transform:capitalize']);
	echo $form->field($model, 'apellido')->textInput(['maxlength' => true,'style' => 'text-transform:capitalize']);
	 
    $sexos=ArrayHelper::map(Sexo::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'id_sexo', ['options'=>['style' => 'display: block;'] ])->dropDownList($sexos,['prompt'=>'Seleccionar...','style'=>'width:215px']);

		$dataGenero=ArrayHelper::map(app\modules\mpi\models\Genero::find()->asArray()->all(), 'id', 'nombre');
    	echo $form->field($model, 'id_genero')->dropDownList($dataGenero,['prompt'=>'Seleccione genero']);
        echo $form->field($model, 'fecha_nacimiento')->widget(DatePicker::className(),[
            'options' => ['placeholder' => 'Ingrese una fecha de nacimiento' ],
            'pluginOptions' => ['autoclose'=>true],
            'language' => 'es',
            ])->label('Fecha nacimiento');
        $data=ArrayHelper::map(app\modules\mpi\models\ObraSocial::find()->asArray()->all(), 'id', 'nombre');
        if($model->id_obra_social==null){
            $model->id_obra_social=1;
        }
        
        $planSocial=ArrayHelper::map(TipoPlanSocial::find()->asArray()->all(), 'id', 'nombre');
        echo $form->field($model, 'id_tipo_plan_social', [
            'labelOptions' => ['style' => 'font-weight: bold; display: block;'],
       ])->dropDownList($planSocial,['prompt'=>'Seleccione una opción','onchange' => 'habilitarObraSocial(event)'])->label("Tipo de plan");
      
        echo $form->field($model, 'id_obra_social')->widget(Select2::classname(), [
            'data' => $data,   
            'options' => ['placeholder' => Yii::t('app', 'Seleccione obra social') . '...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]);
  
        
            echo $form->field($model, 'id_establecimiento')->widget(Select2::classname(), [
                'data' => $establecimiento,
                'options' => ['placeholder' => Yii::t('app', 'Seleccione establecimiento') . '...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label('Establecimiento');
            
         
    $arrayProvincia = ArrayHelper::map(Provincia::find()->asArray()->all(), 'id', 'nombre');

   echo $form->field($localidad, 'id_provincia', [
        'options' => ['style' => 'width: 50%;'], 
        'labelOptions' => ['style' => 'font-weight: bold;']
    ])->widget(Select2::classname(), [
        'data' => $arrayProvincia,
        'options' => [
            'placeholder' => 'Select provincia', // Placeholder visible hasta seleccionar una opción
            'id' => 'provincia-id', // Asegúrate de que el ID sea correcto
        ],
        'pluginOptions' => [
            'allowClear' => true, // Agrega la opción para limpiar la selección y mostrar el placeholder de nuevo
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10
        ],

    ])
    ->label('Provincia');
            echo $form->field($model, 'id_localidad')->widget(Select2::classname(), [
                'data' => $listaLocalidad,
                'options' => ['placeholder' => Yii::t('app', 'Seleccione localidad') . '...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label('Localidad');
        	echo $form->field($contacto, 'numero_telefono')->textInput([
                'maxlength' => true,               
                'pattern' => '[0-9]*', // HTML5 pattern to allow only numbers
                'inputmode' => 'numeric', // Show numeric keyboard on mobile devices
                'oninput' => 'this.value = this.value.replace(/[^0-9]/g, "")', // Restrict input to numbers
                ]);


    ?>
    <?php 
$asistio=ArrayHelper::map(Asistio::find()->asArray()->all(), 'id', 'nombre');
echo $form->field($sistemaEducativo, 'id_asistio', [
    'labelOptions' => ['style' => 'font-weight: bold; display: block;'] ])->dropDownList($asistio,['prompt'=>'Seleccione una opción','onchange' => 'hablitarSistemaEducativo(event.target.value)'])->label("Asistio sistema educativo");

    ?>

<div id="sistemaEducativo" style="display: none;">

<?= $form->field($sistemaEducativo, 'primario_completo')->radioList(
        [Educacion::PRIMARIO_COMPLETO => Educacion::PRIMARIO_COMPLETO,Educacion::PRIMARIO_INCOMPLETO => Educacion::PRIMARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <?= $form->field($sistemaEducativo, 'secundario_completo')->radioList(
        [Educacion::SECUNDARIO_COMPLETO => Educacion::SECUNDARIO_COMPLETO, Educacion::SECUNDARIO_INCOMPLETO => Educacion::SECUNDARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <?= $form->field($sistemaEducativo, 'superior_completo')->radioList(
        [Educacion::SUPERIOR_COMPLETO => Educacion::SUPERIOR_COMPLETO, Educacion::SUPERIOR_INCOMPLETO => Educacion::SUPERIOR_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <?= $form->field($sistemaEducativo, 'universitario_completo')->radioList(
        [Educacion::UNIVERSITARIO_COMPLETO => Educacion::UNIVERSITARIO_COMPLETO, Educacion::UNIVERSITARIO_INCOMPLETO => Educacion::UNIVERSITARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>


</div>

<?php $form->end(); ?>
</div>