<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;
$this->title = 'Pacientes';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

// agrego acciones a $columns
if ($permisos['indexdetalle']) {
    $columns = array_merge(array(array(
        'class' => '\kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => Url::to(['indexdetalle','id_paciente'=>$dataProvider->id]),
        'detailRowCssClass' => 'expanded-collapse',
        'expandIcon' => '<i class="glyphicon glyphicon-expand" style="color:#337ab7"></i>',
        'collapseIcon' => '<i class="glyphicon glyphicon-collapse-down" style="color:#337ab7"></i>',
        'detailAnimationDuration' => 100,
        'allowBatchToggle' => false,
    )), $columns);
}

$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Paciente',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]
            
    ];

$stringToolbar="";

if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){

    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

?>
<div class="paciente-index"> <!--  style='width:100%;margin-right: auto;margin-left: auto;' -->
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ]  , 'options' => ['style' => 'width: fit-content;'], 
        ])?>
    </div>
</div>
<!-- <style> .modal-dialog { width: 900px !important; } </style> -->
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>