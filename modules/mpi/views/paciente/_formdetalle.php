<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\HistoriaClinica */
/* @var $form yii\widgets\ActiveForm */
?>
<script>
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
</script>
<div class="historia-clinica-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'historia_clinica')->textInput(['maxlength' => true]);   // ->textarea()?>

    <?php 
        
        echo $form->field($model, 'id_establecimiento')->widget(Select2::classname(), [
            'data' => $establecimiento,
            'options' => ['placeholder' => Yii::t('app', 'Seleccione establecimiento') . '...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label('establecimiento');
        
        /*
        echo $form->field($model, 'id_establecimiento')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => ($model->id_establecimiento)?$model->consultasestadisticas.establecimiento->nombre:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['consultasestadisticas.establecimiento/list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_establecimiento) { return id_establecimiento.text; }'),
                'templateSelection' => new JsExpression('function(id_establecimiento) { return id_establecimiento.text; }'),
            ],
        ]);
        */
    ?>

    <?php ActiveForm::end(); ?>
    
</div>