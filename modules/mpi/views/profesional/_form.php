<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\Profesional */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profesional-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'dni')->textInput();       // ->textarea()?>

    <?php echo $form->field($model, 'nombre')->textInput();       // ->textarea()?>

    <?php echo $form->field($model, 'apellido')->textInput();       // ->textarea()?>

    <?php echo $form->field($model, 'matricula')->textInput();        // ->textarea()?>


    <?php ActiveForm::end(); ?>
    
</div>
