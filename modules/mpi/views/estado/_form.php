<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\EstadoTurno */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estado-turno-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'id')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]);       // ->textarea()?>

    <?php ActiveForm::end(); ?>
    
</div>
