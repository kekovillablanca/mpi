<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */

?>

<div class="modelos-form">
    <b>CONSULTA AL FEDERADOR</b><br><br>

    <table id="w0" class="table table-striped table-bordered detail-view">
    <tbody>

    <?php

    if (is_string($return)){

        echo $return;

    }else {

        foreach ($return["entry"] as $entry) {

            echo "<tr>";
            $apellido="";
            foreach ($entry["resource"]["name"]["0"]["_family"]["extension"] as $_family) {
                $apellido = $apellido." ".$_family["valueString"];
            }
            $nombre="";
            foreach ($entry["resource"]["name"]["0"]["given"] as $name) {
                $nombre=$nombre." ".$name;
            }

            $gender = $entry["resource"]["gender"];
            $birthDate = $entry["resource"]["birthDate"];
            $score = $entry["search"]["score"];

            echo "<td style='font-weight: bold;'>$apellido, $nombre</td>";
            echo "<td style='font-weight: bold;'>".$gender." / ".$birthDate." / Score:".$score."</td>";

            echo "</tr>";

            $identifier = $entry["resource"]["identifier"];

            foreach ($identifier as $key1 => $value_aux) {
                echo "<tr>";
                $system = $value_aux['system'];
                $value = $value_aux['value'];
                echo "<td>$system</td><td>$value</td>";
                echo "</tr>";
            }
        }
    }

    ?>

    </tbody>
    </table>

    <div class="modelos-view">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => $attributes,
        ]) ?>
    </div>

</div>


