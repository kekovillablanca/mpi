<?php

use app\components\Metodos\Metodos;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
?>
<div class="modelos-view">
    <b>CONSULTA AL RENAPER</b><br><br>

    <table id="w0" class="table table-striped table-bordered detail-view">
    <tbody>

    <?php
    if (is_string($return)){

        echo $return;

    }else {

        $apellido_comparacion=strtoupper(trim(trim($model->primer_apellido)." ".trim($model->segundo_apellido)));
        $nombre_comparacion=strtoupper(trim(trim($model->primer_nombre)." ".trim($model->segundo_nombre)));   // minusculas
        $fecha_nacimiento_comparacion = Metodos::dateConvert($model->fecha_nacimiento,'toSql');
        $documento_tipo_comparacion=trim($model->documento_tipo);

        // tipo de documento, el renaper no lo devuelve, pero siempre es DNI
        if (trim("dni")==$documento_tipo_comparacion){
            echo "<tr><th>" . ucfirst("tipoDocumento") . "</th><td>dni</td></tr>";
        } else {
            echo "<tr><th style='color:red; font-weight: bold;'>" . ucfirst("tipoDocumento") . "</th><td style='color:red; font-weight: bold;'>dni</td></tr>";
        }

        foreach($return as $key=>$value) {

                switch ($key){
                    case "apellido":
                        // como el renaper es una garca y puedo tener 2 espacios cuando hay dos apellidos o mas
                        // tengo que hacer esta chotada.
                        $auxiliar=explode(" ",$value);
                        $value="";
                        foreach($auxiliar as $apellido){
                            if ($apellido) {
                                $value = $value . " " . $apellido;
                            }
                        }
                        if (strtoupper(trim($value))==$apellido_comparacion){
                            echo "<tr><th>" . ucfirst($key) . "</th><td>$value</td></tr>";
                        } else {
                            echo "<tr><th style='color:red; font-weight: bold;'>" . ucfirst($key) . "</th><td style='color:red; font-weight: bold;'>$value</td></tr>";
                        }
                        break;
                    case "nombres":
                        // como el renaper es una garca y puedo tener 2 espacios cuando hay dos nombres o mas
                        // tengo que hacer esta chotada.
                        $auxiliar=explode(" ",$value);
                        $value="";
                        foreach($auxiliar as $nombre){
                            if ($nombre) {
                                $value = $value . " " . $nombre;
                            }
                        }
                        if (strtoupper(trim($value))==$nombre_comparacion){
                            echo "<tr><th>" . ucfirst($key) . "</th><td>$value</td></tr>";
                        } else {
                            echo "<tr><th style='color:red; font-weight: bold;'>" . ucfirst($key) . "</th><td style='color:red; font-weight: bold;'>$value</td></tr>";
                        }
                        break;
                    case "fechaNacimiento":
                        if (trim($value)==$fecha_nacimiento_comparacion){
                            echo "<tr><th>" . ucfirst($key) . "</th><td>$value</td></tr>";
                        } else {
                            echo "<tr><th style='color:red; font-weight: bold;'>" . ucfirst($key) . "</th><td style='color:red; font-weight: bold;'>$value</td></tr>";
                        }
                       break;
                    case "foto":
                        // nada . muestro al final
                       // echo "<tr><th>" . ucfirst($key) . "</th><td><img src=".$value."></td></tr>";
                        break;
                    default:
                        echo "<tr><th>" . ucfirst($key) . "</th><td>$value</td></tr>";
                }
        }
    }
    ?>

    </tbody>
    </table>
<?php
if (!is_string($return)){
    echo "<center><img src='".$return['foto']."' /></center>";
}
?>
</div>

