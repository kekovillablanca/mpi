<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaestroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Indice Maestro';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$buttons = [
    'renaper' => function ($url) {
        return Html::a('<b style="font-size:16px;">R</b>', $url,
            ['role'=>'modal-remote','title'=> 'Consulta al Renaper',]);
    },
    'federador' => function ($url) {
        return Html::a('<b style="font-size:16px;">F</b>', $url,
            ['role'=>'modal-remote','title'=> 'Consulta al Federador',]);
    },
    'ips' => function ($url) {
        return Html::a('<b style="font-size:16px;">I</b>', $url,
            ['role'=>'modal-remote','title'=> 'Consulta IPS',]);
    },

];
$template='&nbsp;{view}&nbsp;{renaper}&nbsp;{federador}&nbsp;{ips}&nbsp;&nbsp;&nbsp;';

if (isset($inconsistencia)){
        
    $buttons=array_merge($buttons,
        array('diferencia' => function ($url) {
            return Html::a('<b style="font-size:16px;">D</b>', $url,
            ['role'=>'modal-remote','title'=> 'Diferencias',]);
        }));
        
    $template = '&nbsp;{view}&nbsp;{renaper}&nbsp;{federador}&nbsp;{diferencia}&nbsp;&nbsp;';
}
    
// agrego acciones a $columns
$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => $template,
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'buttons' => $buttons,
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'renaper'=> $permisos['renaper'],
            'federador'=> $permisos['federador'],
            'ips'=> true,
            ]
    ];

$stringToolbar="";

if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r'],'modelo'=>'maestro'],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r'],'modelo'=>'app\modules\mpi\models\Maestro'],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}


?>
    <div class="maestro-index">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size"=>"modal-lg"
])?>
<?php Modal::end(); ?>