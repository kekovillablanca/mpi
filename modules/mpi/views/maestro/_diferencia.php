<?php

use app\components\Metodos\Metodos;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
?>
<div class="modelos-view">
    <br>

    <table id="w0" class="table table-striped table-bordered detail-view">
    <tbody>

    <?php

        echo "<tr><th></th><td><b>BASE HOSPITAL</b></td><td><b>MAESTRO</b></td></tr>";

        foreach($datosmodificados as $key=>$value) {

            if (strtoupper(trim($value))==strtoupper(trim($model->$key))){
                echo "<tr><th>" . ucfirst($key) . "</th><td>$value</td><td>".$model->$key."</td></tr>";
            } else {
                echo "<tr><th style='color:red; font-weight: bold;'>" . ucfirst($key) . "</th><td style='color:red; font-weight: bold;'>$value</td><td style='color:red; font-weight: bold;'>".$model->$key."</td></tr>";
            }
    
        }

    ?>

    </tbody>
    </table>
</div>

