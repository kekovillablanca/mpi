<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\Establecimiento */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="establecimiento-form">

    <?php $form = ActiveForm::begin(); 

    echo $form->field($model, 'codigo_sisa')->textInput(['maxlength' => true]); 

     echo $form->field($model, 'nombre')->textInput(['maxlength' => true]);

     echo $form->field($model, 'id_localidad')->widget(Select2::classname(), [
        'data' => $localidad,
        'options' => ['placeholder' => Yii::t('app', 'Seleccione localidad') . '...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);

    ActiveForm::end(); ?>
    
</div>
