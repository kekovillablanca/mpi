<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "profesional_establecimiento".
 *
 * @property int $id
 * @property int $id_profesional
 * @property int $id_establecimiento
 *
 * @property Establecimiento $establecimiento
 * @property Profesional $profesional
 */
class ProfesionalEstablecimiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.profesional_establecimiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_establecimiento','id_profesional'],'required'],
            [['id_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimiento::className(), 'targetAttribute' => ['id_establecimiento' => 'id']],
            [['id_profesional'], 'exist', 'skipOnError' => true, 'targetClass' => Profesional::className(), 'targetAttribute' => ['id_profesional' => 'id']],
            [['id_establecimiento'], 'unique', 'targetAttribute' => ['id_profesional', 'id_establecimiento'],'message' => "el establecimiento ya se encuentra"],
            [['id_profesional'], 'unique', 'targetAttribute' => ['id_profesional', 'id_establecimiento'],'message' => "el profesional ya se encuentra"]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_profesional_nom' => 'Profesional Nombre',
            'id_profesional' => 'Profesional Apellido',
            'id_establecimiento' => 'Establecimiento',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('profesional_establecimiento.id', 10),
            'id_profesional_nom' => array('profesional.nombre',19),  
            'id_profesional' => array('profesional.apellido',19),     
        
			'id_establecimiento' => array('establecimiento.nombre',19),
        ];
    }

    public function attributeView()
    {
        return [
		
			'id_profesional'=>
			[
				'attribute'=>'profesional.apellido',
				'label'=>'Profesional Apellido',
			],
            'id_profesional_nom'=>
            [
				'attribute'=>'profesional.nombre',
				'label'=>'Profesional Nombre',
			],
			'id_establecimiento'=>
			[
				'attribute'=>'establecimiento.nombre',
				'label'=>'Establecimiento',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_profesional_nom',
				'value'=>'profesional.nombre',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_profesional',
				'value'=>'profesional.apellido',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_establecimiento',
				'value'=>'establecimiento.nombre',
			],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstablecimiento()
    {
        return $this->hasOne(Establecimiento::className(), ['id' => 'id_establecimiento']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesional()
    {
        return $this->hasOne(Profesional::className(), ['id' => 'id_profesional']);
    }

    public function getProfesionalNomAp()
    {
        $ape= 'profesional.apellido';
        $nom= 'profesional.nombre';                  
        $profe= $ape.", ".$nom;
        return $profe;
    }

    

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
