<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\components\Metodos\Metodos;

/**
 * LogscriptSearch represents the model behind the search form about `app\modules\mpi\models\Logscript`.
 */
class LogscriptSearch extends Logscript
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_hospital', 'id_maestro'], 'integer'],
            [['script', 'codigo_error', 'descripcion', 'base_hospital', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Logscript::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'script',
				'codigo_error',
				'descripcion',
				'base_hospital',
				'id_hospital',
				'id_maestro',
				'timestamp',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $control=explode('/',$this->timestamp);
        if (count($control)==3){
            $fecha_busqueda=Metodos::dateConvert($this->timestamp,'toSql');
        }else{
            $fecha_busqueda='';
            $this->timestamp='';
        }

        $query->andFilterWhere([
            'logscript.id' => $this->id,
            'logscript.id_hospital' => $this->id_hospital,
            'logscript.id_maestro' => $this->id_maestro,
        ]);

        $query->andFilterWhere(['like', 'lower(logscript.script)',strtolower($this->script)])
              ->andFilterWhere(['like', 'lower(logscript.codigo_error)',strtolower($this->codigo_error)])
              ->andFilterWhere(['like', 'lower(logscript.descripcion)',strtolower($this->descripcion)])
              ->andFilterWhere(['like', 'lower(logscript.base_hospital)',strtolower($this->base_hospital)])
              ->andFilterWhere(['like', 'lower(logscript.timestamp::text)',strtolower($fecha_busqueda)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('logscript-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
