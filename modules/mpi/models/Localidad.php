<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\admin\models\UsuarioEstadistica;

/**
 * This is the model class for table "consultasestadisticas.localidad".
 *
 * @property int $id
 * @property string $nombre
 * @property Provincia provincia
 * * @property UsuarioEstadistica[] $usuarioEstadistica
 */
class Localidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.localidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 50],
            [['id_provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincia::className(), 'targetAttribute' => ['id_provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'id_provincia'=>'provincia'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('consultasestadisticas.localidad.id', 10),
			'nombre' => array('consultasestadisticas.localidad.nombre', 20),
            'id_provincia' => array('provincia.nombre', 19),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
            'id_provincia'=>
            [
				'attribute'=>'provincia.nombre',
				'label'=>'provincia',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_provincia',
				'value'=>'provincia.nombre',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioEstadistica()
    {
        return $this->hasMany(UsuarioEstadistica::className(), ['id_localidad' => 'id']);
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia()
    {
        return $this->hasOne(Provincia::className(), ['id' => 'id_provincia']);
    }

    public function getDepartamento()
    {
        return $this->hasOne(Departamento::className(), ['id' => 'id_departamento']);
    }
    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
