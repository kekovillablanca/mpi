<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PacienteSearch represents the model behind the search form about `app\modules\mpi\models\Paciente`.
 */

class PacienteSearch extends Paciente
{

    /**
     * @inheritdoc
     */
    public function rules()
    {

    
        return [
            [['numero_documento'],'integer'],

            [[ 'nombre', 'apellido', 'id_sexo', 'fecha_nacimiento', 'id', 'id_genero', 'id_obra_social','historia_clinica','id_localidad','id_tipo_documento','id_tipo_plan_social','id_establecimiento','numero_telefono','id_sistema_educativo'], 'safe'],
            [[ 'documento_ajeno'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    
        $query = Paciente::find();
		$query->joinWith(['genero']);
        $query->joinWith(['sistemaeducativo.idasistio']);
		$query->joinWith(['obrasocial']);
        $query->joinWith(['tipoplansocial']);
        $query->joinWith(['tipodocumento']);
        $query->joinWith(['localidad']);
        $query->joinWith(['historiaclinicas']);
        $query->joinWith(['idsexo']);
        $query->joinWith(['establecimiento']);
        $query->joinWith(['contacto']);


        $session = Yii::$app->session;
     
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30, // Establece el tamaño de página aquí
            ],
            
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'nombre',
                'documento_ajeno',
				'apellido',
                'id_sexo'=> [
					'asc' => ['sexo.nombre' => SORT_ASC],
					'desc' => ['sexo.nombre' => SORT_DESC],
				],
                'id_tipo_plan_social'=>[

                    'asc' => ['id_tipo_plan_social.nombre' => SORT_ASC],
					'desc' => ['id_tipo_plan_social.nombre' => SORT_DESC],
                ],
                'id_establecimiento'=> [
					'asc' => ['establecimiento.nombre' => SORT_ASC],
					'desc' => ['establecimiento.nombre' => SORT_DESC],
				],
                'numero_telefono'=> [
					'asc' => ['contacto.numero_telefono' => SORT_ASC],
					'desc' => ['contacto.numero_telefono' => SORT_DESC],
				],
				// mysql
				// 'fecha_nacimiento'=> [
					// 'asc' => ['date_format(paciente.fecha_nacimiento, "%Y%m%d")' => SORT_ASC],
					// 'desc' => ['date_format(paciente.fecha_nacimiento,"%Y%m%d")' => SORT_DESC],
				// ],
				// postgres
				'fecha_nacimiento',
				'id',
				'numero_documento',
               'historia_clinica',
				'id_genero'=> [
					'asc' => ['genero.nombre' => SORT_ASC],
					'desc' => ['genero.nombre' => SORT_DESC],
				],
                'id_localidad'=> [
					'asc' => ['localidad.nombre' => SORT_ASC],
					'desc' => ['localidad.nombre' => SORT_DESC],
				],
				'id_obra_social'=> [
					'asc' => ['obra_social.nombre' => SORT_ASC],
					'desc' => ['obra_social.nombre' => SORT_DESC],
				],
                'id_tipo_documento'=> [
					'asc' => ['tipo_documento.nombre' => SORT_ASC],
					'desc' => ['tipo_documento.nombre' => SORT_DESC],
				],
                'id_sistema_educativo'=> [
					'asc' => ['asistio.nombre' => SORT_ASC],
					'desc' => ['asistio.nombre' => SORT_DESC],
				],


            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'paciente.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(paciente.nombre)',mb_strtolower($this->nombre,'UTF-8')])
              ->andFilterWhere(['like', 'lower(paciente.apellido)',strtolower($this->apellido)])
              ->andFilterWhere(['paciente.documento_ajeno'=>$this->documento_ajeno])
              ->andFilterWhere(['like', 'lower(historia_clinica.historia_clinica)',strtolower($this->historia_clinica)])
              //mysql
              // ->andFilterWhere(['like', 'date_format(paciente.fecha_nacimiento,\'%d/%m/%Y\')',$this->fecha_nacimiento])
              //postgres
              ->andFilterWhere(['like', 'to_char(paciente.fecha_nacimiento,\'DD/MM/YYYY\')',$this->fecha_nacimiento])
              ->andFilterWhere(['like', 'lower(genero.nombre)',strtolower($this->id_genero)])
              ->andFilterWhere(['like', 'lower(sexo.nombre)',strtolower($this->id_sexo)])
              ->andFilterWhere(['like', 'lower(obra_social.nombre)',strtolower($this->id_obra_social)])
              ->andFilterWhere(['like', 'lower(tipo_plan_social.nombre)',strtolower($this->id_tipo_plan_social)])
              ->andFilterWhere(['like', 'lower(localidad.nombre)',strtolower($this->id_localidad)])
              ->andFilterWhere(['like','CAST(numero_documento AS varchar)',$this->numero_documento])
              ->andFilterWhere(['like','lower(asistio.nombre)',$this->id_sistema_educativo])
              ->andFilterWhere(['like','lower(tipo_documento.nombre)',$this->id_tipo_documento])
              //->andFilterWhere(['like','lower(contacto.numero_telefono)',$this->numero_telefono])
              ->andFilterWhere(['like','lower(contacto.numero_telefono)',$this->numero_telefono])
              ->andFilterWhere(['like', 'lower(establecimiento.nombre)',mb_strtolower($this->id_establecimiento,'UTF-8')]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('paciente-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
