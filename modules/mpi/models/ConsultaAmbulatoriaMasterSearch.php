<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ConsultaAmbulatoriaMasterSearch represents the model behind the search form about `app\models\ConsultaAmbulatoriaMaster`.
 */
class ConsultaAmbulatoriaMasterSearch extends ConsultaAmbulatoriaMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id1', 'dominio','centro_id', 'paciente_id', 'medico_id', 'numero_turno'], 'integer'],
            [['id2', 'paciente_numero_hc', 'paciente_ape', 'paciente_sexo','paciente_nom','paciente_nac','fecha',
             'paciente_tipodoc', 'paciente_numerodoc', 'especialidad', 'servicio', 'medico_ape', 'area','centro', 
             'medico_nom','cie10_observacion','cie10_tipo_diag','cie10_codigo','cie10_descricion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = ConsultaAmbulatoriaMaster::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id1',
				'id2',
                'dominio',
                'area',
                'centro_id',
                'centro',
                'paciente_id',
				'paciente_numero_hc',
				'paciente_ape',
				'paciente_nom',
				'paciente_nac',
				'paciente_sexo',
				'paciente_tipodoc',
				'paciente_numerodoc',
                'fecha',
				'especialidad',
				'servicio',
				'medico_id',
				'medico_ape',
				'medico_nom',
				'numero_turno',
				'cie10_codigo',
				'cie10_tipo_diag',
				'cie10_descricion',
				'cie10_observacion',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'consulta_ambulatoria_master.id1' => $this->id1,
            'consulta_ambulatoria_master.id2' => $this->id2,
            'consulta_ambulatoria_master.dominio' => $this->dominio,
            'consulta_ambulatoria_master.centro_id' => $this->centro_id,
            'consulta_ambulatoria_master.paciente_id' => $this->paciente_id,
            'consulta_ambulatoria_master.medico_id' => $this->medico_id,
            'consulta_ambulatoria_master.numero_turno' => $this->numero_turno,
        ]);

        $query->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.paciente_numero_hc)',strtolower($this->paciente_numero_hc)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.paciente_ape)',strtolower($this->paciente_ape)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.paciente_nom)',strtolower($this->paciente_nom)])
              ->andFilterWhere(['like', 'to_char(consulta_ambulatoria_master.paciente_nac,\'DD/MM/YYYY\')',$this->paciente_nac])
              ->andFilterWhere(['like', 'to_char(date(consulta_ambulatoria_master.fecha),\'DD/MM/YYYY\')',$this->fecha])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.paciente_sexo)',strtolower($this->paciente_sexo)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.paciente_tipodoc)',strtolower($this->paciente_tipodoc)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.paciente_numerodoc)',strtolower($this->paciente_numerodoc)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.especialidad)',strtolower($this->especialidad)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.servicio)',strtolower($this->servicio)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.area)',strtolower($this->area)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.centro)',strtolower($this->centro)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.medico_ape)',strtolower($this->medico_ape)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.medico_nom)',strtolower($this->medico_nom)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.cie10_codigo)',strtolower($this->cie10_codigo)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.cie10_tipo_diag)',strtolower($this->cie10_tipo_diag)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.cie10_descricion)',strtolower($this->cie10_descricion)])
              ->andFilterWhere(['like', 'lower(consulta_ambulatoria_master.cie10_observacion)',strtolower($this->cie10_observacion)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('consultaambulatoriamaster-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
