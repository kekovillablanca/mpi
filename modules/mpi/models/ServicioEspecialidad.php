<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "servicio_especialidad".
 *
 * @property int $id_servicio
 * @property string $nombre_servicio
 * @property string $id_especialidad
 * @property string $nombre_especialidad
 */
class ServicioEspecialidad extends \yii\db\ActiveRecord
{
	public static $dbid;
    public static $schema;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::$schema.'.servicio_especialidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_servicio'], 'default', 'value' => null],
            [['id_servicio'], 'integer'],
            [['nombre_servicio', 'nombre_especialidad'], 'string', 'max' => 50],
            [['id_especialidad'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_servicio' => 'Id Servicio',
            'nombre_servicio' => 'Servicio',
            'id_especialidad' => 'Id Especialidad',
            'nombre_especialidad' => 'Especialidad',
        ];
    }
    public function attributePrint()
    {
        return [
			'id_servicio' => array('servicio_especialidad.id_servicio', 12),
			'nombre_servicio' => array('servicio_especialidad.nombre_servicio', 20),
			'id_especialidad' => array('servicio_especialidad.id_especialidad', 16),
			'nombre_especialidad' => array('servicio_especialidad.nombre_especialidad', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id_servicio',
			'nombre_servicio',
			'id_especialidad',
			'nombre_especialidad',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_servicio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre_servicio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_especialidad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre_especialidad',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

    public function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;
        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
        }
    }

	public static function primaryKey()
	{
		return ['id_servicio','id_especialidad'];
	}


    
}
