<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EspecialidadSearch represents the model behind the search form about `app\modules\mpi\models\Especialidad`.
 */
class EspecialidadSearch extends Especialidad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'nombre', 'id_servicio','codigo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Especialidad::find();
		$query->joinWith(['pservicio']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'nombre',
                'codigo',
				'id_servicio'=> [
					'asc' => ['p_servicio.nombre' => SORT_ASC],
					'desc' => ['p_servicio.nombre' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'especialidad.id' => $this->id,
            'especialidad.codigo' => $this->codigo,
        ]);

        $query->andFilterWhere(['like', 'lower(especialidad.nombre)',strtolower($this->nombre)])
              ->andFilterWhere(['like', 'lower(p_servicio.nombre)',strtolower($this->id_servicio)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('especialidad-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
