<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HistoriaClinicaSearch represents the model behind the search form about `app\modules\mpi\models\HistoriaClinica`.
 */
class HistoriaClinicaSearch extends HistoriaClinica
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'historia_clinica', 'id_establecimiento', 'id_paciente'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HistoriaClinica::find();
		$query->joinWith(['establecimiento']);
		$query->joinWith(['paciente']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				//'historia_clinica',
				'id_establecimiento'=> [
                    'asc' => ['establecimiento.nombre' => SORT_ASC],
					'desc' => ['establecimiento.nombre' => SORT_DESC],
				],
				'id_paciente'=> [
					'asc' => ['paciente.nombre' => SORT_ASC],
					'desc' => ['paciente.nombre' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'historia_clinica.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(historia_clinica.historia_clinica)',strtolower($this->historia_clinica)])
              ->andFilterWhere(['like', 'lower(consultasestadisticas.establecimiento.nombre)',strtolower($this->id_establecimiento)])
              ->andFilterWhere(['like', 'lower(consultasestadisticas.paciente.nombre)',strtolower($this->id_paciente)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('historiaclinica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
