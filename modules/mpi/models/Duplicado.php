<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "duplicado".
 *
 * @property int $id_maestro
 * @property string $documento_tipo
 * @property string $documento_numero
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property string $numero_hc
 * @property bool $estado
 * @property int $id_area
 * @property int $id_federado_provincial
 * @property string $id_federado_nacional
 * @property int $id_duplicado
 * @property int $id_error
 * @property bool $flag_federado
 * @property bool $flag_modificado
 * @property bool $flag_autenticado
 * @property string $stamp_id_error
 * @property string $stamp_flag_federado
 * @property string $stamp_flag_modificado
 * @property string $stamp_flag_autenticado
 *
 * @property Dominio $area
 */
class Duplicado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'duplicado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento', 'stamp_id_error', 'stamp_flag_federado', 'stamp_flag_modificado', 'stamp_flag_autenticado'], 'safe'],
            [['flag_federado', 'flag_modificado', 'flag_autenticado'], 'boolean'],
            [['estado','id_area', 'id_federado_provincial', 'id_duplicado', 'id_error'], 'default', 'value' => null],
            [['estado'.'id_area', 'id_federado_provincial', 'id_duplicado', 'id_error'], 'integer'],
            [['documento_tipo', 'documento_numero', 'sexo', 'numero_hc', 'id_federado_nacional'], 'string', 'max' => 20],
            [['primer_apellido', 'segundo_apellido', 'primer_nombre', 'segundo_nombre'], 'string', 'max' => 100],
            [['id_area', 'id_federado_provincial'], 'unique', 'targetAttribute' => ['id_area', 'id_federado_provincial']],
            [['id_area'], 'exist', 'skipOnError' => true, 'targetClass' => Dominio::className(), 'targetAttribute' => ['id_area' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_maestro' => 'Id',
            'documento_tipo' => 'Doc',
            'documento_numero' => 'Numero',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'primer_nombre' => 'Primer Nombre',
            'segundo_nombre' => 'Segundo Nombre',
            'fecha_nacimiento' => 'Nacimiento',
            'sexo' => 'Sexo',
            'numero_hc' => 'Numero Hc',
            'estado' => 'Estado',
            'id_area' => 'Area',
            'id_federado_provincial' => 'Id Fd Provincial',
            'id_federado_nacional' => 'Id Fd Nacional',
            'id_duplicado' => 'Duplicado',
            'id_error' => 'Error',
            'flag_federado' => 'Flag Fed',
            'flag_modificado' => 'Flag Mod',
            'flag_autenticado' => 'Flag Aut',
            'stamp_id_error' => 'Stamp Error',
            'stamp_flag_federado' => 'Stamp Federado',
            'stamp_flag_modificado' => 'Stamp Modificado',
            'stamp_flag_autenticado' => 'Stamp Autenticado',
        ];
    }
    public function attributePrint()
    {
        return [
			'id_maestro' => array('duplicado.id_maestro', 11),
			'documento_tipo' => array('duplicado.documento_tipo', 20),
			'documento_numero' => array('duplicado.documento_numero', 20),
			'primer_apellido' => array('duplicado.primer_apellido', 20),
			'segundo_apellido' => array('duplicado.segundo_apellido', 20),
			'primer_nombre' => array('duplicado.primer_nombre', 20),
			'segundo_nombre' => array('duplicado.segundo_nombre', 20),
			'fecha_nacimiento' => array('duplicado.fecha_nacimiento', 17),
			'sexo' => array('duplicado.sexo', 20),
			'numero_hc' => array('duplicado.numero_hc', 20),
			'estado' => array('duplicado.estado', 10),
			'id_area' => array('dominio.area',10),
			'id_federado_provincial' => array('duplicado.id_federado_provincial', 20),
			'id_federado_nacional' => array('duplicado.id_federado_nacional', 20),
			'id_duplicado' => array('duplicado.id_duplicado', 13),
			'id_error' => array('duplicado.id_error', 10),
			'flag_federado' => array('duplicado.flag_federado', 14),
			'flag_modificado' => array('duplicado.flag_modificado', 16),
			'flag_autenticado' => array('duplicado.flag_autenticado', 17),
			'stamp_id_error' => array('duplicado.stamp_id_error', 15),
			'stamp_flag_federado' => array('duplicado.stamp_flag_federado', 20),
			'stamp_flag_modificado' => array('duplicado.stamp_flag_modificado', 20),
			'stamp_flag_autenticado' => array('duplicado.stamp_flag_autenticado', 20),
        ];
    }

    public function attributeView()
    {
        return [
//			'id_maestro',
			'documento_tipo',
			'documento_numero',
			'primer_apellido',
			'segundo_apellido',
			'primer_nombre',
			'segundo_nombre',
			'fecha_nacimiento',
			'sexo',
			'numero_hc',
            'estado',
			'id_area'=>
			[
				'attribute'=>'dominio.area',
				'label'=>'Area',
			],
			'id_federado_provincial',
			'id_federado_nacional',
/*			'id_duplicado',
			'id_error',
            'flag_federado'=>
                [
                    'attribute'=>'flag_federado',
                    'format'=>'boolean',
                ],
            'flag_modificado'=>
                [
                    'attribute'=>'flag_modificado',
                    'format'=>'boolean',
                ],
            'flag_autenticado'=>
                [
                    'attribute'=>'flag_autenticado',
                    'format'=>'boolean',
                ],
			'stamp_id_error',
			'stamp_flag_federado',
			'stamp_flag_modificado',
			'stamp_flag_autenticado', */
        ];
    }

    public function attributeViewInfo()
    {
        return [
                        'id_federado_provincial',
                        'id_federado_nacional',
                        'id_duplicado',
                        'id_error',
                        'flag_federado'=>
                            [
                                'attribute'=>'flag_federado',
                                'format'=>'boolean',
                            ],
                        'flag_modificado'=>
                            [
                                'attribute'=>'flag_modificado',
                                'format'=>'boolean',
                            ],
                        'flag_autenticado'=>
                            [
                                'attribute'=>'flag_autenticado',
                                'format'=>'boolean',
                            ],
                        'stamp_id_error',
                        'stamp_flag_federado',
                        'stamp_flag_modificado',
                        'stamp_flag_autenticado',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_maestro',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'documento_tipo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'documento_numero',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'primer_apellido',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'segundo_apellido',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'primer_nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'segundo_nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_nacimiento',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'sexo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero_hc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'estado',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_area',
				'value'=>'dominio.area',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_federado_provincial',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_federado_nacional',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_duplicado',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_error',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'flag_federado',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'flag_modificado',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'flag_autenticado',
				'format'=>'boolean',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'stamp_id_error',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'stamp_flag_federado',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'stamp_flag_modificado',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'stamp_flag_autenticado',
			// ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDominio()
    {
        return $this->hasOne(Dominio::className(), ['id' => 'id_area']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha_nacimiento=Metodos::dateConvert($this->fecha_nacimiento,'View');

		$estado='Activo';
		if ($this->estado==1)
			$estado='Unificado';
		if ($this->estado==2)
			$estado='Fallecido';

		$this->estado=$estado;

	}

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha_nacimiento=Metodos::dateConvert($this->fecha_nacimiento,'toSql');
            return true;
        } else {
            return false;
        }
	}

	public static function primaryKey()
    {
        return ['id_maestro'];
    }


}
