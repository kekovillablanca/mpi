<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "domicilio".
 *
 * @property string $paciente_id
 * @property string $domicilio
 * @property string $cp
 * @property string $localidad
 * @property string $provincia
 */
class Domicilio extends \yii\db\ActiveRecord
{
    public $domicilio_completo;
    public static $dbid;
    public static $schema;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::$schema.'.domicilio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['domicilio','cp','localidad'],'provincia', 'string'],
            [['paciente_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paciente_id' => 'ID',
            'domicilio' => 'Domicilio',
            'cp' => 'Codigo Postal',
            'localidad' => 'Localidad',
            'provincia' => 'Provincia',
        ];
    }
    public function attributePrint()
    {
        return [
			'paciente_id' => array('domicilio.paciente_id', 4),
			'domicilio' => array('domicilio.domicilio', 10),
			'cp' => array('domicilio.cp', 10),
			'localidad' => array('domicilio.localidad', 10),
			'provincia' => array('domicilio.provincia', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'paciente_id',
			'domicilio',
			'cp',
			'localidad',
			'provincia',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
            // faltan las demas pero no interesan
        ];
    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->domicilio_completo=$this->domicilio." (".$this->cp.") ".$this->localidad." . ".$this->provincia ;
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

    public static function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;

        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
            case 500:
                self::$schema='hospitalsao';
                break;
        }
    }

	public static function primaryKey()
	{
		return ['paciente_id'];
	}


}
