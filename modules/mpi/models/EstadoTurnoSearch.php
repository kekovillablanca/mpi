<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EstadoTurnoSearch represents the model behind the search form about `app\modules\mpi\models\EstadoTurno`.
 */
class EstadoTurnoSearch extends EstadoTurno
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EstadoTurno::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'nombre',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'estado_turno.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(estado_turno.nombre)',strtolower($this->nombre)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('estadoturno-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
