<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProfesionalEstablecimientoSearch represents the model behind the search form about `app\modules\mpi\models\ProfesionalEstablecimiento`.
 */
class ProfesionalEstablecimientoSearch extends ProfesionalEstablecimiento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_profesional', 'id_establecimiento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$q)
    {
        $query = ProfesionalEstablecimiento::find();
		$query->joinWith(['establecimiento']);
		$query->joinWith(['profesional']);

        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null))
        $query->joinWith('establecimiento.localidad.usuarioEstadistica')->where(['usuario_estadistica.id_usuario'=>Yii::$app->user->identity->id])->all();
        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_profesional'=> [
					'asc' => ['profesional.apellido' => SORT_ASC],
					'desc' => ['profesional.apellido' => SORT_DESC],
				],
				'id_establecimiento'=> [
					'asc' => ['establecimiento.nombre' => SORT_ASC],
					'desc' => ['establecimiento.nombre' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'profesional_establecimiento.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(profesional.apellido)',strtolower($this->id_profesional)])
              ->andFilterWhere(['like', 'lower(establecimiento.nombre)',strtolower($this->id_establecimiento)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('profesionalestablecimiento-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
