<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LocalidadSearch represents the model behind the search form about `app\modules\mpi\models\Localidad`.
 */
class LocalidadSearch extends Localidad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'nombre', 'id_provincia'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Localidad::find();
		$query->joinWith(['consultasestadisticas.provincia']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'nombre',
				'id_provincia'=> [
					'asc' => ['consultasestadisticas.provincia.nombre' => SORT_ASC],
					'desc' => ['consultasestadisticas.provincia.nombre' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'localidad.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(localidad.nombre)',strtolower($this->nombre)])
              ->andFilterWhere(['like', 'lower(consultasestadisticas.provincia.nombre)',strtolower($this->id_provincia)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('localidad-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
