<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EstablecimientoSearch represents the model behind the search form about `app\modules\mpi\models\Establecimiento`.
 */
class EstablecimientoSearch extends Establecimiento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['nombre', 'id_localidad', 'id'], 'safe'],
            [['codigo_sisa'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Establecimiento::find();
        $query->joinWith(['localidad']);
        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'codigo_sisa',
				'nombre',
				'id_localidad'=> [
					'asc' => ['localidad.nombre' => SORT_ASC],
					'desc' => ['localidad.nombre' => SORT_DESC],
				],
				'id',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'establecimiento.id' => $this->id,
           
        ]);

        $query->andFilterWhere(['like', 'lower(establecimiento.nombre)',strtolower($this->nombre)])
              ->andFilterWhere(['like', 'lower(localidad.nombre)',strtolower($this->id_localidad)])
              ->andFilterWhere(['like','CAST(codigo_sisa AS varchar)',$this->codigo_sisa]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('establecimiento-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
