<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "unidad_edad".
 *
 * @property string $descripcion
 * @property int $id
 *
 * @property Turno[] $turnos
 */
class Edad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.unidad_edad';
    }
    public function fields()
{
    return ['id', 'descripcion'];
}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'descripcion' => 'Descripcion',
            'id' => 'ID',
        ];
    }
    public function attributePrint()
    {
        return [
			'descripcion' => array('unidad_edad.descripcion', 12),
			'id' => array('unidad_edad.id', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'descripcion',
			'id',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurnos()
    {
        return $this->hasMany(Turno::className(), ['id_unidad_edad' => 'id']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
