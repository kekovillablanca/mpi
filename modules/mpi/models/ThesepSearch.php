<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ThesepSearch represents the model behind the search form about `app\modules\mpi\models\Thesep`.
 */
class ThesepSearch extends Thesep
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_especialidad', 'id_servicio', 'fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Thesep::find();
		$query->joinWith(['especialidad']);
		$query->joinWith(['pservicio']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_especialidad'=> [
					'asc' => ['consultasestadisticas.especialidad.nombre' => SORT_ASC],
					'desc' => ['consultasestadisticas.especialidad.nombre' => SORT_DESC],
				],
				'id_servicio'=> [
					'asc' => ['consultasestadisticas.p_servicio.nombre' => SORT_ASC],
					'desc' => ['consultasestadisticas.p_servicio.nombre' => SORT_DESC],
				],
				// mysql
				// 'fecha'=> [
					// 'asc' => ['date_format(thesep.fecha, "%Y%m%d")' => SORT_ASC],
					// 'desc' => ['date_format(thesep.fecha,"%Y%m%d")' => SORT_DESC],
				// ],
				// postgres
				'fecha',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'thesep.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(consultasestadisticas.especialidad.nombre)',strtolower($this->id_especialidad)])
              ->andFilterWhere(['like', 'lower(consultasestadisticas.p_servicio.nombre)',strtolower($this->id_servicio)])
              //mysql
              // ->andFilterWhere(['like', 'date_format(thesep.fecha,\'%d/%m/%Y\')',$this->fecha])
              //postgres
              ->andFilterWhere(['like', 'to_char(thesep.fecha,\'DD/MM/YYYY\')',$this->fecha]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('thesep-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
