<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\internacion\models\SistemaEducativo;

use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "paciente".

 * @property string $nombre
 * @property string $apellido
 * @property string $fecha_nacimiento
 * @property int $id
 * @property int $numero_documento
 * @property int $id_genero
 * @property int $id_contacto
 * @property int $id_obra_social
 * @property int $id_establecimiento
 * @property int $historia_clinica
 * @property int $id_localidad
 * @property int $id_sexo
 * @property boolean $is_internacion
 * @property boolean $documento_ajeno
 * @property int $id_tipo_plan_social
 * @property int $id_sistema_educativo
 * @property Genero $genero
 * @property ObraSocial $obraSocial
 * @property Turno[] $turnos
 * @property TipoDocumento $id_tipo_documento
 */
class Paciente extends \yii\db\ActiveRecord
{   public $numero_telefono;
    public $historia_clinica;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.paciente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento','id_sexo','nombre','apellido','id_tipo_documento','id_tipo_plan_social','numero_documento','id_sistema_educativo'],'required'],
//            ['id_obra_social','validateObraSocial','params' => ['message' => 'Si tiene obra social debe especificar tipo.']],
            [['historia_clinica'], 'string', 'max' => 8],
   
            [['id_sistema_educativo'], 'default', 'value' => 2],
            [['id_genero','id_tipo_plan_social','id_obra_social','id_establecimiento','id_localidad','id_contacto'], 'default', 'value' => null],
             [['is_internacion','documento_ajeno'], 'default', 'value' => false],
             [['is_internacion','documento_ajeno'],'boolean'],
            [['id_genero','id_localidad','id_obra_social','id_sexo','id_tipo_documento','id_tipo_plan_social'],'integer'],
            [['id_obra_social'], 'required', 'when' => function ($model) {
                return in_array($model->id_tipo_plan_social, ['1', '2', '3', '4']);
            },'whenClient'=> "function(attribute,value){

            console.log(document.getElementById('paciente-id_tipo_plan_social').value);
                return document.getElementById('paciente-id_tipo_plan_social').value!=5;
            }"],
            [['numero_documento'], 'match', 'pattern' => '/^[a-zA-Z0-9]*$/', 'when' => function ($model) {
                // Permite letras y números cuando id_tipo_documento es diferente de 1
                return $model->id_tipo_documento != 1;
            }, 'message' => 'Error de documento.'],

            
            
            // Validación en el servidor
/*[['numero_documento'], 'required', 'when' => function ($model) {
    // Campo requerido solo si id_tipo_documento es 1 (DNI)
    return $model->id_tipo_documento == 1;
}, 'whenClient' => "function (attribute, value) {
    // Validación en el cliente: se ejecuta en el navegador
    var tipoDocumento = $('#paciente-id_tipo_documento').val(); // Obtiene el valor del tipo de documento
    return tipoDocumento == 1; // El campo es requerido si el tipo de documento es 1 (DNI)
}"],*/
[['numero_documento'], 'validateNumeroDocumento'],
[['numero_documento'], 'string', 'max' => 10],

            
            
            
            [['nombre', 'apellido'], 'string','message' => 'El valor debe ser una cadena de texto.', 'max'=>128],
            [['id_genero'], 'exist', 'skipOnError' => true, 'targetClass' => Genero::className(), 'targetAttribute' => ['id_genero' => 'id']],
            [['id_obra_social'], 'exist', 'skipOnError' => true, 'targetClass' => ObraSocial::className(), 'targetAttribute' => ['id_obra_social' => 'id']],
            [['id_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimiento::className(), 'targetAttribute' => ['id_establecimiento' => 'id']],
            [['id_localidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['id_localidad' => 'id']],
            [['id_tipo_documento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['id_tipo_documento' => 'id']],
            [['id_sexo'], 'exist', 'skipOnError' => true, 'targetClass' => Sexo::className(), 'targetAttribute' => ['id_sexo' => 'id']],
            [['id_tipo_plan_social'], 'exist', 'skipOnError' => true, 'targetClass' => TipoPlanSocial::className(), 'targetAttribute' => ['id_tipo_plan_social' => 'id']],
            [['id_contacto'], 'exist', 'skipOnError' => true, 'targetClass' => Contacto::className(), 'targetAttribute' => ['id_contacto' => 'id']],
            [['id_sistema_educativo'], 'exist', 'skipOnError' => true, 'targetClass' => SistemaEducativo::className(), 'targetAttribute' => ['id_sistema_educativo' => 'id']],
            [['nombre', 'apellido'], 'match', 'pattern' => '/^[^\d]*$/', 'message' => 'El valor no debe contener números.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'id_sexo' => 'Sexo',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'id' => 'ID',
            'numero_documento' => 'Numero de Documento',
            'id_genero' => 'Genero',
            'id_obra_social' => 'Obra Social',
            'id_localidad'=>'Localidad',
            'historia_clinica'=>'historia clinica',
            'id_tipo_documento'=>'Tipo de Documento',
            'is_internacion'=>'Internacion',
             'id_tipo_plan_social'=>'Tipo de plan social',
             'id_establecimiento'=>'Establecimiento',
             'numero_telefono'=>'Contacto',
             'documento_ajeno'=>'documento_ajeno',
            'id_sistema_educativo'=>'Sistema Educativo'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('paciente.id', 10),
			'nombre' => array('paciente.nombre', 20),
			'apellido' => array('paciente.apellido', 20),
			// postgres
			'fecha_nacimiento' => array('paciente.fecha_nacimiento', 20),
			// mysql
			//'fecha_nacimiento' => array('DATE_FORMAT(paciente.fecha_nacimiento,\'%d/%m/%Y\')', 17),
			'id_genero' => array('genero.nombre',10),
			'id_obra_social' => array('obra_social.nombre',15),
			'id_sexo' => array('sexo.nombre', 10),
			'numero_documento' => array('paciente.numero_documento', 10),
            'historia_clinica'=>array('historia_clinica.historia_clinica',50),
            'id_localidad'=>array('localidad.nombre',50),
            'id_tipo_documento'=>array('tipo_documento.nombre',50),
            'is_internacion'=>array('`paciente.is_internacion',50),
            'id_tipo_plan_social'=>array('tipo_plan_social.nombre',50),
            'id_establecimiento' => array('establecimiento.nombre', 20),
            'numero_telefono' => array('contacto.numero_telefono', 20),
            'documento_ajeno' => array('paciente.documento_ajeno', 20),
            'id_sistema_educativo'=>array('sistema_educativo.asistio.nombre',50),
        ];
    }
    public function attributeView()
    {
        return [
			'nombre',
			'apellido',
			'fecha_nacimiento',
			'numero_documento',
            'documento_ajeno',
            'numero_telefono'=>[

                'attribute'=>'contacto.numero_telefono',
				'label'=>'Numero telefono',
        ],
            'id_establecimiento'=>
			[
				'attribute'=>'establecimiento.nombre',
				'label'=>'Establecimiento',
			],
      
			'id_genero'=>
			[
				'attribute'=>'genero.nombre',
				'label'=>'Genero',
			],
            'id_sexo'=>
			[
				'attribute'=>'idsexo.nombre',
				'label'=>'Sexo',
			],
			'id_obra_social'=>
			[
				'attribute'=>'obrasocial.nombre',
				'label'=>'Obra Social',
			],
            'historia_clinica'=>
			[
				'attribute'=>'historia',
				'label'=>'Historia clinica',
			],
            'id_localidad'=>
			[
				'attribute'=>'localidad.nombre',
				'label'=>'Localidad',
			],
            'id_tipo_documento'=>[
                'attribute'=>'tipodocumento.nombre',
				'label'=>'Tipo de Documento',
            ],

            'id_tipo_plan_social'=>[
                'attribute'=>'tipoplansocial.nombre',
				'label'=>'Tipo de plan social',
            ],
            'id_sistema_educativo'=>[
                'attribute'=>'sistemaeducativo.idasistio.nombre',
				'label'=>'Sistema educativo',
            ]
            
      
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
			    'attribute'=>'id_establecimiento',
				 'value'=>'establecimiento.nombre',
             ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'apellido',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
			    'attribute'=>'id_sexo',
				 'value'=>'idsexo.nombre',
             ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_nacimiento',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'documento_ajeno',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero_documento',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero_telefono',
                'value'=>'contacto.numero_telefono',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_genero',
				'value'=>'genero.nombre',
			],			
			 [
				'class'=>'\kartik\grid\DataColumn',
			'attribute'=>'id_obra_social',
				 'value'=>'obrasocial.nombre',
             ],
             [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_tipo_plan_social',
                'value'=>'tipoplansocial.nombre',
			],
             [
				'class'=>'\kartik\grid\DataColumn',
			    'attribute'=>'historia_clinica',
                'value'=>'historia'

             ],
         [
				'class'=>'\kartik\grid\DataColumn',
			    'attribute'=>'id_localidad',
                'value'=>'localidad.nombre'

             ],
            [
				'class'=>'\kartik\grid\DataColumn',
			    'attribute'=>'id_sistema_educativo',
                'value'=>'sistemaeducativo.idasistio.nombre'

             ],
             [

                'class'=>'\kartik\grid\DataColumn',
			    'attribute'=>'id_tipo_documento',
                'value'=>'tipodocumento.nombre'
             ],
       
        ];
    }
 
    public function validateNumeroDocumento($attribute, $params)
{
    if ($this->id_tipo_documento == 1 && $this->documento_ajeno==false && (strlen($this->numero_documento) > 10 || strlen($this->numero_documento) < 7)) {
        $this->addError($attribute, 'El número de documento solo puede tener numero de documento mayor a 8.');
    }
    else if ($this->id_tipo_documento == 1 && $this->documento_ajeno==true && (strlen($this->numero_documento) > 10  || strlen($this->numero_documento) < 9)) {
        $this->addError($attribute, 'El número de documento solo puede tener hasta 9 o 10 dígitos.');
    }
    elseif ($this->id_tipo_documento == 4 && strlen($this->numero_documento) != 9 ) {
        $this->addError($attribute, 'El número de pasaporte solo puede tener 9 dígitos.');
    }
}
   public function  validateObraSocial($attribute,$params, $validator){

    if ($this->id_tipo_plan_social=='5' && (!empty($attribute) || $attribute != '') ){
        $this->addError($attribute, Yii::t('app', 'Si tiene obra social debe especificar tipo.'));
    }
    }


      /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacto()
    {
        return $this->hasOne(Contacto::className(), ['id' => 'id_contacto']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenero()
    {
        return $this->hasOne(Genero::className(), ['id' => 'id_genero']);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdsexo()
    {
        return $this->hasOne(Sexo::className(), ['id' => 'id_sexo']);
    }
   public function getSistemaeducativo()
    {
        return $this->hasOne(SistemaEducativo::className(), ['id' => 'id_sistema_educativo']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObrasocial()
    {
        return $this->hasOne(ObraSocial::className(), ['id' => 'id_obra_social']);
    }
    public function getTipoplansocial()
    {
        return $this->hasOne(TipoPlanSocial::className(), ['id' => 'id_tipo_plan_social']);
    }
    public function getTipodocumento()
    {
        return $this->hasOne(TipoDocumento::className(), ['id' => 'id_tipo_documento']);
    }

    public function geObrasocialnombre()
    {
        return $this->getObrasocial()->select('nombre');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurnos()
    {
        return $this->hasMany(Turno::className(), ['id_paciente' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstablecimiento()
    {
        return $this->hasOne(Establecimiento::className(), ['id' => 'id_establecimiento']);
    }
 public function getLocalidad()
    {
        return $this->hasOne(Localidad::className(), ['id' => 'id_localidad']);
    }
    public function getHistoriaclinicas()
    {
        return $this->hasMany(HistoriaClinica::className(), ['id_paciente' => 'id'])->select('historia_clinica');
    }

    public function getHistoria()
    {
        $array=$this->getHistoriaclinicas()
        ->andWhere(['not', ['historia_clinica' => null]])->andWhere(['id_establecimiento'=>$this->id_establecimiento])->all();
        $this->historia_clinica=implode(", ", $array);
        return $this->historia_clinica;
    }


    public function afterFind(){
        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha_nacimiento=Metodos::dateConvert($this->fecha_nacimiento,'View');
    }

public function beforeSave($insert)
{
    if (!parent::beforeSave($insert)) {
        return false;
    }

    if ($this->isNewRecord) {
        // Verificar si ya existe un paciente con el mismo número de documento y tipo
        $existe = $this->find()
            ->where(['numero_documento' => $this->numero_documento])
            ->andWhere(['id_tipo_documento' => $this->id_tipo_documento])
            ->exists();

        if ($existe) {
            // Si ya existe, lanzar una excepción

            throw new NotFoundHttpException(
                "El paciente con número de documento {$this->numero_documento} y tipo de documento ya existe."
            );
        }
    }


    // Convertir la fecha de nacimiento a formato SQL
    $this->fecha_nacimiento = Metodos::dateConvert($this->fecha_nacimiento, 'toSql');

    // Continuar con el guardado
    return true;
}





}
