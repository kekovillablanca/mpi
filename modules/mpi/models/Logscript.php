<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "logscript".
 *
 * @property int $id
 * @property string $script
 * @property string $codigo_error
 * @property string $descripcion
 * @property string $base_hospital
 * @property int $id_hospital
 * @property int $id_maestro
 * @property string $timestamp
 */
class Logscript extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logscript';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['script', 'codigo_error', 'descripcion', 'base_hospital'], 'string'],
            [['id_hospital', 'id_maestro'], 'default', 'value' => null],
            [['id_hospital', 'id_maestro'], 'integer'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'script' => 'Script',
            'codigo_error' => 'Codigo Error',
            'descripcion' => 'Descripcion',
            'base_hospital' => 'Base Hospital',
            'id_hospital' => 'Id Hospital',
            'id_maestro' => 'Id Maestro',
            'timestamp' => 'Fecha',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('logscript.id', 10),
			'script' => array('logscript.script', 10),
			'codigo_error' => array('logscript.codigo_error', 13),
			'descripcion' => array('logscript.descripcion', 12),
			'base_hospital' => array('logscript.base_hospital', 14),
			'id_hospital' => array('logscript.id_hospital', 12),
			'id_maestro' => array('logscript.id_maestro', 11),
			'timestamp' => array('logscript.timestamp', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'script',
			'codigo_error',
			'descripcion',
			'base_hospital',
			'id_hospital',
			'id_maestro',
			'timestamp',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
				],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'script',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_error',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'base_hospital',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_hospital',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_maestro',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'timestamp',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->timestamp=date('d/m/Y', strtotime($this->timestamp));    
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

}
