<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ObraSocialSearch represents the model behind the search form about `app\modules\mpi\models\ObraSocial`.
 */
class ObraSocialSearch extends ObraSocial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'nombre','cod_os'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
        
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObraSocial::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'nombre',
                'cod_os'
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'obra_social.id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'lower(obra_social.nombre)',strtolower($this->nombre)])
        ->andFilterWhere(['like', 'lower(CAST(obra_social.cod_os as varchar))',strtolower($this->cod_os)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('obrasocial-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
