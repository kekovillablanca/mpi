<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProfesionalSearch represents the model behind the search form about `app\modules\mpi\models\Profesional`.
 */
class ProfesionalSearch extends Profesional
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['dni', 'nombre', 'apellido', 'matricula', 'id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Profesional::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'dni',
				'nombre',
				'apellido',
				'matricula',	
				'id',
				
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'profesional.matricula' => $this->matricula,
            'profesional.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(profesional.dni)',strtolower($this->dni)])
              ->andFilterWhere(['like', 'lower(profesional.nombre)',strtolower($this->nombre)])
              ->andFilterWhere(['like', 'lower(profesional.apellido)',strtolower($this->apellido)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('profesional-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
