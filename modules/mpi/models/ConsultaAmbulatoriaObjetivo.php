<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consulta_ambulatoria_objetivo".
 *
 * @property int $paciente_id
 * @property string $fecha_consulta
 * @property string $nomenclador
 * @property string $codigo_nomenclador
 * @property string $examen_especialidad
 * @property string $examen_area
 * @property string $examen_tipo
 * @property string $examen_resultado
 * @property string $valor
 * @property string $detalle
 */
class ConsultaAmbulatoriaObjetivo extends \yii\db\ActiveRecord
{

	public static $dbid;
    public static $schema;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::$schema.'.consulta_ambulatoria_objetivo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paciente_id'], 'default', 'value' => null],
            [['paciente_id'], 'integer'],
            [['fecha_consulta'], 'safe'],
            [['nomenclador','codigo_nomenclador','examen_especialidad', 'examen_area', 'examen_tipo', 'examen_resultado', 'valor'], 'string'],
            [['detalle'], 'string', 'max' => 8192],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paciente_id' => 'Paciente ID',
            'fecha_consulta' => 'Fecha Consulta',
            'nomenclador' => 'Nomenclador',
            'codigo_nomenclador' => 'Codigo Nomenclador',
            'examen_especialidad' => 'Examen Especialidad',
            'examen_area' => 'Examen Area',
            'examen_tipo' => 'Examen Tipo',
            'examen_resultado' => 'Examen Resultado',
            'valor' => 'Valor',
            'detalle' => 'Detalle',
        ];
    }
    public function attributePrint()
    {
        return [
			'paciente_id' => array('consulta_ambulatoria_objetivo.paciente_id', 12),
			'fecha_consulta' => array('consulta_ambulatoria_objetivo.fecha_consulta', 15),
			'nomenclador' => array('consulta_ambulatoria_objetivo.nomenclador', 12),
			'codigo_nomenclador' => array('consulta_ambulatoria_objetivo.codigo_nomenclador', 19),
			'examen_especialidad' => array('consulta_ambulatoria_objetivo.examen_especialidad', 20),
			'examen_area' => array('consulta_ambulatoria_objetivo.examen_area', 12),
			'examen_tipo' => array('consulta_ambulatoria_objetivo.examen_tipo', 12),
			'examen_resultado' => array('consulta_ambulatoria_objetivo.examen_resultado', 17),
			'valor' => array('consulta_ambulatoria_objetivo.valor', 10),
			'detalle' => array('consulta_ambulatoria_objetivo.detalle', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'paciente_id',
			'fecha_consulta',
			'nomenclador',
			'codigo_nomenclador',
			'examen_especialidad',
			'examen_area',
			'examen_tipo',
			'examen_resultado',
			'valor',
			'detalle',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_id',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_consulta',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'examen_especialidad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'examen_area',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'examen_tipo',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'examen_resultado',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'valor',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'detalle',
			// ],
        ];
    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

    public function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;
        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
            case 500:
                self::$schema='hospitalsao';
                break;
            case 600:
                self::$schema='hospitalbolson';
                break;
        }
    }

    public static function primaryKey()
	{
        return ['paciente_id','fecha_consulta'];
	}

}
