<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "telefono".
 *
 * @property string $paciente_id
 * @property string $telefono
 * @property string $tipo
 */
class Telefono extends \yii\db\ActiveRecord
{
    public $telefono_completo;
    public static $dbid;
    public static $schema;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::$schema.'.telefono';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono','tipo'], 'string'],
            [['paciente_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paciente_id' => 'ID',
            'telefono' => 'Telefono',
            'tipo' => 'Tipo',
        ];
    }
    public function attributePrint()
    {
        return [
			'paciente_id' => array('telefono.paciente_id', 4),
			'telefono' => array('telefono.telefono', 10),
			'tipo' => array('telefono.tipo', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'paciente_id',
			'telefono',
			'tipo',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
            // faltan los atributos pero no interesa
            ];
    }




    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->telefono_completo=$this->telefono." (".$this->tipo.")";
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

    public static function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;
        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
        }
    }

	public static function primaryKey()
	{
		return ['paciente_id'];
	}


}
