<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consulta_ambulatoria_plan".
 *
 * @property int $paciente_id
 * @property string $fecha_consulta
 * @property string $nomenclador
 * @property string $codigo_nomenclador
 * @property string $seccion_nomenclador
 * @property string $grupo_nomenclador
 * @property string $descripcion_breve
 * @property string $descripcion_completa
 * @property string $detalle
 * @property string $preinforme
 * @property int $informe_adjunto
 */
class ConsultaAmbulatoriaPlan extends \yii\db\ActiveRecord
{

	public static $dbid;
    public static $schema;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::$schema.'.consulta_ambulatoria_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paciente_id'], 'default', 'value' => null],
            [['paciente_id','informe_adjunto'], 'integer'],
            [['fecha_consulta'], 'safe'],
            [['nomenclador', 'codigo_nomenclador', 'seccion_nomenclador', 'grupo_nomenclador', 'descripcion_breve', 'descripcion_completa', 'detalle', 'preinforme'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paciente_id' => 'Paciente ID',
            'fecha_consulta' => 'Fecha Consulta',
            'nomenclador' => 'Nomenclador',
            'codigo_nomenclador' => 'Codigo',
            'seccion_nomenclador' => 'Seccion',
            'grupo_nomenclador' => 'Grupo',
            'descripcion_breve' => 'Descripcion Breve',
            'descripcion_completa' => 'Descripcion Completa',
            'detalle' => 'Detalle',
            'preinforme' => 'Preinforme',
            'informe_adjunto' => 'Informe Adjunto',
        ];
    }
    public function attributePrint()
    {
        return [
			'paciente_id' => array('consulta_ambulatoria_plan.paciente_id', 12),
			'fecha_consulta' => array('consulta_ambulatoria_plan.fecha_consulta', 15),
			'nomenclador' => array('consulta_ambulatoria_plan.nomenclador', 12),
			'codigo_nomenclador' => array('consulta_ambulatoria_plan.codigo_nomenclador', 10),
			'seccion_nomenclador' => array('consulta_ambulatoria_plan.seccion_nomenclador', 10),
			'grupo_nomenclador' => array('consulta_ambulatoria_plan.grupo_nomenclador', 10),
			'descripcion_breve' => array('consulta_ambulatoria_plan.descripcion_breve', 18),
			'descripcion_completa' => array('consulta_ambulatoria_plan.descripcion_completa', 20),
			'detalle' => array('consulta_ambulatoria_plan.detalle', 12),
			'preinforme' => array('consulta_ambulatoria_plan.preinforme', 11),
			'informe_adjunto' => array('consulta_ambulatoria_plan.informe_adjunto', 16),
        ];
    }

    public function attributeView()
    {
        return [
			'paciente_id',
			'fecha_consulta',
			'nomenclador',
			'codigo_nomenclador',
			'seccion_nomenclador',
			'grupo_nomenclador',
			'descripcion_breve',
			'descripcion_completa',
			'detalle',
			'preinforme',
			'informe_adjunto',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_id',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_consulta',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'seccion_nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'grupo_nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion_breve',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'descripcion_completa',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'detalle',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'preinforme',
			// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'informe_adjunto',
			],
        ];
    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

    public function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;
        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
            case 500:
                self::$schema='hospitalsao';
                break;
            case 600:
                self::$schema='hospitalbolson';
                break;
        }
    }

    public static function primaryKey()
	{
        return ['paciente_id','fecha_consulta'];
	}

}
