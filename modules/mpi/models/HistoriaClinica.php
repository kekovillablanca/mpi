<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consultasestadisticas.historia_clinica".
 *
 * @property int $id
 * @property string $historia_clinica
 * @property int $id_establecimiento
 * @property int $id_paciente
 * @property Paciente $paciente
 * @property Establecimiento $establecimiento
 */
class HistoriaClinica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.historia_clinica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_establecimiento', 'id_paciente'], 'default', 'value' => null],
            [['id_establecimiento', 'id_paciente'], 'integer'],
            [['historia_clinica'], 'default', 'value' => null],
            [['historia_clinica'], 'string', 'max' => 8],
            [['id_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimiento::className(), 'targetAttribute' => ['id_establecimiento' => 'id']],
            [['id_paciente'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['id_paciente' => 'id']],
            [['historia_clinica'], 'unique', 'targetAttribute' => ['id_establecimiento','historia_clinica'],'message' => "la historia clincia ya se encuentra ocupada en el efector"],
            ['historia_clinica', 'unique', 'targetAttribute' => ['id_establecimiento', 'historia_clinica','id_paciente'], 'skipOnEmpty' => false],
            [['id_establecimiento'], 'unique', 'targetAttribute' => ['historia_clinica', 'id_establecimiento','id_paciente'],'message' => "la historia clinica ya se encuentra en este establecimiento"],
            
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            //'id' => 'ID',
            'historia_clinica' => 'Historia Clinica',
            'id_establecimiento' => 'Id Establecimiento',
            'id_paciente' => 'Id Paciente',
        ];
    }
    public function attributePrint()
    {
        return [
			//'id' => array('consultasestadisticas.historia_clinica.id', 10),
			'historia_clinica' => array('consultasestadisticas.historia_clinica.historia_clinica', 17),
			'id_establecimiento' => array('consultasestadisticas.establecimiento.nombre',19),
			'id_paciente' => array('consultasestadisticas.paciente.nombre',12),
        ];
    }

    public function attributeView()
    {
        return [
			//'id',
			'historia_clinica',
			'id_establecimiento'=>
			[
				'attribute'=>'establecimiento.nombre',
				'label'=>'Establecimiento',
			],
			'id_paciente'=>
			[
				'attribute'=>'paciente.nombre',
				'label'=>'Paciente',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'historia_clinica',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_establecimiento',
				'value'=>'establecimiento.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_paciente',
				'value'=>'paciente.nombre',
			],
        
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
   /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['id' => 'id_paciente']);
    }
	  /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstablecimiento()
    {
        return $this->hasOne(Establecimiento::className(), ['id' => 'id_establecimiento']);
    }
   
      /**
     * @return \yii\db\ActiveQuery
     */

       
        public function __toString() {
            return $this->historia_clinica;
        }
}
