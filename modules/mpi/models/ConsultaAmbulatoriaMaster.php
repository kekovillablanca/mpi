<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;



use app\modules\mpi\utils\CsvUtil;
use Yii;
use app\components\Metodos\Metodos;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "consulta_ambulatoria_master".
 *
 * @property int $id1
 * @property string $id2
 * @property int $dominio
 * @property string $area
 * @property int $centro_id
 * @property string $centro
 * @property int $paciente_id
 * @property string $paciente_numero_hc
 * @property string $paciente_ape
 * @property string $paciente_nom
 * @property string $paciente_nac
 * @property string $paciente_sexo
 * @property string $paciente_tipodoc
 * @property string $paciente_numerodoc
 * @property string $fecha
 * @property string $especialidad
 * @property string $servicio
 * @property int $medico_id
 * @property string $medico_ape
 * @property string $medico_nom
 * @property int $numero_turno
 * @property string $cie10_codigo
 * @property string $cie10_tipo_diag
 * @property string $cie10_descricion
 * @property string $cie10_observacion
 * 
 */
class ConsultaAmbulatoriaMaster extends \yii\db\ActiveRecord
{

	public static $dbid;
    public static $schema;
	public $nombre_completo;
	public $medico_completo;


	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {

		return self::$schema.'.consulta_ambulatoria_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id1', 'dominio','centro_id', 'paciente_id', 'medico_id', 'numero_turno'], 'default', 'value' => null],
            [['id1', 'dominio','centro_id', 'paciente_id', 'medico_id', 'numero_turno'], 'integer'],
            [['id2', 'paciente_nac', 'fecha'], 'safe'],
            [['paciente_numero_hc'], 'string', 'max' => 20],
            [['paciente_ape', 'medico_ape'], 'string', 'max' => 85],
            [['paciente_nom', 'medico_nom'], 'string', 'max' => 60],
            [['paciente_sexo', 'cie10_tipo_diag'], 'string', 'max' => 1],
            [['paciente_tipodoc'], 'string', 'max' => 3],
            [['paciente_numerodoc'], 'string', 'max' => 15],
            [['area','centro' ,'especialidad', 'servicio'], 'string', 'max' => 50],
            [['cie10_codigo'], 'string', 'max' => 7],
            [['cie10_descricion'], 'string', 'max' => 200],
            [['cie10_observacion'], 'string', 'max' => 8192],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id1' => 'Id1',
            'id2' => 'Fecha Hora',
            'dominio' => 'Dominio',
            'centro_id' => 'Centro ID',
            'paciente_id' => 'Paciente ID',
            'paciente_numero_hc' => 'Numero Hc',
            'paciente_ape' => 'Apellido',
            'paciente_nom' => 'Nombre',
            'paciente_nac' => 'Fecha Nac',
            'paciente_sexo' => 'Sexo',
            'paciente_tipodoc' => 'Tipo Doc',
            'paciente_numerodoc' => 'Numero Doc',
            'fecha' => 'Fecha',
            'especialidad' => 'Especialidad',
            'servicio' => 'Servicio',
            'medico_id' => 'Médico ID',
            'medico_ape' => 'Médico Ape',
            'medico_nom' => 'Médico Nom',
            'numero_turno' => 'Numero Turno',
            'area' => 'Area',
            'centro' => 'Centro',
			'cie10_codigo' => 'CIE10 Código',
            'cie10_tipo_diag' => 'CIE10 Tipo Diag',
            'cie10_descricion' => 'CIE10 Descricion',
            'cie10_observacion' => 'CIE10 Observacion',
			'medico_completo' => 'Médico'
		];
    }
    public function attributePrint()
    {
        return [
			'id1' => array('consulta_ambulatoria_master.id1', 10),
			'id2' => array('consulta_ambulatoria_master.id2', 10),
			'dominio' => array('consulta_ambulatoria_master.dominio', 10),
			'centro_id' => array('consulta_ambulatoria_master.centro_id', 10),
			'area' => array('consulta_ambulatoria_master.area', 20), 
			'centro' => array('consulta_ambulatoria_master.centro', 20), 
			'paciente_id' => array('consulta_ambulatoria_master.paciente_id', 12),
			'paciente_numero_hc' => array('consulta_ambulatoria_master.paciente_numero_hc', 10),
			'paciente_ape' => array('consulta_ambulatoria_master.paciente_ape', 30),
			'paciente_nom' => array('consulta_ambulatoria_master.paciente_nom', 30),
			'paciente_nac' => array('to_char(consulta_ambulatoria_master.paciente_nac,\'DD/MM/YYYY\')', 10),
			'paciente_sexo' => array('consulta_ambulatoria_master.paciente_sexo', 6),
			'paciente_tipodoc' => array('consulta_ambulatoria_master.paciente_tipodoc', 9),
			'paciente_numerodoc' => array('consulta_ambulatoria_master.paciente_numerodoc', 10),
			'fecha' => array('to_char(consulta_ambulatoria_master.fecha,\'DD/MM/YYYY\')', 10),
			'especialidad' => array('consulta_ambulatoria_master.especialidad', 20),
			'servicio' => array('consulta_ambulatoria_master.servicio', 20),
			'medico_id' => array('consulta_ambulatoria_master.medico_id', 10),
			'medico_ape' => array('consulta_ambulatoria_master.medico_ape', 30),
			'medico_nom' => array('consulta_ambulatoria_master.medico_nom', 30),
			'numero_turno' => array('consulta_ambulatoria_master.numero_turno', 13),
			'cie10_codigo' => array('consulta_ambulatoria_master.cie10_codigo', 13),
			'cie10_tipo_diag' => array('consulta_ambulatoria_master.cie10_tipo_diag', 10),
			'cie10_descricion' => array('consulta_ambulatoria_master.cie10_descricion', 30),
			'cie10_observacion' => array('consulta_ambulatoria_master.cie10_observacion', 30),
        ];
    }

    public function attributeView()
    {
        return [
			'id1',
			'id2',
			'dominio',
			'area',
			'centro_id',
			'centro',
			'paciente_id',
			'paciente_numero_hc',
			'paciente_ape',
			'paciente_nom',
			'paciente_nac',
			'paciente_sexo',
			'paciente_tipodoc',
			'paciente_numerodoc',
			'fecha',
			'especialidad',
			'servicio',
			'medico_id',
			'medico_ape',
			'medico_nom',
			'numero_turno',
			'cie10_codigo',
			'cie10_tipo_diag',
			'cie10_descricion',
			'cie10_observacion',
        ];
    }

    public function attributeColumns()
    {
        return [
/*			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id1',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id2',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_id',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'dominio',
			],
*/
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'area',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'centro',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_numero_hc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_ape',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_nom',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_nac',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_sexo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_tipodoc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_numerodoc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'especialidad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'medico_ape',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'medico_nom',
			],
/*			
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'medico_id',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero_turno',
			],
*/			
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cie10_codigo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cie10_tipo_diag',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cie10_descricion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cie10_observacion',
			],

        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->paciente_nac=Metodos::dateConvert($this->paciente_nac,'View');
		$this->fecha=Metodos::dateConvert($this->fecha,'View');
		$this->nombre_completo=$this->paciente_ape." ".$this->paciente_nom;
		$this->medico_completo=$this->medico_ape." ".$this->medico_nom;
		
	}

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom cde here
			$this->paciente_nac=Metodos::dateConvert($this->paciente_nac,'toSql');
			$this->fecha=Metodos::dateConvert($this->fecha,'toSql');
			return true;
        } else {
            return false;
        }
	}

    public static function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;
        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
			case 500:
				self::$schema='hospitalsao';
				break;
			case 600:
				self::$schema='hospitalbolson';
				break;
        }
    }

	public static function primaryKey()
	{
		return ['id1','id2'];
	}
	


	public function createCsvOdontologia($fechas)
{
    // Formatea las fechas
    $fechaInicio = date('Y-m-d', strtotime($fechas["fechaInicio"]));
    $fechaFin = date('Y-m-d', strtotime($fechas["fechaFin"]));

    // Obtiene la conexión a la base de datos
    $connection = Yii::$app->db;

    // Prepara la consulta para usar con un cursor
    $sql = "
        SELECT 
            profesional_nombre,
            profesional_apellido,
            paciente_nombre,
            paciente_apellido,
            descripcion_diagnostico,
            fecha_turno,
            diagnostico_codigo,
            descripcion_especialidad,
            centro_nombre,
            codigo
        FROM hospitalroca.odontologia 
        WHERE fecha_turno BETWEEN :fechaInicio AND :fechaFin";

    $command = $connection->createCommand($sql, [
        ':fechaInicio' => $fechaInicio,
        ':fechaFin' => $fechaFin,
    ]);

	$currentDate = date('Y-m-d');
    $delimiter = ',';
    $filename = "odontologia_".$fechaInicio.$fechaFin.".csv"; 
    $filepath = Yii::getAlias('@runtime/') .$filename;
	/*$parts = explode('_', $filename);
	$baseName = $parts[0]; // 'odontologia'
	$datePart = pathinfo($parts[1], PATHINFO_FILENAME); */
	if(!CsvUtil::send_file($filepath)){

	//if($datePart)
    $f = fopen($filepath, 'w');
    // Escribe las cabeceras CSV
    $fields = [
        'profesional_nombre', 'profesional_apellido', 'paciente_nombre', 'paciente_apellido', 
        'descripcion_diagnostico', 'fecha_turno', 'diagnostico_codigo', 'descripcion_especialidad', 
        'centro_nombre', 'codigo'
    ];
    fputcsv($f, $fields, $delimiter);
    $reader = $command->query();
	var_dump($reader);
	exit;
    while (($row = $reader->read()) !== false) {
        // Escribe los datos de cada fila en el archivo CSV
        $lineData = [
            $row['profesional_nombre'], $row['profesional_apellido'], $row['paciente_nombre'],
            $row['paciente_apellido'], $row['descripcion_diagnostico'], $row['fecha_turno'],
            $row['diagnostico_codigo'], $row['descripcion_especialidad'], $row['centro_nombre'],
            $row['codigo']
        ];
        fputcsv($f, $lineData, $delimiter);
    }
    rewind($f);

    fclose($f);
    return [
        'success' => true,
        'message' => 'Archivo CSV guardado correctamente.',
        'filepath' => '@runtime/' . $filename
    ];
    // Finaliza la ejecución del script

	}
	header('Content-Disposition: attachment; filename="'.$filename.'";');
    //setup utf8 encoding
    header('Content-Type: application/csv; charset=UTF-8');
    // showing the results

	Yii::$app->end();

}



}