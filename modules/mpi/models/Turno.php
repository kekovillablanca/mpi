<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\admin\models\UsuarioEstadistica;
use app\modules\sies\models\MArea;
use Exception;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "turno".
 *
 * @property int $id_paciente
 * @property string $fecha_turno
 * @property int $id_unidad_edad
 * @property string $ubicacion_hc
 * @property int $id
 * @property int $numero
 * @property bool $control_embarazo
 * @property bool $nino_sano
 * @property bool $primera_vez
 * @property bool $presente
 * @property string $observacion
 * @property int $id_diagnostico
 * @property int $id_area
 * @property int $id_servicio
 * @property int $id_profesional
 * @property int $edad
 * @property int $id_especialidad
* @property int $id_paciente_nom
 * @property Diagnostico $diagnostico
 * @property PServicio $servicio
 * @property Paciente $paciente
 * @property Profesional $profesional
 * @property Edad $unidadEdad
 * @property Establecimiento $establecimiento
 * @property Especialidad $especialidad
 */
class Turno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.turno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
         
            [['id_paciente', 'id_unidad_edad', 'numero', 'id_diagnostico', 'id_area', 'id_servicio', 'id_profesional','presente','id_especialidad'], 'integer' ],
			[['fecha_turno','id_paciente','id_unidad_edad', 'id_area', 'id_servicio', 'id_profesional','presente','edad','id_especialidad'],'required'],//
            [['control_embarazo', 'nino_sano', 'primera_vez'], 'boolean'],
			[['edad'],'integer','min'=>1,'max' => 999],
            [['observacion'], 'string', 'max' => 255],
            [['ubicacion_hc'], 'string', 'max' => 8],
			[['ubicacion_hc'], 'default', 'value' => null],
			[['control_embarazo'], 'default', 'value' => null],
            [['id_diagnostico'], 'exist', 'skipOnError' => true, 'targetClass' => Diagnostico::className(), 'targetAttribute' => ['id_diagnostico' => 'id']],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => PServicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
            [['id_paciente'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['id_paciente' => 'id']],
            [['id_profesional'], 'exist', 'skipOnError' => true, 'targetClass' => Profesional::className(), 'targetAttribute' => ['id_profesional' => 'id']],
            [['id_unidad_edad'], 'exist', 'skipOnError' => true, 'targetClass' => Edad::className(), 'targetAttribute' => ['id_unidad_edad' => 'id']],
            [['id_area'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimiento::className(), 'targetAttribute' => ['id_area' => 'id']],
			[['id_especialidad'], 'exist', 'skipOnError' => true, 'targetClass' => Especialidad::className(), 'targetAttribute' => ['id_especialidad' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
			
			'pacientenombre' => 'Paciente Nombre',
            'id_paciente' => 'Paciente Dni',
			'pacientesexo'=>'Paciente sexo',
			'pacienteapellido'=>'paciente apellido',
            'fecha_turno' => 'Fecha turno',
			'edad'=>'Edad',
            'id_unidad_edad' => 'Unidad Edad',
			'id_especialidad' =>'Especialidad',
            'ubicacion_hc' => 'Ubicacion HC',
            'numero' => 'Numero',
            'control_embarazo' => 'Control Embarazo',
            'nino_sano' => 'Control Ninio Sano',
            'primera_vez' => 'Primera Vez',
            'presente' => 'Presente',
            'observacion' => 'Observaciones',
            'id_diagnostico' => 'Diagnostico nombre',
			'diagnosticocodigo' => 'Diagnostico codigo',
            'id_area' => 'Nombre Efector',
            'id_servicio' => 'Servicio',
			'profesionalnombre' => 'Profesional Nombre',
            'id_profesional' => 'Profesional Apellido',
			'pacientehistoria'=>'Historia clinica del paciente',
			'pacienteobrasocial'=>'Paciente obra social',
			'establecimientolocalidad'=>'localidad'
			
        ];
    }
    public function attributePrint()
    {


        return [
			'id_area' => array('establecimiento.nombre', 82),
			'id_paciente' => array('paciente.numero_documento',12),
			'pacienteobrasocial'=>array('obra_social.nombre',19),
			'establecimientolocalidad'=>array('localidad.nombre',19),
			'pacienteapellido' => array('paciente.apellido',19),
			'pacientenombre' => array('paciente.nombre',19),
			'pacientesexo'=> array('sexo.nombre', 10),
			'id_especialidad'=> array('especialidad.nombre',20),
			// postgres
			//'fecha_turno' => array('TO_CHAR(turno.fecha_turno,\'DD/MM/YYYY\')', 12),
			// mysql
			'fecha_turno' => array('turno.fecha_turno', 20),
			'id_unidad_edad' => array('unidad_edad.descripcion',15),
			'ubicacion_hc' => array('turno.ubicacion_hc', 20),
			
			//'numero' => array('turno.numero', 10),
			'edad' => array('turno.edad', 10),
			// postgres
			'control_embarazo' => array('CASE WHEN turno.control_embarazo IS true THEN \'SI\' ELSE \'NO\' END', 10),
			// mysql
			//'control_embarazo' => array('IF(turno.control_embarazo = 1, \'SI\', \'NO\')', 17),
			// postgres
			'nino_sano' => array('CASE WHEN turno.nino_sano IS true THEN \'SI\' ELSE \'NO\' END', 10),
			// mysql
			//'nino_sano' => array('IF(turno.nino_sano = 1, \'SI\', \'NO\')', 10),
			// postgres
			'primera_vez' => array('CASE WHEN turno.primera_vez IS true THEN \'SI\' ELSE \'NO\' END', 12),
			// mysql
			//'primera_vez' => array('IF(turno.primera_vez = 1, \'SI\', \'NO\')', 12),
			// postgres
			'presente' => array('CASE WHEN turno.presente IS true THEN \'SI\' ELSE \'NO\' END', 10),
			// mysql
			//'presente' => array('IF(turno.presente = 1, \'SI\', \'NO\')', 10),
			'observacion' => array('turno.observacion', 255),
			'id_diagnostico' => array('diagnostico.nombre',15),
			'id_servicio' => array('p_servicio.nombre',12),
			'id_profesional' => array('profesional.apellido',15),
			'profesionalnombre' => array('profesional.nombre',19),
			'diagnosticocodigo'=>array('diagnostico.codigo',19),
			'pacientehistoria'=>array('STRING_AGG(historia_clinica.historia_clinica::text, \', \')', 50)

			
			
          
        ];
    }

    public function attributeView()
    {
        return [
			//'numero',
			'observacion',
			'fecha_turno',
			'ubicacion_hc',
			'id_paciente'=>
			[
				'attribute'=>'paciente.numero_documento',
				'label'=>'Paciente Dni',
			],
            'pacientenombre'=>
            [
				'attribute'=>'paciente.nombre',
				'label'=>'Paciente Nombre',
			],
			'pacientesexo'=>
            [
				'attribute'=>'paciente.idsexo.nombre',
				'label'=>'Paciente Sexo',
			],
			'pacienteapellido'=>
            [
				'attribute'=>'paciente.apellido',
				'label'=>'Paciente Apellido',
			],

			'pacientehistoria'=>
			[
				'attribute'=>'pacientehistoria',
				'label'=>'Historia clinica',
			],
			'edad',
			'id_unidad_edad'=>
			[
				'attribute'=>'unidadedad.descripcion',
				'label'=>'Unidad Edad',
			],
			'id_area'=>
			[
				'attribute'=>'establecimiento.nombre',
				'label'=>'Nombre efector',
			],
			'id_especialidad'=>
			[
				'attribute'=>'especialidad.nombre',
				'label'=>'Especialidad nombre',
			],
			'id_servicio'=>
			[
				'attribute'=>'servicio.nombre',
				'label'=>'Servicio',
			],
			'id_profesional'=>
			[
				'attribute'=>'profesional.apellido',
				'label'=>'Profesional Apellido',
			],
            'profesionalnombre'=>
            [
				'attribute'=>'profesional.nombre',
				'label'=>'Profesional Nombre',
			],
			'pacienteobrasocial'=>
            [
				'label'=>'obra social Nombre',
				'attribute'=>'pacienteobrasocial.obrasocial.nombre',

			],
			'establecimientolocalidad'=>
            [
				'label'=>'obra social Nombre',
				'attribute'=>'establecimientolocalidad.localidad.nombre',

			],
			'id_diagnostico'=>
			[
				'attribute'=>'diagnostico.nombre',
				'label'=>'Diagnostico',
			],
			'diagnosticocodigo'=>
			[
				'attribute'=>'diagnostico.codigo',
				'label'=>'Diagnostico',
			],
			'control_embarazo:boolean',
			'nino_sano:boolean',
			'primera_vez:boolean',
			'presente:boolean',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }
    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'pacientenombre',
				'value'=>'paciente.nombre',
			],
			['class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_paciente',
				'value'=>'paciente.numero_documento',
			],
			['class'=>'\kartik\grid\DataColumn',
			'attribute'=>'pacientesexo',
			'value'=>'paciente.idsexo.nombre',
		],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'pacienteapellido',
				'value'=>'paciente.apellido',
			],

			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'pacienteobrasocial',
				'value'=>'pacienteobrasocial.obrasocial.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'establecimientolocalidad',
				'value'=>'establecimientolocalidad.localidad.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_unidad_edad',
				'value'=>'unidadedad.descripcion',
			],
			
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_turno',
			],
			
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'edad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'ubicacion_hc',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			/*[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero',
			],*/
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'control_embarazo',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nino_sano',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'primera_vez',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'presente',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'observacion',
			],
			[
				 'class'=>'\kartik\grid\DataColumn',
				 'attribute'=>'id_diagnostico',
				 'value'=>'diagnostico.nombre',
			 ],
			 [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diagnosticocodigo',
				'value'=>'diagnostico.codigo',
			],
			 [
				 'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_area',
				 'value'=>'establecimiento.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_servicio',
				 'value'=>'servicio.nombre',
			 ],
			 [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'profesionalnombre',
				'value'=>'profesional.nombre',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_profesional',
				'value'=>'profesional.apellido',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_especialidad',
				'value'=>'especialidad.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'pacientehistoria',
				'value'=>'pacientehistoria',
			],
        ];
    }
	public function verificarFechaSeaCorrecta($fecha){
				$descripcciones=explode("/",$fecha);
                $fechaTotal=[];
                $fechaTotal['dia']=$descripcciones[0];
                $fechaTotal['mes']=$descripcciones[1];   
                $fechaTotal['anio']=$descripcciones[2];
                $fechaEntera = time();
                $anio = date("Y", $fechaEntera);
                $mes = date("m", $fechaEntera);
                $dia = date("d", $fechaEntera);
				//verifica que la fecha del turno asignada sea menor a el año actual
				if($fechaTotal['anio']<$anio){
                    return 'errorPasado';
                }
				if(($anio<$fechaTotal['anio'] || $anio==$fechaTotal['anio'] && $fechaTotal['mes'] > $mes)){
					
					return 'errorFuturo';
			}
			return 'ok';
	}
	public function getPacientehistoria()
    {
		$historias=HistoriaClinica::find()->andWhere(['not', ['historia_clinica' => null]])->andWhere(['id_paciente'=>$this->id_paciente])->select('historia_clinica')->all();
		return implode(", ", $historias);
    }


	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiagnostico()
    {
        return $this->hasOne(Diagnostico::className(), ['id' => 'id_diagnostico']);
    }
	
	 /**
     * @return \yii\db\ActiveQuery
     */
	public function getEspecialidad()
    {
        return $this->hasOne(Especialidad::className(), ['id' => 'id_especialidad']);
    }
  
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(PServicio::className(), ['id' => 'id_servicio']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['id' => 'id_paciente']);
    }
	

	public function getPacienteobrasocial()
    {
		return $this->getPaciente();
	
    }
	public function getDiagnosticocodigo()
    {
		return $this->getDiagnostico()->select('codigo');
	
    }
	
    public function getPacienteapellido()
    {
		return $this->getPaciente()->select('apellido');
    }
	public function getPacientenombre()
    {
		return $this->getPaciente()->select('nombre');
    }
	
	public function getProfesionalnombre()
    {
		return $this->getProfesional()->select('nombre');
    }

	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesional()
    {
        return $this->hasOne(Profesional::className(), ['id' => 'id_profesional']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidadedad()
    {
        return $this->hasOne(Edad::className(), ['id' => 'id_unidad_edad']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstablecimiento()
    {
        return $this->hasOne(Establecimiento::className(), ['id' => 'id_area']);
    }
	public function getEstablecimientolocalidad()
    {
        return $this->getEstablecimiento();
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha_turno=Metodos::dateConvert($this->fecha_turno,'View');
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha_turno=Metodos::dateConvert($this->fecha_turno,'toSql');
            return true;
        } else {
            return false;
        }
    }
}
