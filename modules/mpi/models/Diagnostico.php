<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\internacion\models\DiagnosticoObservacion;
use DateTime;
use yii\web\Response;

/**
 * This is the model class for table "diagnostico".
 *
 * @property string $codigo
 * @property string $nombre
 * @property int $id
 *
 */
class Diagnostico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.diagnostico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'string', 'max' => 6],
            [['nombre'], 'string', 'max' => 151],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'id' => 'ID',
        ];
    }
    public function attributePrint()
    {
        return [
			'codigo' => array('diagnostico.codigo', 20),
			'nombre' => array('diagnostico.nombre', 20),
			'id' => array('diagnostico.id', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'codigo',
			'nombre',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
        ];
    }

/**
 * Método para buscar diagnósticos según el ID y otros criterios adicionales.
 *
 * @param int $idDiagnostico El ID del diagnóstico.
 * @return Diagnostico|null El resultado de la consulta o null si no se encuentra.
 */
public function buscarDiagnosticos($idDiagnostico)
{
    $resultado = Diagnostico::find()
    ->where(['id' => $idDiagnostico])
    ->andWhere(['like', 'codigo', 'O', false])  // 'false' evita el escape automático de '%'
    ->one();

return $resultado;

}


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurnos()
    {
        return $this->hasMany(Turno::className(), ['id_diagnostico' => 'id']);
    }
      public function getDiagnosticoobservacion()
    {
        return $this->hasMany(DiagnosticoObservacion::className(), ['id_diagnostico' => 'id']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
