<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consulta_ambulatoria_subjetivo".
 *
 * @property int $paciente_id
 * @property string $fecha_consulta
 * @property string $nomenclador
 * @property string $codigo_nomenclador
 * @property string $codigo_capitulo
 * @property string $codigo_componente
 * @property string $codigo_descripcion
 * @property string $detalle
 */
class ConsultaAmbulatoriaSubjetivo extends \yii\db\ActiveRecord
{

	public static $dbid;
    public static $schema;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::$schema.'.consulta_ambulatoria_subjetivo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paciente_id'], 'default', 'value' => null],
            [['paciente_id'], 'integer'],
            [['fecha_consulta'], 'safe'],
            [['nomenclador', 'codigo_nomenclador', 'codigo_capitulo', 'codigo_componente', 'codigo_descripcion'], 'string'],
            [['detalle'], 'string', 'max' => 8192],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paciente_id' => 'Paciente ID',
            'fecha_consulta' => 'Fecha Consulta',
            'nomenclador' => 'Nomenclador',
            'codigo_nomenclador' => 'Codigo Nomenclador',
            'codigo_capitulo' => 'Codigo Capitulo',
            'codigo_componente' => 'Codigo Componente',
            'codigo_descripcion' => 'Codigo Descripcion',
            'detalle' => 'Detalle',
        ];
    }
    public function attributePrint()
    {
        return [
			'paciente_id' => array('consulta_ambulatoria_subjetivo.paciente_id', 12),
			'fecha_consulta' => array('consulta_ambulatoria_subjetivo.fecha_consulta', 15),
			'nomenclador' => array('consulta_ambulatoria_subjetivo.nomenclador', 12),
			'codigo_nomenclador' => array('consulta_ambulatoria_subjetivo.codigo_nomenclador', 19),
			'codigo_capitulo' => array('consulta_ambulatoria_subjetivo.codigo_capitulo', 16),
			'codigo_componente' => array('consulta_ambulatoria_subjetivo.codigo_componente', 18),
			'codigo_descripcion' => array('consulta_ambulatoria_subjetivo.codigo_descripcion', 19),
			'detalle' => array('consulta_ambulatoria_subjetivo.detalle', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'paciente_id',
			'fecha_consulta',
			'nomenclador',
			'codigo_nomenclador',
			'codigo_capitulo',
			'codigo_componente',
			'codigo_descripcion',
			'detalle',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_id',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_consulta',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_nomenclador',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_capitulo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_componente',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_descripcion',
			],
		    [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'detalle',
			],
        ];
    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

    public function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;
        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
            case 500:
                self::$schema='hospitalsao';
                break;
            case 600:
                self::$schema='hospitalbolson';
                break;
        }
    }

	public static function primaryKey()
	{
        return ['paciente_id','fecha_consulta'];
	}


}
