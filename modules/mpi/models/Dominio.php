<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "dominio".
 *
 * @property int $id
 * @property string $area
 * @property string $base
 * @property string $dominio
 * @property string $nombre_renaper
 * @property string $clave_renaper
 * @property string $coddominio_renaper
 *
 * @property Maestro[] $maestros
 * @property Usuario[] $usuarios
 *
 */

class Dominio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dominio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['area', 'base'], 'string', 'max' => 50],
            [['dominio'], 'string', 'max' => 200],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area' => 'Area',
            'base' => 'Base',
            'dominio' => 'Dominio',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('dominio.id', 10),
			'area' => array('dominio.area', 20),
			'base' => array('dominio.base', 20),
			'dominio' => array('dominio.dominio', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'area',
			'base',
			'dominio',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
            ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'area',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'base',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'dominio',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaestros()
    {
        return $this->hasMany(Maestro::className(), ['id_area' => 'id']);
    }

/*    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id_dominio' => 'id']);
    }
*/

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
