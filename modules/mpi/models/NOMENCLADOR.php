<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "nomenclador".
 *
 * @property string $id
 * @property string $nombre
 */
class NOMENCLADOR extends \yii\db\ActiveRecord
{
	public static $dbid;
    public static $schema;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return self::$schema.'.nomenclador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string'],
            [['id'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('nome.id', 4),
			'nombre' => array('nome.nombre', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
        ];
    }




    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

    public function setDbId($dbid)		// dbid is a string
    {
        self::$dbid = $dbid;
        switch(self::$dbid)
        {

			case 100:
                self::$schema='hospitalviedma';
                break;
			case 200:
                self::$schema='hospitalbariloche';
                break;
			case 300:
                self::$schema='hospitalroca';
                break;
            case 400:
                self::$schema='hospitalcipolletti';
                break;
        }
    }

	public static function primaryKey()
	{
		return ['id'];
	}


}
