<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consultasestadisticas.contacto".
 *
 * @property int $id
 * @property string $numero_telefono
 */
class Contacto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.contacto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_telefono'], 'default', 'value' => null],
            [['numero_telefono'], 'string', 'max' => 10],
            [['numero_telefono'], 'match', 'pattern' => '/^\d+$/', 'message' => 'solamente numeros.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_telefono' => 'Numero Telefono',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('consultasestadisticas.contacto.id', 10),
			'numero_telefono' => array('consultasestadisticas.contacto.numero_telefono', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'numero_telefono',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero_telefono',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
