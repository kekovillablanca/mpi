<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consultasestadisticas.especialidad".
 *
 * @property int $id
 * @property string $nombre
 * @property int $id_servicio
 */
class Especialidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.especialidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           
            [['codigo'], 'default', 'value' => null],
            [['id_servicio'], 'integer'],
            [['id_servicio'],'required'],
            [['codigo'], 'double'],
            [['nombre'], 'string', 'max' => 40],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => PServicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'id_servicio' => 'servicio',
            'codigo' => 'codigo',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('especialidad.id', 10),
			'nombre' => array('especialidad.nombre', 20),
			'codigo' => array('especialidad.codigo',12),
            'id_servicio' => array('p_servicio.nombre',12),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
			'id_servicio'=>
			[
				'attribute'=>'pservicio.nombre',
				'label'=>'Id Servicio',
			],
            'codigo'
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			 [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
				 ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_servicio',
				'value'=>'pservicio.nombre',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }
    public function getPservicio(){
        return $this->hasOne(PServicio::className(), ['id' => 'id_servicio']);
    }

    public function getTurnos()
    {
        return $this->hasMany(Turno::className(), ['id_servicio' => 'id']);
    }


    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
