<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consultasestadisticas.thesep".
 *
 * @property int $id
 * @property int $id_especialidad
 * @property int $id_servicio
 * @property string $fecha
 */
class Thesep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.thesep';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_especialidad', 'id_servicio'], 'required'],
            [['id_especialidad', 'id_servicio'], 'default', 'value' => null],
            [['id_especialidad', 'id_servicio'], 'integer'],
            [['fecha'], 'safe'],
            [['id_especialidad'], 'exist', 'skipOnError' => true, 'targetClass' => Especialidad::className(), 'targetAttribute' => ['id_especialidad' => 'id']],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => PServicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_especialidad' => 'Especialidad',
            'id_servicio' => 'Servicio',
            'fecha' => 'Fecha',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('consultasestadisticas.thesep.id', 10),
			'id_especialidad' => array('consultasestadisticas.especialidad.nombre',16),
			'id_servicio' => array('consultasestadisticas.p_servicio.nombre',12),
			// postgres
			'fecha' => array('TO_CHAR(consultasestadisticas.thesep.fecha,\'DD/MM/YYYY\')', 10),
			// mysql
			'fecha' => array('DATE_FORMAT(consultasestadisticas.thesep.fecha,\'%d/%m/%Y\')', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_especialidad'=>
			[
				'attribute'=>'especialidad.nombre',
				'label'=>'Especialidad',
			],
			'id_servicio'=>
			[
				'attribute'=>'pservicio.nombre',
				'label'=>'Nombre',
			],
			'fecha',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_especialidad',
				'value'=>'especialidad.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_servicio',
				'value'=>'pservicio.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha=Metodos::dateConvert($this->fecha,'View');
    }

    public function getPservicio(){
        return $this->hasOne(PServicio::className(), ['id' => 'id_servicio']);
    }
    public function getEspecialidad(){
        return $this->hasOne(Especialidad::className(), ['id' => 'id_especialidad']);
    }
    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha=Metodos::dateConvert($this->fecha,'toSql');
            return true;
        } else {
            return false;
        }
    }
}
