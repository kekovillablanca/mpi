<?php

namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ListadoForm extends Model
{
    public $fecha_desde;
    public $fecha_hasta;
    public $dominio;
    public $centro;
    public $servicio;
    public $especialidad;
    public $diagnostico;

    public $subjetivoicpc2_1;
    public $subjetivoicpc2_2;
    public $subjetivoicpc2_3;
    public $subjetivoicpc2_4;
    public $subjetivoicpc2_5;
    public $subjetivoobs;

    public $objetivosistema_1;
    public $objetivosistema_2;
    public $objetivosistema_3;
    public $objetivosistema_4;
    public $objetivosistema_5;
    public $objetivoobs;

    public $analisiscie10_1;
    public $analisiscie10_2;
    public $analisiscie10_3;
    public $analisiscie10_4;
    public $analisiscie10_5;
    public $analisisobs;

    public $plannome_1;
    public $plannome_2;
    public $plannome_3;
    public $plannome_4;
    public $plannome_5;
    public $planobs;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['fecha_desde', 'fecha_hasta', 'dominio'], 'required'],
            [['servicio', 'especialidad','diagnostico','centro'], 'safe'],
            [['subjetivoicpc2_1','subjetivoicpc2_2','subjetivoicpc2_3','subjetivoicpc2_4','subjetivoicpc2_5','subjetivoobs'], 'safe'],
            [['objetivosistema_1','objetivosistema_2','objetivosistema_3','objetivosistema_4','objetivosistema_5','objetivoobs'], 'safe'],
            [['analisiscie10_1','analisiscie10_2','analisiscie10_3','analisiscie10_4','analisiscie10_5','analisisobs'], 'safe'],
            [['plannome_1','plannome_2','plannome_3','plannome_4','plannome_5','planobs'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
	return [
	    'fecha_desde' => 'Fecha desde',
	    'fecha_hasta' => 'Fecha hasta',
        'dominio' => 'Area',
        'centro' => 'Centro',
        'servicio' => 'Servicio',
        'especialidad' => 'Especialidad',
        'diagnostico' => 'Diagnostico Principal',
        'subjetivoicpc2_1' => 'ICPC2 (1)',
        'subjetivoicpc2_2' => 'ICPC2 (2)',
        'subjetivoicpc2_3' => 'ICPC2 (3)',
        'subjetivoicpc2_4' => 'ICPC2 (4)',
        'subjetivoicpc2_5' => 'ICPC2 (5)',
        'subjetivoobs' => 'Observaciones (busqueda por palabra clave)',
        'objetivosistema_1' => 'Sistema (1)',
        'objetivosistema_2' => 'Sistema (2)',
        'objetivosistema_3' => 'Sistema (3)',
        'objetivosistema_4' => 'Sistema (4)',
        'objetivosistema_5' => 'Sistema (5)',
        'objetivoobs' => 'Observaciones (busqueda por palabra clave)',
        'analisiscie10_1' => 'CIE10 (1)',
        'analisiscie10_2' => 'CIE10 (2)',
        'analisiscie10_3' => 'CIE10 (3)',
        'analisiscie10_4' => 'CIE10 (4)',
        'analisiscie10_5' => 'CIE10 (5)',
        'analisisobs' => 'Observaciones (busqueda por palabra clave)',
        'plannome_1' => 'NOMENCLADOR (1)',
        'plannome_2' => 'NOMENCLADOR (2)',
        'plannome_3' => 'NOMENCLADOR (3)',
        'plannome_4' => 'NOMENCLADOR (4)',
        'plannome_5' => 'NOMENCLADOR (5)',
        'planobs' => 'Observaciones (busqueda por palabra clave)',

    ];
    }

}


