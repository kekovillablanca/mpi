<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PServicioSearch represents the model behind the search form about `app\modules\mpi\models\PServicio`.
 */
class PServicioSearch extends PServicio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['codigo'],'integer'],
            [['id', 'nombre', 'codigo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PServicio::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'nombre',
				'codigo',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'p_servicio.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(p_servicio.nombre)',strtolower($this->nombre)])
              ->andFilterWhere(['like', 'CAST(codigo as varchar)',strtolower($this->codigo)]);
              

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('pservicio-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
