<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "p_servicio".
 *
 * @property int $id
 * @property int $id_profesional
 * @property string $nombre
 * @property string $codigo
 *
 */
class PServicio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.p_servicio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['nombre'], 'string', 'max'=>150],
            [['nombre','codigo'],'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('p_servicio.id', 10),
			'nombre' => array('p_servicio.nombre', 20),
			'codigo' => array('p_servicio.codigo', 17),
        ];
    }

    public function attributeView()
    {
        return [
		
			
			'nombre',
			'codigo',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
        ];
    }
   
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecialidad()
    {
        return $this->hasMany(Especialidad::className(), ['id_servicio' => 'id']);
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
