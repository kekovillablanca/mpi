<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DiagnosticoSearch represents the model behind the search form about `app\modules\mpi\models\Diagnostico`.
 */
class DiagnosticoSearch extends Diagnostico
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['codigo', 'nombre', 'id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diagnostico::find()->joinWith(['diagnosticoobservacion']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'codigo',
				'nombre',
				'id',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'diagnostico.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(diagnostico.codigo)',strtolower($this->codigo)])
              ->andFilterWhere(['like', 'lower(diagnostico.nombre)',strtolower($this->nombre)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('diagnostico-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
