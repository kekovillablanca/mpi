<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MaestroSearch represents the model behind the search form about `app\models\Maestro`.
 */
class MaestroSearch extends Maestro
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id_maestro'], 'integer'],
            [['estado','documento_tipo', 'documento_numero', 'primer_apellido', 'segundo_apellido', 'primer_nombre', 'segundo_nombre', 'fecha_nacimiento', 'sexo', 'numero_hc', 'id_area', 'id_federado_provincial', 'id_federado_nacional', 'id_duplicado', 'id_error', 'stamp_id_error', 'stamp_flag_federado', 'stamp_flag_modificado', 'stamp_flag_autenticado'], 'safe'],
            [['flag_federado', 'flag_modificado', 'flag_autenticado'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Maestro::find();
		$query->joinWith(['dominio']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // para el search de estado
        $codigo_estado=null;
        if ($this->estado){
            $input = preg_quote(ucfirst($this->estado), '~');
            $estado=['0'=>'Activo','1'=>'Unificado','2'=>'Fallecido'];
            $auxiliar = preg_grep('~' . $input . '~', $estado);
            if ($auxiliar)
                $codigo_estado=array_key_first($auxiliar);
         
        }
            
        $dataProvider->setSort([
            'attributes' => [
				'id_maestro',
				'documento_tipo',
				'documento_numero' => [
                    'asc' => ["cast(NULLIF(regexp_replace(maestro.documento_numero, '\D', '', 'g'), '') AS numeric)" => SORT_ASC],
                    'desc' => ["cast(NULLIF(regexp_replace(maestro.documento_numero, '\D', '', 'g'), '') AS numeric)" => SORT_DESC],
                ],
				'primer_apellido',
				'segundo_apellido',
				'primer_nombre',
				'segundo_nombre',
				'fecha_nacimiento',
				'sexo',
				'numero_hc',
				'estado',
				'id_area'=> [
					'asc' => ['dominio.area' => SORT_ASC],
					'desc' => ['dominio.area' => SORT_DESC],
				],
				'id_federado_provincial',
				'id_federado_nacional'=> [
                    'asc' => ["cast(NULLIF(regexp_replace(maestro.id_federado_nacional, '\D', '', 'g'), '') AS numeric)" => SORT_ASC],
                    'desc' => ["cast(NULLIF(regexp_replace(maestro.id_federado_nacional, '\D', '', 'g'), '') AS numeric)" => SORT_DESC],
                ],
				'id_duplicado',
				'id_error',
				'flag_federado',
				'flag_modificado',
				'flag_autenticado',
				'stamp_id_error',
				'stamp_flag_federado',
				'stamp_flag_modificado',
				'stamp_flag_autenticado',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'maestro.id_maestro' => $this->id_maestro,
            'maestro.id_federado_provincial' => $this->id_federado_provincial,
            'maestro.id_duplicado' => $this->id_duplicado,
            'maestro.id_error' => $this->id_error,
            'maestro.stamp_id_error' => $this->stamp_id_error,
            'maestro.stamp_flag_federado' => $this->stamp_flag_federado,
            'maestro.stamp_flag_modificado' => $this->stamp_flag_modificado,
            'maestro.stamp_flag_autenticado' => $this->stamp_flag_autenticado,
        ]);

        $query->andFilterWhere(['like', 'lower(maestro.documento_tipo)',strtolower($this->documento_tipo)])
              ->andFilterWhere(['like', 'lower(maestro.documento_numero)',strtolower($this->documento_numero)])
              ->andFilterWhere(['like', 'lower(maestro.primer_apellido)',strtolower($this->primer_apellido)])
              ->andFilterWhere(['like', 'lower(maestro.segundo_apellido)',strtolower($this->segundo_apellido)])
              ->andFilterWhere(['like', 'lower(maestro.primer_nombre)',strtolower($this->primer_nombre)])
              ->andFilterWhere(['like', 'lower(maestro.segundo_nombre)',strtolower($this->segundo_nombre)])
              ->andFilterWhere(['like', 'to_char(maestro.fecha_nacimiento,\'DD/MM/YYYY\')',$this->fecha_nacimiento])
              ->andFilterWhere(['like', 'lower(maestro.sexo)',strtolower($this->sexo)])
              ->andFilterWhere(['like', 'lower(maestro.numero_hc)',strtolower($this->numero_hc)])
              ->andFilterWhere(['like', 'lower(dominio.area)',strtolower($this->id_area)])
              ->andFilterWhere(['like', 'lower(maestro.id_federado_nacional)',strtolower($this->id_federado_nacional)])
              ->andFilterWhere(['maestro.flag_federado'=>$this->flag_federado])
              ->andFilterWhere(['maestro.flag_modificado'=>$this->flag_modificado])
              ->andFilterWhere(['maestro.flag_autenticado'=>$this->flag_autenticado]);

        // para el search de estado
        if ($codigo_estado===0){
            $query->andWhere(['IS','maestro.estado', null ]);
        }else{
            $query->andFilterWhere(['maestro.estado'=>$codigo_estado]);
        }

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('maestro-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
