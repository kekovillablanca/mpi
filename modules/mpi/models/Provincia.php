<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consultasestadisticas.provincia".
 *
 * @property int $id
 * @property string $nombre
 * @property int $codigo
 */
class Provincia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.provincia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'default', 'value' => null],
            [['codigo'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo Provincia',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('consultasestadisticas.provincia.id', 10),
			'nombre' => array('consultasestadisticas.provincia.nombre', 20),
			'codigo' => array('consultasestadisticas.provincia.codigo', 17),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
			'codigo',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
