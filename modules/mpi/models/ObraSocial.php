<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "obra_social".
 *
 * @property int $id
 * @property string $nombre
 * * @property string $cod_os
 */
class ObraSocial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.obra_social';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           
            [['nombre'], 'string', 'max'=>160],
            [['cod_os'], 'integer'],
            [['nombre','cod_os'],'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'cod_os'=> 'Codigo'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('obra_social.id', 10),
			'nombre' => array('obra_social.nombre', 20),
            'cod_os' => array('obra_social.cod_os', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
            'cod_os'
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cod_os',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
