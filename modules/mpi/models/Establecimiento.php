<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "establecimiento".
 *
 * @property int $codigo_sisa
 * @property string $nombre
 * @property string $id_localidad
 * @property int $id
 */
class Establecimiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.establecimiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_sisa'], 'integer'],
            [['nombre'], 'string', 'max' => 82],
            [['id_localidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['id_localidad' => 'id']],
            [['codigo_sisa','nombre','id_localidad'] ,'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_sisa' => 'Codigo Sisa',
            'nombre' => 'Nombre',
            'id_localidad' => 'Localidad',
        
        ];
    }
    public function attributePrint()
    {
        return [
			'codigo_sisa' => array('establecimiento.codigo_sisa', 20),
			'nombre' => array('establecimiento.nombre', 82),
			'id_localidad' => array('localidad.nombre', 19),

        ];
    }

    public function attributeView()
    {
        return [
			'codigo_sisa',
			'nombre',
			'id_localidad'=>
            [
				'attribute'=>'localidad.nombre',
				'label'=>'localidad',
			],
	
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_sisa',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_localidad',
				'value'=>'localidad.nombre',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidad()
    {
        return $this->hasOne(Localidad::className(), ['id' => 'id_localidad']);
    }
    public function getHistoriaclinicas()
    {
        return $this->hasMany(HistoriaClinica::className(), ['id_establecimiento' => 'id']);
    }
  
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
