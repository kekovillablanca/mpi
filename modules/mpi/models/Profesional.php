<?php
/* Modelo generado por Model(Q) */
namespace app\modules\mpi\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\sies\models\MArea;
use yii\debug\models\search\Mail;

/**
 * This is the model class for table "profesional".
 *
 * @property string $dni
 * @property string $nombre
 * @property string $apellido
 * @property int $matricula
 * @property int $id
 * @property int $id_especialidad
 *
 * @property PServicio[] $pServicios
 * @property Especialidad $especialidad
 */
class Profesional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultasestadisticas.profesional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
                
            [['dni'],'string','max'=>10],
            [['dni'],'required'],
            [['matricula'],'string' ,'max'=>6,],
            [['matricula'],'required'],
            [['nombre', 'apellido'], 'string' ,'max'=>32],
            [['nombre','apellido'],'required'],
                 
           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'matricula' => 'Matricula',
            //'id' => 'ID',
        ];
    }
    public function attributePrint()
    {
        return [
			'dni' => array('profesional.dni', 10),
			'nombre' => array('profesional.nombre', 32),
			'apellido' => array('profesional.apellido', 32),
			'matricula' => array('profesional.matricula', 6),
			//'id' => array('profesional.id', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'dni',
			'nombre',
			'apellido',
			'matricula',
			
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'dni',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'apellido',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'matricula',
			],
	
			/* [
				'class'=>'\kartik\grid\DataColumn',
				 'attribute'=>'id',
				'width'=>'30px',
				 ],*/
        ];
    }
   

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
