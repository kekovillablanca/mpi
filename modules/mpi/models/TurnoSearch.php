<?php

namespace app\modules\mpi\models;

use app\modules\admin\models\UsuarioEstadistica;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;
use yii\debug\models\timeline\DataProvider;

/**
 * TurnoSearch represents the model behind the search form about `app\modules\mpi\models\Turno`.
 */
class TurnoSearch extends Turno
{
   
    public $pacientenombre;
    public $pacienteapellido;
    public $pacienteobrasocial;
    public $diagnosticocodigo;
    public $profesionalnombre;
    public $pacientehistoria;
    public $pacientesexo;
    public $establecimientolocalidad;
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['numero','edad'],'integer'],
            [['fecha_turno', 'id_unidad_edad', 'ubicacion_hc', 'id', 'observacion', 'id_diagnostico', 'id_area', 'id_servicio', 'id_profesional','id_paciente','pacientenombre','pacienteapellido','diagnosticocodigo','profesionalnombre','id_especialidad','pacientehistoria','pacienteobrasocial','establecimientolocalidad','pacientesexo'], 'safe'],
            [['control_embarazo', 'nino_sano', 'primera_vez', 'presente'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$q,$fechaInicio=null,$fechaFin=null)
    {     
        $query = Turno::find();
        $query->joinWith(['especialidad']);
		$query->joinWith(['diagnostico']);
		$query->joinWith(['servicio']);
		$query->joinWith(['paciente']);
        $query->joinWith(['paciente.idsexo']);
        $query->joinWith(['paciente.obrasocial']);
        $query->joinWith(['servicio.especialidad']);
		$query->joinWith(['profesional']);
		$query->joinWith(['unidadedad']);
		$query->joinWith(['establecimiento']);
        $query->joinWith(['establecimiento.localidad']);
        $query->joinWith(['establecimiento.localidad.provincia']);
        $query->joinWith(['paciente.historiaclinicas']);
        $query->addGroupBy([
            'turno.id',
            'establecimiento.codigo_sisa',
            'establecimiento.nombre',
            'turno.fecha_turno',
            'turno.edad',
            'sexo.nombre',
            'p_servicio.nombre',
            'localidad.nombre',
            'paciente.numero_documento',
            'paciente.nombre',
             'paciente.apellido',
             'obra_social.nombre',
             'turno.edad',
             'unidad_edad.descripcion',
             'diagnostico.nombre',
             'diagnostico.codigo',
             'profesional.nombre',
             'profesional.apellido',
             'especialidad.nombre'


        ]);
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null)){
           
        $query->where(['localidad.nombre'=>$q]);
        }
       
            $session = Yii::$app->session;
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);
        $this->load($params);
    
        
        $dataProvider->setSort([
            'attributes' => [
				'id_paciente'=> [
					'asc' => ['paciente.numero_documento' => SORT_ASC],
					'desc' => ['paciente.numero_documento' => SORT_DESC],
				],
                'id_especialidad'=> [
					'asc' => ['especialidad.nombre' => SORT_ASC],
					'desc' => ['especialidad.nombre' => SORT_DESC],
				],
                'pacientenombre'=> [
					'asc' => ['paciente.nombre' => SORT_ASC],
					'desc' => ['paciente.nombre' => SORT_DESC],
				],
                'pacienteapellido'=> [
					'asc' => ['paciente.apellido' => SORT_ASC],
					'desc' => ['paciente.apellido' => SORT_DESC],
				],
                'pacientesexo'=> [
					'asc' => ['sexo.nombre' => SORT_ASC],
					'desc' => ['sexo.nombre' => SORT_DESC],
				],
                'pacienteobrasocial'=> [
					'asc' => ['obra_social.nombre' => SORT_ASC],
					'desc' => ['obra_social.nombre' => SORT_DESC],
				],
                'establecimientolocalidad'=> [
					'asc' => ['localidad.nombre' => SORT_ASC],
					'desc' => ['localidad.nombre' => SORT_DESC],
				],
                
                'profesionalnombre'=> [
					'asc' => ['profesional.nombre' => SORT_ASC],
					'desc' => ['profesional.nombre' => SORT_DESC],
				],
               
				// mysql
				// 'fecha_turno'=> [
					// 'asc' => ['date_format(turno.fecha_turno, "%Y%m%d")' => SORT_ASC],
					// 'desc' => ['date_format(turno.fecha_turno,"%Y%m%d")' => SORT_DESC],
				// ],
				// postgres
				'fecha_turno',
				'id_unidad_edad'=> [
					'asc' => ['unidad_edad.descripcion' => SORT_ASC],
					'desc' => ['unidad_edad.descripcion' => SORT_DESC],
				],
				'ubicacion_hc',
				'id',
                'edad',
				'numero',
				'control_embarazo',
				'nino_sano',
				'primera_vez',
				'presente',
				'observacion',
                'diagnosticocodigo'=> [
					'asc' => ['diagnostico.codigo' => SORT_ASC],
					'desc' => ['diagnostico.codigo' => SORT_DESC],
				],
				'id_diagnostico'=> [
					'asc' => ['diagnostico.nombre' => SORT_ASC],
					'desc' => ['diagnostico.nombre' => SORT_DESC],
                    
				],
				'id_area'=> [
					'asc' => ['establecimiento.nombre' => SORT_ASC],
					'desc' => ['establecimiento.nombre' => SORT_DESC],
				],
				'id_servicio'=> [
					'asc' => ['p_servicio.nombre' => SORT_ASC],
					'desc' => ['p_servicio.nombre' => SORT_DESC],
				],
				'id_profesional'=> [
					'asc' => ['profesional.apellido' => SORT_ASC],
					'desc' => ['profesional.apellido' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
           //  return $dataProvider;
        }

        $query->andFilterWhere([
            'turno.id' => $this->id,
            'turno.numero' => $this->numero,
            'turno.edad'=>$this->edad,
        ]);

        
        //agregar si se quiere filtrar todo completo de todos los meses
        if($this->id_area!=null || $this->fecha_turno!=null || $this->id_unidad_edad!=null || $this->ubicacion_hc!=null ||
        $this->control_embarazo!=null || $this->nino_sano!=null || $this->primera_vez!=null || $this->presente!=null ||
        $this->observacion!=null || $this->id_diagnostico!=null || $this->id_area!=null || $this->id_servicio!=null ||
        $this->id_profesional!=null || $this->id_paciente!=null || $this->pacientenombre!=null || $this->pacienteapellido!=null 
        || $this->diagnosticocodigo!=null || $this->profesionalnombre!=null || $this->id_especialidad!=null
        || $this->pacientehistoria!=null || $this->pacientesexo!=null || $this->pacienteobrasocial!=null || $this->establecimientolocalidad!=null ){
         
            $query
              //mysql
               //->andFilterWhere(['like', 'date_format(turno.fecha_turno,\'%d/%m/%Y\')',$this->fecha_turno])
              //postgres
              
              ->andFilterWhere(['like', 'to_char(turno.fecha_turno,\'DD/MM/YYYY\')',$this->fecha_turno])
              ->andFilterWhere(['like', 'lower(unidad_edad.descripcion)',strtolower($this->id_unidad_edad)])
              ->andFilterWhere(['like', 'lower(turno.ubicacion_hc)',strtolower($this->ubicacion_hc)])
              ->andFilterWhere(['turno.control_embarazo'=>$this->control_embarazo])
              ->andFilterWhere(['turno.nino_sano'=>$this->nino_sano])
              ->andFilterWhere(['turno.primera_vez'=>$this->primera_vez])
              ->andFilterWhere(['turno.presente'=>$this->presente])
              ->andFilterWhere(['like', 'lower(CAST(turno.observacion AS varchar))',strtolower($this->observacion)])
              ->andFilterWhere(['like', 'lower(diagnostico.nombre)',strtolower($this->id_diagnostico)])
              ->andFilterWhere(['like', 'lower(establecimiento.nombre)',strtolower($this->id_area)])
              ->andFilterWhere(['like', 'lower(p_servicio.nombre)',strtolower($this->id_servicio)])
              ->andFilterWhere(['like', 'lower(profesional.apellido)',strtolower($this->id_profesional)])
              ->andFilterWhere(['like','CAST(paciente.numero_documento AS varchar)',strtolower($this->id_paciente)])
              ->andFilterWhere(['like', 'lower(paciente.nombre)',strtolower($this->pacientenombre)])
              ->andFilterWhere(['like', 'lower(paciente.apellido)',strtolower($this->pacienteapellido)])
              ->andFilterWhere(['like', 'lower(sexo.nombre)',strtolower($this->pacientesexo)])
              ->andFilterWhere(['like','lower(CAST(diagnostico.codigo AS varchar))',strtolower($this->diagnosticocodigo)])
              ->andFilterWhere(['like','lower(especialidad.nombre)',strtolower($this->id_especialidad)])
              ->andFilterWhere(['like', 'lower(profesional.nombre)',strtolower($this->profesionalnombre)])
              ->andFilterWhere(['like', 'lower(historia_clinica.historia_clinica)',strtolower($this->pacientehistoria)])
              ->andFilterWhere(['like', 'lower(obra_social.nombre)',strtolower($this->pacienteobrasocial)])
              ->andFilterWhere(['like', 'lower(localidad.nombre)',strtolower($this->establecimientolocalidad)]);
               
        }
        else{
            $query->andFilterWhere(['between', 'turno.fecha_turno',trim($fechaInicio),trim($fechaFin)]);
       }
        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('turno-dataprovider',$dataProvider);

        return $dataProvider;
    }
    public function reporteRangoEdad($params){
        $sql="select  p2.codigo as codprov,e.codigo_sisa,extract(YEAR FROM t.fecha_turno) as anioconsul,extract(month FROM t.fecha_turno) as mesconsul,ps.codigo as coduoperat,  CASE
    WHEN  p.id_sexo=1 and count(p.id_sexo)>0 THEN count(p.id_sexo)
    else
        0
  END AS masculino,
  CASE
    WHEN  p.id_sexo=2 and count(p.id_sexo)>0 THEN count(p.id_sexo) 
    else 
      0
  END AS femenino,
   CASE
    WHEN u.tipo_unidad = 2 or u.tipo_unidad= 3 or u.tipo_unidad=4 or u.tipo_unidad=5  THEN '<1'
    WHEN t.edad >= 1 and t.edad <= 4 and u.tipo_unidad= 1 THEN '1-4'
    WHEN  t.edad >= 5 and  t.edad <= 9 and u.tipo_unidad= 1 THEN '5-9'
    WHEN   t.edad >= 10 and  t.edad <= 14 and u.tipo_unidad=1 THEN '10-14'
    WHEN   t.edad  >= 15 and  t.edad <= 19 and u.tipo_unidad=1  THEN '15-19'
    WHEN t.edad  >= 20 and  t.edad <=24 and u.tipo_unidad=1   THEN '20-24'
    WHEN  t.edad  >= 25 and  t.edad <=34 and u.tipo_unidad=1  THEN '25-34'
    WHEN  t.edad  >= 35 and  t.edad <=44 and u.tipo_unidad=1  THEN '35-44'
    WHEN  t.edad  >= 45 and  t.edad <=49 and u.tipo_unidad=1  THEN '45-49'
    WHEN  t.edad  >= 50 and  t.edad <=64 and u.tipo_unidad=1  then '50-64'
    WHEN  t.edad  >= 65 and  t.edad <=74 and u.tipo_unidad=1  then '65-74'
    WHEN  t.edad  >= 75 and u.tipo_unidad=1  then '>75'
    else
        'S/E'
  END AS rango_edad
from consultasestadisticas.turno t join consultasestadisticas.paciente p on (p.id=t.id_paciente) join consultasestadisticas.establecimiento e on(e.id=t.id_area) join consultasestadisticas.p_servicio ps  on(ps.id=t.id_servicio) join consultasestadisticas.localidad
l on(l.id=e.id_localidad) join consultasestadisticas.provincia p2 on(p2.id=l.id_provincia) join consultasestadisticas.unidad_edad u on(u.id=t.id_unidad_edad)  

group by(p2.codigo,e.codigo_sisa,anioconsul,mesconsul,coduoperat,t.edad,u.tipo_unidad,p.id_sexo)
";
    $dataProvider = new SqlDataProvider([
        'sql' => $sql,
    ]);
        return $dataProvider;
    }
    public function exportExcel($params,$q=null)
    {   
       
        $query = Turno::find();
       

		$query->joinWith(['diagnostico']);
		$query->joinWith(['servicio']);
		$query->joinWith(['paciente']);
        $query->joinWith(['paciente.sexo']);
        $query->joinWith(['paciente.obrasocial']);
        $query->joinWith(['especialidad']);
		$query->joinWith(['profesional']);
		$query->joinWith(['unidadedad']);
		$query->joinWith(['establecimiento']);
        $query->joinWith(['establecimiento.localidad']);
        $query->joinWith(['establecimiento.localidad.provincia']);
        $query->joinWith(['paciente.historiaclinicas']);
        
        //$query
        
        if((Yii::$app->user->identity->username!='administrador' && $q!='CENTRAL') && ($q!='' || $q!=null)){
        $query->joinWith('establecimiento.localidad.usuarioEstadistica')->where(['usuario_estadistica.id_usuario'=>Yii::$app->user->identity->id])->all();
        }
            
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
         
        ]);
        
        $this->load($params);
    
        
        $dataProvider->setSort([
            'attributes' => [
				'id_paciente'=> [
					'asc' => ['paciente.numero_documento' => SORT_ASC],
					'desc' => ['paciente.numero_documento' => SORT_DESC],
				],
                'id_especialidad'=> [
					'asc' => ['especialidad.nombre' => SORT_ASC],
					'desc' => ['especialidad.nombre' => SORT_DESC],
				],
                'pacientenombre'=> [
					'asc' => ['paciente.nombre' => SORT_ASC],
					'desc' => ['paciente.nombre' => SORT_DESC],
				],
                'pacienteapellido'=> [
					'asc' => ['paciente.apellido' => SORT_ASC],
					'desc' => ['paciente.apellido' => SORT_DESC],
				],
                'pacientesexo'=> [
					'asc' => ['sexo.nombre' => SORT_ASC],
					'desc' => ['sexo.nombre' => SORT_DESC],
				],
                
                'profesionalnombre'=> [
					'asc' => ['profesional.nombre' => SORT_ASC],
					'desc' => ['profesional.nombre' => SORT_DESC],
				],
                'pacienteobrasocial'=> [
					'asc' => ['paciente.obrasocial.nombre' => SORT_ASC],
					'desc' => ['paciente.obrasocial.nombre' => SORT_DESC],
				],
                'establecimientolocalidad'=> [
					'asc' => ['establecimiento.localidad.nombre' => SORT_ASC],
					'desc' => ['establecimiento.localidad.nombre' => SORT_DESC],
				],
				// mysql
				// 'fecha_turno'=> [
					// 'asc' => ['date_format(turno.fecha_turno, "%Y%m%d")' => SORT_ASC],
					// 'desc' => ['date_format(turno.fecha_turno,"%Y%m%d")' => SORT_DESC],
				// ],
				// postgres
				'fecha_turno',
				'id_unidad_edad'=> [
					'asc' => ['unidad_edad.descripcion' => SORT_ASC],
					'desc' => ['unidad_edad.descripcion' => SORT_DESC],
				],
				'ubicacion_hc',
				'id',
                'edad',
				'numero',
				'control_embarazo',
				'nino_sano',
				'primera_vez',
				'presente',
				'observacion',
                'diagnosticocodigo'=> [
					'asc' => ['diagnostico.codigo' => SORT_ASC],
					'desc' => ['diagnostico.codigo' => SORT_DESC],
				],
				'id_diagnostico'=> [
					'asc' => ['diagnostico.nombre' => SORT_ASC],
					'desc' => ['diagnostico.nombre' => SORT_DESC],
                    
				],
				'id_area'=> [
					'asc' => ['establecimiento.nombre' => SORT_ASC],
					'desc' => ['establecimiento.nombre' => SORT_DESC],
				],
				'id_servicio'=> [
					'asc' => ['p_servicio.nombre' => SORT_ASC],
					'desc' => ['p_servicio.nombre' => SORT_DESC],
				],
				'id_profesional'=> [
					'asc' => ['profesional.apellido' => SORT_ASC],
					'desc' => ['profesional.apellido' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
           //  return $dataProvider;
        }

        $query->andFilterWhere([
            'turno.id' => $this->id,
            'turno.numero' => $this->numero,
            'turno.edad'=>$this->edad,
        ]);

        
        //agregar si se quiere filtrar todo completo de todos los meses
      
            $query
              //mysql
               //->andFilterWhere(['like', 'date_format(turno.fecha_turno,\'%d/%m/%Y\')',$this->fecha_turno])
              //postgres
              
              ->andFilterWhere(['like', 'to_char(turno.fecha_turno,\'DD/MM/YYYY\')',$this->fecha_turno])
              ->andFilterWhere(['like', 'lower(unidad_edad.descripcion)',strtolower($this->id_unidad_edad)])
              ->andFilterWhere(['like', 'lower(turno.ubicacion_hc)',strtolower($this->ubicacion_hc)])
              ->andFilterWhere(['like', 'lower(historia_clinica.historia_clinica)',strtolower($this->pacientehistoria)])
              ->andFilterWhere(['turno.control_embarazo'=>$this->control_embarazo])
              ->andFilterWhere(['turno.nino_sano'=>$this->nino_sano])
              ->andFilterWhere(['turno.primera_vez'=>$this->primera_vez])
              ->andFilterWhere(['turno.presente'=>$this->presente])
              ->andFilterWhere(['like', 'lower(CAST(turno.observacion AS varchar))',strtolower($this->observacion)])
              ->andFilterWhere(['like', 'lower(diagnostico.nombre)',strtolower($this->id_diagnostico)])
              ->andFilterWhere(['like', 'lower(establecimiento.nombre)',strtolower($this->id_area)])
              ->andFilterWhere(['like', 'lower(p_servicio.nombre)',strtolower($this->id_servicio)])
              ->andFilterWhere(['like', 'lower(profesional.apellido)',strtolower($this->id_profesional)])
              ->andFilterWhere(['like','CAST(paciente.numero_documento AS varchar)',strtolower($this->id_paciente)])
              ->andFilterWhere(['like','lower(especialidad.nombre)',strtolower($this->id_especialidad)])
              ->andFilterWhere(['like','lower(sexo.nombre)',strtolower($this->pacientesexo)])
              ->andFilterWhere([
                'or',
                ['like', 'lower(paciente.nombre)',strtolower($this->pacientenombre)],
                ['like', 'lower(paciente.apellido)',strtolower($this->pacienteapellido)],
                ['like','lower(CAST(diagnostico.codigo AS varchar))',strtolower($this->diagnosticocodigo)],
                ['like', 'lower(profesional.nombre)',strtolower($this->profesionalnombre)],
                ['like', 'lower(historia_clinica.historia_clinica)',strtolower($this->pacientehistoria)],
                ['like', 'lower(obra_social.nombre)',strtolower($this->pacienteobrasocial)],
                ['like', 'lower(localidad.nombre)',strtolower($this->establecimientolocalidad)]
            ]);
           
        
      
            
              
     
      

        return $dataProvider;
    }
}
