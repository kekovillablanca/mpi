<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DuplicadoSearch represents the model behind the search form about `app\models\Duplicadoaestro`.
 */
class DuplicadoSearch extends Duplicado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id_maestro'], 'integer'],
            [['estado','documento_tipo', 'documento_numero', 'primer_apellido', 'segundo_apellido', 'primer_nombre', 'segundo_nombre', 'fecha_nacimiento', 'sexo', 'numero_hc', 'id_area', 'id_federado_provincial', 'id_federado_nacional', 'id_duplicado', 'id_error', 'stamp_id_error', 'stamp_flag_federado', 'stamp_flag_modificado', 'stamp_flag_autenticado'], 'safe'],
            [['flag_federado', 'flag_modificado', 'flag_autenticado'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Duplicado::find();
		$query->joinWith(['dominio']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // para el search de estado
        $codigo_estado=null;
        if ($this->estado){
            $input = preg_quote(ucfirst($this->estado), '~');
            $estado=['0'=>'Activo','1'=>'Unificado','2'=>'Fallecido'];
            $auxiliar = preg_grep('~' . $input . '~', $estado);
            if ($auxiliar)
                $codigo_estado=array_key_first($auxiliar);
         
        }

        $dataProvider->setSort([
            'attributes' => [
				'id_maestro',
				'documento_tipo',
				'documento_numero' => [
                    'asc' => ["cast(NULLIF(regexp_replace(duplicado.documento_numero, '\D', '', 'g'), '') AS numeric)" => SORT_ASC],
                    'desc' => ["cast(NULLIF(regexp_replace(duplicado.documento_numero, '\D', '', 'g'), '') AS numeric)" => SORT_DESC],
                ],
				'primer_apellido',
				'segundo_apellido',
				'primer_nombre',
				'segundo_nombre',
				'fecha_nacimiento',
				'sexo',
				'numero_hc',
				'estado',
				'id_area'=> [
					'asc' => ['dominio.area' => SORT_ASC],
					'desc' => ['dominio.area' => SORT_DESC],
				],
				'id_federado_provincial',
				'id_federado_nacional'=> [
                    'asc' => ["cast(NULLIF(regexp_replace(duplicado.id_federado_nacional, '\D', '', 'g'), '') AS numeric)" => SORT_ASC],
                    'desc' => ["cast(NULLIF(regexp_replace(duplicado.id_federado_nacional, '\D', '', 'g'), '') AS numeric)" => SORT_DESC],
                ],
				'id_duplicado',
				'id_error',
				'flag_federado',
				'flag_modificado',
				'flag_autenticado',
				'stamp_id_error',
				'stamp_flag_federado',
				'stamp_flag_modificado',
				'stamp_flag_autenticado',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'duplicado.id_maestro' => $this->id_maestro,
            'duplicado.id_federado_provincial' => $this->id_federado_provincial,
            'duplicado.id_duplicado' => $this->id_duplicado,
            'duplicado.id_error' => $this->id_error,
            'duplicado.stamp_id_error' => $this->stamp_id_error,
            'duplicado.stamp_flag_federado' => $this->stamp_flag_federado,
            'duplicado.stamp_flag_modificado' => $this->stamp_flag_modificado,
            'duplicado.stamp_flag_autenticado' => $this->stamp_flag_autenticado,
        ]);

        $query->andFilterWhere(['like', 'lower(duplicado.documento_tipo)',strtolower($this->documento_tipo)])
              ->andFilterWhere(['like', 'lower(duplicado.documento_numero)',strtolower($this->documento_numero)])
              ->andFilterWhere(['like', 'lower(duplicado.primer_apellido)',strtolower($this->primer_apellido)])
              ->andFilterWhere(['like', 'lower(duplicado.segundo_apellido)',strtolower($this->segundo_apellido)])
              ->andFilterWhere(['like', 'lower(duplicado.primer_nombre)',strtolower($this->primer_nombre)])
              ->andFilterWhere(['like', 'lower(duplicado.segundo_nombre)',strtolower($this->segundo_nombre)])
              ->andFilterWhere(['like', 'to_char(duplicado.fecha_nacimiento,\'DD/MM/YYYY\')',$this->fecha_nacimiento])
              ->andFilterWhere(['like', 'lower(duplicado.sexo)',strtolower($this->sexo)])
              ->andFilterWhere(['like', 'lower(duplicado.numero_hc)',strtolower($this->numero_hc)])
              ->andFilterWhere(['like', 'lower(dominio.area)',strtolower($this->id_area)])
              ->andFilterWhere(['like', 'lower(duplicado.id_federado_nacional)',strtolower($this->id_federado_nacional)])
              ->andFilterWhere(['duplicado.flag_federado'=>$this->flag_federado])
              ->andFilterWhere(['duplicado.flag_modificado'=>$this->flag_modificado])
              ->andFilterWhere(['duplicado.flag_autenticado'=>$this->flag_autenticado]);


        // para el search de estado
        if ($codigo_estado===0){
            $query->andWhere(['IS','duplicado.estado', null ]);
        }else{
            $query->andFilterWhere(['duplicado.estado'=>$codigo_estado]);
        }

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('duplicado-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
