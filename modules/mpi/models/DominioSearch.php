<?php

namespace app\modules\mpi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DominioSearch represents the model behind the search form about `app\models\Dominio`.
 */
class DominioSearch extends Dominio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id'], 'integer'],
            [['area', 'base', 'dominio'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dominio::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'area',
				'base',
				'dominio',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'dominio.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(dominio.area)',strtolower($this->area)])
              ->andFilterWhere(['like', 'lower(dominio.base)',strtolower($this->base)])
              ->andFilterWhere(['like', 'lower(dominio.dominio)',strtolower($this->dominio)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('dominio-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
