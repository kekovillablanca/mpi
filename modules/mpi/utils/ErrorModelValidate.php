<?php
namespace app\modules\mpi\utils;





class  ErrorModelValidate
{
    public static function convertError($arrayError) 
    {

$filteredErrors = array_filter($arrayError, function($error) {
    return !empty($error);
});

// Concatenamos los errores en un solo string
$errorMessages = '';
foreach ($filteredErrors as $errorArray) {
    foreach ($errorArray as $errorField => $messages) {
        $errorMessages .= implode(', ', $messages) . "\n"; // Une los mensajes de error
    }
}

// Imprimimos los mensajes concatenados
return $errorMessages;
    }
}
?>