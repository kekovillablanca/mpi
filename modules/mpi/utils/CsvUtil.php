<?php
namespace app\modules\mpi\utils;

use Yii;
use yii\web\Response;



class CsvUtil
{
    public static function send_file($path, $name = null)
    {
        // Verifica si el archivo existe y es legible
        if (!is_file($path) || !is_readable($path)) {
            return false;
        }
    
        // Define el nombre del archivo si no se proporciona
        if (is_null($name)) {
            $name = basename($path);
        }
    
        // Limpia el buffer de salida si es necesario
        if (ob_get_length()) {
            ob_end_clean();
        }
    
        // Configura los encabezados para la descarga y cache
        Yii::$app->response->headers->set('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0');
        Yii::$app->response->headers->set('Pragma', 'no-cache');
    
        // Establece el nombre del archivo en los encabezados
        Yii::$app->response->headers->set('Content-Disposition', 'attachment; filename="' . $name . '"');
    
        // Envía el archivo usando xSendFile
        Yii::$app->response->setDownloadHeaders($name, 'text/csv', true, filesize($path));

        // Enviar el archivo como respuesta

        return Yii::$app->response->sendFile($path);


    }

    public static function Array2CSVDownload($array, $filename = "export.csv", $delimiter=";") {

        // force object to be array, sorry i was working with object items
        $keys = array_keys( (array) $array[0] );
        // use keys as column titles
        $data = [];
        array_push($data, implode($delimiter, $keys));
        // working with items
        foreach ($array as $item) {
            $values = array_values((array) $item);
            array_push($data, implode($delimiter, $values)); 
        }
        // flush buffer
        ob_flush();
        // mixing items
        $csvData = join("\n", $data);
        //setup headers to download the file
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        //setup utf8 encoding
        header('Content-Type: application/csv; charset=UTF-8');
        // showing the results
        die($csvData);
    
    }   
    
    
    
}
?>