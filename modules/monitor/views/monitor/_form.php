<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\monitor\models\Monitor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'host')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'service')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'categoria')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'ip')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'ping')->checkbox();?>

    <?php echo $form->field($model, 'puerto')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'tipo_puerto')->radioList( [0=>'TCP', 1 => 'UDP'] ); ?>

    <?php echo $form->field($model, 'latencia')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'email_alerta')->textInput();        // ->textarea()?>

    <?php // echo $form->field($model, 'estado')->textInput();        // ->textarea()?>

    <?php // echo $form->field($model, 'color')->textInput();        // ->textarea()?>

    <?php // echo $form->field($model, 'time1')->textInput();        // ->textarea()?>

    <?php // echo $form->field($model, 'time2')->textInput();        // ->textarea()?>

    <?php ActiveForm::end(); ?>
    
</div>
