<?php
/* Modelo generado por Model(Q) */
namespace app\modules\monitor\models;

use Yii;
//
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "monitor".
 *
 * @property int $id
 * @property string $host
 * @property string $service
 * @property string $categoria
 * @property string $ip
 * @property bool $ping
 * @property int $puerto
 * @property int $tipo_puerto
 * @property int $latencia
 * @property string $email_alerta
 * @property int $estado
 * @property string $color
 * @property string $time1
 * @property string $time2
 */
class Monitor extends \yii\db\ActiveRecord
{
	public $tiempo;
	public $tipo_puerto_nombre;

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monitor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['host', 'service', 'categoria', 'ip', 'email_alerta', 'color'], 'string'],
            [['ping'], 'boolean'],
            [['puerto', 'tipo_puerto', 'latencia', 'estado'], 'default', 'value' => null],
            [['puerto', 'tipo_puerto', 'latencia', 'estado'], 'integer'],
            [['time1', 'time2'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => 'Nombre Servidor',
            'service' => 'Nombre Servicio',
            'categoria' => 'Categoria',
            'ip' => 'Ip',
            'ping' => 'Ping',
            'puerto' => 'Puerto',
//            'tipo_puerto' => 'Tipo Puerto',
			'tipo_puerto_nombre' => 'Tipo Puerto',
			'latencia' => 'Latencia',
            'email_alerta' => 'Email Alerta',
            'estado' => 'Estado',
            'color' => 'Color',
            'time1' => 'Time1',
            'time2' => 'Time2',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('monitor.id', 10),
			'host' => array('monitor.host', 10),
			'service' => array('monitor.service', 10),
			'categoria' => array('monitor.categoria', 10),
			'ip' => array('monitor.ip', 10),
			// postgres
			'ping' => array('CASE WHEN monitor.ping IS true THEN \'SI\' ELSE \'NO\' END', 10),
			'puerto' => array('monitor.puerto', 10),
			'tipo_puerto' => array('monitor.tipo_puerto', 12),
			'latencia' => array('monitor.latencia', 10),
			'email_alerta' => array('monitor.email_alerta', 13),
			'estado' => array('monitor.estado', 10),
			'color' => array('monitor.color', 10),
			'time1' => array('monitor.time1', 10),
			'time2' => array('monitor.time2', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'host',
			'service',
			'categoria',
			'ip',
			'ping:boolean',
			'puerto',
//			'tipo_puerto',
			'tipo_puerto_nombre',
			'latencia',
			'email_alerta',
			'estado',
			'color',
			'time1',
			'time2',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'host',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'service',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'categoria',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'ip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'ping',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'puerto',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'tipo_puerto',
				'value'=>'tipo_puerto_nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'latencia',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'email_alerta',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'estado',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'color',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'time1',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'time2',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $datetime1 = date_create($this->time1);
        $datetime2 = date_create($this->time2);
        $diferencia=date_diff($datetime2, $datetime1);
        $this->tiempo=$diferencia->format('%dd %hh');
		if($this->tipo_puerto==1){
			$this->tipo_puerto_nombre='UDP';
		}else{
			$this->tipo_puerto_nombre='TCP';
		}
		if (empty($this->puerto)){
			$this->puerto='ping';
			$this->tipo_puerto_nombre='ICMP';
		}
	}

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
