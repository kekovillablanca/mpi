<?php

namespace app\modules\monitor\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MonitorSearch represents the model behind the search form about `app\modules\monitor\models\Monitor`.
 */
class MonitorSearch extends Monitor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'host', 'service', 'categoria', 'ip', 'puerto', 
            //'tipo_puerto',
             'latencia', 'email_alerta', 'estado', 'color', 'time1', 'time2'], 'safe'],
            [['ping'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Monitor::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'host',
				'service',
				'categoria',
				'ip',
				'ping',
				'puerto',
				'tipo_puerto',
				'latencia',
				'email_alerta',
				'estado',
				'color',
				'time1',
				'time2',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'monitor.id' => $this->id,
            'monitor.puerto' => $this->puerto,
//            'monitor.tipo_puerto' => $this->tipo_puerto,
            'monitor.latencia' => $this->latencia,
            'monitor.estado' => $this->estado,
            'monitor.time1' => $this->time1,
            'monitor.time2' => $this->time2,
        ]);

        $query->andFilterWhere(['like', 'lower(monitor.host)',strtolower($this->host)])
              ->andFilterWhere(['like', 'lower(monitor.service)',strtolower($this->service)])
              ->andFilterWhere(['like', 'lower(monitor.categoria)',strtolower($this->categoria)])
              ->andFilterWhere(['like', 'lower(monitor.ip)',strtolower($this->ip)])
              ->andFilterWhere(['monitor.ping'=>$this->ping])
              ->andFilterWhere(['like', 'lower(monitor.email_alerta)',strtolower($this->email_alerta)])
              ->andFilterWhere(['like', 'lower(monitor.color)',strtolower($this->color)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('monitor-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
