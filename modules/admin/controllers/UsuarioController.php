<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
//
use app\modules\admin\models\Usuario;
use app\modules\admin\models\UsuarioSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\models\ChangePassword;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;

// Modelos que se usan en el Detalle de Grupo
use app\modules\admin\models\UsuarioGrupo;
use app\modules\admin\models\UsuarioGrupoSearch;
use app\modules\admin\models\Grupo;
use app\modules\admin\models\GrupoSearch;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'export', 'select', 'indexdetalle', 'viewdetalle', 'adddetalle', 'deletedetalle','misdatos'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'export', 'select','indexdetalle', 'viewdetalle', 'adddetalle', 'deletedetalle','misdatos'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {    
       
        $model= new Usuario();
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $columnas=Metodos::obtenerColumnas($model,'admin/usuario/index');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    }


    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView($model);

        if(isset($_GET['pdf'])){

            $titulo='Datos de Usuario';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Usuario #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $this->findModel($id),
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Usuario model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Usuario();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
                $model->actualizarFecha=true;
            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Usuario #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Crear Usuario",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing Usuario model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
       
        $model = $this->findModel($id);       
        $password=$model->password;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->actualizarFecha=true;
            if($model->load($request->post())){
       
                try {

                    // cambio de contraseña
                    if ($model->password!=md5($password)){
                    }
                    else {
                        Yii::$app->response->format = Response::FORMAT_HTML;
                        throw new NotFoundHttpException('El password esta mal ya se utilizo antes.');
                    }
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Usuario #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Editar Usuario #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing Usuario model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($this->findModel($id)->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error en la base de datos.',500);
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null)
    {

        $request = Yii::$app->request;
        $reporte= new Reporte();

        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();

        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            $dataProvider=Yii::$app->session->get('usuario-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new Usuario();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionMisdatos()
    {

        $request = Yii::$app->request;

        $id=Yii::$app->user->identity->getId();
        $model = $this->findModel($id);
        $changePassword=new ChangePassword();
        if($request->isAjax) {    // modal para cambiar contraseña

            Yii::$app->response->format = Response::FORMAT_JSON;

            if($dato=$request->post()){
                $password=$dato['Usuario']['password'];
                $changePassword->pass_new=$dato['ChangePassword']['pass_new'];
                $changePassword->pass_new_check=$dato['ChangePassword']['pass_new_check'];

                if ($changePassword->pass_new<>$changePassword->pass_new_check){

                    $model->addError('pass_new', 'La contraseña ingresada no coincide.');
                    $model->addError('pass_new_check', 'La contraseña ingresada no coincide.');

                }else{

                    if (md5($password)<>$model->password) {

                        $model->addError('pass_ctrl', 'La contraseña ingresada no es correcta.');

                    }else {

                        try {
                            // cambiar solo contraseña
                            $model->password = $changePassword->pass_new;
                            if ($model->save()) {
                                $content = '<span class="text-success">Contraseña cambiada correctamente</span>';
                                return [
                                    'title' => "Cambiar Contraseña",
                                    'content' => $content,
                                ];
                            }
                        } catch (yii\db\Exception $e) {
                            Yii::$app->response->format = Response::FORMAT_HTML;
                            throw new NotFoundHttpException('Error en la base de datos.', 500);
                        }
                    }
                }
            }

            return [
                'title'=> "Cambiar Contraseña",
                'content'=>$this->renderAjax('@app/modules/admin/views/admin/usuario/_password', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        } else {
            if ($model->load($request->post())) {
                try {

                    if ($model->save()) {
                        Yii::$app->session->setFlash('misDatosSubmitted');
                        return $this->refresh();
                    }

                } catch (yii\db\Exception $e) {

                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.', 500);
                }
            }

            return $this->render('misdatos', [
                'model' => $model,
                'change' => $changePassword,
            ]);
        }

    }
    
    // maestro - detalle actions

    public function actionIndexdetalle($id_maestro=null) 
    { 

        $request = Yii::$app->request; 
  
        if($request->isAjax){ 

            $permisos=[ 'indexdetalle' => Seguridad::tienePermiso('indexdetalle'), 
                        'viewdetalle' => Seguridad::tienePermiso('viewdetalle'), 
//                        'updatedetalle' => Seguridad::tienePermiso('updateetalle'), 
                        'deletedetalle' => Seguridad::tienePermiso('deletedetalle'), 
                        'adddetalle' => Seguridad::tienePermiso('adddetalle'), 
//                        'exportdetalle' => Seguridad::tienePermiso('exportdetalle'), 
            ];

            if ( isset($id_maestro) || isset($_POST['expandRowKey']) ) { 

                if (!isset($id_maestro))
                    $id_maestro=$_POST['expandRowKey'];

                $searchModel = new UsuarioGrupoSearch(); 
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->where(['id_usuario' => $id_maestro]); 
                $dataProvider->setPagination(false); 

                $columnas= [
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'id_grupo',
                            'value'=>'id_grupo',
                            'width'=>'50px',
                            'header'=>  '<div id="crud-detail-id_grupo-'.$id_maestro.'"  
                                         style="cursor:pointer;color:#337ab7;" 
                                         data-sort="id_grupo" 
                                         onclick="sortDetalle('.$id_maestro.',this)">
                                         ID
                                         </div>', 
                                         // soporte sort, debe estar el atributo en el Search
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'id_grupo_nom',
                            'value'=>'grupo.nombre',
                            'header'=>  '<div id="crud-detail-id_grupo_nom-'.$id_maestro.'" 
                                         style="cursor:pointer;color:#337ab7;" 
                                         data-sort="id_grupo_nom" 
                                         onclick="sortDetalle('.$id_maestro.',this)">
                                         Grupo
                                         </div>',
                                         // soporte sort, debe estar el atributo en el Search
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'id_grupo_obs',
                            'value'=>'grupo.obs',
                            'header'=>  '<div style="color:#337ab7;">Observaciones</div>',  // sin soporte sort
                        ],
                ];

                if ($dataProvider) {
                        return $this->renderPartial('indexdetalle', [ 
                        'id_maestro' => $id_maestro, 
                        'searchModel' => $searchModel, 
                        'dataProvider' => $dataProvider,
                        'permisos' => $permisos,
                        'columns' => $columnas,
                    ]); 
                }else{
                    return '<div>No se encontraron resultados</div>'; 
                }
            }

        }else{ 
            // Process for non-ajax request 
            return $this->redirect(['index']); 
        } 

    } 

    public function actionViewdetalle($id_detalle,$id_maestro) 
    { 
  
        $request = Yii::$app->request;

        $modelGrupo = Grupo::findOne($id_detalle); 
        $attributes = $modelGrupo->attributeView(); 
  
        if (isset($_GET['pdf'])) { 
  
            $titulo = 'Datos de Grupo'; 
  
            \app\components\Viewpdf\Viewpdf::widget(array( 
                'titulo' => $titulo, 
                'subtitulo' => 'Grupo #' . $id_detalle, 
                'data' => $modelGrupo, 
                'attributes' => $attributes, 
                'resumen' => '', 
            )); 
  
            $fileName = preg_replace("/ /", "_", $titulo) . ".pdf"; 
            Yii::$app->response->sendFile('../runtime/' . $fileName)->send(); 
            Yii::$app->end();
        } 
      
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [ 
                'title' => "Grupo #" . $id_detalle, 
                'content' => $this->renderAjax('@app/components/Vistas/_view', [ 
                    'model' => $modelGrupo, 
                    'attributes' => $attributes, 
                ]), 
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) . 
                (Seguridad::tienePermiso('viewdetalle')?Html::a('Imprimir',['viewdetalle','id_detalle'=>$id_detalle,'id_maestro'=>$id_maestro,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                '<iframe src="" style="display:none" name="exportFrame"/>' 
            ]; 
    
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    } 


    public function actionAdddetalle($id_maestro=null) 
    { 
  
        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON; 

            // Verifico si es el POST de adddetalle con la seleccion (guardar)
            if (isset($_POST['keylist']) AND isset($_POST['id_maestro'])) { 
    
                $error = false; 
                $id_maestro = $_POST['id_maestro']; 
                
                foreach ($_POST['keylist'] as $value) { 
    
                    $modelUsuarioGrupo = new UsuarioGrupo();

                    $modelUsuarioGrupo->id_usuario = $id_maestro; 
                    $modelUsuarioGrupo->id_grupo = $value; 
    
                    if (!$modelUsuarioGrupo->save()) { 
                        // en el caso en que quiera controlar este save, descomento esto
                        // pero hay que trabajar transacciones con el try /catch
                        // $error = true; 
                        // break; 

                        // en este caso, puedo tener multiples ingresos de detalle
                        // por lo tanto no controlo esto, si el detalle ya pertenece a ese
                        // maestro el model->save() , no lo agrega y no pasa nada.
                    } 

                } 

                if ($error) { 
                    return [
                        'status'=>'error',
                        'title'=> '<p style="color:red">ERROR</p>',
                        'content'=>'<div style="font-size: 18px">Error en la acción realizada</div><div style="font-size: 14px">Error de base de datos.</div>',
                        'footer'=>''];
                } 
                return ['status' => 'success'];   
            }

            $searchModel = new GrupoSearch(); 
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
                  
            $columnas = [
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'id',
                    'width' => '30px',
                ],    
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'nombre'
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'obs'
                ],
            ];    

            $permisos=[ 'adddetalle' => Seguridad::tienePermiso('adddetalle'), 
//                        'createdetalle' => Seguridad::tienePermiso('createdetalle'), 
            ];
    
            if ($id_maestro){
                if (isset($_GET['_pjax'])) { 
                    // Respuesta para el GET de filter y sort 
                    return $this->renderAjax('_adddetalle', [ 
                        'searchModel' => $searchModel, 
                        'dataProvider' => $dataProvider, 
                        'columns' => $columnas, 
                        'id_maestro' => $id_maestro,
                        'permisos' => $permisos,

                    ]); 
        
                } else { 
        
                    // Respuesta para el form   
                    return [ 
                        'title' => '', 
                        'content' => $this->renderAjax('_adddetalle', [ 
                            'searchModel' => $searchModel, 
                            'dataProvider' => $dataProvider, 
                            'columns' => $columnas, 
                            'id_maestro' => $id_maestro, 
                            'permisos' => $permisos,
                        ])    
                    ]; 
                } 
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }   

    public function actionDeletedetalle($id_detalle, $id_maestro) 
    { 

        $request = Yii::$app->request;
        $modelUsuarioGrupo = UsuarioGrupo::findOne(['id_grupo'=>$id_detalle,'id_usuario'=>$id_maestro]);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
    
            try { 
                if ($modelUsuarioGrupo->delete()){ 
                    // borro registro en este caso por que es una relacion NN 
                    return ['forceClose' => true, 'success' => 'reloadDetalle('.$id_maestro.')'];   
                } 
            } catch (yii\db\Exception $e) { 
                return ['forceClose' => false, 
                    'title' => '<p style="color:red">ERROR</p>', 
                    'content' => '<div style="font-size: 18px">Error en la acción realizada</div><div style="font-size: 14px">Error de base de datos.</div>',
                    'success' => 'reloadDetalle('.$id_maestro.')']; 
            } 

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }    
    } 

}
