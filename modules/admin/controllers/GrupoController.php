<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
//
use app\modules\admin\models\Grupo;
use app\modules\admin\models\GrupoSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;

// Modelos que se usan en el Detalle de Grupo
use app\modules\admin\models\GrupoPermiso;
use app\modules\admin\models\GrupoPermisoSearch;
use app\modules\admin\models\Permiso;
use app\modules\admin\models\PermisoSearch;

/**
 * GrupoController implements the CRUD actions for Grupo model.
 */
class GrupoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete', 'export', 'select',
                 'indexdetalle', 'viewdetalle', 'adddetalle', 'deletedetalle','viewusuarios'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'export', 'select', 'indexdetalle', 'viewdetalle', 'adddetalle', 'deletedetalle','viewusuarios'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Grupo models.
     * @return mixed
     */
    public function actionIndex()
    {    
       
        $model= new Grupo();
        $searchModel = new GrupoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $columnas=Metodos::obtenerColumnas($model,'admin/grupo/index');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
                    'viewusuarios' => Seguridad::tienePermiso('viewusuarios'),
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
        ]);
    }


    /**
     * Displays a single Grupo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView($model);

        if(isset($_GET['pdf'])){

            $titulo='Datos de Grupo';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $request = Yii::$app->request;
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Grupo #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $this->findModel($id),
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Grupo model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Grupo();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Grupo #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Crear Grupo",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing Grupo model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Grupo #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Editar Grupo #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing Grupo model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($this->findModel($id)->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error en la base de datos.',500);
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Grupo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Grupo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

     protected function findModel($id)
    {
        if (($model = Grupo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null)
    {

        $request = Yii::$app->request;
        $reporte= new Reporte();

        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();

        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            $dataProvider=Yii::$app->session->get('grupo-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new Grupo();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionIndexdetalle($id_maestro=null) 
    { 

        $request = Yii::$app->request; 
  
        if($request->isAjax){ 

            $permisos=[ 'indexdetalle' => Seguridad::tienePermiso('indexdetalle'), 
                        'viewdetalle' => Seguridad::tienePermiso('viewdetalle'), 
//                        'updatedetalle' => Seguridad::tienePermiso('updateetalle'), 
                        'deletedetalle' => Seguridad::tienePermiso('deletedetalle'), 
                        'adddetalle' => Seguridad::tienePermiso('adddetalle'), 
//                        'exportdetalle' => Seguridad::tienePermiso('exportdetalle'), 
            ];

            if ( isset($id_maestro) || isset($_POST['expandRowKey']) ) { 

                if (!isset($id_maestro))
                    $id_maestro=$_POST['expandRowKey'];

                $searchModel = new GrupoPermisoSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->where(['id_grupo' => $id_maestro]); 
                $dataProvider->setPagination(false);     

                $columnas= [
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'id_permiso',
                            'value'=>'id_permiso',
                            'width'=>'50px',
                            'header'=>  '<div id="crud-detail-id_permiso-'.$id_maestro.'"  
                                         style="cursor:pointer;color:#337ab7;" 
                                         data-sort="id_permiso" 
                                         onclick="sortDetalle('.$id_maestro.',this)">
                                         ID
                                         </div>',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'id_permiso_nom',
                            'value'=>'permiso.nombre',
                            'header'=>  '<div id="crud-detail-id_permiso_nom-'.$id_maestro.'" 
                                         style="cursor:pointer;color:#337ab7;" 
                                         data-sort="id_permiso_nom" 
                                         onclick="sortDetalle('.$id_maestro.',this)">
                                         Permiso
                                         </div>',
                        ],
                        [
                            'class'=>'\kartik\grid\DataColumn',
                            'attribute'=>'id_permiso_obs',
                            'value'=>'permiso.obs',
                            'header'=>  '<div style="color:#337ab7;">Observaciones</div>',  // sin soporte sort
                        ],
                ];

                if ($dataProvider) {
                        return $this->renderPartial('indexdetalle', [ 
                        'id_maestro' => $id_maestro, 
                        'searchModel' => $searchModel, 
                        'dataProvider' => $dataProvider,
                        'permisos' => $permisos,
                        'columns' => $columnas,
                    ]); 
                }else{
                    return '<div>No se encontraron resultados</div>'; 
                }
            }

        }else{ 
            // Process for non-ajax request 
            return $this->redirect(['index']); 
        } 

    } 


    public function actionViewdetalle($id_detalle,$id_maestro)
    {

        $request = Yii::$app->request;

        $modelPermiso = Permiso::findOne($id_detalle);
        $attributes = $modelPermiso->attributeView($modelPermiso);

        if (isset($_GET['pdf'])) {

            $titulo = 'Datos de Permiso';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo' => $titulo,
                'subtitulo' => 'Registro #' . $id_detalle,
                'data' => $modelPermiso,
                'attributes' => $attributes,
                'resumen' => '',
            ));

            $request = Yii::$app->request;
            $fileName = preg_replace("/ /", "_", $titulo) . ".pdf";
            Yii::$app->response->sendFile('../runtime/' . $fileName)->send();

        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [ 
                'title' => "Permiso #" . $id_detalle, 
                'content' => $this->renderAjax('@app/components/Vistas/_view', [ 
                    'model' => $modelPermiso, 
                    'attributes' => $attributes, 
                ]), 
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) . 
                (Seguridad::tienePermiso('viewdetalle')?Html::a('Imprimir',['viewdetalle','id_detalle'=>$id_detalle,'id_maestro'=>$id_maestro,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                '<iframe src="" style="display:none" name="exportFrame"/>' 
            ]; 
    
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    public function actionAdddetalle($id_maestro=null) 
    { 
  
        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON; 

            // Verifico si es el POST de adddetalle con la seleccion (guardar)
            if (isset($_POST['keylist']) AND isset($_POST['id_maestro'])) { 
    
                $error = false; 
                $id_maestro = $_POST['id_maestro']; 
                
                foreach ($_POST['keylist'] as $value) { 
                    
                    $modelGrupoPermiso = new GrupoPermiso();
                    $modelGrupoPermiso->id_grupo = $id_maestro;
                    $modelGrupoPermiso->id_permiso = $value;
    
                    if (!$modelGrupoPermiso->save()) { 
                        // en el caso en que quiera controlar este save, descomento esto
                        // pero hay que trabajar transacciones con el try /catch
                        // $error = true; 
                        // break; 

                        // en este caso, puedo tener multiples ingresos de detalle
                        // por lo tanto no controlo esto, si el detalle ya pertenece a ese
                        // maestro el model->save() , no lo agrega y no pasa nada.
                    } 

                } 

                if ($error) { 
                    return [
                        'status'=>'error',
                        'title'=> '<p style="color:red">ERROR</p>',
                        'content'=>'<div style="font-size: 18px">Error en la acción realizada</div><div style="font-size: 14px">Error de base de datos.</div>',
                        'footer'=>''];
                } 
                return ['status' => 'success'];   
            }

            $searchModel = new PermisoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                      
            $columnas = [
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'id',
                    'width' => '30px',
                ],    
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'nombre'
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'obs'
                ],
            ];    

            $permisos=[ 'adddetalle' => Seguridad::tienePermiso('adddetalle'), 
//                        'createdetalle' => Seguridad::tienePermiso('createdetalle'), 
            ];
    
            if ($id_maestro){
                if (isset($_GET['_pjax'])) { 
                    // Respuesta para el GET de filter y sort 
                    return $this->renderAjax('_adddetalle', [ 
                        'searchModel' => $searchModel, 
                        'dataProvider' => $dataProvider, 
                        'columns' => $columnas, 
                        'id_maestro' => $id_maestro,
                        'permisos' => $permisos,

                    ]); 
        
                } else { 
        
                    // Respuesta para el form   
                    return [ 
                        'title' => '', 
                        'content' => $this->renderAjax('_adddetalle', [ 
                            'searchModel' => $searchModel, 
                            'dataProvider' => $dataProvider, 
                            'columns' => $columnas, 
                            'id_maestro' => $id_maestro, 
                            'permisos' => $permisos,
                        ])    
                    ]; 
                } 
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }   

    public function actionDeletedetalle($id_detalle, $id_maestro) 
    { 

        $request = Yii::$app->request;
        $modelGrupoPermiso = GrupoPermiso::findOne(['id_permiso'=>$id_detalle,'id_grupo'=>$id_maestro]);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
    
            try { 
                if ($modelGrupoPermiso->delete()){ 
                    // borro registro en este caso por que es una relacion NN 
                    return ['forceClose' => true, 'success' => 'reloadDetalle('.$id_maestro.')'];   
                } 
            } catch (yii\db\Exception $e) { 
                return ['forceClose' => false, 
                    'title' => '<p style="color:red">ERROR</p>', 
                    'content' => '<div style="font-size: 18px">Error en la acción realizada</div><div style="font-size: 14px">Error de base de datos.</div>',
                    'success' => 'reloadDetalle('.$id_maestro.')']; 
            } 

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }    
    } 
    
    public function actionViewusuarios($id)
    {
        $request = Yii::$app->request;
        $model=$this->findModel($id);

        $usuarios=$model->getUsuarios()->all();

        $content='<table class="table table-striped table-bordered">';

        foreach ($usuarios as $user){
            $content=$content.'<tr><th>'.$user->usuario.'</th><td>'.$user->nombre.'</td></tr>';
        }
        $content=$content.'</table>';

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Usuarios del Grupo #".$id,
                'content'=>$content,
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]),
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

}
