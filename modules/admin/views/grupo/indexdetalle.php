<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
?>
<link rel="stylesheet" type="text/css" href="<?= Yii::$app->request->baseUrl ?>/css/detail.css" />
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<?php
$stringToolbar="";
if ($permisos['adddetalle']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', Url::to(['adddetalle','id_maestro'=>$id_maestro]), 
            ['role' => 'modal-remote' ,'title' => 'Agregar registro','class' => 'btn btn-default',
            'onclick' => '$("#ajaxCrudModal .modal-dialog").css({"width":"1000px"});
                          $("#ajaxCrudModal").on("hidden.bs.modal", function () {
                              $("#ajaxCrudModal .modal-dialog").css({"width":"600px"});
                          });']);
}
/*if ($permisos['exportdetalle']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['exportdetalle','accion'=>$_GET['r'],'id_maestro'=>$id_maestro],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}*/
// agrego acciones a $columns
$columns[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
//    'template' => '{viewdetalle}&nbsp;{updatedetalle}&nbsp;{deletedetalle}',
    'template' => '{viewdetalle}&nbsp;{deletedetalle}',
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
/*        'updatedetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Modificar datos del registro',]);
            },*/
        'deletedetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                    ['role'=>'modal-remote','title'=>'Quitar registro',
                     'data-url'=> $url,
                     'data-confirm'=>false, 'data-method'=>false,
                     'data-request-method'=>'post',
                     'data-toggle'=>'tooltip',
                     'data-confirm-title'=>'Quitar Registro',
                     'data-confirm-message'=>'¿ Desea quitar el registro de este listado ?']);

            },
        ],
    'urlCreator' => function ($action, $searchModel) {
            $url=Url::to([$action,'id_detalle'=>$searchModel->id_permiso,'id_maestro'=>$searchModel->id_grupo]);
            return $url;
    },
    'visibleButtons'=>[
        'viewdetalle'=> $permisos['viewdetalle'],
//        'updatedetalle'=> $permisos['updatedetalle'],
        'deletedetalle'=> $permisos['deletedetalle']
    ],

];

?>

<div class="detalle-expand">

    <div style="font-size: 15px;"><b>Grupos</b></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id'=>'crud-detail',
        'layout' => '{items}',
        'pjax'=>true,
        'columns' => $columns,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
    ]);
        
    ?>
</div>

