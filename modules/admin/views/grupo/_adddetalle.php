<?php
use yii\helpers\Html;
use kartik\grid\GridView;
?>
<style> .detalle-seleccionado > td {background-color: #ccc !important;} 
        .glyphicon {color: #337ab7;}
</style>

<?php
$stringToolbar="";
/*if ($permisos['createdetalle']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>',
    ['createdetalle','id_maestro'=>$id_maestro],
        ['role'=>'modal-remote',
         'title'=> 'Crear nuevo Registro',
         'class' => 'btn btn-default',
         'onclick' => '$("#ajaxCrudModal .modal-dialog").css({"width":"600px"})'
         ]);
}
*/
if ($permisos['adddetalle']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-ok"></i>', '#',
        ['title' => 'Agregar registros seleccionados',
         'class' => 'btn btn-default',
         'onclick' => 'submitAddDetalle('.$id_maestro.')']);
}

// agrego acciones a $columns
$columns[]=
    ['class' => '\kartik\grid\CheckboxColumn',
    'header' => $stringToolbar,
    'rowSelectedClass' => 'detalle-seleccionado',
    ];
?>

<div class="permiso-index">
    <div id="ajaxCrudDatatableAddDetalle">
        <?=GridView::widget([
            'id'=>'crud-datatable-detalle',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'layout'=>"{items}",
            'columns' => $columns,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbar'=> [ ['content'=>'']],
            'panel' => [
                'type' => 'primary',
                'heading' => 'Agregar Permmisos',
            ]
        ])?>
    </div>
</div>
