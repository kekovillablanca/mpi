<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Grupo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'regla')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'obs')->textInput(['maxlength' => true]);?>

    <?php ActiveForm::end(); ?>
    
</div>
