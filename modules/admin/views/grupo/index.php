<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GrupoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grupos';
$this->params['breadcrumbs'][] = $this->title;
?>

<script>
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
    // mostrarDetalle se usa cuando hacemos un delete de detalle, desde la accion deletedetalle se
    // devuelve success => mostrarDetalle. para que se renderize nuevamente la ventana de detalle
    // tambien se usa cuando se agrega un detalle adddetalle

    function reloadDetalle(id_maestro){
        $.ajax({
            url: '<?php echo Url::to(['indexdetalle']) ?>',
            type:"POST",
            data:{
                expandRowKey: id_maestro,
            },
            success: function(detalle) {
                element = $("tr").find("div[data-key='" + id_maestro + "']");
                $(element).html(detalle);}
        });
    }

    function reloadCreate(id_maestro){
        $.ajax({
            url: '<?php echo Url::to(['adddetalle']) ?>',
            data:{
                _pjax: true,
                id_maestro: id_maestro,
            },
            dataType: 'json',
            success: function(detalle) {
                $('#ajaxCrudModal .modal-title').html('');
                $('#ajaxCrudModal .modal-body').html(detalle);
                $('#ajaxCrudModal .modal-footer').html('');
                }
        });
    }

    function sortDetalle(id_maestro,objeto){
        var id = $(objeto).attr('id');        
        var sort = $(objeto).attr('data-sort');
        if (sort.substring(0,1)=='-')
            new_sort=sort.substring(1);
        else
            new_sort='-'+sort;

        $.ajax({
            url: '<?php echo Url::to(['indexdetalle']) ?>',
            type:"GET",
            data:{
                sort: sort,
                id_maestro: id_maestro,
            },
            success: function(detalle) {
                element = $("tr").find("div[data-key='" + id_maestro + "']");
                $(element).html(detalle);
                $('#'+id).attr('data-sort',new_sort);
            }
        });
    }

    function submitUpdateDetalle(id_detalle,id_maestro){
        
        var $form = $('#form-detalle');
        var data = $form.serialize();

        $.ajax({
            url: '<?php echo Url::to(['updatedetalle']) ?>'+'&id_detalle='+id_detalle+'&id_maestro='+id_maestro,
            dataType: 'json',
            type:"POST",
            data: data,
            success: function(data) {
                if( data.status == 'success' ){
                    $('#ajaxCrudModal').modal('hide');
                    reloadDetalle(id_maestro);
                }else{
                    $('#ajaxCrudModal .modal-title').html(data.title);
                    $('#ajaxCrudModal .modal-body').html(data.content);
                    $('#ajaxCrudModal .modal-footer').html(data.footer);
                }
            }
        });
    }

    function submitAddDetalle(id_maestro){
        var keys = $('#crud-datatable-detalle').yiiGridView('getSelectedRows');
        $.ajax({
            url: '<?php echo Url::to(['adddetalle']) ?>',
            dataType: 'json',
            type:"POST",
            data:{
                keylist: keys,
                id_maestro: id_maestro
            },
            success: function(data) {

                if( data.status == 'success' ){
                    $('#ajaxCrudModal').modal('hide');
                    reloadDetalle(id_maestro);
                }else{
                    $('#ajaxCrudModal .modal-dialog').css({'width':'600px'});
                    $('#ajaxCrudModal .modal-title').html(data.title);
                    $('#ajaxCrudModal .modal-body').html(data.content);
                    $('#ajaxCrudModal .modal-footer').html(data.footer);

                }
            }
        });
    }

    function submitCreateDetalle(id_maestro){
        
        var $form = $('#form-detalle');
        var data = $form.serialize();

        $.ajax({
            url: '<?php echo Url::to(['createdetalle']) ?>'+'&id_maestro='+id_maestro,
            dataType: 'json',
            type:"POST",
            data: data,
            success: function(data) {
                if( data.status == 'success' ){
                    $('#ajaxCrudModal .modal-dialog').css({'width':'1000px'});
                    reloadCreate(id_maestro);
                }else{
                    $('#ajaxCrudModal .modal-title').html(data.title);
                    $('#ajaxCrudModal .modal-body').html(data.content);
                    $('#ajaxCrudModal .modal-footer').html(data.footer);
                }
            }
        });
    }

</script>

<?php
CrudAsset::register($this);

// Cuando tengo detalle agrego expandrowcolumn como  primer columna
if ($permisos['indexdetalle']) {
    $columns = array_merge(array(array(
        'class' => '\kartik\grid\ExpandRowColumn',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => Url::to(['indexdetalle']),   //  action mostrarDetalle con POST expandRowKey como ID
        'detailRowCssClass' => 'expanded-row',
        'expandIcon' => '<i class="glyphicon glyphicon-expand" style="color:#337ab7"></i>',
        'collapseIcon' => '<i class="glyphicon glyphicon-collapse-down" style="color:#337ab7"></i>',
        'expandOneOnly' => true,
        'detailAnimationDuration' => 100,
        'allowBatchToggle' => false,
    )), $columns);
}
// agrego acciones a $columns
$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '&nbsp;&nbsp;{view} {update} {delete} {viewusuarios}',
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Grupo',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'buttons' => [
            'viewusuarios' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-user">&nbsp;</span>', $url,
                        ['role'=>'modal-remote','title'=> 'Ver usuarios de este grupo',]);
                },
        ],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete'],
            'viewusuarios'=> $permisos['viewusuarios']
        ]
    ];

$stringToolbar="";

if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

?>
<div class="grupo-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ]
        ])?>
    </div>
</div>
<style> .modal-dialog { width: 600px; } </style>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>