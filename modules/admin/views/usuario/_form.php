<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'usuario')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'password')->passwordInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'email')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'activo')->checkbox();?>

    <?php // echo $form->field($model, 'control')->textInput(['maxlength' => true]);?>

    <?php
    $data=ArrayHelper::map(app\modules\mpi\models\Dominio::find()->asArray()->all(), 'id', 'area');
    echo $form->field($model, 'id_dominio')->dropDownList($data,['prompt'=>'Seleccionar...']);
    ?>

    <?php echo $form->field($model, 'obs')->textInput(['maxlength' => true]);?>

    <?php ActiveForm::end(); ?>
    
</div>
