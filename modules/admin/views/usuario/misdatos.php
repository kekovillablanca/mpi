<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use quidu\ajaxcrud\CrudAsset;

$this->title = 'Mis Datos';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<div class="usuario-misdatos">
    <h2><?= Html::encode($this->title) ?></h2>

    <?php if (Yii::$app->session->hasFlash('misDatosSubmitted')): ?>

        <div class="alert alert-success">
            Datos Guardados correctamente.
        </div>

    <?php else: ?>

        <div class="misdatos-form">

            <?php $form = ActiveForm::begin(); ?>

            <?php echo $form->field($model, 'usuario')->textInput(['maxlength' =>true,'readonly'=>true, 'style'=>'width:50%']);?>

            <?php echo $form->field($model, 'nombre')->textInput(['maxlength' => true, 'style'=>'width:50%']);?>

            <?php echo $form->field($model, 'email')->textInput(['maxlength' => true, 'style'=>'width:50%']);?>

            <?php echo $form->field($model, 'obs')->textarea(['rows' => 6, 'maxlength' =>true, 'style'=>'width:50%']);?>

            <div class="form-group">
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary pull-left', 'name' => 'Aceptar']) ?>
                <?= Html::button('Cambiar Contraseña', ['class' => 'btn btn-primary', 'role'=>'modal-remote','name' => 'Password','style'=>'margin-left: 337px;' ]) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    <?php endif; ?>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

