<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioEstadistica */
/* @var $form yii\widgets\ActiveForm */
?>
<script>
	$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	function verArea(event){
		
		$.ajax({
            url: '<?php echo Url::to(['buscarlocalidad']) ?>',
            
            dataType: "json",
            type:"POST",
            data: event,
            success: function(data) {
				console.log(data);
				document.getElementById("usuarioestadistica-id_localidad").value=data.idLocalidad;


			}
		});
	}
</script>
<div class="usuario-estadistica-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
		$data=ArrayHelper::map(app\modules\admin\models\Usuario::find()->asArray()->all(), 'id', 'nombre');
    	echo $form->field($model, 'id_usuario')->dropDownList($data,['prompt'=>'Seleccionar...'
]);
	?>

    <?php 
		$data=ArrayHelper::map(app\modules\sies\models\MArea::find()->asArray()->all(), 'id', 'descrip');
    	echo $form->field($model, 'id_m_area')->dropDownList($data,['prompt'=>'Seleccionar...','onchange'=>'verArea(this.value)']);
	?>
	<?php 
		$data=ArrayHelper::map(app\modules\mpi\models\Localidad::find()->asArray()->all(), 'id', 'nombre');
    	echo $form->field($model, 'id_localidad')->dropDownList($data,['prompt'=>'Seleccionar...']);
	?>
    <?php ActiveForm::end(); ?>
    
</div>
