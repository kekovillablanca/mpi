<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GrupoPermisoSearch represents the model behind the search form about `app\models\GrupoPermiso`.
 */
class GrupoPermisoSearch extends GrupoPermiso
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id'], 'integer'],
            [['id_grupo', 'id_permiso'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GrupoPermiso::find();
		$query->joinWith(['grupo']);
		$query->joinWith(['permiso']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_grupo'=> [
					'asc' => ['grupo.nombre' => SORT_ASC],
					'desc' => ['grupo.nombre' => SORT_DESC],
				],
                'id_permiso',
                'id_permiso_nom'=> [
					'asc' => ['permiso.nombre' => SORT_ASC],
					'desc' => ['permiso.nombre' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'grupo_permiso.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(grupo.nombre)',strtolower($this->id_grupo)])
              ->andFilterWhere(['like', 'lower(permiso.nombre)',strtolower($this->id_permiso)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('grupopermiso-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
