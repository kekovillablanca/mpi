<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsuarioGrupoSearch represents the model behind the search form about `app\models\UsuarioGrupo`.
 */
class UsuarioGrupoSearch extends UsuarioGrupo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id'], 'integer'],
            [['id_usuario', 'id_grupo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UsuarioGrupo::find();
		$query->joinWith(['grupo']);
		$query->joinWith(['usuario']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_usuario'=> [
					'asc' => ['usuario.nombre' => SORT_ASC],
					'desc' => ['usuario.nombre' => SORT_DESC],
				],
                'id_grupo',
                'id_grupo_nom'=> [
					'asc' => ['grupo.nombre' => SORT_ASC],
					'desc' => ['grupo.nombre' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'usuario_grupo.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(usuario.nombre)',strtolower($this->id_usuario)])
              ->andFilterWhere(['like', 'lower(grupo.nombre)',strtolower($this->id_grupo)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('usuariogrupo-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
