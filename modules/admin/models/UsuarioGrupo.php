<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "usuario_grupo".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_grupo
 *
 * @property Grupo $grupo
 * @property Usuario $usuario
 */
class UsuarioGrupo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario_grupo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_grupo'], 'required'],
            [['id_usuario', 'id_grupo'], 'integer'],
            [['id_usuario', 'id_grupo'], 'unique', 'targetAttribute' => ['id_usuario', 'id_grupo']],
            [['id_grupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupo::className(), 'targetAttribute' => ['id_grupo' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_grupo' => 'Id Grupo',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('usuario_grupo.id', 20),
			'id_usuario' => array('usuario.nombre',20),
			'id_grupo' => array('grupo.nombre',20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_usuario'=>
			[
				'attribute'=>'usuario.nombre',
				'label'=>'Id Usuario',
			],
			'id_grupo'=>
			[
				'attribute'=>'grupo.nombre',
				'label'=>'Id Grupo',
			],
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_usuario',
				'value'=>'usuario.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_grupo',
				'value'=>'grupo.nombre',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupo()
    {
        return $this->hasOne(Grupo::className(), ['id' => 'id_grupo']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario']);
    }



    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
