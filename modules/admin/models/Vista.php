<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;

use Yii;
use app\components\Metodos\Metodos;


/**
 * This is the model class for table "Vista".
 *
 * @property int $id
 * @property int $id_usuario
 * @property string $accion
 * @property string $modelo
 * @property string $columna
 */
class Vista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return "vista";
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'accion', 'modelo', 'columna'], 'required'],
            [['id_usuario'], 'default', 'value' => null],
            [['id_usuario'], 'integer'],
            [['accion', 'modelo'], 'string', 'max' => 100],
            [['columna'], 'string', 'max' => 5000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'id_usuario' => 'Id Usuario',
            'accion' => 'Accion',
            'modelo' => 'Modelo',
            'columna' => 'Columna',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('"Vista".id', 10),
			'id_usuario' => array('"Vista".id_usuario', 11),
			'accion' => array('"Vista".accion', 20),
			'modelo' => array('"Vista".modelo', 20),
			'columna' => array('"Vista".columna', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_usuario',
			'accion',
			'modelo',
			'columna',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_usuario',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'accion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'modelo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'columna',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
