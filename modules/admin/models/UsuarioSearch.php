<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsuarioSearch represents the model behind the search form about `app\models\Usuario`.
 */
class UsuarioSearch extends Usuario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id'], 'integer'],
            [['usuario', 'password', 'nombre', 'email', 'control', 'obs','id_dominio'], 'safe'],
            [['activo'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuario::find();
        $query->joinWith(['dominio']);
        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        $dataProvider->setSort([
            'attributes' => [
				'id',
				'usuario',
				'password',
				'nombre',
				'email',
				'activo',
				'control',
                'id_dominio'=> [
                    'asc' => ['dominio.area' => SORT_ASC],
                    'desc' => ['dominio.area' => SORT_DESC],
                ],
                'obs',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'usuario.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(usuario.usuario)',strtolower($this->usuario)])
              ->andFilterWhere(['like', 'lower(usuario.password)',strtolower($this->password)])
              ->andFilterWhere(['like', 'lower(usuario.nombre)',strtolower($this->nombre)])
              ->andFilterWhere(['like', 'lower(usuario.email)',strtolower($this->email)])
              ->andFilterWhere(['usuario.activo'=>$this->activo])
              ->andFilterWhere(['like', 'lower(usuario.control)',strtolower($this->control)])
              ->andFilterWhere(['like', 'lower(dominio.area)',strtolower($this->id_dominio)])
              ->andFilterWhere(['like', 'lower(usuario.obs)',strtolower($this->obs)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('usuario-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
