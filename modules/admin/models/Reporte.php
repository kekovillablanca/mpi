<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "reporte".
 *
 * @property int $id
 * @property int $id_usuario
 * @property string $titulo
 * @property string $subtitulo
 * @property string $descripcion
 * @property string $modelo
 * @property int $tamano_letra
 * @property int $tipo_hoja
 * @property int $ancho_reporte
 * @property string $datos
 * @property string $seleccion
 * @property string $resumen
 * 
 */
class Reporte extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reporte';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'titulo', 'modelo', 'datos', 'seleccion'], 'required'],
            [['id_usuario', 'tamano_letra', 'tipo_hoja', 'ancho_reporte'], 'default', 'value' => null],
            [['id_usuario', 'tamano_letra', 'tipo_hoja', 'ancho_reporte'], 'integer'],
            [['titulo', 'modelo', 'subtitulo'], 'string', 'max' => 100],
            [['datos', 'descripcion'], 'string', 'max' => 1000],
            [['seleccion', 'resumen'], 'string', 'max' => 5000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'titulo' => 'Titulo',
            'subtitulo' => 'Subtitulo',
            'descripcion' => 'Descripcion',
            'modelo' => 'Modelo',
            'tamano_letra' => 'Tamano Letra',
            'tipo_hoja' => 'Tipo Hoja',
            'ancho_reporte' => 'Ancho Reporte',
            'datos' => 'Datos',
            'seleccion' => 'Seleccion',
            'resumen' => 'Resumen',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('reporte.id', 20),
			'id_usuario' => array('reporte.id_usuario', 20),
			'titulo' => array('reporte.titulo', 20),
			'subtitulo' => array('reporte.subtitulo', 20),
			'descripcion' => array('reporte.descripcion', 20),
			'modelo' => array('reporte.modelo', 20),
			'tamano_letra' => array('reporte.tamano_letra', 13),
			'tipo_hoja' => array('reporte.tipo_hoja', 11),
			'ancho_reporte' => array('reporte.ancho_reporte', 14),
			'datos' => array('reporte.datos', 20),
			'seleccion' => array('reporte.seleccion', 20),
			'resumen' => array('reporte.resumen', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_usuario',
			'titulo',
			'subtitulo',
			'descripcion',
			'modelo',
			'tamano_letra',
			'tipo_hoja',
			'ancho_reporte',
			'datos',
			'seleccion',
			'resumen',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_usuario',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'titulo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'subtitulo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'modelo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'tamano_letra',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'tipo_hoja',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'ancho_reporte',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'datos',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'seleccion',
			// ],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'resumen',
			// ],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }

	public function setFromDataProvider($dataProvider,$accion){ 
		 
		// datos que uso en este metodo 

		
		$query=$dataProvider->query; 
		$sqlBase=$query->createCommand()->getSql(); 

		$clase=$query->modelClass; 

		$param=$query->createCommand()->params; 
		$order=$dataProvider->getSort()->getOrders(); 

		$modelo=new $clase;  // modelo que se va a exportar 
		$columnasVista=array(); 
		$seleccion=array(); 
		$columnasVista=Metodos::obtenerColumnas($modelo,$accion); 

		$seleccion=Metodos::obtenerAttributosColumnas($columnasVista); 
		$datosImpresion=$modelo->attributePrint(); 
		$etiquetasImpresion=$modelo->attributeLabels(); 
  
		// Obtengo SQL a partir del dataprovider 
  
		// primer parte de la consulta sql (SELECT). Hay que construirla 
		$select='SELECT '; 
		foreach($seleccion as $key=>$value){ 
			// mysql 
			// $select.=$datosImpresion[$value][0]." AS '".$etiquetasImpresion[$value]."',"; 
			// postgres 
			$select.=$datosImpresion[$value][0].' AS "'.$etiquetasImpresion[$value].'",'; 
  
		} 
  
		$select= substr($select, 0, -1)." "; // saco la ultima coma y agrego espacio 
  
		// segunda parte de la consulta sql ( desde FROM ) 
		$from=substr($sqlBase, strpos($sqlBase,'FROM')); 
		// acomodo where con sus parametros 
  
		foreach ($param as $key => $value) { 
			$from=str_replace($key,"'".$value."'",$from); 
		} 
  
		// acomodo order 
		if ($order){ 
			$order_aux=key($order); 
  
			if (!(strpos ( $order_aux , '.'))){ 
				// no encontre punto 
				$order_aux='"'.$modelo->tableSchema->name.'"."'.$order_aux.'"'; 
			} 
  
			if ($order[key($order)]==3){ 
				$from.=" ORDER BY ".$order_aux.' DESC'; 
			}else{ 
				$from.=" ORDER BY ".$order_aux.' ASC'; 
			} 
		} 
		$sql=$select.$from; 
		$ancho=0; 
		$datos=array(); 
  
		foreach($columnasVista as $datosAux) { 
			$ancho =$ancho+$datosImpresion[$datosAux['attribute']][1]; 
			$datos[$datosAux['attribute']]=array($etiquetasImpresion[$datosAux['attribute']],$datosImpresion[$datosAux['attribute']][1]); 
		} 

		// Nombre del Listado 
		$auxiliar=explode('\\',$clase); 
		$nombre=$auxiliar[count($auxiliar)-1]; 
  
		$this->id_usuario=Yii::$app->user->id ; 
		$this->modelo='';
		$this->titulo="Listado de ".Metodos::pluralize($nombre); 
		$this->subtitulo=""; 
		$this->tamano_letra=8; 
  
		$pixeles=$ancho*(0.6*$this->tamano_letra+0.6); 
		$tipo_hoja=($pixeles>528)?1:0; 
  
		$this->tipo_hoja=$tipo_hoja; 
		$this->ancho_reporte=$ancho; 
		$this->resumen=''; 
		$this->datos=serialize($datos); 
		$this->seleccion=$sql; 
  
	} 

	public function setFromDataProviderNotClass($dataProvider, $accion)
{

		// Verificar que el DataProvider sea válido
		if (!$dataProvider instanceof \yii\data\ActiveDataProvider) {
			throw new \yii\web\ServerErrorHttpException('El DataProvider no es válido.');
		}

	
		// Definir las columnas a seleccionar
		$selectColumns = [
			'profesional_nombre',
			'profesional_apellido',
			'paciente_nombre',
			'paciente_apellido',
			'descripcion_diagnostico',
			'fecha_turno',
			'diagnostico_codigo',
			'descripcion_especialidad',
			'centro_nombre',
			'codigo',
		];
	
		// Crear la consulta
		$query = (new \yii\db\Query())
			->select($selectColumns)
			->from('hospitalroca.odontologia')
			->andFilterWhere(['between', 'fecha_turno', 1, 5]);
	
		// Crear el DataProvider a partir de la consulta
		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $query,
			'pagination' => false, // Puedes ajustar la paginación según sea necesario
		]);
	
		// Obtener SQL a partir del DataProvider
		$sqlBase = $query->createCommand()->getSql(); 
		$param = $query->createCommand()->params; 
		$order = $dataProvider->getSort()->getOrders(); 
	
		// Obtener datos desde el DataProvider
		$data = $dataProvider->getModels();
		if (empty($data)) {
			throw new \yii\web\ServerErrorHttpException('No se encontraron datos para exportar.');
		}
	
		// Obtener atributos de la primera fila para determinar las columnas
		$firstRow = $data[0];
		$attributes = array_keys($firstRow->attributes);
	
		// Construir las etiquetas de impresión
		$etiquetasImpresion = [];
		foreach ($attributes as $attribute) {
			$etiquetasImpresion[$attribute] = $firstRow->getAttributeLabel($attribute) ?: ucfirst($attribute);
		}
	
		// Construir la primera parte de la consulta SQL (SELECT)
		$select = 'SELECT '; 
		foreach ($selectColumns as $column) { 
			$select .= $column . ' AS "' . $etiquetasImpresion[$column] . '",'; 
		} 
	
		$select = substr($select, 0, -1) . " "; // Quitar última coma y agregar espacio 
	
		// Segunda parte de la consulta SQL (FROM)
		$from = substr($sqlBase, strpos($sqlBase, 'FROM')); 
	
		// Ajustar WHERE con sus parámetros 
		foreach ($param as $key => $value) { 
			$from = str_replace($key, "'" . $value . "'", $from); 
		} 
	
		// Ajustar el orden 
		if ($order) { 
			$order_aux = key($order); 
			$order_aux = '"' . 'hospitalroca.odontologia' . '"."' . $order_aux . '"'; // Cambia 'hospitalroca.odontologia' por el nombre real de tu tabla
	
			if ($order[key($order)] == 3) { 
				$from .= " ORDER BY " . $order_aux . ' DESC'; 
			} else { 
				$from .= " ORDER BY " . $order_aux . ' ASC'; 
			} 
		} 
	
		$sql = $select . $from; 
		$ancho = 0; 
		$datos = []; 
	
		foreach ($selectColumns as $column) { 
			$ancho += 100; // Ajusta el ancho de columna aquí según tus necesidades
			$datos[$column] = [$etiquetasImpresion[$column], 100]; // Define el ancho de cada columna
		} 
	
		// Nombre del Listado 
		$nombre = "Listado de Odontología"; // Cambiar según sea necesario
	
		$this->id_usuario = Yii::$app->user->id; 
		$this->modelo = '';
		$this->titulo = "Listado de " . $nombre; 
		$this->subtitulo = ""; 
		$this->tamano_letra = 8; 
	
		$pixeles = $ancho * (0.6 * $this->tamano_letra + 0.6); 
		$tipo_hoja = ($pixeles > 528) ? 1 : 0; 
	
		$this->tipo_hoja = $tipo_hoja; 
		$this->ancho_reporte = $ancho; 
		$this->resumen = ''; 
		$this->datos = serialize($datos); 
		$this->seleccion = $sql; 
	}




	// metodo propio de este modelo 
	public function setFromSql($clase,$sqlBase,$accion){ 

			// segunda parte de la consulta sql ( desde FROM ) 
            $from=substr($sqlBase, strpos($sqlBase,'FROM')); 

			// count
//            $sqlCount='SELECT count(*) '.$from; 
//            $cantidad_registros=Yii::$app->db->createCommand($sqlCount)->queryOne()['count'];            

			$modelo=new $clase;  // modelo que se va a exportar 

            $columnasVista=array(); 
            $seleccion=array(); 
            $columnasVista=Metodos::obtenerColumnas($modelo,$accion); 

            $seleccion=Metodos::obtenerAttributosColumnas($columnasVista); 
            $datosImpresion=$modelo->attributePrint(); 
            $etiquetasImpresion=$modelo->attributeLabels(); 
    
            // Obtengo SQL a partir del sqlBase 
    
            // primer parte de la consulta sql (SELECT). Hay que construirla 
            $select='SELECT '; 
            foreach($seleccion as $key=>$value){ 
                $select.=$datosImpresion[$value][0].' AS "'.$etiquetasImpresion[$value].'",';     
            } 
    
            $select= substr($select, 0, -1)." "; // saco la ultima coma y agrego espacio 
    
            $sql=$select.$from; 

			// acomodo parametros y datos de impresion
			$ancho=0; 
            $datos=array(); 
    
            foreach($columnasVista as $datosAux) { 
                $ancho =$ancho+$datosImpresion[$datosAux['attribute']][1]; 
                $datos[$datosAux['attribute']]=array($etiquetasImpresion[$datosAux['attribute']],$datosImpresion[$datosAux['attribute']][1]); 
            } 

			// Nombre del Listado 
			$auxiliar=explode('\\',$clase); 
			$nombre=$auxiliar[count($auxiliar)-1]; 
	  
			// reporte
			$this->id_usuario=Yii::$app->user->id ; 
			$this->modelo=$nombre;
			if (substr($nombre, -1)=='s')
				$this->titulo="Listado de ".$nombre; 
			else
				$this->titulo="Listado de ".Metodos::pluralize($nombre); 
	
			$this->subtitulo=""; 
            $this->tamano_letra=8; 
    
            $pixeles=$ancho*(0.6*$this->tamano_letra+0.6); 
            $tipo_hoja=($pixeles>528)?1:0; 
    
            $this->tipo_hoja=$tipo_hoja; 
            $this->ancho_reporte=$ancho; 
            $this->resumen=''; 
            $this->datos=serialize($datos); 
            $this->seleccion=$sql;             

//			$this->cantidad_registros=$cantidad_registros;

	}

    public function setFromDataProviderVistas($dataProvider,$sqlBase,$param,$accion){

        // datos que uso en este metodo
        $query=$dataProvider->query;        
        $clase=$query->modelClass;

        $order=$dataProvider->getSort()->getOrders();
        
        $modelo=new $clase;   // modelo que se va a exportar
        $columnasVista=array();
        $seleccion=array();
        $columnasVista=Metodos::obtenerColumnas($modelo,$accion);
        $seleccion=Metodos::obtenerAttributosColumnas($columnasVista);
        $datosImpresion=$modelo->attributePrint();
        $etiquetasImpresion=$modelo->attributeLabels();

        // Obtengo SQL a partir del dataprovider

        // primer parte de la consulta sql (SELECT). Hay que construirla
        $select='SELECT ';
        foreach($seleccion as $key=>$value){
            // mysql
            // $select.=$datosImpresion[$value][0]." AS '".$etiquetasImpresion[$value]."',";
            // postgres
            $select.=$datosImpresion[$value][0].' AS "'.$etiquetasImpresion[$value].'",';
        }
        $select= substr($select, 0, -1)." ";  // saco la ultima coma y agrego espacio

        // segunda parte de la consulta sql ( desde FROM )
        $from=substr($sqlBase, strpos($sqlBase,'FROM'));
        // acomodo where con sus parametros
        foreach ($param as $key => $value) {
            $from=str_replace($key,"'".$value."'",$from);
        }
        // acomodo order
        if ($order){
            if ($order[key($order)]==3){
                $from.=" ORDER BY ".key($order).' DESC';
            }else{
                $from.=" ORDER BY ".key($order).' ASC';
            }
        }
        $sql=$select.$from;
        $ancho=0;
        $datos=array();

        foreach($columnasVista as $datosAux) {
            $ancho =$ancho+$datosImpresion[$datosAux['attribute']][1];
            $datos[$datosAux['attribute']]=array($etiquetasImpresion[$datosAux['attribute']],$datosImpresion[$datosAux['attribute']][1]);
        }

        // Nombre del Listado
        $auxiliar=explode('\\',$clase);
        $nombre=$auxiliar[count($auxiliar)-1];

        $this->id_usuario=Yii::$app->user->id ;
        $this->modelo=$clase;
        $this->titulo="Listado de ".Metodos::pluralize($nombre);
        $this->subtitulo="";
        $this->tamano_letra=8;

        $pixeles=$ancho*(0.6*$this->tamano_letra+0.6);
        $tipo_hoja=($pixeles>528)?1:0;

        $this->tipo_hoja=$tipo_hoja;
        $this->ancho_reporte=$ancho;
        $this->resumen='';
        $this->datos=serialize($datos);
        $this->seleccion=$sql;

    }

}
