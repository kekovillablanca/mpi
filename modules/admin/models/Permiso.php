<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "permiso".
 *
 * @property int $id
 * @property string $nombre
 * @property string $regla
 * @property string $obs
 *
 * @property GrupoPermiso[] $grupoPermisos
 * @property Grupo[] $grupos
 */

class Permiso extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permiso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 150],
            [['regla'], 'string', 'max' => 5000],
            [['obs'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'regla' => 'Regla',
            'obs' => 'Obs',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('permiso.id', 20),
			'nombre' => array('permiso.nombre', 20),
			'regla' => array('permiso.regla', 20),
			'obs' => array('permiso.obs', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
			'regla',
			'obs',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'50px',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'regla',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'obs',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoPermisos()
    {
        return $this->hasMany(GrupoPermiso::className(), ['id_permiso' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupos()
    {
        return $this->hasMany(Grupo::className(), ['id' => 'id_grupo'])->viaTable('grupo_permiso', ['id_permiso' => 'id']);
    }



    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
