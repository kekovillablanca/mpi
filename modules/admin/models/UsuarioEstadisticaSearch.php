<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsuarioEstadisticaSearch represents the model behind the search form about `app\models\UsuarioEstadistica`.
 */
class UsuarioEstadisticaSearch extends UsuarioEstadistica
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_usuario', 'id_m_area','id_localidad'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UsuarioEstadistica::find();
		$query->joinWith(['marea']);
		$query->joinWith(['usuario']);
        $query->joinWith(['localidad']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_usuario'=> [
					'asc' => ['usuario.nombre' => SORT_ASC],
					'desc' => ['usuario.nombre' => SORT_DESC],
				],
				'id_m_area'=> [
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
				],
                'id_localidad'=> [
					'asc' => ['localidad.nombre' => SORT_ASC],
					'desc' => ['localidad.nombre' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'usuario_estadistica.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(usuario.nombre)',strtolower($this->id_usuario)])
        ->andFilterWhere(['like', 'lower(localidad.nombre)',strtolower($this->id_localidad)])
              ->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->id_m_area)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('usuarioestadistica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
