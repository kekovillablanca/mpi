<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "grupo_permiso".
 *
 * @property int $id
 * @property int $id_grupo
 * @property int $id_permiso
 *
 * @property Grupo $grupo
 * @property Permiso $permiso
 */
class GrupoPermiso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupo_permiso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_grupo', 'id_permiso'], 'required'],
            [['id_grupo', 'id_permiso'], 'integer'],
            [['id_grupo', 'id_permiso'], 'unique', 'targetAttribute' => ['id_grupo', 'id_permiso']],
            [['id_grupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupo::className(), 'targetAttribute' => ['id_grupo' => 'id']],
            [['id_permiso'], 'exist', 'skipOnError' => true, 'targetClass' => Permiso::className(), 'targetAttribute' => ['id_permiso' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_grupo' => 'Id Grupo',
            'id_permiso' => 'Id Permiso',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('grupo_permiso.id', 20),
			'id_grupo' => array('grupo.nombre',20),
			'id_permiso' => array('permiso.nombre',20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_grupo'=>
			[
				'attribute'=>'grupo.nombre',
				'label'=>'Id Grupo',
			],
			'id_permiso'=>
			[
				'attribute'=>'permiso.nombre',
				'label'=>'Id Permiso',
			],
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_grupo',
				'value'=>'grupo.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_permiso',
				'value'=>'permiso.nombre',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupo()
    {
        return $this->hasOne(Grupo::className(), ['id' => 'id_grupo']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermiso()
    {
        return $this->hasOne(Permiso::className(), ['id' => 'id_permiso']);
    }



    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
