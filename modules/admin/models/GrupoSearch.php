<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GrupoSearch represents the model behind the search form about `app\models\Grupo`.
 */
class GrupoSearch extends Grupo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id'], 'integer'],
            [['nombre', 'regla', 'obs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Grupo::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'nombre',
				'regla',
				'obs',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'grupo.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(grupo.nombre)',strtolower($this->nombre)])
              ->andFilterWhere(['like', 'lower(grupo.regla)',strtolower($this->regla)])
              ->andFilterWhere(['like', 'lower(grupo.obs)',strtolower($this->obs)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('grupo-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
