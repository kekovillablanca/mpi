<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "grupo".
 *
 * @property int $id
 * @property string $nombre
 * @property string $regla
 * @property string $obs
 *
 * @property GrupoPermiso[] $grupoPermisos
 * @property Permiso[] $permisos
 * @property UsuarioGrupo[] $usuarioGrupos
 * @property Usuario[] $usuarios
 */
class Grupo extends \yii\db\ActiveRecord
{

    public $tieneRegla;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 150],
            [['regla'], 'string', 'max' => 5000],
            [['obs'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'regla' => 'Regla',
            'obs' => 'Obs',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('grupo.id', 20),
			'nombre' => array('grupo.nombre', 20),
			'regla' => array('grupo.regla', 20),
			'obs' => array('grupo.obs', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
			'regla',
			'obs',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'regla',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'obs',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoPermisos()
    {
        return $this->hasMany(GrupoPermiso::className(), ['id_grupo' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermisos()
    {
        return $this->hasMany(Permiso::className(), ['id' => 'id_permiso'])->viaTable('grupo_permiso', ['id_grupo' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioGrupos()
    {
        return $this->hasMany(UsuarioGrupo::className(), ['id_grupo' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id' => 'id_usuario'])->viaTable('usuario_grupo', ['id_grupo' => 'id']);
    }



    public function afterFind(){
        // tareas despues de encontrar el objeto
        parent::afterFind();
        if ($this->regla)
            $this->tieneRegla="Si";
        else
            $this->tieneRegla="No";
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
