<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;

use Yii;
use app\components\Metodos\Metodos;

use app\modules\mpi\models\Dominio;
use DateTime;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $password
 * @property string $nombre
 * @property string $email
 * @property bool $activo
 * @property string $control
 * @property string $obs
 * @property int $id_dominio
 * @property string $fecha_cambio
 * @property string $verification_code;
 * @property Dominio $dominio
 * @property UsuarioGrupo[] $usuarioGrupos
 * @property Grupo[] $grupos
 */
class Usuario extends \yii\db\ActiveRecord
{

    public $actualizarFecha=false;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'password', 'nombre', 'email', 'activo'], 'required'],
            [['activo'], 'boolean'],
            [['id_dominio','fecha_cambio'], 'default', 'value' => null],
            [['id_dominio'], 'integer'],
            [['usuario', 'password', 'nombre', 'email'], 'string', 'max' => 100],
            [['verification_code'], 'string', 'max' => 100],
            [['control', 'obs'], 'string', 'max' => 1000],
            [['usuario'], 'unique'],
            [['id_dominio'], 'exist', 'skipOnError' => true, 'targetClass' => Dominio::className(), 'targetAttribute' => ['id_dominio' => 'id']],
            
  
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuario',
            'password' => 'Contraseña',
            'nombre' => 'Nombre',
            'email' => 'Email',
            'activo' => 'Activo',
            'control' => 'Control',
            'obs' => 'Obs',
            'id_dominio' => 'Dominio',
        ];
    }

    public function attributePrint()
    {
        return [
			'id' => array('usuario.id', 20),
			'usuario' => array('usuario.usuario', 20),
			'password' => array('usuario.password', 20),
			'nombre' => array('usuario.nombre', 20),
			'email' => array('usuario.email', 20),
			'activo' => array('usuario.activo', 7),
			'control' => array('usuario.control', 20),
			'obs' => array('usuario.obs', 20),
            'id_dominio' => array('dominio.area',11),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'usuario',
//			'password',
			'nombre',
			'email',
			'activo'=>
			[
				'attribute'=>'activo',
				'format'=>'boolean',
			],
//			'control',
            'id_dominio'=>
                [
                    'attribute'=>'dominio.area',
                    'label'=>'Dominio',
                ],
			'obs',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'100px',
            ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'usuario',
			],
//			[
//				'class'=>'\kartik\grid\DataColumn',
//				'attribute'=>'password',
//			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'email',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'activo',
				'format'=>'boolean',
			],
//			[
//				'class'=>'\kartik\grid\DataColumn',
//				'attribute'=>'control',
//			],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'id_dominio',
                'value'=>'dominio.area',
            ],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'obs',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_cambio',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDominio()
    {
        return $this->hasOne(Dominio::className(), ['id' => 'id_dominio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioGrupos()
    {
        return $this->hasMany(UsuarioGrupo::className(), ['id_usuario' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupos()
    {
        return $this->hasMany(Grupo::className(), ['id' => 'id_grupo'])->viaTable('usuario_grupo', ['id_usuario' => 'id']);
    }



    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            
      
            if($this->isNewRecord){
                $this->password=md5($this->password);
               

            }else

            {
                $this->fecha_cambio=null;
                $passBd=$this::findOne($this->id);
                if($this->password!=$passBd->password)
                    $this->password=md5($this->password);
                else{
                    $this->actualizarFecha=false;
                }
                
                if($this->actualizarFecha==false)
                    $this->fecha_cambio=date('Y-m-d', strtotime('+30 days'));
                
            }
        
            // Place your custom code here
            return true;
        } else {
           
            return false;
        }
    }

}
