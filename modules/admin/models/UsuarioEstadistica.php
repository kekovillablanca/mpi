<?php
/* Modelo generado por Model(Q) */
namespace app\modules\admin\models;
//namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\mpi\models\Localidad;
use app\modules\sies\models\MArea;

/**
 * This is the model class for table "usuario_estadistica".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_m_area
 * @property int $id_localidad
 * @property MArea $mArea
 * @property Usuario $usuario
 */
class UsuarioEstadistica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.usuario_estadistica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['id_usuario', 'id_m_area'], 'default', 'value' => null],
           
            [['id_usuario', 'id_m_area','id_localidad'], 'required'],
            [['id_usuario', 'id_m_area','id_localidad'], 'integer'],
            [['id_m_area'], 'exist', 'skipOnError' => true, 'targetClass' => MArea::className(), 'targetAttribute' => ['id_m_area' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id_usuario' => 'id']],
            [['id_localidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['id_localidad' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Usuario',
            'id_m_area' => 'Area',
            'id_localidad'=>'localidad'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('usuario_estadistica.id', 10),
			'id_usuario' => array('usuario.nombre',20),
			'id_m_area' => array('m_area.descrip',30),
            'id_localidad' => array('localidad.nombre',30),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_usuario'=>
			[
				'attribute'=>'usuario.nombre',
				'label'=>'Usuario',
			],
			'id_m_area'=>
			[
				'attribute'=>'marea.descrip',
				'label'=>'Area',
			],
            'id_localidad'=>[
                'attribute'=>'localidad.nombre',
				'label'=>'nombre',
            ]
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_usuario',
				'value'=>'usuario.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_m_area',
				'value'=>'marea.descrip',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_localidad',
				'value'=>'localidad.nombre',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarea()
    {
        return $this->hasOne(MArea::className(), ['id' => 'id_m_area']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidad()
    {
        return $this->hasOne(Localidad::className(), ['id' => 'id_localidad']);
    }



    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
