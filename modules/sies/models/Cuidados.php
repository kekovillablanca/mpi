<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "cuidados".
 *
 * @property int $id
 * @property int $id_area
 * @property int $mes
 * @property int $anio
 * @property int $medico
 * @property int $psicologo
 * @property int $enfermero
 * @property int $trabajador_social
 * @property int $kinesiologo
 * @property int $terapista_ocupacional
 * @property int $secretario
 * @property int $voluntario
 * @property int $otros
 *
 * @property MArea $area
 * @property CuidadosPractica[] $cuidadosPracticas
 */
/*
medico
psicologo
enfermero
trabajador_social
kinesiologo
terapista_ocupacional
secretario
voluntario
otros
*/
 class Cuidados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.cuidados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_area', 'mes', 'anio','medico','psicologo','enfermero','trabajador_social','kinesiologo','terapista_ocupacional','secretario','voluntario','otros' ], 'default', 'value' => null],
           //le agrego required para que funcione 
           [['anio'], 'required'],
            [['id_area', 'mes', 'anio','medico','psicologo','enfermero','trabajador_social','kinesiologo','terapista_ocupacional','secretario','voluntario','otros'], 'integer'],
            [['id_area'], 'exist', 'skipOnError' => true, 'targetClass' => MArea::className(), 'targetAttribute' => ['id_area' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_area' => 'Area',
            'mes' => 'Mes',
            'anio' => 'Año',
            'medico' => 'Médico',
            'psicologo'=>'Psicólogo',
            'enfermero'=>'Enfermero',
            'trabajador_social'=>'Trabajador Social',
            'kinesiologo'=>'Kinesiólogo',
            'terapista_ocupacional'=>'Terapista Ocupacional',
            'secretario'=>'Secretario',
            'voluntario'=>'Voluntario',
            'otros'=>'Otros'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('cuidados.id', 10),
			'id_area' => array('m_area.descrip',10),
			'mes' => array('cuidados.mes', 10),
			'anio' => array('cuidados.anio', 10),
            'medico' => array('sies.cuidados.medico', 10),
            'psicologo' => array('sies.cuidados.psicologo', 10),
            'enfermero' => array('sies.cuidados.enfermero', 10),
            'trabajador_social' => array('sies.cuidados.trabajador_social', 18),
            'kinesiologo' => array('sies.cuidados.kinesiologo', 12),
            'terapista_ocupacional' => array('sies.cuidados.terapista_ocupacional', 20),
            'secretario' => array('sies.cuidados.secretario', 11),
            'voluntario' => array('sies.cuidados.voluntario', 11),
            'otros' => array('sies.cuidados.otros', 10),            
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_area'=>
			[
				'attribute'=>'area.descrip',
				'label'=>'Area',
			],
			'mes',
			'anio',
            'medico',
            'psicologo',
            'enfermero',
            'trabajador_social',
            'kinesiologo',
            'terapista_ocupacional',
            'secretario',
            'voluntario',
            'otros',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_area',
				'value'=>'area.descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'mes',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'anio',
			],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'medico',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'psicologo',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'enfermero',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'trabajador_social',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'kinesiologo',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'terapista_ocupacional',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'secretario',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'voluntario',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'otros',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(MArea::className(), ['id' => 'id_area']);
    }
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuidadospracticas()
    {
        return $this->hasMany(CuidadosPractica::className(), ['id_cuidados' => 'id']);
    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
