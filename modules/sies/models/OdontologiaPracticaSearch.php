<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OdontologiaPracticaSearch represents the model behind the search form about `app\models\OdontologiaPractica`.
 */
class OdontologiaPracticaSearch extends OdontologiaPractica
{
    public $area;
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_odontologia', 'id_practica','practica_nombre', 'cantidad_practica','area','sttipo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OdontologiaPractica::find();
		$query->joinWith(['practica']);
        $query->joinWith('odontologia.area');
        $query->joinWith('odontologia.sttipo');

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_practica',
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'sttipo'=>[
                    'asc' => ['st_tipo.nombre' => SORT_ASC],
					'desc' => ['st_tipo.nombre' => SORT_DESC],
                ],
                'odontologiaanio'=>[
                    'asc' => ['odontologia.anio' => SORT_ASC],
					'desc' => ['odontologia.anio' => SORT_DESC],
                ],
                'area'=>[
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
                ],
				'cantidad_practica',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'odontologia_practica.id' => $this->id,
            'odontologia_practica.cantidad_practica' => $this->cantidad_practica,
        ]);

        $query->andFilterWhere(['like', 'lower(odontologia_practica.id_practica)',strtolower($this->id_practica)])
              ->andFilterWhere(['like', 'lower(st_nomenclador.nombre)',strtolower($this->practica_nombre)])
              ->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->area)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('odontologiapractica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
