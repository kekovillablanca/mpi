<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "varios_practica".
 *
 * @property int $id
 * @property int $id_varios
 * @property string $id_practica
 * @property int $int
 * @property int $cex
 * @property int $gua
 * @property int $otr
 * @property int $der
 * @property int $de2
 *
 * @property Varios $varios
 * @property STNomenclador $practica
 */
class VariosPractica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $practica_nombre;

     public static function tableName()
    {
        return 'sies.varios_practica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_varios', 'int', 'cex', 'gua', 'otr', 'der', 'de2'], 'default', 'value' => null],
            [['id_varios', 'int', 'cex', 'gua', 'otr', 'der', 'de2'], 'integer'],
            [['id_practica'], 'string', 'max' => 20],
            [['id_varios'], 'exist', 'skipOnError' => true, 'targetClass' => Varios::className(), 'targetAttribute' => ['id_varios' => 'id']],
            [['id_practica'], 'exist', 'skipOnError' => true, 'targetClass' => STNomenclador::className(), 'targetAttribute' => ['id_practica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_practica' => 'Código',
            'practica_nombre' => 'Práctica',
            'int' => 'Interna',
            'cex' => 'Consulta',
            'gua' => 'Guardia',
            'otr' => 'Otros',
            'der' => 'Deriva DE',
            'de2' => 'Deriva A',
            'variosint'=>'Pac Internados',
            'varioscon'=>'Pac Consultorio',
            'variosgua'=> 'Pac Guardia',
            'variosanio'=>'Anio',
            'variosmes'=> 'Mes',
            'sttipo' =>'Tipo',
            'area'=> 'area'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('varios_practica.id', 10),
			'id_practica' => array('varios_practica.id_practica',12),
			'practica_nombre' => array('st_nomenclador.nombre',12),
			'int' => array('varios_practica.int', 10),
			'cex' => array('varios_practica.cex', 10),
			'gua' => array('varios_practica.gua', 10),
			'otr' => array('varios_practica.otr', 10),
			'der' => array('varios_practica.der', 10),
			'de2' => array('varios_practica.de2', 10),
            'variosint' => array('varios.int', 10),
			'varioscon' => array('varios.con', 10),
			'variosgua' => array('varios.gua', 10),
            'variosmes' => array('varios.mes', 10),
			'variosanio' => array('varios.anio', 10),
            'sttipo' => array('st_tipo.nombre',11),
            'area' => array('m_area.descrip',10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'area'=>
			[
				'attribute'=>'varios.area.descrip',
				'label'=>'Area',
			],
            'sttipo'=>
			[
				'attribute'=>'varios.sttipo.nombre',
				'label'=>'Area',
			],
			'variosanio'=>
			[
				'attribute'=>'varios.anio',
				'label'=>'Año',
			],
			'variosmes'=>
			[
				'attribute'=>'varios.mes',
				'label'=>'Mes',
			],
            'varioscon'=>
			[
				'attribute'=>'varios.con',
				'label'=>'Con',
			],
            'variosint'=>
			[
				'attribute'=>'varios.int',
				'label'=>'Int',
			],
            'variosgua'=>
			[
				'attribute'=>'varios.gua',
				'label'=>'Gua',
			],
			'sttipo'=>
			[
				'attribute'=>'varios.sttipo.nombre',
				'label'=>'Tipo',
			],
            'id_practica',
			'practica_nombre'=>
			[
				'attribute'=>'practica.nombre',
				'label'=>'Práctica',
			],
			'int',
			'cex',
			'gua',
			'otr',
			'der',
			'de2',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_practica',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'practica_nombre',
//				'value'=>'practica.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'int',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cex',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'gua',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otr',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'der',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'de2',
			],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'variosmes',
                'label'=> 'Mes',
                'value'=>'varios.mes'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'variosanio',
                'label'=> 'Anio',
                'value'=>'varios.anio'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'varioscon',
                'label'=> 'Pac Consultorio',
                'value'=>'varios.con'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'variosint',
                'label'=> 'Pac Internados',
                'value'=>'varios.int'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'variosgua',
                'label'=> 'Pac Guardia',
                'value'=>'varios.gua'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'sttipo',
                'label'=> 'Tipo',
                'value'=>'varios.sttipo.nombre'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'area',
                'label'=> 'Area',
                'value'=>'varios.area.descrip'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVarios()
    {
        return $this->hasOne(Varios::className(), ['id' => 'id_varios']);
    }
    public function getVariosint(){
        return $this->getVarios()->select('int');
    }
    public function getVarioscon(){
        return $this->getVarios()->select('con');
    }
    public function getVariosgua(){
        return $this->getVarios()->select('gua');
    }
    public function getVariosanio(){
        return $this->getVarios()->select('anio');
    }
    public function getVariosmes(){
        return $this->getVarios()->select('mes');
    }
    public function getArea()
    {
        return $this->getVarios()->select('descrip')->join('inner join','sies.m_area ma','ma.id=varios.id_area');
    }
    public function getSttipo()
    {
        return $this->getVarios()->select('nombre')->join('inner join','sies.st_tipo st','st.id=varios.id_st_tipo');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractica()
    {
        return $this->hasOne(STNomenclador::className(), ['id' => 'id_practica']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->practica_nombre=$this->practica->nombre;

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
