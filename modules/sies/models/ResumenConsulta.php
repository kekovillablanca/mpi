<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "sies.resumen_consulta".
 *
 * @property int $id
 * @property int $anio
 * @property int $mes
 * @property int $id_puesto
 * @property int $id_servicio
 * @property int $id_rango
 * @property int $varones
 * @property int $mujeres
 * 
 */
class ResumenConsulta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $id_area;

    public static function tableName()
    {
        return 'sies.resumen_consulta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['anio', 'mes', 'id_puesto', 'id_servicio', 'id_rango', 'varones', 'mujeres'], 'default', 'value' => null],
            [['anio', 'mes', 'id_puesto', 'id_servicio', 'id_rango', 'varones', 'mujeres'], 'integer'],
            [['id_puesto'], 'exist', 'skipOnError' => true, 'targetClass' => MPuesto::className(), 'targetAttribute' => ['id_puesto' => 'id']],
            [['id_rango'], 'exist', 'skipOnError' => true, 'targetClass' => MRango::className(), 'targetAttribute' => ['id_rango' => 'id']],
            [['id_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => MServicio::className(), 'targetAttribute' => ['id_servicio' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'anio' => 'Año',
            'mes' => 'Mes',
            'id_puesto' => 'Puesto',
            'id_area' => 'Area',
            'id_servicio' => 'Servicio',
            'id_rango' => 'Rango',
            'varones' => 'Varones',
            'mujeres' => 'Mujeres',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('sies.resumen_consulta.id', 10),
			'anio' => array('sies.resumen_consulta.anio', 10),
			'mes' => array('sies.resumen_consulta.mes', 10),
			'id_puesto' => array('sies.m_puesto.descrip',10),
			'id_area' => array('sies.m_puesto.area',10),
			'id_servicio' => array('sies.m_servicio.descrip',12),
			'id_rango' => array('sies.m_rango.nombre',10),
			'varones' => array('sies.resumen_consulta.varones', 10),
			'mujeres' => array('sies.resumen_consulta.mujeres', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'anio',
			'mes',
			'id_puesto'=>
			[
				'attribute'=>'sies.m_puesto.descrip',
				'label'=>'Puesto',
			],
			'id_area'=>
			[
				'attribute'=>'sies.m_puesto.descrip',
				'label'=>'Area',
			],
			'id_servicio'=>
			[
				'attribute'=>'sies.m_servicio.descrip',
				'label'=>'Servicio',
			],
			'id_rango'=>
			[
				'attribute'=>'sies.m_rango.nombre',
				'label'=>'Rango',
			],
			'varones',
			'mujeres',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'anio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'mes',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_puesto',
				'value'=>'puesto.descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_area',
				'value'=>'puesto.area',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_servicio',
				'value'=>'servicio.descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_rango',
				'value'=>'rango.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'varones',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'mujeres',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRango()
    {
        return $this->hasOne(MRango::className(), ['id' => 'id_rango']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPuesto()
    {
        return $this->hasOne(MPuesto::className(), ['id' => 'id_puesto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
/*    public function getArea()
    {
        return $this->hasOne(MPuesto::className(), ['id' => 'id_area']);
    }
*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(MServicio::className(), ['id' => 'id_servicio']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->id_area=$this->id_puesto;
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
