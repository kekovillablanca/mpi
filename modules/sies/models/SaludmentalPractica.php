<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "saludmental_practica".
 *
 * @property int $id
 * @property int $id_saludmental
 * @property string $id_practica
 * @property int $total_mensual
 *
 * @property Saludmental $saludmental
 * @property STNomenclador $practica
 */
class SaludmentalPractica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $practica_nombre;

     public static function tableName()
    {
        return 'sies.saludmental_practica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_saludmental', 'total_mensual'], 'default', 'value' => null],
            [['id_saludmental', 'total_mensual'], 'integer'],
            [['id_practica'], 'string', 'max' => 20],
            [['id_saludmental'], 'exist', 'skipOnError' => true, 'targetClass' => Saludmental::className(), 'targetAttribute' => ['id_saludmental' => 'id']],
            [['id_practica'], 'exist', 'skipOnError' => true, 'targetClass' => STNomenclador::className(), 'targetAttribute' => ['id_practica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_practica' => 'Código',
            'practica_nombre' => 'Práctica',
            'total_mensual' => 'Total Mensual',
            'saludmentalanio'=> 'anio',
            'area'=>'Area',
            'id_saludmental'=>'Tipo'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('saludmental_practica.id', 10),
			'id_practica' => array('saludmental_practica.id_practica',12),
			'practica_nombre' => array('st_nomenclador.nombre',12),
            'total_mensual' => array('saludmental_practica.total_mensual', 10),
            'saludmentalanio'=>array('saludmental.anio', 10),
            'area' => array('m_area.descrip',12),
            'id_saludmental' => array('st_tipo.nombre',11)
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'area'=>
			[
				'attribute'=>'saludmental.area.descrip',
				'label'=>'Area',
			],
			
            'id_practica',
			'practica_nombre'=>
			[
				'attribute'=>'practica.nombre',
				'label'=>'Práctica',
			],
			'total_mensual',
            'saludmentalanio'=>[
                'attribute'=>'saludmental.anio',
				'label'=>'Año',
            ],
			
            'id_saludmental'=>
			[
				'attribute'=>'saludmental.sttipo.nombre',
				'label'=>'Tipo',
			],
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_practica',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'practica_nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'total_mensual',
			],
            [
                'class'=>'\kartik\grid\DataColumn',
                 'attribute'=>'area',
                 'label'=> 'salud mental area',
                 'value'=>'saludmental.area.descrip',
             ],
             [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'id_saludmental',
                'label'=> 'saludmental tipo',
                'value'=>'saludmental.sttipo.nombre'
            ],
            
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'saludmentalanio',
                'label'=> 'anio',
                'value'=>'saludmental.anio'
            ],
           
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaludmental()
    {
        return $this->hasOne(Saludmental::className(), ['id' => 'id_saludmental']);
    }
    public function getSttipo()
    {
        return $this->getSaludmental()->select('nombre')->join('inner join','sies.st_tipo st','st.id=saludmental.id_st_tipo');
    }
   /* public function getSaludmentalconspv(){
        return $this->getSaludmental()->select('cons_pv');
    }
    public function getSaludmentalconstotal(){
        return $this->getSaludmental()->select('cons_total');
    }
    public function getSaludmentalmes(){
        return $this->getSaludmental()->select('mes');
    }*/
    public function getSaludmentalanio(){
        return $this->getSaludmental()->select('anio');
    }

    public function getArea()
    {
        return $this->getSaludmental()->select('descrip')->join('inner join','sies.m_area ma','ma.id=saludmental.id_area');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractica()
    {
        return $this->hasOne(STNomenclador::className(), ['id' => 'id_practica']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->practica_nombre=$this->practica->nombre;

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
