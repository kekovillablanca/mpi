<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MLocalSearch represents the model behind the search form about `app\models\MLocal`.
 */
class MLocalSearch extends MLocal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'descrip'], 'safe'],
            [['codigo', 'codloc', 'coddepar', 'codprovi', 'codpos', 'codsisa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MLocal::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'codigo',
				'descrip',
				'codloc',
				'coddepar',
				'codprovi',
				'codpos',
				'codsisa',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'm_local.id' => $this->id,
            'm_local.codigo' => $this->codigo,
            'm_local.codloc' => $this->codloc,
            'm_local.coddepar' => $this->coddepar,
            'm_local.codprovi' => $this->codprovi,
            'm_local.codpos' => $this->codpos,
            'm_local.codsisa' => $this->codsisa,
        ]);

        $query->andFilterWhere(['like', 'lower(m_local.descrip)',strtolower($this->descrip)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('mlocal-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
