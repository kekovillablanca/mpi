<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "varios".
 *
 * @property int $id
 * @property int $id_area
 * @property int $mes
 * @property int $anio
 * @property int $id_st_tipo
 * @property int $int
 * @property int $con
 * @property int $gua
 *
 * @property MArea $area
 * @property STTipo $STtipo
 * @property VariosPractica[] $variosPracticas
 */
class Varios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.varios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_area', 'mes', 'int', 'con', 'gua'], 'default', 'value' => null],
            //ingreso required
            [[ 'anio', 'id_st_tipo'], 'required'],
            [['id_area', 'mes', 'anio', 'id_st_tipo', 'int', 'con', 'gua'], 'integer'],
            [['id_area'], 'exist', 'skipOnError' => true, 'targetClass' => MArea::className(), 'targetAttribute' => ['id_area' => 'id']],
            [['id_st_tipo'], 'exist', 'skipOnError' => true, 'targetClass' => STTipo::className(), 'targetAttribute' => ['id_st_tipo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_area' => 'Area',
            'mes' => 'Mes',
            'anio' => 'Año',
            'id_st_tipo' => 'Tipo',
            'int' => 'Pac Internados',
            'con' => 'Pac Consultorio',
            'gua' => 'Pac Guardia',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('varios.id', 10),
			'id_area' => array('m_area.descrip',10),
			'mes' => array('varios.mes', 10),
			'anio' => array('varios.anio', 10),
			'id_st_tipo' => array('st_tipo.nombre',11),
			'int' => array('varios.int', 10),
			'con' => array('varios.con', 10),
			'gua' => array('varios.gua', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_area'=>
			[
				'attribute'=>'area.descrip',
				'label'=>'Area',
			],
			'mes',
			'anio',
			'id_st_tipo'=>
			[
				'attribute'=>'sttipo.nombre',
				'label'=>'Tipo',
			],
			'int',
			'con',
			'gua',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_area',
				'value'=>'area.descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'mes',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'anio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_st_tipo',
				'value'=>'sttipo.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'int',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'con',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'gua',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(MArea::className(), ['id' => 'id_area']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSttipo()
    {
        return $this->hasOne(STTipo::className(), ['id' => 'id_st_tipo']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariospracticas()
    {
        return $this->hasMany(VariosPractica::className(), ['id_varios' => 'id']);
    }



    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
