<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VariosPracticaSearch represents the model behind the search form about `app\models\VariosPractica`.
 */
class VariosPracticaSearch extends VariosPractica
{
    public $variosmes;
    public $variosgua;
    public $variosint;
    public $varioscon;
    public $area;
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_varios', 'id_practica','practica_nombre', 'int', 'cex', 'gua', 'otr', 'der', 'de2','variosint','varioscon','variosgua',
        'variosmes','sttipo','area'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VariosPractica::find();
		$query->joinWith(['practica']);
        $query->joinWith(['varios']);
        $query->joinWith(['varios.area']);
        $query->joinWith(['varios.sttipo']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_practica',
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'sttipo'=>[
					'asc' => ['st_tipo.nombre' => SORT_ASC],
					'desc' => ['st_tipo.nombre' => SORT_DESC],
                ],
                'area'=>[
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
                ],
                'variosmes'=>[
					'asc' => ['varios.mes' => SORT_ASC],
					'desc' => ['varios.mes' => SORT_DESC],
                ],
                'variosanio'=>[
					'asc' => ['varios.anio' => SORT_ASC],
					'desc' => ['varios.anio' => SORT_DESC],
                ],
                'variosgua'=>[
					'asc' => ['varios.gua' => SORT_ASC],
					'desc' => ['varios.gua' => SORT_DESC],
                ], 'varioscon'=>[
					'asc' => ['varios.con' => SORT_ASC],
					'desc' => ['varios.con' => SORT_DESC],
                ]
                , 'variosint'=>[
					'asc' => ['varios.int' => SORT_ASC],
					'desc' => ['varios.int' => SORT_DESC],
                ],
                'int',
				'cex',
				'gua',
				'otr',
				'der',
				'de2',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'varios_practica.id' => $this->id,
            'varios_practica.int' => $this->int,
            'varios_practica.cex' => $this->cex,
            'varios_practica.gua' => $this->gua,
            'varios_practica.otr' => $this->otr,
            'varios_practica.der' => $this->der,
            'varios_practica.de2' => $this->de2,
        ]);

        $query->andFilterWhere(['like', 'lower(varios_practica.id_practica)',strtolower($this->id_practica)])
              ->andFilterWhere(['like', 'lower(st_nomenclador.nombre)',strtolower($this->practica_nombre)])
              ->andFilterWhere(['like', 'cast(varios.int as varchar )',$this->variosint])
              ->andFilterWhere(['like', 'cast(varios.con as varchar )',$this->varioscon])
              ->andFilterWhere(['like', 'cast(varios.gua as varchar )',$this->variosgua])
              ->andFilterWhere(['like', 'lower(area.descrip)',strtolower($this->area)])
              ->andFilterWhere(['like', 'cast(varios.mes as varchar )',$this->variosmes]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('variospractica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
