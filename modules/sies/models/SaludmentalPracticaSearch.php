<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SaludmentalPracticaSearch represents the model behind the search form about `app\models\SaludmentalPractica`.
 */
class SaludmentalPracticaSearch extends SaludmentalPractica
{
    public $area;

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_practica','practica_nombre', 'total_mensual'
            ,'area' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SaludmentalPractica::find();
		$query->joinWith(['practica']);
        $query->joinWith(['saludmental']);
        $query->joinWith(['saludmental.sttipo']);
        $query->joinWith(['saludmental.area']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
                'total_mensual',
				'id_practica',
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'area'=>[
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
                ],
                'id_saludmental'=>[
					'asc' => ['st_tipo.nombre' => SORT_ASC],
					'desc' => ['st_tipo.nombre' => SORT_DESC],
                ],
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],

            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'saludmental_practica.id' => $this->id,
            'saludmental_practica.total_mensual' => $this->total_mensual,
        ]);

        $query->andFilterWhere(['like', 'lower(saludmental_practica.id_practica)',strtolower($this->id_practica)])
              ->andFilterWhere(['like', 'lower(st_nomenclador.nombre)',strtolower($this->practica_nombre)])
              ->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->area)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('saludmentalpractica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
