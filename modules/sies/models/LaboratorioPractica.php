<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "laboratorio_practica".
 *
 * @property int $id
 * @property int $id_laboratorio
 * @property string $id_practica
 * @property int $int
 * @property int $cex
 * @property int $gua
 * @property int $otr
 * @property int $der
 * @property int $de2
 *
 * @property Laboratorio $laboratorio
 * @property STNomenclador $practica
 */
class LaboratorioPractica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $practica_nombre;

     public static function tableName()
    {
        return 'sies.laboratorio_practica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_laboratorio', 'int', 'cex', 'gua', 'otr', 'der', 'de2'], 'default', 'value' => null],
            [['id_laboratorio', 'int', 'cex', 'gua', 'otr', 'der', 'de2'], 'integer'],
            [['id_practica'], 'string', 'max' => 20],
            [['id_laboratorio'], 'exist', 'skipOnError' => true, 'targetClass' => Laboratorio::className(), 'targetAttribute' => ['id_laboratorio' => 'id']],
            [['id_practica'], 'exist', 'skipOnError' => true, 'targetClass' => STNomenclador::className(), 'targetAttribute' => ['id_practica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_practica' => 'Código',
            'practica_nombre' => 'Práctica',
            'int' => 'Interna',
            'cex' => 'Consulta',
            'gua' => 'Guardia',
            'otr' => 'Otros',
            'der' => 'Deriva DE',
            'de2' => 'Deriva A',
            
           /* 'laboratorioint' => 'Int',
            'laboratoriocon' => 'Con',
            'laboratoriogua' => 'Gua',*/
            
            'laboratorioanio'=> 'Año',
            'sttipo'=>'tipo',
            'area' =>'area',
            'laboratoriomes'=> 'Mes',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('laboratorio_practica.id', 10),
			'id_practica' => array('laboratorio_practica.id_practica',12),
			'practica_nombre' => array('st_nomenclador.nombre',12),
			'int' => array('laboratorio_practica.int', 10),
			'cex' => array('laboratorio_practica.cex', 10),
			'gua' => array('laboratorio_practica.gua', 10),
			'otr' => array('laboratorio_practica.otr', 10),
			'der' => array('laboratorio_practica.der', 10),
			'de2' => array('laboratorio_practica.de2', 10),
            
            'laboratoriomes' => array('sies.laboratorio.mes', 10),
            /*'laboratorioint'=>array('laboratorio.int', 10),
            'laboratoriocon'=>array('laboratorio.con', 10),
            'laboratoriogua'=>array('laboratorio.gua', 10),*/
            'area' => array('m_area.descrip',12),
            'sttipo' => array('st_tipo.nombre',11),
            'laboratorioanio'=>array('laboratorio.anio', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			
			'laboratorioanio'=>
			[
				'attribute'=>'laboratorio.anio',
				'label'=>'Año',
			],
            /*'laboratorioint'=>
			[
				'attribute'=>'laboratorio.int',
				'label'=>'Int',
			],
            'laboratoriogua'=>
			[
				'attribute'=>'laboratorio.gua',
				'label'=>'Gua',
			],
            'laboratoriocon'=>
			[
				'attribute'=>'laboratorio.con',
				'label'=>'Con',
			],*/
           
		
			'sttipo'=>
			[
				'attribute'=>'laboratorio.sttipo.nombre',
				'label'=>'Tipo',
			],
            'id_practica',
			'practica_nombre'=>
			[
				'attribute'=>'practica.nombre',
				'label'=>'Práctica',
			],
			'int',
			'cex',
			'gua',
			'otr',
			'der',
			'de2',	
            /*'laboratoriomes'=>
			[
				'attribute'=>'laboratorio.mes',
				'label'=>'Mes',
			],*/
            'area'=>
			[
				'attribute'=>'laboratorio.area.descrip',
				'label'=>'Area',
			],
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_practica',
			],
          
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'practica_nombre',
//				'value'=>'practica.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'int',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cex',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'gua',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otr',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'der',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'de2',
			], 
            
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'laboratorioanio',
                'label'=> 'Año',
                'value'=>'laboratorio.anio'
            ],
            /*[
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'laboratorioint',
                'label'=> 'Laboratorio int',
                'value'=>'laboratorio.int'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'laboratoriocon',
                'label'=> 'Laboratorio con',
                'value'=>'laboratorio.con'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'laboratoriogua',
                'label'=> 'Laboratorio gua',
                'value'=>'laboratorio.gua'
            ],*/
          
             [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'sttipo',
                'label'=> 'Laboratorio tipo',
                'value'=>'laboratorio.sttipo.nombre'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                 'attribute'=>'area',
                 'value'=>'laboratorio.area.descrip'
                 
             ],
            /* [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'laboratoriomes',
                'value'=>'laboratorio.mes'
                
            ],*/
           
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLaboratorio()
    {
        return $this->hasOne(Laboratorio::className(), ['id' => 'id_laboratorio']);
    }
  
    public function getLaboratoriomes(){
        return $this->getLaboratorio()->select('mes');
    }
    public function getLaboratorioanio(){
        return $this->getLaboratorio()->select('anio');
    }
    public function getLaboratorioint(){
        return $this->getLaboratorio()->select('int');
    }
    public function getLaboratoriocon(){
        return $this->getLaboratorio()->select('con');
    }
    public function getLaboratoriogua(){
        return $this->getLaboratorio()->select('gua');
    }
    public function getArea()
    {
        return $this->getLaboratorio()->select('descrip')->join('inner join','sies.m_area ma','ma.id=laboratorio.id_area');
    }
    public function getSttipo()
    {
        return $this->getLaboratorio()->select('nombre')->join('inner join','sies.st_tipo st','st.id=laboratorio.id_st_tipo');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractica()
    {
        return $this->hasOne(STNomenclador::className(), ['id' => 'id_practica']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->practica_nombre=$this->practica->nombre;

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}