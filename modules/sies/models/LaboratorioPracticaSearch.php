<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LaboratorioPracticaSearch represents the model behind the search form about `app\models\LaboratorioPractica`.
 */
class LaboratorioPracticaSearch extends LaboratorioPractica
{
    public $area;
    public $laboratoriomes;
    public $laboratorioint;
    public $laboratoriocon;
    public $laboratoriogua;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        
        return [
            [['int', 'cex', 'gua', 'otr', 'der', 'de2'],'integer'],
            [['id', 'id_laboratorio', 'id_practica','practica_nombre','area','sttipo',
            'laboratoriomes','laboratorioint','laboratoriocon','laboratoriogua' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

  
        $query = LaboratorioPractica::find();
		$query->joinWith(['practica']);
        $query->joinWith(['laboratorio']);
        $query->joinWith(['laboratorio.area']);
        $query->joinWith(['laboratorio.sttipo']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
       

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_practica',
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'laboratorioanio'=>[
					'asc' => ['laboratorio.anio' => SORT_ASC],
					'desc' => ['laboratorio.anio' => SORT_DESC],
                ],
                'laboratoriomes'=>[
					'asc' => ['laboratorio.mes' => SORT_ASC],
					'desc' => ['laboratorio.mes' => SORT_DESC],
                ],
               
                'laboratorioint'=>[
					'asc' => ['laboratorio.int' => SORT_ASC],
					'desc' => ['laboratorio.int' => SORT_DESC],
                ],
                'laboratoriogua'=>[
					'asc' => ['laboratorio.gua' => SORT_ASC],
					'desc' => ['laboratorio.gua' => SORT_DESC],
                ],
                'laboratoriocon'=>[
					'asc' => ['laboratorio.con' => SORT_ASC],
					'desc' => ['laboratorio.con' => SORT_DESC],
                ],
                'sttipo'=>[
                    'asc' => ['st_tipo.nombre' => SORT_ASC],
					'desc' => ['st_tipo.nombre' => SORT_DESC],
                ],
                'area'=>[
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
                ],
                'int',
				'cex',
				'gua',
				'otr',
				'der',
				'de2',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'laboratorio_practica.id' => $this->id,
            'laboratorio_practica.int' => $this->int,
            'laboratorio_practica.cex' => $this->cex,
            'laboratorio_practica.gua' => $this->gua,
            'laboratorio_practica.otr' => $this->otr,
            'laboratorio_practica.der' => $this->der,
            'laboratorio_practica.de2' => $this->de2,
            
        ]);

        $query->andFilterWhere(['like', 'lower(laboratorio_practica.id_practica)',strtolower($this->id_practica)])
              ->andFilterWhere(['like', 'lower(st_nomenclador.nombre)',strtolower($this->practica_nombre)])
              ->andFilterWhere(['like','CAST(laboratorio.int AS varchar)',$this->laboratorioint])
              ->andFilterWhere(['like','CAST(laboratorio.con AS varchar)',$this->laboratoriocon])
              ->andFilterWhere(['like','CAST(laboratorio.gua AS varchar)',$this->laboratoriogua])
              ->andFilterWhere(['like','lower(m_area.descrip)',strtolower($this->area)])
              ->andFilterWhere(['like','CAST(laboratorio.mes AS varchar)',$this->laboratoriomes]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('laboratoriopractica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
