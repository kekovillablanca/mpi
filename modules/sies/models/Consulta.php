<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "consulta".
 *
 * @property int $id
 * @property string $numero
 * @property string $hospital
 * @property string $puesto
 * @property string $servicio
 * @property string $medico
 * @property string $fecha
 * @property string $nrohc
 * @property string $nrodoc
 * @property string $apellido
 * @property string $edad
 * @property string $cdgosexo
 * @property string $obra
 * @property string $primera
 * @property string $cemb
 * @property string $cns
 * @property string $diag
 */
class Consulta extends \yii\db\ActiveRecord
{

	public $anio='2021';  // default
	public $archivo_upload;

	public static $table;
	
	/**
     * {@inheritdoc}
     */

	public static function tableName()
	{
		return "sies.".self::$table;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero', 'nrohc', 'nrodoc', 'edad', 'obra', 'primera', 'cemb', 'cns'], 'number'],
            [['fecha'], 'safe'],
            [['hospital', 'puesto'], 'string', 'max' => 20],
            [['servicio', 'medico'], 'string', 'max' => 10],
            [['apellido'], 'string', 'max' => 30],
            [['cdgosexo'], 'string', 'max' => 1],
            [['diag'], 'string', 'max' => 5],
            [['archivo_upload'], 'safe'],
            [['archivo_upload'], 'file','extensions' => 'cmp, CMP'],
            [['archivo_upload'], 'file', 'maxSize'=>'10485760'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'hospital' => 'Hospital',
            'puesto' => 'Puesto',
            'servicio' => 'Servicio',
            'medico' => 'Medico',
            'fecha' => 'Fecha',
            'nrohc' => 'Nro HC',
            'nrodoc' => 'Nro Doc',
            'apellido' => 'Apellido Nombre',
            'edad' => 'Edad',
            'cdgosexo' => 'Sexo',
            'obra' => 'OS',
            'primera' => 'Primera',
            'cemb' => 'Ctrl Emb',
            'cns' => 'Ctrl NS',
            'diag' => 'Diagnostico',
			'archivo_upload' => 'Archivo',

        ];
    }
    public function attributePrint()
    {

		$this->anio=substr($this->tableName(),-4);
		
		return [
			'id' => array('consulta'.$this->anio.'.id', 10),
			'numero' => array('consulta'.$this->anio.'.numero', 10),
			'hospital' => array('consulta'.$this->anio.'.hospital', 20),
			'puesto' => array('consulta'.$this->anio.'.puesto', 20),
			'servicio' => array('consulta'.$this->anio.'.servicio', 10),
			'medico' => array('consulta'.$this->anio.'.medico', 10),
			'fecha' => array('consulta'.$this->anio.'.fecha', 10),
			'nrohc' => array('consulta'.$this->anio.'.nrohc', 10),
			'nrodoc' => array('consulta'.$this->anio.'.nrodoc', 10),
			'apellido' => array('consulta'.$this->anio.'.apellido', 20),
			'edad' => array('consulta'.$this->anio.'.edad', 10),
			'cdgosexo' => array('consulta'.$this->anio.'.cdgosexo', 9),
			'obra' => array('consulta'.$this->anio.'.obra', 10),
			'primera' => array('consulta'.$this->anio.'.primera', 10),
			'cemb' => array('consulta'.$this->anio.'.cemb', 10),
			'cns' => array('consulta'.$this->anio.'.cns', 10),
			'diag' => array('consulta'.$this->anio.'.diag', 5),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'numero',
			'hospital',
			'puesto',
			'servicio',
			'medico',
			'fecha',
			'nrohc',
			'nrodoc',
			'apellido',
			'edad',
			'cdgosexo',
			'obra',
			'primera',
			'cemb',
			'cns',
			'diag',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'hospital',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'puesto',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'medico',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nrohc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nrodoc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'apellido',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'edad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cdgosexo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'obra',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'primera',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cemb',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cns',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diag',
			],
        ];
    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha=Metodos::dateConvert($this->fecha,'View');
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha=Metodos::dateConvert($this->fecha,'toSql');
            return true;
        } else {
            return false;
        }
    }

	public function createCsv(){
		$connection = Yii::$app->db; 
        $query=$connection->createCommand("SELECT i.codprov,i.anio,i.numero_internacion,i.edad,i.fecha_ingreso,i.fecha_nacimiento,i.fecha_egreso,i.dias_estado,i.dias_internado,i.centro,
        i.centro_codigo,i.nombre_apellido,i.dni,i.numero_dni,i.trabaja,i.obra_social,i.plan,i.nivel_estudio,i.tipo_egreso,i.codigo_egreso,i.diagnostico_principal,i.codigo_diagnostico,
        d.codigo,i.diagnostico_de_episodio,i.profesional,i.servicio,i.especialidad,i.diagnostico_2,i.origen_codigo_internacion,i.lugar_ocurrido,i.producido_por,i.fecha_pase,i.provincia,
        i.departamento_descripcion,i.pais_paciente,i.sexo,i.codigo_provincia,i.pais_residencia,i.numero_hc
        FROM hospitalviedma.internacion i left join consultasestadisticas.diagnostico d 
        on (i.diagnostico_principal =d.nombre OR i.diagnostico_2=d.nombre);")->queryAll();
        $csv = 'codprov,anio,numero_internacion,edad,fecha_ingreso,fecha_nacimiento,fecha_egreso,dias_estado,dias_internado,centro,centro_codigo,nombre_apellido,dni,numero_dni,trabaja,obra_social,plan,nivel_estudio,tipo_egreso,codigo_egreso,diagnostico_principal,codigo_diagnostico,diagnostico_de_episodio,profesional,servicio,especialidad,diagnostico_2,origen_codigo_internacion,lugar_ocurrido,producido_por,fecha_pase,provincia,departamento_descripcion,pais_paciente,sexo,codigo_provincia,pais_residencia,numero_hc,diagnostico_codigo_real';
		$delimiter = ',';
		$filename = 'reporteinternacion.csv';
		
		// Abrir el archivo en memoria
		$f = fopen('php://memory', 'w');
		
		// Escribir las cabeceras CSV
		$fields = [
			'codprov','anio','numero_internacion','edad','fecha_ingreso','fecha_nacimiento','fecha_egreso','dias_estado','dias_internado','centro','centro_codigo','nombre_apellido',
			'dni','numero_dni','trabaja','obra_social','plan','nivel_estudio','tipo_egreso','codigo_egreso','diagnostico_principal','codigo_diagnostico','diagnostico_de_episodio',
			'profesional','servicio','especialidad','diagnostico_2','origen_codigo_internacion','lugar_ocurrido','producido_por','fecha_pase','provincia','departamento_descripcion',
			'pais_paciente','sexo','codigo_provincia','pais_residencia','numero_hc','diagnostico_codigo'
		];
		fputcsv($f, $fields, $delimiter);
		
		// Escribir los datos de cada fila en el archivo CSV
		foreach($query as $row) {
			$lineData = [
				$row['codprov'],$row['anio'],$row['numero_internacion'],$row['edad'],$row['fecha_ingreso'],$row['fecha_nacimiento'],$row['fecha_egreso'],$row['dias_estado'],
				$row['dias_internado'],$row['centro'],$row['centro_codigo'],$row['nombre_apellido'],$row['dni'],$row['numero_dni'],$row['trabaja'],$row['obra_social'],
				$row['plan'],$row['nivel_estudio'],$row['tipo_egreso'],$row['codigo_egreso'],$row['diagnostico_principal'],$row['codigo_diagnostico'],$row['diagnostico_de_episodio'],
				$row['profesional'],$row['servicio'],$row['especialidad'],$row['diagnostico_2'],$row['origen_codigo_internacion'],$row['lugar_ocurrido'],$row['producido_por'],
				$row['fecha_pase'],$row['provincia'],$row['departamento_descripcion'],$row['pais_paciente'],$row['sexo'],$row['codigo_provincia'],$row['pais_residencia'],
				$row['numero_hc'],$row['codigo']
			];
			fputcsv($f, $lineData, $delimiter);
		}

		// Rebobinar el puntero del archivo al principio
		fseek($f, 0);
    
		//set headers to download file rather than displayed
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '";');
		
		//output all remaining data on a file pointer
		fpassthru($f);
		
		return $f;
	}


}
