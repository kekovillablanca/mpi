<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "m_puesto".
 *
 * @property int $id
 * @property string $codigo
 * @property string $descrip
 * @property string $codpuesto
 * @property string $codzona
 * @property string $codarea
 * @property string $area
 * @property string $zona
 * @property bool $nacion
 * @property string $codsisa
 */
class MPuesto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.m_puesto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'codpuesto', 'codzona', 'codarea'], 'number'],
            [['nacion'], 'boolean'],
            [['descrip', 'area'], 'string', 'max' => 20],
            [['zona'], 'string', 'max' => 12],
            [['codsisa'], 'string', 'max' => 14],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'descrip' => 'Descripcion',
            'codpuesto' => 'Cod Puesto',
            'codzona' => 'Cod Zona',
            'codarea' => 'Cod Area',
            'area' => 'Area',
            'zona' => 'Zona',
            'nacion' => 'Nacion',
            'codsisa' => 'Cod SISA',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('m_puesto.id', 10),
			'codigo' => array('m_puesto.codigo', 10),
			'descrip' => array('m_puesto.descrip', 20),
			'codpuesto' => array('m_puesto.codpuesto', 10),
			'codzona' => array('m_puesto.codzona', 10),
			'codarea' => array('m_puesto.codarea', 10),
			'area' => array('m_puesto.area', 20),
			'zona' => array('m_puesto.zona', 12),
			'nacion' => array('m_puesto.nacion', 10),
			'codsisa' => array('m_puesto.codsisa', 14),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'codigo',
			'descrip',
			'codpuesto',
			'codzona',
			'codarea',
			'area',
			'zona',
			'nacion',
			'codsisa',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codpuesto',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codzona',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codarea',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'area',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'zona',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nacion',
				'format'=>'boolean',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codsisa',
			],
        ];
    }




    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
