<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "morbimor".
 *
 * @property int $id
 * @property string $numero
 * @property string $historiacl
 * @property string $establec
 * @property string $apellidos
 * @property string $ndedocumen
 * @property string $tipo
 * @property string $fechadenac
 * @property string $edading
 * @property string $sexo
 * @property string $locresid
 * @property string $depresid
 * @property string $provresid
 * @property string $paisresid
 * @property string $tieneobras
 * @property string $codobrasoc
 * @property string $codhospit
 * @property string $fechaingr
 * @property string $servicio
 * @property string $diasest
 * @property string $fechapase
 * @property string $servicio01
 * @property string $diasest01
 * @property string $fechapas01
 * @property string $servicio02
 * @property string $diasest02
 * @property string $fechapas02
 * @property string $servicio03
 * @property string $diasest03
 * @property string $fechapas03
 * @property string $servicio04
 * @property string $diasest04
 * @property string $fechaegres
 * @property string $totdiasest
 * @property string $egresopor
 * @property string $operado
 * @property string $diagprinc
 * @property string $otrodiag
 * @property string $otrodiag1
 * @property string $otrodiag2
 * @property string $otrodiag3
 * @property string $producidop
 * @property string $operimport
 * @property string $lugardonde
 * @property string $otraoper
 * @property string $otraoper1
 * @property string $codigodele
 * @property string $otrascir
 * @property string $diascir
 * @property string $termina
 * @property string $tparto
 * @property string $totctrls
 * @property string $condnacer
 * @property string $sexonaci
 * @property string $peso
 * @property string $condnace01
 * @property string $sexonaci01
 * @property string $peso01
 * @property string $condnace02
 * @property string $sexonaci02
 * @property string $peso02
 * @property string $estudios
 * @property string $trabajos
 * @property string $trabajtxt
 * @property string $htalorig
 * @property string $htaldest
 * @property string $nrodocmad
 * @property string $fechaparto
 * @property string $edadgest
 * @property string $paridad
 * @property string $parto
 * @property string $horaingr
 * @property string $horapase
 * @property string $horapase01
 * @property string $horapase02
 * @property string $horapase03
 * @property string $horaegres
 * @property string $servaux
 * @property string $servaux01
 * @property string $servaux02
 * @property string $servaux03
 * @property string $servaux04
 */
class Morbimor extends \yii\db\ActiveRecord
{
	public $anio='2023';  // default

	public static $table="morbimor";

	/**
     * {@inheritdoc}
     */
	public static function tableName()
	{
		return "sies.".self::$table;
		
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero', 'historiacl', 'ndedocumen', 'tipo', 'edading', 'locresid', 'depresid', 'provresid', 'paisresid', 'tieneobras', 'codobrasoc', 'codhospit', 'diasest', 'diasest01', 'diasest02', 'diasest03', 'diasest04', 'totdiasest', 'egresopor', 'operado', 'diascir', 'totctrls', 'peso', 'peso01', 'peso02', 'estudios', 'trabajos', 'htalorig', 'htaldest', 'nrodocmad', 'edadgest', 'paridad', 'parto', 'horaingr', 'horapase', 'horapase01', 'horapase02', 'horapase03', 'horaegres', 'servaux', 'servaux01', 'servaux02', 'servaux03', 'servaux04'], 'number'],
            [['fechadenac', 'fechaingr', 'fechapase', 'fechapas01', 'fechapas02', 'fechapas03', 'fechaegres', 'fechaparto'], 'safe'],
            [['establec', 'otrascir', 'trabajtxt'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 30],
            [['sexo', 'termina', 'tparto', 'condnacer', 'sexonaci', 'condnace01', 'sexonaci01', 'condnace02', 'sexonaci02'], 'string', 'max' => 1],
            [['servicio', 'servicio01', 'servicio02', 'servicio03', 'servicio04'], 'string', 'max' => 10],
            [['diagprinc', 'otrodiag', 'otrodiag1', 'otrodiag2', 'otrodiag3', 'operimport', 'otraoper', 'otraoper1', 'codigodele'], 'string', 'max' => 5],
            [['producidop', 'lugardonde'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'historiacl' => 'Nro HC',
            'establec' => 'Hospital',
            'apellidos' => 'Apellido Nombre',
            'ndedocumen' => 'Nro Doc',
            'tipo' => 'Tipo Doc',
            'fechadenac' => 'Fecha Nac',
            'edading' => 'Edad',
            'sexo' => 'Sexo',
            'locresid' => 'Localidad',
            'depresid' => 'Departamento',
            'provresid' => 'Provincia',
            'paisresid' => 'Pais',
            'tieneobras' => 'Obra Social',
            'codobrasoc' => 'Cod OS',
            'codhospit' => 'Cod Hosp',
            'fechaingr' => 'Fecha ingreso',
            'servicio' => 'Servicio',
            'diasest' => 'Dias estada',
            'fechapase' => 'Fecha pase',
            'servicio01' => 'Servicio 01',
            'diasest01' => 'Dias estada 01',
            'fechapas01' => 'Fecha pase 01',
            'servicio02' => 'Servicio 02',
            'diasest02' => 'Dias estada 02',
            'fechapas02' => 'Fecha pase 02',
            'servicio03' => 'Servicio 03',
            'diasest03' => 'Dias estada 03',
            'fechapas03' => 'Fecha pase 03',
            'servicio04' => 'Servici o04',
            'diasest04' => 'Dias estada 04',
            'fechaegres' => 'Fecha egreso',
            'totdiasest' => 'Total dias',
            'egresopor' => 'Egreso',
            'operado' => 'Operacion',
            'diagprinc' => 'Diag principal',
            'otrodiag' => 'Otro Diag',
            'otrodiag1' => 'Otro Diag 1',
            'otrodiag2' => 'Otro Diag 2',
            'otrodiag3' => 'Otro Diag 3',
            'producidop' => 'Producido',
            'operimport' => 'Proc Quirurgico',
            'lugardonde' => 'Lugar',
            'otraoper' => 'Otro Proc 1',
            'otraoper1' => 'Otro Proc 2',
            'codigodele' => 'Codigo Prod',
            'otrascir' => 'Otras Cir',
            'diascir' => 'Dias Cir',
            'termina' => 'Terminacion',
            'tparto' => 'Tipo Parto',
            'totctrls' => 'Total Ctrl',
            'condnacer' => 'Cond Nacer',
            'sexonaci' => 'Sexo Nac',
            'peso' => 'Peso',
            'condnace01' => 'Cond Nac 01',
            'sexonaci01' => 'Sexo Nac 01',
            'peso01' => 'Peso 01',
            'condnace02' => 'Cond Nac 02',
            'sexonaci02' => 'Sexo Nac 02',
            'peso02' => 'Peso 02',
            'estudios' => 'Estudios',
            'trabajos' => 'Trabajos',
            'trabajtxt' => 'Desc Trabajo',
            'htalorig' => 'Htal orig',
            'htaldest' => 'Htal dest',
            'nrodocmad' => 'Nro Doc Madre',
            'fechaparto' => 'Fecha Parto',
            'edadgest' => 'Edad Gest',
            'paridad' => 'Paridad',
            'parto' => 'Parto',
            'horaingr' => 'Hora Ingr',
            'horapase' => 'Hora Pase',
            'horapase01' => 'Hora Pase 01',
            'horapase02' => 'Hora Pase 02',
            'horapase03' => 'Hora Pase 03',
            'horaegres' => 'Hora Egreso',
            'servaux' => 'Serv Aux',
            'servaux01' => 'Serv Aux 01',
            'servaux02' => 'Serv Aux 02',
            'servaux03' => 'Serv Aux 03',
            'servaux04' => 'Serv Aux 04',
        ];
    }
    public function attributePrint()
    {
		$this->anio=substr($this->tableName(),-4);

		return [
			'id' => array('morbimor'.$this->anio.'.id', 10),
			'numero' => array('morbimor'.$this->anio.'.numero', 10),
			'historiacl' => array('morbimor'.$this->anio.'.historiacl', 11),
			'establec' => array('morbimor'.$this->anio.'.establec', 20),
			'apellidos' => array('morbimor'.$this->anio.'.apellidos', 20),
			'ndedocumen' => array('morbimor'.$this->anio.'.ndedocumen', 11),
			'tipo' => array('morbimor'.$this->anio.'.tipo', 10),
			'fechadenac' => array('morbimor'.$this->anio.'.fechadenac', 11),
			'edading' => array('morbimor'.$this->anio.'.edading', 10),
			'sexo' => array('morbimor'.$this->anio.'.sexo', 5),
			'locresid' => array('morbimor'.$this->anio.'.locresid', 10),
			'depresid' => array('morbimor'.$this->anio.'.depresid', 10),
			'provresid' => array('morbimor'.$this->anio.'.provresid', 10),
			'paisresid' => array('morbimor'.$this->anio.'.paisresid', 10),
			'tieneobras' => array('morbimor'.$this->anio.'.tieneobras', 11),
			'codobrasoc' => array('morbimor'.$this->anio.'.codobrasoc', 11),
			'codhospit' => array('morbimor'.$this->anio.'.codhospit', 10),
			'fechaingr' => array('morbimor'.$this->anio.'.fechaingr', 10),
			'servicio' => array('morbimor'.$this->anio.'.servicio', 10),
			'diasest' => array('morbimor'.$this->anio.'.diasest', 10),
			'fechapase' => array('morbimor'.$this->anio.'.fechapase', 10),
			'servicio01' => array('morbimor'.$this->anio.'.servicio01', 11),
			'diasest01' => array('morbimor'.$this->anio.'.diasest01', 10),
			'fechapas01' => array('morbimor'.$this->anio.'.fechapas01', 11),
			'servicio02' => array('morbimor'.$this->anio.'.servicio02', 11),
			'diasest02' => array('morbimor'.$this->anio.'.diasest02', 10),
			'fechapas02' => array('morbimor'.$this->anio.'.fechapas02', 11),
			'servicio03' => array('morbimor'.$this->anio.'.servicio03', 11),
			'diasest03' => array('morbimor'.$this->anio.'.diasest03', 10),
			'fechapas03' => array('morbimor'.$this->anio.'.fechapas03', 11),
			'servicio04' => array('morbimor'.$this->anio.'.servicio04', 11),
			'diasest04' => array('morbimor'.$this->anio.'.diasest04', 10),
			'fechaegres' => array('morbimor'.$this->anio.'.fechaegres', 11),
			'totdiasest' => array('morbimor'.$this->anio.'.totdiasest', 11),
			'egresopor' => array('morbimor'.$this->anio.'.egresopor', 10),
			'operado' => array('morbimor'.$this->anio.'.operado', 10),
			'diagprinc' => array('morbimor'.$this->anio.'.diagprinc', 10),
			'otrodiag' => array('morbimor'.$this->anio.'.otrodiag', 9),
			'otrodiag1' => array('morbimor'.$this->anio.'.otrodiag1', 10),
			'otrodiag2' => array('morbimor'.$this->anio.'.otrodiag2', 10),
			'otrodiag3' => array('morbimor'.$this->anio.'.otrodiag3', 10),
			'producidop' => array('morbimor'.$this->anio.'.producidop', 11),
			'operimport' => array('morbimor'.$this->anio.'.operimport', 11),
			'lugardonde' => array('morbimor'.$this->anio.'.lugardonde', 11),
			'otraoper' => array('morbimor'.$this->anio.'.otraoper', 9),
			'otraoper1' => array('morbimor'.$this->anio.'.otraoper1', 10),
			'codigodele' => array('morbimor'.$this->anio.'.codigodele', 11),
			'otrascir' => array('morbimor'.$this->anio.'.otrascir', 20),
			'diascir' => array('morbimor'.$this->anio.'.diascir', 10),
			'termina' => array('morbimor'.$this->anio.'.termina', 8),
			'tparto' => array('morbimor'.$this->anio.'.tparto', 7),
			'totctrls' => array('morbimor'.$this->anio.'.totctrls', 10),
			'condnacer' => array('morbimor'.$this->anio.'.condnacer', 10),
			'sexonaci' => array('morbimor'.$this->anio.'.sexonaci', 9),
			'peso' => array('morbimor'.$this->anio.'.peso', 10),
			'condnace01' => array('morbimor'.$this->anio.'.condnace01', 11),
			'sexonaci01' => array('morbimor'.$this->anio.'.sexonaci01', 11),
			'peso01' => array('morbimor'.$this->anio.'.peso01', 10),
			'condnace02' => array('morbimor'.$this->anio.'.condnace02', 11),
			'sexonaci02' => array('morbimor'.$this->anio.'.sexonaci02', 11),
			'peso02' => array('morbimor'.$this->anio.'.peso02', 10),
			'estudios' => array('morbimor'.$this->anio.'.estudios', 10),
			'trabajos' => array('morbimor'.$this->anio.'.trabajos', 10),
			'trabajtxt' => array('morbimor'.$this->anio.'.trabajtxt', 20),
			'htalorig' => array('morbimor'.$this->anio.'.htalorig', 10),
			'htaldest' => array('morbimor'.$this->anio.'.htaldest', 10),
			'nrodocmad' => array('morbimor'.$this->anio.'.nrodocmad', 10),
			'fechaparto' => array('morbimor'.$this->anio.'.fechaparto', 11),
			'edadgest' => array('morbimor'.$this->anio.'.edadgest', 10),
			'paridad' => array('morbimor'.$this->anio.'.paridad', 10),
			'parto' => array('morbimor'.$this->anio.'.parto', 10),
			'horaingr' => array('morbimor'.$this->anio.'.horaingr', 10),
			'horapase' => array('morbimor'.$this->anio.'.horapase', 10),
			'horapase01' => array('morbimor'.$this->anio.'.horapase01', 11),
			'horapase02' => array('morbimor'.$this->anio.'.horapase02', 11),
			'horapase03' => array('morbimor'.$this->anio.'.horapase03', 11),
			'horaegres' => array('morbimor'.$this->anio.'.horaegres', 10),
			'servaux' => array('morbimor'.$this->anio.'.servaux', 10),
			'servaux01' => array('morbimor'.$this->anio.'.servaux01', 10),
			'servaux02' => array('morbimor'.$this->anio.'.servaux02', 10),
			'servaux03' => array('morbimor'.$this->anio.'.servaux03', 10),
			'servaux04' => array('morbimor'.$this->anio.'.servaux04', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'numero',
			'historiacl',
			'establec',
			'apellidos',
			'ndedocumen',
			'tipo',
			'fechadenac',
			'edading',
			'sexo',
			'locresid',
			'depresid',
			'provresid',
			'paisresid',
			'tieneobras',
			'codobrasoc',
			'codhospit',
			'fechaingr',
			'servicio',
			'diasest',
			'fechapase',
			'servicio01',
			'diasest01',
			'fechapas01',
			'servicio02',
			'diasest02',
			'fechapas02',
			'servicio03',
			'diasest03',
			'fechapas03',
			'servicio04',
			'diasest04',
			'fechaegres',
			'totdiasest',
			'egresopor',
			'operado',
			'diagprinc',
			'otrodiag',
			'otrodiag1',
			'otrodiag2',
			'otrodiag3',
			'producidop',
			'lugardonde',
			'operimport',
			'otraoper',
			'otraoper1',
			'codigodele',
			'otrascir',
			'diascir',
			'termina',
			'tparto',
			'totctrls',
			'condnacer',
			'sexonaci',
			'peso',
			'condnace01',
			'sexonaci01',
			'peso01',
			'condnace02',
			'sexonaci02',
			'peso02',
			'fechaparto',
			'edadgest',
			'paridad',
			'parto',
			'nrodocmad',
			'estudios',
			'trabajos',
			'trabajtxt',
			'htalorig',
			'htaldest',
			'horaingr',
			'horapase',
			'horapase01',
			'horapase02',
			'horapase03',
			'horaegres',
			'servaux',
			'servaux01',
			'servaux02',
			'servaux03',
			'servaux04',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'historiacl',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'establec',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'apellidos',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'ndedocumen',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'tipo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechadenac',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'edading',
			 ],
			 [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'sexo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'locresid',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'depresid',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'provresid',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paisresid',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'tieneobras',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codobrasoc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codhospit',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechaingr',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diasest',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechapase',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diasest01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechapas01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diasest02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechapas02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio03',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diasest03',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechapas03',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servicio04',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diasest04',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechaegres',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'totdiasest',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'egresopor',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'operado',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diagprinc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otrodiag',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otrodiag1',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otrodiag2',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otrodiag3',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'producidop',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'lugardonde',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'operimport',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otraoper',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otraoper1',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigodele',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otrascir',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'diascir',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'termina',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'tparto',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'totctrls',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'condnacer',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'sexonaci',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'peso',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'condnace01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'sexonaci01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'peso01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'condnace02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'sexonaci02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'peso02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fechaparto',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'edadgest',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paridad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'parto',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nrodocmad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'estudios',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'trabajos',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'trabajtxt',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'htalorig',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'htaldest',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'horaingr',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'horapase',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'horapase01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'horapase02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'horapase03',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'horaegres',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servaux',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servaux01',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servaux02',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servaux03',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'servaux04',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fechadenac=Metodos::dateConvert($this->fechadenac,'View');
		$this->fechaingr=Metodos::dateConvert($this->fechaingr,'View');
		$this->fechapase=Metodos::dateConvert($this->fechapase,'View');
		$this->fechapas01=Metodos::dateConvert($this->fechapas01,'View');
		$this->fechapas02=Metodos::dateConvert($this->fechapas02,'View');
		$this->fechapas03=Metodos::dateConvert($this->fechapas03,'View');
		$this->fechaegres=Metodos::dateConvert($this->fechaegres,'View');
		$this->fechaparto=Metodos::dateConvert($this->fechaparto,'View');
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fechadenac=Metodos::dateConvert($this->fechadenac,'toSql');
			$this->fechaingr=Metodos::dateConvert($this->fechaingr,'toSql');
			$this->fechapase=Metodos::dateConvert($this->fechapase,'toSql');
			$this->fechapas01=Metodos::dateConvert($this->fechapas01,'toSql');
			$this->fechapas02=Metodos::dateConvert($this->fechapas02,'toSql');
			$this->fechapas03=Metodos::dateConvert($this->fechapas03,'toSql');
			$this->fechaegres=Metodos::dateConvert($this->fechaegres,'toSql');
			$this->fechaparto=Metodos::dateConvert($this->fechaparto,'toSql');
            return true;
        } else {
            return false;
        }
    }
}
