<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "odontologia_practica".
 *
 * @property int $id
 * @property int $id_odontologia
 * @property string $id_practica
 * @property int $cantidad_practica
 *
 * @property Odontologia $odontologia
 * @property STNomenclador $practica
 */
class OdontologiaPractica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $practica_nombre;

     public static function tableName()
    {
        return 'sies.odontologia_practica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_odontologia', 'cantidad_practica', ], 'default', 'value' => null],
            [['id_odontologia', 'cantidad_practica', ], 'integer'],
            [['id_practica'], 'string', 'max' => 20],
            [['id_odontologia'], 'exist', 'skipOnError' => true, 'targetClass' => Odontologia::className(), 'targetAttribute' => ['id_odontologia' => 'id']],
            [['id_practica'], 'exist', 'skipOnError' => true, 'targetClass' => STNomenclador::className(), 'targetAttribute' => ['id_practica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_practica' => 'Código',
            'practica_nombre' => 'Práctica',
            'cantidad_practica' => 'Cantidad Prácticas',
            'area'=> 'Area',
            'sttipo'=>'tipo',
            'odontologiaanio'=>'Anio'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('odontologia_practica.id', 10),
			'id_practica' => array('odontologia_practica.id_practica',12),
			'practica_nombre' => array('st_nomenclador.nombre',12),
			'cantidad_practica' => array('odontologia_practica.cantidad_practica', 10),
            'sttipo' => array('st_tipo.nombre',11),
            'area' => array('m_area.descrip',12),
            'odontologiaanio'=> array('odontologia.anio', 12),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'area'=>
			[
				'attribute'=>'odontologia.area.descrip',
				'label'=>'Area',
			],
			'odontologiaanio'=>
			[
				'attribute'=>'odontologia.anio',
				'label'=>'Año',
			],
			'mes'=>
			[
				'attribute'=>'odontologia.mes',
				'label'=>'Mes',
			],
			'sttipo'=>
			[
				'attribute'=>'odontologia.sttipo.nombre',
				'label'=>'Tipo',
			],
            'id_practica',
			'practica_nombre'=>
			[
				'attribute'=>'practica.nombre',
				'label'=>'Práctica',
			],
			'cantidad_practica',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_practica',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'practica_nombre',
//				'value'=>'practica.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cantidad_practica',
			],
            [
                'class'=>'\kartik\grid\DataColumn',
                 'attribute'=>'area',
                 'label'=> ' area',
                 'value'=>'odontologia.area.descrip',
             ],
             [
                'class'=>'\kartik\grid\DataColumn',
                 'attribute'=>'odontologiaanio',
                 'label'=> ' Odontologia anio',
                 'value'=>'odontologia.anio',
             ],
             [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'sttipo',
                'label'=> 'tipo',
                'value'=>'odontologia.sttipo.nombre'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOdontologia()
    {
        return $this->hasOne(Odontologia::className(), ['id' => 'id_odontologia']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractica()
    {
        return $this->hasOne(STNomenclador::className(), ['id' => 'id_practica']);
    }
    public function getOdontologiaanio(){
        return $this->getOdontologia()->select('anio');
    }
    public function getArea()
    {
        return $this->getOdontologia()->select('descrip')->join('inner join','sies.m_area ma','ma.id=odontologia.id_area');
    }
    public function getSttipo()
    {
        return $this->getOdontologia()->select('nombre')->join('inner join','sies.st_tipo st','st.id=odontologia.id_st_tipo');
    }
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->practica_nombre=$this->practica->nombre;

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
