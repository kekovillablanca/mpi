<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MAreaSearch represents the model behind the search form about `app\models\MArea`.
 */
class MAreaSearch extends MArea
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'descrip', 'codsisa'], 'safe'],
            [['codigo', 'codfos'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MArea::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'codigo',
				'descrip',
				'codfos',
				'codsisa',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'm_area.id' => $this->id,
            'm_area.codigo' => $this->codigo,
            'm_area.codfos' => $this->codfos,
        ]);

        $query->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->descrip)])
              ->andFilterWhere(['like', 'lower(m_area.codsisa)',strtolower($this->codsisa)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('marea-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
