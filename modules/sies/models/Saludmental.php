<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "saludmental".
 *
 * @property int $id
 * @property int $id_area
 * @property int $mes
 * @property int $anio
 * @property int $id_st_tipo
 * @property int $cons_pv
 * @property int $cons_total
 *
 * @property MArea $area
 * @property STTipo $STtipo
 * @property SaludmentalPractica[] $saludmentalPracticas
 */
class Saludmental extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.saludmental';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_area', 'mes',  'cons_pv', 'cons_total'], 'default', 'value' => null],
            //agrego required
            [[ 'anio', 'id_st_tipo', ], 'required'],
            [['id_area', 'mes', 'anio', 'id_st_tipo', 'cons_pv', 'cons_total'], 'integer'],
            [['id_area'], 'exist', 'skipOnError' => true, 'targetClass' => MArea::className(), 'targetAttribute' => ['id_area' => 'id']],
            [['id_st_tipo'], 'exist', 'skipOnError' => true, 'targetClass' => STTipo::className(), 'targetAttribute' => ['id_st_tipo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_area' => 'Area',
            'mes' => 'Mes',
            'anio' => 'Año',
            'id_st_tipo' => 'Tipo',
            'cons_pv' => 'Consultas 1 Vez',
            'cons_total' => 'Consultas Total',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('saludmental.id', 10),
			'id_area' => array('m_area.descrip',10),
			'mes' => array('saludmental.mes', 10),
			'anio' => array('saludmental.anio', 10),
			'id_st_tipo' => array('st_tipo.nombre',11),
			'cons_pv' => array('saludmental.cons_pv', 10),
			'cons_total' => array('saludmental.cons_total', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_area'=>
			[
				'attribute'=>'area.descrip',
				'label'=>'Area',
			],
			'mes',
			'anio',
			'id_st_tipo'=>
			[
				'attribute'=>'sttipo.nombre',
				'label'=>'Tipo',
			],
			'cons_pv',
			'cons_total',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_area',
				'value'=>'area.descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'mes',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'anio',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_st_tipo',
				'value'=>'sttipo.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cons_pv',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cons_total',
			],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(MArea::className(), ['id' => 'id_area']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSttipo()
    {
        return $this->hasOne(STTipo::className(), ['id' => 'id_st_tipo']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaludmentalpracticas()
    {
        return $this->hasMany(SaludmentalPractica::className(), ['id_saludmental' => 'id']);
    }



    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
