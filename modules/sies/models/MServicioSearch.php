<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MServicioSearch represents the model behind the search form about `app\models\MServicio`.
 */
class MServicioSearch extends MServicio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'descrip', 'snomed', 'descrip2020','codigo_nacional2022'], 'safe'],
            [['codigo', 'codnacion', 'codnac2008'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MServicio::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'codigo',
				'descrip',
				'codnacion',
				'codnac2008',
				'snomed',
				'descrip2020',
                'codigo_nacional2022'
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'm_servicio.id' => $this->id,
            'm_servicio.codigo' => $this->codigo,
            'm_servicio.codnacion' => $this->codnacion,
            'm_servicio.codnac2008' => $this->codnac2008,
        ]);

        $query->andFilterWhere(['like', 'lower(m_servicio.descrip)',strtolower($this->descrip)])
              ->andFilterWhere(['like', 'lower(m_servicio.snomed)',strtolower($this->snomed)])
              ->andFilterWhere(['like', 'lower(m_servicio.descrip2020)',strtolower($this->descrip2020)])
              ->andFilterWhere(['like', 'lower(m_servicio.codigo_nacion2022)',strtolower($this->descrip2020)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('mservicio-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
