<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CuidadosSearch represents the model behind the search form about `app\models\Cuidados`.
 */
class CuidadosSearch extends Cuidados
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_area', 'mes', 'anio', 'medico', 'psicologo', 'enfermero', 'trabajador_social', 'kinesiologo', 'terapista_ocupacional', 'secretario', 'voluntario', 'otros'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cuidados::find();
		$query->joinWith(['area']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_area'=> [
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
				],
				'mes',
				'anio',
                'medico',
                'psicologo',
                'enfermero',
                'trabajador_social',
                'kinesiologo',
                'terapista_ocupacional',
                'secretario',
                'voluntario',
                'otros',            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'cuidados.id' => $this->id,
            'cuidados.mes' => $this->mes,
            'cuidados.anio' => $this->anio,
            'cuidados.medico' => $this->medico,
            'cuidados.psicologo' => $this->psicologo,
            'cuidados.enfermero' => $this->enfermero,
            'cuidados.trabajador_social' => $this->trabajador_social,
            'cuidados.kinesiologo' => $this->kinesiologo,
            'cuidados.terapista_ocupacional' => $this->terapista_ocupacional,
            'cuidados.secretario' => $this->secretario,
            'cuidados.voluntario' => $this->voluntario,
            'cuidados.otros' => $this->otros,
        ]);

        $query->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->id_area)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('cuidados-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
