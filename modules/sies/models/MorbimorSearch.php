<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MorbimorSearch represents the model behind the search form about `app\models\Morbimor`.
 */
class MorbimorSearch extends Morbimor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'establec', 'apellidos', 'fechadenac', 'sexo', 'fechaingr', 'servicio', 'fechapase', 'servicio01', 'fechapas01', 'servicio02', 'fechapas02', 'servicio03', 'fechapas03', 'servicio04', 'fechaegres', 'diagprinc', 'otrodiag', 'otrodiag1', 'otrodiag2', 'otrodiag3', 'producidop', 'operimport', 'lugardonde', 'otraoper', 'otraoper1', 'codigodele', 'otrascir', 'termina', 'tparto', 'condnacer', 'sexonaci', 'condnace01', 'sexonaci01', 'condnace02', 'sexonaci02', 'trabajtxt', 'fechaparto'], 'safe'],
            [['numero', 'historiacl', 'ndedocumen', 'tipo', 'edading', 'locresid', 'depresid', 'provresid', 'paisresid', 'tieneobras', 'codobrasoc', 'codhospit', 'diasest', 'diasest01', 'diasest02', 'diasest03', 'diasest04', 'totdiasest', 'egresopor', 'operado', 'diascir', 'totctrls', 'peso', 'peso01', 'peso02', 'estudios', 'trabajos', 'htalorig', 'htaldest', 'nrodocmad', 'edadgest', 'paridad', 'parto', 'horaingr', 'horapase', 'horapase01', 'horapase02', 'horapase03', 'horaegres', 'servaux', 'servaux01', 'servaux02', 'servaux03', 'servaux04'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $this->anio=substr($this->tableName(),-4);
        
        $query = Morbimor::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'numero',
				'historiacl',
				'establec',
				'apellidos',
				'ndedocumen',
				'tipo',
				'fechadenac',
				'edading',
				'sexo',
				'locresid',
				'depresid',
				'provresid',
				'paisresid',
				'tieneobras',
				'codobrasoc',
				'codhospit',
				'fechaingr',
				'servicio',
				'diasest',
				'fechapase',
				'servicio01',
				'diasest01',
				'fechapas01',
				'servicio02',
				'diasest02',
				'fechapas02',
				'servicio03',
				'diasest03',
				'fechapas03',
				'servicio04',
				'diasest04',
				'fechaegres',
				'totdiasest',
				'egresopor',
				'operado',
				'diagprinc',
				'otrodiag',
				'otrodiag1',
				'otrodiag2',
				'otrodiag3',
				'producidop',
				'operimport',
				'lugardonde',
				'otraoper',
				'otraoper1',
				'codigodele',
				'otrascir',
				'diascir',
				'termina',
				'tparto',
				'totctrls',
				'condnacer',
				'sexonaci',
				'peso',
				'condnace01',
				'sexonaci01',
				'peso01',
				'condnace02',
				'sexonaci02',
				'peso02',
				'estudios',
				'trabajos',
				'trabajtxt',
				'htalorig',
				'htaldest',
				'nrodocmad',
				'fechaparto',
				'edadgest',
				'paridad',
				'parto',
				'horaingr',
				'horapase',
				'horapase01',
				'horapase02',
				'horapase03',
				'horaegres',
				'servaux',
				'servaux01',
				'servaux02',
				'servaux03',
				'servaux04',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'morbimor'.$this->anio.'.id' => $this->id,
            'morbimor'.$this->anio.'.numero' => $this->numero,
            'morbimor'.$this->anio.'.historiacl' => $this->historiacl,
            'morbimor'.$this->anio.'.ndedocumen' => $this->ndedocumen,
            'morbimor'.$this->anio.'.tipo' => $this->tipo,
            'morbimor'.$this->anio.'.edading' => $this->edading,
            'morbimor'.$this->anio.'.locresid' => $this->locresid,
            'morbimor'.$this->anio.'.depresid' => $this->depresid,
            'morbimor'.$this->anio.'.provresid' => $this->provresid,
            'morbimor'.$this->anio.'.paisresid' => $this->paisresid,
            'morbimor'.$this->anio.'.tieneobras' => $this->tieneobras,
            'morbimor'.$this->anio.'.codobrasoc' => $this->codobrasoc,
            'morbimor'.$this->anio.'.codhospit' => $this->codhospit,
            'morbimor'.$this->anio.'.diasest' => $this->diasest,
            'morbimor'.$this->anio.'.diasest01' => $this->diasest01,
            'morbimor'.$this->anio.'.diasest02' => $this->diasest02,
            'morbimor'.$this->anio.'.diasest03' => $this->diasest03,
            'morbimor'.$this->anio.'.diasest04' => $this->diasest04,
            'morbimor'.$this->anio.'.totdiasest' => $this->totdiasest,
            'morbimor'.$this->anio.'.egresopor' => $this->egresopor,
            'morbimor'.$this->anio.'.operado' => $this->operado,
            'morbimor'.$this->anio.'.diascir' => $this->diascir,
            'morbimor'.$this->anio.'.totctrls' => $this->totctrls,
            'morbimor'.$this->anio.'.peso' => $this->peso,
            'morbimor'.$this->anio.'.peso01' => $this->peso01,
            'morbimor'.$this->anio.'.peso02' => $this->peso02,
            'morbimor'.$this->anio.'.estudios' => $this->estudios,
            'morbimor'.$this->anio.'.trabajos' => $this->trabajos,
            'morbimor'.$this->anio.'.htalorig' => $this->htalorig,
            'morbimor'.$this->anio.'.htaldest' => $this->htaldest,
            'morbimor'.$this->anio.'.nrodocmad' => $this->nrodocmad,
            'morbimor'.$this->anio.'.edadgest' => $this->edadgest,
            'morbimor'.$this->anio.'.paridad' => $this->paridad,
            'morbimor'.$this->anio.'.parto' => $this->parto,
            'morbimor'.$this->anio.'.horaingr' => $this->horaingr,
            'morbimor'.$this->anio.'.horapase' => $this->horapase,
            'morbimor'.$this->anio.'.horapase01' => $this->horapase01,
            'morbimor'.$this->anio.'.horapase02' => $this->horapase02,
            'morbimor'.$this->anio.'.horapase03' => $this->horapase03,
            'morbimor'.$this->anio.'.horaegres' => $this->horaegres,
            'morbimor'.$this->anio.'.servaux' => $this->servaux,
            'morbimor'.$this->anio.'.servaux01' => $this->servaux01,
            'morbimor'.$this->anio.'.servaux02' => $this->servaux02,
            'morbimor'.$this->anio.'.servaux03' => $this->servaux03,
            'morbimor'.$this->anio.'.servaux04' => $this->servaux04,
        ]);

        $query->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.establec)',strtolower($this->establec)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.apellidos)',strtolower($this->apellidos)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechadenac,\'DD/MM/YYYY\')',$this->fechadenac])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.sexo)',strtolower($this->sexo)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechaingr,\'DD/MM/YYYY\')',$this->fechaingr])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.servicio)',strtolower($this->servicio)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechapase,\'DD/MM/YYYY\')',$this->fechapase])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.servicio01)',strtolower($this->servicio01)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechapas01,\'DD/MM/YYYY\')',$this->fechapas01])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.servicio02)',strtolower($this->servicio02)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechapas02,\'DD/MM/YYYY\')',$this->fechapas02])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.servicio03)',strtolower($this->servicio03)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechapas03,\'DD/MM/YYYY\')',$this->fechapas03])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.servicio04)',strtolower($this->servicio04)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechaegres,\'DD/MM/YYYY\')',$this->fechaegres])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.diagprinc)',strtolower($this->diagprinc)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.otrodiag)',strtolower($this->otrodiag)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.otrodiag1)',strtolower($this->otrodiag1)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.otrodiag2)',strtolower($this->otrodiag2)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.otrodiag3)',strtolower($this->otrodiag3)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.producidop)',strtolower($this->producidop)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.operimport)',strtolower($this->operimport)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.lugardonde)',strtolower($this->lugardonde)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.otraoper)',strtolower($this->otraoper)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.otraoper1)',strtolower($this->otraoper1)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.codigodele)',strtolower($this->codigodele)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.otrascir)',strtolower($this->otrascir)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.termina)',strtolower($this->termina)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.tparto)',strtolower($this->tparto)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.condnacer)',strtolower($this->condnacer)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.sexonaci)',strtolower($this->sexonaci)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.condnace01)',strtolower($this->condnace01)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.sexonaci01)',strtolower($this->sexonaci01)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.condnace02)',strtolower($this->condnace02)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.sexonaci02)',strtolower($this->sexonaci02)])
              ->andFilterWhere(['like', 'lower(morbimor'.$this->anio.'.trabajtxt)',strtolower($this->trabajtxt)])
              ->andFilterWhere(['like', 'to_char(morbimor'.$this->anio.'.fechaparto,\'DD/MM/YYYY\')',$this->fechaparto]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('morbimor-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
