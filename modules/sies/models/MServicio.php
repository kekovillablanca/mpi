<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "m_servicio".
 *
 * @property int $id
 * @property string $codigo
 * @property string $descrip
 * @property string $codnacion
 * @property string $codnac2008
 * @property string $snomed
 * @property string $descrip2020
 */
class MServicio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.m_servicio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'codnacion', 'codnac2008'], 'number'],
            [['snomed', 'descrip2020','codigo_nacional2022'], 'string'],
            [['descrip'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'descrip' => 'Descripcion',
            'codnacion' => 'Cod Nacion',
            'codnac2008' => 'Cod Nacion 2008',
            'snomed' => 'SNOMED',
            'descrip2020' => 'Descripcion 2020',
            'codigo_nacional2022'=>'Codigo nacion'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('m_servicio.id', 10),
			'codigo' => array('m_servicio.codigo', 10),
			'descrip' => array('m_servicio.descrip', 10),
			'codnacion' => array('m_servicio.codnacion', 10),
			'codnac2008' => array('m_servicio.codnac2008', 11),
			'snomed' => array('m_servicio.snomed', 10),
			'descrip2020' => array('m_servicio.descrip2020', 12),
            'codigo_nacional2022'=>array('m_servicio.codigo_nacional2022', 12)
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'codigo',
			'descrip',
			'codnacion',
			'codnac2008',
			'snomed',
			'descrip2020',
            'codigo_nacional2022'
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codnacion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codnac2008',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'snomed',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descrip2020',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo_nacional2022',
			],
        ];
    }




    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
