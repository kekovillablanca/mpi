<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CuidadosPracticaSearch represents the model behind the search form about `app\models\CuidadosPractica`.
 */
class CuidadosPracticaSearch extends CuidadosPractica
{
    public $cuidadosmes;
    public $cuidadospsicologo;
    public $cuidadosenfermero;
    public $cuidadoskinesiologo;
    public $cuidadosterapistaocupacional;
    public $cuidadossecretario;
    public $cuidadosvoluntario;
    public $cuidadostrabajadorsocial;
    public $cuidadosotros;
    public $cuidadosmedico;
    public $area;
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_cuidados', 'id_practica','practica_nombre', 'total_mensual','cuidadosmes',
        'cuidadospsicologo','cuidadosenfermero','cuidadoskinesiologo','cuidadosterapistaocupacional','cuidadossecretario',
        'cuidadosvoluntario','cuidadostrabajadorsocial','cuidadosotros','cuidadosmedico','area'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuidadosPractica::find();
		$query->joinWith(['practica']);
        $query->joinWith(['cuidados']);
        $query->joinWith(['cuidados.area']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_practica',
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'cuidadosmes'=>[
					'asc' => ['cuidados.mes' => SORT_ASC],
					'desc' => ['cuidados.mes' => SORT_DESC],
                ],
                'area'=>[
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
                ],
                'cuidadosmedico'=>[
					'asc' => ['cuidados.medico' => SORT_ASC],
					'desc' => ['cuidados.medico' => SORT_DESC],
                ],
                'cuidadosanio'=>[
					'asc' => ['cuidados.anio' => SORT_ASC],
					'desc' => ['cuidados.anio' => SORT_DESC],
                ],
                'cuidadospsicologo'=>[
					'asc' => ['cuidados.psicologo' => SORT_ASC],
					'desc' => ['cuidados.psicologo' => SORT_DESC],
                ],
                'cuidadosenfermero'=>[
					'asc' => ['cuidados.enfermero' => SORT_ASC],
					'desc' => ['cuidados.enfermero' => SORT_DESC],
                ],
                'cuidadoskinesiologo'=>[
					'asc' => ['cuidados.kinesiologo' => SORT_ASC],
					'desc' => ['cuidados.kinesiologo' => SORT_DESC],
                ],
                'cuidadosterapistaocupacional'=>[
					'asc' => ['cuidados.terapista_ocupacional' => SORT_ASC],
					'desc' => ['cuidados.terapista_ocupacional' => SORT_DESC],
                ],
                'cuidadossecretario'=>[
					'asc' => ['cuidados.secretario' => SORT_ASC],
					'desc' => ['cuidados.secretario' => SORT_DESC],
                ],
                'cuidadosvoluntario'=>[
					'asc' => ['cuidados.voluntario' => SORT_ASC],
					'desc' => ['cuidados.voluntario' => SORT_DESC],
                ],
                'cuidadosotros'=>[
					'asc' => ['cuidados.otros' => SORT_ASC],
					'desc' => ['cuidados.otros' => SORT_DESC],
                ],
                'cuidadostrabajadorsocial'=>[
					'asc' => ['cuidados.trabajador_social' => SORT_ASC],
					'desc' => ['cuidados.trabajador_social' => SORT_DESC],
                ],
                'total_mensual',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'cuidados_practica.id' => $this->id,
            'cuidados_practica.total_mensual' => $this->total_mensual,
            'cuidados.mes' => $this->cuidadosmes,
        ]);

        $query->andFilterWhere(['like', 'lower(cuidados_practica.id_practica)',strtolower($this->id_practica)])
              ->andFilterWhere(['like', 'lower(st_nomenclador.nombre)',strtolower($this->practica_nombre)])
              ->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->area)])
              ->andFilterWhere(['like', 'CAST(cuidados.trabajador_social AS VARCHAR)',$this->cuidadostrabajadorsocial])
              ->andFilterWhere(['like', 'CAST(cuidados.otros AS VARCHAR)',$this->cuidadosotros])
              ->andFilterWhere(['like', 'CAST(cuidados.voluntario AS VARCHAR)',$this->cuidadosvoluntario])
              ->andFilterWhere(['like', 'CAST(cuidados.terapista_ocupacional AS VARCHAR)',$this->cuidadosterapistaocupacional])
              ->andFilterWhere(['like', 'CAST(cuidados.kinesiologo AS VARCHAR)',$this->cuidadoskinesiologo])
              ->andFilterWhere(['like', 'CAST(cuidados.enfermero AS VARCHAR)',$this->cuidadosenfermero])
              ->andFilterWhere(['like', 'CAST(cuidados.psicologo AS VARCHAR)',$this->cuidadospsicologo])
              ->andFilterWhere(['like', 'CAST(cuidados.medico AS VARCHAR)',$this->cuidadosmedico])
              ->andFilterWhere(['like', 'CAST(cuidados.secretario AS VARCHAR)',$this->cuidadossecretario])
              ->andFilterWhere(['like', 'CAST(cuidados.trabajador_social AS VARCHAR)',$this->cuidadostrabajadorsocial]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('cuidadospractica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
