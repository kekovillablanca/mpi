<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "st_tipo".
 *
 * @property int $id
 * @property string $nombre
 * @property int $clasificacion
 *
 * @property Laboratorio[] $laboratorios
 */
class STTipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.st_tipo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string'],
            [['clasificacion'], 'default', 'value' => null],
            [['clasificacion'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'clasificacion' => 'Clasificacion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('st_tipo.id', 10),
			'nombre' => array('st_tipo.nombre', 10),
			'clasificacion' => array('st_tipo.clasificacion', 14),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
			'clasificacion',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'clasificacion',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
