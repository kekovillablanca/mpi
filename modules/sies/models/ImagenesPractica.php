<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "imagenes_practica".
 *
 * @property int $id
 * @property int $id_imagenes
 * @property string $id_practica
 * @property int $int
 * @property int $cex
 * @property int $gua
 * @property int $otr
 * @property int $der
 * @property int $de2
 *
 * @property Imagenes $imagenes
 * @property STNomenclador $practica
 */
class ImagenesPractica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $practica_nombre;

     public static function tableName()
    {
        return 'sies.imagenes_practica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_imagenes', 'int', 'cex', 'gua', 'otr', 'der', 'de2'], 'default', 'value' => null],
            [['id_imagenes', 'int', 'cex', 'gua', 'otr', 'der', 'de2'], 'integer'],
            [['id_practica'], 'string', 'max' => 20],
            [['id_imagenes'], 'exist', 'skipOnError' => true, 'targetClass' => Imagenes::className(), 'targetAttribute' => ['id_imagenes' => 'id']],
            [['id_practica'], 'exist', 'skipOnError' => true, 'targetClass' => STNomenclador::className(), 'targetAttribute' => ['id_practica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_practica' => 'Código',
            'practica_nombre' => 'Práctica',
            'int' => 'Interna',
            'cex' => 'Consulta',
            'gua' => 'Guardia',
            'otr' => 'Otros',
            'der' => 'Deriva DE',
            'de2' => 'Deriva A',
            'area' =>'area',
            'imagenesanio'=>'anio',
            'sttipo'=>'tipo'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('imagenes_practica.id', 10),
			'id_practica' => array('imagenes_practica.id_practica',12),
			'practica_nombre' => array('st_nomenclador.nombre',12),
			'int' => array('imagenes_practica.int', 10),
			'cex' => array('imagenes_practica.cex', 10),
			'gua' => array('imagenes_practica.gua', 10),
			'otr' => array('imagenes_practica.otr', 10),
			'der' => array('imagenes_practica.der', 10),
			'de2' => array('imagenes_practica.de2', 10),
            'area' => array('m_area.descrip',12),
            'imagenesanio'=>array('imagenes.anio', 10),
            'sttipo' => array('st_tipo.nombre',11),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'area'=>
			[
				'attribute'=>'imagenes.area.descrip',
				'label'=>'Area',
			],
			
            'id_practica',
			'practica_nombre'=>
			[
				'attribute'=>'practica.nombre',
				'label'=>'Práctica',
			],
			'int',
			'cex',
			'gua',
			'otr',
			'der',
			'de2',
            'imagenesanio'=>
			[
				'attribute'=>'imagenes.anio',
				'label'=>'Año',
			],
            
			'sttipo'=>
			[
				'attribute'=>'imagenes.sttipo.nombre',
				'label'=>'Tipo',
			],
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_practica',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'practica_nombre',
//				'value'=>'practica.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'int',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cex',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'gua',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'otr',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'der',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'de2',
			],
           
           
          
            [
                'class'=>'\kartik\grid\DataColumn',
                 'attribute'=>'area',
                 'label'=> 'Imagenes area',
                 'value'=>'imagenes.area.descrip',
             ],
             [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'sttipo',
                'label'=> 'Imagenes tipo',
                'value'=>'imagenes.sttipo.nombre'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'imagenesanio',
                'label'=> 'Año',
                'value'=>'imagenes.anio'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagenes()
    {
        return $this->hasOne(Imagenes::className(), ['id' => 'id_imagenes']);
    }
   
    public function getImagenesanio(){
        return $this->getImagenes()->select('anio');
    }
    /*public function getImagenesint(){
        return $this->getImagenes()->select('int');
    }
    public function getImagenescon(){
        return $this->getImagenes()->select('con');
    }
    public function getImagenesgua(){
        return $this->getImagenes()->select('gua');
    }
     public function getImagenesmes(){
        return $this->getImagenes()->select('mes');
    }*/
    public function getArea()
    {
        return $this->getImagenes()->select('descrip')->join('inner join','sies.m_area ma','ma.id=imagenes.id_area');
    }
    public function getSttipo()
    {
        return $this->getImagenes()->select('nombre')->join('inner join','sies.st_tipo st','st.id=imagenes.id_st_tipo');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractica()
    {
        return $this->hasOne(STNomenclador::className(), ['id' => 'id_practica']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->practica_nombre=$this->practica->nombre;

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
