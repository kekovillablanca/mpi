<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CirugiaPracticaSearch represents the model behind the search form about `app\models\CirugiaPractica`.
 */
class CirugiaPracticaSearch extends CirugiaPractica
{

   
    public $sttpipo;
    public $area;
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_cirugia', 'id_practica','practica_nombre', 'int', 'cex', 'gua','area','sttipo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CirugiaPractica::find();
		$query->joinWith(['practica']);
        $query->joinWith(['cirugia']);
        $query->joinWith('cirugia.area');
        $query->joinWith('cirugia.sttipo');

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_practica',
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'area'=>[
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
                ],
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'sttipo'=>[
                    'asc' => ['st_tipo.nombre' => SORT_ASC],
					'desc' => ['st_tipo.nombre' => SORT_DESC],
                ],
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'id_cirugia'=>[
					'asc' => ['cirugia.mes' => SORT_ASC],
					'desc' => ['cirugia.mes' => SORT_DESC],
                ],
                'int',
				'cex',
				'gua',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'cirugia_practica.id' => $this->id,
            'cirugia_practica.int' => $this->int,
            'cirugia_practica.cex' => $this->cex,
            'cirugia_practica.gua' => $this->gua,
        ]);

        $query->andFilterWhere(['like', 'lower(cirugia_practica.id_practica)',strtolower($this->id_practica)])
              ->andFilterWhere(['like', 'lower(st_nomenclador.nombre)',strtolower($this->practica_nombre)])
              ->andFilterWhere(['like', 'lower(m_area.area)',strtolower($this->area)])
              ->andFilterWhere(['like', 'lower(st_tipo.nombre)',strtolower($this->sttpipo)])
              ->andFilterWhere(['like', 'CAST(cirugia.mes as VARCHAR)',$this->id_cirugia]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('cirugiapractica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
