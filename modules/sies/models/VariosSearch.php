<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VariosSearch represents the model behind the search form about `app\models\Varios`.
 */
class VariosSearch extends Varios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_area', 'mes', 'anio', 'id_st_tipo', 'int', 'con', 'gua'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Varios::find();
		$query->joinWith(['area']);
		$query->joinWith(['sttipo']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_area'=> [
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
				],
				'mes',
				'anio',
				'id_st_tipo'=> [
					'asc' => ['st_tipo.nombre' => SORT_ASC],
					'desc' => ['st_tipo.nombre' => SORT_DESC],
				],
				'int',
				'con',
				'gua',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'varios.id' => $this->id,
            'varios.mes' => $this->mes,
            'varios.anio' => $this->anio,
            'varios.int' => $this->int,
            'varios.con' => $this->con,
            'varios.gua' => $this->gua,
        ]);

        $query->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->id_area)])
              ->andFilterWhere(['like', 'lower(st_tipo.nombre)',strtolower($this->id_st_tipo)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('varios-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
