<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "m_local".
 *
 * @property int $id
 * @property string $codigo
 * @property string $descrip
 * @property string $codloc
 * @property string $coddepar
 * @property string $codprovi
 * @property string $codpos
 * @property string $codsisa
 */
class MLocal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.m_local';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'codloc', 'coddepar', 'codprovi', 'codpos', 'codsisa'], 'number'],
            [['descrip'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'descrip' => 'Descripcion',
            'codloc' => 'Cod Localidad',
            'coddepar' => 'Cod Departamento',
            'codprovi' => 'Cod Provincia',
            'codpos' => 'Cod Postal',
            'codsisa' => 'Cod SISA',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('m_local.id', 10),
			'codigo' => array('m_local.codigo', 10),
			'descrip' => array('m_local.descrip', 20),
			'codloc' => array('m_local.codloc', 10),
			'coddepar' => array('m_local.coddepar', 10),
			'codprovi' => array('m_local.codprovi', 10),
			'codpos' => array('m_local.codpos', 10),
			'codsisa' => array('m_local.codsisa', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'codigo',
			'descrip',
			'codloc',
			'coddepar',
			'codprovi',
			'codpos',
			'codsisa',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codloc',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'coddepar',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codprovi',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codpos',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codsisa',
			],
        ];
    }




    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
