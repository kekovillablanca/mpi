<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ResumenConsultaSearch represents the model behind the search form about `app\models\ResumenConsulta`.
 */
class ResumenConsultaSearch extends ResumenConsulta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'anio', 'mes', 'id_puesto', 'id_area' , 'id_servicio', 'id_rango', 'varones', 'mujeres'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ResumenConsulta::find();
		$query->joinWith(['puesto']);
		$query->joinWith(['rango']);
		$query->joinWith(['servicio']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'anio',
				'mes',
				'id_puesto'=> [
					'asc' => ['m_puesto.descrip' => SORT_ASC],
					'desc' => ['m_puesto.descrip' => SORT_DESC],
				],
				'id_area'=> [
					'asc' => ['m_puesto.area' => SORT_ASC],
					'desc' => ['m_puesto.area' => SORT_DESC],
				],
				'id_servicio'=> [
					'asc' => ['m_servicio.descrip' => SORT_ASC],
					'desc' => ['m_servicio.descrip' => SORT_DESC],
				],
				'id_rango'=> [
					'asc' => ['m_rango.nombre' => SORT_ASC],
					'desc' => ['m_rango.nombre' => SORT_DESC],
				],
				'varones',
				'mujeres',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'resumen_consulta.id' => $this->id,
            'resumen_consulta.anio' => $this->anio,
            'resumen_consulta.mes' => $this->mes,
            'resumen_consulta.varones' => $this->varones,
            'resumen_consulta.mujeres' => $this->mujeres,
        ]);

        $query->andFilterWhere(['like', 'lower(m_puesto.descrip)',strtolower($this->id_puesto)])
              ->andFilterWhere(['like', 'lower(m_puesto.area)',strtolower($this->id_area)])
              ->andFilterWhere(['like', 'lower(m_servicio.descrip)',strtolower($this->id_servicio)])
              ->andFilterWhere(['like', 'lower(m_rango.nombre)',strtolower($this->id_rango)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('resumenconsulta-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
