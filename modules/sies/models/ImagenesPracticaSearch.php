<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ImagenesPracticaSearch represents the model behind the search form about `app\models\ImagenesPractica`.
 */
class ImagenesPracticaSearch extends ImagenesPractica
{
    public $area;
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['int', 'cex', 'gua', 'otr', 'der', 'de2'],'integer'],
            [['id', 'id_imagenes', 'id_practica','practica_nombre','area','sttipo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImagenesPractica::find();
		$query->joinWith(['practica']);
        $query->joinWith('imagenes');
        $query->joinWith('imagenes.area');
        $query->joinWith('imagenes.sttipo');

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_practica',
                'practica_nombre'=>[
					'asc' => ['st_nomenclador.nombre' => SORT_ASC],
					'desc' => ['st_nomenclador.nombre' => SORT_DESC],
                ],
                'int',
				'cex',
				'gua',
				'otr',
				'der',
				'de2',
                'imagenesanio'=>[
					'asc' => ['imagenes.anio' => SORT_ASC],
					'desc' => ['imagenes.anio' => SORT_DESC],
                ],
                'sttipo'=>[
                    'asc' => ['st_tipo.nombre' => SORT_ASC],
					'desc' => ['st_tipo.nombre' => SORT_DESC],
                ],
                'area'=>[
					'asc' => ['m_area.descrip' => SORT_ASC],
					'desc' => ['m_area.descrip' => SORT_DESC],
                ],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'imagenes_practica.id' => $this->id,
            'imagenes_practica.int' => $this->int,
            'imagenes_practica.cex' => $this->cex,
            'imagenes_practica.gua' => $this->gua,
            'imagenes_practica.otr' => $this->otr,
            'imagenes_practica.der' => $this->der,
            'imagenes_practica.de2' => $this->de2,
        ]);

        $query->andFilterWhere(['like', 'lower(imagenes_practica.id_practica)',strtolower($this->id_practica)])
              ->andFilterWhere(['like', 'lower(st_nomenclador.nombre)',strtolower($this->practica_nombre)])
              ->andFilterWhere(['like', 'lower(m_area.descrip)',strtolower($this->area)]);
              

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('imagenespractica-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
