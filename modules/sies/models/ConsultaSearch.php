<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ConsultaSearch represents the model behind the search form about `app\models\Consulta`.
 */
class ConsultaSearch extends Consulta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id'], 'integer'],
            [['numero', 'nrohc', 'nrodoc', 'edad', 'obra', 'primera', 'cemb', 'cns'], 'number'],
            [['hospital', 'puesto', 'servicio', 'medico', 'fecha', 'apellido', 'cdgosexo', 'diag'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

		$this->anio=substr($this->tableName(),-4);

        $query = Consulta::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'numero',
				'hospital',
				'puesto',
				'servicio',
				'medico',
				'fecha',
				'nrohc',
				'nrodoc',
				'apellido',
				'edad',
				'cdgosexo',
				'obra',
				'primera',
				'cemb',
				'cns',
				'diag',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'consulta'.$this->anio.'.id' => $this->id,
            'consulta'.$this->anio.'.numero' => $this->numero,
            'consulta'.$this->anio.'.nrohc' => $this->nrohc,
            'consulta'.$this->anio.'.nrodoc' => $this->nrodoc,
            'consulta'.$this->anio.'.edad' => $this->edad,
            'consulta'.$this->anio.'.obra' => $this->obra,
            'consulta'.$this->anio.'.primera' => $this->primera,
            'consulta'.$this->anio.'.cemb' => $this->cemb,
            'consulta'.$this->anio.'.cns' => $this->cns,
        ]);

        $query->andFilterWhere(['like', 'lower(consulta'.$this->anio.'.hospital)',strtolower($this->hospital)])
              ->andFilterWhere(['like', 'lower(consulta'.$this->anio.'.puesto)',strtolower($this->puesto)])
              ->andFilterWhere(['like', 'lower(consulta'.$this->anio.'.servicio)',strtolower($this->servicio)])
              ->andFilterWhere(['like', 'lower(consulta'.$this->anio.'.medico)',strtolower($this->medico)])
              ->andFilterWhere(['like', 'to_char(consulta'.$this->anio.'.fecha,\'DD/MM/YYYY\')',$this->fecha])
              ->andFilterWhere(['like', 'lower(consulta'.$this->anio.'.apellido)',strtolower($this->apellido)])
              ->andFilterWhere(['like', 'lower(consulta'.$this->anio.'.cdgosexo)',strtolower($this->cdgosexo)])
              ->andFilterWhere(['like', 'lower(consulta'.$this->anio.'.diag)',strtolower($this->diag)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('consulta-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
