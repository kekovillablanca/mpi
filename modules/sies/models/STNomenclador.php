<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "st_nomenclador".
 *
 * @property string $id
 * @property string $nombre
 * @property string $clasificacion
 *
 * @property LaboratorioPractica[] $laboratorioPracticas
 */
class STNomenclador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.st_nomenclador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['clasificacion'], 'number'],
            [['id'], 'string', 'max' => 8],
            [['nombre'], 'string', 'max' => 110],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'clasificacion' => 'Clasificacion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('st_nomenclador.id', 8),
			'nombre' => array('st_nomenclador.nombre', 20),
			'clasificacion' => array('st_nomenclador.clasificacion', 14),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'nombre',
			'clasificacion',
        ];
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'clasificacion',
			],
        ];
    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
