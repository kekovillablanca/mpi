<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "cuidados_practica".
 *
 * @property int $id
 * @property int $id_cuidados
 * @property string $id_practica
 * @property int $total_mensual
 *
 * @property Cuidados $cuidados
 * @property STNomenclador $practica
 */
class CuidadosPractica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $practica_nombre;

     public static function tableName()
    {
        return 'sies.cuidados_practica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cuidados', 'total_mensual'], 'default', 'value' => null],
            [['id_cuidados', 'total_mensual'], 'integer'],
            [['id_practica'], 'string', 'max' => 20],
            [['id_cuidados'], 'exist', 'skipOnError' => true, 'targetClass' => Cuidados::className(), 'targetAttribute' => ['id_cuidados' => 'id']],
            [['id_practica'], 'exist', 'skipOnError' => true, 'targetClass' => STNomenclador::className(), 'targetAttribute' => ['id_practica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_practica' => 'Código',
            'practica_nombre' => 'Práctica',
            'total_mensual' => 'Total Mensual',
            'cuidadosmes' => 'Mes',
            'cuidadosanio' => 'Anio',
            'cuidadosmedico' => 'Medico',
            'cuidadospsicologo' => 'Psicologo',
            'cuidadosenfermero' => 'Enfermero',
            'cuidadoskinesiologo' => 'kinesiologo',
            'cuidadosterapistaocupacional' => 'Terapista',
            'cuidadossecretario' => 'Secretario',
            'cuidadosvoluntario' => 'Voluntario',
            'cuidadosotros' => 'Otros',
            'area'=>'Area',
            'cuidadostrabajadorsocial'=> 'Trabajador social'
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('sies.cuidados_practica.id', 10),
			'id_practica' => array('sies.cuidados_practica.id_practica',12),
			'practica_nombre' => array('sies.st_nomenclador.nombre',12),
			'total_mensual' => array('sies.cuidados_practica.total_mensual', 10),
            'area' => array('sies.m_area.descrip', 12),
            'cuidadosmes' => array('sies.cuidados.mes', 10),
			'cuidadosanio' => array('sies.cuidados.anio', 10),
            'cuidadosmedico' => array('sies.cuidados.medico', 10),
            'cuidadospsicologo' => array('sies.cuidados.psicologo', 10),
            'cuidadosenfermero' => array('sies.cuidados.enfermero', 10),
            'cuidadostrabajadorsocial' => array('sies.cuidados.trabajador_social', 18),
            'cuidadoskinesiologo' => array('sies.cuidados.kinesiologo', 12),
            'cuidadosterapistaocupacional' => array('sies.cuidados.terapista_ocupacional', 20),
            'cuidadossecretario' => array('sies.cuidados.secretario', 11),
            'cuidadosvoluntario' => array('sies.cuidados.voluntario', 11),
            'cuidadosotros' => array('sies.cuidados.otros', 10),         
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'area'=>
			[
				'attribute'=>'cuidados.area.descrip',
				'label'=>'Area',
			],
			'cuidadosanio'=>
			[
				'attribute'=>'cuidados.anio',
				'label'=>'Año',
			],
			'cuidadosmes'=>
			[
				'attribute'=>'cuidados.mes',
				'label'=>'Mes',
			],
            'cuidadosmedico'=>
			[
				'attribute'=>'cuidados.medico',
				'label'=>'Medico',
			],
            'cuidadospsicologo'=>
			[
				'attribute'=>'cuidados.psicologo',
				'label'=>'Psicologo',
			],
            'cuidadosenfermero'=>
			[
				'attribute'=>'cuidados.enfermero',
				'label'=>'Enfermero',
			],
            'cuidadostrabajadorsocial'=>
			[
				'attribute'=>'cuidados.trabajador_social',
				'label'=>'Trabajador social',
			],
            'cuidadoskinesiologo'=>
			[
				'attribute'=>'cuidados.kinesiologo',
				'label'=>'Kinesiologo',
			],
            'cuidadosterapistaocupacional'=>
			[
				'attribute'=>'cuidados.terapista_ocupacional',
				'label'=>'Terapista ocupacional',
			],
            'cuidadoskinesiologo'=>
			[
				'attribute'=>'cuidados.kinesiologo',
				'label'=>'Kinesiologo',
			],
            'cuidadossecretario'=>
			[
				'attribute'=>'cuidados.Secretarop',
				'label'=>'Secretario',
			],
            'cuidadosvoluntario'=>
			[
				'attribute'=>'cuidados.voluntario',
				'label'=>'Voluntario',
			],
            'cuidadosotros'=>
			[
				'attribute'=>'cuidados.otros',
				'label'=>'Otros',
			],
            'id_practica',
			'practica_nombre'=>
			[
				'attribute'=>'practica.nombre',
				'label'=>'Práctica',
			],
			'total_mensual',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_practica',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'practica_nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'total_mensual',
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'area',
                'label'=> 'area',
                'value'=>'cuidados.area.descrip'
			],
            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cuidadosmes',
                'label'=> 'mes',
                'value'=>'cuidados.mes'
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cuidadosanio',
                'label'=> 'anio',
                'value'=>'cuidados.anio'
			],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadosmedico',
                'label'=> 'medico',
                'value'=>'cuidados.medico'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadospsicologo',
                'label'=> 'psicologo',
                'value'=>'cuidados.psicologo'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadosenfermero',
                'label'=> 'enfermero',
                'value'=>'cuidados.enfermero'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadostrabajadorsocial',
                'label'=> 'Trabajador social',
                'value'=>'cuidados.trabajador_social'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadoskinesiologo',
                'label'=> 'Kinesiologo',
                'value'=>'cuidados.kinesiologo'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadosterapistaocupacional',
                'label'=> 'terapista ocupacional',
                'value'=>'cuidados.terapista_ocupacional'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadossecretario',
                'label'=> 'Secretario',
                'value'=>'cuidados.secretario'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadosvoluntario',
                'label'=> 'Voluntario',
                'value'=>'cuidados.voluntario'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cuidadosotros',
                'label'=> 'Otros',
                'value'=>'cuidados.secretario'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuidados()
    {
        return $this->hasOne(Cuidados::className(), ['id' => 'id_cuidados']);
    }
    public function getArea()
    {
        return $this->getCuidados()->select('descrip')->join('inner join','sies.m_area ma','ma.id=cuidados.id_area');
    }
    public function getCuidadosmes(){
        return $this->getCuidados()->select('mes');
    }
    public function getCuidadosanio(){
        return $this->getCuidados()->select('anio');
    }
    public function getCuidadospsicologo(){
        return $this->getCuidados()->select('psicologo');
    }
    public function getCuidadosmedico(){
        return $this->getCuidados()->select('medico');
    }
    public function getCuidadosenfermero(){
        return $this->getCuidados()->select('enfermero');
    }
    public function getCuidadostrabajadorsocial(){
        return $this->getCuidados()->select('trabajador_social');
    }
    public function getCuidadoskinesiologo(){
        return $this->getCuidados()->select('kinesiologo');
    }
    public function getCuidadosterapistaocupacional(){
        return $this->getCuidados()->select('terapista_ocupacional');
    }
    public function getCuidadossecretario(){
        return $this->getCuidados()->select('secretario');
    }
    public function getCuidadosvoluntario(){
        return $this->getCuidados()->select('voluntario');
    }
    public function getCuidadosotros(){
        return $this->getCuidados()->select('otros');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractica()
    {
        return $this->hasOne(STNomenclador::className(), ['id' => 'id_practica']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->practica_nombre=$this->practica->nombre;

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
