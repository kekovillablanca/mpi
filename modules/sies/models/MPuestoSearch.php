<?php

namespace app\modules\sies\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MPuestoSearch represents the model behind the search form about `app\models\MPuesto`.
 */
class MPuestoSearch extends MPuesto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'descrip', 'area', 'zona', 'codsisa'], 'safe'],
            [['codigo', 'codpuesto', 'codzona', 'codarea'], 'number'],
            [['nacion'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MPuesto::find();

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'codigo',
				'descrip',
				'codpuesto',
				'codzona',
				'codarea',
				'area',
				'zona',
				'nacion',
				'codsisa',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'm_puesto.id' => $this->id,
            'm_puesto.codigo' => $this->codigo,
            'm_puesto.codpuesto' => $this->codpuesto,
            'm_puesto.codzona' => $this->codzona,
            'm_puesto.codarea' => $this->codarea,
        ]);

        $query->andFilterWhere(['like', 'lower(m_puesto.descrip)',strtolower($this->descrip)])
              ->andFilterWhere(['like', 'lower(m_puesto.area)',strtolower($this->area)])
              ->andFilterWhere(['like', 'lower(m_puesto.zona)',strtolower($this->zona)])
              ->andFilterWhere(['like', 'm_puesto.nacion', $this->nacion])
              ->andFilterWhere(['like', 'lower(m_puesto.codsisa)',strtolower($this->codsisa)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('mpuesto-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
