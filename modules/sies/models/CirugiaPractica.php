<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "cirugia_practica".
 *
 * @property int $id
 * @property int $id_cirugia
 * @property string $id_practica
 * @property int $int
 * @property int $cex
 * @property int $gua
 *
 * @property Cirugia $cirugia
 * @property STNomenclador $practica
 */
class CirugiaPractica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $practica_nombre;

     public static function tableName()
    {
        return 'sies.cirugia_practica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cirugia', 'int', 'cex', 'gua'], 'default', 'value' => null],
            [['id_cirugia', 'int', 'cex', 'gua'], 'integer'],
            [['id_practica'], 'string', 'max' => 20],
            [['id_cirugia'], 'exist', 'skipOnError' => true, 'targetClass' => Cirugia::className(), 'targetAttribute' => ['id_cirugia' => 'id']],
            [['id_practica'], 'exist', 'skipOnError' => true, 'targetClass' => STNomenclador::className(), 'targetAttribute' => ['id_practica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_practica' => 'Código',
            'practica_nombre' => 'Práctica',
            'int' => 'Interna',
            'cex' => 'Consulta',
            'gua' => 'Guardia',
            'sttipo'=>'tipo',
            'area' =>'area',
            'id_cirugia'=>'Cirugia mes',
            'cirugiaanio'=>'Cirugia anio',
                ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('cirugia_practica.id', 10),
			'id_practica' => array('cirugia_practica.id_practica',12),
			'practica_nombre' => array('st_nomenclador.nombre',12),
			'int' => array('cirugia_practica.int', 10),
			'cex' => array('cirugia_practica.cex', 10),
			'gua' => array('cirugia_practica.gua', 10),
            'sttipo' => array('st_tipo.nombre',11),
            'area' => array('m_area.descrip',12),
            'cirugiaanio'=>array('cirugia.anio', 10),
            'id_cirugia' => array('cirugia.mes', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'area'=>
			[
				'attribute'=>'cirugia.area.descrip',
				'label'=>'Area',
			],
			'cirugiaanio'=>
			[
				'attribute'=>'cirugia.anio',
				'label'=>'Año',
			],
			'id_cirugia'=>
			[
				'attribute'=>'cirugia.mes',
				'label'=>'Mes',
			],
			'tipo'=>
			[
				'attribute'=>'cirugia.sttipo.nombre',
				'label'=>'Tipo',
			],
            'id_practica',
			'practica_nombre'=>
			[
				'attribute'=>'practica.nombre',
				'label'=>'Práctica',
			],
            'sttipo'=>
			[
				'attribute'=>'cirugia.sttipo.nombre',
				'label'=>'Tipo',
			],'area'=>
			[
				'attribute'=>'cirugia.area.descrip',
				'label'=>'Area',
			],
			'int',
			'cex',
			'gua',
        ];
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_practica',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'practica_nombre',
//				'value'=>'practica.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'int',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cex',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'gua',
			],
            [
                'class'=>'\kartik\grid\DataColumn',
                 'attribute'=>'area',
                 'label'=> 'cirugia area',
                 'value'=>'cirugia.area.descrip',
             ],
             [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'sttipo',
                'label'=> 'cirugia tipo',
                'value'=>'cirugia.sttipo.nombre'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'id_cirugia',
                'label'=> 'cirugia mes',
                'value'=>'cirugia.mes'
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'cirugiaanio',
                'label'=> 'cirugia anio',
                'value'=>'cirugia.anio'
            ],
        ];
    }

    public function getCirugiames(){
        return $this->getCirugia()->select('mes');
    }
    public function getCirugiaanio(){
        return $this->getCirugia()->select('mes');
    }
  
    public function getArea()
    {
        return $this->getCirugia()->select('descrip')->join('inner join','sies.m_area ma','ma.id=cirugia.id_area');
    }
    public function getSttipo()
    {
        return $this->getCirugia()->select('nombre')->join('inner join','sies.st_tipo st','st.id=cirugia.id_st_tipo');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCirugia()
    {
        return $this->hasOne(Cirugia::className(), ['id' => 'id_cirugia']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractica()
    {
        return $this->hasOne(STNomenclador::className(), ['id' => 'id_practica']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
        $this->practica_nombre=$this->practica->nombre;

    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}

