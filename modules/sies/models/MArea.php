<?php
/* Modelo generado por Model(Q) */
namespace app\modules\sies\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "m_area".
 *
 * @property int $id
 * @property string $codigo
 * @property string $descrip
 * @property string $codfos
 * @property string $codsisa
 */
class MArea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sies.m_area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'codfos'], 'number'],
            [['codsisa'], 'string'],
            [['descrip'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'descrip' => 'Descripcion',
            'codfos' => 'Cod FOS',
            'codsisa' => 'Cod SISA',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('m_area.id', 10),
			'codigo' => array('m_area.codigo', 10),
			'descrip' => array('m_area.descrip', 20),
			'codfos' => array('m_area.codfos', 10),
			'codsisa' => array('m_area.codsisa', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'codigo',
			'descrip',
			'codfos',
			'codsisa',
        ];
    }

    public function getDescripcion(){
        return $this->descrip;
    }
    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id',
				'width'=>'30px',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descrip',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codfos',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codsisa',
			],
        ];
    }




    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
