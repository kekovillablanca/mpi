<?php

namespace app\modules\sies\controllers;

use kartik\spinner\Spinner;
use Yii;
use app\components\Listado\Listado;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
//
use app\modules\sies\models\Consulta;
use app\modules\sies\models\ConsultaSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;
use app\modules\sies\models\ConsultaLogica;

use yii\base\ViewNotFoundException;
use yii\bootstrap\Alert;
use yii\data\ActiveDataProvider;
use yii\db\Query;

use yii\web\MethodNotAllowedHttpException;
use yii\web\UploadedFile;


/**
 * ConsultaController implements the CRUD actions for Consulta model.
 */
class ConsultaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select',
                           'recepcioncmp','confirmarorigen','comparacion'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select',
                                      'recepcioncmp','confirmarorigen','comparacion','reportesiessigesh'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Consulta models.
     * @return mixed
     */
    public function actionIndex($anio)
    {    
       
        Consulta::$table='consulta'.$anio;
        $model= new Consulta();

        $searchModel = new ConsultaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $columnas=Metodos::obtenerColumnas($model,'sies/consulta/index');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
            'anio'=>$anio
        ]);
    }


    /**
     * Displays a single Consulta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$anio)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id,$anio);
        $attributes = $model->attributeView($model);

        if(isset($_GET['pdf'])){

            $titulo='Datos de Consulta';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $request = Yii::$app->request;
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Consulta #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $this->findModel($id,$anio),
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true,'anio'=>$anio],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id,'anio'=>$anio],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Consulta model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
/*        $request = Yii::$app->request;
        Consulta::$table='consulta2020';
        $model = new Consulta();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Consulta #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Crear Consulta",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/
    }

    /**
     * Updates an existing Consulta model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$anio)
    {
/*        $request = Yii::$app->request;
        $model = $this->findModel($id,$anio);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Consulta #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Editar Consulta #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/        
    }

    /**
     * Delete an existing Consulta model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$anio)
    {
/*
        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($this->findModel($id,$anio)->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error en la base de datos.',500);
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/
    }

    /**
     * Finds the Consulta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Consulta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id,$anio)
    {
        Consulta::$table='consulta'.$anio;
        if (($model = Consulta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /*private function recorrerReportes($columna1,$columna2){
    
        foreach($columna1 as $r1){
           
        $columnas=[
            'codprov'=>$r1['codprov'],'puesto'=>$r1['puesto'],'anioconsul'=>$r1['anioconsul'],'mesconsul'=>$r1['mesconsul'],'coduoperat'=>$r1['coduoperat'],'rangoedad'=>$r1['rangoedad'],'varones'=>$r1['varones'],'mujeres'=>$r1['mujeres']];
            
        foreach($columna2 as $key => $r2){
        if($r1['codprov']==$r2['codprov']  && $r1['puesto']==$r2['puesto'] && $r1['anioconsul']==$r2['anioconsul'] &&
        $r1['mesconsul']==$r2['mesconsul'] && $r1['coduoperat'] == $r2['coduoperat'] && $r1['rangoedad'] == $r2['rangoedad']){
            
            $columnas['varones']+=$r2['varones'];
            $columnas['mujeres']+=$r2['mujeres'];
            
            unset($columna2[$key]);
        }
    }
        array_push($columna2,$columnas);
        }
    
       
       return $columna2;
       
    }*/
    

    public function actionInternacionviedma(){

        if(Seguridad::tienePermiso('reportesiessigesh')){
            $connection = Yii::$app->db; 
            $updateQuery=$connection->createCommand("UPDATE hospitalviedma.internacion
            SET
            centro=REPLACE(centro  , ',', ' '),  nombre_apellido=REPLACE(nombre_apellido  , ',', ' '), dni=REPLACE(dni  , ',', ' '), numero_dni=REPLACE(numero_dni  , ',', ' '),
            trabaja=REPLACE(trabaja  , ',', ' '), obra_social=REPLACE(obra_social  , ',', ' '), plan=REPLACE(plan  , ',', ' '), nivel_estudio=REPLACE(nivel_estudio  , ',', ' '), tipo_egreso=REPLACE(tipo_egreso  , ',', ' '),
            diagnostico_principal=REPLACE(diagnostico_principal  , ',', ' '), codigo_diagnostico=REPLACE(codigo_diagnostico  , ',', ' '),
            diagnostico_de_episodio=REPLACE(diagnostico_de_episodio  , ',', ' '), profesional=REPLACE(profesional  , ',', ' '), servicio=REPLACE(servicio, ',', ' '),
            especialidad=REPLACE(especialidad, ',', ' '), diagnostico_2=REPLACE(diagnostico_2, ',', ' '),
            lugar_ocurrido=REPLACE(lugar_ocurrido, ',', ' '), producido_por=REPLACE(producido_por, ',', ' '), provincia=REPLACE(provincia, ',', ' '),
            departamento_descripcion=REPLACE(departamento_descripcion, ',', ' '), pais_paciente=REPLACE(pais_paciente, ',', ' '), sexo=REPLACE(sexo, ',', ' '),
            codigo_provincia=REPLACE(codigo_provincia , ',', ' '), pais_residencia=REPLACE(pais_residencia  , ',', ' '), numero_hc=REPLACE(numero_hc, ',', ' ');")->queryAll();
        $dataProvider = new ActiveDataProvider([
            'query' => (new Query())
                ->select([
                  "codprov","anio","numero_internacion","edad","fecha_ingreso","fecha_nacimiento","fecha_egreso","dias_estado","dias_internado","centro",
       "centro_codigo","nombre_apellido","dni","numero_dni","trabaja","obra_social","plan","nivel_estudio","tipo_egreso","codigo_egreso","diagnostico_principal","codigo_diagnostico",
        "codigo","diagnostico_de_episodio","profesional","servicio","especialidad","diagnostico_2","origen_codigo_internacion","lugar_ocurrido","producido_por","fecha_pase","provincia",
        "departamento_descripcion","pais_paciente","sexo","codigo_provincia","pais_residencia","numero_hc"
                ])
                ->from('hospitalviedma.internacion')->leftJoin('consultasestadisticas.diagnostico d','(internacion.diagnostico_principal =d.nombre OR internacion.diagnostico_2=d.nombre)'),
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'attributes' => [
                    'codprov',
                    'anio',
                    'numero_internacion',
                    'edad',
                    'fecha_ingreso',
                    'fecha_nacimiento',
                    'fecha_egreso',
                    'dias_estado',
                    'dias_internado',
                    'centro',
                    'centro_codigo',
                    'nombre_apellido',
                    'dni',
                    'numero_dni',
                    'trabaja',
                    'obra_social',
                    'plan',
                    'nivel_estudio',
                    'tipo_egreso',
                    'codigo_egreso',
                    'diagnostico_principal',
                    'codigo_diagnostico',
                    'codigo',
                    'diagnostico_de_episodio',
                    'profesional',
                    'servicio',
                    'especialidad',
                    'diagnostico_2',
                    'origen_codigo_internacion',
                    'lugar_ocurrido',
                    'producido_por',
                    'fecha_pase',
                    'provincia',
                    'departamento_descripcion',
                    'pais_paciente',
                    'sexo',
                    'codigo_provincia',
                    'pais_residencia',
                    'numero_hc',

                ],
                'defaultOrder' => [
                    'numero_internacion' => SORT_ASC, // Orden predeterminado ascendente por el nombre del profesional
                ],
            ],
        ]);
        $columnas = [
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'codprov',
                'value'=>'codprov',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'anio',
                'value'=>'anio',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'fecha_ingreso',
                'value'=>'fecha_ingreso',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'fecha_nacimiento',
                'value'=>'fecha_nacimiento',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'fecha_egreso',
                'value'=>'fecha_egreso',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'dias_estado',
                'value'=>'dias_estado',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'dias_internado',
                'value'=>'dias_internado',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'centro',
                'value'=>'centro',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'centro_codigo',
                'value'=>'centro_codigo',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'nombre_apellido',
                'value'=>'nombre_apellido',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'dni',
                'value'=>'dni',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'numero_dni',
                'value'=>'numero_dni',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'trabaja',
                'value'=>'trabaja',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'obra_social',
                'value'=>'obra_social',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'plan',
                'value'=>'plan',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'nivel_estudio',
                'value'=>'nivel_estudio',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'tipo_egreso',
                'value'=>'tipo_egreso',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'codigo_egreso',
                'value'=>'codigo_egreso',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'diagnostico_principal',
                'value'=>'diagnostico_principal',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'codigo_diagnostico',
                'value'=>'codigo_diagnostico',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'codigo',
                'value'=>'codigo',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'diagnostico_de_episodio',
                'value'=>'diagnostico_de_episodio',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'profesional',
                'value'=>'profesional',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'servicio',
                'value'=>'servicio',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'especialidad',
                'value'=>'especialidad',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'diagnostico_2',
                'value'=>'diagnostico_2',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'origen_codigo_internacion',
                'value'=>'origen_codigo_internacion',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'lugar_ocurrido',
                'value'=>'lugar_ocurrido',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'producido_por',
                'value'=>'producido_por',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'fecha_pase',
                'value'=>'fecha_pase',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'provincia',
                'value'=>'provincia',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'departamento_descripcion',
                'value'=>'departamento_descripcion',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'pais_paciente',
                'value'=>'pais_paciente',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'sexo',
                'value'=>'sexo',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'codigo_provincia',
                'value'=>'codigo_provincia',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'pais_residencia',
                'value'=>'pais_residencia',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'numero_hc',
                'value'=>'numero_hc',
            ],
        
        ];
        $permisos=['export'=>Seguridad::tienePermiso('export'),
        'select'=>Seguridad::tienePermiso('select')];
        return $this->render('internacion', [
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos

        ]);

       
     
        
    }
}
public function actionReportresumenconsulta(){
       // Establecer la consulta SQL para el año proporcionado
   $sql = "SELECT * FROM sies.report_resumen_consulta2023";
   
    
    
   if (!Seguridad::tienePermiso('reportesiessigesh')) {
       throw new MethodNotAllowedHttpException('No tiene permitido ejecutar esta acción');
   }

   // Crear una nueva instancia del reporte
   $reporte = new Reporte();

   $reporte->titulo="reporte_resumenconsulta2023";
   $reporte->seleccion=$sql;
   // Definir el tipo de archivo
   $tipo_archivo = "csv-pesado";

   // Llamar al método para generar el reporte
   \app\components\Listado\Listado::widget(array(
       'reporte'=>$reporte,
       'tipo' => $tipo_archivo
   ));

   // Preparar el nombre del archivo a descargar
   $fileName = preg_replace("/ /", "_", $reporte->titulo) . "." . $tipo_archivo;

   // Enviar el archivo generado al usuario
   $filePath = Yii::getAlias('@runtime') . '/' . $fileName; // Asegúrate de que la ruta sea correcta
   if (file_exists($filePath)) {
       Yii::$app->response->sendFile($filePath)->send();
       Yii::$app->end();
   } else {
       throw new \Exception("El archivo no se pudo encontrar: $filePath");
   }
}
public function actionExportinternacion($accion=null){
    if(Seguridad::tienePermiso('reportesiessigesh')){

        $consulta=new Consulta();
        $consulta->createCsv();
        

        return $this->actionInternacionviedma();

       
}
}
   public function actionReportesiessigeshturno()
{
    // Establecer la consulta SQL para el año proporcionado
   $sql = "SELECT * FROM sies.report_sies_sigesh_turno_2023";
   
    
    
    if (!Seguridad::tienePermiso('reportesiessigesh')) {
        throw new MethodNotAllowedHttpException('No tiene permitido ejecutar esta acción');
    }

    // Crear una nueva instancia del reporte
    $reporte = new Reporte();

    $reporte->titulo="reporte_siesSigeshTurno2023";
    $reporte->seleccion=$sql;
    // Definir el tipo de archivo
    $tipo_archivo = "csv-pesado";

    // Llamar al método para generar el reporte
    \app\components\Listado\Listado::widget(array(
        'reporte'=>$reporte,
        'tipo' => $tipo_archivo
    ));

    // Preparar el nombre del archivo a descargar
    $fileName = preg_replace("/ /", "_", $reporte->titulo) . "." . $tipo_archivo;

    // Enviar el archivo generado al usuario
    $filePath = Yii::getAlias('@runtime') . '/' . $fileName; // Asegúrate de que la ruta sea correcta
    if (file_exists($filePath)) {
        Yii::$app->response->sendFile($filePath)->send();
        Yii::$app->end();
    } else {
        throw new \Exception("El archivo no se pudo encontrar: $filePath");
    }
}
public function actionReportturno()
{
    $sql = "SELECT * FROM sies.report_turno";
   
    
    
    if (!Seguridad::tienePermiso('reportesiessigesh')) {
        throw new MethodNotAllowedHttpException('No tiene permitido ejecutar esta acción');
    }

    // Crear una nueva instancia del reporte
    $reporte = new Reporte();

    $reporte->titulo="report_turno2023";
    $reporte->seleccion=$sql;
    // Definir el tipo de archivo
    $tipo_archivo = "csv-pesado";

    // Llamar al método para generar el reporte
    \app\components\Listado\Listado::widget(array(
        'reporte'=>$reporte,
        'tipo' => $tipo_archivo
    ));

    // Preparar el nombre del archivo a descargar
    $fileName = preg_replace("/ /", "_", $reporte->titulo) . "." . $tipo_archivo;

    // Enviar el archivo generado al usuario
    $filePath = Yii::getAlias('@runtime') . '/' . $fileName; // Asegúrate de que la ruta sea correcta
    if (file_exists($filePath)) {
        Yii::$app->response->sendFile($filePath)->send();
        Yii::$app->end();
    } else {
        throw new \Exception("El archivo no se pudo encontrar: $filePath");
    }
}
public function actionReportesiessigesh($anio)
{
    // Establecer la consulta SQL para el año proporcionado
   $sql = "SELECT * FROM sies.report_sies_sigesh_" . $anio;
   
    
    
    if (!Seguridad::tienePermiso('reportesiessigesh')) {
        throw new MethodNotAllowedHttpException('No tiene permitido ejecutar esta acción');
    }

    // Crear una nueva instancia del reporte
    $reporte = new Reporte();

    $reporte->titulo="reporte_siesSigesh".$anio;
    $reporte->seleccion=$sql;
    // Definir el tipo de archivo
    $tipo_archivo = "csv-pesado";

    // Llamar al método para generar el reporte
    \app\components\Listado\Listado::widget(array(
        'reporte'=>$reporte,
        'tipo' => $tipo_archivo
    ));

    // Preparar el nombre del archivo a descargar
    $fileName = preg_replace("/ /", "_", $reporte->titulo) . "." . $tipo_archivo;

    // Enviar el archivo generado al usuario
    $filePath = Yii::getAlias('@runtime') . '/' . $fileName; // Asegúrate de que la ruta sea correcta
    if (file_exists($filePath)) {
        Yii::$app->response->sendFile($filePath)->send();
        Yii::$app->end();
    } else {
        throw new \Exception("El archivo no se pudo encontrar: $filePath");
    }
}


        public function actionReporte($anio){    

        if (!Seguridad::tienePermiso('reportesiessigesh')) {
            throw new MethodNotAllowedHttpException('No tiene permitido ejecutar esta acción');
        }
      $sql="select * from sies.report_sies".$anio;
 

    // Crear una nueva instancia del reporte
    $reporte = new Reporte();

    $reporte->titulo="reporte_sies".$anio;
    $reporte->seleccion=$sql;
    // Definir el tipo de archivo
    $tipo_archivo = "csv-pesado";

    // Llamar al método para generar el reporte
    \app\components\Listado\Listado::widget(array(
        'reporte'=>$reporte,
        'tipo' => $tipo_archivo
    ));

    // Preparar el nombre del archivo a descargar
    $fileName = preg_replace("/ /", "_", $reporte->titulo) . "." . $tipo_archivo;

    // Enviar el archivo generado al usuario
    $filePath = Yii::getAlias('@runtime') . '/' . $fileName; // Asegúrate de que la ruta sea correcta
    if (file_exists($filePath)) {
        Yii::$app->response->sendFile($filePath)->send();
        Yii::$app->end();
    } else {
        throw new \Exception("El archivo no se pudo encontrar: $filePath");
    }
        
    }
    public function actionExport($accion=null,$anio)
    {

        Consulta::$table='consulta'.$anio;
        $request = Yii::$app->request;
        $reporte= new Reporte();

        
        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

           $valor= Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
             
            Yii::$app->end();
        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
          
            $dataProvider=Yii::$app->session->get('consulta-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
//onclick'=>'cargar();

        }else{
            // Process for non-ajax request
            return $this->redirect([
                'index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null,$anio)
    {

        $request = Yii::$app->request;
        Consulta::$table='consulta'.$anio;
        $model = new Consulta();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionRecepcioncmp()
    {

        $request = Yii::$app->request;

        if ($request->post()) {

                // archivo

                $directorio = "../scripts/estadistica/";

                if (is_readable($directorio."tx.cmp")) {

                    // ejecuto script de uncmp y cargar en la base
                    chmod ( $directorio."tx.cmp" , 0666);
                    shell_exec($directorio."convertir.sh");

                }else{
                    Yii::$app->session->setFlash('errorcmp');
                    return $this->refresh();
                }

                // consulta sobre los datos de txconsu y txmorbi
                $query = (new \yii\db\Query())->select(["numero"])->from("sies.txconsu");    
                $count_consulta = $query->count();

                $query = (new \yii\db\Query())->select(["numero"])->from("sies.txmorbi");    
                $count_morbimor = $query->count();
                
                if ($count_consulta==0 and $count_morbimor==0){
                    // archivos vacio
                    Yii::$app->session->setFlash('errorvacio');
                    return $this->refresh();
                }

                // busco origen, trimestre y año de los dos archivos
                $query = (new \yii\db\Query())->select(["hospital","fecha"])->from("sies.txconsu");    
                $consulta = $query->one();
                $hospital=null;
                $periodo_c=null;
                $trimestre=null;
                $anio=null;
                if($consulta){
                    $periodo_c=Metodos::trimestre($consulta['fecha']);
                    $hospital=$consulta['hospital'];   
                    $trimestre=$periodo_c[0];
                    $anio=$periodo_c[1];
                    }

                $query = (new \yii\db\Query())->select(["establec","fechaegres"])->from("sies.txmorbi"); 
                $morbimor = $query->one();

                $periodo_m=Metodos::trimestre($morbimor['fechaegres']);

           
                if ($hospital!=null){
                    $hospital=$morbimor['establec'];
                }
                    

                if ($trimestre==0){
                    $trimestre=$periodo_m[0];
                }


                if ($anio==0){
                    $anio=$periodo_m[1];
                }    

                $this->redirect(['confirmarorigen', 'hospital'=>$hospital, 'trimestre'=>$trimestre, 'anio'=>$anio]);

        }

        return $this->render('recepcioncmp', []);
    }

    public function actionConfirmarorigen($hospital=null,$trimestre=null,$anio=null)
    {

        $request = Yii::$app->request;

        if($request->isAjax) {    // modal para cambiar contraseña                        

            Yii::$app->response->format = Response::FORMAT_JSON;

            if (isset($_GET['tx']) and $_GET['tx']=='morbi'){

                $query = (new \yii\db\Query())
                ->select([
                    "establec",
                    "servicio",
                    "fechaegres",
                    "ndedocumen",
                    "apellidos"
                ])
                ->from("sies.txmorbi");
                $columns=[
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'establec',
                        'label'=>'Hospital'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'servicio',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'fechaegres',
                        'label'=>'Fecha Egreso'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'ndedocumen',
                        'label'=>'Documento'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'apellidos',
                        'label'=>'Paciente'
                    ],
                ];        
                        
                $title="MORBIMOR";
            }else{

                $query = (new \yii\db\Query())
                ->select([
                    "hospital",
                    "servicio",
                    "fecha",
                    "nrodoc",
                    "apellido",
                ])
                ->from("sies.txconsu");
                $columns=[
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'hospital',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'servicio',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'fecha',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'nrodoc',
                        'label'=>'Documento'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'apellido',
                        'label'=>'Paciente'
                    ],
                ];        
                $title="CONSULTA";
                    
            }

            $datos = $query->all();

            $dataProvider = new ArrayDataProvider([
                'allModels' => $datos,
            ]);

            $dataProvider->sort = false;
    
            return [
                'title'=> 'Verificar Datos',
                'content' => $this->renderAjax('_verificar', [
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'title'=> $title,
                    ])
            ];

        } else {
            
            if ($post=$request->post()) {

                if ($post['hospital'] and $post['trimestre'] and $post['anio']){

                    // errores
                    if ($post['hospital']==null or $post['anio']<2020 or
                        $post['trimestre']<1 or $post['trimestre']>4){
                        // error en los datos
                        Yii::$app->session->setFlash('errororigen');
                        return $this->refresh();
                    }

                    $this->redirect(['comparacion', 'hospital'=>$post['hospital'], 'trimestre'=>$post['trimestre'], 'anio'=>$post['anio']]);

                }else{

                    Yii::$app->session->setFlash('errororigen');
                    return $this->refresh();

                }

            }

            return $this->render('confirmarorigen', [
                'hospital' => $hospital,
                'trimestre' => $trimestre,
                'anio' => $anio,
            ]);
        }

    }

    public function actionComparacion($hospital,$trimestre,$anio)
    {

        $request = Yii::$app->request;

        switch ($trimestre) {
            case 1:
                $desde=$anio."-01-01";
                $hasta=$anio."-03-31";
                break;
            case 2:
                $desde=$anio."-04-01";
                $hasta=$anio."-06-30";
                break;
            case 3:
                $desde=$anio."-07-01";
                $hasta=$anio."-09-30";
                break;
            case 4:
                $desde=$anio."-10-01";
                $hasta=$anio."-12-31";
                break;
            default:
                Yii::$app->session->setFlash('errorperiodo');
                return $this->refresh();
        }

        if ($request->post()) {

            // operacion de todo OK. reemplazar los datos existentes
            $connection = Yii::$app->getDb();

            try {

                $sql="DELETE FROM sies.consulta".$anio."
                        WHERE fecha>='".$desde."' and fecha<='".$hasta."' and hospital='".$hospital."'";
                
                $command = $connection->createCommand($sql);
                $command->execute();

                $sql="  INSERT INTO sies.consulta".$anio." (
                        numero,hospital,puesto,servicio,medico,fecha,nrohc,nrodoc,
                        apellido,edad,cdgosexo,obra,primera,cemb,cns,diag
                        )
                        SELECT
                        numero,'".$hospital."',puesto,servicio,medico,fecha,nrohc,nrodoc,
                        apellido,edad,cdgosexo,obra,primera,cemb,cns,diag::text
                        FROM sies.txconsu";

                $command = $connection->createCommand($sql);
                $command->execute();


                $sql="  DELETE FROM sies.morbimor".$anio."
                        WHERE fechaegres>='".$desde."' and fechaegres<='".$hasta."' and establec='".$hospital."'";
                
                $command = $connection->createCommand($sql);
                $command->execute();

                $sql="  INSERT INTO sies.morbimor".$anio." (
                        numero,historiacl,establec,apellidos,ndedocumen,tipo,fechadenac,edading,
                        sexo,locresid,depresid,provresid,paisresid,tieneobras,codobrasoc,codhospit,
                        fechaingr,servicio,diasest,fechapase,servicio01,diasest01,fechapas01,servicio02,
                        diasest02,fechapas02,servicio03,diasest03,fechapas03,servicio04,diasest04,fechaegres,
                        totdiasest,egresopor,operado,diagprinc,otrodiag,otrodiag1,otrodiag2,otrodiag3,
                        producidop,operimport,lugardonde,otraoper,otraoper1,codigodele,otrascir,diascir,
                        termina,tparto,totctrls,condnacer,sexonaci,peso,condnace01,sexonaci01,
                        peso01,condnace02,sexonaci02,peso02,estudios,trabajos,trabajtxt,htalorig,
                        htaldest,nrodocmad,fechaparto,edadgest,paridad,parto
                        )
                        SELECT 
                        numero,historiacl,'".$hospital."',apellidos,ndedocumen,tipo,fechadenac,edading,
                        sexo,locresid,depresid,provresid,paisresid,tieneobras,codobrasoc,codhospit,
                        fechaingr,servicio,diasest,fechapase,servicio01,diasest01,fechapas01,servicio02,
                        diasest02,fechapas02,servicio03,diasest03,fechapas03,servicio04,diasest04,fechaegres,
                        totdiasest,egresopor,operado,diagprinc,otrodiag,otrodiag1,otrodiag2,otrodiag3,
                        producidop,operimport,lugardonde,otraoper,otraoper1,codigodele,otrascir,diascir,
                        termina,tparto,totctrls,condnacer,sexonaci,peso,condnace01,sexonaci01,
                        peso01,condnace02,sexonaci02,peso02,estudios,trabajos,trabajtxt,htalorig,
                        htaldest,nrodocmad,fechaparto,edadgest,paridad,parto
                        FROM sies.txmorbi";

                $command = $connection->createCommand($sql);
                $command->execute();

                // backup de cmp y acomodo carpeta de recepcion

                $directorio = "../scripts/estadistica/";
                $archivos_fisicos = FileHelper::findFiles($directorio);

                foreach ($archivos_fisicos as $key=>$value){
                    if ($value==$directorio."tx.cmp"){

                        // muevo a backup
                        $nombre="tx-".substr($hospital,9)."-".$trimestre."-".$anio."-".date("Ymd-His").".cmp";
                        rename($value,"../scripts/backupcmp/".$nombre);

                    }else{
                        if ($value==$directorio."convertir.sh")
                            continue;
                        if ($value==$directorio."uncmp.exe")
                            continue;
                        
                        FileHelper::unlink($value);
                    }
                }

                return $this->render('datosguardados', [
                    'hospital'=>$hospital,
                    'trimestre'=>$trimestre,
                    'anio'=>$anio
                    ]);
    
            } catch (yii\db\Exception $e ) {
                return $this->render('datosguardados', [
                    'hospital'=>$hospital,
                    'trimestre'=>$trimestre,
                    'anio'=>$anio,
                    'error'=>true,
                    ]);
            }

            Yii::$app->die();

        }

        $query = (new \yii\db\Query())
            ->select([new \yii\db\Expression("EXTRACT(MONTH FROM fecha) as mes, count(*) as cantidad")])
            ->from("sies.consulta".$anio)
            ->where(['>=','fecha',$desde]) 
            ->andWhere(['<=', 'fecha', $hasta])
            ->andWhere(['=', 'hospital', $hospital])
            ->orderBy('mes')
            ->groupBy(['mes']);

        $consulta_base = $query->all();

        $query = (new \yii\db\Query())
            ->select([new \yii\db\Expression("EXTRACT(MONTH FROM fecha) as mes, count(*) as cantidad")])
            ->from("sies.txconsu")
            ->where(['>=','fecha',$desde]) 
            ->andWhere(['<=', 'fecha', $hasta])
            ->orderBy('mes')
            ->groupBy(['mes']);

        $consulta_tx = $query->all();

        $query = (new \yii\db\Query())
            ->select([new \yii\db\Expression("EXTRACT(MONTH FROM fechaegres) as mes, count(*) as cantidad")])
            ->from("sies.morbimor".$anio)
            ->where(['>=','fechaegres',$desde]) 
            ->andWhere(['<=', 'fechaegres', $hasta])
            ->andWhere(['=', 'establec', $hospital])
            ->orderBy('mes')
            ->groupBy(['mes']);

        $morbimor_base = $query->all();

        $query = (new \yii\db\Query())
            ->select([new \yii\db\Expression("EXTRACT(MONTH FROM fechaegres) as mes, count(*) as cantidad")])
            ->from("sies.txmorbi")
            ->where(['>=','fechaegres',$desde]) 
            ->andWhere(['<=', 'fechaegres', $hasta])
            ->orderBy('mes')
            ->groupBy(['mes']);

        $morbimor_tx = $query->all();

        return $this->render('comparacion', [
            'consulta_base' => $consulta_base,
            'consulta_tx' => $consulta_tx,
            'morbimor_base' => $morbimor_base,
            'morbimor_tx' => $morbimor_tx,
            'hospital'=>$hospital,
            'trimestre'=>$trimestre,
            'anio'=>$anio
            ]);

    }

    public function actionSubirarchivo()
    {

        // esta accion recibe el archivo en la clase UploadedFile

        Yii::$app->response->format = Response::FORMAT_JSON;
        $maxSize = 10485760;   

        $file = UploadedFile::getInstanceByName('archivo_upload'); // clases para subir archivos

        // datos para crear el thumbnail
        $allowedExtensions = array("cmp","CMP");

        $error = null;

        if ($file) {
            if ($file->size > $maxSize)
                $error = "Archivo muy grande. Máximo 10Mb";
            else if (!in_array($file->extension, $allowedExtensions))
                $error = "Tipo de archivo incorrecto";
        } else {
            $error = "No es posible subir el archivo";
        }

        if (!$error) {

            // preparo directorios con el sessionID
            $directorio = "../runtime/";
            $carpeta = "../scripts/estadistica/";

            if (is_readable($directorio . $file->name)) {
                // Borro en el disco el archivo anterior, si es que existe y guardo la nueva
                FileHelper::unlink($directorio . $file->name);
            }

            if ($file->saveAs($directorio . $file->name)) {

                rename($directorio . $file->name, $carpeta . 'tx.cmp');                
                return [
                    'thumbnail' => $file->name,
                ];
            } else {
                $error = "No es posible guardar el archivo";
            }
        }

        return [
            'thumbnail' => "<p style='color:red;'>ERROR. $error</p>"
        ];
    }
}
