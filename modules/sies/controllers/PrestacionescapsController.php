<?php

namespace app\modules\sies\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
//
use app\modules\sies\models\Prestacionescaps;
use yii\data\SqlDataProvider;
/**
 * LogscriptController implements the CRUD actions for Logscript model.
 */
class PrestacionescapsController extends Controller
{

    public function actionIndex()
    {    
       
        //$model= new Prestacionescaps();
        //$searchModel = new LogscriptSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT * FROM sies.prestaciones_caps pc ORDER BY pc.localidad asc',
        ]);
        //$columnas=Metodos::obtenerColumnas($model,'mpi/prestacionescaps/index');
        $columnas = [
          [
            'attribute'=> 'localidad',
            'label' => 'Localidad',
          ],
          [
            'attribute'=> 'efector',
            'label' => 'Efector',
          ],
          [
            'attribute'=> 'cant_ecg',
            'label' => 'Cant ECGs',
          ],
          [
            'attribute'=> 'cant_eco',
            'label' => 'Cant ECOs',
          ],
        ];



        /*$permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
                    'list'=>Seguridad::tienePermiso('list'),
        ];*/

        return $this->render('index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            //'permisos'=>$permisos,
        ]);
    }




}
