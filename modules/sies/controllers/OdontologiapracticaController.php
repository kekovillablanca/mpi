<?php

namespace app\modules\sies\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
//
use app\modules\sies\models\Odontologia;
use app\modules\sies\models\OdontologiaPractica;
use app\modules\sies\models\OdontologiaPracticaSearch;
use app\modules\sies\models\STNomenclador;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;

/**
 * OdontologiapracticaController implements the CRUD actions for OdontologiaPractica model.
 */
class OdontologiapracticaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select','nomenclador'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select','nomenclador'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    private function viewHtml($modelMaestro,$imprimir,$anio=null,$tipo=null){
        $html=' <div style="font-family: Helvetica, sans-serif"> 

        <style>
table {  border:1px solid  #337ab7;    border-collapse: separate; border-radius:5px; border-spacing:0;
        }
td, th  { border: none; }
td + td, th + th {border-left:1px solid #ddd}
th, tr td  {border-bottom:1px solid #ddd}
th:first-child { border-radius:5px 0 0 0; border-left:solid}
th:last-child { border-radius:0 5px 0 0;border-right:solid }
tfoot td:first-child {  border-radius:0 0 0 5px ;border-left:solid}
tfoot td:last-child {  border-radius:0 0 5px 0;border-right:solid}
</style>';


// inicio datos subjetivos
    
        
            $html.='<br>
            <b>Laboratorio pacientes</b>
            <div><table class="table table-condensed" ><tbody>';
            $html.='<thead style="background: #337ab7;color: white;"><tr>
            <th>Anio</th>
            <th>Area</th>
            <th>Mes</th>
            <th>Tipo</th>
            <th>Cantidad de pacientes</th>';
            if($imprimir==false){
                $html.= '<th style="width:2px;">'.Html::a('<button type="button" class="btn btn-primary"style="width:2px;display: contents; text-align:center;" ><i class="glyphicon glyphicon-download-alt"></i></button>', ['html','anio'=>$anio,'tipo'=>$tipo], ['class' => "btn btn-primary"]).'</th>';
            }
          
            $html.='</tr>
            </thead>';
        foreach($modelMaestro as $modelMaestro){
            
        
           
            $html.='<tr>';
            if(!isset($modelMaestro->anio))
                $html.='<td> (no definido) </td>';
            else
                $html.='<td>'.$modelMaestro->anio.'</td>';
            if(!isset($modelMaestro->area['descrip']))
                $html.='<td> (no definido) </td>';
            else
                $html.='<td>'.$modelMaestro->area['descrip'].'</td>';
            if(!isset($modelMaestro->mes))
                $html.='<td> (no definido) </td>';
            else
                $html.='<td>'.$modelMaestro->mes.'</td>';
            if(!isset($modelMaestro->sttipo->nombre))
                $html.='<td> (no definido) </td>';
            else
                $html.='<td>'.$modelMaestro->sttipo->nombre.'</td>';
            if(!isset($modelMaestro->cantidad_paciente))
                $html.='<td>0</td>';
            else
                $html.='<td>'.$modelMaestro->cantidad_paciente.'</td>';
            
            $html.='</tr>';
           
        }        
        $html.='</tbody></table></div>';  
        return $html;
    }
    public function actionHtml($anio=null,$tipo=null){
        $modelMaestro=Odontologia::find()->where(['odontologia.anio'=>$anio,'odontologia.id_st_tipo'=>$tipo])->all();
        $csv = 'ANIO,AREA,MES,TIPO,CANTIDAD PACIENTES';
        $arrayDeCsv=[];
        $array2csv=[];
        $i=0;
        $j=0;
        foreach($modelMaestro as $columna){
            
            if(isset($columna->anio))
                $arrayDeCsv[$j]=$columna->anio;
            else{
                $arrayDeCsv[$j]=0;
            }
            $j++;
            if(isset($columna->area['descrip']))
            $arrayDeCsv[$j]=$columna->area['descrip'];
            else{
                $arrayDeCsv[$j]='no definido';
            }
            $j++;
            if(isset($columna->mes))
            $arrayDeCsv[$j]=$columna->mes;
            else{
                $arrayDeCsv[$j]=0;
            }
            $j++;
            if(isset($columna->sttipo->nombre))
            $arrayDeCsv[$j]=$columna->sttipo->nombre;
            else{
                $arrayDeCsv[$j]='no definido';
            }
            $j++;
            if(isset($columna->cantidad_paciente))
            $arrayDeCsv[$j]=$columna->cantidad_paciente;
            else{
                $arrayDeCsv[$j]=0;
            }
            $j++;
            $array2csv[$i]=$arrayDeCsv;
            $j=0;
            $i++;
        }
        
      
      foreach($array2csv as $columna){
    

        $csv .= "\n".implode(',', $columna);


      }
     
     
       /* header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");*/
        return \Yii::$app->response->sendContentAsFile($csv, 'Reporte encabezado'.$anio.'.csv', [
         'mimeType' => 'text/csv', 
         'inline'   => false
     
     ]);
      
    }
    /**
     * Lists all OdontologiaPractica models.
     * @return mixed
     */
    public function actionIndex($id_maestro=null,$anio=null,$id_st_tipo=null)
    {    

        $searchModel = new OdontologiaPracticaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model= new OdontologiaPractica();
        $columnas=null;
        $html=null;
        $totalMaestro=false;
        $modelMaestro=null;
       if($anio!=null && $id_st_tipo!=null && $id_maestro!=null){
       
        $modelMaestro=Odontologia::findOne($id_maestro);
     
        $dataProvider->query->andWhere(['id_odontologia'=>$id_maestro]);
    
        
        }
        else{
       
            $modelMaestro=null;
            $dataProvider->query->andWhere(['odontologia.anio'=>$anio])->andWhere(['odontologia.id_st_tipo'=>$id_st_tipo])->all();
           
            $modelMaestro=Odontologia::find()->where(['odontologia.anio'=>$anio,'odontologia.id_st_tipo'=>$id_st_tipo])->all();
            $totalMaestro=true;
          
            
            $html=$this->viewHtml($modelMaestro,false,$anio,$id_st_tipo);
                
              
         
        }
        
        $columnas=Metodos::obtenerColumnas($model,'sies/odontologiapractica/index');

      

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
            'modelMaestro'=>$modelMaestro,
            'totalMaestro'=>$totalMaestro,
            'html'=>$html
        ]);
    }

    /**
     * Displays a single OdontologiaPractica model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView($model);

        if(isset($_GET['pdf'])){

            $titulo='Datos de OdontologiaPractica';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $request = Yii::$app->request;
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "OdontologiaPractica #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $this->findModel($id),
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new OdontologiaPractica model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_maestro)
    {
        $request = Yii::$app->request;

        if (!$id_maestro)
            return $this->redirect(['/site/index']);

        $model = new OdontologiaPractica();  
        // valores por defecto
        $model->cantidad_practica=0;
        $model->id_odontologia=$id_maestro;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "OdontologiaPractica #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Crear Odontologia Practica",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing OdontologiaPractica model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Odontologia Practica #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Editar Odontologia Practica #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing OdontologiaPractica model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($this->findModel($id)->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error en la base de datos.',500);
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the OdontologiaPractica model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OdontologiaPractica the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OdontologiaPractica::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null)
    {

        $request = Yii::$app->request;
        $reporte= new Reporte();

        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();

        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            $dataProvider=Yii::$app->session->get('odontologiapractica-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new OdontologiaPractica();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionNomenclador($q = null,$id = null)
    {

        $request = Yii::$app->request;

        if($request->isAjax){
		
            Yii::$app->response->format = Response::FORMAT_JSON;

            $results = ['results' => ['id' => '', 'text' => '']];
            if (!is_null($q)) {

                $q="'%".strtoupper(addslashes($q))."%'";

                $sql = "SELECT id as id, concat(id,' ',nombre) as text
                        FROM sies.st_nomenclador
                        WHERE (upper(nombre) LIKE $q OR id LIKE $q) AND (clasificacion=4) LIMIT 50";

                $query= Yii::$app->db->createCommand($sql);
                $data = $query->queryAll();
      
                $results['results'] = array_values($data);
            }
            elseif ($id > 0) {
                $results['results'] = ['id' => $id, 'text' => STNomenclador::findOne($id)->nombre];
            }
            return $results;

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

}
