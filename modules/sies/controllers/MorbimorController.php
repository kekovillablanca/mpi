<?php

namespace app\modules\sies\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
//
use app\modules\sies\models\Morbimor;
use app\modules\sies\models\MorbimorSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;

/**
 * MorbimorController implements the CRUD actions for Morbimor model.
 */
class MorbimorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Morbimor models.
     * @return mixed
     */
    public function actionIndex($anio)
    {    
       

        Morbimor::$table='morbimor'.$anio;
        $model= new Morbimor();
        $searchModel = new MorbimorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $columnas=Metodos::obtenerColumnas($model,'sies/morbimor/index');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
            'anio'=>$anio
        ]);
    }


    /**
     * Displays a single Morbimor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$anio)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id,$anio);
        $attributes = $model->attributeView($model);

        if(isset($_GET['pdf'])){

            $titulo='Datos de Morbimor';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $request = Yii::$app->request;
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Morbimor #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $this->findModel($id,$anio),
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true,'anio'=>$anio],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id,'anio'=>$anio],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Morbimor model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
/*
        $request = Yii::$app->request;
        $model = new Morbimor();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Morbimor #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Crear Morbimor",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/
    }

    /**
     * Updates an existing Morbimor model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
/*        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "Morbimor #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Editar Morbimor #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/
    }

    /**
     * Delete an existing Morbimor model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
/*        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($this->findModel($id)->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error en la base de datos.',500);
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/
    }

    /**
     * Finds the Morbimor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Morbimor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id,$anio)
    {
        Morbimor::$table='morbimor'.$anio;

        if (($model = Morbimor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null,$anio)
    {
        $request = Yii::$app->request;
        Morbimor::$table='morbimor'.$anio;
        $reporte= new Reporte();
    
        
        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

           $valor= Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
             
            Yii::$app->end();
        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
    
            $dataProvider=Yii::$app->session->get('morbimor-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
//onclick'=>'cargar();

        }else{
            // Process for non-ajax request
            return $this->redirect([
                'index']);
        }
        /*$connection = Yii::$app->db; 
        $columnas=$connection->createCommand("SELECT m.id, m.numero, m.historiacl, m.establec, m.apellidos, m.ndedocumen, m.tipo, m.fechadenac, m.edading, m.sexo, m.locresid, m.depresid, m.provresid, m.paisresid, m.tieneobras, m.codobrasoc, m.codhospit, m.fechaingr, 
        m.servicio, m.diasest, m.fechapase, m.servicio01, m.diasest01, m.fechapas01, m.servicio02, m.diasest02, m.fechapas02, m.servicio03, m.diasest03, m.fechapas03, m.servicio04, m.diasest04, m.fechaegres, m.totdiasest, m.egresopor, m.operado, m.diagprinc,d.nombre, m.otrodiag, m.otrodiag1, m.otrodiag2, m.otrodiag3, m.producidop, 
        m.operimport, m.lugardonde, m.otraoper, m.otraoper1, m.codigodele, m.otrascir, m.diascir, m.termina, m.tparto, m.totctrls, m.condnacer, m.sexonaci, m.peso, m.condnace01, m.sexonaci01, m.peso01, m.condnace02, m.sexonaci02, m.peso02, m.estudios, m.trabajos, m.trabajtxt, m.htalorig, m.htaldest, m.nrodocmad, m.fechaparto, m.edadgest, m.paridad, m.parto, m.horaingr, m.horapase, m.horapase01, m.horapase02, 
        m.horapase03, m.horaegres, m.servaux, m.servaux01, m.servaux02, m.servaux03, m.servaux04
        FROM sies.morbimor".$anio." m LEFT JOIN consultasestadisticas.diagnostico d on (d.codigo=m.diagprinc) ")->queryAll();

        $csv = 'ID,NUMERO,HISTORIACL,ESTABLEC,APELLIDOS,NDEDOCUMEN,TIPO,FECHADENAC,EDADING,SEXO,LOCRESID,DEPRESID,PROVRESID,PAISRESID,TIENEOBRAS,CODOBRASOC,CODHOSPIT,FECHAINGR,SERVICIO,DIASEST,FECHAPASE,SERVICIO01,DIASEST01,FECHAPAS01,SERVICIO02,DIASEST02,FECHAPAS02,SERVICIO03,DIASEST03,FECHAPAS03,SERVICIO04,DIASEST04,FECHAEGRES,TOTDIASEST,EGRESOPOR,OPERADO,DIAGPRINC,NOMBRE DIAGNOSTICO,OTRODIAG,OTRODIAG1,OTRODIAG2,OTRODIAG3,PRODUCIDOP,OPERIMPORT,LUGARDONDE,OTRAOPER,OTRAOPER1,CODIGODELE,OTRASCIR,DIASCIR,TERMINA,TPARTO,TOTCTRLS,CONDNACER,SEXONACI,PESO,CONDNACE01,SEXONACI01,PESO01,CONDNACE02,SEXONACI02,PESO02,ESTUDIOS,TRABAJOS,TRABAJTXT,HTALORIG,HTALDEST,NRODOCMAD,FECHAPARTO,EDADGEST,PARIDAD,PARTO,HORAINGR,HORAPASE,HORAPASE01,HORAPASE02,HORAPASE03,HORAEGRES,SERVAUX,SERVAUX01,SERVAUX02,SERVAUX03,SERVAUX04';
        
        foreach($columnas as $columna){
         $csv .= "\n".implode(',', $columna);   
        
        }
     
     
       /* header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");*/
        /*return \Yii::$app->response->sendContentAsFile($csv, 'morbimor'.$anio.'.csv', [
            'mimeType' => 'text/csv', 
            'inline'   => false
        ]);*/
     }
    
    

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null,$anio)
    {

        $request = Yii::$app->request;
        Morbimor::$table='morbimor'.$anio;
        $model = new Morbimor();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

}
