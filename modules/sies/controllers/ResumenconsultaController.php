<?php

namespace app\modules\sies\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
//
use app\modules\sies\models\ResumenConsulta;
use app\modules\sies\models\ResumenConsultaSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\modules\admin\models\UsuarioEstadistica;
use app\modules\sies\models\MArea;
use app\modules\sies\models\MRango;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;

/**
 * ResumenconsultaController implements the CRUD actions for ResumenConsulta model.
 */
class ResumenconsultaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select','resumen'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select','resumen'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ResumenConsulta models.
     * @return mixed
     */
    public function actionIndex($anio)
    {    
       
        $model= new ResumenConsulta();
        $searchModel = new ResumenConsultaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // filtro por año
        $dataProvider->query->andWhere(['anio'=>$anio]);

        $user_estad = UsuarioEstadistica::findOne(['id_usuario'=>Yii::$app->user->id]);

        if ($user_estad){

            if ($user_estad->marea->codigo!=999){   
                // organismo central
                $dataProvider->query->andFilterWhere(['m_puesto.codarea' => $user_estad->marea->codigo]);    
            }
            

        }else{
            // usuario no habilitado
            Yii::$app->end();
        }


        $columnas=Metodos::obtenerColumnas($model,'sies/resumenconsulta/index');

        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
                    'view'=>Seguridad::tienePermiso('view'),
                    'create'=>Seguridad::tienePermiso('create'),
                    'update'=>Seguridad::tienePermiso('update'),
                    'delete'=>Seguridad::tienePermiso('delete'),
                    'export'=>Seguridad::tienePermiso('export'),
                    'select'=>Seguridad::tienePermiso('select'),
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'permisos'=>$permisos,
            'anio'=>$anio,
        ]);
    }


    /**
     * Displays a single ResumenConsulta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model=$this->findModel($id);
        $attributes = $model->attributeView($model);

        if(isset($_GET['pdf'])){

            $titulo='Datos de ResumenConsulta';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=>'Registro #'.$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $request = Yii::$app->request;
            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "ResumenConsulta #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $this->findModel($id),
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new ResumenConsulta model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
/*        $request = Yii::$app->request;
        $model = new ResumenConsulta();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "ResumenConsulta #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Crear ResumenConsulta",
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/
    }

    /**
     * Updates an existing ResumenConsulta model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-pjax',
                            'title'=> "ResumenConsulta #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error en la base de datos.',500);
                }
            }

            return [
                'title'=> "Editar ResumenConsulta #".$id,
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }

    }

    /**
     * Delete an existing ResumenConsulta model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
/*        $request = Yii::$app->request;
        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($this->findModel($id)->delete()){
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error en la base de datos.',500);
            }

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
*/
    }

    /**
     * Finds the ResumenConsulta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ResumenConsulta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ResumenConsulta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null)
    {

        $request = Yii::$app->request;
        $reporte= new Reporte();

        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];

            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;

            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();

        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            $dataProvider=Yii::$app->session->get('resumenconsulta-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);

            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,
                    ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];


        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Personalizar vista del grid por usuario. model VISTA.
     * For ajax request will return json object
     * and for non-ajax request process la vistat
     * @param model $seleccion
     * @return mixed
     */
    public function actionSelect($accion=null)
    {
        $request = Yii::$app->request;
        $model = new ResumenConsulta();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;

            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];

                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }

                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }

                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);

                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                $vista->save();

                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

            }

            // columnas mostradas actualmente
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);

            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionResumen()
    {

        $model = new ResumenConsulta();

        $user_estad = UsuarioEstadistica::findOne(['id_usuario'=>Yii::$app->user->id]);
        if ($user_estad){

            $cod_area= MArea::findOne(['id'=>$user_estad->id_m_area])->codigo;
        }else{
            Yii::$app->session->setFlash('usuarionohabilitado');
            return $this->render('resumen', [
                'model' => $model,
                'cod_area'=>0,
            ]);
        }
    
        if (array_key_exists ( 'encabezado' , $_POST)){          // el primer submit se abre modal

            // verifico datos
            if ($_POST['ResumenConsulta']['anio']>=2021 and
                $_POST['ResumenConsulta']['mes']>=1 and
                $_POST['ResumenConsulta']['mes']<=12 and
                $_POST['ResumenConsulta']['id_puesto']!=0 and
                $_POST['ResumenConsulta']['id_servicio']!=0)
            {
                // datos OK
                $datos=ResumenConsulta::find()
                ->where(['anio'=>$_POST['ResumenConsulta']['anio']])
                ->andWhere(['mes'=>$_POST['ResumenConsulta']['mes']])
                ->andWhere(['id_puesto'=>$_POST['ResumenConsulta']['id_puesto']])
                ->andWhere(['id_servicio'=>$_POST['ResumenConsulta']['id_servicio']])
                ->orderBy('id_rango')                
                ->all();
      
                if (!$datos){
                    // no encontrado, creo los registros en blanco
                    $data_rango=MRango::find()->all();
    
                    foreach($data_rango as $value){
                        
                        $modelRes = new ResumenConsulta();
                        $modelRes->anio=$_POST['ResumenConsulta']['anio'];
                        $modelRes->mes=$_POST['ResumenConsulta']['mes'];
                        $modelRes->id_puesto=$_POST['ResumenConsulta']['id_puesto'];
                        $modelRes->id_servicio=$_POST['ResumenConsulta']['id_servicio'];
                        $modelRes->id_rango=$value->id;
                        $modelRes->save();
    
                    }
    
                    $datos=ResumenConsulta::find()
                    ->where(['anio'=>$_POST['ResumenConsulta']['anio']])
                    ->andWhere(['mes'=>$_POST['ResumenConsulta']['mes']])
                    ->andWhere(['id_puesto'=>$_POST['ResumenConsulta']['id_puesto']])
                    ->andWhere(['id_servicio'=>$_POST['ResumenConsulta']['id_servicio']])
                    ->orderBy('id_rango')                
                    ->all();
                
                }
    
                Yii::$app->response->format = Response::FORMAT_JSON;
    
                return [
                    'title'=> "<h4>Carga de Resumen de Consultas</h4>",
                    'content'=>$this->renderAjax('_resumen', [
                        'datos' => $datos
                    ]),
                    'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Guardar', ['class' => 'btn btn-primary pull-right', 'name' => 'Aceptar',
                             'onclick' => 'submitDatos()'])
                ];
                    
            }

        } else if (array_key_exists ( 'datos' , $_POST)) {          // el segundo subnit

            Yii::$app->response->format = Response::FORMAT_JSON;

            $transaction = \Yii::$app->db->beginTransaction();

            try {
                $Ok=true;
                foreach($_POST['ResumenConsulta'] as $value){

                        $modelRes=ResumenConsulta::findOne([
                            'anio'=>$value['anio'],
                            'mes'=>$value['mes'],
                            'id_puesto'=>$value['id_puesto'],
                            'id_servicio'=>$value['id_servicio'],
                            'id_rango'=>$value['id_rango']
                        ]);
                        if (!$modelRes){
                            $modelRes = new ResumenConsulta();
                        }

                        $modelRes->anio=$value['anio'];
                        $modelRes->mes=$value['mes'];
                        $modelRes->id_puesto=$value['id_puesto'];
                        $modelRes->id_servicio=$value['id_servicio'];
                        $modelRes->id_rango=$value['id_rango'];
                        $modelRes->varones=$value['varones'];
                        $modelRes->mujeres=$value['mujeres'];
                        
                        if (!$modelRes->save()){
                            $Ok=false;
                            break;
                        }
                }

                if ($Ok) {
                    $transaction->commit();
                    $content='<span class="text-success">Registro guardado correctamente</span>';
                    return [
                        'title'=> "<h4>Carga de Resumen por grupo etareo</h4>",
                        'content'=>$content,
                        'footer'=> 
                        Html::button('Cerrar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
    
                } else {
                    $transaction->rollBack();
                    $content='<span class="text-danger">Los datos no se pudieron guardar. Reintente</span>';
                    return [
                        'title'=> "<h4>Carga de Resumen por grupo etareo</h4>",
                        'content'=>$content,
                        'footer'=> 
                        Html::button('Cerrar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                    ];
    
                }

            } catch (yii\db\Exception $e) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de base de datos.', 500);
            }

        }else{

            return $this->render('resumen', [
                'model' => $model,
                'cod_area'=>$cod_area,
            ]);
        }

    }
}
