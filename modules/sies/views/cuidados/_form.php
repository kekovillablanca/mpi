<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Cuidados */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="cuidados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
		$data=ArrayHelper::map(app\modules\sies\models\MArea::find()->asArray()->all(), 'id', 'descrip');
    echo $form->field($model, 'id_area')->dropDownList($data,['prompt'=>'Seleccionar...']);
	  ?>

    <?php echo $form->field($model, 'mes')->textInput();?>

    <?php echo $form->field($model, 'anio')->textInput();?>

    <?php echo $form->field($model, 'medico')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'psicologo')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'enfermero')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'trabajador_social')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'kinesiologo')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'terapista_ocupacional')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'secretario')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'voluntario')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'otros')->textInput();        // ->textarea()?>

    <?php ActiveForm::end(); ?>
    
</div>
