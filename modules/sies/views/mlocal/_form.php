<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MLocal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mlocal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'codigo')->textInput();?>

    <?php echo $form->field($model, 'descrip')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'codloc')->textInput();?>

    <?php echo $form->field($model, 'coddepar')->textInput();?>

    <?php echo $form->field($model, 'codprovi')->textInput();?>

    <?php echo $form->field($model, 'codpos')->textInput();?>

    <?php echo $form->field($model, 'codsisa')->textInput();?>

    <?php ActiveForm::end(); ?>
    
</div>
