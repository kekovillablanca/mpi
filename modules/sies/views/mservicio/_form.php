<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MServicio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mservicio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'codigo')->textInput();?>

    <?php echo $form->field($model, 'descrip')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'codnacion')->textInput();?>

    <?php echo $form->field($model, 'codnac2008')->textInput();?>

    <?php echo $form->field($model, 'snomed')->textInput();?>

    <?php echo $form->field($model, 'descrip2020')->textInput();?>

    <?php ActiveForm::end(); ?>
    
</div>
