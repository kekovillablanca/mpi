<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MPuesto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpuesto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'codigo')->textInput();?>

    <?php echo $form->field($model, 'descrip')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'codpuesto')->textInput();?>

    <?php echo $form->field($model, 'codzona')->textInput();?>

    <?php echo $form->field($model, 'codarea')->textInput();?>

    <?php echo $form->field($model, 'area')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'zona')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'nacion')->textInput();?>

    <?php echo $form->field($model, 'codsisa')->textInput(['maxlength' => true]);?>

    <?php ActiveForm::end(); ?>
    
</div>
