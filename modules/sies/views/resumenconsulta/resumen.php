<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use quidu\ajaxcrud\CrudAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


$this->title = 'Carga Resumen de Consultas';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<script>
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
function submitEncabezado(){
        
        var $form = $('#form-encabezado');
        var data = $form.serialize();

        $.ajax({
            url: '<?php echo Url::to(['resumen']) ?>',
            dataType: 'json',
            type:"POST",
            data: data,
            success: function(data) {

                $('#ajaxCrudModal').modal('show');
                $('#ajaxCrudModal .modal-header').html(data.title);
                $('#ajaxCrudModal .modal-body').html(data.content);
                $('#ajaxCrudModal .modal-footer').html(data.footer);

            }
        });
    }

    function submitDatos(){
        
        var $form = $('#form-datos');
        var data = $form.serialize();

        $.ajax({
            url: '<?php echo Url::to(['resumen']) ?>',
            dataType: 'json',
            type:"POST",
            data: data,
            success: function(data) {
                
                $('#ajaxCrudModal .modal-header').html(data.title);
                $('#ajaxCrudModal .modal-body').html(data.content);
                $('#ajaxCrudModal .modal-footer').html(data.footer);
               
            }
        });
    }


</script>

<div class="resumen" style="width: 450px">

    <h2><?= Html::encode($this->title) ?></h2>
    <br>

    <?php if (Yii::$app->session->hasFlash('usuarionohabilitado')): ?>

        <div class="alert alert-success">
            Usuario no habilitado.
        </div>

    <?php else: ?>

        <div class="resumen-form">
            <style>
            .form-group { margin-bottom: 2px; } 
            .help-block {margin-top: 0px;margin-bottom: 0px;}
            </style>

            <?php $form = ActiveForm::begin(['id' => 'form-encabezado']); ?>

            <?php
            echo Html::textInput('encabezado', '',['style'=>'display:none']);            
            ?>

            <div style="float:left;">
            <?php 
            $data=['2021' => '2021','2022' => '2022','2023' => '2023','2024' => '2024','2025' => '2025',
                   '2026' => '2026','2027' => '2027','2028' => '2028','2029' => '2029','2030' => '2030'];
            echo $form->field($model, 'anio')->dropDownList($data,['prompt'=>'Seleccionar...','style'=>'width:215px']);
            ?>        
            </div>

            <div style="float:left;margin-left:20px;">
            <?php 
            $data=['1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12'];
             echo $form->field($model, 'mes')->dropDownList($data,['prompt'=>'Seleccionar...','style'=>'width:215px']);
            ?>
            </div>

            <div style="clear:both;"></div>

            <?php 
            if($cod_area!=999){
                $data=ArrayHelper::map(app\modules\sies\models\Mpuesto::find()
                ->where(['codarea'=>$cod_area])
                ->orderBy('descrip')                
                ->asArray()->all(), 'id', 'descrip');
                echo $form->field($model, 'id_puesto')->dropDownList($data,['prompt'=>'Seleccionar...']);
            }
            else{
                $data=ArrayHelper::map(app\modules\sies\models\Mpuesto::find()
                ->orderBy('descrip')                
                ->asArray()->all(), 'id', 'descrip');
                echo $form->field($model, 'id_puesto')->dropDownList($data,['prompt'=>'Seleccionar...']);
            }
            ?>

            <?php 
                $data=ArrayHelper::map(app\modules\sies\models\Mservicio::find()
                ->orderBy('descrip')
                ->asArray()->all(), 'id', 'descrip');
                echo $form->field($model, 'id_servicio')->dropDownList($data,['prompt'=>'Seleccionar...']);
            ?>

            <br>
            <div class="form-group">
                <?php echo Html::button('Aceptar', ['class' => 'btn btn-primary pull-left', 'name' => 'Aceptar',
                         'onclick' => 'submitEncabezado()']);
                ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    <?php endif; ?>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

