<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ResumenConsultaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resumen Consultas '.$anio;
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

// agrego acciones a $columns
$columns[] = [
    'class' => 'kartik\grid\ActionColumn',
    'dropdown' => false,
    'vAlign' => 'middle',
   
    'viewOptions' => ['role' => 'modal-remote', 'title' => 'Ver', 'data-toggle' => 'tooltip'],
    'updateOptions' => ['role' => 'modal-remote', 'title' => 'Editar', 'data-toggle' => 'tooltip'],
    'deleteOptions' => [
        'role' => 'modal-remote', 'title' => 'Borrar',
        'data-confirm' => false, 'data-method' => false,
        'data-request-method' => 'post',
        'data-toggle' => 'tooltip',
        'data-confirm-title' => 'Resumen Consulta ' . $anio,
        'data-confirm-message' => '¿ Desea borrar este registro ?'
    ],
    'visibleButtons' => [
        'view' => $permisos['view'],
        'update' => false,  // Asegúrate de que 'update' esté visible
        'delete' =>false
    ]
];


$stringToolbar="";

/* if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}*/
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['','anio'=>$anio],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r'],'anio'=>$anio],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r'],'anio'=>$anio],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

?>
<div class="resumen-consulta-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>