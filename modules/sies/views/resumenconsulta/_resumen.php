<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ResumenConsulta */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="resumen-consulta-form">
    <style>
    .form-group { margin-bottom: 2px; } 
    .help-block {margin-top: 0px;margin-bottom: 0px;}
    </style>

    <?php $form = ActiveForm::begin(['id' => 'form-datos']); ?>

    <div style="font-size:16px;font-weight:bold;margin-bottom:15px;margin-left: 50px;">
    RANGO 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; VARONES 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MUJERES
    </div>

    <?php 

      echo Html::textInput('datos', '',['style'=>'display:none']);            
      
      foreach ($datos as $key=>$value){

        echo $form->field($value, "[$key]anio")->textInput()->hiddenInput()->label(false);
        echo $form->field($value, "[$key]mes")->textInput()->hiddenInput()->label(false);
        echo $form->field($value, "[$key]id_puesto")->textInput()->hiddenInput()->label(false);
        echo $form->field($value, "[$key]id_servicio")->textInput()->hiddenInput()->label(false);
        echo $form->field($value, "[$key]id_rango")->textInput()->hiddenInput()->label(false);

        echo '<div style="float:left;width:100px;font-weight: bold;font-size: 18px;margin-top: 1px;margin-left: 50px;">';
        echo $value->rango->nombre;
        echo '</div>';

        echo '<div style="float:left;">';
        echo $form->field($value, "[$key]varones")->textInput(['style'=>'width:160px'])->label(false);
        echo '</div>';
        
        echo '<div style="float:left;margin-left:20px;">';
        echo $form->field($value, "[$key]mujeres")->textInput(['style'=>'width:160px'])->label(false);
        echo '</div>';
        echo '<div style="clear:both;"></div>';

      }
      
    ?>
    <?php ActiveForm::end(); ?>
    
</div>
