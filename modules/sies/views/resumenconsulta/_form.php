<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ResumenConsulta */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="resumen-consulta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'anio')->textInput();?>

    <?php echo $form->field($model, 'mes')->textInput();?>

    <?php 
		$data=ArrayHelper::map(app\modules\sies\models\Mpuesto::find()->asArray()->all(), 'id', 'descrip');
    	echo $form->field($model, 'id_puesto')->dropDownList($data,['prompt'=>'Seleccionar...']);
  	?>

    <?php 
		$data=ArrayHelper::map(app\modules\sies\models\Mservicio::find()->asArray()->all(), 'id', 'descrip');
    	echo $form->field($model, 'id_servicio')->dropDownList($data,['prompt'=>'Seleccionar...']);
	  ?>

    <?php 
		$data=ArrayHelper::map(app\modules\sies\models\Mrango::find()->asArray()->all(), 'id', 'nombre');
    	echo $form->field($model, 'id_rango')->dropDownList($data,['prompt'=>'Seleccionar...']);
	  ?>

    <?php echo $form->field($model, 'varones')->textInput();?>

    <?php echo $form->field($model, 'mujeres')->textInput();?>

    <?php ActiveForm::end(); ?>
    
</div>
