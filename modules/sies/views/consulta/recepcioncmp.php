<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use quidu\ajaxcrud\CrudAsset;
use yii\helpers\Url;

$this->title = 'Subir Archivo CMP';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<script>
function subirarchivo() {

var formData = new FormData($("#form-cmp")[0]);

$.ajax({
    url: '<?php echo Url::to(["subirarchivo"]); ?>',
    type: 'POST',
    data: formData,
    success: function (data) {
        $('#archivo_thumbnail').html(data.thumbnail);
    },
    cache: false,
    contentType: false,
    processData: false,
});

return false;
}
</script>

<div class="consulta-cmp" style="width:50%">
    <h2><?= Html::encode($this->title) ?></h2>
    <br>
    <?php 
        if (Yii::$app->session->hasFlash('errorcmp')){
            echo "<div class='alert alert-danger'>
                Error en la carga del archivo o en la descompresion de datos.
                </div>";
        }else if (Yii::$app->session->hasFlash('errorvacio')){ 
            echo "<div class='alert alert-danger'>
                Archivos txconsu y txmorbi vacios.
                </div>";
        }else{ ?>

        <div class="subircmp-form">

            <?php
                $form = ActiveForm::begin(['id'=>'form-cmp','options'=>['enctype'=>'multipart/form-data']]);
            ?>

            <label class='control-label' for='archivo_upload'>Archivo (10Mb max)</label>
            <div style='position: relative;'>
                <div style='position:relative;float:left;width: 115px'>
                    <div class="form-group field-consulta-archivo_upload">

                        <label class="btn btn-primary" style="width:110px;" for="consulta-archivo_upload">
                            <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Examinar
                        </label>

                        <input type="hidden" name="archivo_upload" value="">
                        <input type="file" id="consulta-archivo_upload" class="sr-only" name="archivo_upload" onchange="subirarchivo();">
                    </div>

                </div>
                <div id='archivo_thumbnail' class='form-control' style='width: 445px; position:relative;left:8px;display: flex;'></div>
            </div>
        
            <div style='clear:both;'></div><br>

            <div class="form-group">
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary pull-left', 'name' => 'Aceptar']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    <?php } ?>

</div>
