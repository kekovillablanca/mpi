<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

$toolbar=
Html::a('<span class="btn btn-primary" style="margin-right:10px;">TXCONSU</span>', Url::to(['','tx'=>'consu']),
['role'=>'modal-remote','title'=> 'Ver TXCONSU']).
Html::a('<span class="btn btn-primary">TXMORBI</span>', Url::to(['','tx'=>'morbi']),
['role'=>'modal-remote','title'=> 'Ver TXMORBI']);

?>


<div class="tx-index">
    <div id="ajaxCrudDatatableShareDetalle">
        <?=GridView::widget([
            'id'=>'crud-datatable-share',
            'dataProvider' => $dataProvider,
            'filterModel' => null,
            'pjax'=>true,
            'layout'=>"{items}",
            'columns' => $columns,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbar'=> [ ['content'=>$toolbar]],
            'panel' => [
                'type' => 'primary',
                'heading' => $title,
            ]
        ])?>
    </div>
</div>
