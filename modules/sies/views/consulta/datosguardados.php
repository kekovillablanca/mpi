<?php
use yii\helpers\Html;
use quidu\ajaxcrud\CrudAsset;

$this->title = 'Recepcion de Datos';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>

<div class="datosguardados">
    <h2><?= Html::encode($this->title) ?></h2>
    <br>
    <?php if (isset($error)){ ?>
        <div class='alert alert-danger' style="font-size: 18px;">
            <b>
            Error en la operacion idicada<br>
            <?= $hospital ?><br>
            PERIODO <?= $trimestre?> / <?= $anio?>
            </b>
        </div>
    <?php }else{ ?>
        <div class='alert alert-success' style="font-size: 18px;">
            <b>
            Datos Guardados Exitosamente<br>
            <?= $hospital ?><br>
            PERIODO <?= $trimestre?> / <?= $anio?>
            </b>
        </div>
    <?php } ?>
</div>
