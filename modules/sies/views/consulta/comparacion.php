<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use quidu\ajaxcrud\CrudAsset;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$this->title = 'Comparacion de Datos';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<div class="comparacion">
    <h2><?= Html::encode($this->title) ?></h2>
    <br>
        <div class="confirmarorigen-form">

            <?php
                $form = ActiveForm::begin(['id'=>'form-origen']);
            ?>
            
            <b>
            <?= $hospital ?><br>
            PERIODO <?= $trimestre?> / <?= $anio?>
            </b>
            <br><br>
            <div style="float:left;">
            <b>CONSULTA</b>
            <table class="table table-striped table-bordered detail-view" style="width:500px;"><tbody>
            <tr><th>MES</th><th style="width:180px;">RECIBIDO</th><th>LOCAL</th></tr>
            <tr>
                <td><?= (isset($consulta_tx[0]['mes']))?$consulta_tx[0]['mes']:'-'?></td>
                <td><?= (isset($consulta_tx[0]['cantidad']))?$consulta_tx[0]['cantidad']:'0'?></td>
                <td><?= (isset($consulta_base[0]['cantidad']))?$consulta_base[0]['cantidad']:'0' ?></td>
            </tr>
            <tr>
                <td><?= (isset($consulta_tx[1]['mes']))?$consulta_tx[1]['mes']:'-'?></td>
                <td><?= (isset($consulta_tx[1]['cantidad']))?$consulta_tx[1]['cantidad']:'0'?></td>
                <td><?= (isset($consulta_base[1]['cantidad']))?$consulta_base[1]['cantidad']:'0' ?></td>
            </tr>
            <tr>
                <td><?= (isset($consulta_tx[2]['mes']))?$consulta_tx[2]['mes']:'-'?></td>
                <td><?= (isset($consulta_tx[2]['cantidad']))?$consulta_tx[2]['cantidad']:'0'?></td>
                <td><?= (isset($consulta_base[2]['cantidad']))?$consulta_base[2]['cantidad']:'0' ?></td>
            </tr>
            </tbody></table>
            </div>

            <div style="float:left;margin-left: 30px;">
            <b>MORBIMOR</b>
            <table class="table table-striped table-bordered detail-view" style="width:500px;"><tbody>
            <tr><th>MES</th><td style="width:180px;"><b>RECIBIDO</b></td><td><b>LOCAL</b></td></tr>
            <tr>
                <td><?= (isset($morbimor_tx[0]['mes']))?$morbimor_tx[0]['mes']:'-' ?></td>
                <td><?= (isset($morbimor_tx[0]['cantidad']))?$morbimor_tx[0]['cantidad']:'0' ?></td>
                <td><?= (isset($morbimor_base[0]['cantidad']))?$morbimor_base[0]['cantidad']:'0' ?></td>
            </tr>
            <tr>
                <td><?= (isset($morbimor_tx[1]['mes']))?$morbimor_tx[1]['mes']:'-' ?></td>
                <td><?= (isset($morbimor_tx[1]['cantidad']))?$morbimor_tx[1]['cantidad']:'0' ?></td>
                <td><?= (isset($morbimor_base[1]['cantidad']))?$morbimor_base[1]['cantidad']:'0' ?></td>
            </tr>
            <tr>
                <td><?= (isset($morbimor_tx[2]['mes']))?$morbimor_tx[2]['mes']:'-' ?></td>
                <td><?= (isset($morbimor_tx[2]['cantidad']))?$morbimor_tx[2]['cantidad']:'0' ?></td>
                <td><?= (isset($morbimor_base[2]['cantidad']))?$morbimor_base[2]['cantidad']:'0' ?></td>
            </tr>
            </tbody></table>
            </div>        

            <div style='clear:both;'></div><br>
            <div class="form-group">
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary pull-left', 'name' => 'Aceptar']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

</div>
