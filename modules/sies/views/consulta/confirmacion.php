<?php
use yii\helpers\Html;

?>

<div class="confirmacion-csv">
    <h3>Confirmación de Generación de CSV</h3>
    <p>El archivo CSV está siendo generado.</p>
    
    <!-- Una vez listo, podrías mostrar el botón de descarga -->
    <?= Html::a('Descargar CSV', ['descargarcsv', 'anio' => $anio], ['class' => 'btn btn-success']) ?>
    
    <!-- Botón para cancelar si el usuario no desea esperar -->
    <?= Html::button('Cancelar', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) ?>
</div>
