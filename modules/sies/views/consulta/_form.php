<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Consulta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consulta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'numero')->textInput();?>

    <?php echo $form->field($model, 'hospital')->textInput(['maxlength' => true,'disabled' => true]);?>

    <?php echo $form->field($model, 'puesto')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'servicio')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'medico')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'fecha')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'nrohc')->textInput();?>

    <?php echo $form->field($model, 'nrodoc')->textInput();?>

    <?php echo $form->field($model, 'apellido')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'edad')->textInput();?>

    <?php echo $form->field($model, 'cdgosexo')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'obra')->textInput();?>

    <?php echo $form->field($model, 'primera')->textInput();?>

    <?php echo $form->field($model, 'cemb')->textInput();?>

    <?php echo $form->field($model, 'cns')->textInput();?>

    <?php echo $form->field($model, 'diag')->textInput(['maxlength' => true]);?>

    <?php ActiveForm::end(); ?>
    
</div>
