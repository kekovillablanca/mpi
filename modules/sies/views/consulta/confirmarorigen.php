<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$this->title = 'Confirmar Origen';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="confirmar-origen" style="width:50%">
    <h2><?= Html::encode($this->title) ?></h2>
    <br>
    <?php 
        if (Yii::$app->session->hasFlash('errororigen')){
            echo "<div class='alert alert-danger'>
                Datos incompletos en la confirmacion de origen o período.
                </div>";
        }else{ ?>

        <div class="confirmarorigen-form">

            <?php
                $form = ActiveForm::begin(['id'=>'form-origen']);


                echo "<div class='form-group' style='width: 140px;float: left;'><label>Trimestre</label><br>";
                $data=['1'=>'1','2'=>'2','3'=>'3','4'=>'4'];
                echo Html::dropDownList('trimestre', $trimestre, $data,['prompt'=>'Seleccionar...','style'=>'width:120px']);
                echo "</div>";

                echo "<div class='form-group'><label>Año</label><br>";
                $data=['2020'=>'2020','2021'=>'2021','2022'=>'2022','2023'=>'2023','2024'=>'2024','2025'=>'2025','2026'=>'2026','2027'=>'2027','2028'=>'2028','2029'=>'2030','2030'];
                echo Html::dropDownList('anio', $anio, $data,['prompt'=>'Seleccionar...','style'=>'width:120px']);
                echo "</div>";

                echo "<div class='form-group' style='width: 280px;float: left;'><label>Hospital</label>";
                $data=ArrayHelper::map(app\modules\sies\models\MArea::find()->asArray()->all(), 'descrip', 'descrip');
                echo Html::dropDownList('hospital', $hospital, $data,['prompt'=>'Seleccionar...','style'=>'width:260px']);
                echo "</div>";

                echo Html::button('Verificar', ['class' => 'btn btn-dark btn-sm', 'role'=>'modal-remote','name' => 'Verificar','style'=>'margin-top: 24px;height: 29px;border:1px solid #b6b6b6' ]);
            ?>
        
            <div style='clear:both;'></div><br>

            <div class="form-group">
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary pull-left', 'name' => 'Aceptar']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    <?php } ?>

</div>

<style> .modal-dialog { width: 900px !important; } </style>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
