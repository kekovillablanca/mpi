<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MArea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marea-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'codigo')->textInput();?>

    <?php echo $form->field($model, 'descrip')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'codfos')->textInput();?>

    <?php echo $form->field($model, 'codsisa')->textInput();?>

    <?php ActiveForm::end(); ?>
    
</div>
