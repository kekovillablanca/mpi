<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OdontologiaPracticaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Odontologia Practicas';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

// agrego acciones a $columns
$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Odontologia Practica',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]
    ];

$stringToolbar="";
if($modelMaestro!=null){
    if($totalMaestro==false){
if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create','id_maestro'=>$modelMaestro->id],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['','id_maestro'=>$modelMaestro->id],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'limpiar orden/filtro']);
    }
}
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

?>

<style>
table {  border:1px solid  #337ab7;    border-collapse: separate; border-radius:5px; border-spacing:0}
td, th  { border: none; }
td + td, th + th {border-left:1px solid #ddd}
th, tr td  {border-bottom:1px solid #ddd}
th:first-child { border-radius:5px 0 0 0; border-left:none}
th:last-child { border-radius:0 5px 0 0;border-right:none }
tfoot td:first-child {  border-radius:0 0 0 5px ;border-left:none}
tfoot td:last-child {  border-radius:0 0 5px 0;border-right:none}
</style>
<?php   if($modelMaestro!=null){
  if($totalMaestro==true){

echo($html);

}else{ ?>
<table class="table table-condensed">
<thead style="background: #337ab7;color: white;">
<tr>
<th>Area</th>
<th>Año</th>
<th>Mes</th>
<th>Tipo</th>
<th>Pacientes</th>
</tr>
</thead>
<tbody style="font-weight: bold;">
<tr>
<td><?= $modelMaestro->area->descrip ?></td>
<td><?= $modelMaestro->anio ?></td>
<td><?= $modelMaestro->mes ?></td>
<td><?= $modelMaestro->sttipo->nombre ?></td>
<td><?= $modelMaestro->cantidad_paciente ?></td>
</tr>
</tbody>
</table>
<?php }}?>
<div class="odontologia-practica-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
