<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\OdontologiaPractica */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="odontologiapractica-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        echo $form->field($model, 'id_odontologia')->hiddenInput()->label(false)->error(false);
    ?>

    <?php    

        echo $form->field($model, 'id_practica')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => ($model->id_practica)?$model->practica->nombre:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['/sies/odontologiapractica/nomenclador']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_practica) { return id_practica.text; }'),
                'templateSelection' => new JsExpression('function(id_practica) { return id_practica.text; }'),
            ],
        ]);

    ?>

    <div style="width:250px;float:left;">

    <?php echo $form->field($model, 'cantidad_practica')->textInput();?>

    </div>

    <div style="clear:both">

    <?php ActiveForm::end(); ?>
    
</div>
