<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use quidu\ajaxcrud\CrudAsset;
use yii\helpers\ArrayHelper;

$this->title = 'Imagenes';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="imagenes-encabezado" style="width:450px">

    <h2><?= Html::encode($this->title) ?></h2>

    <?php if (Yii::$app->session->hasFlash('usuarionohabilitado')): ?>

        <div class="alert alert-success">
            Usuario no habilitado.
        </div>

    <?php else: ?>


        <div class="imagenes-form">

            <?php $form = ActiveForm::begin();
            $pointer=(!$flag)?'pointer-events: none;':''
            ?>

            <?php
            echo Html::textInput('flag', $flag,['style'=>'display:none']);            
            ?>

            <?php 
            echo $form->field($model, 'id_area')->dropDownList($listadoAreas,['prompt'=>'Seleccionar...','style'=>'pointer-events:'.$visible.';']);
	        ?>

            <div style="float:left;">
            <?php 
            $data=['2021' => '2021','2022' => '2022','2023' => '2023','2024' => '2024','2025' => '2025',
                   '2026' => '2026','2027' => '2027','2028' => '2028','2029' => '2029','2030' => '2030'];
            echo $form->field($model, 'anio')->dropDownList($data,['prompt'=>'Seleccionar...','style'=>'width:215px;'.$pointer]);
            ?>        
            </div>

            <div style="float:left;margin-left:20px;">
            <?php 
            $data=['1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12'];
             echo $form->field($model, 'mes')->dropDownList($data,['prompt'=>'Seleccionar...','style'=>'width:215px;'.$pointer]);
            ?>
            </div>

            <div style="clear:both;"></div>

            <?php 
            $data=ArrayHelper::map(app\modules\sies\models\STTipo::find()->where('clasificacion=2')->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'id_st_tipo')->dropDownList($data,['prompt'=>'Seleccionar...','style'=>''.$pointer]);
            ?>

            <?php if (!$flag): ?>

                <div style="float:left;">
                <?php echo $form->field($model, 'int')->textInput(['style'=>'width:146px']);?>
                </div>

                <div style="float:left;;margin-left:6px;">
                <?php echo $form->field($model, 'con')->textInput(['style'=>'width:146px']);?>
                </div>

                <div style="float:left;;margin-left:6px;">
                <?php echo $form->field($model, 'gua')->textInput(['style'=>'width:146px']);?>
                </div>

            <?php endif; ?>
            
            <div style="clear:both;"></div>

            <div class="form-group">
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary pull-left', 'name' => 'Aceptar']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    <?php endif; ?>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

