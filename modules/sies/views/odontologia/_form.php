<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Odontologia */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="odontologia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
		$data=ArrayHelper::map(app\modules\sies\models\MArea::find()->asArray()->all(), 'id', 'descrip');
    echo $form->field($model, 'id_area')->dropDownList($data,['prompt'=>'Seleccionar...']);
	  ?>

    <?php echo $form->field($model, 'mes')->textInput();?>

    <?php echo $form->field($model, 'anio')->textInput();?>

    <?php 
		$data=ArrayHelper::map(app\modules\sies\models\STTipo::find()->where('clasificacion=4')->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'id_st_tipo')->dropDownList($data,['prompt'=>'Seleccionar...']);
	 ?>

    <?php echo $form->field($model, 'cantidad_paciente')->textInput();?>

    <?php ActiveForm::end(); ?>
    
</div>
