<?php
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Morbimor */
/* @var $form yii\widgets\ActiveForm */
?>
<script>$.fn.modal.Constructor.prototype.enforceFocus = function() {};</script>
<div class="morbimor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'numero')->textInput();?>

    <?php echo $form->field($model, 'historiacl')->textInput();?>

    <?php echo $form->field($model, 'establec')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'apellidos')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'ndedocumen')->textInput();?>

    <?php echo $form->field($model, 'tipo')->textInput();?>

    <?php echo $form->field($model, 'fechadenac')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'edading')->textInput();?>

    <?php echo $form->field($model, 'sexo')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'locresid')->textInput();?>

    <?php echo $form->field($model, 'depresid')->textInput();?>

    <?php echo $form->field($model, 'provresid')->textInput();?>

    <?php echo $form->field($model, 'paisresid')->textInput();?>

    <?php echo $form->field($model, 'tieneobras')->textInput();?>

    <?php echo $form->field($model, 'codobrasoc')->textInput();?>

    <?php echo $form->field($model, 'codhospit')->textInput();?>

    <?php echo $form->field($model, 'fechaingr')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'servicio')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'diasest')->textInput();?>

    <?php echo $form->field($model, 'fechapase')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'servicio01')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'diasest01')->textInput();?>

    <?php echo $form->field($model, 'fechapas01')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'servicio02')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'diasest02')->textInput();?>

    <?php echo $form->field($model, 'fechapas02')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'servicio03')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'diasest03')->textInput();?>

    <?php echo $form->field($model, 'fechapas03')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'servicio04')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'diasest04')->textInput();?>

    <?php echo $form->field($model, 'fechaegres')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'totdiasest')->textInput();?>

    <?php echo $form->field($model, 'egresopor')->textInput();?>

    <?php echo $form->field($model, 'operado')->textInput();?>

    <?php echo $form->field($model, 'diagprinc')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'otrodiag')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'otrodiag1')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'otrodiag2')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'otrodiag3')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'producidop')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'operimport')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'lugardonde')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'otraoper')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'otraoper1')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'codigodele')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'otrascir')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'diascir')->textInput();?>

    <?php echo $form->field($model, 'termina')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'tparto')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'totctrls')->textInput();?>

    <?php echo $form->field($model, 'condnacer')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'sexonaci')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'peso')->textInput();?>

    <?php echo $form->field($model, 'condnace01')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'sexonaci01')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'peso01')->textInput();?>

    <?php echo $form->field($model, 'condnace02')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'sexonaci02')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'peso02')->textInput();?>

    <?php echo $form->field($model, 'estudios')->textInput();?>

    <?php echo $form->field($model, 'trabajos')->textInput();?>

    <?php echo $form->field($model, 'trabajtxt')->textInput(['maxlength' => true]);?>

    <?php echo $form->field($model, 'htalorig')->textInput();?>

    <?php echo $form->field($model, 'htaldest')->textInput();?>

    <?php echo $form->field($model, 'nrodocmad')->textInput();?>

    <?php echo $form->field($model, 'fechaparto')->widget(DatePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true]
                ]);?>

    <?php echo $form->field($model, 'edadgest')->textInput();?>

    <?php echo $form->field($model, 'paridad')->textInput();?>

    <?php echo $form->field($model, 'parto')->textInput();?>

    <?php echo $form->field($model, 'horaingr')->textInput();?>

    <?php echo $form->field($model, 'horapase')->textInput();?>

    <?php echo $form->field($model, 'horapase01')->textInput();?>

    <?php echo $form->field($model, 'horapase02')->textInput();?>

    <?php echo $form->field($model, 'horapase03')->textInput();?>

    <?php echo $form->field($model, 'horaegres')->textInput();?>

    <?php echo $form->field($model, 'servaux')->textInput();?>

    <?php echo $form->field($model, 'servaux01')->textInput();?>

    <?php echo $form->field($model, 'servaux02')->textInput();?>

    <?php echo $form->field($model, 'servaux03')->textInput();?>

    <?php echo $form->field($model, 'servaux04')->textInput();?>

    <?php ActiveForm::end(); ?>
    
</div>
