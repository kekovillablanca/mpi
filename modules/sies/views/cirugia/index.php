<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use quidu\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CirugiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cirugias';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

// agrego acciones a $columns
$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Cirugia',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]
    ];

$stringToolbar="";

if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r']],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

?>
<div class="cirugia-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>