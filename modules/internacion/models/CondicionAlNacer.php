<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.condicion_al_nacer".
 *
 * @property int $id
 * @property string $condicion
 */
class CondicionAlNacer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.condicion_al_nacer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['condicion'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'condicion' => 'Condicion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.condicion_al_nacer.id', 10),
			'condicion' => array('internacion.condicion_al_nacer.condicion', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'condicion',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'condicion',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
