<?php

namespace app\modules\internacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * InternacionSearch represents the model behind the search form about `app\modules\internacion\models\Internacion`.
 */
class InternacionSearch extends Internacion
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['embarazoParidad','embarazoEdadGestacional','embarazoTipoParto','pacienteDni','pacienteApellido','id', 'id_sector', 'id_causa_externa', 'id_tipo_egreso', 'anio_activo', 'id_diagnostico', 'id_lugar_ingreso', 'id_paciente', 'numero_informe', 'estado','id_establecimiento','pacienteEducacion'], 'safe'],
            [['total_dias_estadia'], 'integer'], // Asegúrate de validar como entero si corresponde
            [['fecha_ingreso', 'fecha_egreso','id_embarazo'], 'date', 'format' => 'php:Y-m-d'], // Ajusta el formato según sea necesario
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Internacion::find()
            ->joinWith([
                'causaexterna', 
                'establecimiento', 
                'paciente', 
                'paciente.sistemaeducativo.idasistio', 
                'embarazo', 
                'embarazo.tipoparto', 
                'tipoegreso', 
                'diagnostico', 
                'lugaringreso', 
                'sector',
            ]);
    
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30, // Establece el tamaño de página aquí
            ],
        ]);
    
        $this->load($params);
    
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'numero_informe',
                'anio_activo',
                'estado',
                'id_embarazo' => [
                    'asc' => ['embarazo.fecha_fin_embarazo' => SORT_ASC],
                    'desc' => ['embarazo.fecha_fin_embarazo' => SORT_DESC],
                ],
                'embarazoTipoParto' => [
                    'asc' => ['tipo_parto.nombre' => SORT_ASC],
                    'desc' => ['tipo_parto.nombre' => SORT_DESC],
                ],
                'embarazoEdadGestacional' => [
                    'asc' => ['embarazo.edad_gestacional' => SORT_ASC],
                    'desc' => ['embarazo.edad_gestacional' => SORT_DESC],
                ],
                'embarazoParidad' => [
                    'asc' => ['embarazo.paridad' => SORT_ASC],
                    'desc' => ['embarazo.paridad' => SORT_DESC],
                ],
                'id_paciente' => [
                    'asc' => ['paciente.nombre' => SORT_ASC],
                    'desc' => ['paciente.nombre' => SORT_DESC],
                ],
                'id_sector' => [
                    'asc' => ['sector.descripcion' => SORT_ASC],
                    'desc' => ['sector.descripcion' => SORT_DESC],
                ],
                'id_causa_externa' => [
                    'asc' => ['causa_externa.nombre' => SORT_ASC],
                    'desc' => ['causa_externa.nombre' => SORT_DESC],
                ],
                'id_establecimiento' => [
                    'asc' => ['establecimiento.nombre' => SORT_ASC],
                    'desc' => ['establecimiento.nombre' => SORT_DESC],
                ],
                'id_diagnostico' => [
                    'asc' => ['diagnostico.nombre' => SORT_ASC],
                    'desc' => ['diagnostico.nombre' => SORT_DESC],
                ],
                'id_lugar_ingreso' => [
                    'asc' => ['lugar_ingreso.nombre' => SORT_ASC],
                    'desc' => ['lugar_ingreso.nombre' => SORT_DESC],
                ],
                'fecha_ingreso',
                'fecha_egreso',
                'total_dias_estadia',
                'pacienteApellido'=>[

                    'asc' => ['paciente.apellido' => SORT_ASC],
                    'desc' => ['paciente.apellido' => SORT_DESC],
                ],
                'pacienteDni'=>[

                    'asc' => ['paciente.numero_documento' => SORT_ASC],
                    'desc' => ['paciente.numero_documento' => SORT_DESC],
                ],
                'id_tipo_egreso' => [
                    'asc' => ['internacion.tipo_egreso.nombre' => SORT_ASC],
                    'desc' => ['internacion.tipo_egreso.nombre' => SORT_DESC],
                ],
                'pacienteEducacion'=>[

                    'asc' => ['asistio.nombre' => SORT_ASC],
                    'desc' => ['asistio.nombre' => SORT_DESC],
                ],
                // Agrega aquí los atributos de diagnosticoobservacion que quieras ordenar

            ]
        ]);
    
        if (!$this->validate()) {
            // Si la validación falla, no retornar registros
            $query->where('0=1');
            return $dataProvider;
        }
    
        $query->andFilterWhere([
            'id' => $this->id,
            'total_dias_estadia' => $this->total_dias_estadia,
            'numero_informe' => $this->numero_informe,
            'anio_activo' => $this->anio_activo,
        ]);
    
        // Filtrar por campos de la relación diagnosticoobservacion
// Ajusta este campo según sea necesario
    
        $query->andFilterWhere(['like', 'lower(internacion.embarazo.fecha_fin_embarazo)', strtolower($this->id_embarazo)])
        ->andFilterWhere(['like', 'lower(internacion.embarazo.paridad)', strtolower($this->embarazoParidad)])
        ->andFilterWhere(['like', 'internacion.embarazo.edad_gestacional', $this->embarazoEdadGestacional])
        ->andFilterWhere(['like', 'lower(internacion.id_tipo_parto)', strtolower($this->embarazoTipoParto)])
            ->andFilterWhere(['like', 'lower(causa_externa.nombre)', strtolower($this->id_causa_externa)])
            ->andFilterWhere(['like', 'lower(internacion.estado)', strtolower($this->estado)])
            ->andFilterWhere(['like', 'lower(diagnostico.nombre)', strtolower($this->id_diagnostico)])
            ->andFilterWhere(['like', 'lower(paciente.nombre)', strtolower($this->id_paciente)])
            ->andFilterWhere(['like', 'lower(asistio.nombre)', strtolower($this->pacienteEducacion)])
            ->andFilterWhere(['like', 'to_char(internacion.fecha_ingreso, \'DD/MM/YYYY\')', $this->fecha_ingreso])
            ->andFilterWhere(['like', 'to_char(internacion.fecha_egreso, \'DD/MM/YYYY\')', $this->fecha_egreso])
            ->andFilterWhere(['like', 'lower(internacion.tipo_egreso.nombre)', strtolower($this->id_tipo_egreso)]) // Cambié a tipoEgreso
            ->andFilterWhere(['like', 'lower(lugar_ingreso.nombre)', strtolower($this->id_lugar_ingreso)])
            ->andFilterWhere(['like', 'lower(sector.descripcion)', strtolower($this->id_sector)])
            ->andFilterWhere(['like','CAST(consultasestadisticas.paciente.numero_documento AS varchar)',$this->pacienteDni])
            ->andFilterWhere(['like', 'lower(consultasestadisticas.paciente.apellido)', strtolower($this->pacienteApellido)])
            ->andFilterWhere(['like', 'lower(establecimiento.nombre)', strtolower($this->id_establecimiento)]);
    
        // Guardar DataProvider en sesión para recuperarlo en otra acción
        $session = Yii::$app->session;
        $session->set('internacion-dataprovider', $dataProvider);
    
        return $dataProvider;
    }
    
}
