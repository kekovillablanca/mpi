<?php 

namespace app\modules\internacion\models;
class Educacion {
    const PRIMARIO_COMPLETO = 'PRIMARIO COMPLETO';
    const PRIMARIO_INCOMPLETO = 'PRIMARIO INCOMPLETO';
    const SECUNDARIO_COMPLETO = 'SECUNDARIO COMPLETO';
    const SECUNDARIO_INCOMPLETO = 'SECUNDARIO INCOMPLETO';
    const SUPERIOR_COMPLETO = 'SUPERIOR COMPLETO';
    const SUPERIOR_INCOMPLETO = 'SUPERIOR INCOMPLETO';
    const UNIVERSITARIO_COMPLETO = 'UNIVERSITARIO COMPLETO';
    const UNIVERSITARIO_INCOMPLETO = 'UNIVERSITARIO INCOMPLETO';

    // Puedes agregar métodos estáticos si necesitas lógica adicional
    public static function getValues() {
        return [
            self::PRIMARIO_COMPLETO,
            self::PRIMARIO_INCOMPLETO,
            self::SECUNDARIO_COMPLETO,
            self::SECUNDARIO_INCOMPLETO,
            self::SUPERIOR_COMPLETO,
            self::SUPERIOR_INCOMPLETO,
            self::UNIVERSITARIO_COMPLETO,
            self::UNIVERSITARIO_INCOMPLETO,
        ];
    }
}
?>