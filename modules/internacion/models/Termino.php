<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.termino".
 *
 * @property string $completo
 * @property int $id
 */
class Termino extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.termino';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['completo'], 'required'],
            [['completo'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'completo' => 'Completo',
            'id' => 'ID',
        ];
    }
    public function attributePrint()
    {
        return [
			'completo' => array('internacion.termino.completo', 20),
			'id' => array('internacion.termino.id', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'completo',
			'id',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'completo',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
