<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.no_reformado".
 *
 * @property string $descripcion
 * @property int $id
 */
class NoReformado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.no_reformado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'termino'], 'required'],
            [['descripcion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'descripcion' => 'Descripcion',
            'id' => 'ID',
        ];
    }
    public function attributePrint()
    {
        return [
			'descripcion' => array('internacion.no_reformado.descripcion', 12),
			'id' => array('internacion.no_reformado.id', 10),
			// postgres
			// mysql
			//'termino' => array('IF(internacion.no_reformado.termino = 1, \'SI\', \'NO\')', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'descripcion',
			'id',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion',
			],
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],

        ];
    }
    public function getSistemaEducativoNoReformado()
    {
        return $this->hasMany(SistemaEducativoNoReformado::class, ['id_no_reformado' => 'id']);
    }

    public function getSistemasEducativos()
    {
        return $this->hasMany(SistemaEducativo::class, ['id' => 'id_sistema_educativo'])
            ->via('sistemaEducativoNoReformado');
    }
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
