<?php
/* Modelo generado por Model(Q) */

namespace app\modules\internacion\models;
use app\modules\internacion\models\Asistio;
use app\modules\internacion\models\Educacion;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "internacion.sistema_educativo".
 *
 * @property int $id
 * @property bool $primario_completo
 * @property bool $secundario_completo
 * @property bool $superior_completo
 * @property bool $universitario_completo
 * @property int $id_asistio
 */ 
class SistemaEducativo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.sistema_educativo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
{
    return [
        [['id_asistio'], 'required'],
        [['primario_completo', 'secundario_completo', 'superior_completo', 'universitario_completo'], 'string', 'max' => 25],
        [['id_asistio'], 'integer'],
        [['id_asistio'], 'exist', 'skipOnError' => true, 'targetClass' => Asistio::className(), 'targetAttribute' => ['id_asistio' => 'id']],

        // Regla personalizada para validar si se seleccionó algún sistema educativo
        [['primario_completo'], 
            'required', 
            'message' => 'Debe Seleccionar los diferentes estudios que tuvo el paciente.', 
            'when' => function ($model) {
                return $model->id_asistio=='2' && $this->primario_completo!=null && $this->secundario_completo!=null && $this->superior_completo!=null && $this->universitario_completo!=null;
            },
        ],
        [['secundario_completo'], 
        'required', 
        'message' => 'Debe Seleccionar los diferentes estudios que tuvo el paciente.', 
        'when' => function ($model) {
            return $model->id_asistio=='2' && $this->primario_completo!=null && $this->secundario_completo!=null && $this->superior_completo!=null && $this->universitario_completo!=null;
        },
    ],
        [['superior_completo'], 
        'required', 
        'message' => 'Debe Seleccionar los diferentes estudios que tuvo el paciente.', 
        'when' => function ($model) {
            $model->id_asistio=='2' && $this->primario_completo!=null && $this->secundario_completo!=null && $this->superior_completo!=null && $this->universitario_completo!=null;
        },
    ],
    [['universitario_completo'], 
    'required', 
    'message' => 'Debe Seleccionar los diferentes estudios que tuvo el paciente.', 
    'when' => function ($model) {
        $model->id_asistio=='2' && $this->primario_completo!=null && $this->secundario_completo!=null && $this->superior_completo!=null && $this->universitario_completo!=null;
    },
    ]
    ];
}


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_asistio' => 'Asistio',
            'primario_completo'=>"Primario:",
            'secundario_completo'=>"Secundario:",
            'superior_completo'=>"Superior:",
            'universitario_completo'=>"Universitario:"
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.sistema_educativo.id', 10),
			// postgres
            'id_asistio'=>array('internacion.asistio.nombre', 15),
			'primario_completo' => array('internacion.primario_completo', 30),
            'secundario_completo' =>array('internacion.secundario_completo', 30),
            'superior_completo' =>array('internacion.superior_completo', 30),
            'universitario_completo' => array('internacion.universitario_completo', 30),
			// mysql
			//'asistio' => array('IF(internacion.sistema_educativo.asistio = 1, \'SI\', \'NO\')', 10),
        ];
    }

    
    public function attributeView()
    {
        return [
			'id',
			'primario_completo:string',
            'secundario_completo:string',
            'superior_completo:string',
            'universitario_completo:string',
            'id_asistio'		=>	[
				'attribute'=>'idasistio.nombre',
				'label'=>'asistio',
			],

        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
				[
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'id_asistio',
                    'value'=>'idasistio.nombre',
                ],

            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'primario_completo',
				'format'=>'boolean',
			],

            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'secundario_completo',
				'format'=>'boolean',
			],

            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'superior_completo',
				'format'=>'boolean',
			],

            [
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'universitario_completo',
				'format'=>'boolean',
			],

        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
    public function validarSistemaEducativo()
{
    if ($this->id_asistio == 2) {
        if ($this->primario_completo == 'PRIMARIO COMPLETO') {
            if ($this->secundario_completo == 'SECUNDARIO INCOMPLETO') {
                if ($this->superior_completo == 'SUPERIOR COMPLETO' && $this->universitario_completo == 'UNIVERSITARIO COMPLETO') {
                    $this->addError("Error: no se puede cargar de esta forma el sistema educativo.");
                }
                if ($this->superior_completo == 'SUPERIOR INCOMPLETO' && $this->universitario_completo == 'UNIVERSITARIO INCOMPLETO') {
                    return; // Valido
                }
                if ($this->superior_completo == 'SUPERIOR INCOMPLETO' && $this->universitario_completo == 'UNIVERSITARIO COMPLETO') {
                    $this->addError( "Error: no se puede cargar de esta forma el sistema educativo.");
                }
                if ($this->superior_completo == 'SUPERIOR COMPLETO' && $this->universitario_completo == 'UNIVERSITARIO INCOMPLETO') {
                    return; // Valido
                }
            } elseif ($this->secundario_completo == 'SECUNDARIO COMPLETO') {
                if ($this->superior_completo == 'SUPERIOR COMPLETO' && $this->universitario_completo == 'UNIVERSITARIO COMPLETO') {
                    return; // Valido
                }
                if ($this->superior_completo == 'SUPERIOR INCOMPLETO' && $this->universitario_completo == 'UNIVERSITARIO INCOMPLETO') {
                    return; // Valido
                }
                if ($this->superior_completo == 'SUPERIOR INCOMPLETO' && $this->universitario_completo == 'UNIVERSITARIO COMPLETO') {
                    return; // Valido
                }
                if ($this->superior_completo == 'SUPERIOR COMPLETO' && $this->universitario_completo == 'UNIVERSITARIO INCOMPLETO') {
                    return; // Valido
                }
            }
        } elseif ($this->primario_completo == 'PRIMARIO INCOMPLETO' && 
                  $this->secundario_completo == 'SECUNDARIO INCOMPLETO' && 
                  $this->superior_completo == 'SUPERIOR INCOMPLETO' && 
                  $this->universitario_completo == 'UNIVERSITARIO INCOMPLETO') {
            return; // Valido
        } else {
            $this->addError( "Error: no se puede cargar de esta forma el sistema educativo.");
        }
    }
}
public function beforeValidate()
{
    // Llama al método de validación personalizada
    $this->validarSistemaEducativo();

    // Asegúrate de devolver el resultado del método padre
    return parent::beforeValidate();
}

    public function elegirId(){
        if($this->id_asistio==2){
        if($this->primario_completo=='PRIMARIO COMPLETO'){
         if($this->secundario_completo=='SECUNDARIO INCOMPLETO'){
            if($this->superior_completo=='SUPERIOR COMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO COMPLETO' ){
                throw new NotFoundHttpException("error no se puede cargar de esta forma el sistema educativo!!");

            }
            if($this->superior_completo=='SUPERIOR INCOMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO INCOMPLETO' ){
                
                return 3;

            }
            if($this->superior_completo=='SUPERIOR INCOMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO COMPLETO' ){
                throw new NotFoundHttpException("error no se puede cargar de esta forma el sistema educativo!!");
            }
            if($this->superior_completo=='SUPERIOR COMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO INCOMPLETO' ){
               
                return 10;
            }
        }
        else if($this->secundario_completo=='SECUNDARIO COMPLETO'){
            if($this->superior_completo=='SUPERIOR COMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO COMPLETO' ){
                return 7;
            }
            if($this->superior_completo=='SUPERIOR INCOMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO INCOMPLETO' ){
                return 4;
            }
            if($this->superior_completo=='SUPERIOR INCOMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO COMPLETO' ){
                return 6;

            }
            if($this->superior_completo=='SUPERIOR COMPLETO'
            && $this->universitario_completo=='UNIVERSITARIO INCOMPLETO' ){
                return 5;
            }
        }
    }
    else if($this->primario_completo=='PRIMARIO INCOMPLETO' && $this->secundario_completo=='SECUNDARIO INCOMPLETO' && $this->universitario_completo=='UNIVERSITARIO INCOMPLETO' && $this->superior_completo=='SUPERIOR INCOMPLETO')
            return 12;
    else 
    {
   
        throw new NotFoundHttpException("error no se puede cargar de esta forma el sistema educativo!!");
    }
         
    }
    else if($this->id_asistio==1){
        return 1;
    }
    else if($this->id_asistio==3){
        return 2;
    }
    else if($this->id_asistio==4){
        return 11;
    }
    throw new NotFoundHttpException("error no se puede cargar de esta forma el sistema educativo!!");
}


    

    public function getIdasistio()
    {
        return $this->hasOne(Asistio::className(), ['id'=>'id_asistio']);
    }

}
