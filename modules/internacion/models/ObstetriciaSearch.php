<?php

namespace app\modules\internacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ObstetriciaSearch represents the model behind the search form about `app\modules\internacion\models\Obstetricia`.
 */
class ObstetriciaSearch extends Obstetricia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'peso_al_nacer', 'id_condicion_nacimiento', 'id_terminacion', 'id_sexo', 'id_internacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$modelId=null)
    {
        $query = Obstetricia::find();
		$query->joinWith(['sexo']);
		$query->joinWith(['condicionnacimiento']);
		$query->joinWith(['internacion']);
		$query->joinWith(['terminacion']);
        $query->andWhere(['id_internacion'=>$modelId]);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'peso_al_nacer',
				'id_condicion_nacimiento'=> [
					'asc' => ['condicionnacimiento.condicion' => SORT_ASC],
					'desc' => ['condicionnacimiento.condicion' => SORT_DESC],
				],
				'id_terminacion'=> [
					'asc' => ['terminacion.terminacion' => SORT_ASC],
					'desc' => ['terminacion.terminacion' => SORT_DESC],
				],
				'id_sexo'=> [
					'asc' => ['sexo.nombre' => SORT_ASC],
					'desc' => ['sexo.nombre' => SORT_DESC],
				],
				'id_internacion'=> [
					'asc' => ['internacion.numero_informe' => SORT_ASC],
					'desc' => ['internacion.numero_informe' => SORT_DESC],
				],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'obstetricia.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(obstetricia.peso_al_nacer)',strtolower($this->peso_al_nacer)])
              ->andFilterWhere(['like', 'lower(condicionnacimiento.condicion)',strtolower($this->id_condicion_nacimiento)])
              ->andFilterWhere(['like', 'lower(terminacion.terminacion)',strtolower($this->id_terminacion)])
              ->andFilterWhere(['like', 'lower(sexo.nombre)',strtolower($this->id_sexo)])
              ->andFilterWhere(['like', 'lower(internacion.numero_informe)',strtolower($this->id_internacion)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('obstetricia-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
