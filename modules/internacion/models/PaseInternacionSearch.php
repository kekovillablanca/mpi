<?php

namespace app\modules\internacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaseInternacionSearch represents the model behind the search form about `app\modules\internacion\models\PaseInternacion`.
 */
class PaseInternacionSearch extends PaseInternacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_sector', 'dias_estadia', 'numero_pase', 'fecha_ingreso','id_internacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$modelId=null)
    {
      
        $query = PaseInternacion::find();
		$query->joinWith(['internacion']);
		$query->joinWith(['sector']);
       $query->where(['id_internacion'=>$modelId]);
        

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
  
       // $stringParams=$params["r"]."&modelId=".strval($modelId);
  
        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_sector'=> [
					'asc' => ['sector.descripcion' => SORT_ASC],
					'desc' => ['sector.descripcion' => SORT_DESC],
				],
				'dias_estadia',

                'id_internacion'=> [
					'asc' => ['internacion.numero_informe' => SORT_ASC],
					'desc' => ['internacion.numero_informe' => SORT_DESC],
				],
				'numero_pase',
				// mysql
				// 'fecha_ingreso'=> [
					// 'asc' => ['date_format(paseinternacion.fecha_ingreso, "%Y%m%d")' => SORT_ASC],
					// 'desc' => ['date_format(paseinternacion.fecha_ingreso,"%Y%m%d")' => SORT_DESC],
				// ],
				// postgres
				'fecha_ingreso',

            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'pase_internacion.id' => $this->id,
            'pase_internacion.dias_estadia' => $this->dias_estadia,
            'pase_internacion.numero_pase' => $this->numero_pase,
        ]);

        $query->andFilterWhere(['like', 'lower(sector.descripcion)',strtolower($this->id_sector)])
              //mysql
              // ->andFilterWhere(['like', 'date_format(pase_internacion.fecha_ingreso,\'%d/%m/%Y\')',$this->fecha_ingreso])
              //postgres
              ->andFilterWhere(['like', 'to_char(pase_internacion.fecha_ingreso,\'DD/MM/YYYY\')',$this->fecha_ingreso])
              ->andFilterWhere(['like', 'lower(pase_internacion.nombre)',strtolower($this->id_internacion)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('paseinternacion-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
