<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\mpi\models\PServicio;

/**
 * This is the model class for table "internacion.pase_internacion".
 *
 * @property int $id
 * @property int $id_sector
 * @property int $dias_estadia
 * @property int $numero_pase
 * @property string $fecha_ingreso
 * @property int $id_internacion
 */
class PaseInternacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.pase_internacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sector', 'dias_estadia', 'numero_pase', 'id_internacion'], 'default', 'value' => null],
            [['id_sector', 'dias_estadia', 'numero_pase', 'id_internacion'], 'integer'],
            [['fecha_ingreso'], 'safe'],
            [['id_internacion'], 'exist', 'skipOnError' => true, 'targetClass' => Internacion::className(), 'targetAttribute' => ['id_internacion' => 'id']],
            [['id_sector'], 'exist', 'skipOnError' => true, 'targetClass' => Sector::className(), 'targetAttribute' => ['id_sector' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sector' => 'Servicio',
            'dias_estadia' => 'Dias Estadia',

            'numero_pase' => 'Numero Pase',
            'fecha_ingreso' => 'Fecha Ingreso',
            'id_internacion' => 'Internacion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.pase_internacion.id', 10),
			'id_sector' => array('internacion.sector.descripcion',10),
			'dias_estadia' => array('internacion.pase_internacion.dias_estadia', 13),
			'numero_pase' => array('internacion.pase_internacion.numero_pase', 12),
			// postgres
			'fecha_ingreso' => array('TO_CHAR(internacion.pase_internacion.fecha_ingreso,\'DD/MM/YYYY\')', 14),
			// mysql
			//'fecha_ingreso' => array('DATE_FORMAT(internacion.pase_internacion.fecha_ingreso,\'%d/%m/%Y\')', 14),
			'id_internacion' => array('internacion.internacion.numero_informe',15),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_sector'=>
			[
				'attribute'=>'sector.descripcion',
				'label'=>'Sector',
			],
			'dias_estadia',
			'numero_pase',
			'fecha_ingreso',
			'id_internacion'=>
			[
				'attribute'=>'internacion.numero_informe',
				'label'=>'Internacion',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_sector',
				'value'=>'sector.descripcion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'dias_estadia',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'numero_pase',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_ingreso',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_internacion',
				'value'=>'internacion.numero_informe',
			],
        ];
    }

   
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha_ingreso=Metodos::dateConvert($this->fecha_ingreso,'View');
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha_ingreso=Metodos::dateConvert($this->fecha_ingreso,'toSql');
            return true;
        } else {
            return false;
        }
    }



    public function getInternacion(){
        return $this->hasOne(Internacion::className(),['id'=>'id_internacion']);
    }

    public function getSector(){
        return $this->hasOne(Sector::className(),['id'=>'id_sector']);
    }
}
