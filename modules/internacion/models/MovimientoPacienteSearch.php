<?php

namespace app\modules\internacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MovimientoPacienteSearch represents the model behind the search form about `app\modules\internacion\models\MovimientoPaciente`.
 */
class MovimientoPacienteSearch extends MovimientoPaciente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'fecha', 'unidad_operativa', 'sector_internacion', 'dia', 'existencia', 'ingresos', 'pase', 'paciente_vivo', 'paciente_fallecido', 'pases', 'ingresos_egresos', 'id_establecimiento', 'cama_disponible'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MovimientoPaciente::find();
		$query->joinWith(['establecimiento']);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				// mysql
				// 'fecha'=> [
					// 'asc' => ['date_format(movimientopaciente.fecha, "%Y%m%d")' => SORT_ASC],
					// 'desc' => ['date_format(movimientopaciente.fecha,"%Y%m%d")' => SORT_DESC],
				// ],
				// postgres
				'fecha',
				'unidad_operativa',
				'sector_internacion',
				'dia',
				'existencia',
				'ingresos',
				'pase',
				'paciente_vivo',
				'paciente_fallecido',
				'pases',
				'ingresos_egresos',
				'id_establecimiento'=> [
					'asc' => ['establecimiento.nombre' => SORT_ASC],
					'desc' => ['establecimiento.nombre' => SORT_DESC],
				],
				'cama_disponible',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'movimiento_paciente.id' => $this->id,
            'movimiento_paciente.dia' => $this->dia,
            'movimiento_paciente.existencia' => $this->existencia,
            'movimiento_paciente.ingresos' => $this->ingresos,
            'movimiento_paciente.pase' => $this->pase,
            'movimiento_paciente.paciente_vivo' => $this->paciente_vivo,
            'movimiento_paciente.paciente_fallecido' => $this->paciente_fallecido,
            'movimiento_paciente.pases' => $this->pases,
            'movimiento_paciente.ingresos_egresos' => $this->ingresos_egresos,
            'movimiento_paciente.cama_disponible' => $this->cama_disponible,
        ]);

        $query//mysql
              // ->andFilterWhere(['like', 'date_format(movimiento_paciente.fecha,\'%d/%m/%Y\')',$this->fecha])
              //postgres
              ->andFilterWhere(['like', 'to_char(movimiento_paciente.fecha,\'DD/MM/YYYY\')',$this->fecha])
              ->andFilterWhere(['like', 'lower(movimiento_paciente.unidad_operativa)',strtolower($this->unidad_operativa)])
              ->andFilterWhere(['like', 'lower(movimiento_paciente.sector_internacion)',strtolower($this->sector_internacion)])
              ->andFilterWhere(['like', 'lower(consultasestadisticas.establecimiento.nombre)',strtolower($this->id_establecimiento)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('movimientopaciente-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
