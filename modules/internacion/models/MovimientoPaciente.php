<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\mpi\models\Establecimiento;

/**
 * This is the model class for table "internacion.movimiento_paciente".
 *
 * @property int $id
 * @property string $fecha
 * @property string $unidad_operativa
 * @property string $sector_internacion
 * @property int $dia
 * @property int $existencia
 * @property int $ingresos
 * @property int $pase
 * @property int $paciente_vivo
 * @property int $paciente_fallecido
 * @property int $pases
 * @property int $ingresos_egresos
 * @property int $id_establecimiento
 * @property int $cama_disponible
 */
class MovimientoPaciente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.movimiento_paciente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [[ 'dia', 'existencia', 'ingresos', 'pase', 'paciente_vivo', 'paciente_fallecido', 'pases', 'ingresos_egresos', 'id_establecimiento', 'cama_disponible'], 'default', 'value' => null],
            [['dia', 'existencia', 'ingresos', 'pase', 'paciente_vivo', 'paciente_fallecido', 'pases', 'ingresos_egresos', 'id_establecimiento', 'cama_disponible'], 'integer'],
            [['fecha'], 'safe'],
            [['unidad_operativa', 'sector_internacion'], 'string'],
            [['id_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimiento::className(), 'targetAttribute' => ['id_establecimiento' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'fecha' => 'Fecha',
            'unidad_operativa' => 'Unidad Operativa',
            'sector_internacion' => 'Sector Internacion',
            'dia' => 'Dia',
            'existencia' => 'Existencia',
            'ingresos' => 'Ingresos',
            'pase' => 'Pase',
            'paciente_vivo' => 'Paciente Vivo',
            'paciente_fallecido' => 'Paciente Fallecido',
            'pases' => 'Pases',
            'ingresos_egresos' => 'Ingresos Egresos',
            'id_establecimiento' => 'Establecimiento',
            'cama_disponible' => 'Cama Disponible',
        ];
    }
    public function attributePrint()
    {
        return [

			// postgres
			'fecha' => array('TO_CHAR(internacion.movimiento_paciente.fecha,\'DD/MM/YYYY\')', 10),
			// mysql
			'fecha' => array('DATE_FORMAT(internacion.movimiento_paciente.fecha,\'%d/%m/%Y\')', 10),
			'unidad_operativa' => array('internacion.movimiento_paciente.unidad_operativa', 17),
			'sector_internacion' => array('internacion.movimiento_paciente.sector_internacion', 19),
			'dia' => array('internacion.movimiento_paciente.dia', 10),
			'existencia' => array('internacion.movimiento_paciente.existencia', 11),
			'ingresos' => array('internacion.movimiento_paciente.ingresos', 10),
			'pase' => array('internacion.movimiento_paciente.pase', 10),
			'paciente_vivo' => array('internacion.movimiento_paciente.paciente_vivo', 14),
			'paciente_fallecido' => array('internacion.movimiento_paciente.paciente_fallecido', 19),
			'pases' => array('internacion.movimiento_paciente.pases', 10),
			'ingresos_egresos' => array('internacion.movimiento_paciente.ingresos_egresos', 17),
			'id_establecimiento' => array('consultasestadisticas.establecimiento.nombre',19),
			'cama_disponible' => array('internacion.movimiento_paciente.cama_disponible', 16),
        ];
    }

    public function attributeView()
    {
        return [
			'fecha',
			'unidad_operativa',
			'sector_internacion',
			'dia',
			'existencia',
			'ingresos',
			'pase',
			'paciente_vivo',
			'paciente_fallecido',
			'pases',
			'ingresos_egresos',
			'id_establecimiento'=>
			[
				'attribute'=>'establecimiento.nombre',
				'label'=>'Establecimiento',
			],
			'cama_disponible',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'unidad_operativa',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'sector_internacion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'dia',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'existencia',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'ingresos',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'pase',
			],
			[
				 'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paciente_vivo',
			],
			 [
				 'class'=>'\kartik\grid\DataColumn',
				 'attribute'=>'paciente_fallecido',
			 ],
			[
				 'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'pases',
			 ],
			[
				 'class'=>'\kartik\grid\DataColumn',
				 'attribute'=>'ingresos_egresos',
			 ],
				[
				'class'=>'\kartik\grid\DataColumn',
				 'attribute'=>'id_establecimiento',
				 'value'=>'establecimiento.nombre',
			],
			[
				 'class'=>'\kartik\grid\DataColumn',
				 'attribute'=>'cama_disponible',
			],
        ];
    }

	public function getEstablecimiento(){
        return $this->hasOne(Establecimiento::className(),['id'=>'id_establecimiento']);
    }
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha=Metodos::dateConvert($this->fecha,'View');
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha=Metodos::dateConvert($this->fecha,'toSql');
            return true;
        } else {
            return false;
        }
    }
}
