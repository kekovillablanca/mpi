<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\mpi\models\Sexo;

/**
 * This is the model class for table "internacion.obstetricia".
 *
 * @property int $id
 * @property string $peso_al_nacer
 * @property int $id_condicion_nacimiento
 * @property int $id_terminacion
 * @property int $id_sexo
 * @property int $id_internacion
 */
class Obstetricia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.obstetricia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['peso_al_nacer'], 'string'],
            [['id_condicion_nacimiento', 'id_terminacion', 'id_sexo', 'id_internacion'], 'default', 'value' => null],
            [['id_condicion_nacimiento', 'id_terminacion', 'id_sexo', 'id_internacion'], 'integer'],
            [['id_sexo'], 'exist', 'skipOnError' => true, 'targetClass' => Sexo::className(), 'targetAttribute' => ['id_sexo' => 'id']],
            [['id_condicion_nacimiento'], 'exist', 'skipOnError' => true, 'targetClass' => CondicionAlNacer::className(), 'targetAttribute' => ['id_condicion_nacimiento' => 'id']],
            [['id_internacion'], 'exist', 'skipOnError' => true, 'targetClass' => Internacion::className(), 'targetAttribute' => ['id_internacion' => 'id']],
            [['id_terminacion'], 'exist', 'skipOnError' => true, 'targetClass' => Terminacion::className(), 'targetAttribute' => ['id_terminacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'peso_al_nacer' => 'Peso Al Nacer',
            'id_condicion_nacimiento' => 'Condicion Nacimiento',
            'id_terminacion' => 'Terminacion',
            'id_sexo' => 'Sexo',
            'id_internacion' => 'internacion numero informe',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.obstetricia.id', 10),
			'peso_al_nacer' => array('internacion.obstetricia.peso_al_nacer', 14),
			'id_condicion_nacimiento' => array('internacion.condicion_al_nacer.nombre',20),
			'id_terminacion' => array('internacion.terminacion.nombre',15),
			'id_sexo' => array('consultasestadisticas.sexo.nombre',10),
			'id_internacion' => array('internacion.internacion.numero_informe',12),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'peso_al_nacer',
			'id_condicion_nacimiento'=>
			[
				'attribute'=>'condicionnacimiento.condicion',
				'label'=>'Condicion Nacimiento',
			],
			'id_terminacion'=>
			[
				'attribute'=>'terminacion.terminacion',
				'label'=>'Terminacion',
			],
			'id_sexo'=>
			[
				'attribute'=>'sexo.nombre',
				'label'=>'Sexo',
			],
			'id_internacion'=>
			[
				'attribute'=>'internacion.numero_informe',
				'label'=>'internacion',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'id_internacion',
                    'value'=>'internacion.numero_informe',
                ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'peso_al_nacer',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_condicion_nacimiento',
				'value'=>'condicionnacimiento.condicion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_terminacion',
				'value'=>'terminacion.terminacion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_sexo',
				'value'=>'sexo.nombre',
			],

        ];
    }
    public function getSexo(){
        return $this->hasOne(Sexo::className(),['id'=>'id_sexo']);
    }
    
    public function getTerminacion(){
        return $this->hasOne(Terminacion::className(),['id'=>'id_terminacion']);
    }
    public function getInternacion(){
        return $this->hasOne(Internacion::className(),['id'=>'id_internacion']);
    }
    public function getCondicionnacimiento(){
        return $this->hasOne(CondicionAlNacer::className(),['id'=>'id_condicion_nacimiento']);
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
