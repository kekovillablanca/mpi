<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.cirugia".
 *
 * @property int $id
 * @property int $id_internacion
 * @property string $descripcion
 * @property string $codigo
 */
class Cirugia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.cirugia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_internacion'], 'default', 'value' => null],
            [['id_internacion'], 'integer'],
            [['descripcion', 'codigo'],'required','message' => 'Este campo es obligatorio.'],
            [['descripcion', 'codigo'], 'string'],
            [['id_internacion'], 'exist', 'skipOnError' => true, 'targetClass' => Internacion::className(), 'targetAttribute' => ['id_internacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_internacion' => 'Internacion numero',
            'descripcion' => 'Descripcion',
            'codigo' => 'Codigo',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('cirugia.id', 10),
			'id_internacion' => array('internacion.numero_informe',15),
			'descripcion' => array('cirugia.descripcion', 12),
			'codigo' => array('cirugia.codigo', 10),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_internacion'=>
			[
				'attribute'=>'internacion.numero_informe',
				'label'=>'Internacion',
			],
			'descripcion',
			'codigo',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_internacion',
				'value'=>'internacion.numero_informe',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'codigo',
			],
        ];
    }
public function getInternacion(){
    return $this->hasOne(Internacion::className(),["id"=>"id_internacion"]);
}
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
