<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.causa_externa".
 *
 * @property int $id
 * @property int $id_lugar
 * @property string $nombre
 * @property string $como_se_produjo
 * @property int $id_producido_por
 */
class CausaExterna extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.causa_externa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_lugar', 'id_producido_por'], 'integer'],
            [['id_lugar','id_producido_por','nombre','como_se_produjo'], 'default', 'value' => null],
            [['nombre'], 'string', 'max' => 40],
            [['como_se_produjo'], 'string', 'max' => 100],
            [['id_lugar'], 'exist', 'skipOnError' => true, 'targetClass' => Lugar::className(), 'targetAttribute' => ['id_lugar' => 'id']],
            [['id_producido_por'], 'exist', 'skipOnError' => true, 'targetClass' => ProducidoPor::className(), 'targetAttribute' => ['id_producido_por' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_lugar' => 'Lugar que se produjo',
            'nombre' => 'Nombre de Causa',
            'como_se_produjo' => 'Como se Produjo',
            'id_producido_por' => 'Producido Por',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.causa_externa.id', 10),
			'id_lugar' => array('internacion.lugar.nombre',10),
			'nombre' => array('internacion.causa_externa.nombre', 20),
			'como_se_produjo' => array('internacion.causa_externa.como_se_produjo', 20),
			'id_producido_por' => array('internacion.producido_por.nombre',17),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_lugar'=>
			[
				'attribute'=>'lugar.nombre',
				'label'=>'Lugar',
			],
			'nombre',
			'como_se_produjo',
			'id_producido_por'=>
			[
				'attribute'=>'producidopor.nombre',
				'label'=>'Producido Por',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_lugar',
				'value'=>'lugar.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'como_se_produjo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_producido_por',
				'value'=>'producidopor.nombre',
			],
        ];
    }
    public function getProducidopor(){
        return $this->hasOne(ProducidoPor::className(),['id'=>'id_producido_por']);

    }
    public function getLugar(){
        return $this->hasOne(Lugar::className(),['id'=>'id_lugar']);

    }


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
