<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.universitario".
 *
 * @property int $id
 * @property string $descripcion
 */
class Universidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.universitario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'termino'], 'required'],
            [['descripcion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.universitario.id', 10),
			'descripcion' => array('internacion.universitario.descripcion', 12),

        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'descripcion',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion',
			],

        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }
    public function getSistemaEducativoUniversitario()
    {
        return $this->hasMany(SistemaEducativoUniversitario::class, ['id_universitario' => 'id']);
    }

    public function getSistemasEducativos()
    {
        return $this->hasMany(SistemaEducativo::class, ['id' => 'id_sistema_educativo'])
            ->via('sistemaEducativoUniversitario');
    }
    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
