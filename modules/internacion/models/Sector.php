<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.sector".
 *
 * @property int $id
 * @property string $cod_sector
 * @property string $descripcion
 */
class Sector extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.sector';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_sector', 'descripcion'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_sector' => 'Cod Sector',
            'descripcion' => 'Descripcion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.sector.id', 10),
			'cod_sector' => array('internacion.sector.cod_sector', 20),
			'descripcion' => array('internacion.sector.descripcion', 20),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'cod_sector',
			'descripcion',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'cod_sector',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'descripcion',
			],
        ];
    }

    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
