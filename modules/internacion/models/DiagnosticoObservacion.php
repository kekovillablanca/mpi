<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\mpi\models\Diagnostico;

/**
 * This is the model class for table "internacion.diagnostico_observacion".
 *
 * @property int $id
 * @property int $id_diagnostico
 * @property int $id_internacion
 * @property string $observacion
 */
class DiagnosticoObservacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.diagnostico_observacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_diagnostico'],'required'],
            [['id_diagnostico', 'id_internacion'], 'default', 'value' => null],
            [['id_diagnostico', 'id_internacion'], 'integer'],
            [['observacion'], 'string'],
            [['id_diagnostico'], 'exist', 'skipOnError' => true, 'targetClass' => Diagnostico::className(), 'targetAttribute' => ['id_diagnostico' => 'id']],
            [['id_internacion'], 'exist', 'skipOnError' => true, 'targetClass' => Internacion::className(), 'targetAttribute' => ['id_internacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_diagnostico' => 'Diagnostico',
            'id_internacion' => 'Numero informe internacion',
            'observacion' => 'Observacion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.diagnostico_observacion.id', 10),
			'id_diagnostico' => array('consultasestadisticas.diagnostico.nombre',15),
			'id_internacion' => array('internacion.internacion.numero_informe',15),
			'observacion' => array('internacion.diagnostico_observacion.observacion', 12),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_diagnostico'=>
			[
				'attribute'=>'diagnostico.nombre',
				'label'=>'Diagnostico nombre',
			],
			'id_internacion'=>
			[
				'attribute'=>'internacion.numero_informe',
				'label'=>'Internacion numero',
			],
			'observacion',
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_diagnostico',
				'value'=>'diagnostico.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_internacion',
				'value'=>'internacion.numero_informe',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'observacion',
			],
        ];
    }
public function getInternacion(){
    return $this->hasOne(Internacion::className(),['id'=>'id_internacion']);
}
public function getDiagnostico(){
    return $this->hasOne(Diagnostico::className(),['id'=>'id_diagnostico']);
}
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            return true;
        } else {
            return false;
        }
    }
}
