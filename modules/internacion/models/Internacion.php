<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\mpi\models\Diagnostico;
use app\modules\mpi\models\Establecimiento;
use app\modules\mpi\models\Paciente;
use app\modules\mpi\models\Sexo;
use DateTime;
use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "internacion.internacion".
 * 
 * @property int $id
 * @property int $id_embarazo
 * @property int $id_causa_externa
 * @property int $id_paciente
 * @property string $fecha_ingreso
 * @property string $fecha_egreso
 * @property int $total_dias_estadia
 * @property int $id_tipo_egreso
 * @property int $id_establecimiento
 * @property int $anio_activo
 * @property int $numero_informe
 * @property int $id_diagnostico
 * @property int $id_lugar_ingreso
 * @property int $id_sector
 * @property string $estado

 * 
 */
class Internacion extends \yii\db\ActiveRecord
{
	public $pacienteDni;
	public $pacienteApellido;
	public $embarazoEdadGestacional;
	public $embarazoParidad;
	public $embarazoTipoParto;
	public $pacienteEducacion;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.internacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_embarazo', 'id_causa_externa', 'total_dias_estadia', 'id_tipo_egreso','estado'], 'default', 'value' => null],
			[['anio_activo'], 'default', 'value' => 2024],
            [['id_embarazo','id','id_lugar_ingreso', 'id_causa_externa','anio_activo', 'numero_informe', 'total_dias_estadia', 'id_tipo_egreso','id_establecimiento','id_diagnostico','id_sector'] ,'integer'],
            [['id_establecimiento','anio_activo','id_lugar_ingreso','fecha_ingreso','id_sector'],'required','message' => 'Este campo es obligatorio.'],
			[['id_diagnostico'], 'required', 'when' => function ($model, $param) {
			
				return $model->id_diagnostico != -1;
			}],
			/*[['fecha_egreso'], 'required', 'when' => function ($model, $param) {
				return $model->fecha_egreso >= $model->fecha_ingreso;
			}, 'whenClient' => "function (attribute, value) {
				var fechaIngreso = new Date($('#internacion-fecha_ingreso').val());
				var fechaEgreso = new Date(value);
				
				// Check if both dates are valid before running validation
				if (!isNaN(fechaIngreso.getTime()) && !isNaN(fechaEgreso.getTime())) {
					if (fechaEgreso < fechaIngreso) {
						// Add error message and class
						var errorMessage = 'La fecha de egreso debe ser igual o mayor que la fecha de ingreso.';
						var formGroup = $('#internacion-fecha_egreso').closest('.form-group');
						formGroup.addClass('has-erroroveClass('has-success'); // Add error class
						formGroup.find('.help-block').text(errorMessage).show();
						return false;
					} else {
						// Clear the error message and class
						var formGroup = $('#internacion-fecha_egreso').closest('.form-group');
						formGroup.removeClass('has-error').addClass('has-success'); // Add success class
						formGroup.find('.help-block').text('').hide();
					}
				}
				return true;
			}"],*/
			[['fecha_egreso','fecha_ingreso'], 'datetime', 'format' => 'php:d/m/Y H:i:s', 'message' => 'La fecha y hora deben estar en el formato dd/MM/yyyy HH:mm:ss.'],
			//[['fecha_ingreso'], 'datetime', 'format' => 'php:d-m-Y H:i:s', 'message' => 'La fecha y hora deben estar en el formato dd-MM-yyyy HH:mm:ss.'],
            //[['id_causa_externa'], 'exist', 'skipOnError' => true, 'targetClass' => CausaExterna::className(), 'targetAttribute' => ['id_causa_externa' => 'id']],
            [['id_embarazo'], 'exist', 'skipOnError' => true, 'targetClass' => Embarazo::className(), 'targetAttribute' => ['id_embarazo' => 'id']],
			[['id_paciente'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['id_paciente' => 'id']],
			[["id_establecimiento"], 'exist', 'skipOnError' => true, 'targetClass' => Establecimiento::className(), 'targetAttribute' => ['id_establecimiento' => 'id']],
			//[["id_diagnostico"], 'exist', 'skipOnError' => true, 'targetClass' => Diagnostico::className(), 'targetAttribute' => ['id_diagnostico' => 'id']],
			[["id_lugar_ingreso"], 'exist', 'skipOnError' => true, 'targetClass' => LugarIngreso::className(), 'targetAttribute' => ['id_lugar_ingreso' => 'id']],
			[["id_sector"], 'exist', 'skipOnError' => true, 'targetClass' => Sector::className(), 'targetAttribute' => ['id_sector' => 'id']],
			[['id_tipo_egreso'], 'exist', 'skipOnError' => true, 'targetClass' => TipoEgreso::className(), 'targetAttribute' => ['id_tipo_egreso' => 'id']],

		
		];
    }

    /**
     * {@inheritdoc}').rem
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_embarazo' => 'Embarazo',
			'embarazoEdadGestacional' => 'Embarazo gestional',
			'embarazoParidad' => 'Embarazo paridad',
			'embarazoTipoParto'=>'Embarazo tipo de parto',
            'id_causa_externa' => 'Causa Externa',
            'fecha_ingreso' => 'Fecha Ingreso',
            'fecha_egreso' => 'Fecha Egreso',
            'total_dias_estadia' => 'Total Dias Estadia',
            'id_tipo_egreso' => 'Tipo Egreso',
			"id_establecimiento"=>"Establecimiento",
			"anio_activo"=> "año activo",
			"id_diagnostico"=> "Diagnostico principal",
			"numero_informe"=>"numero informe",
			'id_paciente'=>"Paciente",
			"id_lugar_ingreso"=>"Lugar ingreso",
			"id_sector"=>"Servicio",
			"estado"=>"Estado",
			"pacienteDni"=>"Dni del paciente",
			"pacienteApellido"=>"Apellido del paciente",
			"pacienteEducacion"=>"Educacion del paciente"
			
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.internacion.id', 10),

			'id_embarazo' => array('TO_CHAR(internacion.embarazo.fecha_fin_embarazo,\'DD/MM/YYYY\')', 19),
			'embarazoEdadGestacional'=>array('internacion.embarazo.edad_gestacional',17),
			'embarazoParidad'=>array('internacion.embarazo.paridad',17),
			'embarazoTipoParto'=>array('internacion.tipo_parto.nombre',17),
			'id_causa_externa' => array('internacion.causa_externa.nombre',17),
			'fecha_ingreso' => array("TO_CHAR(internacion.fecha_ingreso, 'DD/MM/YYYY HH24:MI')", 14),
			'fecha_egreso' => array("TO_CHAR(internacion.fecha_egreso, 'DD/MM/YYYY HH24:MI')", 13),

			// mysql
			//'fecha_egreso' => array('DATE_FORMAT(internacion.internacion.fecha_egreso,\'%d/%m/%Y\')', 13),
			'total_dias_estadia' => array('internacion.internacion.total_dias_estadia', 20),
			'id_tipo_egreso' => array('internacion.tipo_egreso.nombre', 15),

			"id_establecimiento"=>array("consultasestadisticas.establecimiento.nombre",20),
			"anio_activo"=>array("internacion.anio_activo",10),
			"numero_informe"=>array("internacion.numero_informe",10),
			"id_diagnostico"=> array("consultasestadisticas.diagnostico.nombre",10),
	
			"id_paciente"=>array("consultasestadisticas.paciente.nombre",10),
			"pacienteDni"=>array("consultasestadisticas.paciente.numero_documento",10),
			"pacienteApellido"=>array("consultasestadisticas.paciente.apellido",10),
			"id_lugar_ingreso"=>array("internacion.lugar_ingreso.nombre",10),
			"id_sector"=>array("internacion.sector.descripcion",10),
			"estado"=>array("internacion.estado",10),
			"pacienteEducacion"=>array("internacion.asistio.nombre",10),


        ];
    }
	public function attributeView()
	{
		return [
			'id',
			'id_causa_externa' => [
				'attribute' => 'causaexterna.nombre',
				'label' => 'Causa Externa',
			],
			'fecha_ingreso',
			'estado',
			'fecha_egreso',
			'total_dias_estadia',
			"numero_informe",
			'id_tipo_egreso' => [
				'attribute' => 'tipoegreso.nombre',
				'label' => 'Tipo Egreso',
			],
			"id_establecimiento" => [
				'attribute' => 'establecimiento.nombre',
				'label' => 'Establecimiento',
			],
			"id_diagnostico" => [
				'attribute' => 'diagnostico.nombre',
				'label' => 'Diagnóstico',
			],
			"anio_activo" => [
				'attribute' => 'anio_activo',
				'label' => 'Año activo',
			],
			"id_paciente" => [
				'attribute' => 'paciente.nombre',
				'label' => 'Paciente',
			],
			"id_lugar_ingreso" => [
				'attribute' => 'lugaringreso.nombre',
				'label' => 'Lugar de ingreso',
			],
			"id_sector" => [
				'attribute' => 'sector.descripcion',
				'label' => 'Servicio',
			],
			"pacienteDni" => [
				'attribute' => 'paciente.numero_documento',
				'label' => 'Número de documento',
			],
			"pacienteApellido" => [
				'attribute' => 'paciente.apellido',
				'label' => 'Apellido del paciente',
			],
			"embarazoEdadGestacional" => [
				'attribute' => 'embarazo.edad_gestacional',
				'label' => 'Embarazo edad gestacional',
			],
			"embarazoParidad" => [
				'attribute' => 'embarazo.paridad',
				'label' => 'Embarazo paridad',
			],
			"embarazoTipoParto" => [
				'attribute' => 'embarazo.tipoparto.nombre',
				'label' => 'Embarazo tipo parto',
			],
			'id_embarazo' => [
				'attribute' => 'embarazo.fecha_fin_embarazo',
				'label' => 'Fecha fin embarazo',
			],

			"pacienteEducacion"=>[
				'attribute' => 'paciente.sistemaeducativo.idasistio.nombre',
				'label' => 'Educacion del paciente',
			],

		];
	}
	

	public function attributeColumns()
	{
		return [
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_embarazo',
				'value' => 'embarazo.fecha_fin_embarazo',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'embarazoParidad',
				'value' => 'embarazo.paridad',
			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'embarazoEdadGestacional',
				'value' => 'embarazo.edad_gestacional',
			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'embarazoTipoParto',
				'value' => 'embarazo.tipoparto.nombre',
			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_causa_externa',
				'value' => 'causaexterna.nombre',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'fecha_ingreso',

			],
			[
			'class' => '\kartik\grid\DataColumn',
			'attribute' => 'estado',
				'value' => 'estadoTextoFormateado', // Utiliza el método en el modelo
				'format' => 'raw', // Permite el HTML en la salida
			],  
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'fecha_egreso',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'total_dias_estadia',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'numero_informe',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_tipo_egreso',
				'value' => 'tipoegreso.nombre',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_establecimiento',
				'value' => 'establecimiento.nombre',
		
			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_diagnostico',
				'value' => 'diagnostico.nombre',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'anio_activo',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_paciente',
				'value' => 'paciente.nombre',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'pacienteDni',
				'value' => 'paciente.numero_documento',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'pacienteApellido',
				'value' => 'paciente.apellido',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_lugar_ingreso',
				'value' => 'lugaringreso.nombre',

			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'id_sector',
				'value' => 'sector.descripcion',
			],
			[
				'class' => '\kartik\grid\DataColumn',
				'attribute' => 'pacienteEducacion',
				'value' => 'paciente.sistemaeducativo.idasistio.nombre',
			],


		];
	}
	public function getEstadoTextoFormateado()
	{
		if ($this->estado == "FINALIZADO") {
			return Html::tag('span', 'FINALIZADO', ['style' => 'color: green;']);
		}
		return Html::tag('span', 'PROCESO', ['style' => 'color: red;']);
	}
private function obtenerNombreEstablecimiento()
{
    return $this->getEstablecimiento()->select('nombre');
}





	
	public function getDiagnosticos()
	{
		return  DiagnosticoObservacion::find()
		->joinWith('diagnostico') // Realiza el JOIN con la relación 'diagnostico'
		->all();
	}
public function getDiagnosticoobservacion()
{
    return $this->hasMany(DiagnosticoObservacion::className(), ['id_internacion' => 'id']);
}

public function getEmbarazos(){
	return  Obstetricia::find()
	->joinWith('internacion') // Realiza el JOIN con la relación 'diagnostico'
	->all();
}

public function getObservaciones()
{
    $diagnosticoObservaciones = $this->getDiagnosticoobservacion()->all(); // Llama a all() para obtener una colección
    $observacionesArray = [];
    if (!empty($diagnosticoObservaciones)) {
        // Extraer las observaciones y agregarlas al array
        foreach ($diagnosticoObservaciones as $observacion) {
            $observacionesArray[] = $observacion->observacion; // Agregar cada observación
        }
    }
    return !empty($observacionesArray) ? implode(", ", $observacionesArray) : 'vacio'; // Devuelve 'vacio' si no hay observaciones
}
	public function getPaciente(){

		return $this->hasOne(Paciente::className(),['id'=>'id_paciente']);
	}
	public function getLugaringreso(){

		return $this->hasOne(LugarIngreso::className(),['id'=>'id_lugar_ingreso']);
	}
	public function getSector(){

		return $this->hasOne(Sector::className(),['id'=>'id_sector']);
	}
	public function getTipoegreso()
	{
		return $this->hasOne(TipoEgreso::className(), ['id' => 'id_tipo_egreso']);
	}
	public function getDiagnostico(){

		return $this->hasOne(Diagnostico::className(),['id'=>'id_diagnostico']);
	}
	public function getCausaexterna(){

		return $this->hasOne(CausaExterna::className(),['id'=>'id_causa_externa']);
	}

	public function getEmbarazo(){

		return $this->hasOne(Embarazo::className(),['id'=>'id_embarazo']);
	}

	public function getEstablecimiento(){

		return $this->hasOne(Establecimiento::className(),['id'=>'id_establecimiento']);
	}
	public function getEstablecimientointernacion(){

		return $this->hasOne(Establecimiento::className(),['id'=>'id_establecimiento'])->select('nombre');
	}


    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha_ingreso=Metodos::dateConvert($this->fecha_ingreso,'View');
		$this->fecha_egreso=Metodos::dateConvert($this->fecha_egreso,'View');
    }
	public function beforeDelete()
	{
		// Verificamos que la validación del padre se pase correctamente
		if (!parent::beforeDelete()) {
			return false;
		}
	
		// Verificamos si el paciente está asociado y si está en internación antes de eliminarlo
		if ($this->paciente && $this->paciente->is_internacion) {
			// Intentamos desasociar al paciente antes de eliminarlo
			$this->id_paciente = null;
	
			if (!$this->save(false)) {
				Yii::error('No se pudo desasociar al paciente antes de eliminar.');
				return false; // Detenemos la eliminación
			}
		}
		
	
		// Todo se eliminó correctamente
		return true;
	}


	

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
		if($this->id_diagnostico==-1)
			$this->id_diagnostico=null;
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha_ingreso=Metodos::dateConvert($this->fecha_ingreso,'toSql');
			$this->fecha_egreso=Metodos::dateConvert($this->fecha_egreso,'toSql');
			//var_dump($this->fecha_egreso);
			//exit;
            return true;
        } else {
            return false;
        }
    }
}
