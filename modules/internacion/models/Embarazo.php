<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;

/**
 * This is the model class for table "internacion.embarazo".
 *
 * @property int $id
 * @property string $fecha_fin_embarazo
 * @property int $edad_gestacional
 * @property int $paridad
 * @property int $id_tipo_parto
 */
class Embarazo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.embarazo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_fin_embarazo'], 'safe'],
            [['edad_gestacional', 'paridad', 'id_tipo_parto'], 'default', 'value' => null],
            [['edad_gestacional', 'paridad', 'id_tipo_parto'], 'integer'],
            [['id_tipo_parto'], 'exist', 'skipOnError' => true, 'targetClass' => TipoParto::className(), 'targetAttribute' => ['id_tipo_parto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_fin_embarazo' => 'Fecha Fin Embarazo',
            'edad_gestacional' => 'Edad Gestacional',
            'paridad' => 'Paridad',
            'id_tipo_parto' => 'Tipo Parto',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.embarazo.id', 10),
			// postgres
			'fecha_fin_embarazo' => array('TO_CHAR(internacion.embarazo.fecha_fin_embarazo,\'DD/MM/YYYY\')', 19),
			// mysql
			//'fecha_fin_embarazo' => array('DATE_FORMAT(internacion.embarazo.fecha_fin_embarazo,\'%d/%m/%Y\')', 19),
			'edad_gestacional' => array('internacion.embarazo.edad_gestacional', 17),
			'paridad' => array('internacion.embarazo.paridad', 10),
			'id_tipo_parto' => array('internacion.tipo_parto.nombre',14),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'fecha_fin_embarazo',
			'edad_gestacional',
			'paridad',
			'id_tipo_parto'=>
			[
				'attribute'=>'tipoparto.nombre',
				'label'=>'Tipo Parto',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'fecha_fin_embarazo',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'edad_gestacional',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'paridad',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_tipo_parto',
				'value'=>'tipoparto.nombre',
			],
        ];
    }

    public function getTipoparto(){
        return $this->hasOne(TipoParto::className(),['id'=>'id_tipo_parto']);
    }
    public function afterFind(){

        // tareas despues de encontrar el objeto
        parent::afterFind();
		$this->fecha_fin_embarazo=Metodos::dateConvert($this->fecha_fin_embarazo,'View');
    }

    public function beforeSave($insert)
    {
        // tareas antes de encontrar el objeto
        if (parent::beforeSave($insert)) {
            // Place your custom code here
			$this->fecha_fin_embarazo=Metodos::dateConvert($this->fecha_fin_embarazo,'toSql');
            return true;
        } else {
            return false;
        }
    }
}
