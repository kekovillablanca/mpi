<?php
/* Modelo generado por Model(Q) */
namespace app\modules\internacion\models;

use Yii;
use app\components\Metodos\Metodos;
use app\modules\mpi\models\Paciente;

/**
 * This is the model class for table "internacion.situacion_laboral".
 *
 * @property int $id
 * @property int $id_tipo_situacion
 * @property int $id_paciente
 * @property int $id_ocupacion
 */
class SituacionLaboral extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'internacion.situacion_laboral';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_situacion', 'id_paciente', 'id_ocupacion'], 'default', 'value' => null],
            [['id_tipo_situacion'], 'required'],
            [['id_tipo_situacion', 'id_paciente', 'id_ocupacion'], 'integer'],
            [['id_ocupacion'], 'required', 'when' => function ($model) {
                return in_array($model->id_tipo_situacion, ['2']);
            },'whenClient'=> "function(attribute,value){

            console.log(document.getElementById('situacionlaboral-id_tipo_situacion').value);
                return document.getElementById('situacionlaboral-id_tipo_situacion').value==2
            }"],
            [['id_paciente'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['id_paciente' => 'id']],
            [['id_ocupacion'], 'exist', 'skipOnError' => true, 'targetClass' => Ocupacion::className(), 'targetAttribute' => ['id_ocupacion' => 'id']],
            [['id_tipo_situacion'], 'exist', 'skipOnError' => true, 'targetClass' => TipoSituacionLaboral::className(), 'targetAttribute' => ['id_tipo_situacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tipo_situacion' => 'Tipo Situacion',
            'id_paciente' => ' Paciente',
            'id_ocupacion' => 'Ocupacion',
        ];
    }
    public function attributePrint()
    {
        return [
			'id' => array('internacion.situacion_laboral.id', 10),
			'id_tipo_situacion' => array('internacion.tipo_situacion_laboral.nombre',18),
			// postgres


			'id_paciente' => array('consultasestadisticas.paciente.nombre',12),
			'id_ocupacion' => array('internacion.ocupacion.nombre',13),
        ];
    }

    public function attributeView()
    {
        return [
			'id',
			'id_tipo_situacion'=>
			[
				'attribute'=>'tiposituacion.nombre',
				'label'=>'Tipo Situacion',
			],

			'id_paciente'=>
			[
				'attribute'=>'consultasestadisticas.paciente.nombre',
				'label'=>'Paciente',
			],
			'id_ocupacion'=>
			[
				'attribute'=>'tipoocupacion.nombre',
				'label'=>'Ocupacion',
			],
        ];
		// En el caso de un campo textarea, usar formato html. por ejemplo: observaciones:html
    }

    public function attributeColumns()
    {
        return [
			// [
				// 'class'=>'\kartik\grid\DataColumn',
				// 'attribute'=>'id',
				// 'width'=>'30px',
				// ],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_tipo_situacion',
				'value'=>'tiposituacion.nombre',
			],

			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_paciente',
				'value'=>'consultasestadisticas.paciente.nombre',
			],
			[
				'class'=>'\kartik\grid\DataColumn',
				'attribute'=>'id_ocupacion',
				'value'=>'tipoocupacion.nombre',
			],
        ];
    }

    public function getTiposituacion(){
        return $this->hasOne(TipoSituacionLaboral::className(),['id'=>'id_tipo_situacion']);
    }
    public function getTipoocupacion(){
        return $this->hasOne(Ocupacion::className(),['id'=>'id_ocupacion']);
    }





}
