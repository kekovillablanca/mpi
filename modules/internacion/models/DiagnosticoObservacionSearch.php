<?php

namespace app\modules\internacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DiagnosticoObservacionSearch represents the model behind the search form about `app\modules\internacion\models\DiagnosticoObservacion`.
 */
class DiagnosticoObservacionSearch extends DiagnosticoObservacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_diagnostico', 'id_internacion', 'observacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$modelId)
    {
        $query = DiagnosticoObservacion::find();
		$query->joinWith(['diagnostico']);
		$query->joinWith(['internacion']);
        $query->where(['id_internacion'=>$modelId]);
        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_diagnostico'=> [
					'asc' => ['diagnostico.nombre' => SORT_ASC],
					'desc' => ['diagnostico.nombre' => SORT_DESC],
				],
				'id_internacion'=> [
					'asc' => ['internacion.numero_informe' => SORT_ASC],
					'desc' => ['internacion.numero_informe' => SORT_DESC],
				],
				'observacion',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'diagnostico_observacion.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(consultasestadisticas.diagnostico.nombre)',strtolower($this->id_diagnostico)])
              ->andFilterWhere(['like', 'lower(internacion.internacion.numero_informe)',strtolower($this->id_internacion)])
              ->andFilterWhere(['like', 'lower(internacion.diagnostico_observacion.observacion)',strtolower($this->observacion)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('diagnosticoobservacion-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
