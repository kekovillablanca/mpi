<?php

namespace app\modules\internacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CirugiaSearch represents the model behind the search form about `app\modules\internacion\models\Cirugia`.
 */
class CirugiaSearch extends Cirugia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['id', 'id_internacion', 'descripcion', 'codigo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$modelId)
    {
        $query = Cirugia::find();
		$query->joinWith(['internacion']);
        $query->where(['id_internacion'=>$modelId]);

        $session = Yii::$app->session;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
				'id',
				'id_internacion'=> [
					'asc' => ['internacion.nombre' => SORT_ASC],
					'desc' => ['internacion.nombre' => SORT_DESC],
				],
				'descripcion',
				'codigo',
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            // return $dataProvider;
        }

        $query->andFilterWhere([
            'cirugia.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'lower(internacion.nombre)',strtolower($this->id_internacion)])
              ->andFilterWhere(['like', 'lower(cirugia.descripcion)',strtolower($this->descripcion)])
              ->andFilterWhere(['like', 'lower(cirugia.codigo)',strtolower($this->codigo)]);

        // guardo dataprovider en sesion para recuperarlo en otra accion
        $session->set('cirugia-dataprovider',$dataProvider);

        return $dataProvider;
    }
}
