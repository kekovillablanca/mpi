<?php

use app\modules\mpi\models\Establecimiento;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\MovimientoPaciente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movimiento-paciente-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'fecha')->widget(DatePicker::className(), [
    'options' => ['placeholder' => 'Ingrese una fecha'],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd', // Verifica que el formato sea compatible con tu modelo
    ],
]);
?>

    <?php echo $form->field($model, 'unidad_operativa')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'sector_internacion')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'dia')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'existencia')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'ingresos')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'pase')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'paciente_vivo')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'paciente_fallecido')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'pases')->textInput();        // ->textarea()?>

    <?php echo $form->field($model, 'ingresos_egresos')->textInput();        // ->textarea()?>

    <?php 
		$data=ArrayHelper::map(Establecimiento::find()->asArray()->all(), 'id', 'nombre');
    	echo $form->field($model, 'id_establecimiento')->dropDownList($data,['prompt'=>'Seleccione una opción']);

        /*
        echo $form->field($model, 'id_establecimiento')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => ($model->id_establecimiento)?$model->consultasestadisticas.establecimiento->nombre:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['consultasestadisticas.establecimiento/list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_establecimiento) { return id_establecimiento.text; }'),
                'templateSelection' => new JsExpression('function(id_establecimiento) { return id_establecimiento.text; }'),
            ],
        ]);
        */
	?>

    <?php echo $form->field($model, 'cama_disponible')->textInput();        // ->textarea()?>

    <?php ActiveForm::end(); ?>
    
</div>
