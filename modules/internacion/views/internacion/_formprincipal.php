
<?php 
use app\modules\internacion\models\Asistio;
use app\modules\internacion\models\Educacion;
use app\modules\internacion\models\Ocupacion;
use app\modules\internacion\models\TipoSituacionLaboral;
use app\modules\mpi\models\Localidad;
use app\modules\mpi\models\ObraSocial;
use app\modules\mpi\models\Paciente;
use app\modules\mpi\models\Provincia;
use app\modules\mpi\models\Sexo;
use app\modules\mpi\models\TipoDocumento;
use app\modules\mpi\models\TipoPlanSocial;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="paciente-content" style="border: solid black 1px;
    background-color: lightgoldenrodyellow;
    height: auto;
    margin-left: 1%;
        width: auto;
">

    <?php
$form = ActiveForm::begin([
    'id' => 'form-id-paciente',
    'options' => ['class' => 'form-group mb-4 mr-2 me-2', 'style' => 'margin-left: 1%;width: -webkit-fill-available;'],
]); ?>

<script>
//$('#paciente-numero_documento').prop('disabled', true);
var edad=0;

var paciente = JSON.parse(localStorage.getItem('paciente'));

var contacto = JSON.parse(localStorage.getItem('contacto'));
var internacion = JSON.parse(localStorage.getItem('internacion'));
var sistemaEducativo = JSON.parse(localStorage.getItem('sistemaEducativo'));
var situacionLaboral = JSON.parse(localStorage.getItem('situacionLaboral'));


if(paciente!=null){
    
    if(internacion.id_paciente!=null){
       cargaPaciente(true);
       hablitarSistemaEducativo(sistemaEducativo.id_asistio);
      
    }
    else{
    // Asegúrate de que Select2 esté completamente cargado antes de llamar a esta función
$('#internacion-id_paciente').select2('val', null);

  document.getElementById("paciente-id_tipo_documento").value=paciente.id_tipo_documento;
  document.getElementById("contacto-numero_telefono").value=contacto.numero_telefono
  document.getElementById("paciente-numero_documento").value=paciente.numero_documento;
  document.getElementById("paciente-id_tipo_plan_social").value=paciente.id_tipo_plan_social;
  document.getElementById("paciente-id_obra_social").value=paciente.id_obra_social;
  if(paciente.documento_ajeno=="1" || paciente.documento_ajeno==1){
    console.log("entro a documento ajeno");
    document.getElementById("paciente-documento_ajeno").checked = true; // Marca el checkbox

  }

  var obraSocial=document.getElementById("paciente-id_obra_social").parentElement;
  obraSocial.style.display='none';
  if(paciente.id_tipo_plan_social!=5){
    obraSocial.style.display='block';
  }

  document.getElementById("paciente-apellido").value=paciente.apellido;
  document.getElementById("paciente-nombre").value=paciente.nombre;
  document.getElementById("paciente-fecha_nacimiento").value=paciente.fecha_nacimiento;
  // esto para agregar la fecha verFecha(paciente.fecha_nacimiento);
  document.getElementById("paciente-id_sexo").value=paciente.id_sexo;
  document.getElementById("paciente-id_localidad").value=paciente.id_localidad;
  obtenerLugares(paciente.id_localidad);
  getProvincia(paciente.id_localidad);
    }
  //verFecha(paciente.fecha_nacimiento)
    document.getElementById("sistemaeducativo-id_asistio").value=sistemaEducativo.id_asistio;
    console.log(sistemaEducativo.id_asistio);
    hablitarSistemaEducativo(sistemaEducativo.id_asistio);
  document.getElementById("situacionlaboral-id_tipo_situacion").value=situacionLaboral.id_tipo_situacion;
  document.getElementById("situacionlaboral-id_ocupacion").value=situacionLaboral.id_ocupacion

}

function hablitarSistemaEducativo(value,sistemaEducativo=null) {
    const sistemaEducativoElement = document.getElementById("sistemaEducativo");
    
    // Verificar si el sistema educativo debe ser ocultado
    sistemaEducativoElement.style.display = 'none';
    if (value ==2) {
        sistemaEducativoElement.style.display = 'block';
        document.getElementById('sistemaeducativo-primario_completo').required = true;
        document.getElementById('sistemaeducativo-secundario_completo').required = true;
        document.getElementById('sistemaeducativo-superior_completo').required = true;
        document.getElementById('sistemaeducativo-universitario_completo').required = true;
        document.getElementById(
        sistemaEducativo.primario_completo === 'PRIMARIO COMPLETO' 
        ? "sistemaeducativo-primario-completo--0" 
        : "sistemaeducativo-primario-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'SECUNDARIO COMPLETO' 
        ? "sistemaeducativo-secundario-completo--0" 
        : "sistemaeducativo-secundario-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'SUPERIOR COMPLETO' 
        ? "sistemaeducativo-superior-completo--0" 
        : "sistemaeducativo-superior-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'UNIVERSITARIO COMPLETO' 
        ? "sistemaeducativo-universitario-completo--0" 
        : "sistemaeducativo-universitario-completo--1"
        ).checked = true;
    }


}
function habilitarObraSocial(event){
    var obraSocial=document.getElementById("paciente-id_obra_social").parentElement;
    if(event.target.value==5){
        obraSocial.style.display='none';
    }
    else{
        obraSocial.style.display='block';
    }
}
function createformprincipal(){
    event.preventDefault();
    $("#form-id-paciente input, #form-id-paciente select").each(function() {
                    $(this).prop('disabled', false);
                });
    var data = $('#form-id-paciente');
    var documentoAjeno= $('#paciente-documento_ajeno').prop('checked') ? '1' : '0';

    // Serializamos el resto del formulario
    console.log(documentoAjeno);
    var data = $('#form-id-paciente').serializeArray();
    data = data.filter(function(item) {
    return !(item.name === 'Paciente[documento_ajeno]');
    });
    data.push({name: 'Paciente[documento_ajeno]', value: documentoAjeno});
    // Añadimos los valores de los checkboxes al array de datos
    $.ajax({
    url: '<?php echo Url::to(['internacion/formprincipal', 'modelId' => $modelId]) ?>',
    dataType: "json",
    type: "POST",
    data: $.param(data),
    success: function(data) {
       if(data.response=='success'){
        localStorage.removeItem('paciente');
        localStorage.removeItem('sistemaEducativo');
        document.getElementById("error-block-sistema-educativo").style.display='none';
        MENSAJE = "Se realizo correctamente la carga!!";
        $("#mensaje").html(MENSAJE);
               document.getElementById("mensaje").style.color="#155724"
               $("#title").html(data.content);
        $("#modalMensaje").modal('show');
        if(data.internacion!=null){
            localStorage.setItem('internacion',JSON.stringify(data.internacion))
        }
        localStorage.setItem('paciente',JSON.stringify(data.paciente))
        localStorage.setItem('contacto',JSON.stringify(data.contacto))
        localStorage.setItem('sistemaEducativo',JSON.stringify(data.sistemaEducativo))
        localStorage.setItem('situacionLaboral',JSON.stringify(data.situacionLaboral))

            }
       else{
        document.getElementById("mensaje").style.color = "red";
    $("#title").html(data.title);
    $("#mensaje").html(data.content);
    if (data.errors[2]!='' || data.errors[2]!=null) {
        document.getElementById("error-block-sistema-educativo").style.display = 'block'; // Correcto

        document.getElementById("error-block-sistema-educativo").style.color="red";
    }
 

    $("#modalMensaje").modal('show');
       }

    },
    error: function(error) {
        console.log($("#modalMensaje"));
        $("#modalMensaje .modal-header").html(`<p class="text-warning">Error de datos!</p>`);
        MENSAJE = 'Error:'+error.responseJSON.message;
        document.getElementById("mensaje").style.color="red"
        $("#mensaje").html(MENSAJE);
        $("#modalMensaje").modal('show');
        
    }
     });
    

}


$(document).ready(function() {
    function actualizarCampoDocumento() {
        var tipoDocumento = $('#paciente-id_tipo_documento').val(); // Obtiene el valor del tipo de documento
        var documentoAjeno = $('#paciente-documento_ajeno').is(':checked'); // Verifica si el checkbox está marcado
        // Limpia el campo numero_documento y remueve el patrón anterior
        $('#paciente-numero_documento').val(''); // Limpia el campo
        $('#paciente-numero_documento').removeAttr('pattern'); // Elimina el patrón anterior
        $('#paciente-numero_documento').prop('required', false); // Lo hace opcional al inicio

        // Verifica el tipo de documento y el estado del checkbox
        if (tipoDocumento == 1) {
            // Si el tipo de documento es DNI
            if (documentoAjeno) {
                $('#paciente-numero_documento').attr('pattern', '^[a-zA-Z]{1}[0-9]{9}$'); // Establece el nuevo patrón (1 letra + 9 números)
        $('#paciente-numero_documento').prop('required', true); // Lo hace requerido

        $('#paciente-numero_documento').off('input').on('input', function() {
            var value = $(this).val() || '';  // Asegura que el valor sea una cadena vacía si es undefined

            // Permite solo la primera letra y los siguientes caracteres deben ser números
            if (value.length === 1) {
                value = value.replace(/[^a-zA-Z]/g, '');  // Solo permite letras en el primer carácter
            } else if (value.length > 1) {
                // Mantiene la primera letra y solo números en los siguientes caracteres
                value = value[0].replace(/[^a-zA-Z]/g, '') + value.slice(1).replace(/[^\d]/g, '');
            }

            // Limita la longitud total a 10 caracteres (1 letra + 9 números)
            if (value.length > 10) {
                value = value.slice(0, 10);
            }

            $(this).val(value); // Actualiza el valor del campo
        });
            } else {
                // Si no es documento ajeno, usa el patrón estándar para DNI
                $('#paciente-numero_documento').attr('pattern', '^[0-9]{1,10}$');
                $('#paciente-numero_documento').prop('required', true); // Lo hace requerido

                $('#paciente-numero_documento').off('input').on('input', function() {
                    var value = $(this).val();
                    // Reemplaza cualquier carácter no numérico
                    value = value.replace(/[^\d]/g, '');
                    // Limita la longitud a 8 dígitos
                    if (value.length > 10) {
                        value = value.slice(0, 10);
                    }
                    $(this).val(value); // Actualiza el valor del campo
                });
            }
        } else if (tipoDocumento == 2 || tipoDocumento == 3) {
            // Si es tipo 2 o 3 (otros documentos), permite letras y números sin restricciones
            $('#paciente-numero_documento').off('input').removeAttr('pattern').val('').prop('required', false);
        } else if (tipoDocumento == 4) {
            // Si es tipo 4 (Pasaporte), permite exactamente 9 caracteres alfanuméricos
            console.log("Tipo de documento 4 seleccionado");

            $('#paciente-numero_documento').off('input').val('').prop('required', true).attr('pattern', '^[a-zA-Z0-9]{9}$');

            $('#paciente-numero_documento').on('input', function() {
                var value = $(this).val();
                // Reemplaza cualquier carácter que no sea alfanumérico
                value = value.replace(/[^a-zA-Z0-9]/g, '');
                // Limita la longitud a 9 caracteres
                if (value.length > 9) {
                    value = value.slice(0, 9);
                }
                $(this).val(value); // Actualiza el valor del campo
            });
        } else {
            // Si el tipo de documento es null o vacío, deshabilita el campo
            $('#paciente-numero_documento').off('input').val('').prop('required', false);
        }
    }
    // Manejar el cambio en el tipo de documento
    $('#paciente-id_tipo_documento').change(function() {
        console.log('Cambio en el tipo de documento');
        actualizarCampoDocumento(); // Llama a la función para actualizar el campo
    });

    // Manejar el cambio en el checkbox
    $('#paciente-documento_ajeno').change(function() {
    console.log('Cambio en el checkbox');
    actualizarCampoDocumento();
    });


});
function buscarPersona(event){
    var tipoDocumento=document.getElementById("paciente-id_tipo_documento").value;
    if(tipoDocumento==1){
        $.ajax({
            url: '<?php echo Url::to(['buscarnumerodocumento']) ?>',
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            type:"POST",
            data: event,
            success: function(data) {
                //const datos=JSON.stringify(data);
                if(data.status==="ok"){
                document.getElementById("paciente-nombre").value=data["paciente"].nombre;
                document.getElementById("paciente-apellido").value=data["paciente"].apellido
                document.getElementById("paciente-id_sexo").value=data["paciente"].id_sexo;
                document.getElementById("paciente-fecha_nacimiento").value=data["paciente"].fecha_nacimiento;
                document.getElementById("sistemaeducativo-id_asistio").value=data["paciente"].id_sistema;
              
                if(data["paciente"].id_sistema!=null){
                document.getElementById(
                data["sistemaEducativo"].primario_completo === 'PRIMARIO COMPLETO' 
                ? "sistemaeducativo-primario-completo--0" 
                : "sistemaeducativo-primario-completo--1"
                ).checked = true;
                document.getElementById(
                    data["sistemaEducativo"].secundario_completo === 'SECUNDARIO COMPLETO' 
                ? "sistemaeducativo-secundario-completo--0" 
                : "sistemaeducativo-secundario-completo--1"
                ).checked = true;
                document.getElementById(
                    data["sistemaEducativo"].secundario_completo === 'SUPERIOR COMPLETO' 
                ? "sistemaeducativo-superior-completo--0" 
                : "sistemaeducativo-superior-completo--1"
                ).checked = true;
                document.getElementById(
                    data["sistemaEducativo"].secundario_completo === 'UNIVERSITARIO COMPLETO' 
                ? "sistemaeducativo-universitario-completo--0" 
                : "sistemaeducativo-universitario-completo--1"
                ).checked = true;
                document.getElementById("situacionlaboral-id_tipo_situacion").value=situacionLaboral.id_tipo_situacion;
                document.getElementById("situacionlaboral-id_ocupacion").value=situacionLaboral.id_ocupacion
                }
                }
                else if(data.status==="error"){
                    document.getElementById("paciente-nombre").value=data["paciente"].nombre;
                document.getElementById("paciente-apellido").value=data["paciente"].apellido
                document.getElementById("paciente-id_sexo").value=data["paciente"].id_sexo;
                document.getElementById("paciente-fecha_nacimiento").value=data["paciente"].fecha_nacimiento;
                document.getElementById("alerta").style.display="block"
                document.getElementById("alerta").style.backgroundColor="#FFF3CD";
                document.getElementById("tituloAlerta").innerText = 'No se encontro un paciente';
                document.getElementById("tituloAlerta").style.color="#721c24";
        setTimeout(function() {
                        document.getElementById("alerta").style.display="none"
            }, 5000);
                }
                
            }
            
        });
    }
    
}


function getProvincia(idLocalidad) {
    $.ajax({
        url: '<?php echo Url::to(['getprovincia']) ?>',
        dataType: "json",
        type: "GET",
        data: { param: idLocalidad }, // Asegúrate de que el parámetro tenga el nombre correcto
        success: function(data) {
            console.log(data); // Verifica la respuesta
            if (data && data.idProvincia) { // Asegúrate de que la respuesta tenga el formato esperado
                // Aquí puedes asignar el valor de idProvincia al select de provincia
                $('#localidad-id_provincia').val(data.idProvincia).trigger('change');
            }
        },
        error: function(xhr, status, error) {
            console.error('Error en la petición: ' + error);
        }
    });
}
function obtenerLugares(idLocalidad){

    if (idLocalidad && idLocalidad !== "") {
    $.ajax({
    url: '<?php echo Url::to(['obtenerlugares']) ?>',
    dataType: "json",
    type: "GET",
    data: { param: idLocalidad }, // Asegúrate de que el parámetro tenga el nombre correcto
    success: function(data) {
        console.log(data); // Verifica que recibas los datos correctos del servidor
        if (data.status === "ok") {
            var containerLocalidad = document.getElementById("container-localidad-paciente");
            containerLocalidad.style.display = "inline-grid";
            document.getElementById("departamento-paciente").textContent = data.departamento.nombre;
            document.getElementById("provincia-paciente").textContent = data.provincia.nombre;
        } else if (data.status === "error") {
            // Manejo del error
            document.getElementById("alertaDni").style.display = "block";
            document.getElementById("alertaDni").style.backgroundColor = "#FFF3CD";
            document.getElementById("tituloAlertaDni").innerText = 'No se encontró un equivalente a local';
            document.getElementById("tituloAlertaDni").style.color = "#721c24";
            setTimeout(function() {
                document.getElementById("alertaDni").style.display = "none";
            }, 5000);
        }
    },
    error: function(xhr, status, error) {
        console.error('Error en la petición: ' + error);
    }
});
    }
}

function actualizarLocalidades(provinciaId) {
    if (!provinciaId) return;

    $.ajax({
        url: '<?php echo Url::to(['/mpi/paciente/provinciadelocalidad', 'provinciaId' => '']); ?>' + provinciaId,
    type: 'GET',
    dataType: 'json',
        success: function(data) {
            let select = $('#paciente-id_localidad');
            select.empty().trigger('change'); // Limpia el select
   
            select.append(new Option('Seleccione localidad', '')); // Placeholder
            data.forEach(item => {
                select.append(new Option(item.nombre, item.id));
            });
            select.trigger({
                type: 'select2:selecting',
                params: { data: data } // Evento opcional para manejar los nuevos datos
            });
        }
    });
}

$('#localidad-id_provincia').on('change', function() {
    let provinciaId = $(this).val();

    actualizarLocalidades(provinciaId);
});
function habilitarOcupacion(event){

    var valor= event.target.options[event.target.selectedIndex].text;
    if(valor=="TRABAJA O ESTA DE LICENCIA"){
        document.getElementById("ocupacion").style.display='inline';
    }
    else{
        document.getElementById("ocupacion").style.display='none';
    }
}
function verDni(event){
if(event.length==8 || event.length==7){
    document.getElementById("alertaDni").style.display="block"
            document.getElementById("alertaDni").style.backgroundColor="#d4edda";
            document.getElementById("tituloAlertaDni").innerText = 'longitud correcta';
            document.getElementById("tituloAlertaDni").style.color="#155724";
            setTimeout(function() {
                document.getElementById("alertaDni").style.display="none"
    }, 2000);

}
else{
document.getElementById("alertaDni").style.display="block"
            document.getElementById("alertaDni").style.backgroundColor="#f8d7da";
            document.getElementById("tituloAlertaDni").innerText = 'longitud incorrecta';
           
            document.getElementById("tituloAlertaDni").style.color="#721c24";
            
           
            setTimeout(function() {
                document.getElementById("alertaDni").style.display="none"
    }, 1000);
}
}
/*function existeDocumento(value) {
    console.log(value);
    var internacion=JSON.parse(localStorage.getItem('internacion'));
    var tipoDocumento=document.getElementById("paciente-id_tipo_documento").value;
    var documentoAjeno = document.getElementById("paciente-documento_ajeno");
    console.log(tipoDocumento)
    console.log(documentoAjeno);
    if(tipoDocumento!=null || tipoDocumento==""){
    if(internacion.id_lugar_ingreso!=5 && documentoAjeno==null || documentoAjeno.checked==false ){
        var formNuevo = document.getElementById("paciente-form-nuevo");
        var formPresente = document.getElementById("paciente-form-presente");
    $.ajax({
        url: '<?= Url::to(['/mpi/paciente/existepaciente']) ?>',// Corregido el nombre de la ruta
        dataType: "json",
        type: "GET",
        data: { param: value },
        success: function(data) {
            if(data.status=="ok"){
                cargaPaciente(true);
                formPresente.textContent = "El paciente ya se encuentra registrado en el sistema(dni:"+data.paciente.numero_documento+")";  // Texto del h3
                formPresente.style.color = "#337ab7";              // Establece el color a amarillo
            }
            else{
                console.log("entro aca a form viejo");
                
                formNuevo.textContent = "El paciente no se encuentra registrado";  // Texto del h3
                formNuevo.style.color = "#337ab7"; 
     
            }
        },
        error: function(xhr, status, error) {
            console.error('Error:', error); // Para depurar cualquier error
        }
        });
    }
}
}*/
function vaciarFormulario() {
    // Selecciona todos los campos de entrada dentro del formulario y los vacía
    var form = document.getElementById('form-id-paciente');
    form.reset(); // Restablece todos los campos del formulario a su valor por defecto
}
function cargaPaciente(isCheckBox){
   
   
    document.getElementById("paciente-id_tipo_documento").value = null;
    document.getElementById("contacto-numero_telefono").value = null;
    document.getElementById("paciente-numero_documento").value = null;
    document.getElementById("paciente-id_tipo_plan_social").value = null;
    document.getElementById("paciente-id_obra_social").value = null;
    document.getElementById("paciente-apellido").value = null;
    document.getElementById("paciente-nombre").value = null;
    document.getElementById("paciente-fecha_nacimiento").value = null;
    document.getElementById("paciente-id_sexo").value = null;
    document.getElementById("paciente-id_localidad").value = null;
    document.getElementById("sistemaeducativo-id_asistio").value = null;
    document.getElementById("situacionlaboral-id_tipo_situacion").value = null;
    document.getElementById("situacionlaboral-id_ocupacion").value = null;
    // Desmarcar los radio buttons de "Primario"

    if(document.getElementById("sistemaeducativo-id_asistio").value!=''){
    document.getElementById("sistemaeducativo-primario-completo--0").checked = false;
    document.getElementById("sistemaeducativo-primario-completo--1").checked = false;

    // Desmarcar los radio buttons de "Secundario"
    document.getElementById("sistemaeducativo-secundario-completo--0").checked = false;
    document.getElementById("sistemaeducativo-secundario-completo--1").checked = false;
    

    // Desmarcar los radio buttons de "Superior"
    document.getElementById("sistemaeducativo-superior-completo--0").checked = false;
    document.getElementById("sistemaeducativo-superior-completo--1").checked = false;

    // Desmarcar los radio buttons de "Universitario"
    document.getElementById("sistemaeducativo-universitario-completo--0").checked = false;
    document.getElementById("sistemaeducativo-universitario-completo--1").checked = false;
    }

    localStorage.removeItem('paciente');
        localStorage.removeItem('sistemaEducativo');
        localStorage.removeItem('situacionLaboral');
        const internacionData = localStorage.getItem('internacion');

        // Verificar si hay datos en localStorage
        if (internacionData) {
        // Parsear el JSON a un objeto
        const internacionObj = JSON.parse(internacionData);

        // Eliminar el atributo 'id_paciente'
        delete internacionObj.id_paciente;

        // Guardar el objeto actualizado de nuevo en localStorage
        localStorage.setItem('internacion', JSON.stringify(internacionObj));

        console.log("El atributo 'id_paciente' ha sido eliminado del objeto en localStorage.");
        } else {
        console.log("No hay datos de internacion en localStorage.");
        }

        document.getElementById("paciente-form-nuevo").textContent="";
        document.getElementById("paciente-form-presente").textContent="";
        hablitarSistemaEducativo(1);
    let valorCheckBox=document.getElementById('checkbox');
    if(valorCheckBox.checked || isCheckBox==true){
        document.getElementById("dropDownPaciente").style.display='grid';
        document.getElementById("rellenoPaciente").style.display='none'
        valorCheckBox.checked=true;
        document.getElementById("paciente-form-presente").textContent="";
    }
    else{
        document.getElementById("dropDownPaciente").style.display='none'
        document.getElementById("rellenoPaciente").style.display='grid'
        valorCheckBox.checked=false;
    }

}
function rellenarCamposPaciente(event){
    $.ajax({
    url: '<?php echo Url::to(['buscarpersona']) ?>',
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    type: "GET",
    data: { param: event.target.value },  // Enviar un objeto con clave 'param'
    success: function(data) {
        // Procesar la respuesta
        if(data.sistemaEducativo!=null){
        document.getElementById("sistemaeducativo-id_asistio").value=data.sistemaEducativo.id_asistio;

            hablitarSistemaEducativo(data.sistemaEducativo.id_asistio,data.sistemaEducativo);
        }
     


    }
});
}
</script>



<div style='
    align-content: flex-start;
    width: 100%;
    gap: 3%;'>
<div>
      <input type="checkbox" id="checkbox" name="pacientes" value="pacientes"
      onclick="cargaPaciente()" >
      <label for="pacientes">Cargar paciente</label>
    </div>


<div id="dropDownPaciente" style="display:none;">
<h5 id="paciente-form-presente"></h5>
   <?php 


$listaPaciente=array();

   if(!empty($internacion->id_paciente) ||$internacion->id_paciente!=null ){
    Yii::beginProfile('findPaciente');
    $parametroPaciente = Paciente::findOne($internacion->id_paciente);
    Yii::endProfile('findPaciente');
    $listaPaciente[$parametroPaciente->id]=$parametroPaciente->nombre.','.$parametroPaciente->apellido.','.$parametroPaciente->numero_documento;
   }

   ?> 
<?=$form->field($internacion, 'id_paciente')->widget(Select2::class, [
    'options' => ['placeholder' => 'Select paciente ...'],
    'data' => $listaPaciente,
    'pluginOptions' => [
        'minimumInputLength' => 3,
        'allowClear' => true,
        'language' => [
            'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
        ],
        'ajax' => [
            'url' => Url::to(['/mpi/paciente/list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q: params.term}; }'),
        ],
        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
        'templateResult' => new JsExpression('function(id_paciente) { return id_paciente.text; }'),
        'templateSelection' => new JsExpression('function(id_paciente) { return id_paciente.text; }'),
    ],
    'pluginEvents' => [
        "change" => new \yii\web\JsExpression('function(event) { 
            rellenarCamposPaciente(event); 
        }'),
    ],
]); ?>
 </div>

 <div id="rellenoPaciente" style="display:grid;">
<?php
   $data=ArrayHelper::map(TipoDocumento::find()->asArray()->all(), 'id', 'nombre');
   echo $form->field($model, 'id_tipo_documento',)->dropDownList($data, ['prompt' => 'Seleccione una opción'])->label('Tipo de Documento');



if($activarDniAjeno==true){
echo $form->field($model, 'documento_ajeno')->checkbox([
    'label' => Yii::t('app', 'Documento ajeno'),
    'uncheck' => '0',  // Enviar '0' si no está marcado
    'value' => '1',    // Enviar '1' si está marcado
    'labelOptions' => [
        'style' => 'color: #333; font-size: 18px; font-weight: bolder;',  // Color oscuro y tamaño de fuente más grande
    ]

]); 
}
?>
<?php 
echo $form->field($model, 'numero_documento')->textInput([
    //'onchange' => "existeDocumento(this.value)"// Restrict input to numbers and call verDni function
    ]);?>

    <h5 id="paciente-form-nuevo"></h5>
<div id="alertaDni" style="

    display: none;
    width: 20%;
    background-color: rgb(248, 215, 218);" class="alert alert-danger" role="alert">
    <p id="tituloAlertaDni"style="text-decoration: underline; text-align: center; margin: auto;"></p>
  
    </div>
    <div id="obra_social" style="width: 100%;">
<?php
  $planSocial=ArrayHelper::map(TipoPlanSocial::find()->asArray()->all(), 'id', 'nombre');
  echo $form->field($model, 'id_tipo_plan_social', [
      'labelOptions' => ['style' => 'font-weight: bold; display: block;'],
 ])->dropDownList($planSocial,['prompt'=>'Seleccione una opción','onchange' => 'habilitarObraSocial(event)'])->label("Tipo de plan");

	$obra_social=ArrayHelper::map(ObraSocial::find()->asArray()->all(), 'id', 'nombre');

    echo $form->field($model, 'id_obra_social',['options' => [ 'style' => 'width:inherit; display:none;']])->widget(Select2::classname(), [
        'data' => $obra_social,   
        'options' => [ 'style' => 'display:none; width:inherit;','placeholder' => Yii::t('app', 'Seleccione obra social') . '...'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => 'auto'  // Este ajuste asegura que el Select2 ocupe el 100% del contenedor
        ],
    ]);
?>
</div>
<?php echo $form->field($model, 'apellido')->textInput();        // ->textarea()?>

<?php echo $form->field($model, 'nombre')->textInput();        // ->textarea()?>

<?php echo $form->field($model, 'fecha_nacimiento')->widget(DatePicker::className(), [
    'pluginOptions' => ['autoclose' => true],
    'language' => 'es',
   /* 'pluginEvents' =>[
        "changeDate" => "function(e) {  verFecha(e)}",
    ] ,*/
    // Asegurar que el DatePicker ocupe el 100% del contenedor
])->label('Fecha de Nacimiento');?>
<?php 
    $sexos=ArrayHelper::map(Sexo::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'id_sexo', ['options'=>['style' => 'display: block;'] ])->dropDownList($sexos,['prompt'=>'Seleccionar...','style'=>'width:215px']);

?>



<?php 

$arrayProvincia = ArrayHelper::map(Provincia::find()->asArray()->all(), 'id', 'nombre');

echo $form->field($localidad, 'id_provincia', [
    'options' => ['style' => 'width: 50%;'], 
    'labelOptions' => ['style' => 'font-weight: bold;']
])->widget(Select2::classname(), [
    'data' => $arrayProvincia,
    'options' => [
        'placeholder' => 'Select provincia',
    ],
    'pluginOptions' => [
        'allowClear' => true, // Agrega la opción para limpiar la selección y mostrar el placeholder de nuevo
        'tokenSeparators' => [',', ' '],
        'maximumInputLength' => 10
    ],
    'pluginEvents' => [
        /*"change" => new JsExpression("
            function(e) {
                var provinciaId = e.target.value;
 
                if (provinciaId) {
                    $.ajax({
                        url: '" . Url::to(['/mpi/paciente/provinciadelocalidad']) . "',
                        dataType: 'json',
                        type: 'GET',
                        data: { provinciaId: provinciaId }, // Enviando el id de la provincia
                        success: function(data) {
                            var selectLocalidades = $('#paciente-id_localidad'); // Asegúrate de que este ID sea correcto
                           if(selectLocalidades==null){
                            selectLocalidades.empty(); // Limpiar las opciones previas
                            selectLocalidades.append(new Option('Select localidad', '')); // Añadir la opción de placeholder
                            $.each(data, function(key, value) {
                                selectLocalidades.append(new Option(value.nombre, value.id)); // Llenar el select con las nuevas opciones
                            });
                     
                            }
                                   selectLocalidades.trigger('change'); // Actualizar el select2
                        },
                        error: function(xhr, status, error) {
                            console.error('Error en la petición: ' + error); // En caso de que ocurra un error
                        }
                    });
                }
            }
        ")*/
    ]
])
->label('Provincia');

echo $form->field($model, 'id_localidad', ['options'=>['style'=>'
width: 50%;
'],
      'labelOptions' => ['style' => 'font-weight: bold;']]
)->widget(Select2::classname(), [
    'data' => [],
    'options' => ['placeholder' => 'Select localidad' ],
    'pluginEvents' => [
        // Evento cuando se selecciona una localidad
        "change" => new JsExpression("function(e) {
            console.log('Localidad seleccionada: ' + e.target.value);
            obtenerLugares(e.target.value); // Función que ejecuta alguna acción
        }"),
        // Evento cuando los datos del select cambian (dinámicamente)
        "select2:selecting" => new JsExpression("function(e) {
            console.log('Datos seleccionados:', e.params.args.data);
        }")
    ]
])->label('Localidad');

echo $form->field($contacto, 'numero_telefono')->textInput([
    'maxlength' => true,               
    'pattern' => '[0-9]*', // HTML5 pattern to allow only numbers
    'inputmode' => 'numeric', // Show numeric keyboard on mobile devices
    'oninput' => 'this.value = this.value.replace(/[^0-9]/g, "")', // Restrict input to numbers
    ]);
/*echo $form->field($model, 'id_localidad')->widget(Select2::classname(), [
'data' => $localidad,   
'options' => ['placeholder' => Yii::t('app', 'Seleccione obra social') . '...'],
'pluginOptions' => [
    'allowClear' => true,
],
]);*/
?>

<div style="display: none; width: 60%;
    margin-bottom: 2%;"  id="container-localidad-paciente" >
<label >Departamento:<span type="text" id="departamento-paciente" style="color: grey; font-variation-settings: normal;"   value=""></span><label style="margin-left:4%" >  Provincia: <span  type="text" id="provincia-paciente" style="color: grey; font-variation-settings: normal;" value=""></span></label></label>

</div>
</div>

<?php /*
$data=ArrayHelper::map(ObraSocial::find()->asArray()->all(), 'id', 'nombre');
    
echo $form->field($model, 'id_obra_social')->widget(Select2::classname(), [
    'data' => $data,   
    'options' => ['placeholder' => Yii::t('app', 'Seleccione obra social') . '...'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]); */?>
<div style="flex-direction: column;
 width:50%;">
<?php 
$asistio=ArrayHelper::map(Asistio::find()->asArray()->all(), 'id', 'nombre');
echo $form->field($sistemaEducativo, 'id_asistio', [
    'labelOptions' => ['style' => 'font-weight: bold; display: block;'] ])->dropDownList($asistio,['prompt'=>'Seleccione una opción','onchange' => 'hablitarSistemaEducativo(event.target.value)'])->label("Asistio sistema educativo");

    ?>

<div id="sistemaEducativo" style="display: none;">

    <?= $form->field($sistemaEducativo, 'primario_completo')->radioList(
        [Educacion::PRIMARIO_COMPLETO => Educacion::PRIMARIO_COMPLETO, Educacion::PRIMARIO_INCOMPLETO => Educacion::PRIMARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;'
        ]
    ) ?>

    <?= $form->field($sistemaEducativo, 'secundario_completo')->radioList(
        [Educacion::SECUNDARIO_COMPLETO => Educacion::SECUNDARIO_COMPLETO, Educacion::SECUNDARIO_INCOMPLETO => Educacion::SECUNDARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <?= $form->field($sistemaEducativo, 'superior_completo')->radioList(
        [Educacion::SUPERIOR_COMPLETO => Educacion::SUPERIOR_COMPLETO, Educacion::SUPERIOR_INCOMPLETO => Educacion::SUPERIOR_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <?= $form->field($sistemaEducativo, 'universitario_completo')->radioList(
        [Educacion::UNIVERSITARIO_COMPLETO => Educacion::UNIVERSITARIO_COMPLETO, Educacion::UNIVERSITARIO_INCOMPLETO => Educacion::UNIVERSITARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <p style="display: none;" id="error-block-sistema-educativo">
        Debe seleccionar al menos una educacion.
    </p>
</div>

<?php 

$tipoSituacionLaboral=ArrayHelper::map(TipoSituacionLaboral::find()->asArray()->all(), 'id', 'nombre');
echo $form->field($situacionLaboral, 'id_tipo_situacion', [
    'labelOptions' => ['style' => 'font-weight: bold; display: block;'] ])->dropDownList($tipoSituacionLaboral,['prompt'=>'Seleccione una opción',
    'onchange' => 'habilitarOcupacion(event)'])->label("Situacion laboral")
?>
  <div id="ocupacion" style="display:none;width:100%">
<?php
$ocupacion = ArrayHelper::map(Ocupacion::find()->asArray()->all(), 'id', 'nombre');
?>

<?= $form->field($situacionLaboral, 'id_ocupacion')->widget(Select2::class, [
    'options' => [
        'placeholder' => 'Select ocupacion ...',
        'class' => 'custom-select2' // Aplica solo al <select> original, no al contenedor visible
    ],
    'data' => $ocupacion,
    'pluginOptions' => [
        'allowClear' => true,
        'language' => [
            'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
        ],
        'ajax' => [
            'url' => Url::to(['/internacion/internacion/listocupacion']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q: params.term}; }'),
            'processResults' => new JsExpression('function(data) {
                return {
                    results: data.results
                };
            }'),
        ],
        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
        'templateResult' => new JsExpression('function(data) { return data.text; }'),
        'templateSelection' => new JsExpression('function(data) { return data.text; }'),
        'width' => '300px', // Define el ancho directamente en el widget
    ],
]); ?>




</div>



</div>
</div>
<div class="form-group" style="width: 100%; margin-right: 1%;
    display: grid;">
<?php

echo Html::button('Guardar', ['id'=>'button-formprincipal','class' => 'btn btn-primary','onclick'=>'createformprincipal()']); 
/*echo Html::a('<span>Guardar</span>', ['create','modelId'=>$modelId],
            ['id'=>'button-internacion-create','role'=>'modal-remote','title'=> 'Crear internacion','class'=>'btn btn-primary','style' => 'display: none;']);
*/
?>
</div>

<?php ActiveForm::end() ?>

