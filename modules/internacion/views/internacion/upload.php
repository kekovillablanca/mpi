<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use quidu\ajaxcrud\CrudAsset;
use yii\helpers\Url;

use kartik\file\FileInput;
use kartik\widgets\Growl;

$this->title = 'Subir Archivo Internacion';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>

<script>
/*function subirarchivo() {
    var formData = new FormData($("#form-internacion")[0]);
    $('#archivo_thumbnail').html("<p>Subiendo archivo...</p>");
    console.log(formData);
    $.ajax({
        url: '<?php echo Url::to(["uploadfile"]); ?>',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        cache: false,
        success: function (data) {
            console.log(data);
            if (data.thumbnail) {
                $('#archivo_thumbnail').html(data.thumbnail);
            } else {
                $('#archivo_thumbnail').html("<p>Error al procesar el archivo</p>");
            }
        },
        error: function () {
            $('#archivo_thumbnail').html("<p>Error en la solicitud de subida de archivo</p>");
        }
    });

    return false;
}*/
</script>



<div class="upload-form">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'], // Necesario para cargar archivos
    ]);





foreach (Yii::$app->session->getAllFlashes() as $type => $flash) {
    // Si el flash es un arreglo, pásalo directamente al widget
    if (is_array($flash)) {
        // Usamos Growl como widget y pasamos el arreglo de configuración
        echo Growl::widget([
            'type' => $flash['type'],
            'icon' => $flash['icon'],
            'title' => $flash['title'],
            'body' => $flash['message'],
            'showSeparator' => true,
            'pluginOptions' => [
                'delay' => 1000,
                'placement' => [
                    'from' => 'top',
                    'align' => 'right',
                ],
                'timer' => 1000,
            ],
        ]);
    }
}

    
    
    
    ?>

    <?= $form->field($model, 'file')->widget(FileInput::class, [
    'options' => ['accept' => '.csv'], // Solo permite CSV y Excel
    'pluginOptions' => [
        'showPreview' => false, // No mostrar vista previa
        'showUpload' => false,  // No mostrar botón de carga automática
        'allowedFileExtensions' => ['csv', 'xls', 'xlsx'], // Extensiones permitidas
    ],
]); ?>

    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Subir', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>



?>
