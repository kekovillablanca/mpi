<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<link rel="stylesheet" type="text/css" href="<?= Yii::$app->request->baseUrl ?>/css/detail.css" />

<?php
$stringToolbar="";

$columnsCirugia[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
    'template' => '{viewdetalle} {update} {deletedetalle} ',
    
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
            'update' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'editar registro',]);
                },
            'deletedetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'eliminar registro',]);
                },
        
    ],
    'urlCreator' => function($action, $model, $key, $index) {
            if ($action === 'update') {
                // Solo se pasa 'id' en la URL
                return Url::to(['update', 'id' => $key,'model'=>get_class($model)]);
            }
            if ($action == 'deletedetalle') {
                return Url::to(['deletedetalle','id'=>$model->id,"model"=>get_class($model)]);
            }
            return Url::to([$action, 'id' => $key]);
},
];
$columnsDiagnostico[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
    'template' => '{viewdetalle} {update} {deletedetalle} ',
    
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
            'update' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'editar registro',]);
                },
            'deletedetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'eliminar registro',]);
                },
        
    ],
    'urlCreator' => function($action, $model, $key, $index) {
            if ($action === 'update') {
                // Solo se pasa 'id' en la URL
                return Url::to(['update', 'id' => $key,'model'=>get_class($model)]);
            }

            if ($action == 'deletedetalle') {
                return Url::to(['deletedetalle','id'=>$model->id,"model"=>get_class($model)]);
            }
            return Url::to([$action, 'id' => $key]);
    },
];
$columnsPase[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
    'template' => '{viewdetalle} {update} {deletedetalle} ',
    
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
            'update' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'editar registro',]);
                },
            'deletedetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'eliminar registro',]);
                },
        
    ],
    'urlCreator' => function($action, $model, $key, $index) {
        if ($action === 'update') {
            // Solo se pasa 'id' en la URL
            return Url::to(['update', 'id' => $key,'model'=>get_class($model)]);
        }
        if ($action == 'deletedetalle') {
            return Url::to(['deletedetalle','id'=>$model->id,"model"=>get_class($model)]);
        }
        return Url::to([$action, 'id' => $key]);
},
];
$columnsObstetricia[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
    'template' => '{viewdetalle} {update} {deletedetalle} ',
    
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
            'update' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'editar registro',]);
                },
            'deletedetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'eliminar registro',]);
                },
        
    ],
    'urlCreator' => function($action, $model, $key, $index) {
        if ($action === 'update') {
            // Solo se pasa 'id' en la URL
            return Url::to(['update', 'id' => $key]);
        }
        if ($action == 'deletedetalle') {
            return Url::to(['deletedetalle','id'=>$model->id,"model"=>get_class($model)]);
        }
        return Url::to([$action, 'id' => $key]);
},
];
?>
<div class="detalle-expand">

    <div class="diagnostico-index" style="font-size: 15px;"><b>Diagnósticos</b>
    <div id="ajaxCrudDatatable">
    <?= GridView::widget([
        'id'=>'crud-datatable-diagnostico',
        'pjax'=>true,
        'dataProvider' => $dataProvider,
        'columns' => $columnsDiagnostico,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'layout' => '{items}',
    ]) ?>
        </div>
    <div >
 

    <div class="cirugia-index" style="font-size: 15px; margin-top: 15px;"><b>Cirugías</b>
    <div id="ajaxCrudDatatable">
    <?= GridView::widget([
        'id'=>'crud-datatable-cirugia',
        'dataProvider' => $dataProviderCirugia,
        'columns' => $columnsCirugia,
        'pjax'=>true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'layout' => '{items}',
    ]) ?>
        </div>
        </div>
         

    <div class="pase-index" style="font-size: 15px; margin-top: 15px;"><b>Pases</b>
    <div id="ajaxCrudDatatable">
    <?= GridView::widget([
        'id'=>'crud-datatable-pase',
        'dataProvider' => $dataProviderPase,
        'pjax'=>true,
        'columns' => $columnsPase,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'layout' => '{items}',
    ]) ?>
</div>
</div>
<div class="pase-embarazo" style="font-size: 15px; margin-top: 15px;"><b>Obsetricia</b>
<div id="ajaxCrudDatatable">
    <?= GridView::widget([
        'id'=>'crud-datatable-obstetricia',
        'pjax'=>true,
        'dataProvider' => $dataProviderObstetricia,
        'columns' => $columnsObstetricia,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'layout' => '{items}',
    ]) ?>

</div></div>
</div>

