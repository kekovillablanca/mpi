<?php

use app\modules\internacion\models\CausaExterna;
use app\modules\internacion\models\DiagnosticoObservacion;
use app\modules\internacion\models\TipoEgreso;
use app\modules\mpi\models\Establecimiento;
use kartik\editable\Editable;
use kartik\grid\DataColumn;
use kartik\grid\EditableColumn;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use kartik\widgets\Growl;
use quidu\ajaxcrud\CrudAsset;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\mpi\models\PacienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Internacion';
$this->params['breadcrumbs'][] = $this->title;


foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
    if ($key === 'error') { // Filtrar solo los errores
        echo Growl::widget([
            'type' => $message['type'] ?? Growl::TYPE_DANGER, // Asegúrate de usar Growl::TYPE_DANGER
            'icon' => $message['icon'] ?? 'fa fa-exclamation-circle', // Ícono para errores
            'title' => $message['title'] ?? '¡Error!', // Título predeterminado para errores
            'body' => $message['message'] ?? 'Ocurrió un error inesperado.', // Mensaje predeterminado
            'showSeparator' => $message['showSeparator'] ?? true,
            'pluginOptions' => $message['pluginOptions'] ?? [
                'delay' => 0,
                'placement' => ['from' => 'top', 'align' => 'right'],
                'timer' => 4000,
            ],
        ]);
    }
}
CrudAsset::register($this);
$columns[] = [
    'class' => 'kartik\grid\ActionColumn',
    'contentOptions' => function ($model, $key, $index, $column) {
        return [
            'style' => ['white-space' => 'nowrap'],
            'attribute' => 'inn',
            'format' => 'text',
            'hiddenFromExport' => true,
        ];
    },
    'dropdown' => false,
    'vAlign' => 'middle',
    'urlCreator' => function($action, $model, $key, $index) {

        if ($action === 'update') {
            // Solo se pasa 'id' en la URL
            return Url::to(['internacion/updateinternacion', 'id' => $key]);
        }
        if ($action === 'delete') {
            // Solo se pasa 'id' en la URL
            return Url::to([$action,'id'=>$key,"model"=>get_class($model)]);
        
        }
        if ($action === 'view') {
            // Solo se pasa 'id' en la URL
            return Url::to([$action,'id'=>$key,"model"=>get_class($model)]);
        
        }
        return Url::to([$action, 'id' => $key]);
    },
    'viewOptions' => [
        'role' => 'modal-remote',
        'title' => 'Ver',
        'data-toggle' => 'tooltip'
    ],
    'updateOptions' => [
        'role' => 'modal-remote',
        'title' => 'Editar',
        'data-toggle' => 'tooltip'
    ],
    'deleteOptions' => [
        'role' => 'modal-remote',
        'title' => 'Borrar',
        'data-confirm' => false,
        'data-method' => false, // Override Yii data API
        'data-request-method' => 'post',
        'data-toggle' => 'tooltip',
        'data-confirm-title' => 'Obra Social',
        'data-confirm-message' => '¿ Desea borrar este registro ?'
    ],
    'visibleButtons' => [
        'view' => $permisos['view'],
        'update' => $permisos['update'],
        'delete' => $permisos['delete']
    ]
];





        $columns = array_merge(array(array(
            'class' => '\kartik\grid\ExpandRowColumn',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detailUrl' => Url::to(['indexdetallediagnostico','id_internacion'=>$dataProvider->id]),
            'detailRowCssClass' => 'expanded-collapse',
            'expandIcon' => '<i class="glyphicon glyphicon-expand" style="color:#337ab7"></i>',
            'collapseIcon' => '<i class="glyphicon glyphicon-collapse-down" style="color:#337ab7"></i>',
            'detailAnimationDuration' => 100,
            'allowBatchToggle' => false,
        )), $columns);
   
        

$stringToolbar="";

if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>',['export','accion'=>$_GET['r'],'model'=>"internacion"],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r'],'model'=>"internacion"],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

?>

<div class="internacion-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'toolbar'=> [ ['content'=>$stringToolbar] ],
            'pjax' => true,
            'striped' => true,
            'columns' => $columns,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => $this->title,
            ],
            'options' => ['style' => 'width: fit-content;'],
        ]) ?>
    </div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "", // siempre es necesario para el plugin de jQuery
]) ?>
<?php Modal::end(); ?>