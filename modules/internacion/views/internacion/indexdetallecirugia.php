<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
?>
<link rel="stylesheet" type="text/css" href="<?= Yii::$app->request->baseUrl ?>/css/detail.css" />

<?php
$stringToolbar="";



// agrego acciones a $columns


$columns[]=
[
    'class' => 'kartik\grid\ActionColumn',
    'vAlign'=>'middle',
    'header' => $stringToolbar,
    'template' => '{viewdetalle} {editdetalle} {deletedetalle} ',
    
    'buttons' => [
        'viewdetalle' => function ($url) {
            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url,
                    ['role'=>'modal-remote','title'=> 'Ver datos del registro',]);
            },
            'editdetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'editar registro',]);
                },
            'deletedetalle' => function ($url) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url,
                        ['role'=>'modal-remote','title'=> 'eliminar registro',]);
                },
        
    ],
        'urlCreator' => function ($action, $searchModel) {
            $url=Url::to([$action,'id_historia'=>$searchModel->id]);
            return $url;
    },
]

?>


