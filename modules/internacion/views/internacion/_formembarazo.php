<?php

use app\modules\internacion\models\CondicionAlNacer;
use app\modules\internacion\models\Internacion;
use app\modules\internacion\models\Sector;
use app\modules\internacion\models\Terminacion;
use app\modules\mpi\models\Diagnostico;
use app\modules\mpi\models\PServicio;
use app\modules\mpi\models\Sexo;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

use kartik\form\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\PaseInternacion */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
</script>
<div class="diagnostico-internacion-form">

    <?php 
 
$form = ActiveForm::begin([
    'id' => 'create-obstetricia-form',
    'type' => ActiveForm::TYPE_VERTICAL,
    'options' => ['class' => 'form-group mb-3 mr-2 me-2', 'style' => 'width: 100%; display:grid;'],
    //'formConfig' => ['showErrors' => true],// set style for proper tooltips error display
]);  ?>


<div>
      
    <?php
        $condicionAlNacer=ArrayHelper::map(CondicionAlNacer::find()->asArray()->all(), 'id', 'condicion');
    echo $form->field($model, 'id_condicion_nacimiento', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($condicionAlNacer, ['prompt' => 'Seleccione una opción'])->label('Condicion al nacer');?>


    <?php $terminacion=ArrayHelper::map(Terminacion::find()->asArray()->all(), 'id', 'terminacion');

    echo $form->field($model, 'id_terminacion',['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($terminacion, ['prompt' => 'Seleccione una opción'])->label('Terminacion');?>
        <?php $sexo=ArrayHelper::map(Sexo::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'id_sexo',['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($sexo, ['prompt' => 'Seleccione una opción'])->label('Sexo');?>
        <?php
    echo $form->field($model, 'peso_al_nacer', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->input('number')->label("peso al nacer(gramos)");?>


</div>

    <?php ActiveForm::end(); ?>

</div>
