<?php

use app\modules\internacion\models\LugarIngreso;
use app\modules\internacion\models\Sector;
use app\modules\mpi\models\Establecimiento;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use kartik\tabs\TabsX;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat\Wizard\Date;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\Internacion */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
    
    function activarAlerta(titulo,descripcionTexto,color,colorTitulo){
    document.getElementById("alerta").style.display="block"
    document.getElementById("alerta").style.backgroundColor=color
    document.getElementById("descripccionAlerta").innerText=descripcionTexto
    document.getElementById("tituloAlerta").innerText = titulo
    document.getElementById("tituloAlerta").style.color=colorTitulo;

    setTimeout(function() {
                       document.getElementById("alerta").style.display="none"
           }, 5000);
} 
function createInternacion(){
    event.preventDefault();
 
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);
    const parametro = params.get('modelId');
     var data = $('#your-form-id');
     if(parametro==null){
     $.ajax({
        url: '<?php echo Url::to(['internacion/index']) ?>',
        dataType: 'json',  
        type:"POST",
        cache: false,
        data: data.serialize(),
         success: function(data) {

            if (data.response=="success") {
                
             var modelId = data.modelId;
             
                var urlIndex = '<?= Url::to(['index']) ?>&modelId=' + modelId;
                var urlPaciente = '<?= Url::to(['formprincipal']) ?>&modelId=' + modelId;
                var urlDiagnostico = ' <?= Url::to(['indexdiagnostico']) ?>&modelId=' + modelId;
                var urlPase = '<?= Url::to(['indexpase']) ?>&modelId=' + modelId;
                var urlEmbarazo = '<?= Url::to(['indexembarazo']) ?>&modelId=' + modelId;
                $('#paciente').attr('data-url', urlPaciente);
                $('#diagnostico').attr('data-url', urlDiagnostico);
                $('#pase').attr('data-url', urlPase);
                $('#embarazo').attr('data-url', urlEmbarazo);
              console.log(data.numero_informe);
                document.getElementById("numero-informe").textContent = data.model.numero_informe;
                $("#your-form-id input, #your-form-id select").each(function() {
                    $(this).prop('disabled', true);
                });
                $('.input-group-addon.kv-datetime-picker').remove();
                $('.kv-datetime-remove').css({
                    'pointer-events': 'none',
                    'opacity': '0.5'
                }).attr('title', 'Deshabilitado');
            
           
                var button = document.getElementById('button-internacion');
                button.style.display='none';
                localStorage.clear();
                console.log(data.model);
                
                localStorage.setItem('internacion',JSON.stringify(data.model));
              //activarAlerta('Se creo la internacion','registro creado','#d4edda','#155724');
                history.pushState(null, '', urlIndex);
                $("#mensaje").html(data.content);
                $("#modalMensaje").modal('show');
                $("#title").html(data.title);
                $('#tabs-container').removeClass('disabled');
              //$('#button-internacion-create').attr('disabled',false);
              //document.getElementById("button-internacion-create").style.display="block";
              //document.getElementById("button-internacion").style.display="none";
             // $('#button-internacion').attr('disabled', true);
            
          }
        
          else{
            console.log($("#modalMensaje"));
        $("#modalMensaje .modal-header").html(`<p class="text-warning">Error de datos!</p>`);
        MENSAJE = 'Error:'+error.responseJSON.message;
        document.getElementById("mensaje").style.color="red"
        $("#mensaje").html(MENSAJE);
        $("#modalMensaje").modal('show');
          }
      },
      error: function(error) {
            console.log(error);
          
      }
     });
    }
    else{
        setTimeout(function() {
                    window.location.href = "<?php echo Url::to(['index']) ?>";
                }, 3000);
                document.getElementById("mensaje").style.color="red"
             $("#title").html("Error internacion");
             $("#mensaje").html("<p>Ya se encuentra una carga en proceso, redirigiendo...</p>");
             $("#modalMensaje").modal('show');
    }
     return false;

}

function verFecha(event){
   

    var fechaMesInternacion=event.date.getMonth();
    var fechaDiaInternacion=event.date.getDay();
    var fechaAnioInternacion=event.date.getUTCFullYear();
    var fecha=new Date();
    var fechaMes=fecha.getMonth();
    var fechaDia=fecha.getDay();
    //var fechaDia=fecha.getUTCDate();
    var fechaAnio=fecha.getUTCFullYear();
    //verifica que la fecha del turno asignada sea menor a el año actual
 

    if(fechaAnioInternacion<fechaAnio){
        $('#internacion-fecha_ingreso').val('');
        activarAlerta('Fecha pasada','El año debe ser actual',"#f8d7da","#721c24");
    }
    else{
        $('#button-internacion').prop('disabled', false);
    }
    //verifica si una fecha de turno es pasada la actual no se podra asignar un diagnostico
    if((fechaAnio<fechaAnioInternacion || fechaAnio==fechaAnioInternacion && fechaMesInternacion>fechaMes)
){
        $('#internacion-fecha_ingreso').val('');
        activarAlerta('Fecha futura','Debe ser una fecha actual',"#f8d7da","#721c24");
        $('#button-internacion').prop('disabled', true);

      
    }

}
function verLocalidad(id_establecimiento){

console.log(id_establecimiento);
$.ajax({
    url: '<?php echo Url::to(['obtenerlocalidad']) ?>',
    //contentType: "application/json; charset=utf-8",
    type: 'GET', // o 'POST' si es necesario
    dataType: 'json',
    data: {
        id_establecimiento: id_establecimiento // Envía id_establecimiento como parámetro
    },
    success: function(data) {
        //const datos=JSON.stringify(data);
        if(data.status==="ok"){
            var containerLocalidad=document.getElementById("container-localidad");
            containerLocalidad.style.display="inline-grid"
            document.getElementById("localidad").textContent=data.localidad;
        }
        else if(data.status==="error"){
        /*document.getElementById("paciente-nombre").value="";
        document.getElementById("paciente-apellido").value=""
        document.getElementById("paciente-sexo").value="";
        document.getElementById("paciente-fecha_nacimiento").value=""*/
        document.getElementById("alerta").style.display="block"
        document.getElementById("alerta").style.backgroundColor="#FFF3CD";
        document.getElementById("tituloAlerta").innerText = 'No se encontro un equivalente a paciente';
        document.getElementById("tituloAlerta").style.color="#721c24";
setTimeout(function() {
                document.getElementById("alerta").style.display="none"
    }, 5000);
        }
        
    }
    
});
}


function changeUrl(element){
    var newUrl =element.getAttribute('data-url');
    if(newUrl!=undefined){
   
        

    const params = new URLSearchParams(newUrl.split('?')[1]);
    const modelId = params.get('modelId');
    let pacienteId=JSON.parse(localStorage.getItem('internacion')).id_paciente;
    if(pacienteId==undefined){
        pacienteId="";
    }
    console.log('Model ID:', pacienteId);
    if(element.id=='paciente'){

        newUrl='<?= Url::to(['formprincipal']) ?>&modelId=' + modelId+"&pacienteId="+pacienteId;
    }
    if(element.id=='pase'){

     newUrl='<?= Url::to(['indexpase']) ?>&modelId=' + modelId;
    }
    if(element.id=='diagnostico'){

     newUrl='<?= Url::to(['indexdiagnostico']) ?>&modelId=' + modelId;
    }
    if(element.id=='embarazo'){

        newUrl='<?= Url::to(['indexembarazo']) ?>&modelId=' + modelId;
    }
    var urlObj = new URL(newUrl, window.location.origin);
    urlObj.searchParams.set('_', new Date().getTime());
    newUrl = urlObj.toString();

    var container='#crud-datatable-'+element.getAttribute('id')+'-pjax';
    if (newUrl) {
        history.pushState(null, '', newUrl);
    }
    
    $('#paciente').attr('data-url', newUrl);
    $('#diagnostico').attr('data-url', newUrl);
    $('#pase').attr('data-url', newUrl);
    $('#embarazo').attr('data-url', newUrl);
    
    $.ajax({
        url: newUrl,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        cache: false,
        success: function(data) {
            if(data.response=='error'){

            $("#title").html("Error:"+ data.title);
            $("#mensaje").html("<p>" + (data.content || "Ocurrió un error inesperado.") + "</p>");
            
            // Muestra errores específicos
            
            $("#mensaje").css("color", "red");
            $("#modalMensaje").modal('show');
            }
        },
        error: function(data) {
            console.log(data);
            
                $('#ajaxCrudModal .modal-header').html(`<p class="text-dark">${data.responseJSON.name || "Error"}</p>`);
            $('#ajaxCrudModal .modal-body').html(`<div class="text-danger">${data.responseJSON.message || "An error occurred while processing your request."}</div>`);
            $('#ajaxCrudModal').modal('show');
 
        }
        });
    }
    else{
    $('#ajaxCrudModal .modal-header').html(`<p class="text-dark">Quedan campos vacios.</p>`);
    $('#ajaxCrudModal .modal-body').html(`<div class="text-danger"><p>Por favor, complete todos los campos correctamente de la internacion principal y seleccione en guardar.</p></div>`);
    $('#ajaxCrudModal .modal-footer').html(`
    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
`);
    $('#ajaxCrudModal').modal('show');
    }
    //$.pjax.reload({container:container, timeout:9000, async:true, url: newUrl, replace: true});
}

function generarInternacion(){
    var internacion=  JSON.parse(localStorage.getItem('internacion'));
    var situacionLaboral=JSON.parse(localStorage.getItem("situacionLaboral"));
    var sistemaEducativo=JSON.parse(localStorage.getItem("sistemaEducativo"));
    var contacto=JSON.parse(localStorage.getItem("contacto"));
    var embarazo=JSON.parse(localStorage.getItem("embarazo"));
    var causaExterna=JSON.parse(localStorage.getItem("causaExterna"));
    var paciente=JSON.parse(localStorage.getItem("paciente"));
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);
    const parametro = params.get('modelId');
    var postData = {
        Internacion: internacion,
        SituacionLaboral: situacionLaboral,
        CausaExterna: causaExterna,
        SistemaEducativo: sistemaEducativo,
        Embarazo: embarazo,
        Paciente: paciente,
        Contacto: contacto,
        isCreate:false,
    };
    $.ajax({
    url: '<?= Url::to(['internacion/create']) ?>&modelId=' + parametro,
    dataType: "json",
    contentType: "application/json",
    type:"POST",
    data: JSON.stringify(postData),
    success: function(data) {
   
        $('#ajaxCrudModal .modal-title').html("<h3 class='text-success'>"+data.title+"</h3>");
        $('#ajaxCrudModal .modal-body').html(data.content);
        $('#ajaxCrudModal .modal-footer').html(data.footer);
        // Muestra el modal
        $('#ajaxCrudModal').modal('show');
    

    },
    error: function(xhr, status, error) {
    // Analiza la respuesta JSON
    var response = xhr.responseJSON || {};
    
    // Muestra detalles del error
    $("#title").html("Error");
    $("#mensaje").html("<p>" + (response.error || "Ocurrió un error inesperado.") + "</p>");
    
    // Muestra errores específicos
    
    $("#mensaje").css("color", "red");
    $("#modalMensaje").modal('show');
}
     });
}
</script>


<?php 

if ($permisos['index']){
$form = ActiveForm::begin([
    'id' => 'your-form-id',
    //'enableAjaxValidation' => true,
    //'action' => Url::to(['internacion/index']),
   // 'options' => ['data-pjax' => true,'enctype' => 'multipart/form-data'],
    'type' => ActiveForm::TYPE_INLINE, // Usar el tipo inline proporcionado por kartik
    'fieldConfig' => [
        'options' => ['class' => 'form-group mb-3 mr-2 me-2'], // Espaciado de los grupos de campos
        'deviceSize' => ActiveForm::SIZE_X_SMALL,
  
    ],
]);
$items = [
    [
        'label'=>'<i class="fas fa-home"></i> Paciente',
        'active'=>true,
        'linkOptions'=>['id' => 'paciente'
        ,'onclick' => 'changeUrl(this);return false;'],
        'encode' => false,
    ],
    [
        'label'=>'<i class="fas fa-user"></i> Pase',
        'linkOptions'=>['id' => 'pase', 'onclick' => 'changeUrl(this);return false;'],
        'encode' => false,
    ],
    [
        'label'=>'<i class="fas fa-list-alt"></i> Diagnostico',
        'linkOptions'=>['id' => 'diagnostico', 'onclick' => 'changeUrl(this);return false;'],
        'encode' => false,
    ],
    [
        'label'=>'<i class="fas fa-list-alt"></i> Embarazo',
        'linkOptions'=>['id' => 'embarazo','onclick' => 'changeUrl(this);return false;'],
        'encode' => false,
    ],

];


 ?>
 <div style=" display: block;
    border-bottom-style: inset;
    border-bottom-color: #2e6da4;
    width: auto;
    width: max-content;
    margin: auto;">
    <h1>Internacion #<span id="numero-informe"></span></h1>
 </div>
<div class="internacion-form" style="display: flex;flex-direction: column;flex-direction:column;
gap: 4px 8px;
row-gap: 4px;
column-gap: 8px;">
  
<?php echo "<h1>Año ". date("Y") ."</h1>" ;?>


  

<label>Establecimiento</label>
    <?php
$parametroEstablecimiento = Establecimiento::find()
->joinWith(['localidad'])
->where(['like', 'establecimiento.nombre', "%HOSPITAL%", false]) // Asegúrate de que la búsqueda sea correcta
->all();
$listaEstablecimiento= ArrayHelper::map( $parametroEstablecimiento , 'id', 'nombre');?>
<?= $form->field($model, 'id_establecimiento', [
    'options' => ['style' => 'width: 50%; background-color: #f8f9fa;'], 
    'inputOptions' => ['style' => 'width: 100%; background-color: #f8f9fa;'],
    'labelOptions' => ['style' => 'font-weight: bold;']
])->widget(Select2::classname(), [
    'options' => [
        'placeholder' => 'Select establecimiento ...',
        'style' => 'width: 100%; background-color: #f8f9fa;', // Estilos CSS personalizados
    ],
    'data' => $listaEstablecimiento, // Lista de datos predefinidos si es estática
    'pluginOptions' => [
        'containerCssClass' => 'select2-container-custom', // Clase CSS personalizada para el contenedor
        'dropdownCssClass' => 'select2-dropdown-custom', // Clase CSS personalizada para el desplegable
        'minimumInputLength' => 3,
        'allowClear' => true,
        'language' => [
            'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
        ],
        'ajax' => [
            'url' => \yii\helpers\Url::to(['/mpi/establecimiento/listhospitales']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { 
                return {q: params.term}; 
            }'),
            'processResults' => new JsExpression('function(data) { 
                return { results: data.results }; 
            }')
        ],
        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
        'templateResult' => new JsExpression('function(data) { 
            return data.text; 
        }'),
        'templateSelection' => new JsExpression('function(data) { 
            return data.text; 
        }'),
    ],
    'pluginEvents' => [
        "change" => new JsExpression("function(e) {
            verLocalidad(e.target.value);
        }")
    ]
    ])?>
<?php
/*
if (!empty($model->id_establecimiento) && !empty($model->id)) {
    // Usa json_encode para asegurarte de que el valor esté bien escapado para JavaScript
    $idEstablecimiento = $model->id_establecimiento;
   
    $this->registerJs("
        $(document).ready(function() {
          $('#inputs input, #inputs select, #inputs textarea').each(function() {
                    $(this).prop('disabled', true);
                });
            var idEstablecimiento = " . json_encode($idEstablecimiento) . ";
            if (idEstablecimiento) {
                verLocalidad(idEstablecimiento);
            }
        });
    ");
}*/
?>

<div style="display: none;" id="container-localidad">
<h3>Localidad:<span style="margin: 2;font-size:large" type="text" id="localidad" value=""></span></h3>

</div>


    <div style="margin-top: 1%;margin-bottom: 1%;">
    <label>Fecha de ingreso</label>
    <?php echo $form->field($model, 'fecha_ingreso')->widget(DateTimePicker::classname(), [
    'options' => [
        'placeholder' => 'Ingrese la fecha y hora de ingreso',
        'required' => true,
    ],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd/mm/yyyy hh:ii:ss',
        'todayHighlight' => true,
    ],
    'pluginEvents' => [
        "changeDate" => "function(e) { verFecha(e); }",
    ],
]); ?>


<label style="margin-left: 1%;" >Servicio</label>
<?php 
$servicio =  ArrayHelper::map(Sector::find()->asArray()->all(), 'id', 'descripcion');
echo $form->field($model, 'id_sector', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($servicio,['prompt'=>'Seleccione una opción']);?>


</div>
    <label>Lugar de ingreso</label>
<?php 
$lugarIngreso =  ArrayHelper::map(LugarIngreso::find()->asArray()->all(), 'id', 'nombre');
echo $form->field($model, 'id_lugar_ingreso', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($lugarIngreso,['prompt'=>'Seleccione una opción']);?>


</div>
<div id="alerta" style=" z-index: 2;
    width: 71%;
    height: 12%;
    display: none;" class="alert alert-danger" role="alert">
    <h1 id="tituloAlerta"style="text-decoration: underline; text-align: center;"></h1>
    <h4 id="descripccionAlerta" style=" margin-right: 5%;  font-family: sans-serif;  text-align: center; text-indent:2%; color: black;"> </h4>
 
</div>
<div style="margin-top: 1%;">

<div class="form-group" style="float:right">
<?php

echo Html::button('Guardar', ['id' => 'button-internacion','class' => 'btn btn-primary','type'=>'button' ,'onclick'=>'createInternacion()']); 
echo Html::button('Finalizar internacion', ['id'=>'button-internacion','class' => 'btn btn-primary','type'=>'button' ,'onclick'=>'generarInternacion()']); 
?>
</div>
<?php
echo '<div id="tabs-container">';
echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
echo '</div>';
 


ActiveForm::end(); 


}
?></div>
    <?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>

<?php Modal::end(); ?>
<div class="modal" tabindex="-1" role="dialog" id="modalMensaje">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"  class="modal-title" id="title">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="mensaje">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">ACEPTAR</button>
      </div>
    </div>
  </div>
</div>
