<?php

use app\modules\internacion\models\TipoEgreso;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\grid\GridView;



/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\Internacion */
/* @var $form yii\widgets\ActiveForm */

use quidu\ajaxcrud\CrudAsset;

use yii\bootstrap\Html;

use yii\helpers\Url;
use yii\widgets\Pjax;

// Above

?>
<script>



function obtenerTotalDiasEstadia(fechaFin) {
    var internacion = JSON.parse(localStorage.getItem('internacion'));
    var fechaInicioStr = internacion.fecha_ingreso; // formato "DD/MM/YYYY HH:MM:SS"

    // Convertir fechaInicio a Date (corrigiendo el formato)
    var partesInicio = fechaInicioStr.split(' '); // Separar fecha y hora
    var datePartsInicio = partesInicio[0].split('/'); // ["DD", "MM", "YYYY"]
    var timePartsInicio = partesInicio[1]; // "HH:MM:SS"

    // Formatear la fecha a "YYYY-MM-DD HH:MM:SS" (usando espacio en lugar de "T")
    var fechaInicioISO = `${datePartsInicio[2]}-${datePartsInicio[1]}-${datePartsInicio[0]} ${timePartsInicio}`;
    
    var fechaInicioDate = new Date(fechaInicioISO);

    // Asegurarse de que fechaFin sea un objeto Date
    var fechaFinDate = fechaFin instanceof Date ? fechaFin : new Date(fechaFin);
    
    console.log(fechaInicioDate);

    // Verificar si las fechas son válidas
    if (isNaN(fechaFinDate.getTime())) {
        console.error("fecha fin invalida.");
        return NaN;
    }
    if (isNaN(fechaInicioDate.getTime())) {
        console.error("fecha inicio invalida.");
        return NaN;
    }

    // Obtener la diferencia en milisegundos entre ambas fechas
    var diff = fechaFinDate - fechaInicioDate;
    
    // Convertir la diferencia de tiempo en días calendario
    var diffInDays = diff / (1000 * 60 * 60 * 24); // Convertimos milisegundos a días
    var totalDias = Math.floor(diffInDays); // Redondear hacia abajo
    console.log(totalDias);
    if(totalDias==0){
        totalDias++;
    }
    return totalDias;
}




function verFecha(event) {
    var fechaEgreso = event.date;
    var diasEstadia = obtenerTotalDiasEstadia(fechaEgreso);
    console.log(diasEstadia);
    var formGroup = $('#internacion-fecha_egreso').closest('.form-group');
    var errorContainer = formGroup.find('.help-block.text-danger');

    if (diasEstadia < 0) {
        formGroup.removeClass('has-success').addClass('has-error');
        errorContainer.text('La fecha de egreso debe ser igual o mayor que la fecha de ingreso.').show();
    } else {
        console.log('Fecha de egreso válida');
        formGroup.removeClass('has-error').addClass('has-success');
        if (errorContainer.length && errorContainer.is(':visible')) {
            errorContainer.text('').hide();
        }
        document.getElementById("internacion-total_dias_estadia").value = diasEstadia;
    }
}



//$('#internacion-total_dias_estadia').prop('disabled', true);
var internacion = JSON.parse(localStorage.getItem('internacion'));
if(internacion.id_tipo_egreso!=null && internacion.total_dias_estadia!=null && internacion.fecha_egreso!=null){
    document.getElementById("internacion-id_tipo_egreso").value=internacion.id_tipo_egreso;
    document.getElementById("internacion-total_dias_estadia").value=internacion.total_dias_estadia;
    document.getElementById("internacion-fecha_egreso").value=internacion.fecha_egreso;
    /*
    para desahabilitar
    $("#form-id-pase input, #form-id-pase select").each(function() {
        $(this).prop('disabled', true);
    });
    $('.kv-date-remove').css('pointer-events', 'none').css('opacity', '0.5').attr('title', 'Deshabilitado');
    $('.kv-datetime-picker').css('pointer-events', 'none').css('opacity', '0.5').attr('title', 'Deshabilitado');*/
}

    function createPase(){
   /* $("#form-id-pase input, #form-id-pase select").each(function() {
                $(this).prop('disabled', false);
            });*/

    var data = $('#form-id-pase');
    $('#form-id-pase').append(
    $('<input>', {
        type: 'hidden',
        name: 'Internacion[total_dias_estadia]',
        value: document.getElementById("internacion-total_dias_estadia").value
    })
);

    $.ajax({
        url: '<?php echo Url::to(['internacion/indexpase', 'modelId' => $modelId]) ?>',
    dataType: "json",
    type:"POST",
    data: data.serialize(),
    success: function(data) {
       if(data.response=='success'){
        MENSAJE = "Se realizo correctamente la carga!!";
        localStorage.setItem('internacion',JSON.stringify(data.internacion))
        console.log(data.internacion);
        $("#mensaje").html(MENSAJE);
        document.getElementById("mensaje").style.color="green"
        $("#title").html(data.content);
               document.getElementById("mensaje").style.color="#155724"
        $("#modalMensaje").modal('show');
      /*  $("#form-id-pase input, #form-id-pase select").each(function() {
                    $(this).prop('disabled', true);
                });*/
        /*$('.kv-date-remove')
    .css('pointer-events', 'none')
    .css('opacity', '0.5')
    .attr('title', 'Deshabilitado');
$('.kv-datetime-picker')
    .css('pointer-events', 'none')
    .css('opacity', '0.5')
    .attr('title', 'Deshabilitado');

$('.input-group-addon.kv-date-picker').remove();*/

       }
       else{
        $("#title").html(data.title);
          document.getElementById("mensaje").style.color="red"
        $("#mensaje").html(data.content);
        $("#modalMensaje").modal('show');
       }

    },
    error: function(error) {
        console.log($("#modalMensaje"));
        $("#modalMensaje .modal-header").html(`<p class="text-warning">Error de datos!</p>`);
        MENSAJE = 'Error:'+error.responseJSON.message;
        document.getElementById("mensaje").style.color="red"
        $("#mensaje").html(MENSAJE);
        $("#modalMensaje").modal('show');
    }
     });
    }
</script>
<div id="pase-content" class="pase-form">
<?php

    $this->title = 'pase internacion(Opcional)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => function ($model, $key, $index, $column) {
            return [
                'style' => ['white-space' => 'nowrap'],
                'attribute' => 'inn',
                'format' => 'text',
                'hiddenFromExport' => true,
            ];
        },
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key,"model"=>get_class($model)]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Pase',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]          
    ];

$stringToolbar="";


if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['createpase','accion'=>$_GET['r'] ,'modelId' => $modelId],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['','modelId'=>$modelId],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r'],'model'=>"paseinternacion"],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){

    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r'],'model'=>"paseinternacion"],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}
?>
    <div class="pase-index"> <!--  style='width:100%;margin-right: auto;margin-left: auto;' -->
    <div id="ajaxCrudDatatable">
    <?= GridView::widget([
    'id' => 'crud-datatable-pase',
    'dataProvider' => $dataProvider,
    'filterModel' => null,//filtrado no funciona
    'pjax' => true, // Desactiva Pjax temporalmente
    'columns' => $columns,
    'toolbar' => [['content' => $stringToolbar],],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'panel' => [
        'type' => 'primary',
        'heading' => $this->title,
    ],
]); ?>
    </div>
</div>
<div  style="width: 100%;
    display: flex;
    gap: 2%;">
<?php  
$form = ActiveForm::begin(['id'=>'form-id-pase','options' => [ 'style' => 'display:contents;']]); 
        $data=ArrayHelper::map(TipoEgreso::find()->asArray()->all(), 'id', 'nombre');
        echo $form->field($internacion, 'id_tipo_egreso', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($data,['prompt'=>'Seleccione una opción']);
        /*
        echo $form->field($model, 'id_internacion')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => ($model->id_internacion)?$model->internacion.internacion->nombre:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['internacion.internacion/list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_internacion) { return id_internacion.text; }'),
                'templateSelection' => new JsExpression('function(id_internacion) { return id_internacion.text; }'),
            ],
        ]);
        */
    ?>

    <?php
    echo $form->field($internacion, 'fecha_egreso', [
        'errorOptions' => ['tag' => 'div', 'class' => 'help-block text-danger'],
        'labelOptions' => ['style' => 'font-weight: bold; display: block;'],
        'options' => ['class' => 'form-group field-internacion-fecha_egreso'] 
    ])->widget(DateTimePicker::classname(), [
        'options' => [
            'placeholder' => 'Ingrese la fecha y hora de egreso',
            'required' => true,
            'id' => 'internacion-fecha_egreso',
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yyyy hh:ii:ss',
            'todayHighlight' => true,
        ]
    ]);
    ?>
       <div id="errormsj" class="help-block text-danger"></div>
<?php echo $form->field($internacion,'total_dias_estadia', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->input('number') ?>
</div>
<div class="form-group" style="float:right">
<?php
echo Html::button('Guardar', ['id'=>'button-pase','class' => 'btn btn-primary','type'=>'button' ,'onclick'=>'createPase()']); 
/*echo Html::a('<span>Guardar</span>', ['create','modelId'=>$modelId],
            ['id'=>'button-internacion-create','role'=>'modal-remote','title'=> 'Crear internacion','class'=>'btn btn-primary','style' => 'display: none;']);
*/
?>
</div>
</div>
    <?php ActiveForm::end();?>
</div>