<?php

use app\modules\internacion\models\Asistio;
use app\modules\internacion\models\Educacion;
use app\modules\internacion\models\Lugar;
use app\modules\internacion\models\LugarIngreso;
use app\modules\internacion\models\ProducidoPor;
use app\modules\internacion\models\Sector;
use app\modules\internacion\models\TipoEgreso;
use app\modules\internacion\models\TipoParto;
use app\modules\mpi\models\Edad;
use app\modules\mpi\models\Establecimiento;
use app\modules\mpi\models\EstadoTurno;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use kartik\select2\Select2Asset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\helpers\Url;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\Alert;

use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\mpi\models\ObraSocial */
/* @var $form yii\widgets\ActiveForm */
?>

<script >
$.fn.modal.Constructor.prototype.enforceFocus = function() {};


$(document).ready(function() {
    //obtneer el id por php
    var internacionId = <?= json_encode($model->id) ?>;
    var sistemaEducativo=document.getElementById("sistemaeducativo-id_asistio").value;

    hablitarSistemaEducativo(sistemaEducativo);


});


function habilitarEmbarazo(value) {
    const embarazo = document.getElementById("embarazo-container");
    const lugarIngreso = document.getElementById("internacion-id_lugar_ingreso");

    if (!embarazo || !lugarIngreso) return;

    // Oculta inicialmente el contenedor
    embarazo.style.display = "none";

    if (lugarIngreso.value == 5) {
        $.ajax({
            url: '<?= Url::to(['/mpi/diagnostico/isembarazo']) ?>&id_diagnostico=' + value,
            dataType: "json",
            type: "GET",
            success: function (data) {
                embarazo.style.display = data.result ? "block" : "none";
            },
            error: function (xhr, status, error) {
                console.error('Error en la petición:', error);
                console.log('Detalles:', xhr.responseText);
            }
        });
    }
}

function hablitarSistemaEducativo(value) {
    const sistemaEducativoElement = document.getElementById("sistemaEducativo");
    
    
    sistemaEducativoElement.style.display = 'none';
    if (value ==2) {
        sistemaEducativoElement.style.display = 'block';
        document.getElementById('sistemaeducativo-primario_completo').required = true;
        document.getElementById('sistemaeducativo-secundario_completo').required = true;
        document.getElementById('sistemaeducativo-superior_completo').required = true;
        document.getElementById('sistemaeducativo-universitario_completo').required = true;
        document.getElementById(
        sistemaEducativo.primario_completo === 'PRIMARIO COMPLETO' 
        ? "sistemaeducativo-primario-completo--0" 
        : "sistemaeducativo-primario-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'SECUNDARIO COMPLETO' 
        ? "sistemaeducativo-secundario-completo--0" 
        : "sistemaeducativo-secundario-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'SUPERIOR COMPLETO' 
        ? "sistemaeducativo-superior-completo--0" 
        : "sistemaeducativo-superior-completo--1"
        ).checked = true;
        document.getElementById(
        sistemaEducativo.secundario_completo === 'UNIVERSITARIO COMPLETO' 
        ? "sistemaeducativo-universitario-completo--0" 
        : "sistemaeducativo-universitario-completo--1"
        ).checked = true;
    }


}





</script>


    <?php $form = ActiveForm::begin(['id' => 'form-internacion',
    'enableAjaxValidation' => true]);?>

<div class="internacion-form">

<?php 

     echo $form->field($model, 'id_establecimiento')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Select a establecimiento ...'],
        'data' => $listaEstablecimiento,
        'pluginOptions' => [
        'allowClear' => true,
        'language' => [
            'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
        ],
        'ajax' => [
            'url' => \yii\helpers\Url::to(['/mpi/establecimiento/list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(id_establecimiento) { 
                        
                      
                        return id_establecimiento.text; }'),
                    'templateSelection' => new JsExpression('function(id_establecimiento) { 
                        
                        return id_establecimiento.text; }'),
                   
                ],
    ]); ?>

    <?php echo $form->field($model, 'numero_informe')->textInput(['maxlength' => true, 'readonly' => true]);       // ->textarea()?>


    <?php echo $form->field($model, 'fecha_ingreso')->widget(DateTimePicker::classname(), [
    'options' => [
        'placeholder' => 'Ingrese la fecha y hora de ingreso',
        'required' => true,
    ],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd/mm/yyyy hh:ii:ss',
        'todayHighlight' => true,
         'container' => 'body'
    ],
    'pluginEvents' => [
        "changeDate" => "function(e) { verFecha(e); }",
    ],
]); ?>

<?php
    echo $form->field($model, 'fecha_egreso', [
        'errorOptions' => ['tag' => 'div', 'class' => 'help-block text-danger'],
        'labelOptions' => ['style' => 'font-weight: bold; display: block;'],
        'options' => ['class' => 'form-group field-internacion-fecha_egreso'] 
    ])->widget(DateTimePicker::classname(), [
        'options' => [
            'placeholder' => 'Ingrese la fecha y hora de egreso',
            'required' => true,
            'id' => 'internacion-fecha_egreso',

        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yyyy hh:ii:ss',
            'todayHighlight' => true,
             'container' => 'body'
        ]
    ]);
    ?>
<?php

$servicio =  ArrayHelper::map(Sector::find()->asArray()->all(), 'id', 'descripcion');
echo $form->field($model, 'id_sector', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($servicio,['prompt'=>'Seleccione una opción'])->label('Servicio');


$lugarIngreso =  ArrayHelper::map(LugarIngreso::find()->asArray()->all(), 'id', 'nombre');
echo $form->field($model, 'id_lugar_ingreso', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($lugarIngreso,['prompt'=>'Seleccione una opción']);
?>


<?php 
$tipoEgreso =  ArrayHelper::map(TipoEgreso::find()->asArray()->all(), 'id', 'nombre');
echo $form->field($model, 'id_tipo_egreso', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($tipoEgreso,['prompt'=>'Seleccione una opción']);
?>

<?php    echo $form->field($model, 'total_dias_estadia', [
    'labelOptions' => ['style' => 'font-weight: bold; display: block;']
])->textInput(['type' => 'number']);?>
<h2>Diagnostico</h2>
<?php
echo $form->field($model, 'id_diagnostico')->widget(Select2::classname(), [
    'options' => ['placeholder' => 'Select diagnostico ...',
    'onchange' => 'habilitarEmbarazo(this.value)'    ],
    'data' => $listaDiagnostico,
    'pluginOptions' => [
        'minimumInputLength' => 3,
        'allowClear' => true,
        'language' => [
            'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
        ],
        'ajax' => [
            'url' => \yii\helpers\Url::to(['/mpi/diagnostico/list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
                    'templateSelection' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
                ],

    
      
      
            ]);
?>
<h2>Causa externa</h2>
<?php echo $form->field($causaExterna,'nombre')->textInput([
    'style' => 'max-width: 500px; width: 70%;', // Estilo CSS personalizado
]);
 echo $form->field($causaExterna,'como_se_produjo')->textarea([
    'maxlength' => true, // o un número específico como 'maxlength' => 255
    'rows' => 6, // o cualquier número de filas que prefieras
    'placeholder' => 'Describe cómo se produjo',
    'style' => 'resize: none; width: 70%;'
]);
 $lugarList=ArrayHelper::map(Lugar::find()->asArray()->all(), 'id', 'nombre');
 echo $form->field($causaExterna,'id_lugar')->widget(Select2::classname(), [
'options' => ['placeholder' => 'Selecione lugar'],
'data' => $lugarList,
'pluginOptions' => [
    'allowClear' => true, // Habilita el botón de limpiar (la "x")
],
]) ;
$producidoPorList = ArrayHelper::map(ProducidoPor::find()->asArray()->all(), 'id', function($model) {
    return $model['codigo'] . ' - ' . $model['descripcion'];
});

echo $form->field($causaExterna,'id_producido_por')->widget(Select2::classname(), [
    'options' => ['placeholder' => 'Selecione lugar'],
    'data' => $producidoPorList,
    'pluginOptions' => [
        'allowClear' => true, // Habilita el botón de limpiar (la "x")
    ],
]);?>

<h2>Paciente:</h2>
<?= $form->field($model, 'id_paciente')->widget(Select2::class, [
    'options' => ['placeholder' => 'Select paciente ...'],
    'data' => $listaPaciente,
    'pluginOptions' => [
    'minimumInputLength' => 3,
    'allowClear' => true,
    'language' => [
    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
    ],
    'ajax' => [
    'url' => \yii\helpers\Url::to(['/mpi/paciente/list']),
    'dataType' => 'json',
    'data' => new JsExpression('function(params) { 

        return {q:params.term}; }'),
        ],
    'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
    'templateResult' => new JsExpression('function(id_paciente) { return id_paciente.text; }'),
    'templateSelection' => new JsExpression('function(id_paciente) {  
        return id_paciente.text; }'),
        ]
 ]);?>
<h4>Educacion:</h4>
<?php
$asistio=ArrayHelper::map(Asistio::find()->asArray()->all(), 'id', 'nombre');
echo $form->field($sistemaEducativo, 'id_asistio', [
    'labelOptions' => ['style' => 'font-weight: bold; display: block;'] ])->dropDownList($asistio,['prompt'=>'Seleccione una opción','onchange' => 'hablitarSistemaEducativo(event.target.value)'])->label("Asistio sistema educativo");
?>
    <div id="sistemaEducativo" style="display: none;">

    <?= $form->field($sistemaEducativo, 'primario_completo')->radioList(
        [Educacion::PRIMARIO_COMPLETO => Educacion::PRIMARIO_COMPLETO,Educacion::PRIMARIO_INCOMPLETO => Educacion::PRIMARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']) ?>

    <?= $form->field($sistemaEducativo, 'secundario_completo')->radioList(
        [Educacion::SECUNDARIO_COMPLETO => Educacion::SECUNDARIO_COMPLETO, Educacion::SECUNDARIO_INCOMPLETO => Educacion::SECUNDARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <?= $form->field($sistemaEducativo, 'superior_completo')->radioList(
        [Educacion::SUPERIOR_COMPLETO => Educacion::SUPERIOR_COMPLETO, Educacion::SUPERIOR_INCOMPLETO => Educacion::SUPERIOR_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <?= $form->field($sistemaEducativo, 'universitario_completo')->radioList(
        [Educacion::UNIVERSITARIO_COMPLETO => Educacion::UNIVERSITARIO_COMPLETO, Educacion::UNIVERSITARIO_INCOMPLETO => Educacion::UNIVERSITARIO_INCOMPLETO], 
        ['style' => 'margin-bottom: 5%; margin-left: 2%;']
    ) ?>

    <p style="display: none;" id="error-block-sistema-educativo">
        Debe seleccionar al menos una educacion.
    </p>
</div>



<div id="embarazo-container">
<?php if($isEmbarazo){?>
            <h2>Embarazo</h2>
        <?php
        $data=ArrayHelper::map(TipoParto::find()->asArray()->all(), 'id', 'nombre');
        echo $form->field($embarazo, 'id_tipo_parto', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($data,['prompt'=>'Seleccione una opción']);
    ?>
<?php
    echo $form->field($embarazo,'fecha_fin_embarazo', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->widget(DatePicker::className(),[
        'options' => ['placeholder' => 'Ingrese una fecha'],
        'pluginOptions' => ['autoclose'=>true]
        ]);?>

<?php echo $form->field($embarazo,'edad_gestacional', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->input('number'); ?>
<?php echo $form->field($embarazo,'paridad', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->input('number');?>
    </div> <?php }?>
    <?php ActiveForm::end(); ?>
    
</div>

