<?php

use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\DetailView;
?>
<div class="internacion-form-finalizar">
<script>
function finalizarInternacion(){
        var internacion = JSON.parse(localStorage.getItem('internacion'));
        var situacionLaboral = JSON.parse(localStorage.getItem('situacionLaboral'));
        var sistemaEducativo = JSON.parse(localStorage.getItem('sistemaEducativo'));
        var embarazo = JSON.parse(localStorage.getItem('embarazo'));
        var causaExterna = JSON.parse(localStorage.getItem('causaExterna'));
        var paciente = JSON.parse(localStorage.getItem('paciente'));
        const url = new URL(window.location.href);
        const params = new URLSearchParams(url.search);
        const parametro = params.get('modelId');
        var postData = {
            Internacion: internacion,
            SituacionLaboral: situacionLaboral,
            CausaExterna: causaExterna,
            SistemaEducativo: sistemaEducativo,
            Embarazo: embarazo,
            Paciente: paciente,
            isCreate:true,
        };
        $.ajax({
            url:'<?= Url::to(['internacion/create']) ?>&modelId=' + parametro,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(postData),
            success: function(data) {
                if (data.response == "success") {
                $('#ajaxCrudModal').modal('hide');
                // Asegúrate de que el contenido HTML incluya el contador
                $("#title").html(data.title);
                $("#mensaje").html(data.content + "<p class='text-success' >Redirigiendo en <span id='countdown'>3</span> segundos...</p>");
                $("#modalMensaje").modal('show');
                var countdown = 3;
                document.getElementById("countdown").textContent = countdown;
                var interval = setInterval(function() {
                    countdown--;
                    document.getElementById("countdown").textContent = countdown;
                    if (countdown <= 0) {
                        clearInterval(interval);
                        localStorage.clear();
                        window.location.reload(); // Recarga la página
                    }
                }, 1000); // 1000 milisegundos = 1 segundo

            } else {
                console.log("entro aca");
            }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log('Error:', jqXHR.responseText, textStatus, errorThrown);

            }
        });
  }
</script>

<?php
$form = ActiveForm::begin([
    'id' => 'create-internacion-finalizar-form',
    'options' => [
        'data-pjax' => true, // Si crees que PJAX está causando el problema, intenta cambiar a false
        'class' => 'form-group mb-3 mr-2 me-2',
        'style' => 'display: grid;',

    ],
]);

?>
<div style="flex-wrap: wrap;">
    <div style="
    flex: 1;
    padding: 10px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;">
<h3>Internacion</h3>

<?php

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute' => 'numero_informe',
            'value' => function ($model) {
                return $model->numero_informe;
            },
        ],
        [
            'attribute' => 'anio_activo',
            'value' => function ($model) {
                return $model->anio_activo;
            },
        ],
        [
            'attribute' => 'fecha_ingreso',
            'value' => function ($model) {
                return $model->fecha_ingreso;
            },
        ],
        [
            'attribute' => 'fecha_egreso',
            'value' => function ($model) {
                return $model->fecha_egreso;
            },
        ],
        [
            'attribute' => 'id_establecimiento',
            'value' => function ($model) {
                return $model->establecimiento->nombre;
            },
        ],
        [
            'attribute' => 'id_lugar_ingreso',
            'value' => function ($model) {
                return $model->lugaringreso->nombre;
            },
        ],
        [
            'attribute' => 'id_tipo_egreso',
            'value' => function ($model) {
                return $model->tipoegreso->nombre;
            },
        ],
        [
            'attribute' => 'total_dias_estadia',
            'value' => function ($model) {
                return $model->total_dias_estadia;
            },
        ],
        [
            'attribute' => 'id_diagnostico',
            'value' => function ($model) {
                return $model->diagnostico->nombre;
            },
        ],
    ],
    
]);
?>
</div>
<div style="
    flex: 1;
    padding: 10px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;">
<h3>Paciente</h3>
<?php
echo DetailView::widget([
    'model' => $paciente,
    'attributes' => [
        [
            'attribute' => 'numero_documento',
            'value' => function ($model) {
                return $model->numero_documento;
            },
        ],
        [
            'attribute' => 'nombre',
            'value' => function ($model) {
                return $model->nombre;
            },
        ],
        [
            'attribute' => 'apellido',
            'value' => function ($model) {
                return $model->apellido;
            },
        ],
        [
            'attribute' => 'id_localidad',
            'value' => function ($model) {
       
                return $model->localidad?$model->localidad->nombre:null;
            },
        ],
       [
            'attribute' => 'id_sexo',
            'value' => function ($model) {
                return $model->idsexo->nombre;
            },
        ],
        [
            'attribute' => 'id_tipo_documento',
            'value' => function ($model) {
                return $model->tipodocumento->nombre;
            },
        ],
        [
            'attribute' => 'id_tipo_plan_social',
            'value' => function ($model) {

                return $model->tipoplansocial ? $model->tipoplansocial->nombre: null;
            },
        ],
        [
            'attribute' => 'id_obra_social',
            'value' => function ($model) {
   
                return $model->obrasocial ? $model->obrasocial->nombre: null;
            },
        ],

    ],
    
]);
echo DetailView::widget([
    'model' => $situacionLaboral,
    'attributes' => [
        [
            'attribute' => 'id_tipo_situacion',
            'value' => function ($model) {
                return $model->tiposituacion ? $model->tiposituacion->nombre : null;
            },
        ],
        [
            'attribute' => 'id_ocupacion',
            'value' => function ($model) {
                return $model->tipoocupacion ? $model->tipoocupacion->nombre : null;
            },
        ],
    ],
]);

echo DetailView::widget([
    'model' => $contacto,
    'attributes' => [
        [
            'attribute' => 'contacto',
            'value' => function ($model) {
                return $model->numero_telefono;
            },
        ],


    ],
    
]);
?></div>
<div style="
    flex: 1;
    padding: 10px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;">
<h3>Embarazo</h3>
<?php
echo DetailView::widget([
    'model' => $embarazo,
    'attributes' => [
        [
            'attribute' => 'Fecha fin embarazo',
            'value' => function ($model) {
                return $model->fecha_fin_embarazo?$model->fecha_fin_embarazo:null;
            },
        ],
        [
            'attribute' => 'Edad gestacional embarazo',
            'value' => function ($model) {
                return $model->edad_gestacional?$model->edad_gestacional:null;
            },
        ],
        [
            'attribute' => 'Tipo de parto',
            'value' => function ($model) {
                return $model->tipoparto ? $model->tipoparto->nombre : null;
            },
        ],
        [
            'attribute' => 'Paridad',
            'value' => function ($model) {
                return $model->paridad ? $model->paridad : null;
            },
        ],

    ],
    
]);
?></div>
<div style="
    flex: 1;
    padding: 10px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;">
<h3>Causa externa</h3>
<?php
echo DetailView::widget([
    'model' => $causaExterna,
    'attributes' => [
        [
            'attribute' => 'como_se_produjo',
            'value' => function ($model) {
                return $model->como_se_produjo ? $model->como_se_produjo : null;
            },
        ],
        [
            'attribute' => 'id_producido_por',
            'value' => function ($model) {
                return $model->producidopor ? $model->producidopor->descripcion : null;
            },
        ],
        [
            'attribute' => 'id_lugar',
            'value' => function ($model) {
                return $model->lugar ? $model->lugar->nombre : null;
            },
        ],
        [
            'attribute' => 'nombre',
            'value' => function ($model) {
                return $model->nombre ? $model->nombre : null;
            },
        ],

    ],
    
]);
?></div>
<div style="
    flex: 1;
    padding: 10px;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;">
<h3>Sistema Educativo</h3>
<?php
echo DetailView::widget([
    'model' => $sistemaEducativo,
    'attributes' => [
        [
            'attribute' => 'id_asistio',
            'value' => function ($model) {
                return $model->idasistio->nombre;
            },
        ],
        [
            'attribute' => 'primario_completo',
            'value' => function ($model) {
                return $model->primario_completo;
            },
        ],
        [
            'attribute' => 'secundario_completo',
            'value' => function ($model) {
                return $model->secundario_completo;
            },
        ],
        [
            'attribute' => 'superior_completo',
            'value' => function ($model) {
                return $model->superior_completo;
            },
        ],
        [
            'attribute' => 'universitario_completo',
            'value' => function ($model) {
                return $model->universitario_completo;
            },
        ],

    ],
    
]);
?></div>
</div>
    <?php ActiveForm::end(); ?>

</div>