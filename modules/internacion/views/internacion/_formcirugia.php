<?php


use app\modules\internacion\models\Internacion;
use app\modules\internacion\models\Sector;
use app\modules\mpi\models\Diagnostico;
use app\modules\mpi\models\PServicio;

//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

use kartik\form\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\PaseInternacion */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
    /*var inicio=0;
function agregarDescripcion(){
    if(inicio<1){
    inicio=inicio+1;
    var $clonedElement = $("#cirugia-descripcion").clone();
    var newId = "cirugia-descripcion-clone-" + inicio;
    $clonedElement.attr("id", newId);
    $clonedElement.appendTo("#nuevaactividad");

            /*document.getElementById("button-agregar").setAttribute("style", "display: none;"); 
            // mostra signo menos en filas clonadas
            document.getElementById("button-eliminar").setAttribute("style", "display: block; line-height: 10px !important;");
            linea++;*/
           /* document.getElementsByClassName("fila")[i].innerHTML=linea;
            // eliminar el id de los clonados (se dejan el original)
            $("[id='ActvG']:eq("+ i + ")").attr("id","")
            $("[id='ActvP']:eq("+ i + ")").attr("id","")
            $("[id='SubAct']:eq("+ i + ")").attr("id","")
            $("[id='SubbAct']:eq("+ i + ")").attr("id","")
            // los names se pueden llamar para tomar su valor usando un arreglo
            document.getElementById("button-eliminar").style.display="inline-block";
            document.getElementById("button-agregar").style.display="none";
}
            if(inicio>0)
            document.getElementById("button-eliminar").style.display="inline-block";
} 
function eliminarDescripcion(){
    if(inicio>0){

document.getElementById("cirugia-descripcion-clone-"+inicio).remove();
inicio=inicio-1;
if(inicio==0)
document.getElementById("button-eliminar").style.display="none";
document.getElementById("button-agregar").style.display="inline-block";
}

        
}
*/
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
</script>
<div class="cirugia-internacion-form">

    <?php 
$form = ActiveForm::begin([
    'id' => 'create-cirugia-form', 
    'type' => ActiveForm::TYPE_VERTICAL,
    //'tooltipStyleFeedback' => true, // shows tooltip styled validation error feedback
    'options' => ['class' => 'form-group mb-3 mr-2 me-2', 'style' => 'width: 100%; display:grid;'],// Espaciado de los grupos de campos y ancho

]);  ?>


<div  style="width:90%;">

    <?php echo $form->field($model, 'descripcion')->textInput(['placeholder' => 'descripcion']);
     // ->textarea()?>
    </div>
    <?php echo $form->field($model, 'codigo')->textInput(['placeholder' => 'codigo']) ?>


    

    <?php ActiveForm::end(); ?>
    
</div>
