<?php

use app\modules\internacion\models\CausaExterna;
use app\modules\internacion\models\Circunstancia;
use app\modules\internacion\models\Embarazo;
use app\modules\internacion\models\Lugar;
use app\modules\internacion\models\PaseInternacion;
use app\modules\internacion\models\ProducidoPor;
use app\modules\internacion\models\TipoEgreso;
use app\modules\mpi\models\Diagnostico;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\Internacion */
/* @var $form yii\widgets\ActiveForm */

use kartik\tabs\TabsX;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

// Above

?>

<script>

var causaExterna = JSON.parse(localStorage.getItem('causaExterna'));
var internacion = JSON.parse(localStorage.getItem('internacion'));
if( causaExterna!=null && internacion!=null){
if(causaExterna.nombre!=null && causaExterna.como_se_produjo!=null && causaExterna.id_lugar!=null && causaExterna.id_producido_por!=null && internacion.id_diagnostico!=null){
document.getElementById("causaexterna-nombre").value=causaExterna.nombre;
  document.getElementById("causaexterna-como_se_produjo").value=causaExterna.como_se_produjo;
  document.getElementById("causaexterna-id_lugar").value=causaExterna.id_lugar;
  document.getElementById("causaexterna-id_producido_por").value=causaExterna.id_producido_por;
  document.getElementById("internacion-id_diagnostico").value=internacion.id_diagnostico;
/* deshabilitar los diagnosticos
  $("#inputs input, #inputs select, #inputs textarea").each(function() {
                    $(this).prop('disabled', true);
                });
                document.getElementById('internacion-id_diagnostico').disabled=true;*/
}
}

function filterDiagnostico(search){
    var paciente = JSON.parse(localStorage.getItem('paciente'));
    console.log(search);
    var internacion = JSON.parse(localStorage.getItem('internacion'));
    if(paciente!=null){
    $.ajax({
        url:'<?php echo Url::to(['/mpi/diagnostico/listfilter'])?>'+'&sexo=' + paciente.id_sexo+ '&search=' + encodeURIComponent(search),
    dataType: "json",
    type: "GET",
    success: function(data) {


        return data


       
    }
})
}
else{
    $('#ajaxCrudModal .modal-header').html(`<p class="text-dark">No se puede asignar diagnostico</p>`);
    $('#ajaxCrudModal .modal-body').html(`<div class="text-danger"><p>No se encontro ningun paciente cargado</p></div>`);
    $('#ajaxCrudModal').modal('show');
}
   
}
    function createDiagnostico(){
    event.preventDefault();
    /*desabilitar los diagnosticos
    $("#inputs input, #inputs select, #inputs textarea").each(function() {
                    $(this).prop('disabled', false);
                });
                document.getElementById('internacion-id_diagnostico').disabled=false;*/
    var data = $('#form-id-diagnostico');
    console.log(data.serialize());
    $.ajax({
    url: '<?php echo Url::to(['internacion/indexdiagnostico', 'modelId' => $modelId]) ?>',
    dataType: "json",
    type: "POST",
    data: data.serialize(),
    success: function(data) {
       if(data.response=='success'){

        $("#mensaje").html(data.title);
        document.getElementById("mensaje").style.color="green"
        $("#title").html(data.content);
        $("#modalMensaje").modal('show');
        console.log(JSON.stringify(data.internacion));
        localStorage.setItem('internacion',JSON.stringify(data.internacion))
        localStorage.setItem('causaExterna',JSON.stringify(data.causaExterna))
        /*$("#inputs input, #inputs select, #inputs textarea").each(function() {
        $(this).prop('disabled', true);
        });
        document.getElementById('internacion-id_diagnostico').disabled=true;*/
       }
       else{
               document.getElementById("mensaje").style.color="red"
               $("#title").html(data.title);
               $("#mensaje").html(data.content);
            /* $("#mensaje").html("<p> Nombre causa externa:"+data.errors[0]["causaexterna-nombre"][0]?data.errors[0]["causaexterna-nombre"][0]:""+"</p>"+"<p>Causa externa como se produjo"+data.errors[0]["causaexterna-como_se_produjo"][0]?data.errors[0]["causaexterna-como_se_produjo"][0]:""+"</p>"+"<p>Causa externa lugar"+
            data.errors[0]["causaexterna-id_lugar"][0]?data.errors[0]["causaexterna-id_lugar"][0]:""+"</p>"+"<p> Causa externa producido por:"+data.errors[0]["causaexterna-id_producido_por"][0]?data.errors[0]["causaexterna-id_producido_por"][0]:""+"</p>"
            );*/
            $("#modalMensaje").modal('show');
        
        }
 

    },
    error: function(error) {
        console.log($("#modalMensaje"));
        $("#modalMensaje .modal-header").html(`<p class="text-warning">Error de datos!</p>`);
        MENSAJE = 'Error:'+error.responseJSON.message;
        document.getElementById("mensaje").style.color="red"
        $("#mensaje").html(MENSAJE);
        $("#modalMensaje").modal('show');
    }
     });
    }
</script>
<div id="diagnostico-content" class="internacion-form">

<?php
    $this->title = 'Otros diagnosticos(Opcional)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => function ($model, $key, $index, $column) {
            return [
                'style' => ['white-space' => 'nowrap'],
                'attribute' => 'inn',
                'format' => 'text',
                'hiddenFromExport' => true,
            ];
        },
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key,"model"=>get_class($model)]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Obra Social',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]
            
    ];

$stringToolbar="";

if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['creatediagnostico','accion'=>$_GET['r'],'modelId'=>$modelId],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['','modelId'=>$modelId],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r'],'model'=>"diagnosticoobservacion"],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r'],'model'=>"diagnosticoobservacion"],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}
?>

<div id="error-container" style="color: red; display: none;"></div>
<?php 
$form = ActiveForm::begin(['id' => 'form-id-diagnostico'] ); 
    $diagnosticoList=ArrayHelper::map($diagnosticos = Diagnostico::find()
    ->asArray()
    ->all(), 'id', 'nombre');
    echo $form->field($model, 'id_diagnostico', ['options' => ['style' => 'width: 100%;']])->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Select diagnostico ...'],
        'data' => $diagnosticoList,
        'pluginOptions' => [
            'minimumInputLength' => 3,
            'allowClear' => true,
            'language' => [
                'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/mpi/diagnostico/listfilter']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { 
                    var paciente = JSON.parse(localStorage.getItem("paciente"));
                    var errorContainer = $("#error-container"); // Contenedor para mostrar el error
                    
                    if(paciente == null){
                        errorContainer.text("Error: No se puede ubicar un diagnostico porque no se selecciono paciente.");
                        errorContainer.show(); // Muestra el contenedor de error
                        return false; // Detiene la petición AJAX si paciente es null
                    } else {
                        errorContainer.hide(); // Oculta el mensaje si todo está bien
                    }
                    return {
                        q: params.term, 
                        sexo: paciente ? paciente.id_sexo : null,
                        fecha_nacimiento: paciente ? paciente.fecha_nacimiento : null
                    };
                }'),

            ],
            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
            'templateResult' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
            'templateSelection' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
        ],
    ])->label("Diagnostico principal");
    ?>

    <div class="diagnostico-index"> 
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable-diagnostico',
            'dataProvider' => $dataProvider,
            'filterModel' => null,//$cirugiaSearch, el filtro no funca
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [['content' => $stringToolbar],],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' =>$this->title,
                
            ],
   
        ])?>
    </div>
</div>



    
    

<?php

$this->params['breadcrumbs'][] = "Cirugia(Opcional)";

$columnsCirugia[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => function ($model, $key, $index, $column) {
            return [
                'style' => ['white-space' => 'nowrap'],
                'attribute' => 'inn',
                'format' => 'text',
                'hiddenFromExport' => true,
            ];
        },
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key,"model"=>get_class($model)]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Diagnostico',
            'data-confirm-message'=>'¿ Desea borrar este registro ?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]
            
    ];

$stringToolbarCirugia="";

if ($permisos['create']){
    $stringToolbarCirugia=$stringToolbarCirugia.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['createcirugia','accion'=>$_GET['r'],'modelId'=>$modelId],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default', // Cambia 'internacion/indexembarazo' por la URL a la que quieres redirigir
]);
}
if ($permisos['index']){
    $stringToolbarCirugia=$stringToolbarCirugia.Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['','modelId'=>$modelId],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbarCirugia=$stringToolbarCirugia.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r'],'model'=>"cirugia"],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbarCirugia=$stringToolbarCirugia.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r'],'model'=>"cirugia"],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}
?>
    <div class="cirugia-index"> <!--  style='width:100%;margin-right: auto;margin-left: auto;' -->
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable-cirugia',
            'dataProvider' => $cirugiaProvider,
            'filterModel' => null,//$cirugiaSearch, el filtro no funca
            'pjax'=>true,
            'columns' => $columnsCirugia,
            'toolbar'=> [['content' => $stringToolbarCirugia]],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' =>"Cirugia",
                
            ],
   
        ])?>
    </div>
</div>

<div id="inputs" style="display: flex;
    flex-direction: column;
    width: auto;
    grid-gap: 1%;">
<?php /*
esta mal va en la tabla
echo $form->field($cirugia,'descripcion')->textInput();
echo $form->field($cirugia,'codigo')->textInput()*/?>
<?php echo $form->field($causaExterna,'nombre')->textInput([
    'style' => 'max-width: 500px; width: 70%;', // Estilo CSS personalizado
])?>
<?php echo $form->field($causaExterna,'como_se_produjo')->textarea([
    'maxlength' => true, // o un número específico como 'maxlength' => 255
    'rows' => 6, // o cualquier número de filas que prefieras
    'placeholder' => 'Describe cómo se produjo',
    'style' => 'resize: none; width: 70%;'
])?>
<?php $lugarList=ArrayHelper::map(Lugar::find()->asArray()->all(), 'id', 'nombre');
 echo $form->field($causaExterna,'id_lugar')->widget(Select2::classname(), [
'options' => ['placeholder' => 'Selecione lugar'],
'data' => $lugarList,
'pluginOptions' => [
    'allowClear' => true, // Habilita el botón de limpiar (la "x")
],
])?>
<?php
$producidoPorList = ArrayHelper::map(ProducidoPor::find()->asArray()->all(), 'id', function($model) {
    return $model['codigo'] . ' - ' . $model['descripcion'];
});
echo $form->field($causaExterna,'id_producido_por')->widget(Select2::classname(), [
    'options' => ['placeholder' => 'Selecione lugar'],
    'data' => $producidoPorList,
    'pluginOptions' => [
        'allowClear' => true, // Habilita el botón de limpiar (la "x")
    ],
])?>
</div>


<div class="form-group" style="float:right">
<?php

echo Html::button('Guardar', ['id'=>'button-diagnostico','class' => 'btn btn-primary','type'=>'button' ,'onclick'=>'createDiagnostico()']); 
/*echo Html::a('<span>Guardar</span>', ['create','modelId'=>$modelId],
            ['id'=>'button-internacion-create','role'=>'modal-remote','title'=> 'Crear internacion','class'=>'btn btn-primary','style' => 'display: none;']);
*/
?>
</div>
<?php ActiveForm::end(); ?>


</div>
