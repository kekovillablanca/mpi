<script>

document.getElementsByClassName("footer")[0].style.display="none";


</script>
<?php

use app\modules\internacion\models\CausaExterna;
use app\modules\internacion\models\Circunstancia;
use app\modules\internacion\models\Embarazo;
use app\modules\internacion\models\PaseInternacion;
use app\modules\internacion\models\TipoEgreso;
use app\modules\internacion\models\TipoParto;
use yii\widgets\ActiveForm;
//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\Internacion */
/* @var $form yii\widgets\ActiveForm */

use kartik\tabs\TabsX;
use quidu\ajaxcrud\CrudAsset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

// Above

?>

<script>
var embarazo = JSON.parse(localStorage.getItem('embarazo'));
if(embarazo.edad_gestacional!=null && embarazo.fecha_fin_embarazo!=null && embarazo.paridad!=null && embarazo.id_tipo_parto!=null){

    document.getElementById("embarazo-id_tipo_parto").value=embarazo.id_tipo_parto;
  document.getElementById("embarazo-fecha_fin_embarazo").value=embarazo.fecha_fin_embarazo
  document.getElementById("embarazo-edad_gestacional").value=embarazo.edad_gestacional;
  document.getElementById("embarazo-paridad").value=embarazo.paridad;
 /* deshabilitar embarazo
 
 $("#form-id-embarazo input, #form-id-embarazo select").each(function() {
                $(this).prop('disabled', true);
            });
    $('.kv-date-remove').css('pointer-events', 'none').css('opacity', '0.5').attr('title', 'Deshabilitado');
    $('.kv-datetime-picker').css('pointer-events', 'none').css('opacity', '0.5').attr('title', 'Deshabilitado');*/
}
function createEmbarazo(){
    event.preventDefault();
    /*deshabilitar embarazo
    $("#form-id-embarazo input, #form-id-embarazo select").each(function() {
                $(this).prop('disabled', false);
            });*/
    var data = $('#form-id-embarazo');
    console.log(data.serialize());
    $.ajax({
    url: '<?php echo Url::to(['internacion/indexembarazo', 'modelId' => $modelId]) ?>',
    dataType: "json",
    type: "POST",
    data: data.serialize(),
    success: function(data) {
       if(data.response=='success'){
        $("#mensaje").html(data.title);
        document.getElementById("mensaje").style.color="green"
        $("#title").html(data.content);
        $("#modalMensaje").modal('show');
        localStorage.setItem('embarazo',JSON.stringify(data.embarazo))
        /*deshabilitar
        $("#form-id-embarazo input, #form-id-embarazo select").each(function() {
            $(this).prop('disabled', true);
            });
            $('.kv-date-remove').css('pointer-events', 'none').css('opacity', '0.5').attr('title', 'Deshabilitado');
            $('.kv-datetime-picker').css('pointer-events', 'none').css('opacity', '0.5').attr('title', 'Deshabilitado');
            $('.input-group-addon.kv-date-picker').remove();*/

        }
       else{
             document.getElementById("mensaje").style.color="red"
             $("#title").html(data.title);
             $("#mensaje").html(data.content);
             $("#modalMensaje").modal('show');
          }

    },
    error: function(error) {
        console.log($("#modalMensaje"));
        $("#modalMensaje .modal-header").html(`<p class="text-warning">Error de datos!</p>`);
        MENSAJE = 'Error:'+error.responseJSON.message;
        document.getElementById("mensaje").style.color="red"
        $("#mensaje").html(MENSAJE);
        $("#modalMensaje").modal('show');
    }
     });
}

</script>
<div id="embarazo-content" class="internacion-form">

    <?php 
    $this->title = 'embarazo (Opcional)';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$columns[]=
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => function ($model, $key, $index, $column) {
            return [
                'style' => ['white-space' => 'nowrap'],
                'attribute' => 'inn',
                'format' => 'text',
                'hiddenFromExport' => true,
            ];
        },
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key,"model"=>get_class($model)]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Editar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Borrar',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Embarazo',
            'data-confirm-message'=>'¿Desea borrar este registro?'],
        'visibleButtons'=>[
            'view'=> $permisos['view'],
            'update'=> $permisos['update'],
            'delete'=> $permisos['delete']
            ]
            
    ];

$stringToolbar="";

if ($permisos['create']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-plus"></i>', ['createobstetricia','modelId'=>$modelId],
            ['role'=>'modal-remote','title'=> 'Crear Registro','class'=>'btn btn-default']);
}
if ($permisos['index']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['','modelId'=>$modelId],
            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Limpiar orden/filtro']);
}
if ($permisos['export']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-download-alt"></i>', ['export','accion'=>$_GET['r'],'model'=>"obstetricia"],
            ['role'=>'modal-remote','title'=> 'Exportar','class'=>'btn btn-default']);
}
if ($permisos['select']){
    $stringToolbar=$stringToolbar.Html::a('<i class="glyphicon glyphicon-th"></i>', ['select','accion'=>$_GET['r'],'model'=>"obstetricia"],
            ['role'=>'modal-remote','title'=> 'Personalizar','class'=>'btn btn-default']);
}

$form = ActiveForm::begin(['id'=>"form-id-embarazo"]); 
?>
     
     <div class="embarazo-index"> <!--  style='width:100%;margin-right: auto;margin-left: auto;' -->
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable-obstetricia',
            'dataProvider' => $dataProvider,
            'filterModel' => null,//$cirugiaSearch, el filtro no funca
            'pjax'=>true,
            'columns' => $columns,
            'toolbar'=> [['content' => $stringToolbar],],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' =>"Embarazo",
                
            ],
   
        ])?>
    </div>
</div>

<div  style="width: 100%;
    display: flex;
    gap: 2%;">
<?php 
        $data=ArrayHelper::map(TipoParto::find()->asArray()->all(), 'id', 'nombre');
        echo $form->field($embarazo, 'id_tipo_parto', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($data,['prompt'=>'Seleccione una opción']);
    ?>
<?php
    echo $form->field($embarazo,'fecha_fin_embarazo', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->widget(DatePicker::className(),[
        'options' => ['placeholder' => 'Ingrese una fecha'],
        'pluginOptions' => ['autoclose'=>true]
        ]);?>

<?php echo $form->field($embarazo,'edad_gestacional', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->input('number'); ?>
<?php echo $form->field($embarazo,'paridad', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->input('number'); ?>

</div>



<div class="form-group" style="float:right">
<?php

echo Html::button('Guardar', ['id'=>'button-embarazo','class' => 'btn btn-primary','type'=>'button' ,'onclick'=>'createEmbarazo()']); 

/*echo Html::a('<span>Guardar</span>', ['create','modelId'=>$modelId],
            ['id'=>'button-internacion-create','role'=>'modal-remote','title'=> 'Crear internacion','class'=>'btn btn-primary','style' => 'display: none;']);
*/
?>
</div>

    <?php ActiveForm::end(); ?>

   
</div>
