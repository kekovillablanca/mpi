<?php

use app\modules\internacion\models\Internacion;
use app\modules\internacion\models\Sector;
use app\modules\mpi\models\PServicio;

//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

use kartik\form\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\PaseInternacion */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
</script>
<div class="pase-internacion-form">

    <?php 
$form = ActiveForm::begin();  ?>

    <?php 
        $data=ArrayHelper::map(Sector::find()->asArray()->all(), 'id', 'descripcion');
        echo $form->field($model, 'id_sector', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->dropDownList($data,['prompt'=>'Seleccione una opción']);

        /*
        echo $form->field($model, 'id_sector')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => ($model->id_sector)?$model->internacion.sector->nombre:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['internacion.sector/list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_sector) { return id_sector.text; }'),
                'templateSelection' => new JsExpression('function(id_sector) { return id_sector.text; }'),
            ],
        ]);
        */
    ?>

    <?php echo $form->field($model, 'dias_estadia', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->textInput(['type'=>'number']);        // ->textarea()?>

    <?php 
     /*   $data=ArrayHelper::map(PServicio::find()->asArray()->all(), 'id', 'nombre');
        echo $form->field($model, 'id_servicio', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->widget(Select2::classname(), [
            'options' => ['placeholder' => 'Select servicio ...'],
            'data' => $data,
            'pluginOptions' => [
            'minimumInputLength' => 3,
            'allowClear' => true,
            'language' => [
                'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/mpi/pservicio/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { 
                  
                   return {q:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function(markup) {return markup; }'),
               'templateResult' => new JsExpression('function(id_servicio) {  return id_servicio.text; }'),
               'templateSelection' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
           ],
       ]);  ;

        /*
        echo $form->field($model, 'id_servicio')->widget(Select2::classname(), [
            'options' => ['multiple'=>false, 'placeholder' => 'Buscar...'],
            'initValueText' => ($model->id_servicio)?$model->consultasestadisticas.p_servicio->nombre:"",
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
                ],
                'ajax' => [
                    'url' => yii\helpers\Url::to(['consultasestadisticas.p_servicio/list']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                'templateResult' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
                'templateSelection' => new JsExpression('function(id_servicio) { return id_servicio.text; }'),
            ],
        ]);
        */
    ?>



    <?php echo $form->field($model, 'fecha_ingreso', ['labelOptions' => ['style' => 'font-weight: bold; display: block;']])->widget(DateTimePicker::className(),[
                'options' => ['placeholder' => 'Ingrese una fecha'],
                'pluginOptions' => ['autoclose'=>true,'format' => 'dd-mm-yyyy hh:ii:ss']
                ]);?>

  
<?php  /* Modal::begin([
    "id"=>"ajaxCrudModal",
    'size' => Modal::SIZE_LARGE,
   ]);
   echo '<div id="modal-header"></div>';
   echo '<div id="modal-content"></div>';
   echo '<div id="modal-footer" style="display: grid;"></div>';
 Modal::end();
 $formUrl = Url::to(['internacion/create', 'modelId' => $modelId]);
 $this->registerJs("
 $('#button-pase').on('click', function(event) {
    var form = $('#create-pase-form');
     var modelId = form.data('model-id');
    $.ajax({
         url: '$formUrl',
         type: 'post',
        data: form.serialize(),
        success: function(response) {
            if (response.content) {
                $('#ajaxCrudModal .modal-header').html(response.title);
                $('#ajaxCrudModal #modal-content').html(response.content);
                $('#ajaxCrudModal #modal-footer').html(response.footer);
                $('#ajaxCrudModal').modal('show');    
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('AJAX error:', textStatus, errorThrown);
            alert('Error al guardar el registro');
        }
    });

    return false;
});
");
/*
$this->registerJs("
 $('#button-internacion').on('click', function(event) {
     event.preventDefault();
     var form = $('#your-form-id');
     
     $.ajax({
         url: '$formUrl',
         type: 'post',
         data: form.serialize(),
         success: function(response) {
             if (response.content) {
                 console.log(response);
                 $('#ajaxCrudModal .modal-header').html(response.title);
                 $('#ajaxCrudModal #modal-content').html(response.content);
                 $('#ajaxCrudModal #modal-footer').html(response.footer);
                 $('#ajaxCrudModal').modal('show');
                 
                 var modelId = response.modelId;
                 var urlPaciente = '" . Url::to(['internacion/formprincipal']) . "&modelId=' + modelId;
                 var urlDiagnostico = '" . Url::to(['internacion/indexdiagnostico']) . "&modelId=' + modelId;
                 var urlPase = '" . Url::to(['internacion/indexpase']) . "&modelId=' + modelId;
                 var urlEmbarazo = '" . Url::to(['internacion/indexembarazo']) . "&modelId=' + modelId;
                 
                 $('#paciente').attr('data-url', urlPaciente);
                 $('#diagnostico').attr('data-url', urlDiagnostico);
                 $('#pase').attr('data-url', urlPase);
                 $('#embarazo').attr('data-url', urlEmbarazo);
                 $('#tabs-container').removeClass('disabled');
                 $('#button-internacion').attr('disabled', true);
             }
         },
         error: function(jqXHR, textStatus, errorThrown) {
             console.log('AJAX error:', textStatus, errorThrown);
             alert('Error al guardar el registro');
         }
     });

     return false;
 });
");*/
 ?>
    <?php ActiveForm::end(); ?>

</div>
