<?php


use app\modules\internacion\models\Internacion;
use app\modules\internacion\models\Sector;
use app\modules\mpi\models\Diagnostico;
use app\modules\mpi\models\PServicio;

//
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\internacion\models\PaseInternacion */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
    var inicio=0;
function agregarObservacion(){
    if(inicio<1){
    inicio=inicio+1;
    var $clonedElement = $("#diagnosticoobservacion-observacion").clone();
    var newId = "diagnosticoobservacion-observacion-clone-" + inicio;
    $clonedElement.attr("id", newId);
    $clonedElement.appendTo("#nuevaactividad");

            /*document.getElementById("button-agregar").setAttribute("style", "display: none;"); 
            // mostra signo menos en filas clonadas
            document.getElementById("button-eliminar").setAttribute("style", "display: block; line-height: 10px !important;");
            linea++;*/
           /* document.getElementsByClassName("fila")[i].innerHTML=linea;
            // eliminar el id de los clonados (se dejan el original)
            $("[id='ActvG']:eq("+ i + ")").attr("id","")
            $("[id='ActvP']:eq("+ i + ")").attr("id","")
            $("[id='SubAct']:eq("+ i + ")").attr("id","")
            $("[id='SubbAct']:eq("+ i + ")").attr("id","")
            // los names se pueden llamar para tomar su valor usando un arreglo*/
            document.getElementById("button-eliminar").style.display="inline-block";
            document.getElementById("button-agregar").style.display="none";
}
            if(inicio>0)
            document.getElementById("button-eliminar").style.display="inline-block";
} 
function eliminarObservacion(){
    if(inicio>0){

document.getElementById("diagnosticoobservacion-observacion-clone-"+inicio).remove();
inicio=inicio-1;
if(inicio==0)
document.getElementById("button-eliminar").style.display="none";
document.getElementById("button-agregar").style.display="inline-block";
}

        
}

$.fn.modal.Constructor.prototype.enforceFocus = function() {};
</script>
<div class="diagnostico-internacion-form">

    <?php 
$form = ActiveForm::begin([
    'id' => 'create-diagnostico-form', 
    'type' => ActiveForm::TYPE_VERTICAL,
    //'tooltipStyleFeedback' => true, // shows tooltip styled validation error feedback
    'options' => ['class' => 'form-group mb-3 mr-2 me-2', 'style' => 'width: 100%; display:grid;'],// Espaciado de los grupos de campos y ancho
]);  ?>

    <?php 
    $diagnosticoList=ArrayHelper::map($diagnosticos = Diagnostico::find()
    ->asArray()
    ->all(), 'id', 'nombre');
    
    echo $form->field($model, 'id_diagnostico', ['options' => ['style' => 'width: 100%;']])->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Select diagnostico ...'],
        'data' => $diagnosticoList,
        'pluginOptions' => [
            'minimumInputLength' => 3,
            'allowClear' => true,
            'language' => [
                'errorLoading' => new JsExpression("function() { return 'Un momento...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/mpi/diagnostico/listfilter']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { 
                    var paciente = JSON.parse(localStorage.getItem("paciente"));
                    var errorContainer = $("#error-container-diag"); // Contenedor para mostrar el error
                    
                    if(paciente == null){
                        errorContainer.text("Error: No se puede ubicar un diagnostico porque no se selecciono paciente.");
                        errorContainer.show(); // Muestra el contenedor de error
                        return false; // Detiene la petición AJAX si paciente es null
                    } else {
                        errorContainer.hide(); // Oculta el mensaje si todo está bien
                    }
                    return {
                        q: params.term, 
                        sexo: paciente ? paciente.id_sexo : null,
                        fecha_nacimiento: paciente ? paciente.fecha_nacimiento : null
                    };
                }'),

            ],
            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
            'templateResult' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
            'templateSelection' => new JsExpression('function(id_diagnostico) { return id_diagnostico.text; }'),
        ],
    ])->label("Diagnostico principal");
    ?>
    <div id="error-container-diag" style="color: red; display: none;"></div>

    <?php echo $form->field($model, 'observacion',[  'hintType' => ActiveField::HINT_SPECIAL,
    'hintSettings' => ['placement' => 'right', 'onLabelClick' => true, 'onLabelHover' => false],
    'labelOptions' => ['style' => 'font-weight: bold; display: block;']
])->textArea([
    'id' => 'observacion-input', 
    'placeholder' => 'Observacion...', 
    'rows' => 5,
    'cols' => 50,
    'style' => 'resize: none;'
]);/*  . Html::button('+', [
        'id' => 'button-agregar',
        'class' => 'btn btn-primary',
        'onclick' => 'agregarObservacion()',
        'data-toggle' => 'modal',
        'style'=>'display:inline-block;'
    ]).Html::button('-', [
        'id' => 'button-eliminar',
        'class' => 'btn btn-primary',
        'onclick' => 'eliminarObservacion()',
        'data-toggle' => 'modal',
        'style'=>'display:inline-block;'
        
    ]); ;*/
    ?>

    <div id="nuevaactividad" style="display: grid; width:100%; " >
    </div>
    </div>


    

    <?php ActiveForm::end(); ?>
    
</div>
