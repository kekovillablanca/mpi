<?php

use app\components\Seguridad\Seguridad;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
class InternacionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select','list'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select','list'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }
}
?>