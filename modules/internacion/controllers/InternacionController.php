<?php

namespace app\modules\internacion\controllers;
use app\modules\internacion\models\DiagnosticoObservacion;
use app\modules\internacion\models\PaseInternacion;
use app\modules\internacion\models\SituacionLaboral;
use app\modules\internacion\models\Cirugia;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use app\modules\admin\models\Reporte;
use app\modules\admin\models\Vista;
use app\modules\internacion\models\Internacion;
use app\modules\internacion\models\InternacionSearch;
use app\components\Metodos\Metodos;
use app\components\Seguridad\Seguridad;
use app\models\MultipleUploadForm;
use app\models\UploadForm;
use app\modules\admin\controllers\UsuarioestadisticaController;
use app\modules\internacion\models\Asistio;
use app\modules\internacion\models\CausaExterna;
use app\modules\internacion\models\CirugiaSearch;
use app\modules\internacion\models\DiagnosticoObservacionSearch;
use app\modules\internacion\models\Embarazo;
use app\modules\internacion\models\Obstetricia;
use app\modules\internacion\models\ObstetriciaSearch;
use app\modules\internacion\models\Ocupacion;
use app\modules\internacion\models\PaseInternacionSearch;
use app\modules\internacion\models\SistemaEducativo;
use app\modules\mpi\models\Contacto;
use app\modules\mpi\models\Diagnostico;
use app\modules\mpi\models\DiagnosticoSearch;
use app\modules\mpi\models\Establecimiento;
use app\modules\mpi\models\Localidad;
use app\modules\mpi\models\Paciente;
use app\modules\mpi\utils\ErrorModelValidate;
use DateTime;
use Exception;
use kartik\growl\Growl;
use yii\web\UploadedFile;
use kartik\widgets\ActiveForm;
use Mpdf\Container\NotFoundException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\NotAcceptableHttpException;

/**
 * InternacionController implements the CRUD actions for Internacion model.
 */
class InternacionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','export','select','list'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','export','select','list','uploadfile'],
                        'allow' => Seguridad::tienePermiso(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    // 'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Internacion models.
     * @return mixed
     */
   
     
     public function actionIndex()
     {
        $request = Yii::$app->request;
        $model=new Internacion();
        
        /*$modelId=intval($request->get('modelId'));

        if($modelId!=null || $modelId!=0){
           $model= Internacion::findOne($modelId);
        }*/
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->id_diagnostico=-1;
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                try {
 
                $maxNumeroInforme = Internacion::find()
                ->where(['id_establecimiento' => $model->id_establecimiento])
                ->max('numero_informe');
                if($maxNumeroInforme!=null){
                    $maxNumeroInforme++;
             
                }
                else{
                    $maxNumeroInforme=1;
                }
                $model->numero_informe=$maxNumeroInforme;

               $model->estado='PROCESO';

                    if ($model->save()) {
                        $model->refresh();

                        return [    
                            'response'=>'success',
                            'title' => '<h4 class="text-succes">Registro #'. $model->id.'</span>',
                            'content' => '<span class="text-success">Se realizo la carga correctamente.</span>',
                            'modelId' => $model->id,
                            'model'=>$model,

                        ];
                    }
                } catch (\yii\db\Exception $e) {
                    // Manejar errores de base de datos
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new \yii\web\NotFoundHttpException('Error de base de datos.', 500);
                }
            } else {
                // Si la carga o la validación fallan, retornar un mensaje de error

                       // Si la carga o la validación fallan, retornar un mensaje de error
                       $error=ErrorModelValidate::convertError([ActiveForm::validate($model)]);
                       throw new NotFoundHttpException($error,400);
            }
        } else {
            $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
            'view'=>Seguridad::tienePermiso('view'),
            'create'=>Seguridad::tienePermiso('create'),
            'update'=>Seguridad::tienePermiso('update'),
            'delete'=>Seguridad::tienePermiso('delete'),
            'export'=>Seguridad::tienePermiso('export'),
            'exportall'=>Seguridad::tienePermiso('exportall'),
            'select'=>Seguridad::tienePermiso('select'),
            'list'=>Seguridad::tienePermiso('list'),
            'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
            
];

            return $this->render('index', [
                'model' => $model,
                'modelId'=>null,
                'permisos'=>$permisos,

            ]);
        }
}

public function actionIndexinternacion()
{
    if(Seguridad::tienePermiso('index')){
    $request = Yii::$app->request;
    $model = new Internacion();    
    $searchModel = new InternacionSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $columnas=Metodos::obtenerColumnas($model,'internacion/internacion/indexinternacion');
    $permisos = [
        'index' => Seguridad::tienePermiso('index'),
        'view' => Seguridad::tienePermiso('view'),
        'create' => Seguridad::tienePermiso('create'),
        'update' => Seguridad::tienePermiso('update'),
        'delete' => Seguridad::tienePermiso('delete'),
        'export' => Seguridad::tienePermiso('export'),
        'select' => Seguridad::tienePermiso('select'),
        'list' => Seguridad::tienePermiso('list'),
        'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
        'viewdetalle' => Seguridad::tienePermiso('viewdetalle'),
    ];
    return $this->render('indexinternacion', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => $columnas,
        'permisos' => $permisos,
    ]);
}
}

public function actionUpdateinternacion($id)
{
    $request = Yii::$app->request;
    $model = $this->findModel($id);

    if ($request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        // Preparar datos relacionados
        $listaEstablecimiento = $this->getEntityList(Establecimiento::class, $model->id_establecimiento, 'nombre');
        $listaDiagnostico = $this->getEntityList(Diagnostico::class, $model->id_diagnostico, function ($item) {
            return $item->codigo . ' | ' . $item->nombre;
        });

        $parametroPaciente = $model->id_paciente ? Paciente::findOne($model->id_paciente) : new Paciente();
        $listaPaciente = $parametroPaciente->id ? [
            $parametroPaciente->id => "{$parametroPaciente->nombre} | {$parametroPaciente->apellido} | {$parametroPaciente->numero_documento}"
        ] : null;

        $causaExterna = CausaExterna::findOne($model->id_causa_externa) ?? new CausaExterna();
        
            $sistemaEducativo = SistemaEducativo::findOne($parametroPaciente->id_sistema_educativo) ?? new SistemaEducativo();
       
 
        $embarazo = $this->isEmbarazoConditionMet($model, $listaDiagnostico) ? Embarazo::findOne($model->id_embarazo) : new Embarazo();

        if ($request->isPost) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $errors = [];
                $errors = $this->validateFieldsInternacion($request, ['fecha_egreso', 'fecha_ingreso', 'total_dias_estadia', 'id_establecimiento', 'id_diagnostico', 'id_sector', 'id_lugar_ingreso', 'id_tipo_egreso'], $errors);
                $this->validateFieldsSistemaEducativo($request, ['id_asistio'], $errors);

                $model->load($request->post(), 'Internacion');
                $causaExterna->load($request->post());
                $sistemaEducativo->load($request->post());

                if ($causaExterna->validate()) {
                    $causaExterna->save();
                    $model->id_causa_externa = $causaExterna->id;
                }

                if ($sistemaEducativo->validate()) {
                    $parametroPaciente->id_sistema_educativo = $sistemaEducativo->elegirId();
                    if ($parametroPaciente->validate()) {
                        $parametroPaciente->save();
                    }
                }

                if ($embarazo !== null && $embarazo->validate()) {
                    $embarazo->save();
                }

                if ($model->validate()) {
                    $model->estado = "FINALIZADO";
                    $model->save();
                }

                $transaction->commit();

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Internacion #$id",
                    'content' => '<span class="text-success">Registro guardado correctamente</span>',
                ];
            } catch (\Throwable $e) {
                $transaction->rollBack();
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de base de datos: ' . $e->getMessage(), 400);
            }
        }

        $isEmbarazo = $embarazo->getAttributes()['id'] != null ? true : false;

        return [
            'title' => "Editar Internacion #$id",
            'content' => $this->renderAjax('_forminternacion', [
                'model' => $model,
                'listaEstablecimiento' => $listaEstablecimiento,
                'listaDiagnostico' => $listaDiagnostico,
                'listaPaciente' => $listaPaciente,
                'causaExterna' => $causaExterna,
                'sistemaEducativo' => $sistemaEducativo,
                'paciente' => $parametroPaciente,
                'embarazo' => $embarazo,
                'isEmbarazo' => $isEmbarazo
            ]),
            'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"]),
        ];
    }

    return $this->redirect(['index']);
}


// Helper methods for better code organization
private function getEntityList($class, $id, $nameField)
{
    $entity = $class::findOne($id);
    return $entity ? [$entity->id => is_callable($nameField) ? $nameField($entity) : $entity->$nameField] : [];
}

private function isEmbarazoConditionMet($model, $listaDiagnostico)
{
    $parametroDiagnostico = array_key_first($listaDiagnostico);
    if($parametroDiagnostico!=null && strpos($listaDiagnostico[$parametroDiagnostico], 'O') === 0)
        return true;
}

private function validateFieldsInternacion($request, $fields)
{
    $error=false;
    $firstPart="";
    foreach ($fields as $field) {
        if ($request->getBodyParam("Internacion")[$field] == null) {

            
            
            // Extrae la parte después de "id_"
            $firstPart.= strpos($field, 'id_') !== false ? explode("id_", $field)[1] : $field;
            $firstPart .= ", ";
            $error=true;
        }

    }

    return ['error'=>$error
    ,'textError'=>$firstPart];
}

private function validateFieldsSistemaEducativo($request, $fields,$errors)
{
    $firstPart=$errors['textError'];
    $isError=$errors['error'];
    foreach ($fields as $field) {
        if ($request->getBodyParam("SistemaEducativo")[$field] == null) {
            // Extrae la parte después de "id_"
            $firstPart .= strpos($field, 'id_') !== false ? explode("id_", $field)[1] : $field;
           $firstPart .=", ";
        $isError=true;
  
        }

    }
    if($isError){
    Yii::$app->response->format = Response::FORMAT_HTML;
    throw new NotFoundHttpException("Falta agregar: $firstPart", 400);
    }
}


public function actionIndexdetallediagnostico($id_internacion = null)
{
    $request = Yii::$app->request;
    if ($request->isAjax) {
        $permisos = [
            'indexdetalle' => Seguridad::tienePermiso('indexdetalle'),
        ];
        if (isset($id_internacion) || isset($_POST['expandRowKey'])) {
            if (!isset($id_internacion)) {
                $id_internacion = $_POST['expandRowKey'];
            }
            $searchModelDiagnostico = new DiagnosticoObservacionSearch();
            $dataProvider = $searchModelDiagnostico->search(Yii::$app->request->queryParams, $id_internacion);
            $dataProvider->setPagination(false);
            $searchModelCirugia = new CirugiaSearch();
            $dataProviderCirugia = $searchModelCirugia->search(Yii::$app->request->queryParams, $id_internacion);
            $dataProviderCirugia->setPagination(false);
            $searchModelPase = new PaseInternacionSearch();
            $dataProviderPase = $searchModelPase->search(Yii::$app->request->queryParams, $id_internacion);
            $dataProviderPase->setPagination(false);
            $searchModelObstetricia = new ObstetriciaSearch();
            $dataProviderObstetricia = $searchModelObstetricia->search(Yii::$app->request->queryParams, $id_internacion);
            $dataProviderObstetricia->setPagination(false);
            $columnsDiagnostico = $searchModelDiagnostico->attributeColumns();
            $columnsCirugia = $searchModelCirugia->attributeColumns();
            $columnsPase = $searchModelPase->attributeColumns();
            $columnsObstetricia= $searchModelObstetricia->attributeColumns();
            return $this->renderPartial('indexdetallediagnostico', [
                'dataProviderCirugia' => $dataProviderCirugia,
                'dataProvider' => $dataProvider,
                'dataProviderPase' => $dataProviderPase,
                'dataProviderObstetricia' => $dataProviderObstetricia,
                'columnsDiagnostico' => $columnsDiagnostico,
                'columnsCirugia' => $columnsCirugia,
                'columnsPase' => $columnsPase,
                'columnsObstetricia'=>$columnsObstetricia,
                'permisos' => $permisos,
            ]);
        } else {
            return '<div>No se encontraron resultados</div>';
        }
    } else {
        return $this->redirect(['index']);
    }
}


public function actionDeletedetalle($id = null,$model=null)
{
    $request = Yii::$app->request;

        $model = call_user_func([$model, 'findOne'], $id);

        if($model instanceof PaseInternacion){
            $model = PaseInternacion::findOne($id);
            $crudReload='#crud-datatable-pase-pjax';
        }
        if($model instanceof DiagnosticoObservacion){
            $model = DiagnosticoObservacion::findOne($id);
            $crudReload='#crud-datatable-diagnostico-pjax';
        }
        if($model instanceof Obstetricia){
            $model = Obstetricia::findOne($id);
            $crudReload='#crud-datatable-obstetricia-pjax';
    
        }
        if($model instanceof Cirugia){
            $model = Cirugia::findOne($id);
            $crudReload='#crud-datatable-cirugia-pjax';
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($model->delete()){
                    try {
                    } catch (\Exception $e) {
                        Yii::error("Error al querer borrar el registro" );
                    }
                    return ['forceReload' => $crudReload,'forceClose'=>true,];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de base de datos.',500);
            }
        }else{
            return $this->redirect(['index']);
        }
}


    public function actionCreate($accion=null,$modelId=null){
        $request = Yii::$app->request;
        $model=null;
        $contacto=new Contacto();

        $embarazo=new Embarazo();
        $sistemaEducativo=new SistemaEducativo();
        $causaExterna=new CausaExterna();
        $situacionLaboral=new SituacionLaboral();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $rawBody = $request->getRawBody();
            //var_dump($request->queryParams); // Muestra los parámetros recibidos en la URL
            $data = json_decode($rawBody, true);
            if ($modelId == null || $modelId=="null") {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                Yii::$app->response->statusCode = 400; // Código de error HTTP
                return [
                    'success' => false,
                    'error' => 'Error faltan datos para poder finalizar la internacion.',
                ];
                
            } else {
                $model = Internacion::findOne($modelId);
            }

            $paciente=$model->id_paciente!=null?Paciente::findOne($model->id_paciente):new Paciente();
            if($model->id_paciente==null){
                $paciente->load($data);
                $paciente=Paciente::findOne($data['Paciente']['id']);
            }
         

            $embarazo->load($data);
            $contacto->load($data);
            $sistemaEducativo->load($data);
            $causaExterna->load($data);
            $situacionLaboral->load($data);
            if(!($model->validate() && $paciente->validate() && $contacto->validate() && $embarazo->validate() &&  $sistemaEducativo->validate() && $causaExterna->validate() && $situacionLaboral->validate())){

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                Yii::$app->response->statusCode = 400; // Código de error HTTP
                return [
                    'success' => false,
                    'error' => 'Error faltan datos para poder finalizar la internacion.',
                ];
            }
            if($data['isCreate']){
                try {
            $model->estado="FINALIZADO";
            if ($embarazo->attributes != null) {
                if ($embarazo->validate()) {
                    if ($embarazo->save()) {
                        // Éxito al guardar
                    } else {
                        // Error al guardar (problemas con la base de datos)
                        Yii::error('Error al guardar el modelo embarazo: ' . json_encode($embarazo->errors));
                    }
                } else {
                    // Error de validación
                    Yii::error('Errores de validación: ' . json_encode($embarazo->errors));
                }
            }
                
              $causaExterna->save();
              $model->id_embarazo=$embarazo->id;
         
              $model->id_causa_externa=$causaExterna->id;


                if ($model->save()) {

                    return ['title' => '<span class="text-success">Se realizo la carga correctamente.</span>',
                    'content' => '<span class="text-success">Se cargo correctamente.</span>',
                    'internacion'=>$model,"response"=>"success"];
                
                }
            } catch (yii\db\Exception $e) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de base de datos.', 500);
            }
        
            }
            else{
            return [
                'content' => $this->renderAjax('_create', [
                    'model' => $model,
                    'embarazo'=>$embarazo,
                    'sistemaEducativo'=>$sistemaEducativo,
                    'causaExterna'=>$causaExterna,
                    'situacionLaboral'=>$situacionLaboral,
                    'paciente'=>$paciente,
                    'contacto'=>$contacto
                ]),
                'title' => "Internacion " . $model->id,
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
              Html::button('Finalizar internacion', [
                    'id' => 'button-internacion',
                    'class' => 'btn btn-primary',
                    'type' => 'button',
                    'onclick' => 'finalizarInternacion()'
              ])
            ];
        }
    }
         else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
    /**
    *
    *@throws NotFoundHttpException if the model cannot be found
    */
     public function actionIndexdiagnostico(){

        $request = Yii::$app->request;
        $modelId=null;
        $modelId=intval($request->get('modelId'));
        $diagnostico=new Diagnostico();
        $searchModel=new DiagnosticoObservacionSearch();
        $diagnosticoObservacion=new DiagnosticoObservacion();
        $cirugia= new Cirugia();
        $cirugiaSearch=new CirugiaSearch();
        $model=new Internacion();
        $cirugiaProvider= $cirugiaSearch->search(Yii::$app->request->queryParams,$modelId);
        $causaExterna=new CausaExterna();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$modelId);
        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
        'view'=>Seguridad::tienePermiso('view'),
        'create'=>Seguridad::tienePermiso('create'),
        'update'=>Seguridad::tienePermiso('update'),
        'delete'=>Seguridad::tienePermiso('delete'),
        'export'=>Seguridad::tienePermiso('export'),
        'select'=>Seguridad::tienePermiso('select'),
        'list'=>Seguridad::tienePermiso('list'),
    ];$columnsCirugia=Metodos::obtenerColumnas($cirugia,'internacion/internacion/indexdiagnostico');
    $columns=Metodos::obtenerColumnas($diagnosticoObservacion,'internacion/internacion/indexdiagnostico');

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($modelId==null)
                return $this->redirect(['index'],500)->send();
       
            $model = Internacion::findOne($modelId);

            if($request->post()){
              

                try {
                    $model->id_diagnostico=intval(Yii::$app->request->getBodyParam("Internacion")['id_diagnostico']);
                    $causaExterna->nombre=Yii::$app->request->getBodyParam("CausaExterna")['nombre'];
                    $causaExterna->como_se_produjo=Yii::$app->request->getBodyParam("CausaExterna")['como_se_produjo'];
                    $causaExterna->id_lugar=Yii::$app->request->getBodyParam("CausaExterna")['id_lugar'];
                    $causaExterna->id_producido_por=Yii::$app->request->getBodyParam("CausaExterna")['id_producido_por'];

                    if($model->id_diagnostico!=null && $causaExterna->validate()){
                    
                        $model->save();
                        
                        $model->refresh();
                    return ['title' => '<span class="text-success">Se realizo la carga correctamente.</span>',
                    'content' => '<span class="text-success">Se cargo correctamente.</span>',
                    'internacion'=>$model,'causaExterna'=>$causaExterna,'response'=>'success'];
                }
                else{
                    $arrayError = array(
                        array("El diagnostico se encuentra vacio"),
                        array()
                      );
                    $error=ErrorModelValidate::convertError([$arrayError,ActiveForm::validate($causaExterna)]);
                    throw new NotFoundHttpException($error,400);
            }
             } catch (\yii\db\Exception $e) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.', 500);
                }
            }
        
    
            return $this->render('indexdiagnostico', [
                'searchModel' => $searchModel,
                'diagnostico' => $diagnostico,
                'causaExterna'=>$causaExterna,
                'columns'=>$columns,
                'cirugia'=>$cirugia,
                'model' => $model,
                'columnsCirugia'=>$columnsCirugia,
                'cirugiaSearch'=>$cirugiaSearch,
                'dataProvider'=>$dataProvider,
                'permisos'=>$permisos,
                'cirugiaProvider'=>$cirugiaProvider,
                'diagnosticoObservacion'=>$diagnosticoObservacion,
                'modelId'=>$modelId
            ]);
        }
        else{
            return $this->redirect(['index']);
        }
        
       }

       public function actionCreateobstetricia($modelId=null)
       {
        $request = Yii::$app->request;
        $model = new Obstetricia();
        $internacion = new Internacion();
    
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($modelId!=null)
            $internacion = Internacion::findOne($modelId);
            if($internacion==null){
                return $this->redirect(['index'],500)->send();
            }
            if ($model->load($request->post())) {
                $cantidadBebes=count($model->find()->where(['id_internacion'=>$modelId])->all());
                if($cantidadBebes>4){
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de cantidad de pases, no se pueden agregar mas.', 500);
                }
                $model->id_internacion=$modelId;
              
                try {
                    if ($model->save()) {
                        return [
                            
                            'forceReload'=>'#crud-datatable-obstetricia-pjax',
                            'title' => "obstetricia #" . $model->id,
                            'content' => '<span class="text-success">Registro guardado correctamente</span>',
                        ];
                    }
                } catch (yii\db\Exception $e) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.', 500);
                }
            }
    
            return [
                
                'title' => "Embarazo(Opcional)",
                'content' => $this->renderAjax('_formembarazo', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        } else {
            return $this->render('index', [
                'model' => $internacion,
            ]);
        }
       }
   
       public function actionIndexembarazo(){
        $request = Yii::$app->request;
        $modelId=null;
        $modelId=intval($request->get('modelId'));
    
        $embarazo=new Embarazo();
        $internacion=null;
        $obstetricia=new Obstetricia();
        $searchModel = new ObstetriciaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$modelId);
        $permisos=
        ['index'=>Seguridad::tienePermiso('index'),
        'view'=>Seguridad::tienePermiso('view'),
        'create'=>Seguridad::tienePermiso('create'),
        'update'=>Seguridad::tienePermiso('update'),
        'delete'=>Seguridad::tienePermiso('delete'),
        'export'=>Seguridad::tienePermiso('export'),
        'select'=>Seguridad::tienePermiso('select'),
        'list'=>Seguridad::tienePermiso('list')];
        $columnas=Metodos::obtenerColumnas($obstetricia,'internacion/internacion/indexembarazo');
     

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
     
            if($modelId==null || $modelId=='')
            return $this->redirect(['index'],500)->send();
            $internacion = Internacion::findOne($modelId); 
            if($internacion->id_diagnostico==null || empty(Diagnostico::find()
            ->where(['id' => $internacion->id_diagnostico])
            ->andWhere(['like', 'codigo', 'O%', false])
            ->one()
        )){
                return [
                    'response'=>'error',
                    'content' => '<span class="text-danger">Error el diagnostico generado no corresponde a el embarazo.</span>',
                    'title' => '<span class="text-danger">No se asigno el diagnostico adecuado.</span>',
                ];
            }
            if($embarazo->load($request->post())){
                try {
                    if($embarazo->validate()){
                    return ['title' => '<span class="text-success">Se realizo la carga correctamente.</span>',
                    'content' => '<span class="text-success">Se cargo correctamente.</span>',
                    'embarazo'=>$embarazo,'response'=>'success'];
                }
                else{
                    $error=ErrorModelValidate::convertError([ActiveForm::validate($embarazo)]);
                    throw new NotFoundHttpException($error,400);
                }
                    
             } catch (\yii\db\Exception $e) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new \yii\web\NotFoundHttpException('Error de base de datos.', 500);
                }
            }

            return $this->render('indexembarazo', [
                'searchModel' => $searchModel,
                'embarazo' => $embarazo,
                'internacion'=>$internacion,
                'columns'=>$columnas,
                'obstetricia'=>$obstetricia,
                'dataProvider'=>$dataProvider,
                'permisos'=>$permisos,
                'modelId'=>$modelId
            ]);
         
        }
        else{
            return $this->redirect(['index']);
            }
       }
    /**
     * Finds the Internacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @throws NotFoundHttpException if the model cannot be found
     */
     public function actionIndexpase(){
        $request = Yii::$app->request;
        $modelId=null;
        $modelId=intval($request->get('modelId'));

        $model=new PaseInternacion();
        $searchModel = new PaseInternacionSearch();
        $internacion=new Internacion();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$modelId);
        $permisos=[ 'index'=>Seguridad::tienePermiso('index'),
        'view'=>Seguridad::tienePermiso('view'),
        'create'=>Seguridad::tienePermiso('create'),
        'update'=>Seguridad::tienePermiso('update'),
        'delete'=>Seguridad::tienePermiso('delete'),
        'export'=>Seguridad::tienePermiso('export'),
        'select'=>Seguridad::tienePermiso('select'),
        'list'=>Seguridad::tienePermiso('list'),
        ];      
        $columnas=Metodos::obtenerColumnas($model,'internacion/internacion/indexpase');
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($modelId==null)
                return $this->redirect(['index'],500)->send();
            Yii::beginProfile('findInternacion');
            $internacion = Internacion::findOne($modelId);
            Yii::endProfile('findInternacion');
      
            if( $request->post()){
                
               $internacion->id_tipo_egreso=Yii::$app->request->getBodyParam("Internacion")['id_tipo_egreso'];
                $internacion->fecha_egreso=Yii::$app->request->getBodyParam("Internacion")['fecha_egreso'];
                $internacion->total_dias_estadia=Yii::$app->request->getBodyParam("Internacion")['total_dias_estadia'];
                $internacion->id_diagnostico=-1;
    
                try {
                if($internacion->id_tipo_egreso!=null && $internacion->fecha_egreso!=null && $internacion->total_dias_estadia!=null){
                    
                    $internacion->save();
        
                return [
                    'internacion'=>$internacion,
                    'content' => '<span class="text-success">Se cargo correctamente.</span>',
                    'response'=>'success'];
            }
            else{
                $arrayError = array(
                    array(),
                    array( ),
                    array( ),
                   );
                if($internacion->id_tipo_egreso==null){
                    $arrayError[0][0]= array("Tipo de egreso se encuentra vacio");
                }
                if($internacion->fecha_egreso==null){
                    $arrayError[0][1]= array("fecha ingreso se encuentra vacio");
                }
                if($internacion->total_dias_estadia==null){
                    $arrayError[0][2]= array("total dias estadia se encuentra vacio");
                }
                $error=ErrorModelValidate::convertError($arrayError);
                throw new NotFoundHttpException($error,400);
            }
            }
             catch (\yii\db\Exception $e) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.', 500);
                }
            
            }
            return $this->render('indexpase', [
                'columns'=>$columnas,
                'searchModel' => $searchModel,
                'internacion' => $internacion,
                'model' => $model,
                'dataProvider'=>$dataProvider,
                'permisos'=>$permisos,
                'modelId'=>$modelId

            ]);
      
     }
     else{
        return $this->redirect(['index']);
     }
    }
    public function actionListocupacion($q = null)
{
    Yii::$app->response->format = Response::FORMAT_JSON;

    if ($q === null || trim($q) === '') {
        // Si no hay término de búsqueda, devolver todos los resultados
        $query = Ocupacion::find()->select(['id', 'nombre'])->asArray()->all();
    } else {
        // Si hay término de búsqueda, filtrar los resultados
        $query = Ocupacion::find()
            ->select(['id', 'nombre'])
            ->where(['like', 'lower(nombre)', strtolower($q)])
            ->asArray()
            ->all();
    }

    $results = array_map(function($item) {
        return [
            'id' => $item['id'],
            'text' => $item['nombre'],
        ];
    }, $query);

    return ['results' => $results];
}

    public function actionBuscarpersona($param = null) {
        // Obtener el parámetro de la solicitud GET
        if ($param === null) {
            $param = Yii::$app->request->get('param');
        }

        $paciente=Paciente::findOne($param);
       $sistemaEducativo=SistemaEducativo::findOne($paciente->id_sistema_educativo);
        $situacionLaboral=SituacionLaboral::findOne(['id_paciente'=>$param]);
        // Aquí procesarías la lógica con el parámetro recibido

        return $this->asJson([
            'situacionLaboral'=> $situacionLaboral,
            'paciente' => $paciente,
            'sistemaEducativo' => $sistemaEducativo]);
    }
    public function actionUploadfile()
    {
        $model = new MultipleUploadForm();
        $request = Yii::$app->request;
    
        if ($request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');


     

                // Guardar el archivo en el servidor temporalmente
   


                /*if ($model->file->saveAs($tempFilePath)) {
                    // Usar el lector de CSV de PhpSpreadsheet
                    $reader = new Csv();  // Se puede utilizar IOFactory::createReaderForFile() si prefieres una opción más general
                    $reader->setDelimiter(','); // Asegúrate de que el delimitador sea correcto (normalmente es coma para CSV)
                    $spreadsheet = $reader->load($tempFilePath); // Carga el archivo CSV
    
                    // Obtener la primera hoja
                    $sheet = $spreadsheet->getActiveSheet();
    
                    // Leer los datos de la hoja
                    $rows = $sheet->toArray();  // Devuelve todas las filas de la hoja como un array
    
                    // Mostrar las filas leídas
                    foreach ($rows as $row) {
                        var_dump($row);  // Muestra cada fila
                    }
                }*/
            

   
                // Opción 1: Guardar el archivo en el sistema y almacenar la ruta en la base de datos
                $filePath = '../uploads/' . $model->file->getBaseName() . '.' . $model->file->getExtension();
                if ($model->file->saveAs($filePath)) {
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => Growl::TYPE_SUCCESS,
                        'icon' => 'fa fa-check',
                        'title' => '¡Éxito!',
                        'message' => 'Archivo subido correctamente.',
                        'showSeparator' => true,
                        'pluginOptions' => [
                            'delay' => 0,
                            'placement' => [
                                'from' => 'top',
                                'align' => 'right',
                            ],
                            'timer' => 2000,
                        ],
                    ]);
                    
                }
    
                // Opción 2: Guardar el archivo como binario en la base de datos
                // $model->file_content = file_get_contents($model->file->tempName);
                // $model->file_name = $model->file->baseName . '.' . $model->file->extension;
                // $model->save(false);
                // Yii::$app->session->setFlash('success', 'Archivo subido y guardado en la base de datos correctamente.');
         
    
            return $this->redirect(['uploadfile']); // Redirige a la misma página o a otra según el flujo deseado
        }

        return 
          $this->render('upload', [
            'model' => $model]);
    }
    
        

   
    public function actionFormprincipal()
{
    $request = Yii::$app->request;
    $activarDniAjeno = false;
    $model = new Paciente();
    $internacionModel = new Internacion();
    $contacto = new Contacto();
    $modelId = null;
    $modelId = $request->get('modelId');
    $pacienteId = $request->get('pacienteId');
    $sistemaEducativo = new SistemaEducativo();
    $localidad = new Localidad();
    $situacionLaboral = new SituacionLaboral(); // Este va referenciado a id paciente cuando se crea

    if ($request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($modelId == null) {
            return $this->redirect(['index'], 500)->send();
        }

        $internacionModel = Internacion::findOne($modelId);
        if ($pacienteId != '') {
            $internacionModel->id_paciente = $pacienteId;
        }

        if ($internacionModel->id_lugar_ingreso == 5) {
            $activarDniAjeno = true;
        }

        if ($model->load($request->post())) {
            if ($internacionModel->id_diagnostico == null) {
                $internacionModel->id_diagnostico = -1;
            }

            $sistemaEducativo->load($request->post());
      

            $situacionLaboral->load($request->post());
            $internacionRequest = Yii::$app->request->getBodyParam("Internacion");

            if ($internacionRequest && isset($internacionRequest['id_paciente'])
                && $internacionRequest['id_paciente'] != null) {
                $idPaciente = intval($internacionRequest['id_paciente']);
                $model = Paciente::findOne($idPaciente);
                $internacionModel->id_paciente = $idPaciente;
            }

            $model->numero_documento = (string)$model->numero_documento;
            if ($sistemaEducativo->validate()) {
                $model->id_sistema_educativo = $sistemaEducativo->elegirId();
            }

            // Iniciar una transacción
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->validate() && $situacionLaboral->validate()) {
                    $contacto->numero_telefono = trim(Yii::$app->request->getBodyParam('Contacto')['numero_telefono']);
                    $model->is_internacion = true;
            
                    // Guardar contacto
                    if (!$contacto->save()) {
                        throw new \Exception('Error al guardar Contacto: ' . Json::encode($contacto->errors));
                    }
            
                    $model->id_contacto = $contacto->id;
            
                    // Guardar paciente
                    if (!$model->save()) {
                        throw new \Exception('Error al guardar PACIENTE: ' . Json::encode($model->errors));
                    }
            
                    $internacionModel->id_paciente=$model->id;
                    if (!$internacionModel->save()) {
                        throw new \Exception('Error al guardar Internacion: ' . Json::encode($internacionModel->errors));
                    }
            
                    // Guardar situación laboral
                    $situacionLaboral->id_paciente = $model->id;
                    if (!$situacionLaboral->save()) {
                        throw new \Exception('Error al guardar SituacionLaboral: ' . Json::encode($situacionLaboral->errors));
                    }
            
                    // Si todo sale bien, confirmar la transacción
                    $transaction->commit();
            
                    return [
                        'internacion' => $internacionModel,
                        'paciente' => $model,
                        'content' => '<span class="text-success">Se cargó correctamente.</span>',
                        'sistemaEducativo' => $sistemaEducativo,
                        'situacionLaboral' => $situacionLaboral,
                        'response' => 'success',
                        'contacto' => $contacto,
                    ];
                } else {
                    $error = ErrorModelValidate::convertError([ActiveForm::validate($contacto), ActiveForm::validate($model), ActiveForm::validate($sistemaEducativo), ActiveForm::validate($situacionLaboral)]);
                    throw new \Exception($error);
                }
            } catch (\Exception $e) {
                // Si algo sale mal, hacer rollback y lanzar excepción
                $transaction->rollBack();
                Yii::$app->response->format = Response::FORMAT_JSON;

                throw new \Exception($e->getMessage());
            }
            
        }

        return $this->render('_formprincipal', [
            'model' => $model,
            'internacion' => $internacionModel,
            'activarDniAjeno' => $activarDniAjeno,
            'sistemaEducativo' => $sistemaEducativo,
            'situacionLaboral' => $situacionLaboral,
            'modelId' => $modelId,
            'contacto' => $contacto,
            'localidad' => $localidad,
        ]);
    } else {
        return $this->redirect(['index']);
    }
}

    /**
     * Displays a single Internacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$model)
    {   
        $request = Yii::$app->request;
        $model = call_user_func([$model, 'findOne'], $id);
        if($model instanceof PaseInternacion){
            $model = PaseInternacion::findOne($id);
        }
        if($model instanceof DiagnosticoObservacion){
            $model = DiagnosticoObservacion::findOne($id);
        }
        if($model instanceof Obstetricia){
            $model = Obstetricia::findOne($id);

        }
        if($model instanceof Cirugia){
            $model = Cirugia::findOne($id);

        }
        if($model instanceof Internacion){
            $model = Internacion::findOne($id);

        }
        $attributes = $model->attributeView();
        // soporte de enter para el caso de un campo textarea para view y pdf. por ejemplo campo observaciones.
        // en el $model->attributeView, usamos formato html (campo_textarea:html) y reemplazo enter por <br> en el modelo
        // $model->(campo_textarea)=str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"),"<br>",$model->(campo_texarea);

        if(isset($_GET['pdf'])){

            $titulo='Datos de Internacion';

            \app\components\Viewpdf\Viewpdf::widget(array(
                'titulo'=>$titulo,
                'subtitulo'=> "Internacion #".$id,
                'data'=>$model,
                'attributes'=>$attributes,
                'resumen'=>'',
            ));

            $fileName = preg_replace ("/ /","_",$titulo).".pdf" ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Internacion #".$id,
                'content'=>$this->renderAjax('@app/components/Vistas/_view', [
                    'model' => $model,
                    'attributes' => $attributes,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    (Seguridad::tienePermiso('view')?Html::a('Imprimir',['view','id'=>$id,'pdf'=>true],['class'=>'btn btn-primary','target'=>'exportFrame']):"").
                    (Seguridad::tienePermiso('update')?Html::a('Editar',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote']):"").
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }


   
   public function actionCreatediagnostico($accion=null,$modelId=null)
    {
        $request = Yii::$app->request;
        $model = new DiagnosticoObservacion();  
     
      
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            
            if($model->load($request->post())){
                $model->id_internacion=$modelId;
                try {
                   
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-diagnostico-pjax',
                            'title'=> "pase diagnostico #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }
           
            return [
                
                'title'=> "Diagnostico principal(Opcional)",
                'content'=>$this->renderAjax('_formdiagnostico', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['indexdiagnostico']);
        }
    
    }

    public function actionCreatecirugia($accion=null,$modelId=null)
    {
        $request = Yii::$app->request;
        $model = new Cirugia();  
       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            if($model->load($request->post())){
                $model->id_internacion=$modelId;
                try {
                   
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>'#crud-datatable-cirugia-pjax',
                            'title'=> "pase cirugia #".$model->id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }

            return [
                
                'title'=> "Diagnostico(Opcional)",
                'content'=>$this->renderAjax('_formcirugia', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['indexdiagnostico']);
        }
    }

    public function actionCreatepase($accion,$modelId=null)
    {
        $request = Yii::$app->request;
        $model = new PaseInternacion();
        $internacion=new Internacion();
        

        if ($request->isAjax) {
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $maxPase=$model->find()->where(['id_internacion'=>$modelId])->max('numero_pase');
            $cantidadPases=count($model->find()->where(['id_internacion'=>$modelId])->all());

            if($cantidadPases>4){
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de cantidad de pases, no se pueden agregar mas.', 500);
            }
            if ($model->load($request->post())) {

                $model->numero_pase=$maxPase+1;
               $model->id_internacion=$modelId;
                try {
                    if ($model->save()) {
                        $content = '<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            
                            'forceReload'=>'#crud-datatable-pase-pjax',
                            'title' => "Pase creacion  #" . $model->id,
                            'content' => $content,
                        ];
                    }
                } catch (yii\db\Exception $e) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.', 500);
                }
            }
    
            return [
                
                'title' => "Pase",
                'content' => $this->renderAjax('_formpase', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Cancelar', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Guardar', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        } else {
            return $this->render('index', [
                'model' => $internacion,
            ]);
        }
    }


    /**
     * Updates an existing Internacion model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$model)
    {
        $request = Yii::$app->request;
        $form="";
        $model = call_user_func([$model, 'findOne'], $id);
        if($model instanceof PaseInternacion){
            $model = PaseInternacion::findOne($id);
            $form="_formpase";
            $crudReload='#crud-datatable-pase-pjax';
        }
        if($model instanceof DiagnosticoObservacion){
            $model = DiagnosticoObservacion::findOne($id);
            $crudReload='#crud-datatable-diagnostico-pjax';
            $form="_formdiagnostico";
        }
        if($model instanceof Obstetricia){
            $model = Obstetricia::findOne($id);
            $crudReload='#crud-datatable-embarazo-pjax';
            $form="_formembarazo";
        }
        if($model instanceof Cirugia){
            $model = Cirugia::findOne($id);
            $crudReload='#crud-datatable-cirugia-pjax';
            $form="_formcirugia";
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post())){
                try {
                    if( $model->save() ){
                        $content='<span class="text-success">Registro guardado correctamente</span>';
                        return [
                            'forceReload'=>$crudReload,
                            'title'=> "Internacion #".$id,
                            'content'=>$content,
                        ];
                    }
                } catch (yii\db\Exception $e ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;
                    throw new NotFoundHttpException('Error de base de datos.',500);
                }
            }

            return [
                'title'=> "Editar Internacion #".$id,
                'content'=>$this->renderAjax($form, [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];

        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing Internacion model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */


    public function actionDelete($id,$model)
    {
        $request = Yii::$app->request;
        $model = call_user_func([$model, 'findOne'], $id);
    
        if($model instanceof PaseInternacion){
            $model = PaseInternacion::findOne($id);
            $crudReload='#crud-datatable-pase-pjax';
        }
        if($model instanceof DiagnosticoObservacion){
            $model = DiagnosticoObservacion::findOne($id);
            $crudReload='#crud-datatable-diagnostico-pjax';
        }
        if($model instanceof Obstetricia){
            $model = Obstetricia::findOne($id);
            $crudReload='#crud-datatable-embarazo-pjax';
        }
        if($model instanceof Cirugia){
            $model = Cirugia::findOne($id);
            $crudReload='#crud-datatable-cirugia-pjax';
        }
        if($model instanceof Internacion){
            $model = Internacion::findOne($id);
            $crudReload='#crud-datatable-pjax';
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            try {
                if ($model->delete()){
                    try {
                    } catch (\Exception $e) {
                        Yii::error("Error al abrir la sesión: " . $e->getMessage());
                    }
                    return ['forceReload' => $crudReload,'forceClose'=>true,];
                }
            } catch (yii\db\Exception $e ) {
                Yii::$app->response->format = Response::FORMAT_HTML;
                throw new NotFoundHttpException('Error de base de datos.',500);
            }
        }else{
            return $this->redirect(['index']);
        }
    }
    /**
     * Finds the Internacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Internacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
                if (($model = Internacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Registro no encontrado.',404);
        }
    }

    /**
     * Export Data segun la vista del grid del usuario. model REPORTE
     * For ajax request will return json object
     * and for non-ajax request process the export
     * @param model $Reporte, integer tipo_archivo
     * @return mixed
     */
    public function actionExport($accion=null,$model)
    {
        $request = Yii::$app->request;
        $reporte= new Reporte();
        if(isset($_POST['Reporte']) && isset($_POST['tipo_archivo']) )
        {
            $reporte->attributes=$_POST['Reporte'];
            $tipo_archivo=$_POST['tipo_archivo'];
            \app\components\Listado\Listado::widget(array(
                'reporte'=>$reporte,
                'tipo'=>$tipo_archivo
            ));
            $fileName = preg_replace ("/ /","_",$reporte->titulo).".".$tipo_archivo ;
            Yii::$app->response->sendFile('../runtime/'.$fileName)->send();
            Yii::$app->end();
        }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $dataProvider=Yii::$app->session->get($model.'-dataprovider');
            $reporte->setFromDataProvider($dataProvider,$accion);
            return [
                'title'=> "Exportar Datos",
                'content'=>$this->renderAjax('@app/components/Vistas/_export', [
                    'model' => $reporte,'tipo_archivo'=>'pdf','totalRegistros'=>$dataProvider->totalCount,]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Aceptar',['export','accion'=>$accion],['class'=>'btn btn-primary','target'=>'exportFrame','onclick'=>'document.getElementById("export-form").submit();']).
                    '<iframe src="" style="display:none" name="exportFrame"/>'
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    
    


    public function actionSelect($accion=null,$model)
    {

        $request = Yii::$app->request;
       if($model== "paseinternacion"){
           $model = new PaseInternacion();
           $crudReload='#crud-datatable-pase-pjax';
       }
       if($model== "diagnosticoobservacion"){
           $model = new DiagnosticoObservacion();
           $crudReload='#crud-datatable-diagnostico-pjax';
       }
       if($model == "obstetricia"){
           $model = new Obstetricia();
           $crudReload='#crud-datatable-obstetricia-pjax';
       }
       if($model== "cirugia"){
           $model = new Cirugia();
           $crudReload='#crud-datatable-cirugia-pjax';
       }
       if($model== "internacion"){
        $model = new Internacion();
        $crudReload='#crud-datatable-pjax';
    }
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(isset($_POST['seleccion'])){
                // recibo datos de lo seleccionado, reconstruyo columnas
                $seleccion=$_POST['seleccion'];
                $columnAdmin=$model->attributeColumns();
                $columnSearch=[];
                $columnas=[];
                foreach($columnAdmin as $value){
                    $columnSearch[]=$value['attribute'];
                }
                foreach($seleccion as $key) {
                    $indice=array_search($key, $columnSearch);
                    if ($indice!==null){
                        $columnas[]=$columnAdmin[$indice];
                    }
                }
                // guardo esa informacion, sin controles ni excepciones, no es importante
                $vista = Vista::findOne(['id_usuario'=>Yii::$app->user->id,'accion'=>$accion,'modelo'=>$model->classname()]);
                if($vista==null){
                    $vista = new Vista();
                    $vista->id_usuario=Yii::$app->user->id;
                    $vista->accion=$accion;
                    $vista->modelo=get_class($model);
                }
                $vista->columna=serialize($columnas);
                
                $vista->save();
                return ['forceClose'=>true,'forceReload'=>$crudReload];
            }
            $columnas=Metodos::obtenerColumnas($model,$accion);
            // attributos de las columnas mostradas
            $seleccion=Metodos::obtenerAttributosColumnas($columnas);
 
            // todas las etiquetas
            $etiquetas=Metodos::obtenerEtiquetasColumnas($model,$seleccion);
            return [
                'title'=> "Personalizar Lista",
                'content'=>$this->renderAjax('@app/components/Vistas/_select', [
                    'seleccion' => $seleccion,
                    'etiquetas' => $etiquetas,
                ]),
                'footer'=> Html::button('Cancelar',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Guardar',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }
   
    /**
     * Accion List, utilizada cuando se hace una busqueda autocomplete de este modelo
     * en query->select , verificar el campo de  busqueda
     * @param string de busqueda $q
     * @return arrary de resultados $results
     */
    public function actionList($q = null)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $q="%".strtolower(addslashes($q))."%";
            $sql = "SELECT id as id, nombre as value
                    FROM internacion                    
                    WHERE lower(internacion.nombre) LIKE '$q'
                    LIMIT 50";

            $query= Yii::$app->db->createCommand($sql);
            $data = $query->queryAll();
            $results = array_values($data); 
            return $results;
            
        }else{
            // Process for non-ajax request
            return $this->redirect(['index']);
        }
    }

    public function actionObtenerlocalidad()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $id=$request->get("id_establecimiento");
       $establecimiento  = Establecimiento::findOne($id);
       if($establecimiento==null || empty($establecimiento->localidad->nombre)){
           return ['status'=>'error'];
       }
       return ['status'=>'ok',
       'localidad'=>$establecimiento->localidad->nombre];
    }
    public function actionObtenerlugares($param){

        Yii::$app->response->format = Response::FORMAT_JSON;
       

       $localidad  = Localidad::findOne($param);

       if($localidad==null || empty($localidad)){
           return ['status'=>'error'];
       }
       return ['status'=>'ok',
       'localidad'=>$localidad,
       'provincia'=>$localidad->provincia,
       'departamento'=>$localidad->departamento,
    ];
    }

    public function actionGetprovincia($param){
        Yii::$app->response->format = Response::FORMAT_JSON;

        $localidad=Localidad::find()->where(['id'=>$param])->one();
        if(isset($localidad))
        return ['status'=>'ok',
        'idProvincia'=>$localidad->id_provincia];

        return null;

    }

 

}
