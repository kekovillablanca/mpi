<?php

namespace app\controllers;

use app\models\ChangePassword;
use app\models\FormRecoverPass;
use app\models\FormResetPass;
use yii\web\Session;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;
use app\modules\admin\controllers\UsuarioController;
use app\modules\admin\models\Usuario;
use app\modules\monitor\models\Monitor;
use app\modules\monitor\models\MonitorSearch;
use Exception;
use PHPMailer;
use PHPMailer\PHPMailer\PHPMailer as PHPMailerPHPMailer;
use PHPMailer\PHPMailer\SMTP;
use phpmailerException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Cookie;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\PostHttpException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
//            'error' => [
  //              'class' => 'yii\web\ErrorAction',
    //            ],
/*            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],*/
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    { 
   
        if (Yii::$app->user->isGuest) {
           
            return $this->redirect(['login']);
        }
        else if(!Yii::$app->user->isGuest && Yii::$app->session->get('estadouser')=="FECHAPASADA")
            return $this->redirect(['login']);

     
        
        return $this->render('index');
        
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    { 
      
     
        $this->layout='@app/views/layouts/login.php';
        $model = new LoginForm();
       
        if ($model->load(Yii::$app->request->post()) && $model->login() ) {
            if(Yii::$app->session->get('estadouser')=='FECHAPASADA'){
               return  $this->actionChangepassword();
            }
            else if(!Yii::$app->user->isGuest) {
               
                return $this->redirect(['index']);
            }
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionChangepassword(){
            $request = Yii::$app->request;
           $id= Yii::$app->user->identity->getId();
            $model=new Usuario();
            $changePassword=new ChangePassword();
            if($request->isAjax) { 
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($dato=$request->post()){
                   
                    $model=null;
                    $model=Usuario::findOne($id);
                    $passwordActual=$dato['Usuario']['password'];

                    $changePassword->pass_new=$dato['ChangePassword']['pass_new'];
                    $changePassword->pass_new_check=$dato['ChangePassword']['pass_new_check'];
               
                    if ($changePassword->pass_new!=$changePassword->pass_new_check || ($passwordActual== $changePassword->pass_new && $passwordActual != $changePassword->pass_new_check) || ($passwordActual!= $changePassword->pass_new && $passwordActual == $changePassword->pass_new_check) ||  ($passwordActual== $changePassword->pass_new && $passwordActual == $changePassword->pass_new_check)){
                        return ['status'=>'error',
                                'new_check'=>'check',
                                'new'=>'new'];
                    }
                    if($model->password!==md5($passwordActual)){
                        return ['status'=>'error',
                        'pass_ctrl'=>'ctrl'];

                    }
                        
               
                            try {
                                // cambiar solo contraseña
                                $model->password = $changePassword->pass_new;
                              
                                if ($model->save()) {

                                    Yii::$app->session->set('estadouser','OK');
                                    Yii::$app->session->setFlash('success', 'Contraseña cambiada correctamente');
                                    // Redirige al usuario a la página de inicio de sesión después de 5 segundos
                                    Yii::$app->response->redirect(['site/login'])->send();
                                    Yii::$app->end();

                                }
                            } catch (yii\db\Exception $e) {
                                Yii::$app->response->format = Response::FORMAT_HTML;
                                throw new NotFoundHttpException('Error en la base de datos.', 500);
                            }
                        }

                    
                   
              
                
            } 
            else{
                return $this->render('change', [
                    'model' => $model,
                    'change'=>$changePassword,
                ]);
            }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['index']);
    }

    
    public function actionError()
{
    $exception = Yii::$app->errorHandler->exception;

    if ($exception !== null) {

        
        if ($exception instanceof \yii\web\NotFoundHttpException) {
            return $this->renderPartial('error', ['exception' => $exception]);
        } elseif ($exception instanceof \yii\web\UnauthorizedHttpException) {
            return $this->renderPartial('error', ['exception' => $exception]);
        } elseif ($exception instanceof \yii\web\ForbiddenHttpException) {
            return $this->renderPartial('error', ['exception' => $exception]);
        } elseif ($exception instanceof \yii\web\ServerErrorHttpException) {
            return $this->renderPartial('error', ['exception' => $exception]);
        } elseif ($exception instanceof \yii\web\NotAcceptableHttpException) {
            return $this->renderPartial('error', ['exception' => $exception]);
        } else {
            return $this->renderPartial('error', ['exception' => $exception]);
        }
    }
}

    public function actionMonitor()
    {

        $this->layout = '@app/views/layouts/monitor.php';
        
        // Servidores
        $model= new Monitor();
        $searchModel = new MonitorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['estado' => SORT_ASC, 'host' => SORT_ASC];

        $columnas = [
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'categoria' ],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'host' ],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'service' ],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'ip' ] ,
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'puerto' ] ,
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'tipo_puerto','value'=>'tipo_puerto_nombre'],
            ['class' => '\kartik\grid\DataColumn', 'attribute' => 'tiempo' ] ,
        ];


        # bucardo status
        // $output = Shell_exec("cat status.txt");
        $output = Shell_exec("bucardo -U bucardo -P omega status");
        $bucardo_status = explode( PHP_EOL , $output );

        return $this->render('monitor', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns'=>$columnas,
            'bucardo_status'=>$bucardo_status,

        ]);

    }
    public function actionRecoverpass()
 {
  //Instancia para validar el formulario
  $model = new FormRecoverPass();
  if(!Yii::$app->user->getIsGuest()){
    return $this->redirect(["site/index"]);
  }
  //Mensaje que será mostrado al usuario en la vista
  $msg = null;
  
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {

    //Buscar al usuario a través del email
    $table = Usuario::find()->where(["email" => $model->email]);
    
    //Si el usuario existe
    if ($table->count())
    {
     //Crear variables de sesión para limitar el tiempo de restablecido del password
     //hasta que el navegador se cierre
     $session = new Session;
     $session->open();
     
     //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
     $session["recover"] = Yii::$app->security->generateRandomString(32);
     $recover = $session["recover"];
     
     //También almacenaremos el id del usuario en una variable de sesión
     //El id del usuario es requerido para generar la consulta a la tabla users y 
     //restablecer el password del usuario
     $table = Usuario::find()->where(["email" => $model->email])->one();
     
     $session["id_recover"] = $table->id;
     
     //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
     //para que lo introduzca en un campo del formulario de reseteado
     //Es guardada en el registro correspondiente de la tabla users
     $verification_code =Yii::$app->security->generateRandomString(32);
     //Columna verification_code
     $table->verification_code = $verification_code;
    
     //Guardamos los cambios en la tabla users    
   
    if ($table->validate() && $table->save()) {

        // Guardado exitoso
    
     //Creamos el mensaje que será enviado a la cuenta de correo del usuario
     $subject = "Recuperar password";
     $body = "<p>Copie el siguiente código de verificación para restablecer su password ... ";
     $body .= "<strong>".$verification_code."</strong></p>";
     $body .= "<p><a href='https://salud.rionegro.gov.ar/mpi/index.php?r=site%2Fresetpass' >Recuperar password</a></p>";

     //Enviamos el correo
     Yii::$app->mailer->compose()
     ->setTo($model->email)
     ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
     ->setSubject($subject)
     ->setHtmlBody($body)
     ->send();
     
     //Vaciar el campo del formulario
     $model->email = null;
     
     //Mostrar el mensaje al usuario
     $msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su password";
    }
    }
    else //El usuario no existe
    {
     $msg = "Ha ocurrido un error";
    }
   }
   else
   {
    $model->getErrors();
   }
  }
  return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
 }
 
 public function actionResetpass()
 {

  //Instancia para validar el formulario
  $model = new FormResetPass;
  
  //Mensaje que será mostrado al usuario
  $msg = null;
  
  //Abrimos la sesión
  $session = new Session;
  $session->open();
  
  //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
  if (empty($session["recover"]) || empty($session["id_recover"]) || !Yii::$app->user->getIsGuest())
  {
   return $this->redirect(["site/index"]);
  }
  else
  {
   
   $recover = $session["recover"];
   //El valor de esta variable de sesión la cargamos en el campo recover del formulario
   $model->recover = $recover;
   
   //Esta variable contiene el id del usuario que solicitó restablecer el password
   //La utilizaremos para realizar la consulta a la tabla users
   $id_recover = $session["id_recover"];
   
   
  }
  
  //Si el formulario es enviado para resetear el password
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {
    //Si el valor de la variable de sesión recover es correcta
    if ($recover == $model->recover)
    {
   
     //Preparamos la consulta para resetear el password, requerimos el email, el id 
     //del usuario que fue guardado en una variable de session y el código de verificación
     //que fue enviado en el correo al usuario y que fue guardado en el registro
     $table = Usuario::findOne(["email" => $model->email, "id" => $id_recover, "verification_code" => $model->verification_code]);
     if($table==null){
       throw new NotFoundHttpException("ingresa nuevamente los datos o envia nuevamente el mail");
     }
     //Encriptar el password
     $table->password = $model->password;
  
     //Si la actualización se lleva a cabo correctamente
     if ($table->save())
     {
      
      //Destruir las variables de sesión
      $session->destroy();
      
      //Vaciar los campos del formulario
      $model->email = null;
      $model->password = null;
      $model->password_repeat = null;
      $model->recover = null;
      $model->verification_code = null;
      
      $msg = "Enhorabuena, password reseteado correctamente, redireccionando a la página de login ...";
      Yii::$app->session->setFlash('success', 'Contraseña cambiada correctamente');
      $msg .= "<meta http-equiv='refresh' content='3; ".Url::toRoute("site/login")."'>";
     }
     else
     {
      $msg = "Ha ocurrido un error";
     }
     
    }
    else
    {
     $model->getErrors();
    }
   }
  }
  
  return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
  
 }
}
