<?php

// comment out the following two lines when deployed to production
if ($_SERVER['HTTP_HOST']=='10.11.49.240'){
    // produccion
    defined('YII_ENV') or define('YII_ENV', 'prod');
  
    // defined('YII_ENV') or define('YII_ENV', 'dev');
}else{
    // desarrollo
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'env');
}

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
