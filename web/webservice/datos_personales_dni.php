<?php

    // Link
    // https://salud.rionegro.gov.ar/mpi/webservice/datos_personales_dni.php?dni=DNI
    // Ejemplo: https://salud.rionegro.gov.ar/mpi/webservice/datos_personales_dni.php?dni=35598854
    // Return: objetos JSON con los datos de las personas

    // id_mpi:  identificador en la base maestro provincial
    // apellido
    // nombre
    // documento_tipo
    // documento_numero
    // sexo (male,female)
    // fecha_nacimiento
    // numero_hc (en el formato id_area : numero)
    // id_area ( 100:Viedma, 200:Bariloche, 300:General Roca, 400:Cipolletti)
    // id_federado_nacional: id en el federador nacional de pacientes
    // id_federado_provincial: id en el federador provincial ( se utiliza juntamente con id_area
    // domicilio
    // cp
    // localidad
    // provincia
    // telefono

    // ELSE
    // error: Descripcion del error


    //## Configuracion de la Base de datos
    require_once 'classPgSql.php';

    //## Obtengo los parametros pasados por el metodo GET
    $params = $_GET;
    $return = array();
    
    if(count($params) > 0) {

	$pg_maestro = new PgSql("localhost",5432,"mpi","postgres","omega");

        if(is_numeric($params['dni'])) {

    		$return = array();
	    	$sentenceSQL = "select id_maestro as id_mpi,concat(primer_apellido,' ',segundo_apellido) as apellido, concat(primer_nombre,' ',segundo_nombre) as nombre,documento_tipo,documento_numero,sexo,fecha_nacimiento,numero_hc,id_area,id_federado_nacional,id_federado_provincial from maestro where documento_numero='".$params['dni']."'";
            $r1 = $pg_maestro->getRows($sentenceSQL);
         
            if (empty($r1)) {
                // no hay datos
                $return = array('error'=>'No existen datos');
            }else{
                // busco domicilios
                foreach($r1 as $key=>$value){
                    $id_area=$value['id_area'];
                    $id_federado_provincial=$value['id_federado_provincial'];
                    switch($id_area)
                    {
                        case 100:
                            $pg_area = new PgSql("localhost",5432,"hospitalviedma","postgres","omega");
                            break;
                        case 200:
                            $pg_area = new PgSql("localhost",5432,"hospitalbariloche","postgres","omega");
                            break;
                        case 300:
                            $pg_area = new PgSql("localhost",5432,"hospitalroca","postgres","omega");
                            break;
                        case 400:
                            $pg_area = new PgSql("localhost",5432,"hospitalcipolletti","postgres","omega");
                            break;
                    }
                    // limit 1, para que no me traiga muchos domicilios
                    $sentenceSQL = "select * from domicilio where paciente_id=".$id_federado_provincial." limit 1";
                    $d1 = $pg_area->getRows($sentenceSQL);
                    if ($d1){
                        $r1[$key]['domicilio']=$d1[0]['domicilio'];
                        $r1[$key]['cp']=$d1[0]['cp'];
                        $r1[$key]['localidad']=$d1[0]['localidad'];
                        $r1[$key]['provincia']=$d1[0]['provincia'];
                    }else{
                        $r1[$key]['domicilio']="";
                        $r1[$key]['cp']="";
                        $r1[$key]['localidad']="";
                        $r1[$key]['provincia']="";
                    }
                    $sentenceSQL = "select * from telefono where paciente_id=".$id_federado_provincial." limit 1";
                    $t1 = $pg_area->getRows($sentenceSQL);
                    if ($t1){
                        $r1[$key]['telefono']=$t1[0]['telefono'];
//                        $r1[$key]['tipo_telefono']=$t1[0]['tipo'];
                    }else{
                        $r1[$key]['telefono']="";
//                        $r1[$key]['tipo_telefono']="";
                    }
                    $pg_area->close();
                }

                $return=$r1;
            }

    		$pg_maestro->close();

        } else {
	    	$return = array('error'=>'Parametro dni');
        }

    } else {
		$return = array('error'=>'Faltan parametros');
    }

   echo json_encode($return);     // respuesta en formato JSON

   die();

// $return_xml = new SimpleXMLElement("<?xml version=\"1.0\"?><root></root>");

// array_to_xml($return,$return_xml);

// echo  $return_xml->asXML() ;

function array_to_xml($template_info, &$xml_template_info) {
    foreach($template_info as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_template_info->addChild("$key");
                if(count($value) >1 && is_array($value)){
                    $jump = false;
                    $count = 1;
                    foreach($value as $k => $v) {
		        if(is_array($v)){
	                    if($count++ > 1)
                                $subnode = $xml_template_info->addChild("$key");
                            array_to_xml($v, $subnode);
                            $jump = true;
                        }
	            }
	            if($jump) {
    	                goto LE;
                    }
	            array_to_xml($value, $subnode);
	        } else 
		    array_to_xml($value, $subnode);
            } else {
                array_to_xml($value, $xml_template_info);
            }
        } else {
    	    $value=rtrim($value);
            $xml_template_info->addChild("$key","$value");
        }
        LE: ;
    }
}        
?>
