<?php
    // Link
    // https://salud.rionegro.gov.ar/mpi/webservice/datos_personales.php?id_area=ID_AREA&id_persona=ID_PERSONA
    // Ejemplo: https://salud.rionegro.gov.ar/mpi/webservice/datos_personales.php?id_area=200&id_persona=193408
    // Return: Datos de la consulta en formato XML/JSON

    // { "resultado":"OK",
    //   "apellido":"LIRIO ",
    //   "nombre":"MARIA CRISTINA",
    //   "documento_numero":"24975246",
    //   "sexo":"female",
    //   "fecha_nacimiento":"1976-02-03",
    //   "id_federado_nacional":"1025214",
    //   "id_federado_provincial":[{
    //        "id_area":"300",
    //        "id_persona":"146683"
    //   },{
    //        "id_area":"200",
    //        "id_persona":"193408"
    //   }]
    // }

    // { "resultado": "FAIL",
    //   "error": "Descripcion del error"
    // } 


    //## Configuracion de la Base de datos
    require_once 'classPgSql.php';

    //## Obtengo los parametros pasados por el metodo GET
    $params = $_GET;
    $return = array();
    
    if(count($params) > 0) {

	$pg_maestro = new PgSql("localhost",5432,"mpi","postgres","omega");

        if(is_numeric($params['id_area'])) {

	    if(is_numeric($params['id_persona'])) {

		$return = array();

		$sentenceSQL = "select concat(primer_apellido,' ',segundo_apellido) as apellido, concat(primer_nombre,' ',segundo_nombre) as nombre,documento_numero,sexo,fecha_nacimiento,id_federado_nacional,id_federado_provincial from maestro where id_area=".$params['id_area']." and id_federado_provincial=".$params['id_persona'];
		$row = $pg_maestro->getResource($sentenceSQL);

		$r1 = pg_fetch_assoc($row);

		if (!empty($r1)>=1) {
		    // datos encontrados
		    // verifico el id en otros hospitales
		    $sentenceSQL = "select id_area, id_federado_provincial as id_persona from maestro where id_federado_nacional='".$r1['id_federado_nacional']."' order by id_area";
		    $r2 = $pg_maestro->getRows($sentenceSQL);
		    $r1['id_federado_provincial']=$r2;   // convierto en array con id de otros hospitales el id_federado_provincial

		    $return = array('resultado'=>"OK")+$r1;

		}else{
		    // no hay datos
		    $return = array('resultado'=>"FAIL",'error'=>'No existen datos');
		}


		$pg_maestro->close();

            } else {
		$return = array('resultado'=>"FAIL",'error'=>'Parametro id_persona');
            }
        } else {
		$return = array('resultado'=>"FAIL",'error'=>'Parametro id_area');
        }
    } else {
		$return = array('resultado'=>"FAIL",'error'=>'Faltan parametros');
    }

   echo json_encode($return);     // respuesta en formato JSON

   die();

// $return_xml = new SimpleXMLElement("<?xml version=\"1.0\"?><root></root>");

// array_to_xml($return,$return_xml);

// echo  $return_xml->asXML() ;


function array_to_xml($template_info, &$xml_template_info) {
    foreach($template_info as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_template_info->addChild("$key");
                if(count($value) >1 && is_array($value)){
                    $jump = false;
                    $count = 1;
                    foreach($value as $k => $v) {
		        if(is_array($v)){
	                    if($count++ > 1)
                                $subnode = $xml_template_info->addChild("$key");
                            array_to_xml($v, $subnode);
                            $jump = true;
                        }
	            }
	            if($jump) {
    	                goto LE;
                    }
	            array_to_xml($value, $subnode);
	        } else 
		    array_to_xml($value, $subnode);
            } else {
                array_to_xml($value, $xml_template_info);
            }
        } else {
    	    $value=rtrim($value);
            $xml_template_info->addChild("$key","$value");
        }
        LE: ;
    }
}        
?>
