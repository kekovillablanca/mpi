<?php
namespace app\jobs;

use Yii;
use yii\base\BaseObject;
use yii\helpers\FileHelper;
use yii\queue\JobInterface;

class GenerateCsvJob extends BaseObject implements JobInterface
{
    public $anio;

    public function execute($queue)
    {


        Yii::info('Archivo CSV generado: ');

        $connection = Yii::$app->db;
        $query = $connection->createCommand("WITH TEMP AS (
    SELECT  62 AS CODPROV,
            p.codsisa as PUESTO ,
            e.nombre as DESCRIP,
            EXTRACT('Year' FROM c.fecha) AS ANIOCONSUL,
            EXTRACT('Month' FROM c.fecha) AS MESCONSUL,
            ps.codigo AS CODUOPERAT,
            FLOOR(c.edad) AS EDAD,
            /*COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
            COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES,*/
            c.cdgosexo as CODSEXO,
            ps.nombre as NOMBRE_SERVICIO,
            l.nombre as NOMBRE_LOCALIDAD
    FROM sies.consulta".$this->anio." c 
     INNER JOIN sies.m_servicio s on (s.descrip=c.servicio)
     join consultasestadisticas.p_servicio ps on(s.codnac2008=ps.codigo)
     JOIN sies.m_puesto p on (p.descrip=c.puesto)
     JOIN consultasestadisticas.establecimiento e on(e.codigo_sisa=p.codsisa)
     JOIN consultasestadisticas.localidad l on (l.id=e.id_localidad)
     where l.nombre like concat('%',trim(SUBSTR(c.hospital,9,20)),'%')
     union all
     SELECT  62 AS CODPROV,
            p.codsisa as PUESTO ,
            e.nombre as DESCRIP,
            EXTRACT('Year' FROM c.fecha) AS ANIOCONSUL,
            EXTRACT('Month' FROM c.fecha) AS MESCONSUL,
            ps.codigo AS CODUOPERAT,
            FLOOR(c.edad) AS EDAD,
            /*COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
            COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES,*/
            c.cdgosexo as CODSEXO,
            ps.nombre as NOMBRE_SERVICIO,
            l.nombre as NOMBRE_LOCALIDAD
    FROM sies.consulta".$this->anio." c  
    JOIN sies.m_servicio s on (s.descrip=c.servicio)
    join consultasestadisticas.p_servicio ps on(s.codnac2008=ps.codigo)
    JOIN sies.m_puesto p on (p.descrip=c.hospital) 
    JOIN consultasestadisticas.establecimiento e on(e.codigo_sisa=p.codsisa)
    JOIN consultasestadisticas.localidad l on (l.id=e.id_localidad)
    where c.puesto LIKE '%HOSPITAL%'
    union all 
SELECT 62 AS CODPROV,
c2.codigo_sisa as PUESTO,
c2.nombre as DESCRIP,
EXTRACT('Year' FROM c.fecha) AS ANIOCONSUL,
EXTRACT('Month' FROM c.fecha) AS MESCONSUL,
s.codigo AS CODUOPERAT,
FLOOR(EXTRACT('Year' FROM c.fecha)-extract('Year' from c.paciente_nac)) AS EDAD,
/*COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES,*/
c.paciente_sexo as CODSEXO,
s.nombre as NOMBRE_SERVICIO,
l.nombre as NOMBRE_LOCALIDAD
FROM hospitalroca.consulta_ambulatoria_master c inner join hospitalroca.servicio_auxiliar s
on (c.servicio = s.nombre)
JOIN hospitalroca.centro c2 on (UPPER(c.centro) = UPPER(c2.nombre)) 
join consultasestadisticas.establecimiento e on(c2.codigo_sisa=e.codigo_sisa)
join consultasestadisticas.localidad l on(l.id=e.id_localidad)
where (EXTRACT('Year' FROM c.fecha)=".$this->anio." AND trim(c.especialidad) NOT LIKE '%ENFERMERIA%')
union all 

SELECT 62 AS CODPROV,
c2.codigo_sisa as PUESTO,
c2.nombre as DESCRIP,
EXTRACT('Year' FROM c.fecha) AS ANIOCONSUL,
EXTRACT('Month' FROM c.fecha) AS MESCONSUL,
s.codigo AS CODUOPERAT,
FLOOR(EXTRACT('Year' FROM c.fecha)-extract('Year' from c.paciente_nac)) AS EDAD,
/*COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES,*/
c.paciente_sexo as CODSEXO,
s.nombre as NOMBRE_SERVICIO,
l.nombre as NOMBRE_LOCALIDAD
FROM hospitalviedma.consulta_ambulatoria_master c inner join hospitalviedma.servicio_auxiliar s
on (c.servicio = s.nombre)
JOIN hospitalviedma.centro c2 on (UPPER(c.centro) = UPPER(c2.nombre))
join consultasestadisticas.establecimiento e on(c2.codigo_sisa=e.codigo_sisa)
join consultasestadisticas.localidad l on(l.id=e.id_localidad)
where (EXTRACT('Year' FROM c.fecha)=".$this->anio." AND trim(c.especialidad) NOT LIKE '%ENFERMERIA%')
union all
SELECT 62 AS CODPROV,
c2.codigo_sisa as PUESTO,
c2.nombre as DESCRIP,
EXTRACT('Year' FROM c.fecha) AS ANIOCONSUL,
EXTRACT('Month' FROM c.fecha) AS MESCONSUL,
s.codigo AS CODUOPERAT,
FLOOR(EXTRACT('Year' FROM c.fecha)-extract('Year' from c.paciente_nac)) AS EDAD,
/*COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES,*/
c.paciente_sexo as CODSEXO,
s.nombre as NOMBRE_SERVICIO,
l.nombre as NOMBRE_LOCALIDAD
FROM hospitalcipolletti.consulta_ambulatoria_master c inner join hospitalcipolletti.servicio_auxiliar s
on (c.servicio = s.nombre)
JOIN hospitalcipolletti.centro c2 on (UPPER(c.centro) = UPPER(c2.nombre))
join consultasestadisticas.establecimiento e on(c2.codigo_sisa=e.codigo_sisa)
join consultasestadisticas.localidad l on(l.id=e.id_localidad)
where ( EXTRACT('Year' FROM c.fecha)=".$this->anio." AND trim(c.especialidad) NOT LIKE '%ENFERMERIA%')
union all
SELECT 62 AS CODPROV,
c2.codigo_sisa as PUESTO,
c2.nombre as DESCRIP,
EXTRACT('Year' FROM c.fecha) AS ANIOCONSUL,
EXTRACT('Month' FROM c.fecha) AS MESCONSUL,
s.codigo AS CODUOPERAT,
FLOOR(EXTRACT('Year' FROM c.fecha)-extract('Year' from c.paciente_nac)) AS EDAD,
/*COUNT(1) filter (WHERE CODSEXO = 'M') AS VARONES,
COUNT(1) filter (WHERE CODSEXO = 'F') AS MUJERES,*/
c.paciente_sexo as CODSEXO,
s.nombre as NOMBRE_SERVICIO,
l.nombre as NOMBRE_LOCALIDAD
FROM hospitalbariloche.consulta_ambulatoria_master c inner join hospitalbariloche.servicio_auxiliar s
on (c.servicio = s.nombre)
JOIN hospitalbariloche.centro c2 on (UPPER(c.centro) = UPPER(c2.nombre))  
join consultasestadisticas.establecimiento e on(c2.codigo_sisa=e.codigo_sisa)
join consultasestadisticas.localidad l on(l.id=e.id_localidad)
where (EXTRACT('Year' FROM c.fecha)=".$this->anio." AND  (UPPER(c.especialidad) NOT LIKE '%ENFERMERIA%' AND s.codigo <> 815))
)
SELECT  CODPROV,  PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        'menor a 1' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD < 1.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '1 a 4' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 1.0 AND EDAD < 5.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '5 a 9' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 5.0 AND EDAD < 10.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '10 a 14' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 10.0 AND EDAD < 15.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '15 a 19' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 15.0 AND EDAD < 20.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '20 a 24' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 20.0 AND EDAD < 25.0
GROUP BY CODPROV, NOMBRE_LOCALIDAD,PUESTO,DESCRIP, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '25 a 34' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 25.0 AND EDAD < 35.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '35 a 44' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 35.0 AND EDAD < 45.0
GROUP BY CODPROV,DESCRIP,NOMBRE_LOCALIDAD, PUESTO,ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '45 a 49' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 45.0 AND EDAD < 50.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '50 a 64' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 50.0 AND EDAD < 65.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        '65 a 74' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 65.0 AND EDAD < 75.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        'mayor a 75' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD >= 75.0
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
UNION
SELECT  CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO,
        'sin especificar' AS RANGOEDAD,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'M') AS VARONES,
        COUNT(1) filter (WHERE trim(CODSEXO) = 'F') AS MUJERES,
        COUNT(1) filter (WHERE trim(CODSEXO) IS NULL OR (trim(CODSEXO) <> 'F' AND trim(CODSEXO) <> 'M')) AS INDETERMINADO
FROM TEMP
WHERE EDAD IS NULL
GROUP BY CODPROV, PUESTO,DESCRIP,NOMBRE_LOCALIDAD, ANIOCONSUL, MESCONSUL, CODUOPERAT,NOMBRE_SERVICIO
union all
    select 62 as CODPROV ,
    mp.codsisa AS PUESTO, 
    e.nombre as DESCRIP,
    l.nombre as NOMBRE_LOCALIDAD,
    rc.anio as ANIOCONSUL,
    rc.mes as MESCONSUL,
    ps.codigo AS CODUOPERAT,
    ps.nombre as NOMBRE_SERVICIO,
    mr.nombre as RANGOEDAD,
    rc.varones as VARONES,
    rc.mujeres as MUJERES,
    NULL AS INDETERMINADO
from sies.resumen_consulta rc join sies.m_puesto mp on(rc.id_puesto=mp.id)join sies.m_rango mr on(mr.id=rc.id_rango)
join sies.m_servicio se on (rc.id_servicio=se.id) 
join consultasestadisticas.p_servicio ps on(se.codnac2008=ps.codigo)
JOIN consultasestadisticas.establecimiento e on(e.codigo_sisa=mp.codsisa)
JOIN consultasestadisticas.localidad l on (l.id=e.id_localidad)
where ( rc.anio=".$this->anio. " AND (mp.area like '%GENERAL ROCA%' OR mp.area like '%BARILOCHE%'))"); // Tu consulta SQL aquí
    $data = $query->queryAll();
$filePath = Yii::getAlias('@app/runtime/csv/report_' . $this->anio . '.csv');

// Asegúrate de que el directorio de destino existe
FileHelper::createDirectory(dirname($filePath));

// Abrir el archivo para escritura
$file = fopen($filePath, 'w');
if ($file === false) {
    throw new \RuntimeException('No se pudo abrir el archivo para escribir: ' . $filePath);
}

// Escribir los encabezados (ajusta según tus datos)
fputcsv($file, ['CODPROV', 'PUESTO', 'DESCRIP', 'NOMBRE_LOCALIDAD', 'ANIOCONSUL', 'MESCONSUL', 'CODUOPERAT', 'NOMBRE_SERVICIO', 'RANGOEDAD', 'VARONES', 'MUJERES', 'INDETERMINADO']);

// Escribir los datos
foreach ($data as $row) {
    fputcsv($file, $row);
}

// Cerrar el archivo
fclose($file);


}

}

?>