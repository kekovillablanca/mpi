<?php

namespace app\models;

use app\modules\admin\models\Usuario;


class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id_user;
    public $username;
    public $password;
    public $activo;
    public $authKey;
    public $accessToken;
    public $userdominio;
    public $estadoUser=null;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id_user)
    {

        $usuario= Usuario::findOne($id_user);
//        $usuario= null;

        if ($usuario){

            $model=new User();
            $model->id_user=$usuario->id;
            $model->username=$usuario->usuario;
            $model->userdominio=$usuario->id_dominio;
            $model->password=$usuario->password;
            $model->activo=$usuario->activo;

            return new static($model);
        }

        return null;


    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

        return null;
    }

   /**
 * Finds user by username
 *
 * @param string $username
 * @return static|null
 */
public static function findByUsername($username)
{
    $usuario = Usuario::findOne(['usuario' => $username]);
    $model = new User();
    if ($usuario) {
        $model->id_user = $usuario->id;
        $model->username = $usuario->usuario;
        $model->userdominio = $usuario->id_dominio;
        $model->password = $usuario->password;
        $model->activo = $usuario->activo;
    }

    return new static($model);
}

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id_user;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return (trim($this->password) == trim(md5($password)));
    }

    public function attributeLabels()
    {
        return [
            'id_user' => 'ID',
            'username' => 'Usuario',
            'password' => 'Contraseña',
	    // $authKey;
	    // $accessToken;
        ];
    }


}
