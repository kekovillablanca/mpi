<?php

namespace app\models;

use Yii;
use yii\base\Model;
use \app\modules\admin\models\Usuario;
use yii\web\Session;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user) {
                if (!$user->validatePassword($this->password)) {

                    $this->addError($attribute, 'Error de usuario o contraseña.');
                    return false;
                }
                if (!$user->activo) {
                    $this->addError($attribute, 'Usuario desactivado. Solicite activación.');
                    return false;
                }
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {

        if ($this->validate()) {
           

            return Yii::$app->user->login($this->getUser(), 600);
            
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $userModel = new User();
            $this->_user = $userModel->findByUsername($this->username);
            
            $usuario = Usuario::findOne(['usuario' => $this->username]);
    
            if ($usuario) {
                Yii::$app->session->set('nombreusuario', $usuario->nombre);
    
                // Guardar en sesión todos sus permisos en forma de array.
                $grupos = $usuario->getGrupos()->all();
                $permisosusuario = array();
    
                foreach ($grupos as $grp) {
                    $permisos = $grp->getPermisos()->all();
                    foreach ($permisos as $per) {
                        $permisosusuario[$per->id] = $per->nombre;
                    }
                }
    
                Yii::$app->session->set('permisosusuario', $permisosusuario);
    
                $fecha_actual = date('Y-m-d');
                Yii::$app->session->set('estadouser', 'OK');
                if (!empty($usuario) && $usuario->nombre != "Administrador") {
                    if ($usuario->fecha_cambio <= $fecha_actual || is_null($usuario->fecha_cambio)) {
                        Yii::$app->session->set('estadouser', "FECHAPASADA");
                    }
                }
            }
        }
    
        return $this->_user;
    }
    


    public function attributeLabels()
    {
	return [
	    'username' => 'Usuario',
	    'password' => 'Contraseña',
	];
    }

}

