<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class MultipleUploadForm extends Model
{

    public $file;


    public function rules()
    {
        return [
            [['file'], 'file', 
                'extensions' => ['csv', 'xlsx', 'xls'], // Especifica las extensiones permitidas
                'mimeTypes' => ['text/csv', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'], // MIME types para CSV y Excel
                'skipOnEmpty' => false,
            ],
        ];
    }
    
}
?>