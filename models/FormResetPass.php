<?php
 
namespace app\models;

use yii\base\Model;

class FormResetPass extends Model{
 
    public $email;
 public $password;
 public $password_repeat;
 public $verification_code;
 public $recover;
     
    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat', 'verification_code', 'recover'], 'required', 'message' => 'Campo requerido'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mínimo 5 y máximo 80 caracteres'],
            ['email', 'email', 'message' => 'Formato no válido'],
            [['password','password_repeat'],'match','pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W)[\w\W]*$/',
            'message' => 'La contraseña debe contener al menos una letra minúscula, una letra mayúscula, un número, un carácter especial (@, !, ?, $, %, &, .).'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Los passwords no coinciden'],
        ];
    }
}