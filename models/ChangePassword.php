<?php
 
namespace app\models;

use yii\base\Model;

class ChangePassword extends Model{
 

    public $pass_new="";
    public $pass_new_check="";
     
    public function rules()
    {
        return [
            [['pass_new', 'pass_new_check'], 'required', 'message' => 'Campo requerido'],
            [[ 'pass_new', 'pass_new_check'], 'string', 'max' => 100],
            [['pass_new','pass_new_check'],'match','pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W)[\w\W]*$/',
            'message' => 'La contraseña debe contener al menos una letra minúscula, una letra mayúscula, un número, un carácter especial (@, !, ?, $, %, &, .).'],
            
        ];
    }

    public function attributeLabels()
    {
        return [
            'pass_new' => 'Ingrese Nueva Contraseña',
            'pass_new_check' => 'Repita Nueva Contraseña',

        ];
    }
}
