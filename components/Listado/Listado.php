<?php

namespace app\components\Listado;

use Yii;
use yii\base\Widget;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\data\SqlDataProvider;
use Mpdf\Mpdf;
use TCPDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Listado extends Widget
{
    // Reporte y tipo
    public $reporte;
    public $sql;
    public $tipo = 'pdf';
    public $db_conexion = null;

    // Configuración
    public $filename = '';
    public $html = '';

    public function init()
    {
        parent::init();
        ob_end_clean();

        if ($this->tipo == 'pdf') {
            $this->createPdfReport();
        
        } elseif ($this->tipo == 'csv-pesado') {
            $this->createCsvReportSql();
        }
         elseif ($this->tipo == 'csv') {
            $this->createCsvReport();
        }
        elseif ($this->tipo == 'htm') {
            $this->createHtmlReport();
        }
    }

    public function createPdfReport()
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($this->reporte->seleccion);
        $result = $command->queryAll();

        $pdf = \app\components\Metodos\Metodos::obtenerObjetoPDF(
            $result,
            unserialize($this->reporte->datos),
            $this->reporte->titulo,
            $this->reporte->subtitulo,
            $this->reporte->tamano_letra,
            $this->reporte->tipo_hoja,
            $this->reporte->resumen
        );

        // Generar el nombre y ruta del archivo
        $fileName = preg_replace("/ /", "_", $this->reporte->titulo) . ".pdf";
        $filePath = Yii::getAlias('@runtime') . '/' . $fileName;

        // Guardar el archivo en el servidor
        $pdf->Output($filePath, 'F');

        // Enviar el archivo al usuario
        if (file_exists($filePath)) {
            Yii::$app->response->sendFile($filePath)->send();
            Yii::$app->end();
        } else {
            throw new \Exception("El archivo PDF no se pudo generar: $filePath");
        }
    }

    public function createCsvReport()
    {
        ini_set('memory_limit', '2G'); // Aumentar el límite de memoria si es necesario
        ini_set('max_execution_time', '300'); // Aumentar el tiempo máximo de ejecución si es necesario

        $currentDate = date('Y-m-d_His');
        $filename = preg_replace("/ /", "_", $this->reporte->titulo) . "_" . $currentDate . ".csv";
        $filepath = Yii::getAlias('@runtime') . '/' . $filename;

        $batchSize = 10000; // Tamaño de lote ajustado
        $offset = 0; // Desde donde empezar

        $connection = Yii::$app->getDb();
        $f = fopen($filepath, 'w');

        while (true) {
            // Ejecutar la consulta con paginación
            $command = $connection->createCommand($this->reporte->seleccion . " LIMIT :limit OFFSET :offset")
                ->bindValue(':limit', $batchSize)
                ->bindValue(':offset', $offset);

            $result = $command->queryAll();

            // Si no hay más resultados, salir del bucle
            if (empty($result)) {
                break;
            }

            // Escribir los nombres de los campos como cabecera del CSV si es el primer lote
            if ($offset === 0) {
                fputcsv($f, array_keys($result[0]));
            }

            // Escribir cada fila del lote en el archivo CSV
            foreach ($result as $row) {
                fputcsv($f, $row);
            }

            // Incrementar el offset para el siguiente lote
            $offset += $batchSize;
        }

        fclose($f);

        // Enviar el archivo al usuario
        if (file_exists($filepath)) {
            Yii::$app->response->sendFile($filepath)->send();
            Yii::$app->end();
        } else {
            throw new \Exception("El archivo CSV no se pudo generar: $filepath");
        }
    }
    private function createHtmlReport()
{
    $currentDate = date('Y-m-d_His');
    $filename = preg_replace("/ /", "_", $this->reporte->titulo) . "_" . $currentDate . ".html";
    $filepath = Yii::getAlias('@runtime') . '/' . $filename;

    $batchSize = 10000; // Tamaño de lote ajustado
    $offset = 0; // Desde donde empezar

    $connection = Yii::$app->getDb();
    $f = fopen($filepath, 'w');

    // Comenzar a escribir el contenido HTML
    $htmlContent = "<html><head><title>" . htmlspecialchars($this->reporte->titulo) . "</title>";
    $htmlContent .= "<style>
                        table { width: 100%; border-collapse: collapse; }
                        th, td { border: 1px solid black; padding: 8px; text-align: left; }
                        th { background-color: #f2f2f2; }
                     </style>";
    $htmlContent .= "</head><body>";
    $htmlContent .= "<h1>" . htmlspecialchars($this->reporte->titulo) . "</h1>";
    $htmlContent .= "<table><tr>";

    // Escribir los nombres de los campos como encabezados de la tabla
    $command = $connection->createCommand($this->reporte->seleccion . " LIMIT :limit OFFSET :offset")
        ->bindValue(':limit', $batchSize)
        ->bindValue(':offset', $offset);

    // Obtener los resultados para definir los encabezados
    $fields = $command->queryAll(); // Obtener los resultados
    if (!empty($fields)) {
        // Escribir los nombres de los campos como encabezados
        foreach (array_keys($fields[0]) as $field) {
            $htmlContent .= "<th>" . htmlspecialchars($field) . "</th>";
        }
    }
    $htmlContent .= "</tr>";

    // Bucle para escribir los datos en el HTML
    while (true) {
        // Ejecutar la consulta con paginación
        $command = $connection->createCommand($this->reporte->seleccion . " LIMIT :limit OFFSET :offset")
            ->bindValue(':limit', $batchSize)
            ->bindValue(':offset', $offset);

        $result = $command->queryAll();

        // Si no hay más resultados, salir del bucle
        if (empty($result)) {
            break;
        }

        // Escribir cada fila del lote en la tabla HTML
        foreach ($result as $row) {
            $htmlContent .= "<tr>";
            foreach ($row as $cell) {
                $htmlContent .= "<td>" . htmlspecialchars($cell) . "</td>";
            }
            $htmlContent .= "</tr>";
        }

        // Incrementar el offset para el siguiente lote
        $offset += $batchSize;
    }

    // Cerrar la tabla y el documento HTML
    $htmlContent .= "</table></body></html>";
    fwrite($f, $htmlContent);
    fclose($f);

    // Enviar el archivo al usuario
    if (file_exists($filepath)) {
        Yii::$app->response->sendFile($filepath)->send();
        Yii::$app->end();
    } else {
        throw new \Exception("El archivo HTML no se pudo generar: $filepath");
    }
}


   public  function createCsvReportSql()
{
    ini_set('memory_limit', '2G');
    ini_set('max_execution_time', '300');

    $filename = preg_replace("/ /", "_",$this->reporte->titulo).".csv";
    $filepath = Yii::getAlias('@runtime') . '/' . $filename;

    $batchSize = 5000;
    $offset = 0;

    $connection = Yii::$app->getDb();
    $f = fopen($filepath, 'w');

    while (true) {
        $command = $connection->createCommand($this->reporte->seleccion. " LIMIT :limit OFFSET :offset")
            ->bindValue(':limit', $batchSize)
            ->bindValue(':offset', $offset);

        $result = $command->queryAll();

        if (empty($result)) {
            break;
        }

        if ($offset === 0) {
            fputcsv($f, array_keys($result[0]));
        }

        foreach ($result as $row) {
            fputcsv($f, $row);
        }

        $offset += $batchSize;
    }

    fclose($f);

    if (file_exists($filepath)) {
        Yii::info("Archivo CSV encontrado, enviando al usuario...", __METHOD__);
        Yii::$app->response->sendFile($filepath)->send();
        Yii::$app->end();
    } else {
        throw new \Exception("El archivo CSV no se pudo generar: $filepath");
    }
}


}