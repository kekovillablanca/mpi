<?php

require_once __DIR__ . '/vendor/autoload.php';

// build config
#$config = new \HL7\FHIR\R4\PHPFHIRResponseParserConfig([
#    'registerAutoloader' => true, // use if you are not using Composer
//    'sxeArgs' => LIBXML_COMPACT | LIBXML_NSCLEAN // choose different libxml arguments if you want, ymmv.
#]);

#$parser = new \HL7\FHIR\R4\PHPFHIRResponseParser(null);
#$parser = new \HL7\FHIR\R4\PHPFHIRResponseParser($config);

#$resource = $parser->parse($json);
#print_r($resource);

#$pepe=json_encode($resource);
#print_r($pepe);

#$bundle = new \HL7\FHIR\R4\FHIRResource\FHIRBundle();


$paciente = new \HL7\FHIR\R4\FHIRResource\FHIRDomainResource\FHIRPatient();

$paciente->setIdentifier(
    [[
    'use'=>'usual',
    'system'=>'http://www.renaper.gob.ar/dni',
    'value'=>'56984620'
    ]]
);

$nombre = new \HL7\FHIR\R4\FHIRElement\FHIRHumanName();

    $nameuse = new \HL7\FHIR\R4\FHIRElement\FHIRNameUse();
    $nameuse->setValue('official');

$nombre->setUse($nameuse);
$nombre->setText("DIMAS JOAQUIN ARANIBE VOLPATO");

    $fam_extension1 = new \HL7\FHIR\R4\FHIRElement\FHIRExtension();
    $fam_extension1->setUrl('http://hl7.org/fhir/StructureDefinition/humanname-fathers-family');
    $fam_extension1->setValueString('ARANIBE');

    $fam_extension2 = new \HL7\FHIR\R4\FHIRElement\FHIRExtension();
    $fam_extension2->setUrl('http://hl7.org/fhir/StructureDefinition/humanname-mothers-family');
    $fam_extension2->setValueString('VOLPATO');

    $fam_ext1 = json_decode(json_encode($fam_extension1,JSON_UNESCAPED_SLASHES),true);
    $fam_ext2 = json_decode(json_encode($fam_extension2,JSON_UNESCAPED_SLASHES),true);

    $fam= ['value'=>'ARANIBE VOLPATO']+['extension'=>[$fam_ext1,$fam_ext2]];

$nombre->setFamily($fam);

$nombre->setGiven(['0'=>'DIMAS','1'=>'JOAQUIN']);

$gender = new \HL7\FHIR\R4\FHIRElement\FHIRAdministrativeGender();
$gender->setValue('male');

$paciente->setName([$nombre]);
$paciente->setBirthDate('2018-10-18');
$paciente->setGender($gender);

#$param1 = new \HL7\FHIR\R4\FHIRResource\FHIRParameters();

$param1 = new \HL7\FHIR\R4\FHIRElement\FHIRBackboneElement\FHIRParameters\FHIRParametersParameters();

#$param1->setId(0);
$param1->setParameter(['resource']);
#$param1->setResource($paciente);


$pepe=json_encode($param1,JSON_UNESCAPED_SLASHES);
print_r($pepe);


/*
{
"resourceType":"Parameters",
"id":"0",
"parameter":[{
    "name":"resource",
    "resource":{

////

"resourceType":"Patient",
"identifier":[{
    "use":"usual",
    "system":"http://www.renaper.gob.ar/dni",
    "value":"56984620"
    }],
"name":[{
    "use":"official",
    "text":"DIMAS JOAQUIN ARANIBE VOLPATO",
    "family":"ARANIBE VOLPATO",
    "_family":{
	"extension":[{
	    "url":"http://hl7.org/fhir/StructureDefinition/humanname-fathers-family",
	    "valueString":"ARANIBE"
		    },{
	    "url":"http://hl7.org/fhir/StructureDefinition/humanname-mothers-family",
	    "valueString":"VOLPATO"
	    }
	    ]},
	"given":["DIMAS","JOAQUIN"]
    }],
"birthDate":"2018-10-18",
"gender":"male"

////
}},{"name":"count","valueInteger":"5"}]}

*/

?>

