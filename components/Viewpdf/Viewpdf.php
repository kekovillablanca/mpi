<?php
/*
Viewpdf::widget(array(
                 'titulo'=>'Titulo de texto plano',
                 'subtitulo'=>'Subtitulo de texto plano',
                 'data'=>$model  //modelo,
                 'attributes'=>$attributes,    // atributos que quiero imprimir (formato view)
               	 'resumen'=>$html,   // html que va al final del reporte
));
*/

namespace app\components\Viewpdf;

use Yii;
use yii\base\Widget;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use Mpdf\Mpdf;

class Viewpdf extends Widget
{
	
	public $data;
	public $attributes;
	public $titulo='';    // valor por defecto
	public $subtitulo='';    // valor por defecto
    public $resumen='';

    public $itemTemplate="<tr class=\"{class}\"><th>{label}</th><td>{value}</td></tr>\n";
	public $datahtml=''; 	// $datahtml tiene el codigo en html.
	public $objPDF = null;
                
        /**
	 * Initializes the detail view.
	 * This method will initialize required property values.
	 */
	public function init()
	{
            parent::init();	
            
            if($this->data===null)
                throw new HttpException('Falta propiedad DATA.');
                        
            if($this->attributes===null)
			    throw new HttpException('Falta propiedad ATTRIBUTES.');
	}
        
    public function HeadFoot(){

                    $usuario=Yii::$app->user->identity->username;

                    $this->datahtml=' 
                    <link rel="stylesheet" type="text/css" href="'.Yii::$app->request->baseUrl.'/css/pdf.css" />
                    <sethtmlpageheader name="header" page="ALL" value="on" show-this-page="1"></sethtmlpageheader>
                    <sethtmlpagefooter name="footer" value="on"></sethtmlpagefooter>

                    <htmlpagefooter name="footer"><hr style="height: 1px; color:#000000;">
                        <div id="footer">
                            <div id="footer_date">
                            {DATE d/m/Y H:s} - '.$usuario.' - '.Html::encode(Yii::$app->name).'
                            </div>
                            <div id="footer_page">
                            {PAGENO}/{nbpg}
                            </div>
                        </div>
                    </htmlpagefooter>

                    <htmlpageheader name="header">  
                        <div id="logo_pdf">
                     <img src="' . Yii::$app->request->baseUrl . '/images/rionegrologo.png" style="height: 50px;">
                        </div>
                        <div id="titulo_pdf">
                            <b>'.$this->titulo.'<br>
                        <div id="subtitulo_pdf">'.$this->subtitulo.'</div>
                            </b>
                        </div>                                              
                        <div style="clear:both;margin-top:-10px;"><hr style="height: 0.5px; color:#000000;"></div>
                    </htmlpageheader><br><br>';

    }

        
	/**
	 * Renders the detail view.
	 * This is the main entry of the whole detail view rendering.
	 */
	public function run()
	{

        parent::run();
        $this->headFoot();

        $this->datahtml.="<style>
                table.detail-view th {text-align: left;}
                table.detail-view {font-size: 14px;font-family: sans-serif;}
                table.detail-view td {padding-left:15px;}
                </style>";

        $this->datahtml .= "<table class='detail-view'>";
						
		foreach($this->attributes as $attribute)
		{

            if (is_string($attribute)) {
                if (!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/', $attribute, $matches)) {
                    throw new HttpException('El ATTRIBUTE debe tener formato attribute:format o attribute:format:label');
                }
                $attribute = [
                    'attribute' => $matches[1],
                    'format' => isset($matches[3]) ? $matches[3] : 'text',
                    'label' => isset($matches[5]) ? $matches[5] : null,
                ];
            }

            if (!is_array($attribute)) {
                throw new HttpException('El ATTRIBUTE debe ser un array');
            }

            if (isset($attribute['visible']) && !$attribute['visible']) {
//                unset($this->attributes[$i]);
                continue;
            }

            if (!isset($attribute['format'])) {
                $attribute['format'] = 'text';
            }
            $imagen=false;
            if (is_array($attribute['format'])) {
                if ($attribute['format'][0]=='image') {
                    $ancho = $attribute['format'][1]['width'];
                    $alto = $attribute['format'][1]['height'];
                    $imagen=true;
                }
            }

            if (isset($attribute['attribute'])) {
                $attributeName = $attribute['attribute'];
                if (!isset($attribute['label'])) {
                    $attribute['label'] = $this->data->getAttributeLabel($attributeName);
                }
                if (!array_key_exists('value', $attribute)) {
                    $attribute['value'] = ArrayHelper::getValue($this->data, $attributeName);
                }
            } elseif (!isset($attribute['label']) || !array_key_exists('value', $attribute)) {
                throw new HttpException('Se requiere el elemento ATTRIBUTE para determinar el valor y la etiqueta');
            }


			$tr=array('{label}'=>'');
			if(isset($attribute['label']))
				$tr['{label}']=$attribute['label'];
			else if(isset($attribute['name']))
			{
				if($this->data instanceof Model)
					$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
				else
					$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
			}

			if(!isset($attribute['type']))
				$attribute['type']='text';
			if(isset($attribute['value']))
				$value=$attribute['value'];
			else if(isset($attribute['name']))
				$value=Html::getAttributeValue($this->data,$attribute['name']);
			else
				$value=null;

			if($attribute['format'] == 'boolean'){
		        if($value=='1')
		            $value='SI';
		        else
                    $value='NO';
            }

            if ($imagen){
        	        $tr['{value}']='<img src="' . $value . '" width="' . $ancho . 'px" height="' . $alto . 'px" style="margin-left: auto;margin-right: auto;;position:relative;" >';
            }else{
                $tr['{value}']=$value;
            }

            $this->datahtml.= strtr(isset($attribute['template']) ? $attribute['template'] : $this->itemTemplate,$tr);

//print_r($tr);
//echo "<br><br>";
//print_r(strtr(isset($attribute['template']) ? $attribute['template'] : $this->itemTemplate,$tr));

		}

        $this->datahtml.= '</table>';
		$this->datahtml.=$this->resumen;

		// comienzo creacion de PDF
        //
        // mPDF($mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P')

        $parametrosPDF=['mode'=>'utf-8','format'=>'A4','margin_right'=>15,
            'margin_left'=>15,'margin_top'=>30,'margin_bottom'=>15,'margin_header'=>9,'margin_footer'=>9,'orientation'=>'P'];

        $this->objPDF = new mPDF($parametrosPDF);

        $this->objPDF->WriteHTML($this->datahtml);

        // el objeto pdf se guarda en un archivo en la carpeta runtime
        $this->objPDF->Output('../runtime/'.preg_replace ("/ /","_",$this->titulo).".pdf","F");
               
        }
}

