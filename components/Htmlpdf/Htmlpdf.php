<?php
/*
Htmlpdf::widget(array(
                 'titulo'=>'Titulo de texto plano',
                 'subtitulo'=>'Subtitulo de texto plano',
                 'data'=>$html ,
));
*/

namespace app\components\Htmlpdf;

use Yii;
use yii\base\Widget;
// use yii\base\Component;
use yii\helpers\Html;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;

use Mpdf\Mpdf;

class Htmlpdf extends Widget
{
	
	public $titulo='Título';    // valor por defecto
	public $subtitulo='Subtítulo';    // valor por defecto
    public $data='';
    public $output = false;
//    public $itemTemplate="<tr class=\"{class}\"><th>{label}</th><td>{value}</td></tr>\n";
	public $datahtml=''; 	// $datahtml tiene el codigo en html.
	public $objPDF = null;
                
        /**
	 * Initializes the detail view.
	 * This method will initialize required property values.
	 */
	public function init()
	{
            parent::init();	
            
	}
        
    public function headFoot()
{
    $usuario = Yii::$app->user->identity->username;

    $this->datahtml = '
    <link rel="stylesheet" type="text/css" href="' . Yii::$app->request->baseUrl . '/css/pdf.css" />
    <sethtmlpageheader name="header" page="ALL" value="on" show-this-page="1"></sethtmlpageheader>
    <sethtmlpagefooter name="footer" value="on"></sethtmlpagefooter>

    <htmlpagefooter name="footer"><hr style="height: 1px; color:#000000;">
        <div id="footer">
            <div id="footer_date">
                {DATE d/m/Y H:i} - ' . $usuario . ' - ' . Html::encode(Yii::$app->name) . '
            </div>
            <div id="footer_page">
                {PAGENO}/{nbpg}
            </div>
        </div>
    </htmlpagefooter>

    <htmlpageheader name="header" >
        <div id="logo_pdf">
            <img src="' . Yii::$app->request->baseUrl . '/images/rionegrologo.png" style="height: 50px;">
        </div>
        <div style="margin-left:15%;" id="titulo_pdf">
            <b style="text-align: center;">' . $this->titulo . '<br>
            <div id="subtitulo_pdf">' . $this->subtitulo . '</div>
            </b>
        </div>
        <div >
            <hr style="height: 0.5px; color:#000000;">
        </div>
    </htmlpageheader>';
}

public function run()
{
    parent::run();
    $this->headFoot();

    // Concatenación corregida
    $this->datahtml .= $this->data;

    // Parámetros para mPDF
    $parametrosPDF = [
        'mode' => 'utf-8',
        'format' => 'A4',
        'margin_right' => 15,
        'margin_left' => 15,
        'margin_top' => 40,
        'margin_bottom' => 34,
        'margin_header' => 9,
        'margin_footer' => 20,
        'orientation' => 'P'
    ];

    // Creación del objeto mPDF
    $this->objPDF = new mPDF($parametrosPDF);

    // Escribir el contenido HTML en el PDF
    $this->objPDF->WriteHTML($this->datahtml);

    // Si la propiedad 'output' es verdadera, se devuelve el contenido en lugar de guardar el archivo
    if ($this->output) {
        // Retornar el PDF generado como contenido para la respuesta HTTP
        $pdfContent = $this->objPDF->Output('', 'S'); // 'S' para retornar el PDF como cadena
        return $pdfContent; // Este será el contenido del PDF como cadena binaria
    } else {
        // Guardar el archivo PDF en el servidor (opcional)
        $this->objPDF->Output('../runtime/' . preg_replace("/ /", "_", $this->titulo) . ".pdf", "F");
    }
}

}
