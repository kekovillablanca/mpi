<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\components\Seguridad;

use app\modules\admin\models\UsuarioGrupo;
use app\modules\sies\models\MArea;
use Yii;
use yii\helpers\ArrayHelper;

class Seguridad {

/* tienePermiso()
   Esta funcion toma el parametro r del GET que tiene 3 partes: modulo/controlador/accion
   y chequea si el usuario actual tiene permiso sobre esa acción. Tambien se le puede pasar la accion por parametro
 
   Casos especiales
   modulo/controlador/* indica que tiene permiso sobre todas las acciones del controlador
 
   La seguridad se maneja por grupos y permisos. Un grupo tiene varios permisos. Un usuario pertenece a varios grupos
   @return <boolean> 
*/

public static function tienePermiso($accion=null){

    if(Yii::$app->user->isGuest) return false;
    if(Yii::$app->user->identity->username=='administrador') return true;

    $lreturn = false;

    if (empty($accion)){
                
        $array = explode("/",$_GET['r']);

        if (count($array)==3) {
            $modulo=$array[0].'/';
            $controller = $array[1].'/';
            $accion = $array[2];
        }else if (count($array)==2) {
            $modulo=null;
            $controller = $array[0].'/';
            $accion = $array[1];
        }else{
            $modulo=null;
            $controller = null;
            $accion = $_GET['r'];
        }

    }else{

        $array = explode("/",$accion);
        
        if (count($array)==3) {
            $modulo=$array[0].'/';
            $controller = $array[0].'/';
            $accion = $array[1];
        }else if (count($array)==2) {
            $modulo=null;
            $controller = $array[0].'/';
            $accion = $array[1];
        }else{
            $modulo=strtolower(Yii::$app->controller->module->id.'/');
            $controller = Yii::$app->controller->id.'/';
            $accion = $accion;
        }
    }

    $permisosusuario=Yii::$app->session->get('permisosusuario');

    if (array_search($modulo.$controller.$accion,$permisosusuario)){
        // encontrado
        $lreturn = true;
    }
    if (array_search($modulo.$controller.'*',$permisosusuario)){
        // encontrado
        $lreturn = true;
    }

    return ($lreturn);
}
public static function permisoMenu($accion=null){
    if(Yii::$app->user->isGuest) return false;
    //if(Yii::$app->user->identity->username=='administrador') return true;
    $lreturn = false;

    
    $permisosusuario=Yii::$app->session->get('permisosusuario');
    
    $array=explode("/",$accion);

    $accion1=$array[0]."/".$array[1]."/".$array[2];

    $accion1=str_replace($array[2],"*",$accion);
 
    if(array_search($accion1,$permisosusuario)){
    
        $lreturn = true;
    }
    if (array_search($accion,$permisosusuario) ){
        // encontrado
        $lreturn = true;
    }
   
    return ($lreturn);
}
    public static function VerificarGrupos($user_estad){
        $listaAreas=[];
        $visible=null;
        $usuarioGrupos=UsuarioGrupo::findAll(['id_usuario'=>Yii::$app->user->id]);
        foreach($usuarioGrupos as $usuarioGrupo){
        if($usuarioGrupo->grupo->nombre=='Administrador Central' || $usuarioGrupo->grupo->id==10 ){
            $area = MArea::find()->asArray()->all();       
            $listaAreas= ArrayHelper::map( $area , 'id', 'descrip');
            $visible='visible';
        }
        elseif($listaAreas==null){  
        $area = MArea::findOne(['id'=>$user_estad->id_m_area]);  
        $listaAreas[$area['id']]=$area['descrip'];
        $visible='none';
        }
       
    }
    return ['listAreas'=>$listaAreas,
            'style'=>$visible];
}
public static function tienePermisoAdmin(){
    $usuarioGrupos=UsuarioGrupo::findAll(['id_usuario'=>Yii::$app->user->id]);
    $esAdmin=false;
    foreach($usuarioGrupos as $usuarioGrupo){
       
        if($usuarioGrupo->grupo->nombre == 'Administrador Central' || $usuarioGrupo->grupo->nombre == 'Administrador de Seguridad'){
            $esAdmin=true;
        }
    }
    return $esAdmin;
}

  
    public static function tieneReglaAccion($accion=null){

        if (empty($accion)){
            $param=$_GET['r'];
            $array = explode("/",$param);
            if (count($array)==1) {
                $controller = $param;
                $accion = 'index';
            }else {
                $controller = $array[0];
                $accion = $array[1];
            }
        }else{
            $controller=Yii::$app->controller->id;
            $accion=$accion;
        }

        $permiso=\app\modules\admin\models\Permiso::findOne(['nombre'=>$controller.'/'.$accion]);
        $return=$permiso->regla;

        return ($return);
    }


}
?>
