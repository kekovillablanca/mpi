Federacion

Nota:  el flag modificado, se modifica tambien por un proceso externo
Flag modificado: se modifica en el proceso de actualizacion de tabla, segun los datos que vienen del hospital.
Si el registro es nuevo o fue modificado, el flag modificado = 1.
Si se logra la Federacion, el flag modificado se setea en 0 y se utiliza para detectar inconsistencias.

1. registro federado ?
pregunto por flag federado = 1
Si esta federado, no hago nada, si el dato fue modificado, tengo inconsistencias.

2. activo ?  ( solo si la baja fue unificacion. si baja = fallecido, el registro esta activo, la HC esta activa por 10 años !! )
Si no esta activo , no hago nada. no me interesa ese paciente. Si esta activo continuo

3. si el registro fue federado y hubo modificaciones ( baja o lo que sea ) tengo inconsistencias que las voy a mostrar en la interfaz web.
federacion=1 y NO activo , se federo un registro y luego se dio de baja. error
federacion=1 y modificado=1, se federo un registro y luego se modifico, error 

4. el registro fue modificado ?. 
Esto se hace para preguntar al renaper una vez al principio y luego solo cuando el registro se modifica.
Si el  hopsital no modifica los datos, no intento nuevamente la autenticacion, (¿para que?) hasta que el hospital modifique.
Puede pasar que el renaper actualice sus datos ( por ejemplo recien nacidos). 
Cada tanto, puedo ejecutar una autenticacion general nuevamente ( modificados y no modificados). Un proceso manual...

5. No verifico si el registro ha sido autenticado previamente.
Si no fue autenticado, lo tengo que hacer.
Si el registro estaba autenticado y el hospital modifica ese registro (modificado=1), el dato pierde autenticidad (si es que la tenia).
Por lo tanto, siempre debo verificar autenticidad en esta instancia (con modificado=1), aunque el flag autenticado sea igual a 1.

6. Intento autenticacion. Solicito los datos por renaper, si me devuelve, verifico campos 1 a 1. si no me devuelve nada, esta mal el 
numero de documento y sexo. ( error 5 ). 

7. Si no se pudo autenticar, pregunto por error.  Tres tipos de errores
Error 1 y Error 4 , son errores del proceso del renaper o errores de comunicacion, En estos casos actualizo codigo de error pero dejo todo los flags
como estan , para volver por el mismo camino y intentar autenticar nuevamente. ( solo actualizo el campo json_renaper, para mostar algo al usuario)
Error 5. En este caso hubo comunicacion (status=200), pero no hubo respuesta. esto siginifica que el paciente no fue encontrado. por lo tanto este caso
es similar a NO autenticado. punto 8. 

8. No se pudo autenticar ( algun campo de verificacion es distinto) , entonces  autenticado=0 y modificado=0. ( este flag, me va a impedir que
reintente nuevamente la autenticacion, hasta que el hospital modifique los datos ), error=0, pero guardo json_renaper y foto, para mostrar al usuario.

9. El registro se autentico, por lo tanto, autenticado=1, modificado=0, error=0,  json_renaper y foto OK



