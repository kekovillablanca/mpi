# ejecutar como root

# renombro el archivo .sql como hospitalbariloche.sql
#mv $(ls *.sql) hospitalbariloche.sql

# cambios en el script sql
sed -i 's/Owner: pablon/Owner: postgres/g' hospitalbariloche.sql
sed -i 's/OWNER TO pablon/OWNER TO postgres/g' hospitalbariloche.sql
sed -i 's/Owner: tekhne/Owner: postgres/g' hospitalbariloche.sql
sed -i 's/OWNER TO tekhne/OWNER TO postgres/g' hospitalbariloche.sql

# ingreso a hospitalbariloche.sql como nueva base completa
sudo -u postgres psql -U postgres -d postgres -c "DROP DATABASE IF EXISTS hospitalbariloche;"
sudo -u postgres psql -U postgres -d postgres -c "CREATE DATABASE hospitalbariloche TEMPLATE=template1 ENCODING=UTF8;"
sudo -u postgres psql -U postgres -d hospitalbariloche < hospitalbariloche.sql
