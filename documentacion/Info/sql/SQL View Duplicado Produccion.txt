-- Ejecutar la consulta en la base local MPI

DROP view if exists duplicado;

CREATE VIEW duplicado AS
 SELECT m.id_maestro,
    m.documento_tipo,
    m.documento_numero,
    m.primer_apellido,
    m.segundo_apellido,
    m.primer_nombre,
    m.segundo_nombre,
    m.fecha_nacimiento,
    m.sexo,
    m.numero_hc,
    m.estado,
    m.id_area,
    m.id_federado_provincial,
    m.id_federado_nacional,
    m.id_duplicado,
    m.id_error,
    m.flag_federado,
    m.flag_modificado,
    m.flag_autenticado,
    m.stamp_id_error,
    m.stamp_flag_federado,
    m.stamp_flag_modificado,
    m.stamp_flag_autenticado
   FROM maestro m
     JOIN ( SELECT maestro.documento_numero,
            maestro.primer_apellido,
            maestro.segundo_apellido,
            maestro.primer_nombre,
            maestro.segundo_nombre,
            maestro.fecha_nacimiento,
            maestro.sexo,
            maestro.id_area
           FROM maestro
           WHERE ( maestro.estado !=1 OR maestro.estado IS NULL) -- muestro todos los pacientes menos los unificados, esos ya se corrigieron
          GROUP BY maestro.documento_numero, maestro.primer_apellido, maestro.segundo_apellido, maestro.primer_nombre, maestro.segundo_nombre, maestro.fecha_nacimiento, maestro.sexo, maestro.id_area
         HAVING count(*) > 1) d ON m.documento_numero::text = d.documento_numero::text AND m.primer_apellido::text = d.primer_apellido::text AND m.segundo_apellido::text = d.segundo_apellido::text AND m.primer_nombre::text = d.primer_nombre::text AND m.segundo_nombre::text = d.segundo_nombre::text AND m.fecha_nacimiento = d.fecha_nacimiento AND m.sexo::text = d.sexo::text AND m.id_area = d.id_area;

