SELECT
m.thmedcacod AS hospital,   
ser.thserdescr AS servicio,
esp.espedescri AS especialidad,
-- Totales
COUNT(esp.espedescri) AS total,
COUNT(CASE WHEN empesexo = 'F' THEN e.empesexo END) AS total_femenino,
COUNT(CASE WHEN empesexo = 'M' THEN e.empesexo END) AS total_masculino,
-- 0 a 14
COUNT(CASE WHEN (DATE_PART('year', AGE(empefecnac))::int between 0 AND 14) THEN e.empefecnac END) AS cero_catorce,
COUNT(CASE WHEN ((DATE_PART('year', AGE(empefecnac))::int between 0 AND 14) AND  empesexo = 'F') THEN e.empefecnac END) AS cero_catorce_femenino,
COUNT(CASE WHEN ((DATE_PART('year', AGE(empefecnac))::int between 0 AND 14) AND  empesexo = 'M') THEN e.empefecnac END) AS cero_catorce_masculino,
-- 15 a 49
COUNT(CASE WHEN (DATE_PART('year', AGE(empefecnac))::int between 15 AND 49) THEN e.empefecnac END) AS quince_cuarenta,
COUNT(CASE WHEN ((DATE_PART('year', AGE(empefecnac))::int between 15 AND 49) AND  empesexo = 'F') THEN e.empefecnac END) AS quince_cuarenta_femenino,
COUNT(CASE WHEN ((DATE_PART('year', AGE(empefecnac))::int between 15 AND 49) AND  empesexo = 'M') THEN e.empefecnac END) AS quince_cuarenta_masculino,
-- 50 a mas
COUNT(CASE WHEN (DATE_PART('year', AGE(empefecnac))::int > 50) THEN e.empefecnac END) AS ciencuenta_mas,
COUNT(CASE WHEN ((DATE_PART('year', AGE(empefecnac))::int > 50) AND  empesexo = 'F') THEN e.empefecnac END) AS ciencuenta_mas_femenino,
COUNT(CASE WHEN ((DATE_PART('year', AGE(empefecnac))::int > 50) AND  empesexo = 'M') THEN e.empefecnac END) AS ciencuenta_mas_masculino

FROM thhccons c
INNER JOIN thmedico m ON c.thhccomeco=m.thmecodigo
INNER JOIN emperson e ON c.thhcpanume=e.empenumero
INNER JOIN especial esp ON m.especodigo=esp.especodigo
INNER JOIN thservat ser ON m.thsercodig=ser.thsercodig
LEFT JOIN emphc hc ON e.empenumero=hc.empenumero

WHERE (hc.emphccacod=300) --300 Hospital Roca
AND (c.thhccofech::date between '2019-01-01' AND '2019-12-31') -- FECHA

GROUP BY hospital, servicio, especialidad

ORDER BY total DESC;
