SELECT

m.thmedcacod AS hospital, 
oms_cap.omcadescri AS oms_capitulo,
CONCAT (oms_cap.omcacoddde,' - ', oms_cap.omcacodhta) AS oms_grupo,
COUNT (oms_cap.omcadescri) AS total,
COUNT(CASE WHEN empesexo = 'F' THEN e.empesexo END) AS total_femenino,
COUNT(CASE WHEN empesexo = 'M' THEN e.empesexo END) AS total_masculino

FROM thhccons c
INNER JOIN thmedico m ON c.thhccomeco=m.thmecodigo
INNER JOIN emperson e ON c.thhcpanume=e.empenumero
LEFT JOIN thhcprob diag ON (c.thhcpanume=diag.thhcpanume AND c.thhccofech= diag.thhccofech) --Diagnostico CIE10
LEFT JOIN omsubcap oms ON (diag.thhcprcodi=oms.omsucodigo)
LEFT JOIN omcapit oms_cap ON (oms.omcacodigo = oms_cap.omcacodigo AND oms.omrucodigo = oms_cap.omrucodigo)

WHERE (m.thmedcacod=300) --300 Hospital Roca
AND (c.thhccofech::date between '2020-01-01' AND '2020-08-31') -- FECHA

GROUP BY hospital, oms_grupo, oms_cap.omcadescri

ORDER BY oms_grupo;

