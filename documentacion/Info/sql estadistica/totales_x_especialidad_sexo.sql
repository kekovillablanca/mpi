SELECT DISTINCT   
m.thmedcacod AS hospital,   
ser.thserdescr AS servicio,
esp.espedescri AS especialidad,
COUNT(esp.espedescri) AS especialidad_cantidad,
--Clasificacion por sexo
COUNT(CASE WHEN empesexo = 'F' THEN e.empesexo END) AS femenino,
COUNT(CASE WHEN empesexo = 'M' THEN e.empesexo END) AS masculino

FROM thhccons c
INNER JOIN thmedico m ON c.thhccomeco=m.thmecodigo
INNER JOIN emperson e ON c.thhcpanume=e.empenumero
INNER JOIN especial esp ON m.especodigo=esp.especodigo
INNER JOIN thservat ser ON m.thsercodig=ser.thsercodig
LEFT JOIN emphc hc ON e.empenumero=hc.empenumero

WHERE (hc.emphccacod=300) --300 Hospital Roca
AND (c.thhccofech::date between '2020-01-01' AND '2020-08-30') -- FECHA

GROUP BY m.thmedcacod, ser.thserdescr, esp.espedescri

ORDER BY especialidad_cantidad DESC;
