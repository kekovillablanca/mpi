SELECT DISTINCT 
--c.thhccotunu AS numero_turno, 
c.thhccofech AS fecha, 
--m.thmedcacod AS hospital, 
ser.thserdescr AS servicio,  
esp.espedescri AS especialidad, 
CONCAT( UPPER(pre.presape),', ', INITCAP (pre.presnom)) AS profesional, 
--m.thmedcacod AS medico_dominio, 
--e.empenumero paciente_id, 
hc.emphcnro AS paciente_hc,
CONCAT( UPPER(e.empeapelli),', ', INITCAP (e.empenombre)) AS paciente,  
e.empefecnac AS paciente_nac,
e.empesexo AS paciente_sexo,
CONCAT( doc.emdctdcodi,' ', doc.emdcnumero) AS documento, 
diag.thhcprcodi AS cie10_codigo,
CASE WHEN diag.thhcprtipo='P' THEN 'Presuntivo'
     WHEN diag.thhcprtipo='D' THEN 'Definitivo'
     ELSE 'S/E'
END AS tipo_diagnostico,
--diag.thhcprdesc AS cie10_descricion, 
diag.thhcprobse AS cie10_observacion

FROM thhccons c
INNER JOIN thmedico m ON c.thhccomeco=m.thmecodigo
INNER JOIN emperson e ON c.thhcpanume=e.empenumero
INNER JOIN thservat ser ON m.thsercodig=ser.thsercodig
INNER JOIN especial esp ON m.especodigo=esp.especodigo
INNER JOIN prestad pre ON m.presnumint=pre.presnumint
LEFT JOIN emdocume doc ON e.empenumero=doc.empenumero
LEFT JOIN emphc hc ON e.empenumero=hc.empenumero
LEFT JOIN thhcprob diag ON (c.thhcpanume=diag.thhcpanume AND c.thhccofech= diag.thhccofech) --Diagnostico CIE10

WHERE (hc.emphccacod=300) --300 Hospital Roca
AND (esp.especodigo <> '68') -- 68 ENFERMERIA
AND (c.thhccofech::date between '2021-01-25' AND '2021-02-21') -- FECHA
AND (
(diag.thhcprcodi LIKE 'A37%') or
(diag.thhcprcodi LIKE 'A50%') or
(diag.thhcprcodi LIKE 'A51%') or
(diag.thhcprcodi LIKE 'A53%') or
(diag.thhcprcodi LIKE 'A55%') or
(diag.thhcprcodi LIKE 'A56%') or
(diag.thhcprcodi LIKE 'B26%') or
(diag.thhcprcodi LIKE 'F10%') or
(diag.thhcprcodi LIKE 'F11%') or
(diag.thhcprcodi LIKE 'F12%') or
(diag.thhcprcodi LIKE 'F13%') or
(diag.thhcprcodi LIKE 'F14%') or
(diag.thhcprcodi LIKE 'G00%') or
(diag.thhcprcodi LIKE 'G09%') or
(diag.thhcprcodi LIKE 'J%') or 
(diag.thhcprcodi LIKE 'T14%') or
(diag.thhcprcodi LIKE 'T36%') or
(diag.thhcprcodi LIKE 'T50%') or
(diag.thhcprcodi LIKE 'T58%') or
(diag.thhcprcodi LIKE 'T65%') or

diag.thhcprcodi IN ('A022','A029','A050','A099','A540','B011','B019','B240','B342','B880','D593','N760','T300','T636','U071','U072'))

ORDER BY fecha;
