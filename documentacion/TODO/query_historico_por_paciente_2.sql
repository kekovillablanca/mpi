SELECT 
tur.thhcpanume AS paciente_id,

--Profesional
pro.thserdescr AS servicio,
pro.espedescri AS especialidad,
pro.presapenom AS prestador,
--Turno
CASE
    WHEN (thtubfecha <> '0001-01-01 00:00:00') THEN 'Anulado' 
    WHEN ((thtuifecha <> '0001-01-01 00:00:00') AND (thhccofech IS NOT NULL)) THEN 'Atendido'
    WHEN ((thtuifecha <> '0001-01-01 00:00:00') AND (thhccofech IS NULL)) THEN 'No Responde' 
    WHEN ((thtuafecha <> '0001-01-01 00:00:00') AND (thtucfecha = '0001-01-01 00:00:00') AND (thtuafecha < NOW()::timestamp)) THEN 'Ausente'  
    ELSE 'Pendiente'
END AS estado,
CASE 
    WHEN (thtutfecha <> '0001-01-01 00:00:00') THEN (thtutfecha - thtucfecha)
    ELSE NULL
END AS tiempo_espera,
thtumbdesc AS motivo_anulacion,
CASE
    WHEN (thtuafecha <> '0001-01-01 00:00:00') THEN thtuafecha 
    ELSE NULL
END AS asignado,
CASE
    WHEN (thtucfecha <> '0001-01-01 00:00:00') THEN thtucfecha 
    ELSE NULL
END AS confirmado,
CASE
    WHEN (thtutfecha <> '0001-01-01 00:00:00') THEN thtutfecha 
    ELSE NULL
END AS llamado,
CASE
    WHEN (thtuifecha  <> '0001-01-01 00:00:00') THEN thtuifecha  
    ELSE NULL
END AS ingreso,

thhccofech AS atendido,

CASE
    WHEN (thtubfecha  <> '0001-01-01 00:00:00') THEN thtubfecha  
    ELSE NULL
END AS borrado


FROM thturno tur
LEFT JOIN thtumoba mob USING (thtumbcodi) 

LEFT JOIN (
SELECT thmedcacod, thmecodigo, thcanombre, thsercodig, thserdescr, especodigo, espedescri, presnumint, presapenom
FROM thmedico m
INNER JOIN  thcentro cen ON (m.thmedcacod = cen.thcacodigo)
INNER JOIN thservat ser USING (thsercodig)
INNER JOIN especial esp USING (especodigo)
INNER JOIN prestad pres USING (presnumint)
) pro USING (thmecodigo) -- Profesional

LEFT OUTER JOIN thhccons con ON (con.thhcpanume = tur.thhcpanume AND con.thhccotunu = tur.thtunumero)

WHERE tur.thhcpanume = 23117;