#!/bin/sh


# Borrar datos anteriores
rm -f *.dbf
rm -f *.sql

# Ejecutar DOSEMU2 y el archivo uncmp.exe
dosemu -E "uncmp.exe" > uncmp_output.log 2>&1


# Verificar si los archivos .dbf se han creado
echo "Archivos en el directorio después de ejecutar uncmp.exe:"
ls -l *.dbf

# Procesar datos txconsu
if [ -f "txconsu.dbf" ]; then
    pgdbf -s 437 txconsu.dbf > txconsu.sql
    sed -i 's/txconsu/sies.txconsu/g' txconsu.sql
    psql --variable=search_path=sies "host=localhost port=5432 dbname=mpi user=postgres password=427424" -c "truncate table sies.txconsu;"
    psql --variable=search_path=sies "host=localhost port=5432 dbname=mpi user=postgres password=427424" < txconsu.sql
else
    echo "Error: el archivo txconsu.dbf no se encuentra."
fi

# Procesar datos txmorbi
if [ -f "txmorbi.dbf" ]; then
    pgdbf -s 437 txmorbi.dbf > txmorbi.sql
    sed -i 's/txmorbi/sies.txmorbi/g' txmorbi.sql
    psql "host=localhost port=5432 dbname=mpi user=postgres password=427424" -c "truncate table sies.txmorbi;"
    psql "host=localhost port=5432 dbname=mpi user=postgres password=427424" < txmorbi.sql
else
    echo "Error: el archivo txmorbi.dbf no se encuentra."
fi
