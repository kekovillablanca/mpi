<?php

//## Tarea programada cada 30 segundos . cron
require_once 'classPing.php';
require_once 'classPgSql.php';

$pg_monitor = new PgSql("localhost", 5432, "mpi", "postgres", "omega");


// ping
$sentenceSQL = "select * from public.monitor where ping = 't';";
$rows = $pg_monitor->getRows($sentenceSQL);  // devuelve array

foreach ($rows as $datos){

    $id = $datos['id'];
    $ip = trim($datos['ip']);
    $latencia_max = $datos['latencia'];

    $ping = new Ping($ip);
    $latencia = $ping->ping();

    if ($latencia){
        if ($latencia<=$latencia_max){
            $sentenceSQL = "update public.monitor set estado=3, color='green', time2=NOW() where id = $id;";
        }else{
            $sentenceSQL = "update public.monitor set estado=2, color='yellow', time2=NOW() where id = $id;";
        }
    }else{
        $sentenceSQL = "update public.monitor set estado=1, color='red', time1=NOW(), time2=NOW() where id = $id;";
    }

    $ok = $pg_monitor->exec($sentenceSQL);

    if (!$ok) {
        // error
    }
}

// servicio 
$sentenceSQL = "select * from public.monitor where monitor.puerto>0;";
$rows = $pg_monitor->getRows($sentenceSQL);  // devuelve array

foreach ($rows as $datos){

    $id = $datos['id'];
    $ip = trim($datos['ip']);
    $puerto = $datos['puerto'];

    $latencia_max = $datos['latencia'];

    if ($datos['tipo_puerto']==1){
        $ip='udp://'.$ip;
    }

    $ping = new Ping($ip);
    $ping->setPort($puerto);

    $latencia = $ping->ping('fsockopen');

    if ($latencia){
        if ($latencia<=$latencia_max){
            $sentenceSQL = "update public.monitor set estado=3, color='green', time2=NOW() where id = $id;";
        }else{
            $sentenceSQL = "update public.monitor set estado=2, color='yellow', time2=NOW() where id = $id;";
        }
    }else{
        $sentenceSQL = "update public.monitor set estado=1, color='red', time1=NOW(), time2=NOW() where id = $id;";
    }

    $ok = $pg_monitor->exec($sentenceSQL);

    if (!$ok) {
        // error
    }

}

// email 
$sentenceSQL = "select * from public.monitor where estado=1 and coalesce( trim(email_alerta),'')<>'';";
$rows = $pg_monitor->getRows($sentenceSQL);  // devuelve array

foreach ($rows as $datos){

    $email=trim($datos['email_alerta']);

    if ($datos['ping']=='t'){
        $subject=trim($datos['host'])." (".trim($datos['ip']).") ";
    }else{
        $subject=trim($datos['host'])." ".trim($datos['service'])." (".trim($datos['ip']).":".trim($datos['puerto']).") ";
    }

    $sendMail="sendEmail -f info@salud.rionegro.gov.ar -t $email -u '$subject' -m 'Alerta' -s 10.11.49.8 -xu info@salud.rionegro.gov.ar -xp info";
    shell_exec("/var/www/html/mpi/scripts/monitor/".$sendMail);
}

$pg_monitor->close();

?>
