<?php

//## Federacion de pacientes autenticados y estado <> unificados
require_once 'classPgSql.php';
require_once 'JWT.php';

echo "Inicio recuperacion id. ".date("Y-m-d H:i:s")."\n";

$pg_maestro = new PgSql("localhost",5432,"mpi","postgres","omega");

$sentenceSQL = "select id_maestro from public.maestro where flag_federado='t' and (btrim(id_federado_nacional)='0' or id_federado_nacional is null);";
$row = $pg_maestro->getColValues($sentenceSQL);

foreach ($row as $i){     // $i -> indice id_maestro

    $sentenceSQL = "select * from public.maestro as m, public.dominio as d where m.id_area=d.id and id_maestro=$i;";
    $row = $pg_maestro->getResource($sentenceSQL);
    $datos=pg_fetch_assoc($row);

    if($datos){     // encontre datos

        $respuesta=recuperacion($datos);  // respuesta = array (status,datos,json) (0,1,2)

        if (is_array($respuesta)) {
            // federado
            if ($respuesta["1"]!=0) {
                echo "Registro " . $datos['id_maestro'] . " id_federado_nacional RECUPERADO\n";
                $sentenceSQL = "update public.maestro set
                                    id_federado_nacional='" . $respuesta["1"] . "',
                                    flag_federado=true,
                                    flag_modificado=false,
                                    id_error=0,
                                    stamp_id_error=null,
                                    stamp_flag_federado=CURRENT_TIMESTAMP
                                    where id_maestro=$i;";
                $ok = $pg_maestro->exec($sentenceSQL);
            }else {
                echo "Registro " . $datos['id_maestro'] . " id_federado_nacional NO RECUPERADO\n";
            }

        }
    }
}

function recuperacion($datos)
{
    // URLs
    $urlbase = 'https://federador.msal.gob.ar/masterfile-federacion-service/fhir/Patient';
    $urllogin= 'https://federador.msal.gob.ar/bus-auth/auth';

    $dominio=trim($datos['dominio']);
    $id_federado_provincial = $datos['id_federado_provincial'];

    // obtengo autorizacion TOKEN, con JSON POST y jwt.  Respuesta: Response y status
    $token = array(
        "iss" => trim($datos['dominio']),
        "sub" => trim($datos['sub_federador']),
        "aud" => $urllogin,
        "iat" => time(),
        "exp" => time()+300,
        "name" => trim($datos['name_federador']),
        "role" => trim($datos['role_federador']),
        "ident" => trim($datos['ident_federador'])
    );

    $jwt = JWT::encode($token, trim($datos['key_federador']));

    $post_jwt = array(
        "grantType"=>"client_credentials",
        "scope"=>"Patient/*.read,Patient/*.write",
        "clientAssertionType"=>"urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
        "clientAssertion"=>$jwt
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urllogin);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_jwt, JSON_UNESCAPED_SLASHES));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    $return=null;

    if($status==200){
        $return = json_decode($response, true);
        $token = $return['accessToken'];

        // recupero ID. pregunto por id_federado_nacional al federador. Get x ID
        $urlgetID=$urlbase."?identifier=".$dominio."|".$id_federado_provincial;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$urlgetID);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response=curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $dato=0;
        if (!( $response===false and $status===false )) {
            $response_array = json_decode($response, true);
            if ( intval($response_array['total'])!=0) {
                if ($response_array['entry']['0']['resource']['id']) {
                    $dato = $response_array['entry']['0']['resource']['id'];
                }
            }
        }

        $response = str_replace("'",'"',$response);
        $return = array(201, $dato, $response);
    }

    return $return;
}

?>
