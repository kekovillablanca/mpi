<?php

//## Federacion de  pacientes autenticados y estado <> unificados
require_once 'classPgSql.php';
require_once 'JWT.php';

echo "Inicio federacion. " . date("Y-m-d H:i:s") . "\n";

$pg_maestro = new PgSql("localhost", 5432, "mpi", "postgres", "omega");

// maximo mpi
$sentenceSQL = "select max(id_maestro) as maximo_numero from public.maestro;";
$row = $pg_maestro->getResource($sentenceSQL);
$r = pg_fetch_assoc($row);
$maximo_numero = $r['maximo_numero'];
echo "Maximo ID de MPI: " . $maximo_numero . "\n";

/* pacientes a federar
flag_federado false
flag_autenticado true
estado <> 1 . no fue unificado
*/
// cantidad a federar
$sentenceSQL = "select count(*) as cantidad from public.maestro as m, public.dominio as d where m.id_area=d.id and m.flag_federado!='t' and m.flag_autenticado='t' and (m.estado!=1 or m.estado is null);";

$row = $pg_maestro->getResource($sentenceSQL);
$r = pg_fetch_assoc($row);
$cantidad = $r['cantidad'];
echo "Cantidad para Federar: " . $cantidad . "\n";

$sentenceSQL = "SELECT m.id_maestro FROM maestro m
                JOIN (  SELECT  maestro.documento_numero,
                                maestro.primer_apellido,
                                maestro.segundo_apellido,
                                maestro.primer_nombre,
                                maestro.segundo_nombre,
                                maestro.fecha_nacimiento,
                                maestro.sexo,
                                maestro.id_area
                        FROM maestro
                        WHERE (maestro.estado <> 1 OR maestro.estado IS NULL)
                        GROUP BY maestro.documento_numero, maestro.primer_apellido, maestro.segundo_apellido, maestro.primer_nombre, maestro.segundo_nombre, maestro.fecha_nacimiento, maestro.sexo, maestro.id_area
                        HAVING count(*) > 1) d 
                        ON m.documento_numero = d.documento_numero
                        AND m.primer_apellido = d.primer_apellido
                        AND m.segundo_apellido = d.segundo_apellido
                        AND m.primer_nombre = d.primer_nombre 
                        AND m.segundo_nombre = d.segundo_nombre
                        AND m.fecha_nacimiento = d.fecha_nacimiento 
                        AND m.sexo = d.sexo
                        AND m.id_area = d.id_area;";

$id_maestros_duplicados = $pg_maestro->getColValues($sentenceSQL);

$sentenceSQL = "select m.id_maestro from public.maestro as m, public.dominio as d where m.id_area=d.id and m.flag_federado!='t' and m.flag_autenticado='t' and (m.estado!=1 or m.estado is null);";

$row = $pg_maestro->getColValues($sentenceSQL);

foreach ($row as $i) {     // $i -> indice id_maestro

   if (array_search($i, $id_maestros_duplicados)) {  // si el registro esta en duplicado , lo salteo
       continue;
    }
    
    $sentenceSQL = "select * from public.maestro as m, public.dominio as d where m.id_area=d.id and id_maestro=$i;";
    $fila = $pg_maestro->getResource($sentenceSQL);
    $datos = pg_fetch_assoc($fila);

    $respuesta = federar($datos);  // respuesta = array (status,datos,json) (0,1,2)

    // duermo 1 segundo por que el federador se atora.
    sleep(1);

    if (is_array($respuesta)) {

        if ($respuesta[0] == 201) {         // create OK

            // F1 Registro federado
            $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('federacion','F1','Registro federado','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

            $sentenceSQL = "update public.maestro set
                                id_federado_nacional='" . $respuesta[1] . "',
                                flag_federado=true,
                                flag_modificado=false,
                                id_error=0,
                                stamp_id_error=null,
                                stamp_flag_federado=CURRENT_TIMESTAMP
                                where id_maestro=$i;";
        } else {                            // error de creacion
            if ($respuesta[1] == $datos['id_federado_provincial']) {
                // es el mismo paciente.  reviso flags para no volver.
                // caso rarisimo. no esta en el diagrama

                // F2 Es el mismo registro. Verificar manualmente
                $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('federacion','F2','Es el mismo registro. Verificar manualmente','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

                $sentenceSQL = "update public.maestro set        
                                        flag_federado=true,
                                        flag_modificado=false,
                                        id_error=0,
                                        stamp_id_error=null,
                                        stamp_flag_federado=CURRENT_TIMESTAMP
                                        where id_maestro=$i;";
            } else {                                                    // es un paciente duplicado

                // F3 Paciente duplicado en el dominio
                $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('federacion','F3','Paciente duplicado en el dominio','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

                $sentenceSQL = "update public.maestro set        
                                        id_duplicado=" . $respuesta[1] . ",
                                        flag_modificado=false,
                                        id_error=2,
                                        stamp_id_error=CURRENT_TIMESTAMP
                                        where id_maestro=$i;";
            }
        }
    } else {
        if ($respuesta == "F4") {

            // F4 Error en el proceso de federacion.
            $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('federacion','F4','Error en el proceso de federacion','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";
            $sentenceSQL = "update public.maestro set
                                    id_error=3,
                                    stamp_id_error=CURRENT_TIMESTAMP
                                    where id_maestro=$i;";
        }
        if ($respuesta == "F5") {
            // F5 Error de comunicacion.
            $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('federacion','F5','Error de comunicacion','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

            $sentenceSQL = "update public.maestro set
                                    id_error=1,
                                    stamp_id_error=CURRENT_TIMESTAMP
                                    where id_maestro=$i;";
        }

        if ($respuesta == "F7") {
            // F7 Error de documento.
            $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('federacion','F7','Error de datos (documento_tipo)','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

            $sentenceSQL = "update public.maestro set
                                    id_error=7,
                                    stamp_id_error=CURRENT_TIMESTAMP
                                    where id_maestro=$i;";
        }
    }

    $ok = $pg_maestro->exec($sentenceSQL);
    if ($ok) {
        $pg_maestro->insert($logSQL, "id");
    } else {  // error

        // F6 Error update en la base de datos
        // error en base de datos, escribo log.txt e intento insert en logscript
        echo "F6 federacion - Registro a modificar, error update en la base de datos " . $datos['id_area'] . ":" . $datos['id_federado_provincial'] . " id_maestro:" . $datos['id_maestro'] . " " . date("Y-m-d H:i:s") . "\n";

        $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                ('federacion','F6','Registro a modificar, error update en la base de datos','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

        $pg_maestro->insert($logSQL, "id");
    }
}

// resultados de la instancia de federacion
// consulta a logscript con la fecha actual group by con count y luego echo

$fecha = $hoy = date("Y-m-d"); 
$sentenceSQL = "select codigo_error,descripcion,count(*) as cantidad from logscript where script='federacion' and timestamp::date='$fecha' group by codigo_error,descripcion;";
$datos = $pg_maestro->getRows($sentenceSQL);
foreach ($datos as $row) {     // $i -> indice id_maestro
    echo $row['codigo_error']." ".str_pad(trim($row['cantidad']),6," ",STR_PAD_LEFT)." ".$row['descripcion']."\n";
}

echo "Fin federacion. " . date("Y-m-d H:i:s") . "\n";
// fin script federacion.php
die();

function federar($datos)
{

    // URLs
    $urlbase = 'https://federador.msal.gob.ar/masterfile-federacion-service/fhir/Patient';
    $urlmatch = 'https://federador.msal.gob.ar/masterfile-federacion-service/fhir/Patient/$match';
    $urlrenaper = 'http://www.renaper.gob.ar';
    $urllogin = 'https://federador.msal.gob.ar/bus-auth/auth';

    // datos de la tabla maestro
    $primer_apellido = trim($datos['primer_apellido']);
    $segundo_apellido = trim($datos['segundo_apellido']);
    $primer_nombre = trim($datos['primer_nombre']);
    $segundo_nombre = trim($datos['segundo_nombre']);
    $fecha_nacimiento = $datos['fecha_nacimiento'];
    $sexo = trim($datos['sexo']);
    $documento_tipo = trim($datos['documento_tipo']);
    if ($documento_tipo != "dni") {
        $return = "F7";  // error de documento / caso rarisimo
        return $return;
    }
    $documento_numero = trim($datos['documento_numero']);
    $dominio = trim($datos['dominio']);
    $id_federado_provincial = $datos['id_federado_provincial'];

    // Acomodo nombres y apellidos
    $textNombre = $primer_nombre;
    $arrayNombre = array($primer_nombre);
    $textApellido = $primer_apellido;
    $arrayApellido = array(array(
        "url" => "http://hl7.org/fhir/StructureDefinition/humanname-fathers-family",
        "valueString" => $primer_apellido
    ));
    if ($segundo_nombre) {
        $textNombre = $primer_nombre . " " . $segundo_nombre;
        $arrayNombre = array($primer_nombre, $segundo_nombre);
    }
    if ($segundo_apellido) {
        $textApellido = $primer_apellido . " " . $segundo_apellido;
        $arrayApellido = array(
            array(
                "url" => "http://hl7.org/fhir/StructureDefinition/humanname-fathers-family",
                "valueString" => $primer_apellido
            ),
            array(
                "url" => "http://hl7.org/fhir/StructureDefinition/humanname-mothers-family",
                "valueString" => $segundo_apellido
            )
        );
    }

    // armo post en forma de array
    $post_match = array(
        "resourceType" => "Parameters",
        "id" => "0",
        "parameter" => array(
            array(
                "name" => "resource",
                "resource" =>
                array(
                    "resourceType" => "Patient",
                    "identifier" => array(
                        array(
                            "use" => "usual",
                            "system" => $urlrenaper . "/" . $documento_tipo,
                            "value" => $documento_numero
                        )
                    ),
                    "name" => array(
                        array(
                            "use" => "official",
                            "text" => $textNombre . " " . $textApellido,
                            "family" => $textApellido,
                            "_family" => array(
                                "extension" => $arrayApellido,
                            ),
                            "given" => $arrayNombre,
                        )
                    ),
                    "birthDate" => $fecha_nacimiento,
                    "gender" => $sexo
                )
            ),
            array(
                "name" => "count",
                "valueInteger" => "5"
            )
        )
    );

    // armo post en forma de array para federar
    $post_array = array(
        "resourceType" => "Patient",
        "identifier" => array(
            array(
                "system" => $dominio,
                "value" => $id_federado_provincial
            ),
            array(
                "system" => $urlrenaper . "/" . $documento_tipo,
                "value" => $documento_numero
            )
        ),
        "name" => array(
            array(
                "use" => "official",
                "text" => $textNombre . " " . $textApellido,
                "family" => $textApellido,
                "_family" => array(
                    "extension" => $arrayApellido,
                ),
                "given" => $arrayNombre,
            )
        ),
        "birthDate" => $fecha_nacimiento,
        "gender" => $sexo
    );

    // obtengo autorizacion TOKEN, con JSON POST y jwt.  Respuesta: Response y status
    $token = array(
        "iss" => trim($datos['dominio']),
        "sub" => trim($datos['sub_federador']),
        "aud" => $urllogin,
        "iat" => time(),
        "exp" => time() + 300,
        "name" => trim($datos['name_federador']),
        "role" => trim($datos['role_federador']),
        "ident" => trim($datos['ident_federador'])
    );

    $jwt = JWT::encode($token, trim($datos['key_federador']));

    $post_jwt = array(
        "grantType" => "client_credentials",
        "scope" => "Patient/*.read,Patient/*.write",
        "clientAssertionType" => "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
        "clientAssertion" => $jwt
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urllogin);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_jwt, JSON_UNESCAPED_SLASHES));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    $return = null;

    if ($status == 200) {
        $return = json_decode($response, true);
        $token = $return['accessToken'];

        // ejecuto la consulta match al federador, con JSON POST.  Respuesta: Response y status
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlmatch);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer ' . $token));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_match, JSON_UNESCAPED_SLASHES));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // analizo response y status y preparo $return
        if ($status == 200) {      // tengo respuesta ok, pero puede ser respuesta cero
            /* Recorro respuesta. verifico score=1 y dominio (guardo value)
                si score=1 and dominio = mio -> duplicado, guardo identificador ( value)
                else -> federo
            */
            $response_array = json_decode($response, true);
            $duplicado = false;

            if (intval($response_array["total"]) != 0) {       // encontre registros
                foreach ($response_array["entry"] as $entry) {

                    $identifier = $entry["resource"]["identifier"];
                    $score = $entry["search"]["score"];
                    foreach ($identifier as $key1 => $value_aux) {
                        $system = $value_aux['system'];
                        $id_federado_provincial_nacion = intval($value_aux['value']);
                        if ($system == $dominio and intval($score) == 1) {
                            $duplicado = true;
                            break;
                        }
                    }
                    if ($duplicado)
                        break;
                }
            }

            if ($duplicado) {   // el registro es duplicado. (puede ser el mismo registro)
                $response = str_replace("'", '"', $response);
                $return = array(0, $id_federado_provincial_nacion, $response);
            } else {    // no es duplicado. federo paciente
                // ejecuto la consulta de create al federador, con JSON POST.  Respuesta: Response y status
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $urlbase);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer ' . $token));
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_array, JSON_UNESCAPED_SLASHES));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                // analizo response y status
                if ($status == 201) {      // create ok  (response=null, no hay respuesta del federador)
                    // create ok, recupero ID
                    // pregunto por por id_federado_nacional al federador. Get x ID
                    $urlgetID = $urlbase . "?identifier=" . $dominio . "|" . $id_federado_provincial;

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $urlgetID);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);

                    $dato = 0;
                    if (!($response === false and $status === false)) {
                        $response_array = json_decode($response, true);
                        if ($response_array['entry']['0']['resource']['id']) {
                            $dato = $response_array['entry']['0']['resource']['id'];
                        }
                    }

                    // Si dato = 0, El registro fue federado , pero no tengo el id.
                    // Debo hacer un proceso de recuperacion de id ?
                    $return = array(201, $dato);
                } else {
                    $return = "F4";   // error en  el proceso
                }
            }
        } else {        // No hay respuesta. Error en los datos que envié.
            $return = "F4";   // error en  el proceso
        }
    } else {
        $return = "F5";  // error de comunicacion
    }

    return $return;
}
