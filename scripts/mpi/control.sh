#!/bin/sh
# Script para controlar si la sincronizacion funciona, crea archivos de texto (rapidos) en forma
# remota y local con la cantidad de registros de cada tabla o vista que quiero verificar.
# Table_hash.sh: este script hay que copiarlo a la carpeta /opt/redsalud del servidor remoto.
# Se ejecuta remotamente y se le pasa la consulta por parametro. Para ejecutar remotamente por ssh, debo copiar los
# certificados desde el servidor local al remoto (ssh-copy-id root@172.16.0.n)
# Para ejecutar manualmente este script, como root, en la carpeta /var/www/html/mpi/scripts/mpi  ./control.sh . (salida control.log)

cd /var/www/html/mpi/scripts/mpi

# arrays en /bin/sh
cant_hospital="1 2 3 4 5"
hospital1="hospitalviedma"
hospital2="hospitalbariloche"
hospital3="hospitalcipolletti"
hospital4="hospitalroca"
hospital4="hospitalsao"
ip4="172.16.0.6"
ip1="172.16.0.5"
ip2="172.16.0.2"
ip3="172.16.0.3"
ip4="172.16.0.4"


# tablas que voy a controlar
cant_tabla="01 02 03 04 05 06 07 08 09 10 11 12 13 14"

tabla01="consulta_ambulatoria_master"
tabla02="consulta_ambulatoria_analisis"
tabla03="consulta_ambulatoria_objetivo"
tabla04="consulta_ambulatoria_plan"
tabla05="consulta_ambulatoria_subjetivo"
tabla06="cie10"
tabla07="icpc2"
tabla08="nomenclador"
tabla09="sistema"
tabla10="servicio_especialidad"
tabla11="domicilio"
tabla12="telefono"
tabla13="maestro_paciente"
tabla14="internacion"
seleccion01="select count(*) FROM consulta_ambulatoria_master;"
v_seleccion01="select count(*) FROM v_consulta_ambulatoria_master;"
seleccion02="select count(*) FROM consulta_ambulatoria_analisis;"
v_seleccion02="select count(*) FROM v_consulta_ambulatoria_analisis;"
seleccion03="select count(*) FROM consulta_ambulatoria_objetivo;"
v_seleccion03="select count(*) FROM v_consulta_ambulatoria_objetivo;"
seleccion04="select count(*) FROM consulta_ambulatoria_plan;"
v_seleccion04="select count(*) FROM v_consulta_ambulatoria_plan;"
seleccion05="select count(*) FROM consulta_ambulatoria_subjetivo;"
v_seleccion05="select count(*) FROM v_consulta_ambulatoria_subjetivo;"
seleccion06="select count(*) FROM cie10;"
v_seleccion06="select count(*) FROM v_cie10;"
seleccion07="select count(*) FROM icpc2;"
v_seleccion07="select count(*) FROM v_icpc2;"
seleccion08="select count(*) FROM nomenclador;"
v_seleccion08="select count(*) FROM v_nomenclador;"
seleccion09="select count(*) FROM sistema;"
v_seleccion09="select count(*) FROM v_sistema;"
seleccion10="select count(*) FROM servicio_especialidad;"
v_seleccion10="select count(*) FROM v_servicio_especialidad;"
seleccion11="select count(*) FROM domicilio;"
v_seleccion11="select count(*) FROM v_domicilio;"
seleccion12="select count(*) FROM telefono;"
v_seleccion12="select count(*) FROM v_telefono;"
seleccion13="select count(*) FROM maestro_paciente;"
v_seleccion13="select count(*) FROM v_maestro_paciente;"
seleccion14="select count(*) FROM internacion;"
v_seleccion14="select count(*) FROM v_internacion;"


# este ejemplo es para hacer hash ( es mas complejo )
# seleccion01="select concat(cast(id1 as text),'[&PK&]',cast(id2 as text),'|',md5(consulta_ambulatoria_master::text)) as hash FROM consulta_ambulatoria_master;"
# v_seleccion01="select concat(cast(id1 as text),'[&PK&]',cast(id2 as text),'|',md5(v_consulta_ambulatoria_master::text)) as hash FROM v_consulta_ambulatoria_master;"

# control 
ok=1

# mensaje para email
error="Resultado diferencias MPI\n"
error="$error""hospital (tabla) local remote (diferencia)\n\n"

inicio=`date`
echo "--- inicio $inicio" > control.log

for ih in $cant_hospital; do

    ###truncate tabla control_result
    #echo "inicializo tabla control_result" >> control.log
    #eval hospital="\$hospital$ih"
    #psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "truncate table $hospital.control_result;" >> control.log 2>> control.log

    ### para cada hospital y cada tabla
    echo "ejecuto control para cada hospital" >> control.log
    for it in $cant_tabla; do

        eval hospital="\$hospital$ih"
        eval ip="\$ip$ih"
        eval tabla="\$tabla$it"
        eval seleccion="\$seleccion$it"
        eval v_seleccion="\$v_seleccion$it"

        ##### inicio control

        echo "----------" >> control.log
        echo "inicio procedimiento $hospital($ip) $tabla" >> control.log
        echo "datos: $seleccion" >> control.log

        rm -f control_local.txt
        rm -f control_remoto.txt

        ###local
        echo "hash local" >> control.log
        PGOPTIONS="--search_path=$hospital" psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "$seleccion" > control_local.txt 2>> control.log
                    
        ###rremoto
        echo "hash remoto" >> control.log
        ssh root@$ip "/opt/redsalud/control_remoto.sh $ip \"$v_seleccion\""
        scp root@$ip:/opt/redsalud/control_remoto.txt control_remoto.txt >> control.log

        ###acomodo archivos de texto
        echo "revision archivos txt" >> control.log
        sed -i '1,2d' control_local.txt
        sed -i '$d' control_local.txt
        sed -i '$d' control_local.txt
        sed -i '1,2d' control_remoto.txt
        sed -i '$d' control_remoto.txt
        sed -i '$d' control_remoto.txt

        ## diferencia de archivos
        echo "$hospital ($tabla)"
        diff -y -W 30 control_local.txt control_remoto.txt
        if [ $? -eq 1 ]; then
           ok=0
        fi

        # diferencia de cantidades
        n_local=`cat ./control_local.txt`
        n_remoto=`cat ./control_remoto.txt`
        diferencia=$(( n_local - n_remoto ))

        ### mensaje para email
        echo "preparo mensaje para el mail" >> control.log
        echo "$hospital ($tabla) $n_local $n_remoto  ($diferencia)\n" >> control.log

        error="$error$hospital ($tabla) $n_local $n_remoto  ($diferencia)\n"

        ### estas lineas son para el caso de control por hash
        ###inserto en las bases temporales
#        echo "insert control_local en tabla temporal" >> control.log
#        psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "truncate table $hospital.control_local;" >> control.log 2>> control.log
#        psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "copy $hospital.control_local from '/var/www/html/mpi/scripts/mpi/control_local.txt' (DELIMITER('|'));" >> control.log 2>> control.log

#        echo "insert control_remoto en tabla temporal" >> control.log
#        psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "truncate table $hospital.control_remoto;" >> control.log 2>> control.log
#        psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "copy $hospital.control_remoto from '/var/www/html/mpi/scripts/mpi/control_remoto.txt' (DELIMITER('|'));" >> control.log 2>> control.log

        ###registros que estan en remoto pero no en local. serian los insert desde hospital a local
#        echo "comparacion insert" >> control.log
#        psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "insert into $hospital.control_result (id,operacion,tabla) \
#        select control_remoto.id,'insert','"$tabla"' from $hospital.control_remoto except select control_local.id,'insert','"$tabla"' from $hospital.control_local;" >> control.log 2>> control.log

        ###registros que estan en local pero no en remoto. serian los delete de local
#        echo "comparacion delete" >> control.log
#        psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "insert into $hospital.control_result (id,operacion,tabla) \
#        select control_local.id,'delete','"$tabla"' from $hospital.control_local except select control_remoto.id,'delete','"$tabla"' from $hospital.control_remoto;" >> control.log 2>> control.log

        ###registros que estan en remoto y en local pero son distintos. serian los update de local desde remoto
#        echo "comparacion update" >> control.log
#        psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "insert into $hospital.control_result (id,operacion,tabla) select *,'update','"$tabla"' \
#        from (select distinct id from ( select * from $hospital.control_remoto union all select * from $hospital.control_local ) tempo1 group by id,hash having count(*)<2) tempo2 except \
#        (select distinct id,'update','"$tabla"' from ( select * from $hospital.control_remoto union all select * from $hospital.control_local ) tempo3 group by id having count(*)=1);" >> control.log 2>> control.log

        rm -f control_local.txt
        rm -f control_remoto.txt

        echo "fin procedimiento $hospital($ip) $tabla" >> control.log

    done

#    psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "select count(*) from $hospital.control_result;" > error.txt 2>> control.log
#    sed -i '1,2d' error.txt
#    sed -i '2d' error.txt

#    resultado=`cat ./error.txt`
#    error="$mensaje$hospital $resultado \n"
#    rm ./error.txt

#    echo $error

done

# email
if [ $ok -eq 0 ]; then
    ./sendEmail -f info@salud.rionegro.gov.ar -t info@salud.rionegro.gov.ar -u 'Control MPI' -m "$error" -s 10.11.49.8
else
    ./sendEmail -f info@salud.rionegro.gov.ar -t info@salud.rionegro.gov.ar -u 'Control MPI' -m "OK" -s 10.11.49.8
fi

fin=`date`
echo "--- fin $fin" >> control.log
