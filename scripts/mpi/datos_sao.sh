cd /var/www/html/mpi/scripts/mpi

inicio=`date`
echo "--- inicio $inicio" > datos.log
ok=1
error="ERRORES\n"

# sao
hospital="hospitalsao"
db_hospital="altea_rn_prod"
ip="172.16.0.6"

cant_vistas="01 02 03 04 05 06 07 08 09 10 11 12 13 14"

vista01="consulta_ambulatoria_master"
vista02="consulta_ambulatoria_analisis"
vista03="consulta_ambulatoria_objetivo"
vista04="consulta_ambulatoria_plan"
vista05="consulta_ambulatoria_subjetivo"
vista06="cie10"
vista07="icpc2"
vista08="nomenclador"
vista09="sistema"
vista10="servicio_especialidad"
vista11="domicilio"
vista12="telefono"
vista13="maestro_paciente"
#nueva vista de keko
vista14="internacion"


    ### para cada hospital y cada tabla, ejecuto el comando

    for iv in $cant_vistas; do

        eval vista="\$vista$iv"


        echo "$ip ($hospital): ($iv) $vista" >> datos.log

        # if telnet a 5432 ok 
        netcat -z ${ip} 5432
        if [ $? -eq 0 ]; then

            # creo tabla temporal en hospital para guardar datos y estructura de la vistas
            psql "host=$ip port=5432 dbname=$db_hospital options=--search_path=public user=postgres password=omega" -c "drop table if exists v_$vista;" >> datos.log 2>> datos.log
             psql "host=$ip port=5432 dbname=$db_hospital options=--search_path=public user=postgres password=omega" -c "create table v_$vista as (select * from $vista);" >> datos.log 2>> datos.log

            # me traigo la estructura y los datos
            PGPASSWORD="omega" pg_dump --verbose --host=$ip --port=5432 --dbname=$db_hospital --username=postgres --schema=public --table=v_$vista --file v_$vista.sql 2>>datos.log
            #PGPASSWORD="omega" pg_dump -U postgres -h $ip -p 5432 --db $db_hospital  --verbose    -t public.v_$vista > v_$vista.sql
            if [ $? -eq 0 ]; then
                    # si utilizo bases separadas para hacer el restore
                    # drop y create local
                    #psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -c "drop table if exists v_$vista;" >> datos.log 2>> datos.log
                    #psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -v ON_ERROR_STOP=1 -f v_$vista.sql >>datos.log 2>> datos.log

                    #rename local si todo ok
                    #if [ $? -eq 0 ]; then
                    #    psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -c "drop table if exists $vista;" >> datos.log 2>> datos.log
                    #    psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -c "alter table v_$vista rename to $vista;" >> datos.log 2>> datos.log
                    #else
                    #    echo "ERROR DE RESTORE: $ip ($hospital): ($iv) $vista" >> datos.log
                    #    ok=0
                    #    error="$error ERROR DE RESTORE: $ip ($hospital): ($iv) $vista\n"
                    #fi

                # para esquema dentro de la base principal mpi
                sed -i -e "s/public.v_$vista/$hospital.v_$vista/" v_$vista.sql >> datos.log 2>> datos.log

                # drop y create local
                PGPASSWORD="427424" psql -U postgres -p 5432 -h 10.11.49.78 -d mpi  -c "drop table if exists v_$vista;" >> datos.log 2>> datos.log
                PGPASSWORD="427424" psql -U postgres -p 5432 -h 10.11.49.78 -d mpi  -v ON_ERROR_STOP=1 -f v_$vista.sql >>datos.log 2>> datos.log

                #rename local si todo ok
                if [ $? -eq 0 ]; then

                    PGPASSWORD="427424" psql -U postgres -p 5432 -h 10.11.49.78 -d mpi  -c "drop table if exists $vista;" >> datos.log 2>> datos.log
                    PGOPTIONS="--search_path=$hospital" psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "alter table v_$vista rename to $vista;" >> datos.log 2>> datos.log

                else
                    echo "ERROR DE RESTORE: $ip ($hospital): ($iv) $vista" >> datos.log
                    ok=0
                    error="$error ERROR DE RESTORE: $ip ($hospital): ($iv) $vista\n"
                fi
            else
                echo "ERROR DE DUMP: $ip ($hospital): ($iv) $vista" >> datos.log
                ok=0
                error="$error ERROR DE DUMP: $ip ($hospital): ($iv) $vista\n"
            fi

            #rm v_$vista.sql

        else
            echo "ERROR DE CONEXION: $ip ($hospital): ($iv) $vista" >> datos.log
            ok=0
            error="$error ERROR DE CONEXION: $ip ($hospital): ($iv) $vista\n"
        fi

    done

done

# send email
if [ $ok -eq 0 ]; then
    ./sendEmail -o tls=no -f info@salud.rionegro.gov.ar -t nsegura@salud.rionegro.gov.ar -u "Sicronizacion MPI" -m "$error" -s 10.11.49.8
fi
if [ $ok -eq 1 ]; then
    ./sendEmail -o tls=no -f info@salud.rionegro.gov.ar -t nsegura@salud.rionegro.gov.ar -u "Sincronizacion MPI" -m "OK" -s 10.11.49.8
fi

fin=`date`
echo "--- fin $fin" >> datos.log
