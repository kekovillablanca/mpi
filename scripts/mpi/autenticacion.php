<?php

//## Autenticacion de la base maestro contra el renaper
require_once 'classPgSql.php';

echo "Inicio autenticación. ".date("Y-m-d H:i:s")."\n";

$pg_maestro = new PgSql("localhost",5434,"mpi_estadistica","keko","427424");


/* pacientes a autenticar
flag_modificado true
flag_federado false
documento_numero <> 0 o null o vacio 
estado <> 1 . no fue unificado
*/

$sentenceSQL = "select count(id_maestro) as cantidad from public.maestro where flag_modificado='t' and flag_federado!='t' and trim(documento_numero)!='0' and trim(documento_numero)!='' and (estado!=1 or estado is null);";
$row = $pg_maestro->getResource($sentenceSQL);
$r = pg_fetch_assoc($row);
$cantidad = $r['cantidad'];
echo "Cantidad de registros para autenticar: ".$cantidad."\n";

$sentenceSQL = "select id_maestro from public.maestro where flag_modificado='t' and flag_federado!='t' and trim(documento_numero)!='0' and trim(documento_numero)!='' and (estado!=1 or estado is null);";

$row = $pg_maestro->getColValues($sentenceSQL);

foreach ($row as $i) {     // $i -> indice id_maestro

    $sentenceSQL = "select * from public.maestro as m, public.dominio as d where m.id_area=d.id and id_maestro=$i;";
    $row = $pg_maestro->getResource($sentenceSQL);
    $datos = pg_fetch_assoc($row);

    $respuesta = autenticar($datos);

    // respuesta=array, datos del renaper, respuesta="E?" error
    if (is_array($respuesta)) {

        // Comparacion: apellido, nombre, fechaNacimiento (numeroDocumento, sexo .. van en la consulta)
        $apellido_comparacion = strtoupper(trim(trim($datos['primer_apellido']) . " " . trim($datos['segundo_apellido'])));
        $nombre_comparacion = strtoupper(trim(trim($datos['primer_nombre']) . " " . trim($datos['segundo_nombre'])));   // minusculas
        $fecha_nacimiento_comparacion = $datos['fecha_nacimiento'];
        $documento_tipo = trim($datos['documento_tipo']);

        // datos del renaper
        $apellido_aut = trim($respuesta['apellido']);
        $nombre_aut = trim($respuesta['nombres']);
        $fecha_nacimiento_aut = $respuesta['fechaNacimiento'];
        $documento_tipo_aut = "dni";

        // para corregir la garca del renaper, apellidos y nombres
        $auxiliar = explode(" ", $apellido_aut);
        $apellido_aut = "";
        foreach ($auxiliar as $apellido) {
            if ($apellido) {
                $apellido_aut = $apellido_aut . " " . $apellido;
            }
        }
        $apellido_aut = strtoupper(trim($apellido_aut));

        $auxiliar = explode(" ", $nombre_aut);
        $nombre_aut = "";
        foreach ($auxiliar as $nombre) {
            if ($nombre) {
                $nombre_aut = $nombre_aut . " " . $nombre;
            }
        }
        $nombre_aut = strtoupper(trim($nombre_aut));

        if (
            $apellido_comparacion == $apellido_aut and
            $nombre_comparacion == $nombre_aut and
            $fecha_nacimiento_comparacion == $fecha_nacimiento_aut and
            $documento_tipo == $documento_tipo_aut
        ) {

            // A1 Registro autenticado
            $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('autenticacion','A1','Registro autenticado','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

            $sentenceSQL = "update public.maestro set
                                flag_modificado=false,
                                flag_autenticado=true,
                                id_error=0,
                                stamp_flag_autenticado=CURRENT_TIMESTAMP
                                where id_maestro=$i;";
        } else {
            // A2 Registro no autenticado
            $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('autenticacion','A2','Registro no autenticado','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

            $sentenceSQL = "update public.maestro set
                                flag_modificado=false,
                                flag_autenticado=false,
                                id_error=0
                                where id_maestro=$i;";
        }

    } else {
        switch ($respuesta) {
            case "A3";
                // A3 Renaper sin respuesta.
                $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('autenticacion','A3','Renaper sin respuesta','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

                $sentenceSQL = "update public.maestro set
                                    flag_modificado=false,
                                    flag_autenticado=false,
                                    id_error=5,
                                    stamp_id_error=CURRENT_TIMESTAMP
                                    where id_maestro=$i;";
                break;
            case "A4";   // comunicacion
                // A4 Error de comunicacion.
                $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('autenticacion','A4','Error de comunicacion','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

                $sentenceSQL = "update public.maestro set
                                    id_error=1,
                                    stamp_id_error=CURRENT_TIMESTAMP
                                    where id_maestro=$i;";
                break;
            case "A5";   // error en el proceso
                // A5 Error en el proceso de autenticacion.
                $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('autenticacion','A5','Error en el proceso de autenticacion','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

                $sentenceSQL = "update public.maestro set
                                    id_error=4,
                                    stamp_id_error=CURRENT_TIMESTAMP
                                    where id_maestro=$i;";
                break;
        }
    }

    $ok = $pg_maestro->exec($sentenceSQL);

    if ($ok) {
        $pg_maestro->insert($logSQL, "id");
    } else {  // error
        // A6 Error update en la base de datos
        // error en base de datos, escribo log.txt e intento insert en logscript
        echo "A6 autenticacion - Registro a modificar, error update en la base de datos " . $datos['id_area'] . ":" . $datos['id_federado_provincial'] . " id_maestro:" . $datos['id_maestro'] . " " . date("Y-m-d H:i:s") . "\n";

        $logSQL = "insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                ('autenticacion','A6','Registro a modificar, error update en la base de datos','" . $datos['base'] . "'," . $datos['id_federado_provincial'] . "," . $datos['id_maestro'] . ",CURRENT_TIMESTAMP)";

        $pg_maestro->insert($logSQL, "id");
    }
}


$fecha = date("Y-m-d"); 
$sentenceSQL = "select codigo_error,descripcion,count(*) as cantidad from logscript where script='autenticacion' and timestamp::date='$fecha' group by codigo_error,descripcion;";
$datos = $pg_maestro->getRows($sentenceSQL);
foreach ($datos as $row) {     // $i -> indice id_maestro
    echo $row['codigo_error']." ".str_pad(trim($row['cantidad']),6," ",STR_PAD_LEFT)." ".$row['descripcion']."\n";
}

// cierro base maestro
$pg_maestro->close();

echo "Fin autenticacion. ".date("Y-m-d H:i:s")."\n";
// fin script autenticacion.php
die();

function autenticar($datos)
{

    $nroDocumento= trim($datos['documento_numero']);
    $idSexo = "";
    if (trim($datos['sexo']) == 'female')
        $idSexo = '1';
    if (trim($datos['sexo']) == 'male')
        $idSexo = '2';
    $nombre=$datos['nombre_renaper'];
    $clave=$datos['clave_renaper'];
    $codDominio=$datos['coddominio_renaper'];

    // URLs
    $urllogin = 'https://federador.msal.gob.ar/masterfile-federacion-service/api/usuarios/aplicacion/login';
    $urlrenaper = 'https://federador.msal.gob.ar/masterfile-federacion-service/api/personas/renaper'; //?nroDocumento=28037737&idSexo=2
    $urlrenaper = $urlrenaper.'?nroDocumento='.$nroDocumento.'&idSexo='.$idSexo;

    // armo post en forma de array para login
    $post_array = array(
        "nombre" => $nombre,
        "clave" => $clave,
        "codDominio"=>$codDominio
    );

    // obtengo autorizacion TOKEN, con JSON POST.  Respuesta: Response y status

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urllogin);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_array, JSON_UNESCAPED_SLASHES));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    $return=null;

    if($status==200){

        $response_array = json_decode($response, true);
        $token=$response_array['token'];

        // ejecuto la consulta al GET a renaper
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlrenaper);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('token: '.$token,'codDominio: '.$codDominio));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (!( $response===false and $status===false )) {
            if ($status == 200) {      // tengo respuesta
                $return = json_decode($response, true);
            } else {
                $return = "A3";  // sin respuesta
            }
        }else{
            $return="A5";  // error en el proceso
        }
    }else{
        $return="A4";  // comunicacion
    }
    return $return;
}
