#!/bin/bash

-- maestro de pacientes hospital
CREATE VIEW maestro_paciente AS
SELECT
per.empenumero,
per.empeapelli,
per.empenombre,
per.empefecnac,
per.empesexo,
per.embajcodig,
per.empefecbaj,
doc.emdctdcodi,
doc.emdcnumero,
string_agg(concat(hc.emphccacod,':',trim(hc.emphcnro)),'|' order by hc.emphccacod) as numero_hc 
from public.emperson as per
left join (SELECT DISTINCT ON (dc.empenumero) * FROM public.emdocume as dc ORDER BY dc.empenumero, dc.emdcfecvig DESC NULLS LAST) as doc ON doc.empenumero=per.empenumero
left join public.emphc as hc on hc.empenumero=per.empenumero
group by per.empenumero, per.empeapelli,per.empenombre,per.empefecnac,per.empesexo,per.embajcodig,per.empefecbaj,doc.emdctdcodi,doc.emdcnumero;

-- Consulta Ambulatoria Master
CREATE VIEW consulta_ambulatoria_master AS
SELECT c.thhcpanume AS id1, c.thhccofech AS id2,
    200 AS dominio, -- 100 viedma, 200 bariloche, 300 roca, 400 cipolletti
    'Bariloche'::text AS area, -- Viedma, Bariloche, General Roca, Cipolletti
    cen.thcacodigo AS centro_id, 
        CASE
            WHEN cen.thcacodigo = 200 THEN 'HOSPITAL'::character varying
            ELSE cen.thcadescri
        END AS centro, 
    e.empenumero AS paciente_id,
    string_agg(pg_catalog.concat(hc.emphccacod, ':', btrim(hc.emphcnro::text)), '|'::text ORDER BY hc.emphccacod) AS paciente_numero_hc,
    e.empeapelli AS paciente_ape, e.empenombre AS paciente_nom, 
    e.empefecnac AS paciente_nac, e.empesexo AS paciente_sexo, 
    doc.emdctdcodi AS paciente_tipodoc, doc.emdcnumero AS paciente_numerodoc, 
    c.thhccofech::date AS fecha, esp.espedescri AS especialidad, 
    ser.thserdescr AS servicio, c.thhccomeco AS medico_id, 
    pre.presape AS medico_ape, pre.presnom AS medico_nom, 
    c.thhccotunu AS numero_turno, dia.thhcprcodi AS cie10_codigo, 
    dia.thhcprtipo AS cie10_tipo_diag, dia.thhcprdesc AS cie10_descricion, 
    dia.thhcprobse AS cie10_observacion
   FROM thhccons c
   JOIN thmedico m ON c.thhccomeco = m.thmecodigo
   JOIN emperson e ON c.thhcpanume = e.empenumero
   JOIN thservat ser ON m.thsercodig = ser.thsercodig
   JOIN especial esp ON m.especodigo = esp.especodigo
   JOIN prestad pre ON m.presnumint = pre.presnumint
   JOIN thcentro cen ON m.thmedcacod = cen.thcacodigo
   LEFT JOIN emdocume doc ON e.empenumero = doc.empenumero
   LEFT JOIN emphc hc ON hc.empenumero = e.empenumero
   LEFT JOIN ( SELECT DISTINCT ON (thhcprob.thhccofech, thhcprob.thhcpanume) thhcprob.thhcpanume, 
    thhcprob.thhccofech, thhcprob.thhcprcodi, thhcprob.thhcprdesc, 
    thhcprob.thhcprobse, thhcprob.thhcprtipo
   FROM thhcprob
  WHERE thhcprob.thhcprtico <> 'I'::bpchar
  ORDER BY thhcprob.thhccofech) dia ON c.thhcpanume = dia.thhcpanume AND c.thhccofech = dia.thhccofech
  GROUP BY id1,id2,dominio,area,centro_id,centro,paciente_id,
  paciente_ape,paciente_nom,paciente_nac,paciente_sexo,paciente_tipodoc,paciente_numerodoc,fecha,especialidad,servicio,
  medico_id,medico_ape,medico_nom,numero_turno,cie10_codigo,cie10_tipo_diag,cie10_descricion,cie10_observacion

-- Consulta Ambulatoria Subjetivo
CREATE VIEW consulta_ambulatoria_subjetivo AS
SELECT
problemas.thhcpanume AS paciente_id,
problemas.thhccofech AS fecha_consulta,
'ICPC2' AS nomenclador,
icpc2.icpc_codigo AS codigo_nomenclador,
icpc2.icpc_capitulo AS codigo_capitulo,
icpc2.icpc_componente AS codigo_componente,
icpc2.icpc_descripcion AS codigo_descripcion,
problemas.thhcprobse AS detalle

FROM thhcprob problemas
INNER JOIN(
    SELECT
    icpc.thiccodigo	AS icpc_codigo, 
    icpc_cap.thiccadesc AS icpc_capitulo,
    icpc_com.thiccodesc AS icpc_componente,
    icpc.thicdescri AS icpc_descripcion    
    FROM thiccodi icpc
    INNER JOIN thiccapi icpc_cap ON (icpc.thiccacodi = icpc_cap.thiccacodi)
    INNER JOIN thiccomp icpc_com ON (icpc.thiccocodi = icpc_com.thiccocodi)
) icpc2 ON (problemas.thhcprcodi = icpc2.icpc_codigo  AND problemas.thhcprtico = 'I')

UNION ALL

SELECT 
obs.thhcpanume AS paciente_id,
obs.thhccofech AS fecha_consulta,
NULL AS nomenclador,
NULL AS codigo_nomenclador,
NULL AS codigo_capitulo,
NULL AS codigo_componente,
NULL AS codigo_descripcion,
obs.thhcobserv AS detalle

FROM thhcobsc obs
WHERE (obs.thhcobcodo='S');

-- Consulta Ambulatoria Objetivo
CREATE VIEW consulta_ambulatoria_objetivo AS
SELECT 
examen.thhcpanume AS paciente_id, 
examen.thhccofech AS fecha_consulta,
'SISTEMA' AS nomenclador,
concat(resultado.thexescodi::text,'.',resultado.thexarcodi::text,'.',resultado.thexticodi::text,'.',resultado.thexrecodi::text) as codigo_nomenclador,
especialidad.thexesdesc AS examen_especialidad, 
area.thexardesc AS examen_area, 
tipo.thextidesc AS examen_tipo, 
resultado.thexredesc AS examen_resultado,
examen.thhcexvalo AS valor,
examen.thhcexobse AS detalle

FROM thhcexfi examen
LEFT JOIN thexresu resultado ON ((examen.thexescodi = resultado.thexescodi) AND (examen.thexarcodi = resultado.thexarcodi) AND (examen.thexticodi = resultado.thexticodi) AND (examen.thexrecodi = resultado.thexrecodi))
LEFT JOIN thextipo tipo ON ((resultado.thexescodi = tipo.thexescodi) AND (resultado.thexarcodi = tipo.thexarcodi) AND (resultado.thexticodi = tipo.thexticodi))
LEFT JOIN thexarea area ON ((tipo.thexescodi = area.thexescodi) AND (tipo.thexarcodi = area.thexarcodi))
LEFT JOIN thexespe especialidad ON (area.thexescodi = especialidad.thexescodi)

UNION ALL

SELECT 
obs.thhcpanume AS paciente_id,
obs.thhccofech AS fecha_consulta,
NULL AS nomenclador,
NULL AS codigo_nomenclador,
NULL AS examen_especialidad, 
NULL AS examen_area, 
NULL AS examen_tipo, 
NULL AS examen_resultado,
NULL AS valor,
obs.thhcobserv AS detalle 

FROM thhcobsc obs
WHERE (obs.thhcobcodo='O');

-- Consulta Ambulatoria Analisis
CREATE VIEW consulta_ambulatoria_analisis AS
SELECT 
diag.thhcpanume AS paciente_id,
diag.thhccofech AS fecha_consulta,
ep.thepinro AS episodio,
CASE
    WHEN diag.thhcprtico = 'C' THEN 'CIE10'
    WHEN diag.thhcprtico = 'I' THEN 'ICPC2'
    ELSE 'S/E'
END AS nomenclador,
diag.thhcprcodi AS codigo_nomenclador,
diag.thhcprdesc AS codigo_descripcion,
CASE WHEN diag.thhcprtipo='P' THEN 'Presuntivo'
     WHEN diag.thhcprtipo='D' THEN 'Definitivo'
     ELSE 'Sin especificar'
END AS tipo_diagnostico,
diag.thhcprobse AS detalle

FROM thhcprob diag
LEFT JOIN thhcprep ep ON (ep.thhcpanume = diag.thhcpanume AND ep.thhccofech = diag.thhccofech AND ep.thhcprcodi = diag.thhcprcodi)
WHERE (diag.thhcprtico <> 'I')

UNION ALL

SELECT 
obs.thhcpanume AS paciente_id,
obs.thhccofech AS fecha_consulta,
ep.thepinro AS episodio,
NULL AS nomenclador,
NULL AS codigo_nomenclador,
NULL AS codigo_descripcion,
NULL AS tipo_diagnostico,
obs.thhcobserv AS detalle

FROM thhcobsc obs
LEFT JOIN thhcobep ep ON (ep.thhcpanume = obs.thhcpanume AND ep.thhccofech = obs.thhccofech AND ep.thhcobcodo = obs.thhcobcodo)
WHERE (obs.thhcobcodo='D');

-- Consulta Ambulatoria Plan
CREATE VIEW consulta_ambulatoria_plan AS
SELECT
estudios.thhcpanume AS paciente_id, 
estudios.thhccofech AS fecha_consulta,

ori.orindesori AS nomenclador,
practicas.nomecodigo AS codigo_nomenclador,
sec.secndescri AS seccion_nomenclador,
agrupa.thestdesc AS grupo_nomenclador,
practicas.nomedesbre AS descripcion_breve,
practicas.nomedesamp AS descripcion_completa,

estudios.thhcecresu AS preinforme,
estudios.thhcecobse AS detalle,
media.thmdid AS informe_adjunto

FROM thhcexco AS estudios
LEFT JOIN thmdexco media ON (estudios.thhcpanume = media.thhcpanume AND estudios.thhccofech = media.thhccofech AND estudios.secncodigo = media.secncodigo AND estudios.secanumcap = media.secanumcap AND estudios.nomenumsub = media.nomenumsub AND estudios.nomenumsec = media.nomenumsec AND estudios.subicodsub = media.subicodsub AND estudios.orincodigo = media.orincodigo)
INNER JOIN nomencla practicas ON (estudios.secncodigo = practicas.secncodigo AND estudios.secanumcap = practicas.secanumcap AND estudios.nomenumsub = practicas.nomenumsub AND estudios.nomenumsec = practicas.nomenumsec AND estudios.subicodsub = practicas.subicodsub AND estudios.orincodigo = practicas.orincodigo)
LEFT JOIN orinomen ori ON (practicas.orincodigo = ori.orincodigo)
LEFT JOIN secnomen sec ON (practicas.secncodigo = sec.secncodigo)
LEFT JOIN thetnom gru ON (practicas.secncodigo = gru.thetnsecn AND practicas.secanumcap = gru.thetncap AND practicas.nomenumsub = gru.thetnsub AND practicas.nomenumsec =gru.thetnsec AND practicas.subicodsub = gru.thetnsubi AND practicas.orincodigo = gru.thetnorin)
LEFT JOIN thestip agrupa ON (gru.thestcodig = agrupa.thestcodig)

UNION ALL

SELECT
obs.thhcpanume AS paciente_id, 
obs.thhccofech AS fecha_consulta,
NULL AS nomenclador,
NULL AS codigo_nomenclador,
NULL AS seccion_nomenclador,
NULL AS grupo_nomenclador,
NULL AS descripcion_breve,
NULL AS descripcion_completa,
NULL AS preinforme,
obs.thhcobserv AS detalle,
NULL AS informe_adjunto

FROM thhcobsc obs
WHERE (obs.thhcobcodo='P');

-- Servicio Especialidad
CREATE VIEW servicio_especialidad AS
 SELECT thservat.thsercodig AS id_servicio, 
    thservat.thserdescr AS nombre_servicio, 
    especial.especodigo AS id_especialidad, 
    especial.espedescri AS nombre_especialidad
   FROM thseesp
   JOIN thservat ON thservat.thsercodig = thseesp.thsercodig
   JOIN especial ON especial.especodigo = thseesp.especodigo
GROUP BY thservat.thsercodig,thservat.thserdescr,especial.especodigo,especial.espedescri;

-- ICPC2
CREATE VIEW icpc2 AS
SELECT
    icpc.thiccodigo AS id, 
    concat('(',trim(icpc.thiccodigo),') ',icpc_cap.thiccadesc,' ',icpc_com.thiccodesc,' ',icpc.thicdescri) as nombre
    FROM thiccodi icpc
    INNER JOIN thiccapi icpc_cap ON (icpc.thiccacodi = icpc_cap.thiccacodi)
    INNER JOIN thiccomp icpc_com ON (icpc.thiccocodi = icpc_com.thiccocodi);

-- SISTEMA
CREATE VIEW sistema AS
SELECT 
concat(resultado.thexescodi::text,'.',resultado.thexarcodi::text,'.',resultado.thexticodi::text,'.',resultado.thexrecodi::text) as id,
concat(especialidad.thexesdesc,' ',area.thexardesc,' ',
tipo.thextidesc,' ',resultado.thexredesc) as nombre
FROM thexresu resultado 
LEFT JOIN thextipo tipo ON ((resultado.thexescodi = tipo.thexescodi) AND (resultado.thexarcodi = tipo.thexarcodi) AND (resultado.thexticodi = tipo.thexticodi))
LEFT JOIN thexarea area ON ((tipo.thexescodi = area.thexescodi) AND (tipo.thexarcodi = area.thexarcodi))
LEFT JOIN thexespe especialidad ON (area.thexescodi = especialidad.thexescodi);

-- CIE10
CREATE VIEW cie10 AS
SELECT 
cie10.omsucodigo as id,
concat('(',trim(cie10.omsucodigo),') ',cie10.omsudescri) as nombre
FROM omsubcap cie10;

-- NOMENCLADOR
CREATE VIEW nomenclador AS
SELECT
concat(trim(sec.secndescri),'.',trim(nomencla.nomecodigo)) AS id,
concat('(',trim(sec.secndescri),'.',trim(nomencla.nomecodigo),') ',nomencla.nomedesamp) AS nombre
FROM nomencla
LEFT JOIN secnomen sec ON (nomencla.secncodigo = sec.secncodigo)
where coalesce( trim(nomencla.nomedesamp),'')<>'';

-- DOMICILIO
CREATE VIEW domicilio AS
SELECT domicilio.empenumero AS paciente_id,
concat(btrim(concat(btrim(domicilio.emdocanomb::text), ' ', btrim(
        CASE
            WHEN (btrim(domicilio.emdonumero::text) = '0'::text) IS FALSE THEN domicilio.emdonumero
            ELSE NULL::bpchar
        END::text), ' ', btrim(
        CASE
            WHEN (btrim(domicilio.emdocuerpo::text) = ''::text) IS FALSE THEN 'cpo:'::text
            ELSE NULL::text
        END), btrim(domicilio.emdocuerpo::text), ' ', btrim(
        CASE
            WHEN (btrim(domicilio.emdopiso::text) = ''::text) IS FALSE THEN 'piso:'::text
            ELSE NULL::text
        END), btrim(domicilio.emdopiso::text), ' ', btrim(
        CASE
            WHEN (btrim(domicilio.emdodepto::text) = ''::text) IS FALSE THEN 'dto:'::text
            ELSE NULL::text
        END), btrim(domicilio.emdodepto::text)))) AS domicilio,
domicilio.emdocopost::text AS cp,
localida.locadescri::text AS localidad,
pcias.pciadescri::text AS provincia
FROM emdomici domicilio
LEFT JOIN localida ON localida.locacodigo = domicilio.emdolocodi
LEFT JOIN pcias ON pcias.pciacodigo = localida.pciacodigo
WHERE domicilio.emdofecvig::date > domicilio.emdofecbaj::date OR domicilio.emdofecbaj IS NULL
ORDER BY (domicilio.emdofecvig::date) DESC;
-- utilizar con LIMIT 1

-- TELEFONO
CREATE VIEW telefono AS
SELECT telefono.empenumero AS paciente_id,
btrim(telefono.emtenumcom::text) as telefono,
btrim(tipo.emticodesc::text) as tipo
FROM emtelefo telefono
JOIN emticont tipo ON tipo.emticocodi = telefono.emticocodi
WHERE telefono.emtefecvig::date > telefono.emtefecbaj::date OR telefono.emtefecbaj IS NULL
ORDER BY (telefono.emtefecvig::date) DESC;
-- utilizar con LIMIT 1


