#!/bin/sh
# Script para generar tablas locales a partir de las vistas en cada hospital
# ./datos.sh  # genera datos.log con el resultado
# se debe ejecutar con cron a las 3 de la tarde y a las 3 de la mañana

cd /var/www/html/mpi/scripts/mpi

inicio=`date`
echo "--- inicio $inicio" > datos.log
ok=1
error="ERRORES\n"

# arrays en /bin/sh, feo asco
cant_hospital="1 2 3 4"
# viedma
hospital1="hospitalviedma"
db_hospital1="ALTEA_SP_RN_PROD"
ip1="172.16.0.5"
# bariloche
hospital2="hospitalbariloche"
db_hospital2="ALTEA_RN_PROD"
ip2="172.16.0.2"
# cipolletti
hospital3="hospitalcipolletti"
db_hospital3="ALTEA_RN_PROD"
ip3="172.16.0.3"
# roca
hospital4="hospitalroca"
db_hospital4="ALTEA_RN_PROD"
ip4="172.16.0.4"

cant_vistas="01 02 03 04 05 06 07 08 09 10 11 12 13 14"

vista01="consulta_ambulatoria_master"
vista02="consulta_ambulatoria_analisis"
vista03="consulta_ambulatoria_objetivo"
vista04="consulta_ambulatoria_plan"
vista05="consulta_ambulatoria_subjetivo"
vista06="cie10"
vista07="icpc2"
vista08="nomenclador"
vista09="sistema"
vista10="servicio_especialidad"
vista11="domicilio"
vista12="telefono"
vista13="maestro_paciente"
#nueva vista de keko
vista14="internacion"
vista15="odontologia"
vista16="consulta_migration"

for ih in $cant_hospital; do

    ### para cada hospital y cada tabla, ejecuto el comando

    for iv in $cant_vistas; do

        eval hospital="\$hospital$ih"
        eval db_hospital="\$db_hospital$ih"
        eval ip="\$ip$ih"
        eval vista="\$vista$iv"


        echo "$ip ($hospital): ($iv) $vista" >> datos.log

        # if telnet a 5432 ok 
        netcat -z ${ip} 5432
        if [ $? -eq 0 ]; then

            # creo tabla temporal en hospital para guardar datos y estructura de la vistas
            psql "host=$ip port=5432 dbname=$db_hospital user=postgres password=omega" -c "drop table if exists v_$vista;" >> datos.log 2>> datos.log
            psql "host=$ip port=5432 dbname=$db_hospital user=postgres password=omega" -c "DO $$ 
            BEGIN
            IF EXISTS (SELECT 1 FROM information_schema.views WHERE table_name = $vista) THEN
            EXECUTE 'CREATE TABLE v_$vista AS SELECT * FROM $vista';
            END IF;
END $$;" >> datos.log 2>> datos.log

            # me traigo la estructura y los datos
            # como estaba el export antes: PGPASSWORD="omega" pg_dump --verbose --host=$ip --port=5432 --dbname=$db_hospital --username=postgres --schema=public --table=v_$vista --file v_$vista.sql 2>>datos.log
            PGPASSWORD="omega" pg_dump -U postgres -h $ip -p 5432 --db $db_hospital  --verbose    -t public.v_$vista > v_$vista.sql
            if [ $? -eq 0 ]; then
                    # si utilizo bases separadas para hacer el restore
                    # drop y create local
                    #psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -c "drop table if exists v_$vista;" >> datos.log 2>> datos.log
                    #psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -v ON_ERROR_STOP=1 -f v_$vista.sql >>datos.log 2>> datos.log

                    #rename local si todo ok
                    #if [ $? -eq 0 ]; then
                    #    psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -c "drop table if exists $vista;" >> datos.log 2>> datos.log
                    #    psql "host=10.11.49.78 port=5432 dbname=$hospital user=postgres password=omega" -c "alter table v_$vista rename to $vista;" >> datos.log 2>> datos.log
                    #else
                    #    echo "ERROR DE RESTORE: $ip ($hospital): ($iv) $vista" >> datos.log
                    #    ok=0
                    #    error="$error ERROR DE RESTORE: $ip ($hospital): ($iv) $vista\n"
                    #fi

                # para esquema dentro de la base principal mpi
                sed -i -e "s/public.v_$vista/$hospital.v_$vista/" v_$vista.sql >> datos.log 2>> datos.log

                # drop y create local
                PGOPTIONS="--search_path=$hospital" psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "drop table if exists v_$vista;" >> datos.log 2>> datos.log
                PGOPTIONS="--search_path=$hospital" psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -v ON_ERROR_STOP=1 -f v_$vista.sql >>datos.log 2>> datos.log

                #rename local si todo ok
                if [ $? -eq 0 ]; then

                    PGOPTIONS="--search_path=$hospital" psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "drop table if exists $vista;" >> datos.log 2>> datos.log
                    PGOPTIONS="--search_path=$hospital" psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "alter table v_$vista rename to $vista;" >> datos.log 2>> datos.log
                    PGOPTIONS="--search_path=$hospital" psql "host=10.11.49.78 port=5432 dbname=mpi user=postgres password=omega" -c "COPY (SELECT * FROM consulta_migration) TO E'./consulta_migration_bariloche.csv';" >> datos.log 2>> datos.log
                else
                    echo "ERROR DE RESTORE: $ip ($hospital): ($iv) $vista" >> datos.log
                    ok=0
                    error="$error ERROR DE RESTORE: $ip ($hospital): ($iv) $vista\n"
                fi
            else
                echo "ERROR DE DUMP: $ip ($hospital): ($iv) $vista" >> datos.log
                ok=0
                error="$error ERROR DE DUMP: $ip ($hospital): ($iv) $vista\n"
            fi

            rm v_$vista.sql

        else
            echo "ERROR DE CONEXION: $ip ($hospital): ($iv) $vista" >> datos.log
            ok=0
            error="$error ERROR DE CONEXION: $ip ($hospital): ($iv) $vista\n"
        fi

    done

done

# send email
if [ $ok -eq 0 ]; then
    ./sendEmail -o tls=no -f info@salud.rionegro.gov.ar -t nsegura@salud.rionegro.gov.ar -u "Sicronizacion MPI" -m "$error" -s 10.11.49.8
fi
if [ $ok -eq 1 ]; then
    ./sendEmail -o tls=no -f info@salud.rionegro.gov.ar -t nsegura@salud.rionegro.gov.ar -u "Sincronizacion MPI" -m "OK" -s 10.11.49.8
fi

fin=`date`
echo "--- fin $fin" >> datos.log
