<?php
// Empadronamiento. Actualizacion de la base de datos maestro con los datos de los hospitales.

require_once 'classPgSql.php';

echo "Inicio empadronamiento. ".date("Y-m-d H:i:s")."\n";

$hospitales = array(
    100 => "hospitalviedma",
    200 => "hospitalbariloche",
    300 => "hospitalroca",
    400 => "hospitalcipolletti"
);

$pg_maestro = new PgSql("localhost", 5432, "mpi", "postgres", "omega");

foreach ($hospitales as $id_area => $base_hospital) {

    // base_hospital , seria el schema hospital
    $sentenceSQL = "select max(empenumero) as maximo_numero from ".$base_hospital.".maestro_paciente;";
    $row = $pg_maestro->getResource($sentenceSQL);
    $r = pg_fetch_assoc($row);
    $maximo_numero = $r['maximo_numero'];

    echo "Maximo ID de ".$base_hospital.": ".$maximo_numero."\n";

    $bloque=10000;
    $offset=0;
    while (true){
        /*
        $sentenceSQL = "
        select per.empenumero,per.empeapelli,per.empenombre,per.empefecnac,per.empesexo,per.embajcodig,per.empefecbaj,doc.emdctdcodi,doc.emdcnumero,
        string_agg(concat(hc.emphccacod,':',trim(hc.emphcnro)),'|' order by hc.emphccacod) as numero_hc
        from public.emperson as per
        left join (SELECT DISTINCT ON (dc.empenumero) * FROM public.emdocume as dc ORDER BY dc.empenumero, dc.emdcfecvig DESC NULLS LAST) as doc ON doc.empenumero=per.empenumero
        left join public.emphc as hc on hc.empenumero=per.empenumero
        group by per.empenumero, per.empeapelli,per.empenombre,per.empefecnac,per.empesexo,per.embajcodig,per.empefecbaj,doc.emdctdcodi,doc.emdcnumero
        order by per.empenumero
        limit $bloque offset $offset;";
        */

        $sentenceSQL = "
        select empenumero,empeapelli,empenombre,empefecnac,empesexo,
        embajcodig,empefecbaj,emdctdcodi,emdcnumero,numero_hc
        from ".$base_hospital.".maestro_paciente
        order by empenumero
        limit $bloque offset $offset;";

        $rows = $pg_maestro->getRows($sentenceSQL);  // devuelve array
    
        foreach ($rows as $datos_hospital){

            // datos nuevos
            if (strpos(trim($datos_hospital["empeapelli"]), ' ')){
                $primer_apellido = trim(substr(trim($datos_hospital["empeapelli"]), 0, strpos(trim($datos_hospital["empeapelli"]), ' ')));
                $segundo_apellido = trim(substr(trim($datos_hospital["empeapelli"]), strpos(trim($datos_hospital["empeapelli"]), ' ')));
            }else{
                $primer_apellido = trim($datos_hospital["empeapelli"]);
                $segundo_apellido = "";
            }
            if (strpos(trim($datos_hospital["empenombre"]), ' ')){
                $primer_nombre = trim(substr(trim($datos_hospital["empenombre"]), 0, strpos(trim($datos_hospital["empenombre"]), ' ')));
                $segundo_nombre = trim(substr(trim($datos_hospital["empenombre"]), strpos(trim($datos_hospital["empenombre"]), ' ')));
            }else{
                $primer_nombre = trim($datos_hospital["empenombre"]);
                $segundo_nombre = "";
            }

            $fecha_nacimiento = ($datos_hospital["empefecnac"]) ? "'" . $datos_hospital["empefecnac"] . "'" : "null";

            $sexo = trim(mapeo_sexo($datos_hospital["empesexo"]));
            $id_federado_provincial = $datos_hospital["empenumero"];
        
            $documento_tipo = ($datos_hospital) ? trim(mapeo_documento($datos_hospital["emdctdcodi"])) : "";
            $documento_numero = ($datos_hospital) ? trim($datos_hospital["emdcnumero"]) : "";
            $numero_hc = trim($datos_hospital['numero_hc']);

            $estado = ($datos_hospital["embajcodig"]) ? $datos_hospital["embajcodig"] : "null";

            // fin datos nuevos

            // busco en base maestro
            $sentenceSQL = "select * from public.maestro where id_federado_provincial=$id_federado_provincial and id_area=$id_area;";
            $row = $pg_maestro->getResource($sentenceSQL);
            $datos_maestro = pg_fetch_assoc($row);

            if ($datos_maestro) {    // encontre paciente

                // Comparo los datos principales con los locales
                $fecha_nacimiento_comparacion = ($datos_maestro["fecha_nacimiento"]) ? "'" . $datos_maestro["fecha_nacimiento"] . "'" : "null";

                $estado_comparacion = ($datos_maestro["estado"]) ? $datos_maestro["estado"] : "null";

                if (!(trim($datos_maestro['primer_apellido']) == $primer_apellido and
                    trim($datos_maestro['segundo_apellido']) == $segundo_apellido and
                    trim($datos_maestro['primer_nombre']) == $primer_nombre and
                    trim($datos_maestro['segundo_nombre']) == $segundo_nombre and
                    $fecha_nacimiento_comparacion == $fecha_nacimiento and
                    trim($datos_maestro['sexo']) == $sexo and
                    trim($datos_maestro['documento_tipo']) == $documento_tipo and
                    trim($datos_maestro['documento_numero']) == $documento_numero )) {

                    // se modifico

                    if ($datos_maestro["flag_federado"]=='t') {
                        // E1 Registro federado, solo actualizo flag_modificado, hc y estado
                        $logSQL="insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('empadronamiento','E1','Registro federado, solo actualizo flag_modificado','".$base_hospital."',$id_federado_provincial,".$datos_maestro['id_maestro'].",CURRENT_TIMESTAMP)";

                        $sentenceSQL = "update public.maestro set
                                        numero_hc='".pg_escape_string($numero_hc)."',
                                        estado=$estado,
                                        flag_modificado=true,
                                        stamp_flag_modificado=CURRENT_TIMESTAMP
                                        where id_federado_provincial=$id_federado_provincial and id_area=$id_area;";
                    } else {
                        // E2 Registro no federado, actualizo datos y reseteo flags
                        $logSQL="insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('empadronamiento','E2','Registro no federado, actualizo datos y reseteo flags','".$base_hospital."',$id_federado_provincial,".$datos_maestro['id_maestro'].",CURRENT_TIMESTAMP)";
    
                        $sentenceSQL = "update public.maestro set
                                            primer_apellido='".pg_escape_string($primer_apellido)."',
                                            segundo_apellido='".pg_escape_string($segundo_apellido)."',
                                            primer_nombre='".pg_escape_string($primer_nombre)."',
                                            segundo_nombre='".pg_escape_string($segundo_nombre)."',
                                            fecha_nacimiento=$fecha_nacimiento,
                                            sexo='$sexo',
                                            documento_tipo='$documento_tipo',
                                            documento_numero='".pg_escape_string($documento_numero)."',
                                            numero_hc='".pg_escape_string($numero_hc)."',
                                            estado=$estado,
                                            flag_modificado=true,
                                            flag_autenticado=false,
                                            flag_federado=false,
                                            stamp_flag_modificado=CURRENT_TIMESTAMP,
                                            stamp_flag_autenticado=CURRENT_TIMESTAMP
                                            where id_federado_provincial=$id_federado_provincial and id_area=$id_area;";
                    }

                    $ok = $pg_maestro->exec($sentenceSQL);

                    if ($ok) { //error
                        $pg_maestro->insert($logSQL, "id");
                    }else{

                        // E3 Registro a modificar, error update en la base de datos
                        // error en base de datos, escribo log.txt e intento insert en logscript
                        echo "E3 empadronamiento - Registro a modificar, error update en la base de datos ".$base_hospital.":".$id_federado_provincial." id_maestro:".$datos_maestro['id_maestro']." ".date("Y-m-d H:i:s")."\n";

                        $logSQL="insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                        ('empadronamiento','E3','Registro a modificar, error update en la base de datos','".$base_hospital."',$id_federado_provincial,".$datos_maestro['id_maestro'].",CURRENT_TIMESTAMP)";

                        $pg_maestro->insert($logSQL, "id");
                    }

                } else {

                    // no se modificaron los datos principales, verifico estado
                    // Si estado = 1 (unificado), flag modificado = true, ver trataminto de unificados
                    // los unificados, pueden estar federados previamente, se hace un relacion en el soft
                    // else ( estado = 2 fallecido o estado = 0 activo), queda todo como esta
                    // en este caso, si el paciente fue federado, paso flag_modificado a false, para que no me 
                    // aparezca como inconsistencia

                    if ($estado==1){
                        // flag modificado true, nuevo estado = 1, unificado
                        $sentenceSQL = "update public.maestro set
                        numero_hc='".pg_escape_string($numero_hc)."',
                        estado=$estado,
                        flag_modificado=true
                        where id_federado_provincial=$id_federado_provincial and id_area=$id_area;";
                    }else{
                        if ($datos_maestro["flag_federado"]=='t'){
                            // guardo estado y nro hc, y flag_modificado=false
                            $sentenceSQL = "update public.maestro set
                            numero_hc='".pg_escape_string($numero_hc)."',
                            estado=$estado,
                            flag_modificado=false
                            where id_federado_provincial=$id_federado_provincial and id_area=$id_area;";
                        }else{
                            // guardo estado y nro hc, pero no toco flag
                            $sentenceSQL = "update public.maestro set
                            numero_hc='".pg_escape_string($numero_hc)."',
                            estado=$estado
                            where id_federado_provincial=$id_federado_provincial and id_area=$id_area;";
                        }
                    }
    
                    $ok = $pg_maestro->exec($sentenceSQL);

                    if (!$ok) {
                        // Error update en la base de datos, hc y estado
                        echo "Error update HC y Estado.".$base_hospital.":".$id_federado_provincial." id_maestro:".$datos_maestro['id_maestro']." ".date("Y-m-d H:i:s")."\n";
                    }

                }

            } else { // paciente no encontrado. lo agrego
                $sentenceSQL = "insert into public.maestro (
                                            primer_apellido,
                                            segundo_apellido,
                                            primer_nombre,
                                            segundo_nombre,
                                            fecha_nacimiento,
                                            sexo,
                                            documento_tipo,
                                            documento_numero, 
                                            numero_hc,
                                            estado,
                                            id_area, 
                                            id_federado_provincial,
                                            flag_modificado,
                                            flag_autenticado,
                                            flag_federado,
                                            stamp_flag_modificado
                                   ) values (
                                            '".pg_escape_string($primer_apellido)."',
                                            '".pg_escape_string($segundo_apellido)."',
                                            '".pg_escape_string($primer_nombre)."',
                                            '".pg_escape_string($segundo_nombre)."',
                                             $fecha_nacimiento,
                                            '$sexo',
                                            '$documento_tipo',
                                            '".pg_escape_string($documento_numero)."',
                                            '".pg_escape_string($numero_hc)."',
                                             $estado,
                                             $id_area,
                                             $id_federado_provincial,
                                             true,
                                             false,
                                             false,
                                            CURRENT_TIMESTAMP
                                   )";
                $id_insert = $pg_maestro->insert($sentenceSQL, "id_maestro");
                if (!$id_insert) { //error
                    // E4 Registro a insertar, error insert en la base de datos
                    // error en base de datos, escribo log.txt e intento insert en logscript
                    echo "E4 empadronamiento - Registro a insertar, error inserte en la base de datos ".$base_hospital.":".$id_federado_provincial." id_maestro: 0 ".date("Y-m-d H:i:s")."\n";

                    $logSQL="insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('empadronamiento','E4','Registro a insertar, error insert en la base de datos','".$base_hospital."',$id_federado_provincial,0,CURRENT_TIMESTAMP)";
                    $pg_maestro->insert($logSQL, "id");
                }else {

                    // E5 Registro nuevo ingresado correctamente
                    $logSQL="insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
                    ('empadronamiento','E5','Registro nuevo ingresado correctamente','".$base_hospital."',$id_federado_provincial,$id_insert,CURRENT_TIMESTAMP)";

                    $pg_maestro->insert($logSQL, "id");

                }
            }

        }

        if (count($rows)<>$bloque){
            // termino el while principal
            break;
        }else{
            // aumento offset
            $offset=$offset+$bloque;
        }

    }

}
// cierrro base maestro
$pg_maestro->close();
// fin empadronamiento

// abro base para verificacion posterior
$pg_maestro = new PgSql("localhost", 5432, "mpi", "postgres", "omega");

// verifico pacientes estado = unificado y que no fueron federados,
// cambio el flag_modificado a false para que no se intente federacion
$sentenceSQL = "update public.maestro set
                flag_modificado=false,
                flag_autenticado=false,
                stamp_flag_modificado=CURRENT_TIMESTAMP,
                stamp_flag_autenticado=CURRENT_TIMESTAMP
                where estado=1 and flag_federado='f';";

$ok = $pg_maestro->exec($sentenceSQL);

if (!$ok) { //error
    // E6 Error en la actualizacion de unificados. Error update en la base de datos

    // error en base de datos, escribo log.txt e intento insert en logscript
    echo "E6 empadronamiento - Error en la actualizacion de Unificados. Error update en la base de datos ".$base_hospital." ".date("Y-m-d H:i:s")."\n";

    $logSQL="insert into public.logscript (script,codigo_error,descripcion,base_hospital,id_hospital,id_maestro,timestamp) values 
    ('empadronamiento','E6','Error en la actualizacion de unificados. Error update en la base de datos','".$base_hospital."',0,0,CURRENT_TIMESTAMP)";

    $pg_maestro->insert($logSQL, "id");

}


$fecha = $hoy = date("Y-m-d"); 
$sentenceSQL = "select codigo_error,descripcion,count(*) as cantidad from logscript where script='empadronamiento' and timestamp::date='$fecha' group by codigo_error,descripcion;";
$datos = $pg_maestro->getRows($sentenceSQL);
foreach ($datos as $row) {     // $i -> indice id_maestro
    echo $row['codigo_error']." ".str_pad(trim($row['cantidad']),6," ",STR_PAD_LEFT)." ".$row['descripcion']."\n";
}

// cierrro base maestro
$pg_maestro->close();

echo "Fin empadronamiento. ".date("Y-m-d H:i:s")."\n";
// fin script empadronamiento.php
die();

function mapeo_sexo($sexo_hospital)
{
    $ret = "";    // default
    if ($sexo_hospital == "F") {
        $ret = "female";
    }
    if ($sexo_hospital == "M") {
        $ret = "male";
    }
    return $ret;
}

function mapeo_documento($tipo_documento)
{
    $ret = "dni"; // default
    switch ($tipo_documento) {
        case "DNI":
            $ret = "dni";
            break;
        case "CI":
            $ret = "ci";
            break;
        case "LE":
            $ret = "le";
            break;
        case "OTR":
            $ret = "otr";
            break;
        case "PAS":
            $ret = "pas";
            break;
        case "LC":
            $ret = "lc";
            break;
        case "IN":
            $ret = "in";
            break;
    }
    return $ret;
}
?>
