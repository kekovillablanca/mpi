<?php
/* 
Correccion inconsistencias, pacientes que se federaron y luego el hospital los modifico.
Ingreso en la base del hopsital y reacomodo los datos.
Menos los que se unificaron, esto debo hacerlos a traves del federador.
*/
require_once 'classPgSql.php';

echo "Inicio correcion inconsistencias. ".date("Y-m-d H:i:s")."\n";

$pg_maestro = new PgSql("localhost", 5434, "mpi_estadistica", "keko", "427424");

$sentenceSQL = "
select id_maestro, documento_tipo, documento_numero, primer_apellido, segundo_apellido, 
primer_nombre, segundo_nombre, fecha_nacimiento, sexo, estado, id_area, id_federado_provincial 
from maestro where flag_federado='t' and flag_modificado='t' and (estado <> 1 or estado IS NULL);";

$rows = $pg_maestro->getRows($sentenceSQL);  // devuelve array
    
foreach ($rows as $datos_maestro){

    $id_area=$datos_maestro['id_area'];
    $empenumero=$datos_maestro['id_federado_provincial'];
    $empeapelli=trim($datos_maestro['primer_apellido']." ".$datos_maestro['segundo_apellido']);
    $empenombre=trim($datos_maestro['primer_nombre']." ".$datos_maestro['segundo_nombre']);
    $empefecnac=$datos_maestro['fecha_nacimiento'];
    $empesexo=mapeo_sexo(trim($datos_maestro["sexo"]));
    $emdctdcodi=mapeo_documento(trim($datos_maestro["documento_tipo"]));
    $emdcnumero=trim($datos_maestro['documento_numero']);
                    
    // Selecciono la base a la que me tengo que conectar
    switch($id_area)
    {
        case 100: // Viedma
            $pg_hospital = new PgSql("172.16.0.5", 5432, "ALTEA_SP_RN_PROD", "postgres", "omega");
        break;
        case 200: // Bariloche
            $pg_hospital = new PgSql("172.16.0.2", 5432, "ALTEA_RN_PROD", "postgres", "omega");
        break;
        case 300:  // Roca
            $pg_hospital = new PgSql("172.16.0.4", 5432, "ALTEA_RN_PROD", "postgres", "omega");
        break;
        case 400:  // Cipo
            $pg_hospital = new PgSql("172.16.0.3", 5432, "ALTEA_RN_PROD", "postgres", "omega");
        break;
    }

    // ejecuto select de busqueda de id (numero y time)

    $sentenceSQL="
    select per.empenumero, doc.emdcfecvig
    from public.emperson as per
    left join (SELECT DISTINCT ON (dc.empenumero) * FROM public.emdocume as dc ORDER BY dc.empenumero, dc.emdcfecvig DESC NULLS LAST) as doc ON doc.empenumero=per.empenumero
    left join public.emphc as hc on hc.empenumero=per.empenumero
    where per.empenumero = $empenumero;";
        
    $row = $pg_hospital->getResource($sentenceSQL);
    $datos_hospital = pg_fetch_assoc($row);
    
    if ($datos_hospital) {      // encontre paciente

        $numero=$datos_hospital['empenumero'];
        $fecvig=$datos_hospital['emdcfecvig'];
  
        if ($fecvig and $numero) {   // el paciente tiene numero y documento con fecha de vigencia. 

            // con los id, hago los updates
            $sentenceSQL = "update public.emperson set
            empeapelli='".$empeapelli."',
            empenombre='".$empenombre."',
            empefecnac='".$empefecnac."',
            empesexo='".$empesexo."'
            where empenumero = $numero;";

            $ok = $pg_hospital->exec($sentenceSQL);

            if ($ok) { //error
                echo "Update correctamente emperson en area ".$id_area.". Paciente ".$numero."."."\n";
            }else{
                echo "Error update emperson en area ".$id_area.". Paciente ".$numero."."."\n";
            }

            // segunda parte , tipo y numero de doc
            $sentenceSQL = "update public.emdocume set
            emdcnumero='".$emdcnumero."',
            emdctdcodi='".$emdctdcodi."'
            where empenumero = $numero and emdcfecvig='".$fecvig."';";
        
            $ok = $pg_hospital->exec($sentenceSQL);

            if ($ok) { 
                echo "Update correctamente emdocume en area ".$id_area.". Paciente ".$numero."."."\n";
            }else{
                echo "Error update emdcoume en area ".$id_area.". Paciente ".$numero."."."\n";
            }

        }else{

            // el paciente no tiene documento cargado. le hago un insert en la tabla de documentos
            $sentenceSQL = "insert into public.emdocume (
                empenumero,
                emdcfecvig,
                emdcfecbaj,
                emdctdcodi,
                emdcnumero,
                emdcmfecha,
                emdcmusuar,
                empetipo,
                emdcexport
                ) values (
                $numero,
                CURRENT_TIMESTAMP,
                '0001-01-01 00:00:00',
                '$emdctdcodi',
                '$emdcnumero',
                CURRENT_TIMESTAMP,
                'MPI',
                'F',
                0)";

            $id_insert = $pg_hospital->insert($sentenceSQL, "empenumero");
            if ($id_insert) { //error
                echo "Insert correctamente emdocume en area ".$id_area.". Paciente ".$numero."."."\n";
            }else {
                echo "Error insert emdocume en area ".$id_area.". Paciente ".$numero."."."\n";
            }
            
        }    
    }
    
    // cierro base hospital
    $pg_hospital->close();

}

$pg_maestro->close();

echo "Fin correcion inconsistencias. ".date("Y-m-d H:i:s")."\n";
// fin script empadronamiento.php
die();


function mapeo_sexo($sexo)
{
    $ret = "";    // default
    if ($sexo == "female") {
        $ret = "F";
    }
    if ($sexo == "male") {
        $ret = "M";
    }
    return $ret;
}

function mapeo_documento($tipo_documento)
{
    $ret = "DNI"; // default
    switch ($tipo_documento) {
        case "dni":
            $ret = "DNI";
            break;
        case "ci":
            $ret = "CI";
            break;
        case "le":
            $ret = "LE";
            break;
        case "otr":
            $ret = "OTR";
            break;
        case "pas":
            $ret = "PAS";
            break;
        case "lc":
            $ret = "LC";
            break;
        case "in":
            $ret = "IN";
            break;
    }
    return $ret;
}
?>
