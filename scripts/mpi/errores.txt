Codigo de ERRORES

Empadronamiento

E1 federado, solo actualizo flag_modificado
E2 no federado, actualizo datos y reseteo json, flags, etc.
E3 error update en la base de datos
E4 error insert en la base de datos
E5 nuevo ingresado correctamente
E6 Error en la actualizacion de Unificados

Autenticacion

A1 Registro autenticado
A2 Registro no autenticado
A3 Renaper sin respuesta.
A4 Error de comunicacion.
A5 Error en el proceso de autenticacion.
A6 Error update en la base de datos

Federacion

F1 Registro federado
F2 Es el mismo registro. Verificar manualmente
F3 Paciente duplicado en el dominio
F4 Error en el proceso de federacion
F5 Error de comunicacion
F6 Error update en la base de datos

ERROR DE COMUNICACION AL BUS

0 . Sin error
1 . Error de comunicacion.
2 . Paciente duplicado en el dominio. 
3 . Error en el proceso Federacion.
4 . Error en el proceso Autenticacion.
5 . Renaper sin respuesta. Datos no encontrados
6 . Federador sin respuesta.
7 . Error de datos
