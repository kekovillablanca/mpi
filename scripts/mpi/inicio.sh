#!/bin/bash
#Ejecutar el script de esta forma, como root.
#Con este comando se ejecuta el script en segundo plano y puedo cerrar la termina
#nohup ./inicio.sh > inicio.log &

cd /var/www/html/mpi/scripts/mpi
./datos.sh
./control.sh
php empadronamiento.php
php autenticacion.php
php federacion.php
php correccion.php
