<?php declare(strict_types=1);

require_once 'XBase/Column/DBaseColumn.php';
require_once 'XBase/Enum/FieldType.php';

class VisualFoxproColumn extends DBaseColumn
{
    public function getDataLength()
    {
        switch ($this->type) {
            case FieldType::BLOB:
            case FieldType::MEMO:
                return 4;
            default:
                return parent::getDataLength();
        }
    }
}
