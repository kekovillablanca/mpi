<?php


require_once 'XBase/Record/DBaseRecord.php';
require_once 'XBase/Column/ColumnInterface.php';
require_once 'XBase/Enum/FieldType.php';

class DBase4Record extends DBaseRecord
{
    public function getObject(ColumnInterface $column)
    {
        switch ($column->getType()) {
            case FieldType::FLOAT:
                return $this->getFloat($column->getName());
            default:
                return parent::getObject($column);
        }
    }

    /**
     * @return int
     */
    public function getFloat(string $columnName)
    {
        return (float) ltrim($this->choppedData[$columnName]);
    }
}
