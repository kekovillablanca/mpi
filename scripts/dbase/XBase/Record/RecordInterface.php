<?php

require_once 'XBase/Column/ColumnInterface.php';

interface RecordInterface
{
    public function isDeleted(): bool;

    public function getString(string $columnName);

    public function getObject(ColumnInterface $column);
}
