<?php
// Conversion de DBF
// Ejecutar por hospital
// Vaciar la tablas dbf
// modificar variables 

require_once 'PgSql.php';
require_once 'XBase/WritableTable.php';

$base_hospital="hospitalroca";  // hospitalviedma, hospitalbariloche, hospitalroca,hospitalcipolletti
$desde='2019-01-01';
$hasta='2019-12-31';
$hospital='HOSPITAL GRAL ROCA';  // VIEDMA, GRAL ROCA, CIPLLETTI, BARILOCHE
$archivo='roca.dbf';  // viedma, roca, cipo, barilo

echo "Inicio conversion\n";

    $pg_hospital = new PgSql("localhost", 5432, "mpi", "postgres", "omega");

    $sentenceSQL = "select count(*) as cantidad from consulta_ambulatoria_master
    where ".$base_hospital.".consulta_ambulatoria_master.fecha>='$desde' and consulta_ambulatoria_master.fecha<='$hasta';";

    $rows = $pg_hospital->getRows($sentenceSQL);  // devuelve array
    echo "Cantidad: ".$rows[0]['cantidad']."\n";

    $table = new WritableTable($archivo);
    $table->openWrite();

    $i=1;  // numero de planilla que no es importante
    $record = $table->appendRecord();  // agrego un registro vacio por que la clase anda mal
    $record->numero = $i;
    $table->writeRecord();

    $bloque=1000;
    $offset=0;
    while (true){

        $sentenceSQL = "select consulta_ambulatoria_master.paciente_numero_hc as hc, 
        concat(trim(consulta_ambulatoria_master.paciente_ape),' ',trim(consulta_ambulatoria_master.paciente_nom)) as apellido, 
        consulta_ambulatoria_master.paciente_nac as fecha_nac, 
        consulta_ambulatoria_master.paciente_sexo as sexo, 
        consulta_ambulatoria_master.paciente_tipodoc as tipo_doc, 
        consulta_ambulatoria_master.paciente_numerodoc as numero_doc, 
        consulta_ambulatoria_master.fecha as fecha, 
        consulta_ambulatoria_master.servicio as servicio, 
        consulta_ambulatoria_master.especialidad as especialidad, 
        concat(trim(consulta_ambulatoria_master.medico_ape),' ',trim(consulta_ambulatoria_master.medico_nom)) as profesional 
        from ".$base_hospital.".consulta_ambulatoria_master 
        where consulta_ambulatoria_master.fecha>='$desde' and consulta_ambulatoria_master.fecha<='$hasta'
        order by consulta_ambulatoria_master.fecha
        limit $bloque offset $offset;";

        $rows = $pg_hospital->getRows($sentenceSQL);  // devuelve array

        foreach ($rows as $datos_hospital){

            $record = $table->appendRecord();
            // calculo de edad
            $date2 = new DateTime($datos_hospital['fecha']);
            $date1 = new DateTime($datos_hospital['fecha_nac']);

            $fecha = date("d-m-Y", strtotime($datos_hospital['fecha']));
            
            $sexo=$datos_hospital['sexo'];
            if (empty($sexo))
                $sexo='M';
            
            $diff = $date1->diff($date2);

            $edad = round(intval($diff->days)/365,4);
            $especialidad = substr($datos_hospital['especialidad'],0,10);
            $medico = substr($datos_hospital['profesional'],0,10);
            $apellido = substr($datos_hospital['apellido'],0,30);

	        $record->numero = $i;
		    $record->hospital=$hospital;
            $record->puesto='HOSPITAL';
            $record->servicio=$especialidad;
            $record->medico=$medico;
            $record->fecha=$fecha;

            $hc_aux1=explode("|",$datos_hospital['hc']);
            $hc_aux2=explode(":",$hc_aux1[0]);

            if ($hc_aux2[1]){
                $record->nrohc=intval($hc_aux2[1]);
            }else{
                $record->nrohc=0;
            }

            $record->nrodoc=intval($datos_hospital['numero_doc']);
            $record->apellido=$apellido;

            $record->edad=$edad;
            $record->cdgosexo=$sexo;
            $record->obra=0;
            $record->primera=0;
            $record->cemb=0;
            $record->cns=0;
            $record->diag=0;

            $table->writeRecord();
        
        }

        if (count($rows)<>$bloque){
            // termino el while principal
            break;
        }else{
            // aumento offset
            $offset=$offset+$bloque;
        }

    }

    // cierro base de datos del hospital y abro la nueva en el proximo ciclo
    $pg_hospital->close();

    $table->close();
    echo "Fin Conversion\n";

?>
