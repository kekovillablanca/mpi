<?php

use kartik\datecontrol\Module;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'MPI Provincial',
    'language' => 'es',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [

        'assetManager' => [
            'bundles' => [
                'kartik\form\ActiveFormAsset' => [
                    'bsDependencyEnabled' => false // do not load bootstrap assets for a specific asset bundle
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'fS6f7r3dK0JqnrRkrB3IqO65v2BOiniP',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.salud.rionegro.gov.ar',
                'username' => 'evillablanca@salud.rionegro.gov.ar',
                'password' => 'evillablanca',
                'port' => '25',
              ],
              'useFileTransport' => false,
        ],
     
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        ],
        
    //],
    'params' => $params,
    'modules' => [
            // custom widget settings that will be used to render the date input instead of kartik\widgets,
            // this will be used when autoWidget is set to false at module or widget level.
      

	    'gridview' => [
	        'class' => \kartik\grid\Module::class,
            'downloadAction' => 'gridview/export/download',
        ],
        'admin' => [
            'class' => 'app\modules\admin\admin',
            // ... other configurations for the module ...
        ],
        'mpi' => [
            'class' => 'app\modules\mpi\mpi',
            // ... other configurations for the module ...
        ],
        'sies' => [
            'class' => 'app\modules\sies\sies',
            // ... other configurations for the module ...
        ],
        'monitor' => [
            'class' => 'app\modules\monitor\monitor',
            // ... other configurations for the module ...
        ],
	    'apiv1' => [
            'class' => 'app\modules\apiv1\apiv1module',
            // ... other configurations for the module ...
        ],
        'internacion' => [
            'class' => 'app\modules\internacion\InternacionModule',
            // ... other configurations for the module ...
        ],
    ],
];

    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '172.16.17.145'],
    ];


return $config;
